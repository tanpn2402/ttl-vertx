package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessQueryLoanRefundCreationInfo extends DefaultDefMess {
	
	public DeffMessQueryLoanRefundCreationInfo(){
		super("HKSBOLR003", "20021218201009", "", "01", "2", "123456", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		//ADDTAG
	}
	
}

