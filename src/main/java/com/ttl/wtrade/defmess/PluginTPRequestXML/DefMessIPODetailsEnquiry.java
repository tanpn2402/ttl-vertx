package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessIPODetailsEnquiry extends DefaultDefMess {
	
	public DefMessIPODetailsEnquiry(){
		super("HKSWB007Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[7];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[2] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "");
		tags[3] = new DefMessTag("ENTITLEMENTID", "entitlementid", "");
		tags[4] = new DefMessTag("LOTSPREADDETAIL", "lotspreaddetail", "N");
		tags[5] = new DefMessTag("CALCULATEFEEFLAG", "calculatefeeflag", "N");
		tags[6] = new DefMessTag("APPLIEDQTY", "appliedqty", "");
		//ADDTAG
	}
	
}

