package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessResetToken extends DefaultDefMess {
	
	public DeffMessResetToken(){
		super("HKSTK003Q03", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "00001");
		tags[1] = new DefMessTag("SERINO", "serino", "1234567890");
		tags[2] = new DefMessTag("TOKEN", "token", "00001");
		//ADDTAG
	}
	
}

