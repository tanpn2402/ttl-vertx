package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessIPOGenConfirmRequest_zh_TW extends DefaultDefMess {
	
	public DefMessIPOGenConfirmRequest_zh_TW(){
		super("HKSWB008Q01", "20040507112730", "001", "01", "2", "123456", "0000000000000000000000004", "zh", "TW");
		tags = new DefMessTag[24];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("ACCOUNTSEQ", "accountseq", "");
		tags[2] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "");
		tags[3] = new DefMessTag("ENTITLEMENTID", "entitlementid", "");
		tags[4] = new DefMessTag("APPLIEDQTY", "appliedqty", "");
		tags[5] = new DefMessTag("CONTACTNUMBER", "contactnumber", "");
		tags[6] = new DefMessTag("CONTACTEMAIL", "contactemail", "");
		tags[7] = new DefMessTag("CONTACTMOBILE", "contactmobile", "");
		tags[8] = new DefMessTag("CONTACTSMSRECEIEVED", "contactsmsreceieved", "");
		tags[9] = new DefMessTag("CONTACTSMSLANGUAGE", "contactsmszh", "");
		tags[10] = new DefMessTag("MARGINFINANCINGTYPE", "marginfinancingtype", "");
		tags[11] = new DefMessTag("CREATORUSERID", "creatoruserid", "");
		tags[12] = new DefMessTag("FINANCINGPLAN", "financingplan", "");
		tags[13] = new DefMessTag("LOANRATE", "loanrate", "");
		tags[14] = new DefMessTag("LOANAMT", "loanamt", "");
		tags[15] = new DefMessTag("FLATFEE", "flatfee", "");
		tags[16] = new DefMessTag("FINANCEFEE", "financefee", "");
		tags[17] = new DefMessTag("INTERESTRATEBASIS", "interestratebasis", "");
		tags[18] = new DefMessTag("INTERESTRATE", "interestrate", "");
		tags[19] = new DefMessTag("INTERESTAMOUNT", "interestamount", "");
		tags[20] = new DefMessTag("PASSWORDVERIFICATION", "passwordverification", "N");
		tags[21] = new DefMessTag("PASSWORD", "password", "");
		tags[22] = new DefMessTag("SSOUSERID", "ssouserid", "");
		tags[23] = new DefMessTag("VRUPASSWORDVERIFICATION", "vrupasswordverification", "N");
		//ADDTAG
	}
	
}

