package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessAddOrderToMultiGroup extends DefaultDefMess {
	
	public DefMessAddOrderToMultiGroup(){
		super("HKSBOMO001", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[13];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("BS", "bs", "");
		tags[2] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[3] = new DefMessTag("MARKETID", "marketid", "");
		tags[4] = new DefMessTag("QUANTITY", "quantity", "");
		tags[5] = new DefMessTag("ORDERTYPE", "ordertype", "");
		tags[6] = new DefMessTag("PRICE", "price", "");
		tags[7] = new DefMessTag("STOPPRICE", "stopprice", "");
		tags[8] = new DefMessTag("GOODTILLDATE", "goodtilldate", "");
		tags[9] = new DefMessTag("GROSSAMT", "grossamt", "");
		tags[10] = new DefMessTag("BANKID", "bankid", "");
		tags[11] = new DefMessTag("BANKACID", "bankacid", "");
		tags[12] = new DefMessTag("REFID", "refid", "");
		//ADDTAG
	}
	
}

