package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessQueryAvailableAmount extends DefaultDefMess {
	
	public DeffMessQueryAvailableAmount(){
		super("HKSWB001F02", "20040709022500", "001", "01", "KEVIN", "", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1");
		tags[1] = new DefMessTag("BANKID", "bankid", "");
		tags[2] = new DefMessTag("TRANSFERTYPE", "transfertype", "");
		tags[3] = new DefMessTag("BANKEXTERNALFEEID", "bankexternalfeeid", "");
		tags[4] = new DefMessTag("BANKINTERNALFEEID", "bankinternalfeeid", "");
		tags[5] = new DefMessTag("GETEXTERNALFEE", "getexternalfee", "Y");
		//ADDTAG
	}
	
}

