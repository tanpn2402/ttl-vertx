package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessSubmitBankAdvance extends DefaultDefMess {
	
	public DeffMessSubmitBankAdvance(){
		super("HKSWB001Q02", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[10];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("CLIENTNAME", "clientname", "");
		tags[3] = new DefMessTag("BANKID", "bankid", "");
		tags[4] = new DefMessTag("TPLUSX", "tplusx", "");
		tags[5] = new DefMessTag("TOTALAMT", "totalamt", "");
		tags[6] = new DefMessTag("LENDINGAMOUNT", "lendingamount", "");
		tags[7] = new DefMessTag("ORDERIDLIST", "orderidlist", "");
		tags[8] = new DefMessTag("CONTRACTIDLIST", "contractidlist", "");
		tags[9] = new DefMessTag("FEEID", "feeid", "");
		//ADDTAG
	}
	
}

