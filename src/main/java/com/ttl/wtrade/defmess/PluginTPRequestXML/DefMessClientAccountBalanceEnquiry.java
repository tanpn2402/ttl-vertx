package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessClientAccountBalanceEnquiry extends DefaultDefMess {
	
	public DefMessClientAccountBalanceEnquiry(){
		super("HKSOR007Q01", "20040709022500", "001", "01", "KEVIN", "", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[8];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1");
		tags[1] = new DefMessTag("INCLUDECASHACCOUNTSUMMARY", "includecashaccountsummary", "Y");
		tags[2] = new DefMessTag("INCLUDETRADINGACCOUNTSUMMARY", "includetradingaccountsummary", "Y");
		tags[3] = new DefMessTag("INCLUDETRADINGACCOUNTDETAILS", "includetradingaccountdetails", "Y");
		tags[4] = new DefMessTag("INTERNET", "internet", "Y");
		tags[5] = new DefMessTag("ENABLEADVANCEMONEYQUERY", "enableadvancemoneyquery", "Y");
		tags[6] = new DefMessTag("BANKID", "bankid", "");
		tags[7] = new DefMessTag("BANKACID", "bankacid", "");
		//ADDTAG
	}
	
}

