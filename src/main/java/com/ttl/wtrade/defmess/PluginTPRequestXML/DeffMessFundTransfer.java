package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessFundTransfer extends DefaultDefMess {
	
	public DeffMessFundTransfer(){
		super("HKSWB001F01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[25];
		tags[0] = new DefMessTag("OPRID", "oprid", "");
		tags[1] = new DefMessTag("EMAILLIST", "emaillist", "");
		tags[2] = new DefMessTag("CLIENTID", "clientid", "00001");
		tags[3] = new DefMessTag("DIRECTION", "direction", "D");
		tags[4] = new DefMessTag("BANKID", "bankid", "ORS");
		tags[5] = new DefMessTag("DESTBANKID", "destbankid", "ORS");
		tags[6] = new DefMessTag("DESTCLIENTID", "destclientid", "");
		tags[7] = new DefMessTag("DESTACCOUNTNAME", "destaccountname", "");
		tags[8] = new DefMessTag("AMOUNT", "amount", "");
		tags[9] = new DefMessTag("PASSWORDVERIFICATION", "passwordverification", "N");
		tags[10] = new DefMessTag("PASSWORD", "password", "123456");
		tags[11] = new DefMessTag("SCODEVERIFICATION", "scodeverification", "N");
		tags[12] = new DefMessTag("SECURITYCODE", "securitycode", "");
		tags[13] = new DefMessTag("TRANSFERTYPE", "transfertype", "");
		tags[14] = new DefMessTag("REMARK", "remark", "");
		tags[15] = new DefMessTag("WAIVEALLFLAG", "waiveallflag", "");
		tags[16] = new DefMessTag("BANKEXTERNALFEEID", "bankexternalfeeid", "");
		tags[17] = new DefMessTag("BANKINTERNALFEEID", "bankinternalfeeid", "");
		tags[18] = new DefMessTag("BANKSYSTEMFEEID", "banksystemfeeid", "");
		tags[19] = new DefMessTag("BANKSYSTEMID", "banksystemid", "");
		tags[20] = new DefMessTag("INPUTBANKNAME", "inputbankname", "");
		tags[21] = new DefMessTag("INPUTBANKBRANCH", "inputbankbranch", "");
		tags[22] = new DefMessTag("FULLNAME", "fullname", "");
		tags[23] = new DefMessTag("WITHDRAWAMT", "withdrawamt", "");
		tags[24] = new DefMessTag("AVAIABLEAMT", "avaiableamt", "");
		//ADDTAG
	}
	
}

