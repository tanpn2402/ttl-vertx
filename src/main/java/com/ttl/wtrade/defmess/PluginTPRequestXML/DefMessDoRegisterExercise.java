package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessDoRegisterExercise extends DefaultDefMess {
	
	public DefMessDoRegisterExercise(){
		super("HKSBOENT003", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[9];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[2] = new DefMessTag("MARKETID", "marketid", "");
		tags[3] = new DefMessTag("ENTITLEMENTID", "entitlementid", "");
		tags[4] = new DefMessTag("QUANTITY", "quantity", "");
		tags[5] = new DefMessTag("LOTSIZE", "lotsize", "");
		tags[6] = new DefMessTag("TRADEDATE", "tradedate", "");
		tags[7] = new DefMessTag("LOCATIONID", "locationid", "");
		tags[8] = new DefMessTag("INTERFACESEQ", "interfaceseq", "-1");
		//ADDTAG
	}
	
}

