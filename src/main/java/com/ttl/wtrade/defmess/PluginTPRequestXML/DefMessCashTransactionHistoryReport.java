package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessCashTransactionHistoryReport extends DefaultDefMess {
	
	public DefMessCashTransactionHistoryReport(){
		super("HKSWBOTAI02", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[8];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("FROMDATE", "fromdate", "2002-01-01");
		tags[2] = new DefMessTag("TODATE", "todate", "2002-12-01");
		tags[3] = new DefMessTag("STARTRECORD", "startrecord", "");
		tags[4] = new DefMessTag("ENDRECORD", "endrecord", "");
		tags[5] = new DefMessTag("LANGUAGE", "en", "");
		tags[6] = new DefMessTag("TYPE", "type", "");
		tags[7] = new DefMessTag("ISEXPORTDATA", "isexportdata", "");
		//ADDTAG
	}
	
}

