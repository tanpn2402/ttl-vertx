package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessCheckClientExisting extends DefaultDefMess {
	
	public DeffMessCheckClientExisting(){
		super("HKSBOCCE01", "20120502155230", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		//ADDTAG
	}
	
}

