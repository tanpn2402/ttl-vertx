package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessAuthenticateEntrustChallenge extends DefaultDefMess {
	
	public DeffMessAuthenticateEntrustChallenge(){
		super("HKSTK002Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "00001");
		tags[1] = new DefMessTag("TOKEN", "token", "00001");
		tags[2] = new DefMessTag("SERIAL1", "serial1", "");
		tags[3] = new DefMessTag("SERIAL2", "serial2", "");
		tags[4] = new DefMessTag("ANSWER1", "answer1", "");
		tags[5] = new DefMessTag("ANSWER2", "answer2", "");
		//ADDTAG
	}
	
}

