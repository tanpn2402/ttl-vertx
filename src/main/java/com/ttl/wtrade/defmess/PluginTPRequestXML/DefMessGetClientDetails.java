package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessGetClientDetails extends DefaultDefMess {
	
	public DefMessGetClientDetails(){
		super("HKSCI001Q01", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		//ADDTAG
	}
	
}

