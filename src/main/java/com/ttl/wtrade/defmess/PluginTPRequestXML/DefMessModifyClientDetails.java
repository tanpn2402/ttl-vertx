package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessModifyClientDetails extends DefaultDefMess {
	
	public DefMessModifyClientDetails(){
		super("HKSCD001Q01", "20021122015345", "001", "01", "1", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[12];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("ADDRESSTYPEID", "addresstypeid", "8");
		tags[3] = new DefMessTag("EMAIL", "email", "");
		tags[4] = new DefMessTag("OLDEMAIL", "oldemail", "");
		tags[5] = new DefMessTag("PHONENUMBER", "phonenumber", "");
		tags[6] = new DefMessTag("OLDPHONENUMBER", "oldphonenumber", "");
		tags[7] = new DefMessTag("ADDRESS1", "address1", "");
		tags[8] = new DefMessTag("ADDRESS2", "address2", "");
		tags[9] = new DefMessTag("ADDRESS3", "address3", "");
		tags[10] = new DefMessTag("ADDRESS4", "address4", "");
		tags[11] = new DefMessTag("ADDRESS5", "address5", "");
		//ADDTAG
	}
	
}

