package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessUIManagementRequest extends DefaultDefMess {
	
	public DefMessUIManagementRequest(){
		super("HKSBOUIM001", "20110512110209", "", "01", "ITRADE", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[8];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("GROUPID", "groupid", "");
		tags[3] = new DefMessTag("GROUPNAME", "groupname", "");
		tags[4] = new DefMessTag("GROUPTYPE", "grouptype", "");
		tags[5] = new DefMessTag("ISDEFAULT", "isdefault", "");
		tags[6] = new DefMessTag("SAVEDCONTENT", "savedcontent", "");
		tags[7] = new DefMessTag("ACTION", "action", "");
		//ADDTAG
	}
	
}

