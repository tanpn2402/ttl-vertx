package com.ttl.wtrade.defmess.PluginTPRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessQuerySoldOrderMatchedList extends DefaultDefMess {
	
	public DeffMessQuerySoldOrderMatchedList(){
		super("HKSWB001AN05", "20021218201009", "", "01", "2", "123456", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[2];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("SETTLEDATE", "settledate", "");
		//ADDTAG
	}
	
}

