package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessClientPortfolioEnquiry extends DefaultDefMess {
	
	public DefMessClientPortfolioEnquiry(){
		super("HKSOR002Q022", "20021227054828", "001", "01", "ITRADE", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("INTERNET", "internet", "Y");
		tags[3] = new DefMessTag("WITHTRADE", "withtrade", "N");
		//ADDTAG
	}
	
}

