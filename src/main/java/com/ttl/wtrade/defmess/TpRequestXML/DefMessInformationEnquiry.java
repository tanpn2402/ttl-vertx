package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessInformationEnquiry extends DefaultDefMess {
	
	public DefMessInformationEnquiry(){
		super("HKSWB029Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("INFORMATIONID", "informationid", "");
		//ADDTAG
	}
	
}

