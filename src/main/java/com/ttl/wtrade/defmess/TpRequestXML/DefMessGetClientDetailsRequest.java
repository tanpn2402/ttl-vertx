package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessGetClientDetailsRequest extends DefaultDefMess {
	
	public DefMessGetClientDetailsRequest(){
		super("HKSOR001Q02", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[2];
		tags[0] = new DefMessTag("DISPLAYSETTLEMENTACCS", "displaysettlementaccs", "Y");
		tags[1] = new DefMessTag("CLIENTID", "clientid", "2");
		//ADDTAG
	}
	
}

