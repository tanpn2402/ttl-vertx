package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessDoSubmitSignOrder extends DefaultDefMess {
	
	public DefMessDoSubmitSignOrder(){
		super("HKSFOSO00002", "20021218201009", "", "01", "1234567890", "1234567890123456789012345678", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[2];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("DETAIL", "detail", "");
		//ADDTAG
	}
	
}

