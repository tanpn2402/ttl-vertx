package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessLogin extends DefaultDefMess {
	
	public DefMessLogin(){
		super("HKSSN001Q01", "20021218201009", "0", "01", "COMPANY", "123456", "2002111820100901564885275", "en", "US");
		tags = new DefMessTag[10];
		tags[0] = new DefMessTag("CLIENTID", "mvClientID", "1");
		tags[1] = new DefMessTag("PASSWORD", "mvPassword", "123456");
		tags[2] = new DefMessTag("EBPASSWORD", "ebpassword", "123456");
		tags[3] = new DefMessTag("HOSTSTRING", "hoststring", "00");
		tags[4] = new DefMessTag("INTERNET", "internet", "Y");
		tags[5] = new DefMessTag("ITRADESERVERIP", "itradeserverip", "");
		tags[6] = new DefMessTag("CHECKWWWENABLED", "checkwwwenabled", "");
		tags[7] = new DefMessTag("SKIPPASSWORDCHECKING", "skippasswordchecking", "N");
		tags[8] = new DefMessTag("SINGLESIGNONPHASE", "singlesignonphase", "0");
		tags[9] = new DefMessTag("CINO", "cino", "");
		//ADDTAG
	}
	
}

