package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessOrderAndDepositWithdrawalEnquiry extends DefaultDefMess {
	
	public DefMessOrderAndDepositWithdrawalEnquiry(){
		super("HKSTQ001Q01", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "");
		tags[2] = new DefMessTag("FROMDATE", "fromdate", "2002-01-01");
		tags[3] = new DefMessTag("TODATE", "todate", "2002-12-01");
		tags[4] = new DefMessTag("INTERNET", "internet", "Y");
		tags[5] = new DefMessTag("TXNTYPE", "txntype", "'ABC'");
		//ADDTAG
	}
	
}

