package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessStockInfo extends DefaultDefMess {
	
	public DefMessStockInfo(){
		super("HKSOR007Q01", "20091127132830", "888", "01", "USR1", "123456", "0000000011", "en", "US");
		tags = new DefMessTag[9];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1");
		tags[1] = new DefMessTag("INCLUDECASHACCOUNTSUMMARY", "includecashaccountsummary", "");
		tags[2] = new DefMessTag("INCLUDETRADINGACCOUNTSUMMARY", "includetradingaccountsummary", "");
		tags[3] = new DefMessTag("INCLUDETRADINGACCOUNTDETAILS", "includetradingaccountdetails", "");
		tags[4] = new DefMessTag("Settings", "settings", "");
		tags[5] = new DefMessTag("BANKID", "bankid", "");
		tags[6] = new DefMessTag("BANKACID", "bankacid", "");
		tags[7] = new DefMessTag("MARKETID", "marketid", "");
		tags[8] = new DefMessTag("ISMARGINCLIENT", "ismarginclient", "");
		//ADDTAG
	}
	
}

