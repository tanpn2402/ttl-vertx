package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessMarginPlanRequest extends DefaultDefMess {
	
	public DefMessMarginPlanRequest(){
		super("HKSWB036Q01", "20040507112730", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("ENTITLEMENTID", "entitlementid", "");
		tags[1] = new DefMessTag("LOANAMOUNTLESSTHAN", "loanamountlessthan", "");
		tags[2] = new DefMessTag("INTERESTRATE", "interestrate", "");
		//ADDTAG
	}
	
}

