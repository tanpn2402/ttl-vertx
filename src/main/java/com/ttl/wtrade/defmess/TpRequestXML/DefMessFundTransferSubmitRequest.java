package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessFundTransferSubmitRequest extends DefaultDefMess {
	
	public DefMessFundTransferSubmitRequest(){
		super("HKSWB005Q02", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "00001");
		tags[1] = new DefMessTag("DIRECTION", "direction", "D");
		tags[2] = new DefMessTag("AMOUNT", "amount", "1");
		//ADDTAG
	}
	
}

