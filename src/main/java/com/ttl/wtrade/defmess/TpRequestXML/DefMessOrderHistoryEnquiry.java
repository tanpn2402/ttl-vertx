package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessOrderHistoryEnquiry extends DefaultDefMess {
	
	public DefMessOrderHistoryEnquiry(){
		super("HKSWB010Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[9];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[2] = new DefMessTag("MARKETID", "marketid", "");
		tags[3] = new DefMessTag("BS", "bs", "");
		tags[4] = new DefMessTag("FROMTIME", "fromtime", "");
		tags[5] = new DefMessTag("TOTIME", "totime", "");
		tags[6] = new DefMessTag("SORTING", "sorting", "");
		tags[7] = new DefMessTag("STATUSFILTER", "statusfilter", "");
		tags[8] = new DefMessTag("INTERNET", "internet", "Y");
		//ADDTAG
	}
	
}

