package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessScripOptionStatusEnquiry extends DefaultDefMess {
	
	public DefMessScripOptionStatusEnquiry(){
		super("HKSWB024Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "");
		tags[2] = new DefMessTag("ACCOUNTSEQ", "accountseq", "");
		//ADDTAG
	}
	
}

