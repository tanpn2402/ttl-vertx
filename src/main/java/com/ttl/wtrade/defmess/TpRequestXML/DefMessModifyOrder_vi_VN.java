package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessModifyOrder_vi_VN extends DefaultDefMess {
	
	public DefMessModifyOrder_vi_VN(){
		super("HKSOR004Q01", "20040610104838", "001", "01", "2", "123456", "0000000000000000000000004", "vi", "VN");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("CHANNELID", "channelid", "INT");
		tags[1] = new DefMessTag("MESSAGE", "message", "");
		tags[2] = new DefMessTag("LOOP_ORDER", "loop_order", "");
		//ADDTAG
	}
	
}

