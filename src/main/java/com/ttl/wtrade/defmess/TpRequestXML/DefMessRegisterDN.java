package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessRegisterDN extends DefaultDefMess {
	
	public DefMessRegisterDN(){
		super("HKSWB013Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[3];
		tags[0] = new DefMessTag("TOPICNAME", "topicname", "");
		tags[1] = new DefMessTag("WINDOWSID", "windowsid", "999999");
		tags[2] = new DefMessTag("SUBSCRIBE", "subscribe", "");
		//ADDTAG
	}
	
}

