package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessDownloadTradeDate extends DefaultDefMess {
	
	public DefMessDownloadTradeDate(){
		super("HKSWB019Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("NUMBEROFDAYS", "numberofdays", "");
		//ADDTAG
	}
	
}

