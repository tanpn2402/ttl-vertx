package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessOrderEnquiry extends DefaultDefMess {
	
	public DefMessOrderEnquiry(){
		super("HKSOR002Q021", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[19];
		tags[0] = new DefMessTag("ENTITYID", "entityid", "VDSC");
		tags[1] = new DefMessTag("CLIENTID", "clientid", "2");
		tags[2] = new DefMessTag("STATUS", "status", "");
		tags[3] = new DefMessTag("INTERNALSTATUS", "internalstatus", "");
		tags[4] = new DefMessTag("OPERATORID", "operatorid", "");
		tags[5] = new DefMessTag("CHANNELID", "channelid", "");
		tags[6] = new DefMessTag("BRANCHID", "branchid", "0");
		tags[7] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[8] = new DefMessTag("FUNCTIONNAME", "functionname", "HKSOS002");
		tags[9] = new DefMessTag("SORTING", "sorting", "");
		tags[10] = new DefMessTag("STATIC", "static", "Y");
		tags[11] = new DefMessTag("INTERNET", "internet", "Y");
		tags[12] = new DefMessTag("WINDOWSID", "windowsid", "1");
		tags[13] = new DefMessTag("STARTRECORD", "startrecord", "1");
		tags[14] = new DefMessTag("ENDRECORD", "endrecord", "100");
		tags[15] = new DefMessTag("ORDERTYPE", "ordertype", "");
		tags[16] = new DefMessTag("ORDERBS", "orderbs", "");
		tags[17] = new DefMessTag("INSTRUMENTID", "instrumentid", "");
		tags[18] = new DefMessTag("ORDERID", "orderid", "");
		//ADDTAG
	}
	
}

