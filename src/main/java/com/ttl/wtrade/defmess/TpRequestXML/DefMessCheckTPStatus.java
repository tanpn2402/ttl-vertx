package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessCheckTPStatus extends DefaultDefMess {
	
	public DefMessCheckTPStatus(){
		super("HKSWB022Q01", "20021209053839", "HK", "01", "1", "", "0000000000000000000000001", "en", "US");
		tags = new DefMessTag[2];
		tags[0] = new DefMessTag("MARKETSTATUS", "marketstatus", "CT");
		tags[1] = new DefMessTag("ORDEREXPIRED", "orderexpired", "Y");
		//ADDTAG
	}
	
}

