package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DeffMessGetBranchIDByClient extends DefaultDefMess {
	
	public DeffMessGetBranchIDByClient(){
		super("HKSBOBRNID001", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[1];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1");
		//ADDTAG
	}
	
}

