package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessResetPassword extends DefaultDefMess {
	
	public DefMessResetPassword(){
		super("HKSWB028Q01", "20021122015345", "001", "01", "1", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("BIRTHDAY", "birthday", "");
		tags[2] = new DefMessTag("IDNUMBER", "idnumber", "");
		tags[3] = new DefMessTag("BRANCHID", "branchid", "");
		tags[4] = new DefMessTag("NEWPASSWORD", "newpassword", "");
		tags[5] = new DefMessTag("ISAUTOGENPASSWORD", "isautogenpassword", "N");
		//ADDTAG
	}
	
}

