package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessOrderFormAuthorizedRequest extends DefaultDefMess {
	
	public DefMessOrderFormAuthorizedRequest(){
		super("HKSOFA001Q01", "20021122015806", "001", "01", "1", "", "0000000000000000000000005", "en", "US");
		tags = new DefMessTag[6];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "");
		tags[2] = new DefMessTag("TYPE", "type", "");
		tags[3] = new DefMessTag("ORDERIDLIST", "orderidlist", "");
		tags[4] = new DefMessTag("FROMDATE", "fromdate", "");
		tags[5] = new DefMessTag("TODATE", "todate", "");
		//ADDTAG
	}
	
}

