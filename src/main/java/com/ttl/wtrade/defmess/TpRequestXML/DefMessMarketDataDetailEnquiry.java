package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessMarketDataDetailEnquiry extends DefaultDefMess {
	
	public DefMessMarketDataDetailEnquiry(){
		super("HKSWB030Q01", "20040601111216", "001", "01", "2", "123456", "0000000000000000000000004", "en", "US");
		tags = new DefMessTag[4];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "");
		tags[1] = new DefMessTag("MARKETID", "marketid", "");
		tags[2] = new DefMessTag("SYMBOLID", "symbolid", "");
		tags[3] = new DefMessTag("NEWQUERY", "newquery", "");
		//ADDTAG
	}
	
}

