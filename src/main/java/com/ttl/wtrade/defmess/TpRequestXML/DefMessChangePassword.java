package com.ttl.wtrade.defmess.TpRequestXML;

import com.ttl.wtrade.defmess.DefMessTag;
import com.ttl.wtrade.defmess.DefaultDefMess;

public class DefMessChangePassword extends DefaultDefMess {
	
	public DefMessChangePassword(){
		super("HKSSN003Q01", "20021122015345", "001", "01", "1", "", "0000000000000000000000003", "en", "US");
		tags = new DefMessTag[10];
		tags[0] = new DefMessTag("CLIENTID", "clientid", "1000000011");
		tags[1] = new DefMessTag("TRADINGACCSEQ", "tradingaccseq", "1");
		tags[2] = new DefMessTag("OLDPASSWORD", "oldpassword", "12345678");
		tags[3] = new DefMessTag("NEWPASSWORD", "newpassword", "11111111");
		tags[4] = new DefMessTag("EBPASSWORD", "ebpassword", "");
		tags[5] = new DefMessTag("EBPASSWORDVERIFIED", "ebpasswordverified", "N");
		tags[6] = new DefMessTag("HOSTSTRING", "hoststring", "00");
		tags[7] = new DefMessTag("INTERNET", "internet", "Y");
		tags[8] = new DefMessTag("ISSECONDPASSWORD", "issecondpassword", "N");
		tags[9] = new DefMessTag("ISRESETPASSWORD", "isresetpassword", "N");
		//ADDTAG
	}
	
}

