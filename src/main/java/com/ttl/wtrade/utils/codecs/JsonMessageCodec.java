package com.ttl.wtrade.utils.codecs;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;
import io.vertx.core.json.JsonObject;
import net.sf.json.JSON;

import java.util.Map;
import java.util.Set;

public class JsonMessageCodec implements MessageCodec<JsonObject, JsonObject> {
    public JsonMessageCodec() {
        super();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public void encodeToWire(Buffer buffer, JsonObject EBMessage) {
//        jsonObj.put("mess", EBMessage.getMess());
//        jsonObj.put("key", EBMessage.getKey());
//        jsonObj.put("value", EBMessage.getValue());
        String json2String = EBMessage.encode();

        int length = json2String.getBytes().length;

        buffer.appendInt(length);
        buffer.appendString(json2String);
    }

    @Override
    public JsonObject decodeFromWire(int i, Buffer buffer) {

        int length = buffer.getInt(i);

        String jsonStr = buffer.getString(i += 4, i += length);
        return new JsonObject(jsonStr);
    }

    @Override
    public JsonObject transform(JsonObject EBMessage) {
        return EBMessage;
    }

    @Override
    public String name() {
        return "jsonCodec";
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}
