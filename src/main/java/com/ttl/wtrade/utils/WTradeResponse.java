package com.ttl.wtrade.utils;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.RoutingContext;

import java.util.HashSet;
import java.util.Set;

public class WTradeResponse implements IDefaultResponse {
    private RoutingContext mvRouteCtx;
    private Set<Cookie> mvCookies = null;
    private JsonObject mvResJson = null;
    private String mvClientID = null;

    public WTradeResponse(RoutingContext mvRouteCtx, String mvClientID, Set<Cookie> mvCookies) {
        this.mvRouteCtx = mvRouteCtx;
        this.mvClientID = mvClientID;
        this.mvCookies = mvCookies;
    }

    @Override
    public boolean validate() {
        if (mvResJson == null || mvRouteCtx == null) {
            return false;
        }
        return true;
    }

    @Override
    public void send() {
        if (validate()) {
            //Add cookie to response message
            for (Cookie cookie : mvCookies) {
                mvRouteCtx.addCookie(cookie);
            }
            //Send ResBean as Json to user
            HttpServerResponse res = mvRouteCtx.response().setChunked(true);
            res.end(mvResJson.toBuffer());

            WTradeLogger.print(String.format("WTradeResponse | %s", mvClientID), String.format("Response: %s", mvResJson.toString()));
        }else {
            WTradeLogger.print(String.format("WTradeResponse | %s", mvClientID), "Null JSON");
        }
    }

    public RoutingContext getMvRouteCtx() {
        return mvRouteCtx;
    }

    public void setMvRouteCtx(RoutingContext mvRouteCtx) {
        this.mvRouteCtx = mvRouteCtx;
    }

    public Set getMvCookies() {
        return mvCookies;
    }

    public void setMvCookies(Set mvCookies) {
        this.mvCookies = mvCookies;
    }

    public JsonObject getMvResJson() {
        return mvResJson;
    }

    public void setMvResJson(JsonObject mvResJson) {
        this.mvResJson = mvResJson;
    }

    public void addCookie(Cookie cookie) {
        if (this.mvCookies == null) {
            this.mvCookies = new HashSet<>();
        }

        mvCookies.add(cookie);
    }
}
