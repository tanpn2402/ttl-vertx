package com.ttl.wtrade.utils;

import java.io.Serializable;

public interface IDefaultRequest extends Serializable {
    boolean validate();
}
