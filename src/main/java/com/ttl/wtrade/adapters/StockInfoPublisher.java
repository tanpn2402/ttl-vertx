//package com.ttl.wtrade.adapters;
//
//import com.ttl.old.itrade.ITradeAtmosphereAPI;
//import com.ttl.old.itrade.biz.model.ITradeUser;
//import com.ttl.old.itrade.web.ticker.TickerViewProccessor;
//import com.ttl.wtrade.utils.WTradeLogger;
//import org.atmosphere.cpr.AtmosphereResource;
//
//import java.util.Collection;
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;
//
//public class StockInfoPublisher {
//    public static final String TOPIC_PROTOCOL = "/${service}/${action}/${clientID}";
//
//    public static final String CONTRACT_SERVICE = "service";
//    public static final String CONTRACT_ACTION = "action";
//    public static final String CONTRACT_TOPIC = "action";
//    public static final String CONTRACT_CLIENTID = "clientID";
//
//    public static final String BROADCAST_SERVICE = "ITradePushServer";
//    public static final String BROADCAST_SUBCRIBE = "Subscribe";
//
//    private volatile static ITradeAtmosphereAPI instance = null;
//    private UserRegister userRegister = null;
//
//
//    private class UserRegister {
//        ConcurrentHashMap<String, ITradeUser> users = new ConcurrentHashMap<String, ITradeUser>();
//
//        public void filter(Collection<AtmosphereResource> resource) {
//            for (AtmosphereResource r : resource) {
//                final String uuid = r.uuid();
//                if (!users.containsKey(uuid)) {
//                    getAtmosphereResourceFactory().remove(uuid);
//                }
//            }
//        }
//
//        public ITradeUser offer(AtmosphereResource resource, String clientID) {
//            ITradeUser user = users.get(resource.uuid());
//            ITradeUser userSession = new ITradeUser();
//
//            WTradeLogger.print("AtmosphereAPI","Create new user session");
//
//            userSession.setMvAccountID(clientID);
//            userSession.setMvSeesionID(resource.uuid());
//            userSession.setMvLoginIpAddress(resource.getRequest().getRemoteAddr());
//            userSession.setMvFullName(clientID);
//            userSession.setMvAccountName(clientID);
//            userSession.setMvTickerViewProccessor(new TickerViewProccessor(userSession));
//            if (user == null) {
//                if (userSession != null) {
//                    disconnect(userSession);
//                    if (!users.containsKey(clientID)) {
//                        users.put(clientID, userSession);
//                        WTradeLogger.print("AtmosphereAPI",String.format("-|  Mapped: %s : %s",clientID,resource.uuid()));
//                    }
//                }
//                user = userSession;
//            } else {
//                disconnect(user);
//            }
//            return user;
//        }
//
//        public void disconnect(ITradeUser user) {
//            String uuid = retriveUuid(user.getMvAccountID());
//            if (users.contains(user) && !uuid.equals("")) {
//                users.remove(uuid);
//            }
//        }
//
//        public ITradeUser retrieve(final String clientId) {
//            ITradeUser sessionuser = null;
//            Set<Map.Entry<String, ITradeUser>> entries = users.entrySet();
//            for (Map.Entry<String, ITradeUser> entry : entries) {
//                if (entry.getValue().getMvAccountID().equals(clientId)) {
//                    sessionuser = (ITradeUser) entry.getValue();
//                    break;
//                }
//            }
//            return sessionuser;
//        }
//
//        public ITradeUser retrieve(final AtmosphereResource r) {
//            return users.get(r.uuid());
//        }
//
//        public AtmosphereResource retriveAtmosphereResource(final ITradeUser itradeUser) {
//            return retriveAtmosphereResource(itradeUser.getMvAccountID());
//
//        }
//
//        public AtmosphereResource retriveAtmosphereResource(final String loginid) {
//            Set<Map.Entry<String, ITradeUser>> entries = users.entrySet();
//            AtmosphereResource atmosphereResource = null;
//            for (Iterator<Map.Entry<String, ITradeUser>> iterator = entries.iterator(); iterator.hasNext(); ) {
//                Map.Entry<String, ITradeUser> entry = iterator.next();
//                if (loginid.equals(entry.getValue().getMvAccountID())) {
//                    atmosphereResource = getAtmosphereResourceFactory().find(entry.getKey());
//                    break;
//                }
//            }
//            return atmosphereResource;
//        }
//
//        public String retriveUuid(String clienid) {
//            Set<Map.Entry<String, ITradeUser>> entries = users.entrySet();
//            for (Iterator<Map.Entry<String, ITradeUser>> iterator = entries.iterator(); iterator.hasNext(); ) {
//                Map.Entry<String, ITradeUser> entry = iterator.next();
//                if (clienid.equals(entry.getValue().getMvAccountID())) {
//                    return entry.getKey();
//                }
//            }
//            return "";
//        }
//
//    }
//}
