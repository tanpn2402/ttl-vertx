//package com.ttl.wtrade.tp;
//
//import java.util.Hashtable;
//import java.util.Iterator;
//import java.util.Vector;
//
//import com.systekit.common.msg.IMsgXMLNode;
//import com.ttl.old.itrade.util.Log;
//import com.ttl.wtrade.tp.TPConnector;
//
//public class TPManager extends Thread {
//
//	public static int svRequestWaitTimeOut = 10000;
//	private TPConnector ivTPConnector;
//	private Hashtable ivRequestPendingMap = new Hashtable();
//    private Hashtable ivRequestResultMap = new Hashtable();
//    
//	/**
//     * This method for Send XML file information
//     * @param pLinkID the link id
//     * @param pData the date
//     * @return the xml node message information
//     * @throws Exception the Exception
//     */
//    public IMsgXMLNode send(String pLinkID, String pData) throws Exception
//    {
//
//        Object lvWaitObject = new Object();
//        ivRequestPendingMap.put(pLinkID, lvWaitObject);
//        try
//        {
//            synchronized (lvWaitObject)
//            {
//                ivTPConnector.send(pData.getBytes("UTF8"));
//
//                lvWaitObject.wait(svRequestWaitTimeOut);
//
//                // now, the result is available, remove the result from map
//                return (IMsgXMLNode) ivRequestResultMap.remove(pLinkID);
//            }
//        }
//        catch (Exception e)
//        {
//            throw e;
//        }
//        finally
//        {
//            ivRequestPendingMap.remove(pLinkID);
//        }
//    }
//    
//    /**
//     * start a connector thread to received data
//     */
//        public void run()
//        {
//            // commented by mingl
//            // start a connector thread to received data
//			//		TPConnectorThread lvTPConnectorThread = new TPConnectorThread(this, ivTPConnector);
//			//		lvTPConnectorThread.setName("TPConnector");
//			//		lvTPConnectorThread.start();
//
//            Vector lvTempDataVector = new Vector(1000);
//            String lvReceivedMessage;
//
//            while (true)
//            {
//                try
//                {
//                    synchronized (ivDataReceived)
//                    {
//                            if (ivDataReceived.size() == 0)
//                            {
//                                    ivDataReceived.wait();
//                            }
//                            lvTempDataVector.addAll(ivDataReceived);
//                            ivDataReceived.clear();
//                    }
//
//                    for (Iterator lvIter = lvTempDataVector.iterator(); lvIter.hasNext(); )
//                    {
//                            lvReceivedMessage = (String) lvIter.next();
//                            lvReceivedMessage = lvReceivedMessage.replace('\t', ' ');
//                            processReceivedMessage(lvReceivedMessage);
//                    }
//                    lvTempDataVector.clear();
//                }
//                catch (Exception e)
//                {
//                    Log.println("TPManager throws " + e.toString(), Log.ERROR_LOG);
//                }
//            }
//        }
//}
