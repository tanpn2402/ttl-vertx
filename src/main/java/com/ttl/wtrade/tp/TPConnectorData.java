package com.ttl.wtrade.tp;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

/**
 * CnnWinvestConnectorData is used for extracting TP message
 * The format of the message is the message body following by the message header, the message header
 * describes the message size(include header size). For example:
 * 
 * CnnWinvestConnectorData connector = cnnAMSConnectorData(10, 20000)
 * is expected to receive message in the following format: 0000000007\r\nMESSAGE
 */

class TPConnectorData
{
	private byte[]		ivBuffer;
	private int			ivCurrentIndex;
	private int			ivCurrentRead;
	private int			ivBufferLength;
	private int			ivHeaderLength;
	private InputStream ivInputStream = null;
	DecimalFormat		ivHeaderFormater;

	/**
	 * Construct a new CnnWinvestConnectorData
	 * @param pHeaderLength Header length of the incoming/outgoing message
	 * @param pBufferLength  Buffer size of incoming data stream
	 */
	public TPConnectorData(int pHeaderLength, int pBufferLength)
	{
		super();
		ivBuffer = new byte[pBufferLength];
		ivCurrentIndex = 0;
		ivCurrentRead = 0;
		ivBufferLength = pBufferLength;
		ivHeaderLength = pHeaderLength;
		String  lvFormat = "";
		for (int i = 0; i < pHeaderLength; i++)
		{
			lvFormat += "0";
		}
		ivHeaderFormater = new DecimalFormat(lvFormat);
	}
	/**
	 * This method for initialize inputStream
	 * @param pInputStream the inputStream
	 */
	public void init(InputStream pInputStream)
	{
		ivCurrentIndex = 0;
		ivCurrentRead = 0;
		ivInputStream = pInputStream;
	}
	/**
	 * This method for extract message
	 * @return the message
	 * @throws Exception the IOException
	 */
	public String extract() throws Exception
	{
		String  lvMessage;
		while ((lvMessage = extractMessage()) == null)
		{
			read();
		}
		return lvMessage;
	}
	/**
	 * This method for extract message
	 * @return the extract message
	 * @throws Exception the IOException
	 */
	private String extractMessage() throws Exception
	{
		int lvCurrentBufferLen = ivCurrentRead - ivCurrentIndex;
		if (lvCurrentBufferLen >= ivHeaderLength)
		{
			int lvMessageLength = Integer.parseInt(new String(ivBuffer, ivCurrentIndex, ivHeaderLength, "UTF8")) + ivHeaderLength + 2;
			if (lvCurrentBufferLen >= lvMessageLength)
			{
				String  lvReceivedMessage = new String(ivBuffer, ivCurrentIndex, lvMessageLength, "UTF8");

				ivCurrentIndex += lvMessageLength;
				return lvReceivedMessage.substring(ivHeaderLength + 2);
			}
			else
			{

				// move the in-completed message to the front of the buffer
				System.arraycopy(ivBuffer, ivCurrentIndex, ivBuffer, 0, lvCurrentBufferLen);
				ivCurrentRead = lvCurrentBufferLen;
				ivCurrentIndex = 0;

				// Message content is not fully received
				return null;
			}
		}
		else
		{

			// move the in-completed message to the front of the buffer
			System.arraycopy(ivBuffer, ivCurrentIndex, ivBuffer, 0, lvCurrentBufferLen);
			ivCurrentRead = lvCurrentBufferLen;
			ivCurrentIndex = 0;

			// Header length is not fully received
			return null;
		}
	}
	/**
	 * This method for inputStream reading
	 * @return the now reading
	 * @throws Exception the IOException
	 */
	private int read() throws Exception
	{
		int lvNowRead = 0;

		try
		{
			if (ivBufferLength - ivCurrentRead == 0)
			{
				throw new IOException("Socket receiving buffer has reached!");
			}
			lvNowRead = ivInputStream.read(ivBuffer, ivCurrentRead, ivBufferLength - ivCurrentRead);

			if (lvNowRead >= 0)
			{
				ivCurrentRead += lvNowRead;
			}
			else
			{

				// the stream returns -1
				throw new IOException("Connection is closed");
			}
		}
		catch (Exception lvException)
		{
			ivCurrentRead = 0;
			throw lvException;
		}
		return lvNowRead;
	}
	/**
	 * This method for inputStream clean up 
	 */
	public void cleanUp()
	{
		ivCurrentIndex = 0;
		ivCurrentRead = 0;
		try
		{
			if (ivInputStream != null)
			{
				ivInputStream.close();
				ivInputStream = null;
			}
		}
		catch (Exception e) {}
		finally
		{
			ivInputStream = null;
		}
	}
}
