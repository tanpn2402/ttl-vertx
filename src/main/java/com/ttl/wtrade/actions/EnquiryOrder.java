package com.ttl.wtrade.actions;

import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.ttl.old.itrade.hks.util.FieldSplitter;
import com.ttl.old.itrade.hks.util.TextFormatter;
import com.hazelcast.util.collection.Int2ObjectHashMap.KeySet;
import com.lowagie.text.pdf.hyphenation.TernaryTree.Iterator;
import com.sun.javafx.collections.MappingChange.Map;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.wtrade.actions.DefaultAction.STATUS;
import com.ttl.wtrade.defmess.DefaultDefMess;
import com.ttl.wtrade.utils.ResponseField;
import com.ttl.wtrade.utils.ResponseFieldBuilder;
import com.ttl.wtrade.utils.TPPool;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.utils.WTradeRequest;
import com.ttl.wtrade.utils.WTradeResponse;
import com.ttl.wtrade.utils.XMLMessageBuilder;
import com.ttl.wtrade.utils.XMLParser;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Session;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class EnquiryOrder extends DefaultAction {
	public EnquiryOrder(WTradeRequest pRequest, DefaultDefMess pDefaultDefMess){
		super(pRequest, pDefaultDefMess);
	}

	@Override
	public STATUS exec() {
		TPManager mvManager = TPPool.get(mvRequest.getMvAction());
        JsonObject mvParams = mvRequest.getMvJson();
        Session lvSession = mvRequest.getMvRouteCtx().session();
        String lvClientID = lvSession.get("CLIENTID").toString();
        JsonObject mvJSONResponse = new JsonObject();
        int mvTotalOrders = 0;
        
        try {
			IMsgXMLNode mvXMLMessage = XMLMessageBuilder.buildNode(mvParams, mvDefaultDefMess);
			mvXMLMessage.getChildNode("CLIENTID").setValue(lvClientID.substring(3, lvClientID.length()));	
			
			IMsgXMLNode mvReturnNode = mvManager.send(mvXMLMessage.getAttribute("resvr"), mvXMLMessage.toString());
			
			if(TPErrorHandling.TP_NORMAL == mvTPError.checkError(mvReturnNode)) {
				IMsgXMLNode lvNodeFnames = mvReturnNode.getChildNode(TagName.RESULT).getChildNode(TagName.FieldNames);
				IMsgXMLNodeList lvNodeListValues = mvReturnNode.getChildNode(TagName.RESULT).getChildNode(TagName.LoopRows)
						.getNodeList(TagName.Values);

				if (mvReturnNode.getChildNode(TagName.TOTALQTY) != null) {
					mvTotalOrders = Integer.parseInt(mvReturnNode.getChildNode(TagName.TOTALQTY).getValue());
				}
				mvJSONResponse.put("mvTotalOrders", mvTotalOrders);
				JSONArray lvJsonArr = new JSONArray();
				
				ConcurrentHashMap<String, ResponseField> fields = ResponseFieldBuilder.getFields();
				
				
				for (int i = 0; i < lvNodeListValues.size(); i++) {
					String lvFieldnames = lvNodeFnames.getValue();
					String lvValues = lvNodeListValues.getNode(i).getValue();
					Hashtable<String, ?> lvHMap = FieldSplitter.tokenize(lvFieldnames, lvValues, "|");
					
					Set<String> keys = lvHMap.keySet();
					JSONObject obj = new JSONObject();
					
					for(String key : keys){
						if(fields.containsKey(key)) {
							ResponseField t = (ResponseField)fields.get(key);
							
							obj.put(t.getMvReturn(), (String) lvHMap.get(key));
						} else {
							obj.put(key, (String) lvHMap.get(key));
						}
						
					}
					lvJsonArr.add(obj);
					

				}
				System.out.println(lvJsonArr.toString());
				mvJSONResponse.put("mvOrderBeanList", lvJsonArr);
				
			}
			else {
				
			}
			
			// send back
			WTradeResponse mvResponse = mvRequest.getReponse();
			mvResponse.setMvResJson(mvJSONResponse);
			mvResponse.send();			
		} catch (Exception e) {
			WTradeLogger.print(mvRequest.getMvAction(), " Something has gone wrong ", WTradeLogger.ERROR_MESSAGE);
		}
        return STATUS.NORMAL;
	}
}