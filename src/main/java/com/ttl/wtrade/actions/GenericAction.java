package com.ttl.wtrade.actions;

import com.systekit.common.msg.IMsgXMLNode;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.wtrade.defmess.DefaultDefMess;
import com.ttl.wtrade.utils.TPPool;
import com.ttl.wtrade.utils.WTradeLogger;
import com.ttl.wtrade.utils.WTradeRequest;
import com.ttl.wtrade.utils.WTradeResponse;
import com.ttl.wtrade.utils.XMLMessageBuilder;
import com.ttl.wtrade.utils.XMLParser;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class GenericAction extends DefaultAction {
	
	public GenericAction(WTradeRequest pRequest, DefaultDefMess pDefaultDefMess){
		super(pRequest, pDefaultDefMess);
	}

	@Override
	public STATUS exec() {
		if (!mvRequest.validate()) {
            WTradeLogger.print(this.getClass().getName(),"Invalid request", WTradeLogger.ERROR_MESSAGE);
            return STATUS.ERROR;
        }
        TPManager mvManager = TPPool.get(mvRequest.getMvAction());
        JsonObject mvParams = mvRequest.getMvJson();
        try {
        	// build message and send, after that get return value and init mvJSON (final result), mvRecordList (array of JSON incase mutiple records),
        	// mvRecordJSON(a record), mvNumOfRecord (number of records)
			IMsgXMLNode mvXMLMessage = XMLMessageBuilder.buildNode(mvParams, mvDefaultDefMess);
			IMsgXMLNode mvReturnNode = mvManager.send(mvXMLMessage.getAttribute("resvr"), mvXMLMessage.toString());
			JsonObject mvJSON = XMLParser.toJSONObject(mvReturnNode);
			
			// send back
			WTradeResponse mvResponse = mvRequest.getReponse();
			mvResponse.setMvResJson(mvJSON);
			mvResponse.send();			
		} catch (Exception e) {
			WTradeLogger.print(mvRequest.getMvAction(), " Something has gone wrong ", WTradeLogger.ERROR_MESSAGE);
		}
        return STATUS.NORMAL;
	}

}
