package com.ttl.wtrade.actions;

import com.ttl.wtrade.actions.DefaultAction.STATUS;
import com.ttl.wtrade.defmess.DefaultDefMess;
import com.ttl.wtrade.utils.WTradeRequest;
import com.ttl.wtrade.utils.WTradeResponse;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Session;

public class CheckSession extends DefaultAction{
	public CheckSession(WTradeRequest pRequest, DefaultDefMess pDefaultDefMess){
		super(pRequest, pDefaultDefMess);
	}

	@Override
	public STATUS exec() {
		// TODO Auto-generated method stub
		
		
		
		Session session = mvRequest.getMvRouteCtx().session();
    	String sID = "";
    	String clientID = "";
    	try {
    		sID = session.id();
    		clientID = session.get("CLIENTID");
    	}
    	finally {
    		System.out.println("CHECKSESSION  SessionID = " + sID + " with CLIENTID = " + clientID);
//    		clientID = "";
    	}
    	
    	
    	JsonObject o = new JsonObject();
    	o.put("mvResult", "");
    	o.put("mvResult_2", clientID == null ? "SESSION_EXPIRED" : "");
    	o.put("success", false);
    	

    	WTradeResponse mvResponse = mvRequest.getReponse();
		mvResponse.setMvResJson(o);
		mvResponse.send();	
		return STATUS.NORMAL;
	}
}
