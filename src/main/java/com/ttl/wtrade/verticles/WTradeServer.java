package com.ttl.wtrade.verticles;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.StockInfoSocket;
import com.ttl.old.itrade.util.Log;
import com.ttl.wtrade.utils.*;
import com.ttl.wtrade.utils.codecs.ActionRequestCodec;
import com.ttl.wtrade.utils.codecs.JsonMessageCodec;
import com.ttl.wtrade.utils.codecs.WorkerConfigCodec;
import com.ttl.wtrade.utils.config.ActionConfig;
import com.ttl.wtrade.utils.config.ConfigExtractor;
import com.ttl.wtrade.utils.config.WorkerConfig;
import com.ttl.wtrade.utils.ebmessages.IEBMessage;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.sstore.ClusteredSessionStore;
import io.vertx.ext.web.sstore.LocalSessionStore;
import org.apache.commons.httpclient.HttpStatus;
import org.atmosphere.cpr.ApplicationConfig;
import org.atmosphere.vertx.VertxAtmosphere;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class WTradeServer extends AbstractVerticle {
    private HttpServer httpServer;
    private Vertx vertx;
    private EventBus eventBus;
    private ITradeServlet itrade;
    private Properties WORKERS_CONFIG;
    private Map<String, WorkerConfig> workerMap = null;
    private Map<String, ActionConfig> actionMap = null;

    @Override
    public void init(Vertx vertx, Context context) {
        this.vertx = Vertx.vertx();
        this.httpServer = vertx.createHttpServer();
        eventBus = vertx.eventBus()
                .registerDefaultCodec(JsonObject.class, new JsonMessageCodec())
                .registerDefaultCodec(WTradeRequest.class, new ActionRequestCodec())
                .registerDefaultCodec(WorkerConfig.class, new WorkerConfigCodec());

//        Demo Action Map
//        Map<String, String> demo_config_map = new HashMap<>();
//        demo_config_map.put("doLogin", "DoLoginAction");
//        ActionFactory.generateFactory(demo_config_map);
        //Extract workers'config
        try {
            WORKERS_CONFIG = new Properties();
            WORKERS_CONFIG.load(new FileInputStream("WorkersConfig.ini"));

            ConfigExtractor extractor = new ConfigExtractor(WORKERS_CONFIG);
            this.workerMap = extractor.extractAllWorkerConfig();
            this.actionMap = extractor.getActionMap();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        vertx.executeBlocking(future -> {
            this.itrade = new ITradeServlet();
            String result = this.itrade.init();

            future.complete(result);
        }, asyncResult -> {
            WTradeLogger.print("WTradeServer", "ITradeServlet deployment result: " + asyncResult.result(), WTradeLogger.INFO_MESSAGE);
        });

        //Deploy MDS Worker
        DeploymentOptions MDSWorkerOption = new DeploymentOptions().setWorker(true).setInstances(160);
        DeploymentOptions TpWorkerOption = new DeploymentOptions().setWorker(true);

        vertx.deployVerticle(MDSWorker.class, MDSWorkerOption, res -> {
            if (res.succeeded()) {
                WTradeLogger.print("WTradeServer", String.format("Deploy [%s] successfully", MDSWorker.class));
//                    System.out.println("[WTradeServer] Deploy Worker Verticle MDSWorker is successfully");
                Log.println("[WTradeServer] Deploy Worker Verticle MDSWorker is successfully ", Log.DEBUG_LOG);
            } else {
                System.out.println("[WTradeServer] Deploy Worker Verticle MDSWorker is Error: " + res.cause());
                Log.println("[WTradeServer] Deploy Worker Verticle WatchStockTPWorker is Failled. Cause:  " + res.cause(), Log.ERROR_LOG);
            }
        });

        vertx.deployVerticle(WatchStockTPWorker.class, TpWorkerOption, res -> {
            if (res.succeeded()) {
                WTradeLogger.print("WTradeServer", String.format("Deploy [%s] successfully", WatchStockTPWorker.class));
//                            System.out.println("[WTradeServer] Deploy Worker Verticle WatchStockTPWorker is successfully");
                Log.println("[WTradeServer] Deploy Worker Verticle WatchStockTPWorker is successfully ", Log.DEBUG_LOG);
            } else {
                Log.println("[WTradeServer] Deploy Worker Verticle WatchStockTPWorker is Failled. Cause:  " + res.cause(), Log.ERROR_LOG);
                System.out.println("[WTradeServer] Deploy Worker Verticle WatchStockTPWorker is Error: " + res.cause());
            }
        });


        //Deploy TP Workers
        for (Map.Entry config : workerMap.entrySet()) {
            WorkerConfig curConfig = (WorkerConfig) config.getValue();
            JsonObject workerJSONConfig = new JsonObject().put("WorkerName", curConfig.getName());
            try {
//                ActionWorker willBeDeployedWorker = (ActionWorker) Class.forName(curConfig.getClassName()).newInstance();
                DeploymentOptions deploymentOptions = new DeploymentOptions()
                        .setWorker(true)
                        .setConfig(workerJSONConfig);
//                        .setInstances(16);   // set number of instance for each worker, more at http://vertx.io/docs/vertx-core/java/#_verticle_worker_pool

                vertx.deployVerticle((Class<? extends Verticle>) Class.forName(curConfig.getClassName()), deploymentOptions, res -> {
                    if (res.succeeded()) {
//                        WTradeLogger.print(WTradeServer.class.toString(), String.format("Deploy [%s] successfully", willBeDeployedWorker.getClass()));
                        eventBus.send(curConfig.getName() + "_Config", curConfig, settingConfigRes -> {
                            if (settingConfigRes.succeeded()) {
//                                WTradeLogger.print(WTradeServer.class.toString(), String.format("Set up [%s] successfully", willBeDeployedWorker.getClass()));
                            } else {
//                                WTradeLogger.print(WTradeServer.class.toString(), String.format("[%s] set up failed: %s", willBeDeployedWorker.getClass(), settingConfigRes.cause()));
                            }
                        });
                    } else {
//                        WTradeLogger.print(WTradeServer.class.toString(), String.format("[%s] deployment failed: %s", willBeDeployedWorker.getClass(), res.cause()));
                    }
                });
            
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        }


    }

    @Override
    public void start() {


        Router router = Router.router(this.vertx);
        router.route()
                .handler(BodyHandler.create())
                .handler(CookieHandler.create())
                .handler(SessionHandler.create(LocalSessionStore.create(vertx)));

        ////////////////////// ROUTER //////////////////////////////////
//        router.get("/*").handler(this::onAuth);
        //Watch List related handlers
        router.get("/ITradePushServer/StockInfo/:clientID").handler(this::onWatchListRequest);
        //Actions related handlers
        router.post("/Actions/:actionName").handler(this::onActionRequest)
        	.failureHandler(context-> {
        		WTradeLogger.print("WTradeServer", String.format(" FAIL HANDLER "));
        		JsonObject o = new JsonObject();
    	    	o.put("mvResult", -1);
    	    	context.response().putHeader("content-type", "application/json").end(o.toString());
        	});
        //Redirect /login requests to root
        router.route("/login").handler(context -> {
                    HttpServerResponse res = context.response();
                    res.putHeader("location", "/");
                    res.setStatusCode(HttpStatus.SC_MOVED_TEMPORARILY);
                    res.end();
                }
        );
        router.post("/checkSession.action").handler(context -> {
	    	Session session = context.session();
	    	String sID = "";
	    	try {
	    		sID = session.id();
	    	}
	    	finally {
	    		System.out.println("blabla  SessionID = " + sID);
	    	}
	    	
	    	
	    	JsonObject o = new JsonObject();
	    	o.put("mvResult", "");
	    	o.put("mvResult_2", "");
	    	o.put("success", false);
	    	
	    	context.response().putHeader("content-type", "application/json").end(o.toString());
	    });
        //////////////////////////////////////////////////////////////////

        //Start a static server for React frontend
        router.route().handler(RoutingContext -> {
        	Session session = RoutingContext.session();
	    	String lvClientID = "";
	    	try {
	    		lvClientID = session.get("clientid");
	    	}
	    	finally {
	    		
	    	}
	    	
            StaticHandler.create("webroot").setIndexPage("index.html").handle(RoutingContext);
        });

        //set up Atmosphere listener on /ITradePushServer/StockInfo/:ClientID
        VertxAtmosphere.Builder b = new VertxAtmosphere.Builder();
        b.resource(StockInfoSocket.class).httpServer(httpServer)
                .url("/ITradePushServer/StockInfo/:ClientID")
                .webroot("verticles/main/webroot/")
                .initParam(ApplicationConfig.WEBSOCKET_CONTENT_TYPE, "application/json")
                .vertx(this.vertx)
                .build();

        /*router.mountSubRouter("/StockInfo", StockInfoRouter());*/
        httpServer.requestHandler(router::accept).listen(8089);
    }

    private void onAuth(RoutingContext pvRouteCtx) {

    }

    private void onActionRequest(RoutingContext pvRouteCtx) {
        WTradeRequest newReq = new WTradeRequest(pvRouteCtx);
        String lvWorkerName = null;
        //Check if action exist
        ActionConfig lvActionConfig = this.actionMap.get(newReq.getMvAction());
        if (lvActionConfig == null) {
            WTradeLogger.print("WTradeServer", String.format("Request from user [%s] had invalid action name: %s", newReq.getMvClientID(), newReq.getMvAction()));
            
            JsonObject o = new JsonObject();
	    	o.put("mvResult", -1);
	    	pvRouteCtx.response().putHeader("content-type", "application/json").end(o.toString());
	    	
            return;
        }
        lvWorkerName = lvActionConfig.getWorker();
        if (lvWorkerName == null) {
            WTradeLogger.print("WTradeServer", String.format("Action [%s] has no worker name", newReq.getMvAction()));
            return;
        }

        WTradeLogger.print("WTradeServer", String.format("Receive new action [%s] from [%s] request: %s", newReq.getMvAction(), newReq.getMvClientID(), newReq.getMvJson().toString()));

        eventBus.send(lvWorkerName, newReq, res -> {
            if (res.succeeded()) {
                WTradeLogger.print(String.format("WTradeServer | %s", newReq.getMvClientID()), "Receive reply: " + res.result().body());
            } else {
                WTradeLogger.print(String.format("WTradeServer | %s", newReq.getMvClientID()), "Sending failed");
            }
        });
    }

    private void onWatchListRequest(RoutingContext rtCtx) {
        //Get clientID from the request
        String lvClientID = rtCtx.request().getParam("clientID");

        JsonObject lvMessage = new JsonObject();
        lvMessage.put(IEBMessage.MESSAGE_TYPE, "GetStockWatchList");
        lvMessage.put(IEBMessage.CLIENT_ID, lvClientID);
        //Send clientID to TPWorker
        eventBus.send("TPMessage.data", lvMessage, res2 -> {
            if (res2.succeeded()) {
                WTradeLogger.print(String.format("WTradeServer | %s", lvClientID), "Receive reply: " + res2.result().body());
                Log.println("[StockInfoSocket] Send TPMessage.data To TP Verticle Successfully.Value: " + lvMessage.getString(IEBMessage.CLIENT_ID), Log.ACCESS_LOG);
            } else {
                WTradeLogger.print(String.format("WTradeServer | %s", lvClientID), "Sending failed: " + res2.result().body());
                Log.println("[StockInfoSocket] Send TPMessage.data to TP Verticle Failled. Cause: " + res2.cause(), Log.ERROR_LOG);
            }
        });
//        eventBus.publish("TPMessage.data", new EBMessage("GetStockWatchList", "0", pvClientID));
        rtCtx.response().putHeader("content-type", "text/html").end("Register Socket Session Success!");
        WTradeLogger.print(this.getClass().getName(), "End of response");
    }
}