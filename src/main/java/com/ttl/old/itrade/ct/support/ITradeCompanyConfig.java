/*package com.itrade.ct.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import com.itrade.aastock.EDataFeedSource;
import com.itrade.base.dashboard.DashboardConfig;
import com.itrade.base.dashboard.ITradeDashboardFeedFactory;
import com.itrade.base.dashboard.feed.IDashboardFeed;
import com.itrade.base.data.DataPadding;
import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.biz.model.EMarket;
import com.itrade.biz.model.ITradeMarket;
import com.itrade.configuration.diversity.AppConfiguration.EResourceType;
import com.itrade.configuration.diversity.BaseAppDiversityConfiguration;
import com.itrade.configuration.model.pattern.ClientPatternEntry;
import com.itrade.configuration.model.pattern.StockCodePatternEntry;
import com.itrade.configuration.pattern.PatternContext.EPatternType;
import com.itrade.configuration.pattern.PatternEntry;
import com.itrade.text.format.datetime.DateFormatter;
import com.itrade.text.format.number.NumberFormatter;
import com.itrade.util.CollectionUtil;
import com.itrade.util.StringUtils;
import com.systekit.winvest.hks.util.Utils;

*//**
 * 
 * @author jay.wince
 * @since 2014-04-08
 *//*
public class ITradeCompanyConfig implements Serializable{
	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 20140408L;
	private String 			   mvCompany;
	
	private com.itrade.configuration.diversity.DiversityFacade mvNewConfiguration;
	
	
//  ------------------Models extracting the data from configuration.
	private DashboardConfig mvDashboardConfig;
	
	public ITradeCompanyConfig(final String company) {
		this.mvCompany = company;
		this.mvNewConfiguration = new com.itrade.configuration.diversity.DiversityFacade(company);
		populate();
	}
// -------------------------------------------------------------Private method
	private void populate(){
// ----1.DashboardConfig
	    mvDashboardConfig = new DashboardConfig(this.mvNewConfiguration);
	    mvDashboardConfig.setMvFeedPath(dashboardSettings("feed-path"));
	    mvDashboardConfig.setMvLocalStorage(fileSettingsInPreference().get("local-storage"));
	    mvDashboardConfig.setMvFileNameTpl(fileSettingsInPreference().get("template"));
	}
// -------------------------------------------------------------Public method

//---------------Getter 
	public DashboardConfig getDashboardConfig(){
	    return this.mvDashboardConfig;
	}
	public String getCompanyCode(){
		return this.mvCompany;
	}
 1.Lang REF Start 
	public int defaultLangcode() {
		return ITradeLang.mappingLangCode(getLanguage("default-text"), this.mvCompany);
	}
	
	public String defaultLangText(){
		return getLanguage("default-text");
	}
	
	*//**
	 * Pending to handle exception.
	 * @author jay.wince
	 * @param lang
	 * @return
	 *//*
	public int langCheck(final String lang) {
		String lvLang = lang;
		if (StringUtils.isNullStr(lang)) {
			lvLang = defaultLangcode()+"";
		}
		return ITradeLang.langCheck(lvLang, this.mvCompany);
	}
	
	public int fromLocalToLangCode(Locale locale){
		return ITradeLang.fromLocalToLangCode(locale, this.mvCompany);
	}
	
	public final String mappingLangText(final int langCode) {
		return ITradeLang.mappingLangText(langCode, this.mvCompany);
	}

	public final int mappingLangCode(final String langText) {
		return ITradeLang.mappingLangCode(langText, this.mvCompany);
	}
	
    1.1:TP REF Start
	public String getLanguageSupported(){
		return getLanguage("supported");
	}
	
    *//**
     * 
     * @param lang : 0,1 or "en_US"
     * @return
     *//*
	public Locale toLocal(final String lang){
		return ITradeLang.toLocal(lang,this.mvCompany);
	}
 1.Lang REF Ending 

 2.Market REF Start 

// BEGIN TASK #:TTL-GZ-clover_he-00090 2014-02-11 [ITrade5] get market support according company code.[ITRADEFIVE-85]
	public EMarket[] marketSupported() {
		return ITradeMarket.marketSupported(this.mvCompany);
	}

// END TASK #:TTL-GZ-clover_he-00090 2014-02-11 [ITrade5] get market support according company code.[ITRADEFIVE-85]
//BEGIN TASK #:TTL-GZ-PENGJM-00282 20141013[ITradeR5]Add MAMK supported(ITRADEFIVE-299)
	public String[] selectMarketSupported() {
		String[] lvStringArray = StringUtils.splitToArray(getMarket("select-market-supported"), ",") ;
		return lvStringArray;
	}
//END TASK #:TTL-GZ-PENGJM-00282 20141013[ITradeR5]Add MAMK supported(ITRADEFIVE-299)
// BEGIN TASK #:TTL-GZ-clover_he-00090.1 2014-02-13 [ITrade5] get default market according company code.[ITRADEFIVE-85]
	public EMarket defaultMarket() {
		EMarket lvdefaultMarket = null;
		String lvMarketID = getMarket("default");
		EMarket[] lvEmakets = marketSupported();
		for (EMarket eMarket : lvEmakets) {
			if (eMarket.marketID().equals(lvMarketID)) {
				lvdefaultMarket = eMarket;
				break;
			}
		}
		return lvdefaultMarket;
	}

// END TASK #:TTL-GZ-clover_he-00090.1 2014-02-13 [ITrade5] get default market according company code.[ITRADEFIVE-85]
	
// BEGIN TASK #:TTL-GZ-clover_he-00096.1 2014-03-20 [ITrade5] get enable-multi-market information after login.[ITRADEFIVE-78]
	*//**
	 * Case 1:request-response chain.
	 *//*
	public boolean isMultipleMarketEnable() {
		return Boolean.TRUE.toString().equalsIgnoreCase(getMarket("multiple-market-enable"));
	}

	public EMarket marketOffer(final String pMarketID) {
		EMarket[] markets = ITradeMarket.marketSupported(this.mvCompany);
		for (EMarket market : markets) {
			if (market.marketID().equals(pMarketID)) {
				return market;
			}
		}
		return EMarket.HKEX;
	}
 2.Market REF Ending 	
	
// END TASK #:TTL-GZ-clover_he-00096.1 2014-03-20 [ITrade5] get enable-multi-market information after login.[ITRADEFIVE-78]

	*//**
	 * Get the <b>mapping configuration between form value and FO value</b> for
	 * order enquiry in a transparent manner.
	 * 
	 * @author jay.wince
	 * @since 2014-03-12
	 * @return
	 *//*
	public Map<String, String> orderStatusMappingBtwFormAndFO() {
		return getOrderFoMapping();
	}
    public String[] orderJournalStatusFormValue(){
    	return StringUtils.splitToArray(getOrderFormMapping("order-status-form-mapping-value"),",");
    }
    public String[] orderHistoryStatusFormValue(){
    	return StringUtils.splitToArray(getHistoryFormMapping("order-status-form-mapping-value"), ",");
    }
	*//**
	 * Get the <b>order status used for FO by mapping the order status from form
	 * value</b> for order enquiry in a transparent manner.
	 * 
	 * @author jay.wince
	 * @since 2014-03-12
	 * @param pFormOrderStatus
	 * @return
	 *//*
	public String getFOOrderStatusBy(final String pFormOrderStatus) {
		return orderStatusMappingBtwFormAndFO().get(pFormOrderStatus);
	}

	*//**
	 * Get the <b>order status configuration that is allowed to modify</b> in a
	 * transparent manner.
	 * 
	 * @author jay.wince
	 * @since 2014-03-12
	 * @return
	 *//*
	public String orderStatusForAllowedToModify() {
		return getModifyOrder("order-status");//junming on 20141208:Change node 
	}

	*//**
	 * Get the <b>order status configuration that is allowed to cancel</b> in a
	 * transparent manner.
	 * 
	 * @author jay.wince
	 * @since 2014-03-12
	 * @return
	 *//*
	public String orderStatusForAllowedToCancel() {
		return getCancelOrder("order-status");//junming on 20141208:Change node 
	}


Tabular REF Start
	Group REF Start
	public boolean groupEnable() {
		return ContextConfiguration.groupEnable(this.mvCompany);
	}
	
	private boolean groupEnable(final String module) {
		return ContextConfiguration.groupEnable(this.mvCompany,module);
	}
	public boolean groupEnableForPortfolios() {
		return groupEnable("portfolios");
	}
	Group REF Ending
	
	*//**
	 * Get the <b>tablic columns configuration</b> for order enquiry in a
	 * transparent manner.
	 * 
	 * @author jay.wince
	 * @since 2014-03-12
	 *//*
	public List<String> tablicColumnsConfigurationForOrderEnquiry() {
		return getOrderCloumns();
	}
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	*//**
	 * Get the <b>tablic setting-raw-columns configuration</b> for order enquiry in a
	 * transparent manner.
	 * 
	 * @author kelly.kuang
	 * @since 2015-11-20
	 *//*
	public List<String> tablicSettingColumnsConfigurationForOrderEnquiry() {
		return getOrdeSettingrRawCloumns();
	}
	*//**
	 * Get the <b>tablic default-raw-columns configuration</b> for order enquiry in a
	 * transparent manner.
	 * 
	 * @author kelly.kuang
	 * @since 2015-11-20
	 *//*
	public List<String> tablicDefaultColumnsConfigurationForOrderEnquiry() {
		return getOrderDefaultRawCloumns();
	}
//END TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	
	public int tablePageSizeForPortfolioSummary(final String pDwidth) {
		return tablePageSize(pDwidth, "portfolios");
	}
	
	public List<String> getAccountBalanceTableicColumns() {
		return getAccountBalanceCloumns();
	}
	public List<String> portfolioTableicColumns() {
		return ContextConfiguration.portfolioTableicColumns(this.mvCompany, null);
	}
	
	*//**
	 * Get the common <b>tablic page size configuration</b> for order enquiry in
	 * a transparent manner.
	 * 
	 * @author jay.wince
	 * @since 2014-03-12
	 * @param pDwidth
	 * @return
	 *//*
	public int tablePageSize(final String pDwidth) {
		return ContextConfiguration.tablePageSize(
				ITradeSessionManager.getCurrentCompany(), pDwidth, "10");
	}
	
	private int tablePageSize(final String pDwidth, final String module) {
		return ContextConfiguration.tablePageSize(this.mvCompany,pDwidth, module, "10");
	}
	
Tabular REF Ending

Value REF Start
	
// BEGIN TASK #:TTL-GZ-clover_he-00096.2 2014-04-02 [ITrade5] add configuration empty-string for "&nbsp;".[ITRADEFIVE-78]
	*//**
	 * Case 1:request-response chain.
	 *//*
	public final String getEmptyString() {
		return getDefautValue("empty-string");
	}

// END TASK #:TTL-GZ-clover_he-00096.2 2014-04-02 [ITrade5] add configuration empty-string for "&nbsp;".[ITRADEFIVE-78]

// BEGIN TASK #:TTL-GZ-clover_he-00096.2 2014-04-02 [ITrade5] add configuration hypen-string for "-".[ITRADEFIVE-78]
	*//**
	 * Case 1:request-response chain.
	 *//*
	public final String getHypenString() {
		return getDefautValue("hypen-string");
	}

// END TASK #:TTL-GZ-clover_he-00096.2 2014-04-02 [ITrade5] add configuration hypen-string for "-".[ITRADEFIVE-78]
	
//BEGIN TASK #:TTL-GZ-Busy-00011 20140430 [ITrade5] add configuration max-lot-size for 3000.[ITRADEFIVE-78]	
	public final String getMaxLotSize(){
		return getDefautValue("max-lot-size");
	}
	
//END TASK #:TTL-GZ-Busy-00011 20140430 [ITrade5] add configuration max-lot-size for 3000.[ITRADEFIVE-78]	
	
//BEGIN TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] Add regex for order detail fees.[ITRADEFIVE-152]
	public final String getCommissionRegex(){
		return ContextConfiguration.getCommissionRegex(this.mvCompany); 
	}
	public final String getStampDutyRegex(){
		return ContextConfiguration.getStampDutyRegex(this.mvCompany);
	}
	public final String getTxnLevyRegex(){
		return ContextConfiguration.getTxnLevyRegex(this.mvCompany);
	}
	public final String getHandlingChargeRegex(){
		return ContextConfiguration.getHandlingChargeRegex(this.mvCompany);
	}
	public final String getTradingFeeRegex(){
		return ContextConfiguration.getTradingFeeRegex(this.mvCompany);	 
	}
//END TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] Add regex for order detail fees.[ITRADEFIVE-152]
	 
//BEGIN TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] Code optimization for show order detail.[ITRADEFIVE-152]
	public final boolean getIncludeFeeCalculate(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getOrderDetail("include-fee-calculate"));
	}
	public final String getFeeDetailsEnableStatus(){
		return getOrderDetail("fee-details-enable-status");	 
	}
	public final String getUseUpdatedOrderDetails(){
		return getOrderDetail("use-updated-order-details");	 
	}
	public final String getDisplayOrderAction(){
		return getOrderDetail("display-order-action");	 
	}
	public final String getFilterDisplayRejectedTrade(){
		return getOrderDetail("filter-display-rejected-trade");	 
	}
	public final String getGroupTradeDetails(){
		return getOrderDetail("group-trade-details");	 
	}
//END TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] Code optimization for show order detail.[ITRADEFIVE-152]	 
//BEGIN TASK #:TTL-GZ-Busy-00011 20140428 [ITrade5] Add configuration for good till date of place order module.[ITRADEFIVE-78]
	public final String getMinimumOrderPrice(){
		return getEnterOrderValue("minimum-order-price");
	}
	public final boolean getAllowOddPrice(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getEnterOrderValue("allow-odd-lot"));
	}
	public final String getResumitOrderRiskID(){
		return getEnterOrderValue("resumit-order-risk-id");
	}
	public final String getActionOrderTypeID( ){
		return getEnterOrderValue("action-order-type-id");
	}
	public final String getDueOrderTypeID( ){
		return getEnterOrderValue("due-order-type-id");
	}
	public final String getGenenalOrderTypeID( ){
		return getEnterOrderValue("genenal-order-type-id");
	}
//BEGIN TASK # :TTL-GZ-kelly.kuang-00010 20151216[ITradeR5]Support Order resubmit.(ITRADEFIVE-720)
	public final boolean getAllowAutoResubmit(){
		return Boolean.TRUE.toString().equals(getEnterOrderValue("allow-auto-resubmit"));
	}
//END TASK # :TTL-GZ-kelly.kuang-00010 20151216[ITradeR5]Support Order resubmit.(ITRADEFIVE-720)
//END TASK #:TTL-GZ-Busy-00011 20140428 [ITrade5] Add configuration for good till date of place order module.[ITRADEFIVE-78]

//BEGIN TASK #:TTL-GZ-Busy-00011 20140429[ITrade5] Add configuration for Login module[ITRADEFIVE-78]
	public final boolean getCaptcha(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getLogin("show-captcha"));
	}
	public final String getCaptchaType() {
	    return getLogin("captcha-type");
	}
	public final boolean getCheckWWWEnable(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getLogin("check-www-enable"));
	}
	public final boolean getCheckPasswordExpiryDate(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getLogin("check-password-Expiry-Date"));
	}
	public final int getShowLogoutMode(){
		return Integer.valueOf(getLoginOptions("show-logout-mode"));
	}
	public final String getInvalidSessionUrl(){
		return getLogin("invalid-session-url");
	}
//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00026 20150123[ITradeR5]Config the login url.(ITRADER5-479)
	public final String getLoginUrl(){
		return getLogin("login-url");
	}
//END   TASK #:TTL-GZ-CANYONG.LIN-00026 20150123[ITradeR5]Config the login url.(ITRADER5-479)
	public final boolean getUserNameIgnoreCase(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getLogin("user-name-ignore-Case"));
	}
	public final boolean getLoginAfterTips(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getLogin("login-after-tips"));
	}
//END TASK #:TTL-GZ-Busy-00011 20140429[ITrade5] Add configuration for Login module[ITRADEFIVE-78]
//BEGIN TASK #:TTL-GZ-PENGJM-00341 20150202[ITradeR5]Use user name login itrade(ITRADEFIVE-481)
	public final String getUseUsernameLogin(){
	    return getLogin("username-login-only");
	}
//END TASK #:TTL-GZ-PENGJM-00341 20150202[ITradeR5]Use user name login itrade(ITRADEFIVE-481)
	public final boolean getEnableRegisterUserName() {
	    return Boolean.TRUE.toString().equalsIgnoreCase(getLogin("enable-register-username"));
	}
	
//BEGIN TASK #:TTL-GZ-Busy-00011 20140429[ITrade5] Add configuration for change password module[ITRADEFIVE-78]
	public final boolean getPasswordLettersAndDigits(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getChangePassword("password-Comprise-letters-digits"));
	}
	public final boolean getPasswordUpperAndLower(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getChangePassword("password-comprise-uppercase-lowercase"));
	}
	public final boolean getPasswordNotClientID(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getChangePassword("password-comprise-not-clientId")) ;
	}
	public final boolean getPasswordNotCurrentPassword(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getChangePassword("password-comprise-not-current-password"));
	}
	public final int getPasswordMinLength(){
		String lvMinPasswordLength = (String) (!StringUtils.isNullStr(getChangePassword("password-min-length"))?getChangePassword("password-min-length"):6);
		return Integer.valueOf(lvMinPasswordLength);
	}
	public final int getPasswordMaxLength(){
		String lvMaxPasswordLength = (String) (!StringUtils.isNullStr(getChangePassword("password-max-length"))?getChangePassword("password-max-length"):8);
		return Integer.valueOf(lvMaxPasswordLength);
	}
	
	public final String getPatternForPasswordValidation(){
		return getChangePassword("pattern-for-password-validation");
	}
//END TASK #:TTL-GZ-Busy-00011 20140429[ITrade5] Add configuration for change password module[ITRADEFIVE-78]

//BEGIN TASK #:TTL-GZ-clover_he-00104.2 20140430[ITrade5] add configuration default channel.[ITRADEFIVE-152]
	public final String getDefaultChannelID(){
		return ContextConfiguration.getDefaultChannelID(this.mvCompany);
	}
//END TASK #:TTL-GZ-clover_he-00104.2 20140430[ITrade5] add configuration default channel.[ITRADEFIVE-152]

//BEGIN TASK #:TTL-GZ-clover_he-00104.2 20140430[ITrade5] add configuration for order enquiry module.[ITRADEFIVE-152]	 
	 public final boolean getRejectReasonAndRemark(){
		 return ContextConfiguration.getRejectReasonAndRemark(this.mvCompany);
	 }
	 public final boolean getShowOrderDetail(){
		 return ContextConfiguration.getShowOrderDetail(this.mvCompany);
	 }
//END TASK #:TTL-GZ-clover_he-00104.2 20140430[ITrade5] add configuration for order enquiry module.[ITRADEFIVE-152]
	
//BEGIN TASK #:TTL-GZ-Busy-00011 20140429 [ITrade5]Add Configuration for portfolio module[ITRADEFIVE-78]
	public final String getBuySellHeader(){
		return getSummaryValue("buy-sell-header");
	}
	public final boolean getCurrencyValueEnable(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getSummaryValue("show-currency-value"));
	}
	public final boolean getShowPortfilioMarket(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getSummaryValue("show-portfolio-market"));
	}
	public final String getRedirectEnterOrderUrl(){
		return getSummaryValue("enter-order-frame-url");
	}
//END TASK #:TTL-GZ-Busy-00011 20140429 [ITrade5]Add Configuration for portfolio module[ITRADEFIVE-78]
	
//BEGIN TASK #:TTL-GZ-00011 20140430 [ITrade5]Add Configurable for order history[ITRADEFIVE-78]
	public final boolean getShowOrderGroupID(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getOrderHistoryValue("show-order-group-id"));
	}
	
	public final boolean getOrderUsingTradeDate(){
		return Boolean.TRUE.toString().equalsIgnoreCase(getOrderHistoryValue("order-using-trade-date"));
	}
//END TASK #:TTL-GZ-00011 20140430 [ITrade5]Add Configurable for order history[ITRADEFIVE-78]
//BEGIN TASK #:TTL-GZ-Busy-00011 20140430 [ITrade5]Add Configurable function for transaction History.[ITRADEFIVE-78]	
    public  final boolean getUsingTradeDate(){
    	return Boolean.TRUE.toString().equalsIgnoreCase(getTranHistoryValue("using-trade-date"));
	}
    public final String getDefaultGroupByFlag(){
    	return getTranHistoryValue("default-group-sort-flag");
    }
//END TASK #:TTL-GZ-Busy-00011 20140430 [ITrade5]Add Configurable function for transaction History.[ITRADEFIVE-78]	
Value REF Ending
	
Theme REF Start

	public int solutionForThemeException(){
		return Integer.parseInt(getThemeOptions("theme-exception"));
	}
	
	public String companyThemesSupported(){
		return getTheme("themes-supported");
	}
	
Theme REF Ending

	
Message REF Start
	public int strategyIfMessageNonExist(){
		return Integer.valueOf(getMessageOptions("if-message-non-exist"));
	}
Message REF Ending	

Password encrypt REF Start
	public boolean passwordEncrypt() {
		return Boolean.TRUE.toString().equalsIgnoreCase(getEncryptMethod("password-encrypt"));
	}
	
	public boolean useAppletPasswordEncrypt() {
		return Boolean.TRUE.toString().equalsIgnoreCase(getEncryptMethod("use-applet-encryt-login-password"));
	}
	
	public String getExternalPasswordEncryptClass() {
		return getEncryptMethod("external-password-encrypt-class");
	}
	
	public boolean encryptMethodByCompany() {
		return Boolean.TRUE.toString().equalsIgnoreCase(getEncryptMethod("encrypt-method-by-company"));
	}
	
	public boolean hashEncrypt() {
		return Boolean.TRUE.toString().equalsIgnoreCase(getEncryptMethod("hash-encrypt"));
	}
	
	public String getPasswordAlgorithm() {
		return getEncryptMethod("password-algorithm");
	}
	
Password encrypt REF Ending
	public String formatClientID(Number clientID,Locale locale){
		ClientPatternEntry entry = (ClientPatternEntry)this.mvNewConfiguration.getPatternEntry(EPatternType.CLIENTID);
 		NumberFormatter numberFormatter = new NumberFormatter();
		numberFormatter.setPattern(entry.getPattern());
		String replacement = entry.getGroupingSeperator();	
		return numberFormatter.print(clientID, locale).toString().replaceAll(",", replacement);
	}
Text format Start
    public String formatClientID(String clientID,Locale locale){    	   
//BEGIN TASK#: TTL-GZ-BO.LI-00067.1 20141114[ITradeR5]Format the client ID(ITRADEFIVE-378)
    	*//**
    	 * Format --->  xx-xx-xx-xxxxxx
    	 *//*
//    	ClientPatternEntry entry = (ClientPatternEntry)this.mvNewConfiguration.getPatternEntry(EPatternType.CLIENTID);
//    	NumberFormatter numberFormatter = new NumberFormatter();
//		numberFormatter.setPattern(entry.getPattern());
//		String replacement = entry.getGroupingSeperator();	
//		return numberFormatter.print(clientID, locale).toString().replaceAll(",", replacement);
    	
    	ClientPatternEntry entry = (ClientPatternEntry)this.mvNewConfiguration.getPatternEntry(EPatternType.CLIENTID);
		String replacement = entry.getGroupingSeperator();
    	String lvClientIdValue = "";
    	for(int k = 0; k < clientID.length(); k++) {
			if(k==2||k==4||k==6) {
				lvClientIdValue+=(replacement + clientID.charAt(k));
			} else {
				lvClientIdValue+=clientID.charAt(k);
			}
		 }
		return lvClientIdValue;
//END TASK#: TTL-GZ-BO.LI-00067.1 20141114[ITradeR5]Format the client ID(ITRADEFIVE-378)
	}
    
    public String formatStockCode(String marketID,Number stockCode,Locale locale){
    	StockCodePatternEntry entry = (StockCodePatternEntry) this.mvNewConfiguration.getPatternEntry(EPatternType.STOCKCODE);
    	NumberFormatter codeFormatter = new NumberFormatter(entry.getPattern(marketID));
    	return codeFormatter.print(stockCode, locale);
    }
    
    public String formatPrice(String stockPrice,String dollarSign,Locale locale){
    	String lvFormaterNumber = "";
    	if(!StringUtils.isNullStr(stockPrice)){
    		PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.STOCKPRICE);
    		NumberFormatter priceFormatter = new NumberFormatter(entry.getPattern());
    		lvFormaterNumber = dollarSign+priceFormatter.print(Double.parseDouble(stockPrice), locale);
    	}
    	return lvFormaterNumber;
    }
    
    public String formatAvgPrice(String stockPrice,String dollarSign,Locale locale){
    	String lvFormaterNumber = "";
    	if(!StringUtils.isNullStr(stockPrice)){
    		PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.STOCKAVGPRICE);
    		NumberFormatter priceFormatter = new NumberFormatter(entry.getPattern());
    		lvFormaterNumber = dollarSign+priceFormatter.print(Double.parseDouble(stockPrice), locale);
    	}
    	return lvFormaterNumber;
    }
    
    public String formatStockQuantity(String stockPrice,String dollarSign,Locale locale){
    	String lvFormaterNumber = "";
    	if(!StringUtils.isNullStr(stockPrice)){
    		PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.STOCKQUANTITY);
    		NumberFormatter priceFormatter = new NumberFormatter(entry.getPattern());
    		lvFormaterNumber = dollarSign+priceFormatter.print(Double.parseDouble(stockPrice), locale);
    	}
    	return lvFormaterNumber;
    }
    public String formatPctChange(String stockPrice,String dollarSign,Locale locale){
    	String lvFormaterNumber = "";
    	if(!StringUtils.isNullStr(stockPrice)){
    		PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.PCTCHANGE);
    		NumberFormatter priceFormatter = new NumberFormatter(entry.getPattern());
    		lvFormaterNumber = dollarSign+priceFormatter.print(Double.parseDouble(stockPrice), locale);
    	}
    	return lvFormaterNumber;
    }
    public String formatCashValue(String stockPrice,String dollarSign,Locale locale){
    	String lvFormaterNumber = "";
    	if(!StringUtils.isNullStr(stockPrice)){
    		PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.CASHVALUE);
    		NumberFormatter priceFormatter = new NumberFormatter(entry.getPattern());
    		lvFormaterNumber = dollarSign+priceFormatter.print(Double.parseDouble(stockPrice), locale);
    	}
    	return lvFormaterNumber;
    }
    public String formatCashValue(String stockPrice,String defaultValue,String dollarSign,Locale locale){
        String lvFormaterNumber = (StringUtils.isNullStr(defaultValue) ? "" : defaultValue);
        if(!StringUtils.isNullStr(stockPrice)){
            PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.CASHVALUE);
            NumberFormatter priceFormatter = new NumberFormatter(entry.getPattern());
            lvFormaterNumber = dollarSign+priceFormatter.print(Double.parseDouble(stockPrice), locale);
        }
        return lvFormaterNumber;
    }
    yyyy-MM-dd
    public String formatDate(Date date,Locale locale){
    	PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.DATE);
		DateFormatter formatDate = new DateFormatter(entry.getPattern());
		return formatDate.print(date, locale);
    	
    }
    yyyy-MM-dd HH:mm:ss
    public String formatDateTime(Date date,Locale locale){
    	PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.DATETIME);
		DateFormatter formatDate = new DateFormatter(entry.getPattern());
		return formatDate.print(date, locale);
    }
    HH:mm:ss
    public String formatTime(Date date,Locale locale){
    	PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.TIME);
    	DateFormatter formatDate = new DateFormatter(entry.getPattern());
    	return formatDate.print(date, locale);
    }
    HH:mm
//BEGIN TASK #:TTL-GZ-BO.LI-00033 20140901[ITradeR5]News module time(ITRADEFIVE-248)
    public String formatHourMinute(Date date,Locale locale){
    	PatternEntry entry = this.mvNewConfiguration.getPatternEntry(EPatternType.HOURMINUTE);
    	DateFormatter formatDate = new DateFormatter(entry.getPattern());
    	return formatDate.print(date, locale);
    }
//END TASK #:TTL-GZ-BO.LI-00033 20140901[ITradeR5]News module time (ITRADEFIVE-248)
Text format Ending	
    
//BEGIN TASK#: TTL-GZ-Busy-00013.2 20140430 [ITrade5]Implementation  for the whole project which use new analytic method for Configuration.[ITRADEFIVE-160]
    
new analysis of configuration Start
    public String getDashBoard(String nodeName){
    	return this.mvNewConfiguration.dashboard().get(nodeName);
    }
    
    public String getVersion(String nodeName){
    	return this.mvNewConfiguration.version().get(nodeName);
    }
    
    public String getCustomerService(String nodeName){
    	return this.mvNewConfiguration.customerService().get(nodeName);
    }
    
    public String getCustomerServicePhoneNumber(String nodeName){
    	return this.mvNewConfiguration.customerServicePhoneNumber().get(nodeName);
    }
    
    public String getMessageOptions(String nodeName){
    	return this.mvNewConfiguration.messageOptions().get(nodeName);
    }
    
    *//**
     * @author jay.wince
     * @since  20140725
     * @param nodeName
     * @return
     *//*
    public String getThemeOptions(String nodeName){
    	return this.mvNewConfiguration.themeOptions().get(nodeName);
    }
    
    public String getCTOptions(String nodeName){
    	return this.mvNewConfiguration.ctOptions().get(nodeName);
    }
    
	public String getCommon(String nodeName){
    	return this.mvNewConfiguration.common().get(nodeName);
    }
    
    private String getMessage(String nodeName){
    	return this.mvConfiguraton.message().get(nodeName);
    }
    
    private String getLanguage(String nodeName){
    	return this.mvNewConfiguration.language().get(nodeName);
    }
    
    private String getTheme(String nodeName){
    	return this.mvNewConfiguration.themePadding().get(nodeName);
    }
    public HashMapDataPadding<String> themePadding(){
    	return this.mvNewConfiguration.themePadding();
    }
    
    public String getCurrency(String nodeName){
    	return this.mvNewConfiguration.currency().get(nodeName);
    }
    
    public String getMarket(String nodeName){
    	return this.mvNewConfiguration.market().get(nodeName);
    }
    
    public String getRegex(String nodeName){
    	return this.mvNewConfiguration.regex().get(nodeName);
    }
    
    public String getChannelID(String nodeName){
    	return this.mvNewConfiguration.channel().get(nodeName);
    }
    
  
    
    core module
    public boolean getGroupSupported(String nodeName){
    	return "true".equalsIgnoreCase(getTeblicGroupSupported(null,nodeName));
    }
    //junming on 20141112:change to public
    public String getTeblicGroupSupported(String pModule,String nodeName){
    	return this.mvNewConfiguration.groupSupported(pModule).get(nodeName);
    }
    
    public int getPageSize(String pDwidth){
    	return getTablicPageSize(null,pDwidth);
    }
    
    public int getTablicPageSize(String pModule,String pDwidth){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.pagesize(pModule);
    	return Integer.parseInt(dataPadding.get(pDwidth));
    }
    
	private String getDefautValue(String nodeName){
    	return this.mvNewConfiguration.defaultValue().get(nodeName);
    }
    
    //1.place order ref.
    public String getDisplayMode(String nodeName){
    	return this.mvNewConfiguration.displayMode(nodeName);
    }
    
    public String getGoodTillDate(String nodeName){
    	return this.mvNewConfiguration.goodTillDate().get(nodeName);
    }
//BEGIN TASK #:TTL-GZ-PENGJM-00286 20141017[ITradeR5]Control quote enable configuration(ITRADEFIVE-312)    
    public boolean getPlaceOrderQuoteEnable(){
    	return Boolean.TRUE.toString().equals(this.mvNewConfiguration.placeOrderQuoteEnable().get("quote-enable"));
    }
//END TASK #:TTL-GZ-PENGJM-00286 20141017[ITradeR5]Control quote enable configuration(ITRADEFIVE-312)
    private String getEnterOrderValue(String nodeName){
    	return this.mvNewConfiguration.enterOrderValue().get(nodeName);
    }
    
    private String getModifyOrder(String nodeName){
    	return this.mvNewConfiguration.allowModifyOrder().get(nodeName);//junming on 20141208:Change node 
    }
    
    private String getCancelOrder(String nodeName){
    	return this.mvNewConfiguration.allowCancelOrder().get(nodeName);//junming on 20141208:Change node 
    }
    
    private Map<String,String> getOrderFoMapping(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.orderMapping();
    	String[] lvStatusFormArray = StringUtils.splitToArray(dataPadding.get("order-status-form-mapping-value"), ",");
		String[] lvStatusFoArray = StringUtils.splitToArray(dataPadding.get("order-status-fo-mapping-value"), "/");
		Map<String,String> lvStatusMap = new HashMap<String,String>();
		for(int i=0;i<lvStatusFormArray.length;i++){
			lvStatusMap.put(lvStatusFormArray[i], lvStatusFoArray[i]);
		}	
    	return lvStatusMap;
    }
    
    private String getOrderFormMapping(String nodeName){
    	return this.mvNewConfiguration.orderMapping().get(nodeName);
    }
    
    public String getRejectAndRemark(){
    	return this.mvNewConfiguration.reject();
    }
    
  //3.history
    //3.1 order history
    public List<String> getOrderhistoryCloumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.orderhistoryCloumns();
    	@SuppressWarnings("rawtypes")
		Iterator lvOrderHistorySet = dataPadding.original().keySet().iterator();
		String lvOverrallSettings = "";
		String lvExcludedSettings= "";
		while(lvOrderHistorySet.hasNext()){
			String lvOrderHistory = lvOrderHistorySet.next().toString();
			if(lvOrderHistory.equals("overall")){
				lvOverrallSettings = dataPadding.get(lvOrderHistory);
			}else if(lvOrderHistory.equals("exclude")){
				lvExcludedSettings = dataPadding.get(lvOrderHistory);
			}
		}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
    public String getOrderHistoryFoMapping(String nodeName){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.historyMapping();
    	String[] lvStatusFormArray = StringUtils.splitToArray(dataPadding.get("order-status-form-mapping-value"), ",");
		String[] lvStatusFoArray = StringUtils.splitToArray(dataPadding.get("order-status-fo-mapping-value"), "/");
		Map<String,String> lvStatusMap = new HashMap<String,String>();
		for(int i=0;i<lvStatusFormArray.length;i++){
			lvStatusMap.put(lvStatusFormArray[i], lvStatusFoArray[i]);
		}	
    	return lvStatusMap.get(nodeName);
    }
    
    private String getHistoryFormMapping(String nodeName){
    	return this.mvNewConfiguration.historyMapping().get(nodeName);
    }
    
    private String getOrderHistoryValue(String nodeName){
    	return this.mvNewConfiguration.historyValue().get(nodeName);
    }
  //3.2 transaction history
    public List<String> getTranhistoryColumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.tranhistoryCloumns();
    	@SuppressWarnings("rawtypes")
		Iterator lvTranHistorySet = dataPadding.original().keySet().iterator();
		String lvOverrallSettings = "";
		String lvExcludedSettings= "";
		while(lvTranHistorySet.hasNext()){
			String lvTranHistory = lvTranHistorySet.next().toString();
			if(lvTranHistory.equals("overall")){
				lvOverrallSettings = dataPadding.get(lvTranHistory);
			}else if(lvTranHistory.equals("exclude")){
				lvExcludedSettings = dataPadding.get(lvTranHistory);
			}
		}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
    private String getTranHistoryValue(String nodeName){
    	return  this.mvNewConfiguration.tranhistoryValue().get(nodeName);
    }
    
    public List<String> getTypeSet(String nodeName){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.getTypeSet();
    	return CollectionUtil.excludedTableicColumnsToList(dataPadding.get(nodeName),null);
    }
    
  //4.portfolios
    private String getSummaryValue(String nodeName){
    	return this.mvNewConfiguration.summaryValue().get(nodeName);
    }
    
    public List<String> getSummaryCloumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.summaryCloumns();
    	@SuppressWarnings("rawtypes")
		Iterator lvSummaryKeySet = dataPadding.original().keySet().iterator();
		String lvOverrallSettings = "";
		String lvExcludedSettings= "";
		while(lvSummaryKeySet.hasNext()){
			String lvSummaryKey = lvSummaryKeySet.next().toString();
			if(lvSummaryKey.equals("overall")){
				lvOverrallSettings = dataPadding.get(lvSummaryKey);
			}else if(lvSummaryKey.equals("exclude")){
				lvExcludedSettings = dataPadding.get(lvSummaryKey);
			}
		}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
    private List<String> getAccountBalanceCloumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.accountBalanceCloumns();
    	@SuppressWarnings("rawtypes")
    	Iterator lvAccountBalanceSet = dataPadding.original().keySet().iterator();
    	String lvOverrallSettings = "";
    	String lvExcludedSettings= "";
    	while(lvAccountBalanceSet.hasNext()){
    		String lvAccountBalance = lvAccountBalanceSet.next().toString();
    		if(lvAccountBalance.equals("overall")){
    			lvOverrallSettings = dataPadding.get(lvAccountBalance);
    		}else if(lvAccountBalance.equals("exclude")){
    			lvExcludedSettings = dataPadding.get(lvAccountBalance);
    		}
    	}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
  //5.order detail
    private String getOrderDetail(String nodeName){
    	return this.mvNewConfiguration.orderdetail().get(nodeName);
    }
  //6.ipo
    public String IsDateFormat(){
    	return this.mvNewConfiguration.IPOModule().original().get("date-format");
    }
    
    public String getIPOpasswordConfirm(String nodeName){
    	return this.mvNewConfiguration.ipoPasswordConfirm().original().get("password-confirmatoin-ipo");
    }
    
    public List<String> getHottestCloumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.hottestCloumns();
    	@SuppressWarnings("rawtypes")
    	Iterator lvHottestCloumnsSet = dataPadding.original().keySet().iterator();
    	String lvOverrallSettings = "";
    	String lvExcludedSettings= "";
    	while(lvHottestCloumnsSet.hasNext()){
    		String lvHottestCloumns = lvHottestCloumnsSet.next().toString();
    		if(lvHottestCloumns.equals("overall")){
    			lvOverrallSettings = dataPadding.get(lvHottestCloumns);
    		}else if(lvHottestCloumns.equals("exclude")){
    			lvExcludedSettings = dataPadding.get(lvHottestCloumns);
    		}
    	}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
    public List<String> getStatusCloumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.statusCloumns();
    	@SuppressWarnings("rawtypes")
    	Iterator lvStatusCloumnsSet = dataPadding.original().keySet().iterator();
    	String lvOverrallSettings = "";
    	String lvExcludedSettings= "";
    	while(lvStatusCloumnsSet.hasNext()){
    		String lvStatusCloumns = lvStatusCloumnsSet.next().toString();
    		if(lvStatusCloumns.equals("overall")){
    			lvOverrallSettings = dataPadding.get(lvStatusCloumns);
    		}else if(lvStatusCloumns.equals("exclude")){
    			lvExcludedSettings = dataPadding.get(lvStatusCloumns);
    		}
    	}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
    public List<String> getListfocusCloumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.listFocusCloumns();
    	@SuppressWarnings("rawtypes")
    	Iterator lvListfocusCloumnsSet = dataPadding.original().keySet().iterator();
    	String lvOverrallSettings = "";
    	String lvExcludedSettings= "";
    	while(lvListfocusCloumnsSet.hasNext()){
    		String lvListfocusCloumns = lvListfocusCloumnsSet.next().toString();
    		if(lvListfocusCloumns.equals("overall")){
    			lvOverrallSettings = dataPadding.get(lvListfocusCloumns);
    		}else if(lvListfocusCloumns.equals("exclude")){
    			lvExcludedSettings = dataPadding.get(lvListfocusCloumns);
    		}
    	}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
  //7.CA
    public String getCApasswordConfirm(String nodeName){
    	return this.mvNewConfiguration.caPasswordConfirm().get("password-not-confirmation-CA");
    }
    
    public List<String> getCACloumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.caCloumns();
    	@SuppressWarnings("rawtypes")
    	Iterator lvCACloumnsKeySet = dataPadding.original().keySet().iterator();
    	String lvOverrallSettings = "";
    	String lvExcludedSettings= "";
    	while(lvCACloumnsKeySet.hasNext()){
    		String lvCACloumnsKey = lvCACloumnsKeySet.next().toString();
    		if(lvCACloumnsKey.equals("overall")){
    			lvOverrallSettings = dataPadding.get(lvCACloumnsKey);
    		}else if(lvCACloumnsKey.equals("exclude")){
    			lvExcludedSettings = dataPadding.get(lvCACloumnsKey);
    		}
    	}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
    //9.change password
    private String getChangePassword(String nodeName){
    	return this.mvNewConfiguration.changePassword().get(nodeName);
    }
new analysis of configuration Ending
//END TASK#: TTL-GZ-Busy-00013.2 20140430 [ITrade5]Implementation  for the whole project which use new analytic method for Configuration.[ITRADEFIVE-160]
// SSOL0-2 : Persisting depends on the option configured in base-diversity-config.xml     
    @Deprecated
    private EStoreOptions dashboardDBFeedOption(){
    	String optionValue  = this.mvNewConfiguration.storeOptions().get("based-on-DB");
    	if (StringUtils.isNullStr(optionValue)) {
			return EStoreOptions.BUFFERED;
		}
    	return EStoreOptions.valueOf(optionValue.toUpperCase());
    }
    @Deprecated
    private EStoreOptions dashboardFileFeedOption(){
    	String optionValue  = this.mvNewConfiguration.storeOptions().get("based-on-File");
    	if (StringUtils.isNullStr(optionValue)) {
			return EStoreOptions.INSTANT;
		}
    	return EStoreOptions.valueOf(optionValue.toUpperCase());
    }   
    @Deprecated
    public EStoreOptions dashboardFeedStoreOption(){
    	EStoreOptions option = EStoreOptions.BUFFERED;
    	if(ITradeAPI.userPreferenceOperator() instanceof OperateOnDatabase) {
    		option = dashboardDBFeedOption();
		}else if(ITradeAPI.userPreferenceOperator() instanceof OperateOnXMLFile) {
			option = dashboardFileFeedOption(); 
		}
    	return option;
    }
// ESOL0-2
    
BEGIN TASK #:TTL-GZ-Jay-00176 20140523[ITradeR5]Recursively handle support.    
    public String getAAStockClientID(){
    	return this.mvNewConfiguration.getAAStockSettings().get("AAStockClientID");
    }
    
    public String getAAStocksKey(){
    	return this.mvNewConfiguration.getAAStockSettings().get("AAStocksKey");
    }

	public String aastockSnapshotBroker() {
		return this.mvNewConfiguration.getAAStockSettings().get("broker");
	}
	
	public DataPadding<String> aastockSnapshotParameters(){
		return this.mvNewConfiguration.getAAStockSnapshotParas();
	}
	
	public String aastockSnapshotRetryCount(){
		return this.mvNewConfiguration.getAAStockSnapshotDataFeedOptions().get("retry-count");
	}
ENDING TASK #:TTL-GZ-Jay-00176 20140523[ITradeR5]Recursively handle support.

	private String getEncryptMethod(String nodeName){
		return EncryptMethod(null, nodeName);
	}
	
	private String EncryptMethod(String module,String nodeName){
		return this.mvNewConfiguration.passwordEncrypt(module).get(nodeName);
	}

 * @since 20140718
 * @author Jay
 * @description
 * Configuration parsing new API:  BL & UI settings separated
 * 

// START
	public HashMapDataPadding<String> fileSettingsInPreference(){		
		return this.mvNewConfiguration.fileSettingsInPreference();
	}
	
	public String dashboardSettings(String nodeName){
    	return this.mvNewConfiguration.dashboardPadding().get(nodeName);
    }
	*//**
     * @author canyong.lin
     * @since 20140807
     *//*
	public HashMapDataPadding<String> sessionPadding(){		
		return this.mvNewConfiguration.sessionPadding();
	}
	*//***
	 * 
	 * @param rt : If empty string is returned ,it indicates that use the base resource. 
	 * @return
	 *//*
	private String resourceRef(EResourceType rt){
		String lvRID = this.mvNewConfiguration.getResourceRef(rt);
		if (lvRID == null) {
			lvRID = this.mvCompany;
		}
		return lvRID;
	}
//BEGIN TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]Using new configuration fixed it(ITRADEFIVE-211)	
	//2.order-book.
    private List<String> getOrderCloumns(){
    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.orderColumns();
    	@SuppressWarnings("rawtypes")
		Iterator lvOrderKeySet = dataPadding.original().keySet().iterator();
		String lvOverrallSettings = "";
		String lvExcludedSettings= "";
		while(lvOrderKeySet.hasNext()){
			String lvOrderKey = lvOrderKeySet.next().toString();
			if(lvOrderKey.equals("overall")){
				lvOverrallSettings = dataPadding.get(lvOrderKey);
			}else if(lvOrderKey.equals("exclude")){
				lvExcludedSettings = dataPadding.get(lvOrderKey);
			}
		}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
    }
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
    private List<String> getOrdeSettingrRawCloumns(){
    	String dataPadding = this.mvNewConfiguration.orderSettingRawColumns().trim();
    	String[] lvColumns = dataPadding.split(",");
    	List<String> lvColumnsList = new ArrayList<String>();
    	for(String t : lvColumns){  
    		lvColumnsList.add(t);  
		}
    	return lvColumnsList;
    }
    private List<String> getOrderDefaultRawCloumns(){
    	String dataPadding = this.mvNewConfiguration.orderDefaultRawColumns().trim();
    	String[] lvColumns = dataPadding.split(",");
    	List<String> lvColumnsList = new ArrayList<String>();
    	for(String t : lvColumns){  
    		lvColumnsList.add(t);  
		}
    	return lvColumnsList;
    }
//END TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	
    public String pagingStyle(String pDWidth) {
		return pagingStyle(pDWidth,null);
	}
	public String pagingStyle(String pDWidth,String pModule) {
		HashMapDataPadding<String> lvDataPadding = this.mvNewConfiguration.pagingStyle(pModule);
		return lvDataPadding.get(pDWidth);
	}

	//aastock
	public DataPadding<String> aastockThumbnailStockchartImageView(){
		return this.mvNewConfiguration.getAAStockThumbnailStockchartImageView();
	}
		
	public String getLoginOptions(String nodeName){
    	return this.mvNewConfiguration.loginOptions().get(nodeName);
    }
	
	
	public String getStyleResourceID(){
		return resourceRef(EResourceType.STYLE);
	}
	
	public String getJavascriptResourceID(){
		return resourceRef(EResourceType.JAVASCRIPT);
	}
	

	//login 
    private String getLogin(String nodeName){
    	return this.mvNewConfiguration.login().get(nodeName);
    }
//BEGIN TASK #:TTL-GZ-PENGJM-00326.4 20141215[ITradeR5]Get default sorting flag(ITRADEFIVE-341)
    public String getSortableColumns(String pModule){
        return this.mvNewConfiguration.getSortable(pModule).get("columns");
    }
    public String getSortableOrder(String pModule){
        return this.mvNewConfiguration.getSortable(pModule).get("order");
    }
    public String getSortableDefaultColumns(String pModule){
        return this.mvNewConfiguration.getSortable(pModule).get("default-column");
    }
//END TASK #:TTL-GZ-PENGJM-00326.4 20141215[ITradeR5]Get default sorting flag(ITRADEFIVE-341)
//END TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]Using new configuration fixed it(ITRADEFIVE-211)
//BEGIN TASK #:TTL-GZ-BO.LI-00021 20140726[ITradeR5]New configuration (ITRADEFIVE-211)
  	public List<String> getAAStockIndicesColumns(){
  		return getAAStockIndiceshkcoumns();
  	}
  	public List<String> getAAStockIndiceshkcoumns(){
  	    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.getAAStockIndice();
  	    	@SuppressWarnings("rawtypes")
  			Iterator lvAAStockIndicesSet = dataPadding.original().keySet().iterator();
  			String lvOverrallSettings = "";
  			String lvExcludedSettings= "";
  			while(lvAAStockIndicesSet.hasNext()){
  				String lvAAStockIndices = lvAAStockIndicesSet.next().toString();
  				if(lvAAStockIndices.equals("overall")){
  					lvOverrallSettings = dataPadding.get(lvAAStockIndices);
  				}else if(lvAAStockIndices.equals("exclude")){
  					lvExcludedSettings = dataPadding.get(lvAAStockIndices);
  				}
  			}
  	    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
  	    }
  //BEGIN TASK #:TTL-GZ-BO.LI-00014 20140603[ITradeR5]Add module with Indices[ITRADEFIVE-170]
 	 public List<String> getAAStockIndicesName(String pString){
 	    	HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.showAAStockIndice();
 	    	@SuppressWarnings("rawtypes")
 			Iterator lvAAStockIndicesSet = dataPadding.original().keySet().iterator();
 			String lvAAstockIndices = "";
 			while(lvAAStockIndicesSet.hasNext()){
 				String lvAAStockIndicesName = lvAAStockIndicesSet.next().toString();
 				if(lvAAStockIndicesName.equals(pString)){
 					lvAAstockIndices = dataPadding.get(lvAAStockIndicesName);
 				}
 			}
 	    	return CollectionUtil.excludedTableicColumnsToList(lvAAstockIndices,"");
 	    }
//END TASK #:TTL-GZ-BO.LI-00014 20140603[ITradeR5]Add module with Indices[ITRADEFIVE-170]
//BEGIN TASK #:TTL-GZ-BO.LI-00015 20140609[ITradeR5]Add module with Favorite[ITRADEFIVE-171]
 	 public List <String> getAAStockFavorite(){
 		 HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.getAAStockFavorite();
 	    	@SuppressWarnings("rawtypes")
 			Iterator lvAAStockIndicesSet = dataPadding.original().keySet().iterator();
 			String lvOverrallSettings = "";
 			String lvExcludedSettings= "";
 			while(lvAAStockIndicesSet.hasNext()){
 				String lvAAStockIndices = lvAAStockIndicesSet.next().toString();
 				if(lvAAStockIndices.equals("overall")){
 					lvOverrallSettings = dataPadding.get(lvAAStockIndices);
 				}else if(lvAAStockIndices.equals("exclude")){
 					lvExcludedSettings = dataPadding.get(lvAAStockIndices);
 				}
 			}
 	    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
 	 }
 	 public String getFavoriteStockSearch() {
 		return this.mvNewConfiguration.FavoriteStockSearch().get("stock-search");
	}
//END TASK #:TTL-GZ-BO.LI-00015 20140609[ITradeR5]Add module with Favorite[ITRADEFIVE-171]
 	public List <String> getAAStockNewsColumns(){
 		HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.getAAStockNews();
	    	@SuppressWarnings("rawtypes")
			Iterator lvAAStockNewsSet = dataPadding.original().keySet().iterator();
			String lvOverrallSettings = "";
			String lvExcludedSettings= "";
			while(lvAAStockNewsSet.hasNext()){
				String lvAAStockNews = lvAAStockNewsSet.next().toString();
				if(lvAAStockNews.equals("overall")){
					lvOverrallSettings = dataPadding.get(lvAAStockNews);
				}else if(lvAAStockNews.equals("exclude")){
					lvExcludedSettings = dataPadding.get(lvAAStockNews);
				}
			}
	    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
	 }
 	public List <String> getPriceAlertColumns(){
 		HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.getPriceAlertColumns();
	    	@SuppressWarnings("rawtypes")
			Iterator lvPriceAlertSet = dataPadding.original().keySet().iterator();
			String lvOverrallSettings = "";
			String lvExcludedSettings= "";
			while(lvPriceAlertSet.hasNext()){
				String lvPriceAlert = lvPriceAlertSet.next().toString();
				if(lvPriceAlert.equals("overall")){
					lvOverrallSettings = dataPadding.get(lvPriceAlert);
				}else if(lvPriceAlert.equals("exclude")){
					lvExcludedSettings = dataPadding.get(lvPriceAlert);
				}
			}
	    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
	 }
//END TASK #:TTL-GZ-BO.LI-00021 20140726[ITradeR5]News configuration (ITRADEFIVE-211)
//BEGIN TASK #:TTL-GZ-BO.LI-00027 2014-08-07[ITradeR5]Input letter of the alphabet in Stock ID when selected USEX market(ITRADEFIVE-217)
	 *//**
	 * @author bo.li
	 * @since  20140807
	 * 
	 *//*
    public String EnterOrderSupplementZero(){
    	return this.mvNewConfiguration.enterOrderStockIdHandle().get("supplement-zero");
    }
	 *//**
	 * @author bo.li
	 * @since  20140807
	 * 
	 *//*
    public String EnterOrderInputAlphabet(){
    	return this.mvNewConfiguration.enterOrderStockIdHandle().get("input-alphabet");
    }
//BEGIN TASK #:TTL-GZ-kelly.kuang-00019 20160105[ITradeR5]Stock Code auto convert to Block Letter for oversea Asian market trading.[ITRADEFIVE-739]
    *//**
	 * @author kelly.kuang
	 * @since  20160105
	 * 
	 *//*
    public String EnterOrderExcludeInputAlphabet(){
    	return this.mvNewConfiguration.enterOrderStockIdHandle().get("exclude-input-alphabet");
    }
//END TASK #:TTL-GZ-kelly.kuang-00019 20160105[ITradeR5]Stock Code auto convert to Block Letter for oversea Asian market trading.[ITRADEFIVE-739]
//END TASK #:TTL-GZ-BO.LI-00027 2014-08-07[ITradeR5]Input letter of the alphabet in Stock ID when selected USEX market(ITRADEFIVE-217)
     *//**
	 * @author bo.li
	 * @since  20140811
	 * 
	 *//*
	public int getAAStockNewsPageSize(String pDwidth){
    	return getTablicPageSize("AAstock-News/HKSAANEPINT017",pDwidth);
    }
    // ENDING
	*//**
	 * @author bo.li
	 * @since  20140823
	 * 
	 *//*
	public String showIndicesGroup() {
		return this.mvNewConfiguration.AAStockIndiceShowGroup().get("group");
	}

//BEGIN TASK #:TTL-GZ-BO.LI-00039 2014-09-10[ITradeR5]TickerView group(ITRADEFIVE-250)
	*//**
	 * @author bo.li
	 * @since  20140910
	 * 
	 *//*
	public String showTickerViewGroup(){
		return this.mvNewConfiguration.TickerViewShowGroup().get("group");
	}
//END TASK #:TTL-GZ-BO.LI-00039 2014-09-10[ITradeR5]TickerView group(ITRADEFIVE-250)
	
	*//**
     * @author canyong.lin
     * @since 20140920
     *//*
	public HashMapDataPadding<List<String>> atmosphereTopic(){		
		return this.mvNewConfiguration.atmosphereTopic();
	}
	
	*//**
     * @author canyong.lin
     * @since 20140924
     *//*
	public HashMapDataPadding<String> aastockNewsPadding(){		
		return this.mvNewConfiguration.aastockNewsPadding();
	}
	
	*//**
	 * BEGIN TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]
	 * @author jay.wince
	 * @since  20140925
	 *//*
//BEGIN TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
	public DataPadding<String> aastockMarketInfoLogin(String pMarketID){
		return this.mvNewConfiguration.getAAStockMarketInfoLoginSettings(pMarketID);
	}
//END TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
	*//**
	 * @author bo.li
	 * @since  20141008
	 * 
	 *//*
	public HashMapDataPadding<String> supportIPOMargin(){
		return this.mvNewConfiguration.supportMargin();
	}
	*//**
	 * @author bo.li
	 * @since  20141111
	 * 
	 *//*
	public HashMapDataPadding<String> portfolioFilter(){
		return this.mvNewConfiguration.potfolioFilter();
	}
	*//**
	 * @author bo.li
	 * @since  20141111
	 * 
	 *//*
	public HashMapDataPadding<String> BankAccountBalanceFilter(){
		return this.mvNewConfiguration.BankAccountBalanceFilter();
	}

	*//**
     * @author junming.peng
     * @since  20141108
     * 
     *//*
	public int getTradingMainDayTradeRecord(){
	    return this.mvNewConfiguration.getTradingMainDayTradeRecord();
	}
	
	*//**
     * @author piece.lin	
     * @since  20141119
     * 
     *//*
	public String Sorting(String pModel){
		return this.mvNewConfiguration.tableFieldSorting(pModel).get("sorting-key");
	}
	*//**
     * @author piece.lin	
     * @since  20141119
     * 
     *//*
	public HashMapDataPadding<String> getSettingsModule(final String pModule){
		return this.mvNewConfiguration.settingsDataPadding(pModule);
				
	}
	*//**
	 * @author bo.li
	 * @since  20141125
	 * 
	 *//*
	public HashMapDataPadding<String> getDepositePadding(){
		return this.mvNewConfiguration.getFundManagementPadding("HKSFMEPINT013/deposite");
	}
	*//**
	 * @author bo.li
	 * @since  20141125
	 * 
	 *//*
	public HashMapDataPadding<String> getWithdrawalPadding(){
		return this.mvNewConfiguration.getFundManagementPadding("HKSFMEPINT013/withdrawal");
		
	}
	*//**
	 * @author bo.li
	 * @since  20141125
	 * 
	 *//*
	public List<String> getFundEnquiryColumns(){
 		HashMapDataPadding<String> dataPadding = this.mvNewConfiguration.getFundManagementPadding("HKSFEEPINT014/tableic/columns");
    	@SuppressWarnings("rawtypes")
		Iterator lvFundEnquirySet = dataPadding.original().keySet().iterator();
		String lvOverrallSettings = "";
		String lvExcludedSettings= "";
		while(lvFundEnquirySet.hasNext()){
			String lvFundEnquiry = lvFundEnquirySet.next().toString();
			if(lvFundEnquiry.equals("overall")){
				lvOverrallSettings = dataPadding.get(lvFundEnquiry);
			}else if(lvFundEnquiry.equals("exclude")){
				lvExcludedSettings = dataPadding.get(lvFundEnquiry);
			}
		}
    	return CollectionUtil.excludedTableicColumnsToList(lvOverrallSettings,lvExcludedSettings);
	}
	*//**
	 * @author bo.li
	 * @since  20141125
	 * 
	 *//*
	public HashMapDataPadding<String> getFundManagementSetting(String pNode){
		if (StringUtils.isNullStr(pNode)) {
			return this.mvNewConfiguration.getFundManagementPadding("settings");
		}else {
			return this.mvNewConfiguration.getFundManagementPadding("settings/"+pNode);
		}
	}
	
	*//**
	 * @author canyong.lin
	 * @since 20141201
	 *//*
	public HashMapDataPadding<String> getTokenConfig(String fid){
		return this.mvNewConfiguration.getTokenConfig(fid);
	}
	*//**
     * @author junming.peng
     * @since 20141208
     *//*
	public String orderTypeForAllowedToModify() {
        return this.mvNewConfiguration.nonAllowModifyOrder().get("order-type");
    }
//BEGIN TASK # :TTL-GZ-kelly.kuang-00021 20160111[ITradeR5]Get disable market modify button config.
	*//**
     * @author kelly.kuang
     * @since 20160111
     *//*
	public String disableMarketForModify() {
        return this.mvNewConfiguration.modifyOrder().get("market-disabled");
    }
//END TASK # :TTL-GZ-kelly.kuang-00021 20160111[ITradeR5]Get disable market modify button config.
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	public String getAAStockURIDataPadding(EDataFeedSource source,String leaf) {
		return this.mvNewConfiguration.getAAStockURIDataPadding(source,leaf);
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	public String getAAStockHostDataPadding(EDataFeedSource source,String leaf){
		return this.mvNewConfiguration.getAAStockHostDataPadding(source,leaf);
	}
	*//**
	 * @author jay.wince
	 * @since  20141209
	 * @return
	 *//*
	public IDashboardFeed getDashboardFeed(){
	    return ITradeDashboardFeedFactory.getInstance().getDashboardFeed(this.mvDashboardConfig);
	}
	*//**
     * @author canyong.lin
     * @since 20150116
     *//*
	public HashMapDataPadding<String> actionMapping(String actionID,String param) 
	{
		return this.mvNewConfiguration.actionMapping(actionID, param);
	}
	*//**
     * @author canyong.lin
     * @since 20150116
     *//*
	public HashMapDataPadding<String> functionDefinition(String fid){
		return this.mvNewConfiguration.functionDefinition(fid);
	}
	*//**
	 * @author canyong.lin
	 * @since 20150127
	 *//*
	public HashMapDataPadding<String> getTokenUsage(String fid) {
		return this.mvNewConfiguration.getTokenUsage(fid);
	}
	
	*//**
	 * @author canyong.lin
	 * @since 20150128
	 *//*
	public HashMapDataPadding<String> getFunctionGroup(String fid) {
		return this.mvNewConfiguration.getFunctionGroup(fid);
	}
	*//**
	 * @author canyong.lin
	 * @since 20150129
	 *//*
	public HashMapDataPadding<String> getCSRFConfig(){
		return this.mvNewConfiguration.getCSRFConfig();
	}
	*//**
	 * @author junming.peng
	 * @since 20150210
	 *//*
	public String getTradingSituation(){
	    return this.mvNewConfiguration.getTradingSituation();
	}
	*//**
	 * @author kelly.kuang
	 * @since 20170411
	 *//*
	public boolean getTradingSameSessionID(){
	    return this.mvNewConfiguration.getTradingSameSessionID();
	}
	
	   
    public String getCompanyService(){
        return BaseAppDiversityConfiguration.getInstance().getServiceIDBy(this.mvCompany);
    }
	*//**
     	* @author junming.peng
    	* @since 20150331
     	*//*
    public String getDNTopicName(){
        return this.mvNewConfiguration.getDNTopicName();
    }
    *//**
     * @author canyong.lin
     * @since 20150413
     *//*
	public String getPrimarykey(String fid) {
		return this.mvNewConfiguration.getPrimarykey(fid);
	}
	
	*//**
	 * 
	 * @Author junming.peng
	 * @Date 2015-06-09 14:02:03
	 * @Since  [ITradeR5/V15.05.11]
	 * @see [ITradeCompanyConfig#getHistoryTab]
	 * @description [Control the number of display in the history tab]
	 *//*
	public String getHistoryFunctionTab(){
	    return this.mvNewConfiguration.getHistoryFunctionTab();
	}
	
	*//**
	 * 
	 * @Author junming.peng
	 * @Date 2015-07-03 10:50:31
	 * @Since  [ITradeR5/V15.06.17]
	 * @see [ITradeCompanyConfig#getMarketOrderType]
	 * @description [Corresponding to different market types]
	 *//*
	public Vector<String> getMarketOrderType(String pMarket){
	    String lvOrderType = this.mvNewConfiguration.getMarketOrderType(pMarket);
//BEGIN TASK # :TTL-GZ-kelly.kuang-00023 20160114[ITradeR5]Bug fix orderType empty issue.(ITRADEFIVE-753)
	    Vector<String> lvOrderTypeList = new Vector<String>();
	    if(!Utils.isNullStr(lvOrderType)){
	    	lvOrderTypeList = StringUtils.splitToVector(lvOrderType, "|");
	    }
//END TASK # :TTL-GZ-kelly.kuang-00023 20160114[ITradeR5]Bug fix orderType empty issue.(ITRADEFIVE-753)
	    return lvOrderTypeList;
	}
	
	*//**
     * @Author bo.li
     * @Since  [ITradeR5/V15.06.17]
     * @Date 2015-7-23 17:12:02
     * @see [ITradeCompanyConfig#getFilledEnquiryStatus]
     *//*
	public String getFilledQTYStatus(){
	    return this.mvNewConfiguration.getFilledEnquiryStatus();
	}
	*//**
     * 
     * @Author demonwx.gu
     * @Date 2015-07-27 10:50:31
     * @Since  [ITradeR5/V15.06.17]
     * @see [ITradeCompanyConfig#isEnableSpecialClient]
     * 
     *//*
    public boolean isEnableSpecialClient(){
        String lvSpecialClient = this.mvNewConfiguration.getSpecialClient("enable");
        boolean lvEnable = Boolean.TRUE.toString().equalsIgnoreCase(lvSpecialClient);
        return lvEnable;
    }
    *//**
     * 
     * @Author demonwx.gu
     * @Date 2015-07-27 10:50:31
     * @Since  [ITradeR5/V15.06.17]
     * @see [ITradeCompanyConfig#getSpecialClient]
     * 
     *//*
    public List<String> getSpecialClient(){
        String lvSpecialClient = this.mvNewConfiguration.getSpecialClient("client");
        List<String> lvSpecialClientList = StringUtils.splitToVector(lvSpecialClient, ",");
        return lvSpecialClientList;
    }
    *//**
     * @Author jummyjw.liu
     * @Date 2015-08-19 10:50:31
     * @Since  [ITradeR5/V15.06.17]
     * @see [ITradeCompanyConfig#getSupportBrowser]
     *//*
    public HashMapDataPadding<List<String>> getSupportBrowser(){      
        return this.mvNewConfiguration.getSupportBrowser();
    }
    *//**
     * @Author jummyjw.liu
     * @Date 2015-08-28 13:40:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#specialSessionTimeoutEnable]
     *//*
    public boolean specialSessionTimeoutEnable() {
        boolean lvEnable = false;
        if(!Utils.isNullStr(this.mvNewConfiguration.sessionPadding().original().get("special-timout-enable"))){
            lvEnable = this.mvNewConfiguration.sessionPadding().original().get("special-timout-enable").equals("true") ? true :false;
        }
        return lvEnable;
    }
    *//**
     * @Author jummyjw.liu
     * @Date 2015-08-28 13:40:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getSpecialSessionTimeoutID]
     *//*
    public Vector<String> getSpecialSessionTimeoutID() {
        String lvSessionTimeoutID = this.mvNewConfiguration.sessionPadding().original().get("special-users");
        Vector<String> lvSpecialSessionTimeoutIDVector = new Vector<String>();
        if(!Utils.isNullStr(lvSessionTimeoutID)){
            String[] lvSpecialSessionTimeoutIDArray = lvSessionTimeoutID.split(",");
            if (lvSpecialSessionTimeoutIDArray.length > 0) {
                String lvSpecialSessionTimeoutIDTemp = new String();
                for (int i = 0; i < lvSpecialSessionTimeoutIDArray.length; i++) {
                    lvSpecialSessionTimeoutIDTemp = (String) lvSpecialSessionTimeoutIDArray[i];
                    lvSpecialSessionTimeoutIDVector.add(lvSpecialSessionTimeoutIDTemp);
                }
            }
        }
        return lvSpecialSessionTimeoutIDVector;
    }
    *//**
     * @Author jummyjw.liu
     * @Date 2015-08-28 13:40:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getSpecialSessionTimeout]
     *//*
    public String getSpecialSessionTimeout() {
        try{
            String lvSessionTimeout = this.mvNewConfiguration.sessionPadding().original().get("special-timeout");
            if(!Utils.isNullStr(lvSessionTimeout)){
                lvSessionTimeout = String.valueOf(Long.parseLong(this.mvNewConfiguration.sessionPadding().original().get("special-timeout"))*60*1000);
                return lvSessionTimeout;
            }else{
                return "";
            }
        }catch(Exception e){
            return "";
        }
    }
    *//**
     * @Author jummyjw.liu
     * @Date 2015-08-28 13:40:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getSessionTimeOut]
     *//*
    public String getSessionTimeOut() {
        return String.valueOf(Long.parseLong(this.mvNewConfiguration.sessionPadding().original().get("timeout"))*60*1000);  
    }
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)
    *//**
     * @Author kelly.kuang
     * @Date 2015-11-09 14:45:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getRecoveryFunctionGroup]
     *//*
    public HashMapDataPadding<String> getRecoveryFunctionGroup(String fid) {
		return this.mvNewConfiguration.getRecoveryFunctionGroup(fid);
	}
	
    *//**
     * @Author kelly.kuang
     * @Date 2015-11-09 14:45:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getRecoveryFunction]
     *//*
	public HashMapDataPadding<String> getRecoveryFunction(String fgid, String select) {
		return this.mvNewConfiguration.getRecoveryFunction(fgid,select);
	}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)
	*//**
     * @Author kelly.kuang
     * @Date 2015-11-13 15:07:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getNormalRecoveryFunction]
     *//*
	public HashMapDataPadding<String> getNormalRecoveryFunction(String fid) {
		return this.mvNewConfiguration.getNormalRecoveryFunction(fid);
	}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)

//BEGIN TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
	*//**
     * @Author kelly.kuang
     * @Date 2015-12-03 15:07:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getDisplayTodayValueInGoodTillDateComboBox]
     *//*
	public boolean getDisplayTodayValueInGoodTillDateComboBox(){
		return this.mvNewConfiguration.displayTodayValueInGoodTillDateComboBox();
	}
	
	*//**
     * @Author kelly.kuang
     * @Date 2015-12-03 15:07:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getAllowGoodTillDate]
     *//*
	public boolean getAllowGoodTillDate(){
    	return this.mvNewConfiguration.allowGoodTillDate();
    }
//END TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
	
//BEGIN TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
	*//**
     * @Author kelly.kuang
     * @Date 2015-12-10 15:07:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getMarketQuotaBalance]
     *//*
	public HashMapDataPadding<String> getMarketQuotaBalance() {
		return this.mvNewConfiguration.getMarketQuotaBalance();
	}
//END TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
	
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)
	*//**
	 * @Author stevenzg.li
	 * @Date 2015-12-28
	 * @Since [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getResizableColumns]
	 *//*
    public String getResizableColumns(String pModule) {
        return this.mvNewConfiguration.getResizable(pModule).get("columns");
    }
    *//**
     * @Author stevenzg.li
     * @Date 2015-12-28
     * @Since [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getResizableDefaultColumns]
     *//*
    public String getResizableDefaultColumns(String pModule) {
        return this.mvNewConfiguration.getResizable(pModule).get("default-column");
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)
    
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
    *//**
     * @Author stevenzg.li
     * @Date 2016-01-07
     * @Since [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getIdentityValidationSetting]
     *//*
    public HashMapDataPadding<String> getIdentityValidationSetting() {
        return this.mvNewConfiguration.getIdentityValidationSetting();
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
    
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00026 2016-01-08[ITradeR5-CISI]Add CITIC Disclaimer setting.(ITRADEFIVE-742)
    *//**
     * @Author stevenzg.li
     * @Date 2016-01-08
     * @Since [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getCustomerServicePhoneNumber]
     *//*
    public HashMapDataPadding<String> getCustomerServicePhoneNumber(){
        return this.mvNewConfiguration.getCustomerServicePhoneNumber();
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00026 2016-01-08[ITradeR5-CISI]Add CITIC Disclaimer setting.(ITRADEFIVE-742)  

//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
    *//**
     * @Author kelly.kuang
     * @Date 2016-03-18 16:26:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getPlaceOrderConfirmPasswordRequire]
     *//*
    public boolean getPlaceOrderConfirmPasswordRequire() {
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160405[ITradeR5-CISI]Support confirm-flow override.(ITRADEFIVE-787)
    	HashMapDataPadding<String> lvConfirmFlow = this.mvNewConfiguration.getPlaceOrderConfirmFlow();
    	if(lvConfirmFlow == null || lvConfirmFlow.original().isEmpty()){
    		lvConfirmFlow = this.mvNewConfiguration.getDefaultEnterOrderConfirmFlow();
    	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160405[ITradeR5-CISI]Support confirm-flow override.(ITRADEFIVE-787)
		return Boolean.TRUE.toString().equalsIgnoreCase(lvConfirmFlow.get("password-required"));
	}
    
    *//**
     * @Author kelly.kuang
     * @Date 2016-03-18 16:26:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getModifyOrderConfirmPasswordRequire]
     *//*
    public boolean getModifyOrderConfirmPasswordRequire() {
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160401[ITradeR5-CISI]Support confirm-flow override.(ITRADEFIVE-787)
    	HashMapDataPadding<String> lvConfirmFlow = this.mvNewConfiguration.getModifyOrderConfirmFlow();
    	if(lvConfirmFlow == null || lvConfirmFlow.original().isEmpty()){
    		lvConfirmFlow = this.mvNewConfiguration.getDefaultEnterOrderConfirmFlow();
    	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160401[ITradeR5-CISI]Support confirm-flow override.(ITRADEFIVE-787)
		return Boolean.TRUE.toString().equalsIgnoreCase(lvConfirmFlow.get("password-required"));
	}
    
    *//**
     * @Author kelly.kuang
     * @Date 2016-03-18 16:26:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getCancelOrderConfirmPasswordRequire]
     *//*
    public boolean getCancelOrderConfirmPasswordRequire() {
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160401[ITradeR5-CISI]Support confirm-flow override.(ITRADEFIVE-787)
    	HashMapDataPadding<String> lvConfirmFlow = this.mvNewConfiguration.getCancelOrderConfirmFlow();
    	if(lvConfirmFlow == null || lvConfirmFlow.original().isEmpty()){
    		lvConfirmFlow = this.mvNewConfiguration.getDefaultEnterOrderConfirmFlow();
    	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160401[ITradeR5-CISI]Support confirm-flow override.(ITRADEFIVE-787)
		return Boolean.TRUE.toString().equalsIgnoreCase(lvConfirmFlow.get("password-required"));
	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160401[ITradeR5-CISI]Support special client for password confirm.(ITRADEFIVE-787)
    *//**
     * @Author kelly.kuang
     * @Date 2016-04-01 16:35:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getPlaceOrderConfirmSpecialClient]
     *//*
    public List<String> getPlaceOrderConfirmSpecialClient(){
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160405[ITradeR5-CISI]Support confirm-flow override.(ITRADEFIVE-787)
    	HashMapDataPadding<String> lvConfirmFlow = this.mvNewConfiguration.getPlaceOrderConfirmFlow();
    	if(lvConfirmFlow == null || lvConfirmFlow.original().isEmpty()){
    		lvConfirmFlow = this.mvNewConfiguration.getDefaultEnterOrderConfirmFlow();
    	}
        String lvSpecialClient = lvConfirmFlow.get("special-client");
//END TASK # :TTL-GZ-kelly.kuang-00039 20160405[ITradeR5-CISI]Support confirm-flow override.(ITRADEFIVE-787)
        List<String> lvSpecialClientList = StringUtils.splitToVector(lvSpecialClient, ",");
        return lvSpecialClientList;
    }
    
    *//**
     * @Author kelly.kuang
     * @Date 2016-04-01 16:35:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getModifyOrderConfirmSpecialClient]
     *//*
    public List<String> getModifyOrderConfirmSpecialClient(){
    	HashMapDataPadding<String> lvConfirmFlow = this.mvNewConfiguration.getModifyOrderConfirmFlow();
    	if(lvConfirmFlow == null || lvConfirmFlow.original().isEmpty()){
    		lvConfirmFlow = this.mvNewConfiguration.getDefaultEnterOrderConfirmFlow();
    	}
        String lvSpecialClient = lvConfirmFlow.get("special-client");
        List<String> lvSpecialClientList = StringUtils.splitToVector(lvSpecialClient, ",");
        return lvSpecialClientList;
    }
    
    *//**
     * @Author kelly.kuang
     * @Date 2016-04-01 16:35:00
     * @Since  [ITradeR5/V15.08.28]
     * @see [ITradeCompanyConfig#getCancelOrderConfirmSpecialClient]
     *//*
    public List<String> getCancelOrderConfirmSpecialClient(){
    	HashMapDataPadding<String> lvConfirmFlow = this.mvNewConfiguration.getCancelOrderConfirmFlow();
    	if(lvConfirmFlow == null || lvConfirmFlow.original().isEmpty()){
    		lvConfirmFlow = this.mvNewConfiguration.getDefaultEnterOrderConfirmFlow();
    	}
        String lvSpecialClient = lvConfirmFlow.get("special-client");
        List<String> lvSpecialClientList = StringUtils.splitToVector(lvSpecialClient, ",");
        return lvSpecialClientList;
    }
//END TASK # :TTL-GZ-kelly.kuang-00039 20160401[ITradeR5-CISI]Support special client for password confirm.(ITRADEFIVE-787)
    
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.
    public boolean isSupportOpenAccount() {
        return this.mvNewConfiguration.getOpenAccountInfo().getBoolean("enable");
    }
    
    public String getOpenAccountLink() {
        return this.mvNewConfiguration.getOpenAccountInfo().get("link");
    }
    
    public String getOpenAcctLinkLanguage(String langKey) {
        return this.mvNewConfiguration.getOpenAccountInfo().get(langKey);
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.
    
    public String getRunModeConfigInfo(String nodeName) {
        return this.mvNewConfiguration.getRunModeConfigInfo().get(nodeName);
    }
    
    public String getAuthenticationOTPConfig(String pModule, String nodeName) {
        return this.mvNewConfiguration.getAuthenticationOTPConfig(pModule).get(nodeName);
    }
  
}
*/