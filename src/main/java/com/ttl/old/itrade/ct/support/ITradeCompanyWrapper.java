/**
 * 
 *//*
package com.itrade.ct.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import DashboardCategoriesModel;
import DashboardMgrModel;
import DashboardModel;
import DashboardWidgetModel;
import com.itrade.base.dashboard.strategy.IFunctionIDFilterStrategy;
import com.itrade.base.dashboard.theme.IThemeReadyStrategy;
import com.itrade.base.dashboard.theme.ITradeCompanyTheme;
import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.base.exception.DefaultExceptionRecovery;
import com.itrade.base.exception.IExceptionRecovery;
import com.itrade.base.jesapi.ITradeAPI;
import com.itrade.base.resource.IResourceRegistryImpl;
import com.itrade.biz.model.FunctionIDModel;
import ITradeUser;
import com.itrade.configuration.util.ITradeResourceConfiguration;
import com.itrade.configuration.util.ITradeResourceConfiguration.EResourceType;
import com.itrade.util.CollectionUtil;
import com.itrade.util.StringUtils;
import com.itrade.web.helper.ITradeSessionManager;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140812 by xuejie.xiao</p>
 * @author jay.wince
 * @Date    20140812
 * @version 1.0
 *//*
public final class ITradeCompanyWrapper implements Serializable{
     *//**
	 * 
	 *//*
	private static final long serialVersionUID = 201408121533L;
	*//**
	 * 
	 *//*
	private ITradeCompanyConfig mvConfig;
    private ITradeCompanyTheme  mvCompanyTheme;
    
    private ITradeResourceConfiguration mvResourceConfiguration;
    
    //BEGIN TASK #:TTL-GZ-CANYONG.LIN-00038 20150331[ITradeR5]Save the resources is already loaded.(ITRADEFIVE-511)
  	private IResourceRegistryImpl mvResourceRegistry=null;
  	//END   TASK #:TTL-GZ-CANYONG.LIN-00038 20150331[ITradeR5]Save the resources is already loaded.(ITRADEFIVE-511)
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]Manage exception recovery link.(ITRADEFIVE-693)
  	private IExceptionRecovery mvExceptionRecovery = null;
//END TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]Manage exception recovery link.(ITRADEFIVE-693)
  	
     public ITradeCompanyWrapper (ITradeCompanyConfig pConfig) {
    	 this.mvConfig = pConfig;
    	 initCompanyTheme();
     }
     
     private void initCompanyTheme(){
         HashMapDataPadding<String> lvOptions = this.mvConfig.themePadding();
         lvOptions.put("theme-exception", ""+this.mvConfig.solutionForThemeException());
    	 this.mvCompanyTheme = new ITradeCompanyTheme(lvOptions,
													  this.mvConfig.getStyleResourceID(),
													  this.mvConfig.getCompanyCode());
     }
     
 	public ITradeCompanyTheme getCompanyTheme() {
		    return this.mvCompanyTheme;
	}

	public void setCompanyTheme(final String pTheme){
		getCompanyTheme().setCurrentTheme(pTheme);
	}
	
	public ITradeCompanyConfig getCompanyConfig() {
		return this.mvConfig;
	}
	
	public ITradeResourceConfiguration getResourceConfiguration (){
	    if (this.mvResourceConfiguration == null)
        {
            this.mvResourceConfiguration = new ITradeResourceConfiguration(this.mvConfig.getCompanyService());
        }
	    return this.mvResourceConfiguration;
	}
	
	public String getReadyStylingSettings(String styleid){
		return getReadyStylingSettings(styleid, null);
	}
	public String getReadyStylingSettings(String styleid,String theme){
	    Map<String, String> lvDataMap = new HashMap<String,String>();
	    if (!StringUtils.isNullStr(theme)) {
            lvDataMap.put("theme", theme);
        }else{
            lvDataMap.put("theme", getCompanyTheme().getCurrentTheme());
        }
	    lvDataMap.put("id", this.mvConfig.getCompanyCode().toLowerCase());//bo.li on 20150303:Increase the CMCB company(ITRADEFIVE-495)
	    return getResourceConfiguration().getResourceSrcList(EResourceType.STYLE, styleid, lvDataMap);
	}
	
	public String getReadyJavascriptSettings(String javascriptid){
	    Map<String, String> lvDataMap = new HashMap<String, String>();
	    String lvLangText = this.mvConfig.defaultLangText();
        if (ITradeSessionManager.currentUser()!=null) {
            lvLangText = ITradeSessionManager.getCurrentLangText();
        }
        lvDataMap.put("langText", lvLangText);
		lvDataMap.put("id", this.mvConfig.getCompanyCode().toLowerCase());//bo.li on 20150303:Increase the CMCB company(ITRADEFIVE-495)
	    return getResourceConfiguration().getResourceSrcList(EResourceType.JAVASCRIPT, javascriptid, lvDataMap);
	}
	
	*//**
	 * @author jay.wince
	 * @since  201411126
	 *     Pending to hanle exception
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	public final boolean isCompanyThemeReady() {		
		boolean config = this.mvConfig.themePadding().getBoolean("themes-ready");
		@SuppressWarnings("rawtypes")
		IThemeReadyStrategy themeStrategy = ITradeAPI.themeReadyStrategy();
		themeStrategy.setPolicyParameters(getCompanyTheme());
		return themeStrategy.isReady(config);
	}
	
	*//**
	 * @since 2015-03-31
	 * @author canyong.lin
	 * @version V15.08.28
	 * @description Get ResourceRegistry.
	 * @return
	 *//*
	public IResourceRegistryImpl getResourceRegistry(){
		if(this.mvResourceRegistry==null)
		{
			this.mvResourceRegistry = new IResourceRegistryImpl();
		}
		return this.mvResourceRegistry;
	}
	*//**
	 * @since 2015-10-23
	 * @author canyong.lin
	 * @version V15.08.28
	 * @description Get the user blocks's id.
	 * @return
	 *//*
	public ArrayList<String> getMvBlockIDs(DashboardMgrModel pModel){
		ITradeUser lvITradeUser 					= ITradeSessionManager.getITradeUser();
		List<FunctionIDModel> lvFunctionModelList   = lvITradeUser.getFunctionIDModel();//get function id model
		ArrayList<String> lvBlockIDs =  new ArrayList<String>();
		for (DashboardModel dashboardModel : pModel.getDashboardmodels()) {	
			//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00038 20150326[ITradeR5]Category Manager checking.(ITRADEFIVE-511)
			List<DashboardWidgetModel> lvWidgets = null;
			if (dashboardModel.getCategoryManager()==null) {
				lvWidgets = dashboardModel.getWidgets();
				this.filterDashboardWidget(lvWidgets, lvFunctionModelList,lvBlockIDs);
			}else {
				for(DashboardCategoriesModel lvCategoriesModel:dashboardModel.getCategoryManager().getCategories()){
					lvWidgets = lvCategoriesModel.getWidgets();
					this.filterDashboardWidget(lvWidgets, lvFunctionModelList,lvBlockIDs);
				}
			}
	   }
		return lvBlockIDs;
	}
	*//**
	 * @since 2015-10-23
	 * @author canyong.lin
	 * @version V15.08.28
	 * @description Get the dashboard's blocks id.
	 * @return
	 *//*
	private void filterDashboardWidget(List<DashboardWidgetModel> pWidgets,List<FunctionIDModel> pFunctionModelList,ArrayList<String> pBlockIDs){
		List<String> dashboardWidgetIDList = CollectionUtil.fetchElementPropertyToList(pWidgets,"id");
		IFunctionIDFilterStrategy lvfilterStrategy = ITradeAPI.dashboardFilterStrategy();
		int size = dashboardWidgetIDList.size();
		for (int i = 0; i < size; i++) {
			String dashboardWid = dashboardWidgetIDList.get(i);
			if(lvfilterStrategy.contains(dashboardWid, pFunctionModelList)){
				//BEGIN TASK #:TTL-CANYONG.LIN-00038 20150326[ITradeR5]Block register.(ITRADEFIVE-511)
				String lvBlockID = StringUtils.extractDashboardFunctionWidgetId(dashboardWid);
				if(!pBlockIDs.contains(lvBlockID)){
					pBlockIDs.add(lvBlockID);
				}
				//END   TASK #:TTL-CANYONG.LIN-00038 20150326[ITradeR5]Block register.(ITRADEFIVE-511)
			}
		}
	}
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get exception recovery object.(ITRADEFIVE-693)
	public IExceptionRecovery getExceptionRecovery(){
		if(this.mvExceptionRecovery==null)
		{
			this.mvExceptionRecovery = new DefaultExceptionRecovery();
		}
		return this.mvExceptionRecovery;
	}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get exception recovery object.(ITRADEFIVE-693)
}
*/