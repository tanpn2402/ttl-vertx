/**
 * 
 *//*
package com.itrade.web.helper;

import com.itrade.util.StringUtils;


*//**
 * <p>
 * Copyright: Copyright(c)Transaction Technologies Limited 2014-5-23 by xuejie.xiao
 * </p>
 * 
 * @author jay.wince
 * @Date 2014-5-23
 * @version 1.0
 *//*
public class LanguageManager {

	*//**
	 * The current representation of ITrade language.
	 * @author jay.wince
	 * @since   20140523 
	 *//*
	public static String currentLanguage() {
		return ITradeSessionManager.getCurrentLangText();
	}

	*//**
	 * The current representation of AAStock snapshot language.
	 * @author jay.wince
	 * @since   20140523 
	 *//*
	public static String currentAAStocksSnapShotLanguage() {
		return currentAAStocksSnapShotLanguage(null);
	}
	*//**
	 * The current representation of AAStock snapshot language.
	 * @author jay.wince
	 * @since   20140523 
	 *//*
	public static String currentAAStocksSnapShotLanguage(String pITradeLang) {
		String itradeLang = pITradeLang;
		if (StringUtils.isNullStr(pITradeLang)) {
			itradeLang = currentLanguage();
		}
		String aastockSnapshotLang = "eng";
		if ("zh_CN".equals(itradeLang)) {
			aastockSnapshotLang = "chn";
		} else if ("zh_TW".equals(itradeLang)) {
			aastockSnapshotLang = "chi";
		}
		return aastockSnapshotLang;
	}
	*//**
	 * The available languages for AAStock snapshot.
	 * @author jay.wince
	 * @since   20140523 
	 *//*
	public static String[] availableAAStocksSnapShotLanguages() {
		return new String[] { "eng", "chi", "chn" };
	}

	public static String currentAAStocksSnapShotLanguageChinese() {
		String itradeLang = currentLanguage();
		if ("zh_CN".equals(itradeLang)) {
			return "chn";
		} else {
			return "chi";
		}
	}

	*//**
	 * The current representation of AAStock stock search language.
	 * @author jay.wince
	 * @since   20140523 
	 *//*
	public static String currentAAStocksSearchLanguage() {
		String itradeLang = currentLanguage();
		if ("zh_TW".equals(itradeLang)) {
			return "ChiDesp";
		} else if ("zh_CN".equals(itradeLang)) {
			return "ChnDesp";
		}

		return "Desp";
	}

	*//**
	 * The current representation of AAStock stock auto-complete language.
	 * @author jay.wince
	 * @since   20140523 
	 *//*
	public static String currentAAStocksAutoCompleteLanguage() {
		String itradeLang = currentLanguage();
		if ("zh_TW".equals(itradeLang)) {
			return "tc";
		} else if ("zh_CN".equals(itradeLang)) {
			return "sc";
		}
		return "en";
	}
//BEGIN TASK #:TTL-GZ-BO.LI-00029.1 20140820[ITradeR5]AAStock news language(ITRADEFIVE-180)
	*//**
	 * The current representation of AAStock stock news language.
	 * @author bo.li
	 * @since   20140819 
	 *//*
	public static String currentAAStocksNewsLanguage() {
		String itradeLang = currentLanguage();
		String aastockSnapshotLang = "chn";
		if ("en_CN".equals(itradeLang)) {
			aastockSnapshotLang = "chn";
		} else if ("zh_TW".equals(itradeLang)) {
			aastockSnapshotLang = "chi";
		}
		return aastockSnapshotLang;
	}
//END TASK #:TTL-GZ-BO.LI-00029.1 20140820[ITradeR5]AAStock news language(ITRADEFIVE-180)
	
	*//**
	 * Used for Market info from aastock.
	 * @author jay.wince
	 * @since  20140925
	 * @return
	 *//*
	public static String aastockMarketInfoLanguage(){
		return currentAAStocksSnapShotLanguage();
	}
}
*/