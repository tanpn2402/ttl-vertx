package com.ttl.old.itrade.web.constants;
/**
 * @description used for ${function} of ITRADE_CONTRACT_SESSION_KEY
 * @author clover_he
 * @since 2013-11-22
 */
public final class HKSFunctionNameTag {

	/*
	 * Corporate Action Module
	 */
	public final static String REPLY = "Reply";
	public final static String CANCEL = "Cancel";
	public final static String MODIFY = "Modify";
	

}
	