package com.ttl.old.itrade.web.constants;
/**
 * @since  2013-08-28
 * @author jay.wince
 * @description : key for session holding value. 
 */
public final class HKSSesstionKey {
   /*******************dashboard layout key collection*******************/
	public static final String LAYOUT_CURRENT = "currentLayout";
	/**
	 * @deprecated
	 */
	public static final String LAYOUT_CLASS_NAME = "layoutClassName";
	
	/*******************web theme***************************************/
	public static final String CURRENT_THEME = "theme";
	@Deprecated
	public static final String CURRENT_THEME_FOLDER = "themefolder";
	
	/*******************differ from Company*****************************/
	public static final String CURRENT_COMPANY = "cpy";
	
	/*******************Client************************************************/
	public static final String TRADINGACCSEQ = "TRADINGACCSEQ";
	
//BEGIN TASK #:TTL-GZ-clover_he-00057 2013-11-22 [ITrade5] User session mechanisms achieve entering only one order for CA module
	/*******************itrade contract session key****************************/
	public static final String ITRADE_CONTRACT_SESSION_KEY = "${company}${client}${module}${function}";
//END TASK #:TTL-GZ-clover_he-00057 2013-11-22 [ITrade5] User session mechanisms achieve entering only one order for CA module
	
	/******************Language****************************/
	public static final String LANGUAGE = "lang";
	
//BEGIN TASK #:TTL-GZ-PENGJM-00189 2014-01-03[ITrade5]store user session key 	
	/*******************itrade user session key****************************/
	public static final String ITRADE_USER_SESSION_KEY="ITradeUserSessionKey";
//END TASK #:TTL-GZ-PENGJM-00189 2014-01-03[ITrade5]store user session key
	
//BEGIN TASK #:TTL-GZ-clover_he-00086 2014-02-14 [ITrade5] Action Layer：Code optimization for Place Order.[ITRADEFIVE-109]
	public static final String ORDER_SUCCESS = "orderSuccess";
	public static final String MARKET_MAX_LOTSIZE = "MARKETMAXLOTSIZE";
	public static final String SESSION_MODEL_ENTER_ORDER = "SessionModelEnterOrder";
//END TASK #:TTL-GZ-clover_he-00086 2014-02-14 [ITrade5] Action Layer：Code optimization for Place Order.[ITRADEFIVE-109]
	
//BEGIN TASK #:TTL-GZ-clover_he-00094 2014-02-26 [ITrade5] Action Layer：Code optimization for Modify Order.[ITRADEFIVE-129]
	public static final String SESSION_MODEL_MODIFY_ORDER = "SessionModelModifyOrder";
//END TASK #:TTL-GZ-clover_he-00094 2014-02-26 [ITrade5] Action Layer：Code optimization for Modify Order.[ITRADEFIVE-129]
	
//BEGIN TASK #:TTL-GZ-clover_he-00098 2014-03-05 [ITrade5] Action Layer：Code optimization for Cancel Order.[ITRADEFIVE-133]
	public static final String SESSION_MODEL_CANCEL_ORDER = "SessionModelCancelOrder";
//END TASK #:TTL-GZ-clover_he-00098 2014-03-05 [ITrade5] Action Layer：Code optimization for Cancel Order.[ITRADEFIVE-133]
	
//BEGIN TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade5] Action Layer：Code optimization for show order detail of order enquiry.[ITRADEFIVE-152]
	public static final String C_NAME = "CNAME";
	public static final String FULL_NAME = "FULLNAME";
//END TASK #:TTL-GZ-clover_he-00104.1 20140424 [ITrade] Action Layer：Code optimization for show order detail of order enquiry.[ITRADEFIVE-152]
//BEGIN TASK #:TTL-GZ-PENGJM-00325 20141121[ITradeR5]Modify Order screen closed when modify order failed(ITRADEFIVE-416)	
	public static final String SESSION_MODEL_GEN_MODIFY_ORDER = "SessionModelGenModifyOrder";
//END TASK #:TTL-GZ-PENGJM-00325 20141121[ITradeR5]Modify Order screen closed when modify order failed(ITRADEFIVE-416)
//BEGIN TASK # :TTL-GZ-kelly.kuang-00010 20151216[ITradeR5]Support Order resubmit.(ITRADEFIVE-720)
	public static final String SESSION_MODEL_RESUBMIT_MODIFY_ORDER = "SessionModelResubmitModifyOrder";
//END TASK # :TTL-GZ-kelly.kuang-00010 20151216[ITradeR5]Support Order resubmit.(ITRADEFIVE-720)
//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00025 20150113[ITradeR5]One browser only one main page.(ITRADEFIVE-476)
	public static final String ALREADY_LOADED_MAIN_PAGE = "alreadyLoadedMainPage";
//END   TASK #:TTL-GZ-CANYONG.LIN-00025 20150113[ITradeR5]One browser only one main page.(ITRADEFIVE-476)
//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00014.8 20150129[ITradeR5]Add "csrf" token key.(ITRADEFIVE-369)	
	public static final String CSRF_TOKEN = "csrfToken";
//END   TASK #:TTL-GZ-CANYONG.LIN-00014.8 20150129[ITradeR5]Add "csrf" token key.(ITRADEFIVE-369)
//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00029.2 20150212[ITradeR5]Save login user object.(ITRADEFIVE-489)
	public static final String ALREADY_LOGIN_USER  = "AlreadyLoginedUser";
//END   TASK #:TTL-GZ-CANYONG.LIN-00029.2 20150212[ITradeR5]Save login user object.(ITRADEFIVE-489)
//BEGIN TASK #:TTL-GZ-CANYONG.LIN-00033 20150316[ITradeR5]Login choose page.(ITRADEFIVE-514)
	public static final String LOGIN_CHOOSE_PAGE_CLOSE  = "loginChooseClose";
//END   TASK #:TTL-GZ-CANYONG.LIN-00033 20150316[ITradeR5]Login choose page.(ITRADEFIVE-514)
//BEGIN TASK #:TTL0GZ-CANYONG.LIN-00033 20150316[ITradeR5]Browser tab close.(ITRADEFIVE-514)
	public static final String BROWSER_CLOSE  = "browserTabClose";
//END   TASK #:TTL0GZ-CANYONG.LIN-00033 20150316[ITradeR5]Browser tab close.(ITRADEFIVE-514)
//BEGIN TTL-GZ-CANYONG.LIN-00033.2 20150321[ITradeR5]The same browser checking.(ITRADEFIVE-514)
	public static final String SESSION_ID  = "SID";
//END   TTL-GZ-CANYONG.LIN-00033.2 20150321[ITradeR5]The same browser checking.(ITRADEFIVE-514)
//BEGIN TTL-GZ-CANYONG.LIN-00037 20150323[ITradeR5]Save the old CSRF token value.(ITRADEFIVE-521)
	public static final String OLD_CSRF_TOKEN_VALUE  = "oldCSRFTokenValue";
//END   TTL-GZ-CANYONG.LIN-00037 20150323[ITradeR5]Save the old CSRF token value.(ITRADEFIVE-521)
//BEGIN TTL-GZ-CANYONG.LIN-00076 20151023[ITradeR5]Save the user's blockIDs.(ITRADEFIVE-692)
	public static final String BLOCKIDS  = "blockIDs";
//END   TTL-GZ-CANYONG.LIN-00076 20151023[ITradeR5]Save the user's blockIDs.(ITRADEFIVE-692)

}
