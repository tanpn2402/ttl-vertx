package com.ttl.old.itrade.web.constants;
/**
 * @since  2013-10-28
 * @author clover_he
 * @category : parameter name used for MVCBridgeModel
 */
public final class HKSMVCBridgeModelKey {
	//BEGIN TASK #:TTL-GZ-clover_he-00040.1 2013-10-28 [ITrade5] Clean up code and properties file for the main operation panel of CA
	/**
    * M:CAGenListModel
    * V:MainPage.jsp
    * C:HKSCAGenMainPageAction
    * Module+Functional Description+Model
    */
	public static final String CA_GENLIST_MODEL = "CAGenListModel";
	//END TASK #:TTL-GZ-clover_he-00040.1 2013-10-28 [ITrade5] Clean up code and properties file for the main operation panel of CA
   
	//BEGIN TASK #:TTL-GZ-clover_he-00041 2013-10-28 [ITrade5] Query function development for CA
	/**
    * M:CADetailsModel
    * V:DetailsPage.jsp
    * C:HKSCADetailsPageAction
    * Module+Functional Description+Model
    */
	public static final String CA_DETAILS_MODEL = "CADetailsModel";
	//END TASK #:TTL-GZ-clover_he-00041 2013-10-28 [ITrade5] Query function development for CA
	//BEGIN TASK #:TTL-GZ-PENGJM-00149 2013-10-31[ITrade5]login part trim the invalid code
	/**
	 * M:LAGenLoginModel
	 * V:login.jsp
	 * C:HKSLoginAction.java
	 * Module + Functional Description + Model
	 */
	public static final String LA_GENLOGIN_MODEL = "LAGenLoginModel";
	//END TASK #:TTL-GZ-PENGJM-00149 2013-10-31[ITrade5]login part trim the invalid code
	
	//BEGIN TASK #:TTL-GZ-clover_he-00044 2013-11-08 [ITrade5] Reply function development for CA
		/**
		    * M:CAReplyModel
		    * V:reply.jsp
		    * C:HKSCAReplyAction
		    * Module+Functional Description+Model
		    */
		public static final String CA_REPLY_MODEL = "CAReplyModel";
		
		/**
		    * M:CAReplyConfirmModel
		    * V:ReplyConfirm.jsp
		    * C:HKSCAReplyConfirmAction
		    * Module+Functional Description+Model
		    */
		public static final String CA_REPLYCONFIRM_MODEL = "CAReplyConfirmModel";
		
		/**
		    * M:CAReplyResultModel
		    * V:ReplyResult.jsp
		    * C:HKSCAReplyResultAction
		    * Module+Functional Description+Model
		    */
		public static final String CA_REPLYRESULT_MODEL = "CAReplyResultModel";
		//END TASK #:TTL-GZ-clover_he-00044 2013-11-08 [ITrade5] Reply function development for CA
		
		//BEGIN TASK #:TTL-GZ-clover_he-00045 2013-11-08 [ITrade5] Cancel function development for CA
		/**
		    * M:CACancelConfirmModel
		    * V:CancelConfirm.jsp
		    * C:HKSCACancelConfirmAction
		    * Module+Functional Description+Model
		    */
		public static final String CA_CANCELCONFIRM_MODEL = "CACancelConfirmModel";
		
		/**
		    * M:CACancelResultModel
		    * V:CancelResult.jsp
		    * C:HKSCACancelResultAction
		    * Module+Functional Description+Model
		    */
		public static final String CA_CANCELRESULT_MODEL = "CACancelResultModel";
		//END TASK #:TTL-GZ-clover_he-00045 2013-11-08 [ITrade5] Cancel function development for CA
		
		//BEGIN TASK #:TTL-GZ-clover_he-00046 2013-11-08 [ITrade5] Modify function development for CA
		/**
		    * M:CAModifyModel
		    * V:modify.jsp
		    * C:HKSCAModifyAction
		    * Module+Functional Description+Model
		    */
		public static final String CA_MODIFY_MODEL = "CAModifyModel";
		
		/**
		    * M:CAModifyConfirmModel
		    * V:ModifyConfirm.jsp
		    * C:HKSCAModifyConfirmAction
		    * Module+Functional Description+Model
		    */
		public static final String CA_MODIFYCONFIRM_MODEL = "CAModifyConfirmModel";
		
		/**
		    * M:CAModifyResultModel
		    * V:ModifyResult.jsp
		    * C:HKSCAModifyResultAction
		    * Module+Functional Description+Model
		    */
		public static final String CA_MODIFYRESULT_MODEL = "CAModifyResultModel";
		//END TASK #:TTL-GZ-clover_he-00046 2013-11-08 [ITrade5] Modify function development for CA
		
		//BEGIN TASK #:TTL-GZ-liyuekun-000020 2013-11-12 [ITrade5] Modify function development for IPO and enterOrder
		/**
		    * M:IPOGenInfoModel
		    * info.jsp
		    * C:HKSIPOGenInfoAction
		    * Module+Functional Description+Model
		    */
		public static final String IPO_GENINFO_MODEL = "IPOGenInfoModel";
		
		
		/**
		    * M:IPOStatusModel
		    * V:statusEnquiry.jsp
		    * C:HKSIPOApplStatusEnquiryAction
		    * Module+Functional Description+Model
		    */
		public static final String IPO_STATUS_MODEL = "IPOStatusModel";
		
		
		/**
		    * M:IPOGenRecordCheckModel
		    * V:record.jsp
		    * C:HKSIPOGenRecordCheckAction
		    * Module+Functional Description+Model
		    */
		public static final String IPO_GENRECORDCHECK_MODEL = "IPOGenRecordCheckModel";
		
		/**
		    * M:IPOGenDetail
		    * V:details.jsp
		    * C:HKSIPOGenDetailAction
		    * Module+Functional Description+Model
		    */
		public static final String IPO_GENDETAIL_MODEL = "IPOGenDetailModel";
		
		/**
		    * M:IPOGenTerms
		    * V:terms.jsp
		    * C:HKSIPOGenDetailAction
		    * Module+Functional Description+Model
		    */
		public static final String IPO_GENTERMS_MODEL = "IPOGenTermsModel";
		
		/**
		    * M:IPOGenApply
		    * V:apply.jsp
		    * C:HKSIPOGenApplyAction
		    * Module+Functional Description+Model
		    */
		public static final String IPO_GENAPPLY_MODEL = "IPOGenApplyModel";
		/**
		    * M:IPOGenConFirm
		    * V:result.jsp
		    * C:HKSIPOGenConfirmAction
		    * Module+Functional Description+Model
		    */
		public static final String IPO_GENCONFIRM_MODEL = "IPOGenConFirmModel";
		/**
		    * M:IPOGenPreViewModel
		    * V:preview.jsp
		    * C:HKSIPOPreviewAction
		    * Module+Functional Description+Model
		    */
		public static final String IPO_GENPREVIEW_MODEL = "IPOGenPreViewModel";
		/**
		    * M:ENTERORDERFrameModel
		    * V:EnterOrder.jsp
		    * C:HKSGenEnterOrderFrame
		    * Module+Functional Description+Model
		    */
		public static final String ENTERORDER_FRAME_MODEL = "ENTERORDERFrameModel";
		/**
		    * M:ENTERORDERConfirmModel
		    * V:confirm.jsp
		    * C:HKSIPOGenConfirmAction
		    * Module+Functional Description+Model
		    */
		public static final String ENTERORDER_CONFIRM_MODEL = "ENTERORDERConfirmModel";
		/**
		    * M:ENTERORDERFailModel
		    * V:result.jsp
		    * C:HKSEnterOrderFailAction.java
		    * Module+Functional Description+Model
		    */
		public static final String ENTERORDER_RESULT_MODEL = "ENTERORDERResultModel";
		//END TASK #:TTL-GZ-liyuekun-000020 2013-11-12 [ITrade5] Modify function development for IPO enterOrder
		
		//BEGIN TASK #:TTL-GZ-XYW-000061 2013-11-26[ITrade5]login part trim the invalid code
		/**
		 * M:OrderDetailsModel
		 * V:OrderDetails.jsp
		 * C:HKSOrderDetailsEnquiryAction.java
		 * Module + Functional Description + Model
		 */
		public static final String ORDER_DETAILS_MODEL = "OrderDetailsModel";
		//END TASK #:TTL-GZ-XYW-000061 2013-11-26[ITrade5]login part trim the invalid code
		
		//BEGIN TASK #:TTL-GZ-PENGJM-00169 2013-11-28[ITrade5]use the hashTable model save the parameter
		/**
		 * M:DisclaimerModel
		 * V:disclaimer.jsp
		 * C:HKSDisclaimerAction.java
		 * Module + Model
		 */
		public static final String DISCLAIMER_MODEL = "DisclaimerModel";
		
		/**
		 * M:ChangePasswordModel
		 * V:ChangePassword.jsp
		 * C:HKSGenChangePasswordAction.java
		 * Module + Functional Description + Model
		 */
		public static final String CP_GENCHANGEPASSWORD_MODEL = "ChangePasswordModel";
		//END TASK #:TTL-GZ-PENGJM-00169 2013-11-28[ITrade5]use the hashTable model save the parameter
		
		/**
		    * M:AccountBalanceModel
		    * V:AccountBalance.jsp
		    * C:HKSPortfolioEnquiryAction.java
		    * Module+Functional Description+Model
		    */
		public static final String PORTFOLIO_ACCOUNTBALANCE_MODEL = "AccountBalanceModel";
		
		/**
		    * M:PortfolioSummaryModel
		    * V:PortfolioSummary.jsp
		    * C:HKSPortfolioEnquiryAction.java
		    * Module+Functional Description+Model
		    */
		public static final String PORTFOLIO_SUMMARY = "PortfolioSummaryModel";
		
//BEGIN TASK #:TTL-GZ-clover_he-00094 2014-02-26 [ITrade5] Model key for modify order.[ITRADEFIVE-129]
		/**
	    * M:ModifyOrderModel
	    * V:ModifyOrder.jsp
	    * C:HKSGenModifyOrder.java
	    * Module+Functional Description+Model
	    */
		public static final String MODIFY_ORDER_MODEL = "ModifyOrderModel";
		
		/**
	    * M:ModifyOrderResultModel
	    * V:ModifyOrderResult.jsp
	    * C:HKSModifyOrderResultAction.java
	    * Module+Functional Description+Model
	    */
		public static final String MODIFY_ORDER_RESULT_MODEL = "ModifyOrderResultModel";
//BEGIN TASK #:TTL-GZ-PENGJM-00325 20141121[ITradeR5]Modify Order screen closed when modify order failed(ITRADEFIVE-416)		
      /**
        * M:ModifyOrderResultModel
        * V:ModifyOrderResult.jsp
        * C:HKSModifyOrderResultAction.java
        * Module+Functional Description+Model
        */
	
		public static final String GEN_MODIFY_ORDER_RESULT_MODEL = "GenModifyOrderModel";
//END TASK #:TTL-GZ-PENGJM-00325 20141121[ITradeR5]Modify Order screen closed when modify order failed(ITRADEFIVE-416)
//END TASK #:TTL-GZ-clover_he-00094 2014-02-26 [ITrade5] Model key for modify order.[ITRADEFIVE-129]
		
//BEGIN TASK #:TTL-GZ-clover_he-00093 2014-03-03 [ITrade5] Action Layer：Code optimization for Cancel Order.[ITRADEFIVE-133]
		/**
	    * M:CancelOrderModel
	    * V:CancelOrder.jsp
	    * C:HKSGenCancelOrder.java
	    * Module+Functional Description+Model
	    */
		public static final String CANCEL_ORDER_MODEL = "CancelOrderModel";
		
		/**
	    * M:CancelOrderResultModel
	    * V:CancelOrderResult.jsp
	    * C:HKSCancelOrderResultAction.java
	    * Module+Functional Description+Model
	    */
		public static final String CANCEL_ORDER_RESULT_MODEL = "CancelOrderResultModel";
			
//END TASK #:TTL-GZ-clover_he-00093 2014-03-03 [ITrade5] Action Layer：Code optimization for Cancel Order.[ITRADEFIVE-133]
		
		/**
	    * M:RequestStatementFrameModel
	    * V:HKSRSEPINT021.jsp
	    * C:HKSRequestStatement.java
	    * Module+Functional Description+Model
	    */
		public static final String REQUESTSTATEMENT_FRAME_MODEL = "RequestStatementFrameModel";
		
		/**
		 * M:detectModel
		 * V:ITrade.detect.jsp
		 * C:HKSLoginAction.java
		 * Module+Functional Description+Model
		 */
		public static final String DETECT_MODEL = "detectModel";
		
//BEGIN TASK # :TTL-GZ-kelly.kuang-00016 20151228[ITradeR5]AAStock teletext model.(ITRADEFIVE-701)
		/**
		 * M:DataFeedTeletextModel
		 * V:ITrade.teletext.jsp
		 * C:HKSGenTeleTextAction.java
		 * Module+Functional Description+Model
		 */
		public static final String DATA_FEED_TELETEXT_MODEL = "DataFeedTeletextModel";
//END TASK # :TTL-GZ-kelly.kuang-00016 20151228[ITradeR5]AAStock teletext model.(ITRADEFIVE-701)

//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
		/**
         * M:IdentityValidateModel
         * V:identityValidation.jsp
         * C:HKSIdentityValidationAction.java
         * Module+Functional Description+Model
         */
		public static final String IDENTITY_VALIDATION_MODEL = "IdentityValidateModel";
//END TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
		
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00031 2016-01-18[ITradeR5-CISI]Add Risk Disclosure.(ITRADEFIVE-763)
        /**
         * RiskDisclosureModel
         * V:riskDisclosure.jsp
         * C:HKSRiskDisclosureAction.java
         * Module+Functional Description+Model
         */
        public static final String RISK_DISCLOSURE_MODEL = "RiskDisclosureModel";
//END TASK #:TTL-GZ-STEVENZG.LI-00031 2016-01-18[ITradeR5-CISI]Add Risk Disclosure.(ITRADEFIVE-763)
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00088 2017-01-16[ITradeR5]Add "Register UserName" Function.
        /**
         * RegisterUserNameModel
         * V:registerUserName.jsp
         * C:HKSRegisterUserNameAction.java
         * Module+Functional Description+Model
         */
        public static final String REGISTER_USERNAME_MODEL = "RegisterUserNameModel";
//END TASK #:TTL-GZ-STEVENZG.LI-00088 2017-01-16[ITradeR5]Add "Register UserName" Function.
}
