package com.ttl.old.itrade.web.ticker;

import com.ttl.old.itrade.ITradeAtmosphereAPI;
import com.ttl.old.itrade.biz.model.ITradeUser;
import com.ttl.old.itrade.util.Log;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class TickerViewProccessor {
    private ScheduledExecutorService scheduledThreadPool;
    private ITradeUser mvUser = null;
    private ScheduledFuture<?> lvFuture = null;
    private final int INTERVAL = 60;
    private final int INIT_DELAY = 0;
    private boolean isStarted = false;

    public TickerViewProccessor(ITradeUser itradeUser) {
        this.mvUser = itradeUser;
        scheduledThreadPool = new ScheduledThreadPoolExecutor(1, new ThreadFactory() {
            private final AtomicInteger count = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable r) {
                Thread lvThread = new Thread(r, "Ticker-Data-Processer " + count.incrementAndGet());
                lvThread.setDaemon(true);
                return lvThread;
            }
        }, new RejectedExecutionHandler() {

            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                System.err.println("TASK: " + r + ", rejected to excute by: " + executor);
            }
        });
    }

    public void start() {
        isStarted = true;
        try {
            lvFuture = scheduledThreadPool.scheduleAtFixedRate(new Task(), INIT_DELAY, INTERVAL, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public boolean isStarted() {
        return this.isStarted;
    }

    public void stop() {
        isStarted = false;
        scheduledThreadPool.shutdownNow();
        try {
            if (!scheduledThreadPool.awaitTermination(60, TimeUnit.SECONDS))
                Log.println("Pool used for TickerViewProccessor did not terminate", Log.ERROR_LOG);
        } catch (InterruptedException e) {
            scheduledThreadPool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    private class Task implements Runnable {

        @Override
        public void run() {
            try {
                push();
            } catch (Exception e) {
                Log.println("Error occured when executing the push operation, details: " + e, Log.ERROR_LOG);
            }
        }
    }

    public void push() {
        Object jsonMap = new Object();
        if (ITradeAtmosphereAPI.getInstance().retrieve(mvUser.getMvAccountID()) != null) {
            ITradeAtmosphereAPI.getInstance().pushTopic(mvUser.getMvAccountID(), "STOCK_INFO", jsonMap);
        }
    }

    public void cancelTask() {
        isStarted = false;
        lvFuture.cancel(true);
    }
}
