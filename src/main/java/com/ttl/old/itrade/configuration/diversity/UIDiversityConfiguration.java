/**
 * 
 *//*
package com.itrade.configuration.diversity;

import com.itrade.base.data.ref.HashMapDataPadding;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140709 by xuejie.xiao</p>
 * @author jay.wince
 * @Date    20140709
 * @version 1.0
 *//*
public final class UIDiversityConfiguration extends AbstractDiversity<UIDiversityConfiguration> implements IUIDiversity{

	public UIDiversityConfiguration(String pSID) {
		super(pSID);
	}

	 (non-Javadoc)
	 * @see com.itrade.configuration.diversity.nex.AbstractDiversity#setHandler(com.itrade.configuration.diversity.nex.AbstractDiversity)
	 
	@Override
	public void setHandler(UIDiversityConfiguration handler) {
         this.handler = handler;
	}

	 (non-Javadoc)
	 * @see com.itrade.configuration.diversity.nex.AbstractDiversity#getPath()
	 
	@Override
	protected String getPath() {
//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]use getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
		return WEB_INF_FOLDER+"/config/ui/"+this.mvSID+getPathSuffix();
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]use getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	}
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]add getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	*//**
     * get the config path suffix.
     * @author kelly.kuang
     * @since  20151016
     * @param config path suffix
     *//*
    @Override
    protected String getPathSuffix() {
		return CONFIG_PATH_SUFFIX;
	}
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]add getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	
    @Override
	public Object id() {
		return this.mvSID+"4UI";
	}
//BEGIN TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]UI new configuration (ITRADEFIVE-211)	
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> pagingStyle(String pModule) {
		return invokeMethod0("fallbackDefault", "/paging-style",new String[]{IDiversityConfiguration.ENTITIES,pModule+"/"});
	}
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> orderColumns() {
		String xpath = DOUBLE_LEADING_SLASH + "HKSOEEPINT002" + DOUBLE_LEADING_SLASH + "columns";
		return pathResolve(xpath);
	}
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	*//**
	 * @author kelly.kuang
	 * @since  20151120
	 * 
	 *//*
	@Override
	public String orderSettingRawColumns() {
		String xpath = DOUBLE_LEADING_SLASH + "HKSOEEPINT002" + DOUBLE_LEADING_SLASH + "tableic";
		return pathResolve(xpath).get("setting-raw-columns");
	}
	*//**
	 * @author kelly.kuang
	 * @since  20151120
	 * 
	 *//*
	@Override
	public String orderDefaultRawColumns() {
		String xpath = DOUBLE_LEADING_SLASH + "HKSOEEPINT002" + DOUBLE_LEADING_SLASH + "tableic";
		return pathResolve(xpath).get("default-raw-columns");
	}
//END TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getTypeSet() {
		String xpath = DOUBLE_LEADING_SLASH + "HKSTHEPINT0020" + DOUBLE_LEADING_SLASH + "type-set";
		return pathResolve(xpath);
	}
//END TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]UI new configuration (ITRADEFIVE-211)	
//BEGIN TASK #:TTL-GZ-BO.LI-00023 20140726[ITradeR5]UI configuration (ITRADEFIVE-211)
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> themePadding() {
		return pathResolve("theme");
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockIndice() {
		String xpath = DOUBLE_LEADING_SLASH + "AAStock-indices/HKSAAIEPINT015" + DOUBLE_LEADING_SLASH + "columns";
		return pathResolve(xpath);
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockFavorite() {
		String xpath = DOUBLE_LEADING_SLASH + "AAStock-favorite/HKSAAFEPINT016" + DOUBLE_LEADING_SLASH + "columns";
		return pathResolve(xpath);
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockNews() {
		String xpath = DOUBLE_LEADING_SLASH + "AAstock-News" + DOUBLE_LEADING_SLASH + "columns";
		return pathResolve(xpath);
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getPriceAlertColumns() {
		String xpath = DOUBLE_LEADING_SLASH + "price-alert" + LEADING_SLASH + "HKSPAEPINT010" 
						+ DOUBLE_LEADING_SLASH+ "columns";
		return pathResolve(xpath);
	}
//END TASK #:TTL-GZ-BO.LI-00023 20140726[ITradeR5]UI configuration (ITRADEFIVE-211)
	
//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> common(){
		String xpath = DOUBLE_LEADING_SLASH + "common";
		return pathResolve(xpath);
	}
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> orderhistoryCloumns(){
		String xpath = DOUBLE_LEADING_SLASH + "HKSOHEPINT0021" + DOUBLE_LEADING_SLASH + "columns";
		return pathResolve(xpath);
	}

	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> accountBalanceCloumns(){
		String xpath = DOUBLE_LEADING_SLASH + "account-balance/HKSABEPINT003" + DOUBLE_LEADING_SLASH +  "columns";
		return pathResolve(xpath);
	}
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> groupSupported(String pModule){
		return invokeMethod0("fallbackDefault", "/supported",new String[]{IDiversityConfiguration.ENTITIES,pModule+"/"});
	}
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public HashMapDataPadding<String> pagesize(String pModule){
		return invokeMethod0("fallbackDefault", "/page-size",new String[]{IDiversityConfiguration.ENTITIES,pModule+"/"});
	}
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public String displayMode(String nodeName){
		return fallbackDefaultString(mvDocument,nodeName,new String[]{"ITrade","/display-mode/"});
	}
//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author bo.li
	 * @since  20140823
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> AAStockIndiceShowGroup() {
		String xpath = DOUBLE_LEADING_SLASH + "AAStock-indices/HKSAAIEPINT015"+ DOUBLE_LEADING_SLASH + "show-group";
		return pathResolve(xpath);
	}
//BEGIN TASK #:TTL-GZ-BO.LI-00039 2014-09-10[ITradeR5]TickerView group(ITRADEFIVE-250)
	*//**
	 * @author bo.li
	 * @since  20140910
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> TickerViewShowGroup() {
		String xpath = DOUBLE_LEADING_SLASH + "ticker-view"+ DOUBLE_LEADING_SLASH + "show-group";
		return pathResolve(xpath);
	}
//END TASK #:TTL-GZ-BO.LI-00039 2014-09-10[ITradeR5]TickerView group(ITRADEFIVE-250)
	*//**
	 * @author bo.li
	 * @since  20140920
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> FavoriteStockSearch() {
		String xpath = DOUBLE_LEADING_SLASH + "AAStock-favorite/HKSAAFEPINT016"+ DOUBLE_LEADING_SLASH + "search";
		return pathResolve(xpath);
	}
	
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> aastockNewsPadding() {
		String xpath = DOUBLE_LEADING_SLASH + "AAstock-News/HKSAANEPINT017" + LEADING_SLASH + "aastock-news";
		return pathResolve(xpath);
	}
	*//**
	 * @author bo.li
	 * @since  20141008
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> potfolioFilter() {
		String xpath = DOUBLE_LEADING_SLASH + "portfolios/HKSPFEPINT005";
		return pathResolve(xpath);
	}
	*//**
	 * @author bo.li
	 * @since  20141008
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> BankAccountBalanceFilter() {
		String xpath = DOUBLE_LEADING_SLASH + "portfolios/HKSPFEPINT005";
		return pathResolve(xpath);
	}

	*//**
	 * @author piece.lin
	 * @since  20141119
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> tableFieldSorting(String pModel) {
		
		String xpath = DOUBLE_LEADING_SLASH +pModel+LEADING_SLASH+"tableic";
		return pathResolve(xpath);
	}

	*//**
	 * @author piece.lin
	 * @since  20141119
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> settingsDataPadding(final String pModule) {
		String xpath = "settings/preference/"+pModule;
		return pathResolve(xpath);
	}
	*//**
	 * @author bo.li
	 * @since  20141125
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getFundManagementPadding(String pModel) {
		String xpath = DOUBLE_LEADING_SLASH +"fund-management" +LEADING_SLASH + pModel;
		return pathResolve(xpath);
	}

    @Override
    public HashMapDataPadding<String> getSortable(String pModule)
    {
        String xpath = DOUBLE_LEADING_SLASH +pModule+LEADING_SLASH+"tableic"+LEADING_SLASH+"sortable";
        return pathResolve(xpath);
    }
//BEGIN TASK #:TTL-GZ-PENGJM-00326.7 20150121[ITradeR5]Change the config to UI(ITRADEFIVE-341)    
    *//**
     * @author xiaobo.zhu
     * @since  20140728
     * 
     *//*
    public HashMapDataPadding<String> tranhistoryCloumns(){
        String xpath = DOUBLE_LEADING_SLASH + "HKSTHEPINT0020" + DOUBLE_LEADING_SLASH + "columns";
        return pathResolve(xpath); 
    }
    *//**
     * @author xiaobo.zhu
     * @since  20140728
     * 
     *//*
    public HashMapDataPadding<String> caCloumns(){
        String xpath = DOUBLE_LEADING_SLASH + "CA/HKSCAEPINT011" + DOUBLE_LEADING_SLASH + "columns";
        return pathResolve(xpath); 
    }
    *//**
     * @author xiaobo.zhu
     * @since  20140728
     * 
     *//*
    public HashMapDataPadding<String> summaryCloumns(){
        String xpath = DOUBLE_LEADING_SLASH + "Summary" + DOUBLE_LEADING_SLASH +  "columns";
        return pathResolve(xpath); 
    }
    *//**
     * @author xiaobo.zhu
     * @since  20140728
     * 
     *//*
    public HashMapDataPadding<String> hottestCloumns(){
        String xpath = DOUBLE_LEADING_SLASH + "IPOHottest" + DOUBLE_LEADING_SLASH + "columns";
        return pathResolve(xpath); 
    }
    *//**
     * @author xiaobo.zhu
     * @since  20140728
     * 
     *//*
    public HashMapDataPadding<String> statusCloumns(){
        String xpath = DOUBLE_LEADING_SLASH + "IPOStatus" + DOUBLE_LEADING_SLASH + "columns";
        return pathResolve(xpath); 
    }
    *//**
     * @author xiaobo.zhu
     * @since  20140728
     * 
     *//*
    public HashMapDataPadding<String> listFocusCloumns(){
        String xpath = DOUBLE_LEADING_SLASH + "IPOListedFocus" + DOUBLE_LEADING_SLASH + "columns";
        return pathResolve(xpath); 
    }
//END TASK #:TTL-GZ-PENGJM-00326.7 20150121[ITradeR5]Change the config to UI(ITRADEFIVE-341)
    *//**
     * @author canyong.lin
     * @since 20150413
     *//*
	@Override
	public String getPrimarykey(String fid) {
		String xpath = DOUBLE_LEADING_SLASH + fid + DOUBLE_LEADING_SLASH + "tableic";
		return pathResolve(xpath).get("primary-key");
	}
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
	*//**
     * @author kelly.kuang
     * @since 20151203
     *//*
	@Override
	public boolean displayTodayValueInGoodTillDateComboBox() {
		String xpath = DOUBLE_LEADING_SLASH + "HKSPOEPINT001";
		return Boolean.TRUE.toString().equals(pathResolve(xpath).get("displayTodayValueInGoodTillDateComboBox")); 
	}
//END TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
	
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)
	*//**
	 * @author stevenzg.li
	 * @Date 2015-12-28
	 *//*
	@Override
	public HashMapDataPadding<String> getResizable(String pModule) {
	    String xpath = DOUBLE_LEADING_SLASH + pModule + LEADING_SLASH + "tableic" + LEADING_SLASH + "resizable";
        return pathResolve(xpath);
	}
//END TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)
}
*/