/**
 * 
 *//*
package com.itrade.configuration.diversity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;

import com.itrade.aastock.EDataFeedSource;
import com.itrade.base.data.DataPadding;
import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.configuration.pattern.PatternContext.EPatternType;
import com.itrade.configuration.pattern.PatternEntry;
import com.itrade.web.engine.util.JSONParse;


*//**
<pre>
  <composed-of>
	    <entries>
	        <entry type="BL" ref="base"/>
	        <entry type="UI" ref="ttl"/>			
	    </entries>
	</composed-of>
</pre>
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140709 by xuejie.xiao</p>
 * @author jay.wince
 * @Date    20140709
 * @version 1.0
 *//*
public class AppConfiguration extends AbstractDiversity<AppConfiguration> implements IDiversityConfiguration {
	private BLDiversityConfiguration mvBL;
	private UIDiversityConfiguration mvUI;
	
	private final String NODE_COMPOSED_OF = "composed-of";
		private final String NODE_ENTRIES     = "entries";
			private final String NODE_ENTRY       = "entry";
	
	
	private final String NODE_RESOURCE    = "resource";
		private final String NODE_JAVASCRIPT  = "javascript";
		private final String NODE_STYLE       = "style";
		private final String NODE_VIEW        = "view";
	
//	private final String ATTR_TYPE = "type";
	private final String ATTR_REF  =  "ref";
	
    private final String BASE = "base";

	public static enum ERef {
		BL,UI
	}
	public AppConfiguration(final String pAppID) {
		super(pAppID);
		init();
	}

	private void init(){
		String lvBLID = getEntryElement(ERef.BL.toString()).attributeValue(ATTR_REF);
		String lvUIID = getEntryElement(ERef.UI.toString()).attributeValue(ATTR_REF);
		
		this.mvBL = new BLDiversityConfiguration(lvBLID);
        if (!BASE.equals(lvBLID)) {
            this.mvBL.setHandler(new BLDiversityConfiguration(BASE));
		}
		this.mvUI = new UIDiversityConfiguration(lvUIID);
        if (!BASE.equals(lvUIID)) {
            this.mvUI.setHandler(new UIDiversityConfiguration(BASE));
		}
	}
	
	private BLDiversityConfiguration getBLConfig() {
		if (this.mvBL == null) {
			throw new NullPointerException("BL configuration instance is null.");
		}
		return mvBL;
	}
	
	private UIDiversityConfiguration getUIConfig() {
		if (this.mvUI == null) {
			throw new NullPointerException("UI configuration instance is null.");
		}
		return mvUI;
	}
	
	private Element getEntryElement(final String type){
		Node lvEntryNode = this.mvDocument.getRootElement().selectSingleNode(NODE_COMPOSED_OF+LEADING_SLASH+NODE_ENTRIES+LEADING_SLASH+NODE_ENTRY+"[@type='"+type+"']");
		return (Element)lvEntryNode;
	}
	
	public enum EResourceType{
		JAVASCRIPT,
		STYLE,
		VIEW
	}
	@Override
	public void setHandler(AppConfiguration handler) {
		this.handler = handler;
	}
	
	@Override
	protected String getPath() {
//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]use getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
		return WEB_INF_FOLDER+"/config/appserv/"+this.mvSID+getPathSuffix();
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]use getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	}
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]add getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	*//**
     * get the config path suffix.
     * @author kelly.kuang
     * @since  20151016
     * @param config path suffix
     *//*
    @Override
    protected String getPathSuffix() {
		return "-app-config.xml";
	}
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]add getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	
    @Override
	public Object id() {
		return this.mvSID+"4App";
	}
	
	public HashMapDataPadding<String> fileSettingsInPreference(){
		return this.pathResolve("preference/file");
	}
	public HashMapDataPadding<String> dashboardPadding() {
		return this.pathResolve("dashboard");
	}
	
	public HashMapDataPadding<String> sessionPadding() {
		return this.pathResolve("session");
	}
	public String getResourceRef(EResourceType rt){
		String type = rt.name().toLowerCase();
		HashMapDataPadding<String> dataPadding = this.pathResolve(NODE_RESOURCE+LEADING_SLASH+type);
// SSOL-AttrParse		
		
	    Element typeElement = parseTextToElement(dataPadding, type);
	    String ref = null;
	    if (typeElement != null) {
	    	ref = typeElement.attributeValue(ATTR_REF);
		}		
		if (ref==null) {
			ref = "";//Use base instead.
		}
		
// ESOL-AttrParse
		String ref = dataPadding.get(ATTR_REF);
//@Jay on 20140819: Return the null value directly if the node is not present!	
		return ref;		
	}

	@Override
	*//**
	 * @author jay.wince
	 * @since  20140725
	 *//*
	public HashMapDataPadding<String> themePadding() {
		return getUIConfig().themePadding();
	}

	@Override
	*//**
	 * @author jay.wince
	 * @since  20140725
	 *//*
	public HashMapDataPadding<String> themeOptions() {
		return getBLConfig().themeOptions();
	}
//BEGIN TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]BL&UI new configuration
	*//**
	 * @author pengjm
	 * @since  20140725
	 *//*
	public HashMapDataPadding<String> getAAStockThumbnailStockchartImageView() {
		return getBLConfig().getAAStockThumbnailStockchartImageView();
	}
	*//**
	 * @author pengjm
	 * @since  20140725
	 *//*
	public HashMapDataPadding<String> pagingStyle(String pModule) {
		return getUIConfig().pagingStyle(pModule);
	}

	*//**
	 * @author pengjm
	 * @since  20140725
	 * @description: option/module/login
	 * 
	 *//*
	public HashMapDataPadding<String> loginOptions() {
		return getBLConfig().loginOptions();
	}

	*//**
	 * @author pengjm
	 * @since  20140725
	 * @description: ITrade/module/login
	 * 
	 *//*
	public HashMapDataPadding<String> login() {
		return getBLConfig().login();
	}

	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> orderColumns() {
		return getUIConfig().orderColumns();
	}
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	*//**
	 * @author kelly.kuang
	 * @since  20151120
	 * 
	 *//*
	@Override
	public String orderSettingRawColumns() {
		return getUIConfig().orderSettingRawColumns();
	}
	
	*//**
	 * @author kelly.kuang
	 * @since  20151120
	 * 
	 *//*
	@Override
	public String orderDefaultRawColumns() {
		return getUIConfig().orderDefaultRawColumns();
	}
//END TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getTypeSet() {
		return getUIConfig().getTypeSet();
	}
//END TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]BL&UI new configuration
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockIndice() {
		return getUIConfig().getAAStockIndice();
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockFavorite() {
		return getUIConfig().getAAStockFavorite();
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> showAAStockIndice() {
		return  getBLConfig().showAAStockIndice();
	}
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockNews() {
		return  getUIConfig().getAAStockNews();
	}	
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getPriceAlertColumns() {
		return  getUIConfig().getPriceAlertColumns();
	}
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> common(){
		return getUIConfig().common();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> messageOptions(){
		return getBLConfig().messageOptions();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> ctOptions(){
		return getBLConfig().ctOptions();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140728[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> currency(){
		return getBLConfig().currency();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> market(){
		return getBLConfig().market();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> regex(){
		return getBLConfig().regex();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> channel() {
		return getBLConfig().channel();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> defaultValue(){
		return getBLConfig().defaultValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> enterOrderValue(){
		return getBLConfig().enterOrderValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> goodTillDate(){
		return getBLConfig().goodTillDate();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> modifyOrder(){
		return getBLConfig().modifyOrder();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> cancelOrder(){
		return getBLConfig().cancelOrder();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> orderMapping(){
		return getBLConfig().orderMapping();//junming.peng on 20150121:Change to BL config
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> orderhistoryCloumns(){
		return getUIConfig().orderhistoryCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> historyMapping(){
		return getBLConfig().historyMapping();//junming.peng on 20150121:Change to BL config
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> historyValue(){
		return getBLConfig().historyValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> tranhistoryCloumns(){
		return getUIConfig().tranhistoryCloumns();//junming.peng on 20150121:Change to UI config
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> tranhistoryValue(){
		return getBLConfig().tranhistoryValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> summaryValue(){
		return getBLConfig().summaryValue();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> summaryCloumns(){
		return getUIConfig().summaryCloumns();//junming.peng on 20150121:Change to UI config
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> accountBalanceCloumns(){
		return getUIConfig().accountBalanceCloumns();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> orderdetail(){
		return getBLConfig().orderdetail();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> IPOModule(){
		return getBLConfig().IPOModule();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> ipoPasswordConfirm(){
		return getBLConfig().ipoPasswordConfirm();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> hottestCloumns(){
		return getUIConfig().hottestCloumns();//junming.peng on 20150121:Change to UI config
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> statusCloumns(){
		return getUIConfig().statusCloumns();//junming.peng on 20150121:Change to UI config
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> listFocusCloumns(){
		return getUIConfig().listFocusCloumns();//junming.peng on 20150121:Change to UI config
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> caPasswordConfirm(){
		return getBLConfig().caPasswordConfirm();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> caCloumns(){
		return getUIConfig().caCloumns();//junming.peng on 20150121:Change to UI config
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> changePassword(){
		return getBLConfig().changePassword();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
//	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
//	/**
//	 * @author xiaobo.zhu
//	 * @since  20140729
//	 * 
//	 
//	@Override
//	public  HashMapDataPadding<String> storeOptions(){
//		return getBLConfig().storeOptions();
//	}
//	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> getAAStockSettings(){
		return getBLConfig().getAAStockSettings();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> getAAStockSnapshotDataFeedOptions(){
		return getBLConfig().getAAStockSnapshotDataFeedOptions();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> getAAStockSnapshotParas(){
		return getBLConfig().getAAStockSnapshotParas();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> passwordEncrypt(String pModule){
		return getBLConfig().passwordEncrypt(pModule);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> groupSupported(String pModule){
		return getUIConfig().groupSupported(pModule);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  HashMapDataPadding<String> pagesize(String pModule){
		return getUIConfig().pagesize(pModule);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  String displayMode(String nodeName){
		return getUIConfig().displayMode(nodeName);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]UI configuration (ITRADEFIVE-220)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  PatternEntry getPatternEntry(EPatternType type){
		return getBLConfig().getPatternEntry(type);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140729
	 * 
	 *//*
	@Override
	public  String reject(){
		return getBLConfig().reject();
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140729[ITradeR5]BL configuration 	(ITRADEFIVE-221)
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> enterOrderStockIdHandle(){
		return getBLConfig().enterOrderStockIdHandle();
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> dataFeedProductionSettings() {
		return getBLConfig().dataFeedProductionSettings();
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> dataFeedUATSettings() {
		return getBLConfig().dataFeedUATSettings();
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public String getdataFeedUATSettings(final EDataFeedSource source,final String leaf) {
		return getBLConfig().getdataFeedUATSettings(source, leaf);
	}
	*//**
	 * @author bo.li
	 * @since  20140818
	 * 
	 *//*
	@Override
	public String getAAStockURISettings(final EDataFeedSource source,final String leaf) {
		return getBLConfig().getAAStockURISettings(source, leaf);
	}
//	BEGIN TASK #:TTL-GZ-BO.LI-00030 20140824[ITradeR5]Indices group(ITRADEFIVE-237)
	*//**
	 * @author bo.li
	 * @since  20140823
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> AAStockIndiceShowGroup() {
		return getUIConfig().AAStockIndiceShowGroup();
	}
//	BEGIN TASK #:TTL-GZ-BO.LI-00030 20140824[ITradeR5]Indices group(ITRADEFIVE-237)
	*//**
	 * @author bo.li
	 * @since  20140910
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> TickerViewShowGroup() {
		return getUIConfig().TickerViewShowGroup();
	}

	*//**
	 * @author canyong.lin
	 * @since 20140911
	 *//*
	@Override
	public HashMapDataPadding<String> aastockNewsPadding() {
		return getUIConfig().aastockNewsPadding();
	}
	*//**
	 * @author bo.li
	 * @since  20140920
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> FavoriteStockSearch() {
		return getUIConfig().FavoriteStockSearch();
	}
	*//**
	 *@author canyong.lin
	 *@since 20140922
	 *//*
	@SuppressWarnings("unchecked")
	@Override
	public HashMapDataPadding<List<String>> atmosphereTopic() {
		HashMapDataPadding<List<String>> topicMap = new HashMapDataPadding<List<String>>();
		List<String> topicList = new ArrayList<String>();
		String lvXpath = "StockInfoPublisher/topics/topic[@enable='true']";
		
		List<Element> lvTtlTopics = this.mvDocument.getRootElement().selectNodes(lvXpath);
		for(int i = 0;i<lvTtlTopics.size();i++){
			topicList.add(lvTtlTopics.get(i).attributeValue("id"));
		}
		
		List<Element> lvBaseTopics = this.handler.mvDocument.getRootElement().selectNodes(lvXpath);
		for(int i = 0;i<lvBaseTopics.size();i++){
			topicList.add(lvBaseTopics.get(i).attributeValue("id"));
		}
		
		lvXpath = "StockInfoPublisher/topics/topic[@enable='false']";
		lvTtlTopics = this.mvDocument.getRootElement().selectNodes(lvXpath);
		for(int i = 0;i<lvTtlTopics.size();i++){
			if(topicList.contains(lvTtlTopics.get(i).attributeValue("id"))){
				topicList.remove(lvTtlTopics.get(i).attributeValue("id"));
			}
		}
		topicMap.put("topics", topicList);
		return topicMap;
	}
//BEGIN TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]
//BEGIN TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]	
	@Override
	public DataPadding<String> getAAStockMarketInfoLoginSettings(String pMarketID) {
		return getBLConfig().getAAStockMarketInfoLoginSettings(pMarketID);
	}
//END TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
//END   TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]	

	*//**
	 *@author junming.peng
	 *@since 20141016
	 *//*
	@Override
	public HashMapDataPadding<String> placeOrderQuoteEnable() {
		return getBLConfig().placeOrderQuoteEnable();
	}

	*//**
	 * author jay.wince
	 * @since 20141022
	 *//*
    @Override
    public HashMapDataPadding<String> MDSConnectionSettings()
    {
        return this.pathResolve("mds/client/connection");
    }
	*//**
	 * @author bo.li
	 * @since  20141008
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> supportMargin() {
		return getBLConfig().supportMargin();
	}
	*//**
	 * @author bo.li
	 * @since  20141111
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> potfolioFilter() {
		return getUIConfig().potfolioFilter();
	}
	*//**
	 * @author bo.li
	 * @since  20141111
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> BankAccountBalanceFilter() {
		return getUIConfig().BankAccountBalanceFilter();
	}

	*//**
     * @author junming.peng
     * @since  20141108
     * 
     *//*
    @Override
    public int getTradingMainDayTradeRecord()
    {
        return getBLConfig().getTradingMainDayTradeRecord();
    }

    *//**
     * @author piece.lin	
     * @since  20141119
     * 
     *//*
	@Override
	public HashMapDataPadding<String> tableFieldSorting(String pModel) {
		return getUIConfig().tableFieldSorting(pModel);
	}

	*//**
     * @author piece.lin	
     * @since  20141119
     * 
     *//*
	@Override
	public HashMapDataPadding<String> settingsDataPadding(final String pModule) {
		return getUIConfig().settingsDataPadding(pModule);
	}
	*//**
     * @author bo.li
     * @since  20141125
     * 
     *//*

	@Override
	public HashMapDataPadding<String> getFundManagementPadding(String pModel) {
		return getUIConfig().getFundManagementPadding(pModel);
	}
	
	*//**
	 * @author canyong.lin
	 * @since 20141201
	 *//*
	@Override
	public HashMapDataPadding<String> getTokenConfig(String fid) {
		return getBLConfig().getTokenConfig(fid);
	}
	
	*//**
     * @author junming.peng
     * @since 20141208
     *//*
    @Override
    public HashMapDataPadding<String> allowModifyOrder()
    {
        return getBLConfig().allowModifyOrder();
    }
    *//**
     * @author junming.peng
     * @since 20141208
     *//*
    @Override
    public HashMapDataPadding<String> nonAllowModifyOrder()
    {
        return getBLConfig().nonAllowModifyOrder();
    }

    @Override
    public HashMapDataPadding<String> allowCancelOrder()
    {
        return getBLConfig().allowCancelOrder();
    }
    *//**
     * @author junming.peng
     * @since 20141215
     *//*
    @Override
    public HashMapDataPadding<String> getSortable(String pModule)
    {
        return getUIConfig().getSortable(pModule);
    }
    *//**
	 * @author bo.li
	 * @since  20141224
	 * 
	 *//*
	@Override
	public String getAAStockHostDataPadding(EDataFeedSource source,String leaf) {
		return getBLConfig().getAAStockHostDataPadding(source, leaf);
	}
	*//**
	 * @author bo.li
	 * @since  20141224
	 * 
	 *//*
	@Override
	public String getAAStockURIDataPadding(EDataFeedSource source,String leaf) {
		return getBLConfig().getAAStockURIDataPadding(source, leaf);
	}

    *//**
     * @author jay.wince
     * @since  20141211
     *//*
    @Override
    public HashMapDataPadding<String> findFunctionWidgetById(String id)
    {
       return getWidgetDataPadding(id);
    }

    private HashMapDataPadding<String> getWidgetDataPadding(String id)
    {
        String xpath = "dashboard/predefined-function-widgets/widget[@id='"+id+"']";
        final String URL        = "url";
        final String OPTION     = "toolOptions";
        final String DEFAULT    = "default";
        HashMapDataPadding<String> lvWidgetMap = getdataPadding(xpath);
        HashMapDataPadding<String> lvWidgetMapBase = this.handler.getdataPadding(xpath);
// Merge impl
        if (lvWidgetMap.original().size()==0)
        {
            String lvUrlXML = lvWidgetMapBase.get(URL);
            if (lvUrlXML!=null)//As default widget maybe have no url settings.
            {
                Document document = parseText(lvUrlXML);
                HashMapDataPadding<String> map = elementToMap(document.getRootElement());
                lvWidgetMapBase.put(URL, JSONParse.writeJSON(map.original()));
            }
            String lvOptionXML = lvWidgetMapBase.get(OPTION);
            if (lvOptionXML!=null)
            {
                Document document = parseText(lvOptionXML);
                HashMapDataPadding<String> map = elementToMap(document.getRootElement());
                lvWidgetMapBase.put(OPTION, JSONParse.writeJSON(map.original()));
            }
        }else {
            Set<Entry<String, String>>  lvBaseEntries = lvWidgetMapBase.original().entrySet();
            for (Entry<String, String> entry : lvBaseEntries)
            {
                String key = entry.getKey();
                if (lvWidgetMap.getOptionalValue(key)!=null)
                {
                    String lvOverrideValue = lvWidgetMap.get(key);
                    if (URL.equals(key))
                    {
                        lvOverrideValue = mergeByJSON(entry.getValue(), lvOverrideValue);
                    }
                    if (OPTION.equals(key))
                    {
                        lvOverrideValue = mergeByJSON(entry.getValue(), lvOverrideValue);
                    }
                    lvWidgetMapBase.put(key, lvOverrideValue);
                }
            }
        }
        if (DEFAULT.equals(id))
        {
            return lvWidgetMapBase;
        }
        return merge(getWidgetDataPadding(DEFAULT), lvWidgetMapBase) ;
    }
	@Override
    *//**
     * Pending to implement if required.
     * @author jay.wince
     * @since  20141223
     * @return
     *//*
	public HashMapDataPadding<String> defaultDashboardDefinition() {
//		Dom4jXmlUtil.transform(sourceElement, target);
		return null;
	}

	@Override
	*//**
     * @author canyong.lin
     * @since 20150116
     *//*
	public HashMapDataPadding<String> actionMapping(String actionID,String param) 
	{
		return getBLConfig().actionMapping(actionID, param);
	}

	@Override
	*//**
     * @author canyong.lin
     * @since 20150116
     *//*
	public HashMapDataPadding<String> functionDefinition(String fid) {
		return getBLConfig().functionDefinition(fid);
	}

	@Override
	*//**
	 * @author canyong.lin
	 * @since 20150127
	 *//*
	public HashMapDataPadding<String> getTokenUsage(String fid) {
		return getBLConfig().getTokenUsage(fid);
	}

	@Override
	*//**
	 * @author canyong.lin
	 * @since 20150128
	 *//*
	public HashMapDataPadding<String> getFunctionGroup(String fid) {
		return getBLConfig().getFunctionGroup(fid);
	}

	@Override
	*//**
	 * @author canyong.lin
	 * @since 20150129
	 *//*
	public HashMapDataPadding<String> getCSRFConfig() {
		return this.pathResolve("csrf");
	}
	*//**
     * @author junming.peng
     * @since 20150210
     *//*
    @Override
    public String getTradingSituation()
    {
        return getBLConfig().getTradingSituation();
    }
    *//**
     * @author kelly.kuang
     * @since 20170411
     *//*
    @Override
    public boolean getTradingSameSessionID()
    {
        return getBLConfig().getTradingSameSessionID();
    }
	*//**
     * @author junming.peng
     * @since 20150331
     *//*
    @Override
    public String getDNTopicName()
    {
        return getBLConfig().getDNTopicName();
    }
    *//**
     * @author canyong.lin
     * @since 20150413
     *//*
	@Override
	public String getPrimarykey(String fid) {
		return getUIConfig().getPrimarykey(fid);
	}

     
     * @Author junming.peng 
     * @Date 2015-06-09 13:58:33
     * (non-Javadoc)	
     * @see com.itrade.configuration.diversity.IBLDiversity#getHistoryFunctionTab()
     
    @Override
    public String getHistoryFunctionTab()
    {
        return getBLConfig().getHistoryFunctionTab();
    }

     
     * @Author junming.peng
     * @Date 2015-07-03 10:39:30
     * (non-Javadoc)	
     * @see com.itrade.configuration.diversity.IBLDiversity#getMarketOrderType(java.lang.String)
     
    @Override
    public String getMarketOrderType(String pMarket)
    {
        return getBLConfig().getMarketOrderType(pMarket);
    }
    
     * @Author bo.li
     * @Date 2015-07-23 17:16:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getFilledEnquiryStatus()
     
    @Override
    public String getFilledEnquiryStatus()
    {
        return getBLConfig().getFilledEnquiryStatus();
    }
     
     * @Author demonwx.gu
     * @Date 2015-07-28 10:39:30
     * (non-Javadoc)    
     * @see com.itrade.configuration.diversity.IBLDiversity#getSpecialClient(java.lang.String)
     
    @Override
    public String getSpecialClient(String pFlag)
    {
        return getBLConfig().getSpecialClient(pFlag);
    } 
    
     * @Author jummyjw.liu
     * @Date 2015-08-19 10:39:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IDiversityConfiguration#getSupportBrowser()
     
    @SuppressWarnings("unchecked")
    @Override
    public HashMapDataPadding<List<String>> getSupportBrowser() {
        HashMapDataPadding<List<String>> supportBrowser = new HashMapDataPadding<List<String>>();
        String lvXpath = "support-browser/type[@enable='true']";
        
        List<Element> lvTtlSupport = this.mvDocument.getRootElement().selectNodes(lvXpath);
        for(int i = 0;i<lvTtlSupport.size();i++){
            List<String> lvBrowserInfo = new ArrayList<String>();
            String lvBrowserName = lvTtlSupport.get(i).attribute("name").getText();
            List<Element> element = lvTtlSupport.get(i).elements();
            for (int j = 0; j < element.size(); j++){
                lvBrowserInfo.add(element.get(j).getText());
            }
            supportBrowser.put(lvBrowserName,lvBrowserInfo);
        }
        List<Element> lvBaseSupport = this.handler.mvDocument.getRootElement().selectNodes(lvXpath);
        for(int i = 0;i<lvBaseSupport.size();i++){
            List<String> lvBrowserInfo = new ArrayList<String>();
            String lvBrowserName = lvBaseSupport.get(i).attribute("name").getText();
            List<Element> element = lvBaseSupport.get(i).elements();
            for (int j = 0; j < element.size(); j++){
                lvBrowserInfo.add(element.get(j).getText());
            }
            supportBrowser.put(lvBrowserName,lvBrowserInfo);
        }
        lvXpath = "support-browser/type[@enable='false']";
        lvTtlSupport = this.mvDocument.getRootElement().selectNodes(lvXpath);
        for(int i = 0;i<lvTtlSupport.size();i++){
            if(supportBrowser.containsKey(lvTtlSupport.get(i).attributeValue("name"))){
                supportBrowser.remove(lvTtlSupport.get(i).attributeValue("name"));
            }
        }
        return supportBrowser;
    }
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)
    
     * @Author kelly.kuang
     * @Date 2015-11-09 14:37:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getRecoveryFunctionGroup()
     
    public HashMapDataPadding<String> getRecoveryFunctionGroup(String fid) {
		return getBLConfig().getRecoveryFunctionGroup(fid);
	}
	
    
     * @Author kelly.kuang
     * @Date 2015-11-09 14:37:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getRecoveryFunction()
     
	public HashMapDataPadding<String> getRecoveryFunction(String fgid,String select) {
		return getBLConfig().getRecoveryFunction(fgid,select);
	}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)
	
     * @Author kelly.kuang
     * @Date 2015-11-13 15:01:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getNormalRecoveryFunction()
     
	public HashMapDataPadding<String> getNormalRecoveryFunction(String fid) {
		return getBLConfig().getNormalRecoveryFunction(fid);
	}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)

//BEGIN TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
	
     * @Author kelly.kuang
     * @Date 2015-12-03 15:01:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IUIDiversity#displayTodayValueInGoodTillDateComboBox()
     
	@Override
	public boolean displayTodayValueInGoodTillDateComboBox() {
		return getUIConfig().displayTodayValueInGoodTillDateComboBox(); 
	}
	
	
     * @Author kelly.kuang
     * @Date 2015-12-03 15:01:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#allowGoodTillDate()
     
	@Override
	public boolean allowGoodTillDate() {
		return getBLConfig().allowGoodTillDate(); 
	}
//END TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
	
//BEGIN TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
	
     * @Author kelly.kuang
     * @Date 2015-12-10 15:01:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getMarketQuotaBalance()
     
	@Override
	public HashMapDataPadding<String> getMarketQuotaBalance() {
		return getBLConfig().getMarketQuotaBalance(); 
	}
//END TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
	
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)
	*//**
	 * @author stevenzg.li
	 * @Date 2015-12-28
	 *//*
	@Override
	public HashMapDataPadding<String> getResizable(String pModule) {
	    return getUIConfig().getResizable(pModule);
	}
//END TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)

//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
	*//**
     * @author stevenzg.li
     * @Date 2016-01-07
     *//*
    @Override
    public HashMapDataPadding<String> getIdentityValidationSetting() {
        return getBLConfig().getIdentityValidationSetting();
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)

//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00026 2016-01-08[ITradeR5-CISI]Add CITIC Disclaimer setting.(ITRADEFIVE-742)
    *//**
     * @author stevenzg.li
     * @Date 2016-01-08
     *//*
    @Override
    public HashMapDataPadding<String> getCustomerService() {
        String xpath = DOUBLE_LEADING_SLASH + "customer-service";
        return this.pathResolve(xpath);
    }

    *//**
     * @author stevenzg.li
     * @Date 2016-01-08
     *//*
    @Override
    public HashMapDataPadding<String> getCustomerServicePhoneNumber() {
        String xpath = DOUBLE_LEADING_SLASH + "customer-service" + DOUBLE_LEADING_SLASH + "phone-number";
        return this.pathResolve(xpath);
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00026 2016-01-08[ITradeR5-CISI]Add CITIC Disclaimer setting.(ITRADEFIVE-742)

//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
    
     * @Author kelly.kuang
     * @Date 2016-03-18 16:16:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getPlaceOrderConfirmFlow()
     
    @Override
	public HashMapDataPadding<String> getPlaceOrderConfirmFlow() {
		return getBLConfig().getPlaceOrderConfirmFlow();
	}
    
    
     * @Author kelly.kuang
     * @Date 2016-03-18 16:16:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getModifyOrderConfirmFlow()
     
    @Override
	public HashMapDataPadding<String> getModifyOrderConfirmFlow() {
		return getBLConfig().getModifyOrderConfirmFlow();
	}
    
    
     * @Author kelly.kuang
     * @Date 2016-03-18 16:16:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getCancelOrderConfirmFlow()
     
    @Override
	public HashMapDataPadding<String> getCancelOrderConfirmFlow() {
		return getBLConfig().getCancelOrderConfirmFlow();
	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160405[ITradeR5-CISI]Add default confirm password config.(ITRADEFIVE-787)
    
     * @Author kelly.kuang
     * @Date 2016-04-05 11:04:30
     * (non-Javadoc)
     * @see com.itrade.configuration.diversity.IBLDiversity#getDefaultEnterOrderConfirmFlow()
     
    @Override
	public HashMapDataPadding<String> getDefaultEnterOrderConfirmFlow() {
		return getBLConfig().getDefaultEnterOrderConfirmFlow();
	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Add default confirm password config.(ITRADEFIVE-787)

//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.
    *//**
     * @author STEVENZG.LI
     * @since 2016-07-13
     *//*
    @Override
    public HashMapDataPadding<String> getOpenAccountInfo()
    {
        return getBLConfig().getOpenAccountInfo();
    }
//END TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.

    *//**
     * @author STEVENZG.LI
     * @since 2016-09-08
     *//*
    @Override
    public HashMapDataPadding<String> getRunModeConfigInfo()
    {
        return getBLConfig().getRunModeConfigInfo();
    }

    *//**
     * @author stevenzg.li
     * @since 2017-02-10
     *//*
    @Override
    public HashMapDataPadding<String> language()
    {
        return this.pathResolve("language");
    }

    *//**
     * @author stevenzg.li
     * @since 2017-04-26
     *//*
    @Override
    public HashMapDataPadding<String> getAuthenticationOTPConfig(String pModule)
    {
        return getBLConfig().getAuthenticationOTPConfig(pModule);
    }
}*/