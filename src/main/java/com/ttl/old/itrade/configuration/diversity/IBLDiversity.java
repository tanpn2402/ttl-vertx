/**
 * 
 *//*
package com.itrade.configuration.diversity;

import com.itrade.aastock.EDataFeedSource;
import com.itrade.base.data.DataPadding;
import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.configuration.pattern.PatternContext.EPatternType;
import com.itrade.configuration.pattern.PatternEntry;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 2014-7-17 by xuejie.xiao</p>
 * @author jay.wince
 * @Date    2014-7-17
 * @version 1.0
 *//*
public interface IBLDiversity {
  *//**
   * @author jay.wince
   * @since  20140725
   * @return
   *//*
  public abstract HashMapDataPadding<String> themeOptions();
//BEGIN TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]BL configuration(ITRADEFIVE-211)
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	 public abstract HashMapDataPadding<String> getAAStockThumbnailStockchartImageView();
	 *//**
	 * @author pengjm
	 * @since  20140725
	 * @description: option/module/login
	 *//*
	 public abstract HashMapDataPadding<String> loginOptions();
	 *//**
	 * @author pengjm
	 * @since  20140725
	 * @description: ITrade/module/login
	 *//*
	 public abstract HashMapDataPadding<String> login();
//END TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]BL configuration(ITRADEFIVE-211)
	 *//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
//BEGIN TASK #:TTL-GZ-BO.LI-00023 20140726[ITradeR5]BL configuration for AAStockIndice(ITRADEFIVE-211)
	 public abstract HashMapDataPadding<String> showAAStockIndice();
//END TASK #:TTL-GZ-BO.LI-00023 20140726[ITradeR5]BL configuration for AAStockIndice(ITRADEFIVE-211)
	 
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]BL configuration (ITRADEFIVE-221)
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> messageOptions();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> ctOptions();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> currency();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> market();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> regex();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> channel();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> defaultValue();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> enterOrderValue();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> goodTillDate();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> modifyOrder();
		
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> cancelOrder();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> tranhistoryValue();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> summaryValue();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> orderdetail();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> IPOModule();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> ipoPasswordConfirm();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> caPasswordConfirm();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> changePassword();
		
		public abstract HashMapDataPadding<String> storeOptions();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> getAAStockSettings();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> getAAStockSnapshotDataFeedOptions();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> getAAStockSnapshotParas();
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract HashMapDataPadding<String> passwordEncrypt(String pModule);
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract PatternEntry getPatternEntry(EPatternType type);
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public abstract String reject();
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]BL configuration 	(ITRADEFIVE-221)
//BEGIN TASK #:TTL-GZ-BO.LI-00027 2014-08-07[ITradeR5]Input letter of the alphabet in Stock ID when selected USEX market(ITRADEFIVE-217)
		 *//**
		 * @author bo.li
		 * @since  20140807
		 * 
		 *//*
		public abstract  HashMapDataPadding<String> enterOrderStockIdHandle();
//END TASK #:TTL-GZ-BO.LI-00027 2014-08-07[ITradeR5]Input letter of the alphabet in Stock ID when selected USEX market(ITRADEFIVE-217)
//BEGIN TASK #:TTL-GZ-BO.LI-00029.1 20140820[ITradeR5]AAStock news language(ITRADEFIVE-180)
		*//**
		 * @author bo.li
		 * @since  20140820
		 * 
		 *//*
		public abstract HashMapDataPadding<String> dataFeedProductionSettings();
		*//**
		 * @author bo.li
		 * @since  20140820
		 * 
		 *//*
		public abstract HashMapDataPadding<String> dataFeedUATSettings();
		*//**
		 * @author bo.li
		 * @since  20140820
		 * 
		 *//*
		public abstract String getdataFeedUATSettings(final EDataFeedSource source,final String leaf) ;
		*//**
		 * @author bo.li
		 * @since  20140820
		 * 
		 *//*
		public abstract String getAAStockURISettings(final EDataFeedSource source,final String leaf);
//END TASK #:TTL-GZ-BO.LI-00029.1 20140820[ITradeR5]AAStock news language(ITRADEFIVE-180)
		
//BEGIN TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]
//BEGIN TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
		public abstract DataPadding<String> getAAStockMarketInfoLoginSettings(String pMarketID);
//END TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
//END   TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]
		
		  *//**
	     * @author jay.wince
	     * @since  20141011
	     * 
	     *//*
	    public abstract HashMapDataPadding<String> historyValue();
	    
	    *//**
		 * @author junming.peng
		 * @since  20141016
		 * 
		 *//*
		public abstract HashMapDataPadding<String> placeOrderQuoteEnable();
		*//**
		 * @author bo.li
		 * @since  20141008
		 * 
		 *//*
		public abstract HashMapDataPadding<String> supportMargin();
		
		*//**
         * @author junming.peng
         * @since  20141108
         * 
         *//*
        public abstract int getTradingMainDayTradeRecord();
        
        *//**
         * @author canyong.lin
         * @since 20141201
         *//*
        public abstract HashMapDataPadding<String> getTokenConfig(String fid);
        *//**
         * @author junming.peng
         * @since  20141208
         * 
         *//*
        public abstract HashMapDataPadding<String> allowModifyOrder();
        *//**
         * @author junming.peng
         * @since  20141208
         * 
         *//*
        public abstract HashMapDataPadding<String> nonAllowModifyOrder();
        *//**
         * @author junming.peng
         * @since  20141208
         * 
         *//*
        public abstract HashMapDataPadding<String> allowCancelOrder();
        *//**
		 * @author bo.li
		 * @since  20141224
		 * 
		 *//*
        public abstract String getAAStockHostDataPadding(EDataFeedSource source,String leaf);
        *//**
		 * @author bo.li
		 * @since  20141224
		 * 
		 *//*
        public abstract String getAAStockURIDataPadding(EDataFeedSource source,String leaf);
        *//**
         * @author canyong.lin
         * @since 20150116
         *//*
        public abstract HashMapDataPadding<String> actionMapping(String actionID,String param);
        *//**
         * @author canyong.lin
         * @since 201516
         *//*
        public abstract HashMapDataPadding<String> functionDefinition(String fid);
//BEGIN TASK #:TTL-GZ-PENGJM-00326.7 20150121[ITradeR5]Change the config to BL(ITRADEFIVE-341)        
        *//**
         * @author xiaobo.zhu
         * @since  20140728
         * 
         *//*
        public abstract HashMapDataPadding<String> historyMapping();
        *//**
         * @author xiaobo.zhu
         * @since  20140728
         * 
         *//*
        public abstract HashMapDataPadding<String> orderMapping();
//END TASK #:TTL-GZ-PENGJM-00326.7 20150121[ITradeR5]Change the config to BL(ITRADEFIVE-341)
        *//**
         * @author canyong.lin
         * @since 20150127
         *//*
        public abstract HashMapDataPadding<String> getTokenUsage(String fid);
        *//**
         * @author canyong.lin
         * @since 20150128
         *//*
        public abstract HashMapDataPadding<String> getFunctionGroup(String fid);
        *//**
         * @author junming.peng
         * @since 20150210
         *//*
        public abstract String getTradingSituation();
        *//**
         * @author kelly.kuang
         * @since 20170411
         *//*
        public abstract boolean getTradingSameSessionID();
		*//**
     	* @author junming.peng
    	* @since 20150331
     	*//*
        public abstract String getDNTopicName();

        *//**
         * 
         * @Author junming.peng
         * @Date 2015-06-09 13:51:16
         * @Since  [ITradeR5/V15.05.11]
         * @see [IBLDiversity#getHistoryFunctionTab]
         * @description [Control the number of display in the history tab] 
         *//*
        public abstract String getHistoryFunctionTab();
        
        *//**
         * 
         * @Author junming.peng
         * @Date 2015-07-03 10:34:04
         * @Since  [ITradeR5/V15.06.17]
         * @see [IBLDiversity#getMarketOrderType]
         * @description [Corresponding to different market types]
         *//*
        public abstract String getMarketOrderType(String pMarket);
        
        *//**
         * @Author bo.li
         * @Since  [ITradeR5/V15.06.17]
         * @Date 2015-7-23 17:12:02
         * @see [IBLDiversity#getFilledEnquiryStatus]
         *//*
        public abstract String getFilledEnquiryStatus();
        *//**
         * @Author demonwx.gu
         * @Since  [ITradeR5/V15.06.17]
         * @Date 2015-7-27 17:12:02
         * @see [IBLDiversity#getSpecialClient]
         *//*
        public abstract String getSpecialClient(String pFlag);
        
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2015-11-09 14:43:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#getRecoveryFunctionGroup]
         * @description [get recovery function group config]
         *//*
        public abstract HashMapDataPadding<String> getRecoveryFunctionGroup(String fid);
        
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2015-11-09 14:43:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#getRecoveryFunction]
         * @description [get recovery function config in group]
         *//*
        public abstract HashMapDataPadding<String> getRecoveryFunction(String fgid,String select);
//END TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)

//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)  
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2015-11-13 15:01:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#getNormalRecoveryFunction]
         * @description [get recovery function config which is not in group]
         *//*
        public abstract HashMapDataPadding<String> getNormalRecoveryFunction(String fid);
//END TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)

//BEGIN TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2015-12-03 15:01:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#allowGoodTillDate]
         * @description [get allow GoodTillDate config ]
         *//*
        public abstract boolean allowGoodTillDate();
//END TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
        
//BEGIN TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2015-12-10 15:01:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#getMarketQuotaBalance]
         * @description [get daily quota config ]
         *//*
        public abstract HashMapDataPadding<String> getMarketQuotaBalance();
//END TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
        
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
        *//**
         * Get Identity Validation Setting
         * @Since [ITradeR5/V15.08.28]
         * @Date 2016-01-07
         * @Author stevenzg.li
         * @see [IBLDiversity#getIdentityValidationSetting]
         *//*
        public abstract HashMapDataPadding<String> getIdentityValidationSetting();
//END TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
        
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2016-03-18 16:22:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#getPlaceOrderConfirmFlow]
         * @description [get confirm page and password field config ]
         *//*
        public abstract HashMapDataPadding<String> getPlaceOrderConfirmFlow();
        
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2016-03-18 16:22:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#getModifyOrderConfirmFlow]
         * @description [get confirm page and password field config ]
         *//*
        public abstract HashMapDataPadding<String> getModifyOrderConfirmFlow();
        
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2016-03-18 16:22:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#getCancelOrderConfirmFlow]
         * @description [get confirm page and password field config ]
         *//*
        public abstract HashMapDataPadding<String> getCancelOrderConfirmFlow();
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
        
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160405[ITradeR5-CISI]Add default confirm password config.(ITRADEFIVE-787)
        *//**
         * 
         * @Author kelly.kuang
         * @Date 2016-04-05 16:22:04
         * @Since  [ITradeR5/V15.08.28]
         * @see [IBLDiversity#getDefaultEnterOrderConfirmFlow]
         * @description [get default confirm page and password field config in enter-order]
         *//*
        public abstract HashMapDataPadding<String> getDefaultEnterOrderConfirmFlow();
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Add default confirm password config.(ITRADEFIVE-787)
        
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.
        public abstract HashMapDataPadding<String> getOpenAccountInfo();
//END TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.
        
        *//**
         * @Since [ITradeR5/V15.08.28]
         * @Date 2016-09-08 10:39:03
         * @Author StevenLi
         * @see [IBLDiversity#getRunModeConfigInfo]
         * @@description [Get ITrade running mode configuration]
         *//*
        public abstract HashMapDataPadding<String> getRunModeConfigInfo();
        
        *//**
         * @Since [ITradeR5/V15.08.28]
         * @Date 2017-04-26 10:39:03
         * @Author StevenLi
         * @see [IBLDiversity#getAuthenticationOTPConfig]
         * @@description [Get Authentication one-time password configuration]
         *//*
        public abstract HashMapDataPadding<String> getAuthenticationOTPConfig(String pModule);
}
*/