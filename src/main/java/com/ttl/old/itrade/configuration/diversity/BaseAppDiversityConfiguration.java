/**
 * 
 *//*
package com.itrade.configuration.diversity;


import org.dom4j.Element;
import org.dom4j.Node;

import com.itrade.base.data.ref.HashMapDataPadding;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140709 by xuejie.xiao</p>
 * @author jay.wince
 * @Date   20140709
 * @version 1.0
 *//*
public final class BaseAppDiversityConfiguration extends AppConfiguration{

	protected static final String BASE_APP_ID = "base";
	
	private static volatile BaseAppDiversityConfiguration svInstance;
	
	private final static String NODE_FINAL_SETTINGS = "final-settings";
	  private final static String NODE_OVERRIDE_COMPANIES = "override-companies";
	
	private final static String NODE_COMPANY = "Company";
	
	private final static String NODE_VERSION = "version";
	
	private BaseAppDiversityConfiguration(String pAppID) {
		super(pAppID);
	}

	public static BaseAppDiversityConfiguration getInstance(){
		if (svInstance == null) {
			synchronized (BaseAppDiversityConfiguration.class) {
				if (svInstance == null) {
					svInstance = new BaseAppDiversityConfiguration(BASE_APP_ID);
				}
			}
		}
		return svInstance;
	}
	
	@Deprecated
	public HashMapDataPadding<String> final_CompanySettings(){
		String xpath = NODE_FINAL_SETTINGS + LEADING_SLASH + "CT";
		return this.pathResolve(xpath);
	}
	*//**
	 * Only used for login page to determine whether no options selection or default company selection.
	 * @author jay.wince
	 * @return
	 *//*
	@Deprecated
	public String defaultCompanyOption(){
		String lvCompanyCode = null;
		final HashMapDataPadding<String> lvCompanySettings = this.final_CompanySettings();
		switch (Integer.parseInt(lvCompanySettings.get("negotiate-strategy"))) {
			case 0:
	            lvCompanyCode = null;
				break;
			case 1:
				lvCompanyCode =lvCompanySettings.get("default-company");
				break;
			default:
				break;
		}
		return lvCompanyCode;
	}
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public  HashMapDataPadding<String> customerService(){
		String xpath = DOUBLE_LEADING_SLASH + "customer-service";
		return this.pathResolve(xpath);
	}
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> customerServicePhoneNumber(){
		String xpath = DOUBLE_LEADING_SLASH + "customer-service" + DOUBLE_LEADING_SLASH + "phone-number";
		return this.pathResolve(xpath);
	}

	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> version(){
		String xpath = DOUBLE_LEADING_SLASH + "versionh";
		return pathResolve(xpath);
	}

	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public HashMapDataPadding<String> language(){
		String xpath = DOUBLE_LEADING_SLASH + "language";
		return pathResolve(xpath);
	}

	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public  HashMapDataPadding<String> dashboard(){
		String xpath = DOUBLE_LEADING_SLASH + "dashboard";
		return pathResolve(xpath);
	}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]APP configuration 	(ITRADEFIVE-219)
	*//**
	 * @author jay.wince
	 * @date   20150306
	 * @param pCompanyCode
	 * @return
	 *//*
    public String getServiceIDBy(final String pCompanyCode){
        String xpath = DOUBLE_LEADING_SLASH + NODE_OVERRIDE_COMPANIES + LEADING_SLASH + NODE_COMPANY+"[@code='"+pCompanyCode+"']";
        Node lvCompanyNode = this.mvDocument.selectSingleNode(xpath);
        String lvSID = pCompanyCode;
        if (lvCompanyNode != null)
        {
            lvSID = Element.class.cast(lvCompanyNode).attributeValue("mapping-to");
        }
        return lvSID;
    }
    
    // Jay on 20151116:get the version number from configuration.
    public String getVersion(){
        return this.mvDocument.getRootElement().selectSingleNode(NODE_VERSION).getText();
    }
}
*/