/**
 * 
 *//*
package com.itrade.configuration.diversity;

import java.util.List;

import org.dom4j.Element;

import com.itrade.aastock.EDataFeedSource;
import com.itrade.base.data.DataPadding;
import com.itrade.base.data.ref.HashMapDataPadding;
import com.itrade.base.jesapi.ITradeAPI;
import com.itrade.configuration.pattern.PatternContext;
import com.itrade.configuration.pattern.PatternContext.EPatternType;
import com.itrade.configuration.pattern.PatternEntry;
import com.itrade.util.StringUtils;


*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20140709 by xuejie.xiao</p>
 * @author jay.wince
 * @Date    20140709
 * @version 1.0
 *//*
public class BLDiversityConfiguration extends AbstractDiversity<BLDiversityConfiguration> implements IBLDiversity {

	
	public BLDiversityConfiguration(String pSID) {
		super(pSID);
	}

	@Override
	public void setHandler(BLDiversityConfiguration handler) {
		this.handler = handler;
	}

	@Override
	protected String getPath() {
//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]use getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
		return WEB_INF_FOLDER+"/config/bl/"+this.mvSID+getPathSuffix();
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]use getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	}
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]add getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	*//**
     * get the config path suffix.
     * @author kelly.kuang
     * @since  20151016
     * @param config path suffix
     *//*
    @Override
    protected String getPathSuffix() {
		return CONFIG_PATH_SUFFIX;
	}
//END TASK # :TTL-GZ-kelly.kuang-00003 20151016[ITradeR5]add getPathSuffix method to get the config path suffix.(ITRADEFIVE-681)
	@Override
	public Object id() {
		return this.mvSID+"4BL";
	}
	@Override
	*//**
	 * @author jay.wince
	 * @since  20140725
	 *//*
	public HashMapDataPadding<String> themeOptions() {
		return this.pathResolve("option/theme");
	}
//BEGIN TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]BL new configuration (ITRADEFIVE-211)	
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> getAAStockThumbnailStockchartImageView() {
		return pathResolve("aastock/thumbnail-stockchart-imageview/parameters");
	}

	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> loginOptions() {
		String xpath = DOUBLE_LEADING_SLASH + "option" + DOUBLE_LEADING_SLASH + "login";
		return pathResolve(xpath);
	}
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> login() {
		String xpath = DOUBLE_LEADING_SLASH + "ITrade/"+IDiversityConfiguration.ENTITIES+"/login";
		return pathResolve(xpath);
	}
//END TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]BL new configuration (ITRADEFIVE-211)		
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	@Override
	public HashMapDataPadding<String> showAAStockIndice() {
		String xpath = DOUBLE_LEADING_SLASH + "market" + LEADING_SLASH + 
				"AAStock-indices" + LEADING_SLASH + "HKSAAIEPINT015" + LEADING_SLASH + "market-indices";
		return pathResolve(xpath);
	}
	
	//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]UI configuration (ITRADEFIVE-221)
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public HashMapDataPadding<String> messageOptions(){
			String xpath = DOUBLE_LEADING_SLASH + "option" + DOUBLE_LEADING_SLASH + "message";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public HashMapDataPadding<String> ctOptions(){
			String xpath = DOUBLE_LEADING_SLASH + "option" + DOUBLE_LEADING_SLASH + "CT";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public HashMapDataPadding<String> currency(){
			String xpath = DOUBLE_LEADING_SLASH + "currency";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public HashMapDataPadding<String> market(){
			String xpath = DOUBLE_LEADING_SLASH + "market";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140727
		 * 
		 *//*
		public HashMapDataPadding<String> regex(){
			String xpath = DOUBLE_LEADING_SLASH + "regex";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> channel(){
			String xpath = DOUBLE_LEADING_SLASH + "channel";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> defaultValue(){
			String xpath = DOUBLE_LEADING_SLASH + IDiversityConfiguration.ENTITIES + LEADING_SLASH + "default" + DOUBLE_LEADING_SLASH + "value";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> enterOrderValue(){
			String xpath = DOUBLE_LEADING_SLASH + "HKSPOEPINT001"+ DOUBLE_LEADING_SLASH + "value";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> goodTillDate(){
			String xpath = DOUBLE_LEADING_SLASH + "good-till-date";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> modifyOrder(){
			String xpath = DOUBLE_LEADING_SLASH + "modify-order";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> cancelOrder(){
			String xpath = DOUBLE_LEADING_SLASH + "cancel-order";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> tranhistoryValue(){
			String xpath = DOUBLE_LEADING_SLASH + "HKSTHEPINT0020" + DOUBLE_LEADING_SLASH + "value";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> summaryValue(){
			String xpath = DOUBLE_LEADING_SLASH + "Summary" + DOUBLE_LEADING_SLASH + "value";
			return pathResolve(xpath); 
		}
		
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> orderdetail(){
			String xpath = DOUBLE_LEADING_SLASH + "order-detail";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> IPOModule(){
			String xpath = DOUBLE_LEADING_SLASH + IDiversityConfiguration.ENTITIES + DOUBLE_LEADING_SLASH +  "IPO";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> ipoPasswordConfirm(){
			String xpath = DOUBLE_LEADING_SLASH + "IPO" + DOUBLE_LEADING_SLASH + "IPOStatus";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> caPasswordConfirm(){
			String xpath = DOUBLE_LEADING_SLASH + "CA"+LEADING_SLASH +"CAStatus";
			return pathResolve(xpath); 
		}
		
		*//**
		 * @author xiaobo.zhu
		 * @since  20140728
		 * 
		 *//*
		public HashMapDataPadding<String> changePassword(){
			String xpath = DOUBLE_LEADING_SLASH + "change-password";
			return pathResolve(xpath); 
		}
		
		public HashMapDataPadding<String> storeOptions(){
			String xpath = DOUBLE_LEADING_SLASH + "dashboard" + DOUBLE_LEADING_SLASH + "store-option";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140729
		 * 
		 *//*
		public HashMapDataPadding<String> getAAStockSettings(){
			String xpath = DOUBLE_LEADING_SLASH + "aastock";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140729
		 * 
		 *//*
		public HashMapDataPadding<String> getAAStockSnapshotDataFeedOptions(){
			String xpath = DOUBLE_LEADING_SLASH + "aastock" + DOUBLE_LEADING_SLASH
					+ "snapshot-data-feed";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140729
		 * 
		 *//*
		public HashMapDataPadding<String> getAAStockSnapshotParas(){
			String xpath = DOUBLE_LEADING_SLASH + "aastock" + DOUBLE_LEADING_SLASH
					+ "snapshot-data-feed" + DOUBLE_LEADING_SLASH +"parameters";
			return pathResolve(xpath); 
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140729
		 * 
		 *//*
		public HashMapDataPadding<String> passwordEncrypt(String pModule){
			return invokeMethod0("fallbackDefault", "encrypt",new String[]{IDiversityConfiguration.ENTITIES,pModule+"/"});
		}
		*//**
		 * @author xiaobo.zhu
		 * @since  20140729
		 * 
		 *//*
		public String reject(){
			String xpath = DOUBLE_LEADING_SLASH + "order-book";
			return pathResolve(xpath).original().get("reject-reason-and-remark");	
		}
	//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]UI configuration (ITRADEFIVE-221)
		 *//**
		 * @author bo.li
		 * @since  20140807
		 * 
		 *//*
//BEGIN TASK #:TTL-GZ-BO.LI-00027 2014-08-07[ITradeR5]Input letter of the alphabet in Stock ID when selected USEX market(ITRADEFIVE-217)
		@Override
		public HashMapDataPadding<String> enterOrderStockIdHandle() {
			String xpath = DOUBLE_LEADING_SLASH + "HKSPOEPINT001"+DOUBLE_LEADING_SLASH+"stock-code-handle";
			return pathResolve(xpath); 
		}
//END TASK #:TTL-GZ-BO.LI-00027 2014-08-07[ITradeR5]Input letter of the alphabet in Stock ID when selected USEX market(ITRADEFIVE-217)
		
		private Element getPatternEntryElement(final EPatternType type) {
			Element element = null;
			@SuppressWarnings("unchecked")
			List<Element> nodes = this.mvDocument.getRootElement().selectNodes(
					"format-spec/" + NODE_ENTRIES + "/pattern-" + NODE_ENTRY
							+ "[@type='" + type.toString().toLowerCase() + "']");
			if (nodes.size() > 0) {
				element = nodes.get(0);
			}
					
			return element;
		}
		public  PatternEntry getPatternEntry(EPatternType type){		 
			Element lvBaseElement = getPatternEntryElement(type);
			Element lvOverrider = null;
			if (this.handler != null)
            {
			    lvOverrider = this.handler.getPatternEntryElement(type);
            }
			Element lvResultElement = lvBaseElement == null ? lvOverrider : lvBaseElement;//junming on 20150312:Override format value fixed
//			if (lvOverrider != null) lvResultElement = lvOverrider;
			PatternEntry lvPatternEntry = new PatternContext(lvResultElement).getInstance(type);
			return lvPatternEntry;	     
		}
		*//**
		 * @author bo.li
		 * @since  20140818
		 * 
		 *//*
		public String getdataFeedUATSettings(final EDataFeedSource source,final String leaf){
			String host = null;
			org.dom4j.Element hostElement = null;
			switch (ITradeAPI.iTradeConfiguration().runningmode()) {
	//production
					case 0:
						hostElement = parseTextToElement(dataFeedProductionSettings(), "host");
						break;
	//development		
					case 1:
						
						break;
	//testing			
					case 2:
						hostElement = parseTextToElement(dataFeedUATSettings(), "host");
						break;
					default:
						break;
			}
			host = fallbackDefaultString(hostElement,leaf, new String[]{source.toString().toLowerCase()});			
			return host;
		}
		*//**
		 * @author bo.li
		 * @since  20140818
		 * 
		 *//*
		public String getAAStockURISettings(final EDataFeedSource source,final String leaf){
			String uri = null;
			org.dom4j.Element uriElement = null;
			switch (ITradeAPI.iTradeConfiguration().runningmode()) {
	//production
					case 0:
						uriElement = parseTextToElement(dataFeedProductionSettings(), "uri");
						break;
	//development		
					case 1:
						
						break;
	//testing			
					case 2:
						uriElement = parseTextToElement(dataFeedUATSettings(), "uri");
						break;
					default:
						break;
			}
			uri = fallbackDefaultString(uriElement,leaf, new String[]{source.toString().toLowerCase()});			
			return uri;
		}
		*//**
		 * @author bo.li
		 * @since  20140818
		 * 
		 *//*
		@Override
		public HashMapDataPadding<String> dataFeedProductionSettings() {
			return getdataPadding(DOUBLE_LEADING_SLASH + "data-feed/production");
		}
		*//**
		 * @author bo.li
		 * @since  20140818
		 * 
		 *//*
		@Override
		public HashMapDataPadding<String> dataFeedUATSettings() {
			return getdataPadding(DOUBLE_LEADING_SLASH + "data-feed/UAT");
		}
//BEGIN TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]
//BEGIN TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
		@Override
		public DataPadding<String> getAAStockMarketInfoLoginSettings(String pMarketID) {
			return pathResolve(DOUBLE_LEADING_SLASH + "market-info/login[@id='"+pMarketID+"']");
		}
//END TASK #:TTL-GZ-kelly.kuang-00000 20151209[ITradeR5]Support multiMarket info.[ITRADEFIVE-701]
//END   TASK #:TTL-GZ-Jay-00200 20140925[ITradeR5]AAStock streaming quote[ITRADEFIVE-286]	

        @Override
        *//**
         * @author jay.wince
         * @since  20141011
         * 
         *//*
        public HashMapDataPadding<String> historyValue(){
            String xpath = DOUBLE_LEADING_SLASH + "HKSOHEPINT0021" + DOUBLE_LEADING_SLASH + "value";
            return pathResolve(xpath);
        }
		*//**
	 	*@author junming.peng
	 	*@since 20141016
	 	*//*
		@Override
		public HashMapDataPadding<String> placeOrderQuoteEnable() {
			String xpath = DOUBLE_LEADING_SLASH + "HKSPOEPINT001";
			return pathResolve(xpath); 
		}
//BEGIN TASK #:TTL-GZ-BO.LI-00047 20141008[ITradeR5]Show the IPO margin[ITRADEFIVE-288]
		*//**
	 	*@author bo.li
	 	*@since 20141016
	 	*//*
		@Override
		public HashMapDataPadding<String> supportMargin() {
			String xpath = DOUBLE_LEADING_SLASH + "IPO/margin";
			return pathResolve(xpath); 
		}
//END TASK #:TTL-GZ-BO.LI-00047 20141008[ITradeR5]Show the IPO margin[ITRADEFIVE-288]
		*//**
         * @author junming.peng
         * @since  20141108
         * 
         *//*
		@Override
		public int getTradingMainDayTradeRecord() {
		    String xpath = DOUBLE_LEADING_SLASH+"order-book//tableic";
		    return Integer.parseInt(pathResolve(xpath).original().get("trading-main-record")); 
		}
		
		*//**
		 * @author canyong.lin
		 * @since 20141201
		 *//*
		@Override
		public HashMapDataPadding<String> getTokenConfig(String fid) {		
			String lvXpath = DOUBLE_LEADING_SLASH+"token-config/token[@id='"+fid+"']";
			return pathResolve(lvXpath);
		}
		*//**
         * @author junming.peng
         * @since  20141208
         * 
         *//*
        public HashMapDataPadding<String> allowModifyOrder(){
            String xpath = DOUBLE_LEADING_SLASH + "modify-order"+DOUBLE_LEADING_SLASH+"allow-modify";
            return pathResolve(xpath); 
        }
        *//**
         * @author junming.peng
         * @since  20141208
         * 
         *//*
        public HashMapDataPadding<String> nonAllowModifyOrder(){
            String xpath = DOUBLE_LEADING_SLASH + "modify-order"+DOUBLE_LEADING_SLASH+"non-modify";
            return pathResolve(xpath); 
        }
        *//**
         * @author junming.peng
         * @since  20141208
         * 
         *//*
        @Override
        public HashMapDataPadding<String> allowCancelOrder()
        {
            String xpath = DOUBLE_LEADING_SLASH + "cancel-order"+DOUBLE_LEADING_SLASH+"allow-cancel";
            return pathResolve(xpath);
        }
        *//**
		 * @author bo.li
		 * @since  20141224
		 * 
		 *//*
		@Override
		public String getAAStockHostDataPadding(EDataFeedSource source,String leaf) {
			String lvType = "UAT";
			switch (ITradeAPI.iTradeConfiguration().runningmode()) {
				//production
				case 0:
					lvType = "production";
					break;
				//development		
				case 1:
					
					break;
				//testing			
				case 2:
					lvType = "UAT";
					break;
				default:
					break;
				}
			 String xpath = DOUBLE_LEADING_SLASH + "data-feed"+LEADING_SLASH+lvType+LEADING_SLASH+"host";
			 return fallbackDefaultString(parseTextToElement(pathResolve(xpath),source.toString().toLowerCase()),leaf);
		}
		*//**
		 * @author bo.li
		 * @since  20141224
		 * 
		 *//*
		@Override
		public String getAAStockURIDataPadding(EDataFeedSource source,String leaf) {
			String lvType = "UAT";
			switch (ITradeAPI.iTradeConfiguration().runningmode()) {
				//production
				case 0:
					lvType = "production";
					break;
				//development		
				case 1:
					
					break;
				//testing			
				case 2:
					lvType = "UAT";
					break;
				default:
					break;
				}
			 String xpath = DOUBLE_LEADING_SLASH + "data-feed"+LEADING_SLASH+lvType+LEADING_SLASH+"uri";
			 return fallbackDefaultString(parseTextToElement(pathResolve(xpath),source.toString().toLowerCase()),leaf);
		}
//BEGIN TASK #:TTL-GZ-PENGJM-00326.7 20150121[ITradeR5]Change the config to BL(ITRADEFIVE-341)//junming.peng on 20150121:Change to BL config
		*//**
	     * @author xiaobo.zhu
	     * @since  20140728
	     * 
	     *//*
	    public HashMapDataPadding<String> historyMapping(){
	        String xpath = DOUBLE_LEADING_SLASH + "HKSOHEPINT0021" + DOUBLE_LEADING_SLASH + "history-mapping";
	        return pathResolve(xpath);
	    }
	    *//**
	     * @author xiaobo.zhu
	     * @since  20140728
	     * 
	     *//*
	    public HashMapDataPadding<String> orderMapping(){
	        String xpath = DOUBLE_LEADING_SLASH + "HKSOEEPINT002" + DOUBLE_LEADING_SLASH + "order-mapping";
	        return pathResolve(xpath);
	    }
//END TASK #:TTL-GZ-PENGJM-00326.7 20150121[ITradeR5]Change the config to BL(ITRADEFIVE-341)
	    @Override
	    *//**
	     * @author canyong.lin
	     * @since 20150116
	     *//*
	    public HashMapDataPadding<String> actionMapping(String actionID,String param)
	    {
	    	String lvXpath = DOUBLE_LEADING_SLASH+"action-function-mapping/mapping[@action-id='"+actionID+"']";
	    	if(param!=null){
	    		lvXpath = DOUBLE_LEADING_SLASH+"action-function-mapping/mapping[@action-id='"+actionID+"' and @action-parameter='"+param+"']";
	    	}
	    	return pathResolve(lvXpath);
	    }
	    
	    @Override
	    *//**
	     * @author canyong.lin
	     * @since 20150116
	     *//*
	    public HashMapDataPadding<String> functionDefinition(String fid) {
	    	String lvXpath = DOUBLE_LEADING_SLASH+"function-definition/function-group/function[@id='"+fid+"']";
	    	lvXpath += "|"+DOUBLE_LEADING_SLASH+"function-definition/function[@id='"+fid+"']";
	    	return pathResolve(lvXpath);
	    }

		@Override
		*//**
		 * @author canyong.lin
		 * @since 20150127
		 *//*
		public HashMapDataPadding<String> getTokenUsage(String fid) {
			String lvXpath = DOUBLE_LEADING_SLASH+"token-config/token[contains(@token-usage,'"+fid+"')]";
			return pathResolve(lvXpath);
		}

		@Override
		*//**
		 * @author canyong.lin
		 * @since 20150128
		 *//*
		public HashMapDataPadding<String> getFunctionGroup(String fid) {
			String lvXpath = DOUBLE_LEADING_SLASH+"function-definition/function-group/function[@id='"+fid+"']/..";
	    	return pathResolve(lvXpath);
		}
		*//**
         * @author junming.peng
         * @since 20150210
         *//*
        @Override
        public String getTradingSituation()
        {
            String lvXpath = DOUBLE_LEADING_SLASH+"trading-account-info";
            return pathResolve(lvXpath).get("login-situation");
        }
        *//**
         * @author kelly.kuang
         * @since 20170411
         *//*
        @Override
        public boolean getTradingSameSessionID()
        {
        	String xpath = DOUBLE_LEADING_SLASH+"trading-account-info";
    		return Boolean.TRUE.toString().equals(pathResolve(xpath).get("same-session-id")); 
        }
		*//**
     	* @author junming.peng
     	* @since 20150331
     	*//*
        @Override
        public String getDNTopicName()
        {
            String lvXpath = DOUBLE_LEADING_SLASH+"registerDN";
            return pathResolve(lvXpath).get("topic-name");
        }

         
         * @Author junming.peng
         * @Date 2015-06-09 13:51:16
         * (non-Javadoc)	
         * @see com.itrade.configuration.diversity.IBLDiversity#getHistoryFunctionTab()
         
        @Override
        public String getHistoryFunctionTab()
        {
            String lvXpath = DOUBLE_LEADING_SLASH+"history";
            return pathResolve(lvXpath).get("show-function");
        }

         
         * @Author junming.peng
         * @Date 2015-07-03 10:39:56
         * (non-Javadoc)	
         * @see com.itrade.configuration.diversity.IBLDiversity#getMarketOrderType(java.lang.String)
         
        @Override
        public String getMarketOrderType(String pMarket)
        {
            String xpath = DOUBLE_LEADING_SLASH + "HKSPOEPINT001"+ DOUBLE_LEADING_SLASH + "value";
            return pathResolve(xpath).get(pMarket+"-order-type");
        }
        
         * @Author bo.li
         * @Date 2015-07-23 17:16:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getFilledEnquiryStatus()
         
        @Override
        public String getFilledEnquiryStatus()
        {
            String xpath = DOUBLE_LEADING_SLASH + "HKSFQTYEEPINT020";
            return pathResolve(xpath).get("status");
        }
         
         * @Author demonwx.gu
         * @Date 2015-07-27 10:39:56
         * (non-Javadoc)    
         * @see com.itrade.configuration.diversity.IBLDiversity#getSpecialClient(java.lang.String)
         
        @Override
        public String getSpecialClient(String pFlag)
        {
            String xpath = DOUBLE_LEADING_SLASH + "HKSPOEPINT001"+ DOUBLE_LEADING_SLASH + "special-client";
            return pathResolve(xpath).get(pFlag);
        }
        
//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)
        
         * @Author kelly.kuang
         * @Date 2015-11-09 14:37:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getRecoveryFunctionGroup()
         
        public HashMapDataPadding<String> getRecoveryFunctionGroup(String fid) {
			String lvXpath = DOUBLE_LEADING_SLASH+"exception-recovery/recovery-function-group/recovery-function[@id='"+fid+"']/..";
	    	return pathResolve(lvXpath);
		}
        
        
         * @Author kelly.kuang
         * @Date 2015-11-09 14:37:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getRecoveryFunction()
         
        public HashMapDataPadding<String> getRecoveryFunction(String fgid, String select) {
			String lvXpath = DOUBLE_LEADING_SLASH+"exception-recovery/recovery-function-group[@gid='"+fgid+"']/recovery-function["+select+"]";
	    	return pathResolve(lvXpath);
		}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151109[ITradeR5]get recovery function group config.(ITRADEFIVE-693)

//BEGIN TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)
        
         * @Author kelly.kuang
         * @Date 2015-11-13 15:02:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getNormalRecoveryFunction()
         
        public HashMapDataPadding<String> getNormalRecoveryFunction(String fid) {
			String lvXpath = DOUBLE_LEADING_SLASH+"exception-recovery/recovery-function[@id='"+fid+"']";
	    	return pathResolve(lvXpath);
		}
//END TASK # :TTL-GZ-kelly.kuang-00005 20151113[ITradeR5]get recovery function config which is not in group.(ITRADEFIVE-693)
        
//BEGIN TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
        
         * @Author kelly.kuang
         * @Date 2015-12-03 15:02:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#allowGoodTillDate()
         
        @Override
    	public boolean allowGoodTillDate() {
    		String xpath = DOUBLE_LEADING_SLASH + "HKSPOEPINT001";
    		return Boolean.TRUE.toString().equals(pathResolve(xpath).get("allowGoodTillDate")); 
    	}
//END TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
        
//BEGIN TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]
        
         * @Author kelly.kuang
         * @Date 2015-12-10 15:02:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getMarketQuotaBalance()
         
        @Override
    	public HashMapDataPadding<String> getMarketQuotaBalance() {
        	String lvXpath = DOUBLE_LEADING_SLASH+"market-quota-balance";
	    	return pathResolve(lvXpath);
    	}
//END TASK #:TTL-GZ-kelly.kuang-00009 20151210[ITradeR5-CISI]get Daily Quota config.[ITRADEFIVE-714]

//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
        
         * @Author stevenzg.li
         * @Date 2016-01-07
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getIdentityValidationSetting()
         
        @Override
        public HashMapDataPadding<String> getIdentityValidationSetting() {
            String lvXpath = DOUBLE_LEADING_SLASH + "identity-validation";
            return pathResolve(lvXpath);
        }
//END TASK #:TTL-GZ-STEVENZG.LI-00025 2016-01-07[ITradeR5-CISI]Add Identity Validation Function.(ITRADEFIVE-741)
        
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
        
         * @Author kelly.kuang
         * @Date 2016-03-18 16:16:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getPlaceOrderConfirmFlow()
         
        @Override
    	public HashMapDataPadding<String> getPlaceOrderConfirmFlow() {
    		String xpath = DOUBLE_LEADING_SLASH + "HKSPOEPINT001/confirm-flow";
    		return pathResolve(xpath); 
    	}
        
        
         * @Author kelly.kuang
         * @Date 2016-03-18 16:16:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getModifyOrderConfirmFlow()
         
        @Override
    	public HashMapDataPadding<String> getModifyOrderConfirmFlow() {
    		String xpath = DOUBLE_LEADING_SLASH + "modify-order/confirm-flow";
    		return pathResolve(xpath); 
    	}
        
        
         * @Author kelly.kuang
         * @Date 2016-03-18 16:16:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getCancelOrderConfirmFlow()
         
        @Override
    	public HashMapDataPadding<String> getCancelOrderConfirmFlow() {
    		String xpath = DOUBLE_LEADING_SLASH + "cancel-order/confirm-flow";
    		return pathResolve(xpath); 
    	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Get password field config.(ITRADEFIVE-787)
        
//BEGIN TASK # :TTL-GZ-kelly.kuang-00039 20160405[ITradeR5-CISI]Add default confirm password config.(ITRADEFIVE-787)
        
         * @Author kelly.kuang
         * @Date 2016-04-05 11:01:30
         * (non-Javadoc)
         * @see com.itrade.configuration.diversity.IBLDiversity#getDefaultEnterOrderConfirmFlow()
         
        @Override
    	public HashMapDataPadding<String> getDefaultEnterOrderConfirmFlow() {
    		String xpath = DOUBLE_LEADING_SLASH + "enter-order/confirm-flow";
    		return pathResolve(xpath); 
    	}
//END TASK # :TTL-GZ-kelly.kuang-00039 20160318[ITradeR5-CISI]Add default confirm password config.(ITRADEFIVE-787)

//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.
        *//**
         *@Author STEVENZG.LI
         *@Date 2016-07-13
         *//*
        @Override
        public HashMapDataPadding<String> getOpenAccountInfo()
        {
            String xpath = DOUBLE_LEADING_SLASH + "ITrade/" + IDiversityConfiguration.ENTITIES + "/login/open-account";
            return pathResolve(xpath);
        }
//END TASK #:TTL-GZ-STEVENZG.LI-00053 2016-07-13[ITradeR5]Add "Open Account" Link to Login Page.

        *//**
         *@Author STEVENZG.LI
         *@Date 2016-09-08
         *//*
        @Override
        public HashMapDataPadding<String> getRunModeConfigInfo()
        {
            String xpath = DOUBLE_LEADING_SLASH + "run-mode";
            return pathResolve(xpath);
        }
        
        *//**
         *@Author STEVENZG.LI
         *@Date 2017-04-26
         *//*
        @Override
        public HashMapDataPadding<String> getAuthenticationOTPConfig(String pModule)
        {
            pModule = StringUtils.isNullStr(pModule) ? "" : ("/" + pModule);
            String xpath = DOUBLE_LEADING_SLASH + "ITrade/" + IDiversityConfiguration.ENTITIES + "/authentication/one-time-password" + pModule;
            return pathResolve(xpath);
        }
}
*/