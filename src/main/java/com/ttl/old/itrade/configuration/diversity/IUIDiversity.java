/**
 * 
 *//*
package com.itrade.configuration.diversity;

import com.itrade.base.data.ref.HashMapDataPadding;

*//**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 2014-7-17 by xuejie.xiao</p>
 * @author jay.wince
 * @Date    2014-7-17
 * @version 1.0
 *//*
public interface IUIDiversity {
	*//**
	 * @author jay.wince
	 * @since  20140725
	 * @return
	 *//*
    public abstract HashMapDataPadding<String> themePadding();
//BEGIN TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]UI configuration(ITRADEFIVE-212)	
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	public abstract HashMapDataPadding<String> pagingStyle(String pModule);
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	public abstract HashMapDataPadding<String> orderColumns();
	
//BEGIN TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	*//**
	 * @author kelly.kuang
	 * @since  20151120
	 * 
	 *//*
	public abstract String orderSettingRawColumns();
	*//**
	 * @author kelly.kuang
	 * @since  20151120
	 * 
	 *//*
	public abstract String orderDefaultRawColumns();
//END TASK # :TTL-GZ-kelly.kuang-00006 20151120[ITradeR5]Add raw columns config.(ITRADEFIVE-697)
	
	*//**
	 * @author pengjm
	 * @since  20140725
	 * 
	 *//*
	public abstract HashMapDataPadding<String> getTypeSet();
//END TASK #:TTL-GZ-PENGJM-00252 20140725[ITradeR5]UI configuration(ITRADEFIVE-212)	
//BEGIN TASK #:TTL-GZ-BO.LI-00023 20140726[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	public abstract HashMapDataPadding<String> getAAStockIndice();
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	public abstract HashMapDataPadding<String> getAAStockFavorite();
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	public abstract HashMapDataPadding<String> getAAStockNews();
	*//**
	 * @author bo.li
	 * @since  20140726
	 * 
	 *//*
	public abstract HashMapDataPadding<String> getPriceAlertColumns();
//END TASK #:TTL-GZ-BO.LI-00023 20140726[ITradeR5]UI configuration (ITRADEFIVE-211)	
//BEGIN TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]UI configuration (ITRADEFIVE-220)
	*//**
	 * @author xiaobo.zhu
	 * @since  20140727
	 * 
	 *//*
	public abstract HashMapDataPadding<String> common();
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public abstract HashMapDataPadding<String> orderhistoryCloumns();

	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public abstract HashMapDataPadding<String> accountBalanceCloumns();
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public abstract HashMapDataPadding<String> groupSupported(String pModule);
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public abstract HashMapDataPadding<String> pagesize(String pModule);
	*//**
	 * @author xiaobo.zhu
	 * @since  20140728
	 * 
	 *//*
	public abstract String displayMode(String nodeName);
//END TASK #:TTL-GZ-xiaobo.zhu-0002 20140727[ITradeR5]UI configuration (ITRADEFIVE-220)	
	*//**
	 * @author bo.li
	 * @since  20140823
	 * 
	 *//*
	public abstract HashMapDataPadding<String> AAStockIndiceShowGroup();
//BEGIN TASK #:TTL-GZ-BO.LI-00039 2014-09-10[ITradeR5]TickerView group(ITRADEFIVE-250)
	*//**
	 * @author bo.li
	 * @since  20140910
	 * 
	 *//*
	public abstract HashMapDataPadding<String> TickerViewShowGroup();
//END TASK #:TTL-GZ-BO.LI-00039 2014-09-10[ITradeR5]TickerView group(ITRADEFIVE-250)
	*//**
	 * @author bo.li
	 * @since  20140920
	 * 
	 *//*
	public abstract HashMapDataPadding<String> FavoriteStockSearch();
	
	*//**
	 * @author canyong.lin
	 * @since  20140924
	 * 
	 *//*
	public abstract HashMapDataPadding<String> aastockNewsPadding();
	*//**
	 * @author bo.li
	 * @since  20141111
	 * 
	 *//*
	public abstract HashMapDataPadding<String> potfolioFilter();
	*//**
	 * @author bo.li
	 * @since  20141111
	 * 
	 *//*
	public abstract HashMapDataPadding<String> BankAccountBalanceFilter();
	*//**
	 * @author bo.li
	 * @since  20141125
	 * 
	 *//*
	public abstract HashMapDataPadding<String> getFundManagementPadding(String pModel);
	*//**
	 * @author piece.lin
	 * @since  20141119
	 * 
	 *//*
	public abstract HashMapDataPadding<String> tableFieldSorting(String pModel);
	*//**
	 * @author piece.lin
	 * @since  20141119
	 * 
	 *//*
	public abstract HashMapDataPadding<String> settingsDataPadding(final String pModule);
	*//**
	 * @author junming.peng
	 * @since  20141215
	 * 
	 *//*
	public abstract HashMapDataPadding<String> getSortable(final String pModule);
//BEGIN TASK #:TTL-GZ-PENGJM-00326.7 20150121[ITradeR5]Change the config to UI(ITRADEFIVE-341)
	*//**
     * @author xiaobo.zhu
     * @since  20140728
     * 
     *//*
	public abstract HashMapDataPadding<String> tranhistoryCloumns();
    *//**
     * @author xiaobo.zhu
     * @since  20140728
     * 
     *//*
    public abstract HashMapDataPadding<String> caCloumns();
    
    *//**
     * @author xiaobo.zhu
     * @since  20140727
     * 
     *//*
    public abstract HashMapDataPadding<String> summaryCloumns();
    *//**
     * @author xiaobo.zhu
     * @since  20140727
     * 
     *//*
    public abstract HashMapDataPadding<String> hottestCloumns();
    *//**
     * @author xiaobo.zhu
     * @since  20140727
     * 
     *//*
    public abstract HashMapDataPadding<String> statusCloumns();
    *//**
     * @author xiaobo.zhu
     * @since  20140727
     * 
     *//*
    public abstract HashMapDataPadding<String> listFocusCloumns();
//END TASK #:TTL-GZ-PENGJM-00326.7 20150121[ITradeR5]Change the config to UI(ITRADEFIVE-341)
    *//**
     * @author canyong.lin
     * @since 20150413
     *//*
    public abstract String getPrimarykey(final String fid);
    
//BEGIN TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
    *//**
     * @author kelly.kuang
     * @since 20151203
     *//*
    public abstract boolean displayTodayValueInGoodTillDateComboBox();
//END TASK # :TTL-GZ-kelly.kuang-00008 20151203[ITradeR5]Add GoodTillDate config.(ITRADEFIVE-713)
    
//BEGIN TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)
    *//**
     * @author stevenzg.li
     * @Date 2015-12-28
     *//*
    public abstract HashMapDataPadding<String> getResizable(final String pModule);
//END TASK #:TTL-GZ-STEVENZG.LI-00023 2015-12-29[ITradeR5]Support to resize the table's column width.(ITRADEFIVE-733)
}
*/