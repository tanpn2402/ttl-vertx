/**
 * 
 */
package com.ttl.old.itrade.base.dashboard.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20141208 by xuejie.xiao</p>
 * @author jay.wince
 * @Date   20141209
 * @version 1.0
 */
public class DashboardCategoryMgrModel implements Serializable
{
     /**
     * 
     */
    private static final long serialVersionUID = 201412251551L;
    private String 									mvActive;
    private List<DashboardCategoriesModel> 		mvCategories;
    /**
     * Key-Value pair
     * pin-Y
     * 
     */ 
    private Map<String, Object> 					mvSettings;
     
     public String getActive()
    {
        return mvActive;
    }
     
     public void setActive(String mvActive)
    {
        this.mvActive = mvActive;
    }
     
    public void setCategories(List<DashboardCategoriesModel> mvCategories)
    {
        this.mvCategories = mvCategories;
    }
    
    public List<DashboardCategoriesModel> getCategories()
    {
        return mvCategories;
    }
}
