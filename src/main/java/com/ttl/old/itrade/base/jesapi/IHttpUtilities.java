package com.ttl.old.itrade.base.jesapi;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.itrade.base.data.DataPadding;

/**
 * @category 
 * TTL-GZ-Jay-00119 2013-12-11[ITrade5]ITrade API : HTTP utilities.
 * @author jay.wince
 * @since  2013-12-11
 */
public interface IHttpUtilities {

    HttpServletRequest getCurrentRequest();
    HttpServletResponse getCurrentResponse();
    /**
     * Stores the current HttpRequest and HttpResponse so that they may be readily accessed throughout ITrade API (and elsewhere)
     * @param request  the current request
     * @param response the current response
     */
    void setCurrentHTTP(HttpServletRequest request, HttpServletResponse response);
    /**
	 * Clears the current HttpRequest and HttpResponse associated with the current thread.
	 * @see ITradeAPI#clearCurrent()
	 */
    void clearCurrent();
//BEGIN TASK #:TTL-GZ-clover_he-00084 2014-01-21 [ITrade5] Gets all request data and returns them as a DataPadding instead of ItradeFormData.[getFormDataPadding method instead of getFormData method).[ITRADEFIVE-107]	    
	//DataPadding<String> getFormDataPadding(HttpServletRequest request);
///END  TASK #:TTL-GZ-clover_he-00084 2014-01-21 [ITrade5] Gets all request data and returns them as a DataPadding instead of ItradeFormData.[getFormDataPadding method instead of getFormData method).[ITRADEFIVE-107]
	
	boolean isAsynRequest();
	
	String getCompanyCodeFallback();

	public abstract HttpSession changeSessionIdentifier();
	public abstract HttpSession changeSessionIdentifier(HttpServletRequest request);
}
