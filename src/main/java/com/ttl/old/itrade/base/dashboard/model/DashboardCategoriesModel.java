/**
 * 
 */
package com.ttl.old.itrade.base.dashboard.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>Copyright: Copyright(c)Transaction Technologies Limited 20141208 by xuejie.xiao</p>
 * @author jay.wince
 * @Date   20141208
 * @version 1.0
 */
public class DashboardCategoriesModel implements Serializable ,Comparable<DashboardCategoriesModel>
{
   /**
     * 
     */
   private static final long serialVersionUID = 201412081856L;
   
   private String                  mvIdentifier;
   
   private int                          mvIndex;
   private List<DashboardWidgetModel> mvWidgets;
   private Map<String, String>	   mvNameHashes;
   private String                      mvLayout;
   private Map<String, Object>         mvToolOptions;
    /**
     * @return the identifier
     */
    public String getId()
    {
        return mvIdentifier;
    }
    /**
     * @param identifier the id to set
     */
    public void setId(String identifier)
    {
        this.mvIdentifier = identifier;
    }
    /**
     * @return the widgets
     */
    public List<DashboardWidgetModel> getWidgets()
    {
        return mvWidgets;
    }
    /**
     * @param mvWidgets the widgets to set
     */
    public void setWidgets(List<DashboardWidgetModel> widgets)
    {
        this.mvWidgets = widgets;
    }
    
    public int getIndex()
    {
        return mvIndex;
    }
    
    public void setIndex(int index)
    {
        this.mvIndex = index;
    }
    
    public Map<String, String> getNamehashes() {
		return mvNameHashes;
	}
	public void setNamehashes(Map<String, String> pNameHashes) {
		this.mvNameHashes = pNameHashes;
	}
	
	public String getLayout()
    {
        return mvLayout;
    }
	
	public void setLayout(String pLayout)
    {
        this.mvLayout = pLayout;
    }
	
	public Map<String, Object> getToolOptions()
    {
        return mvToolOptions;
    }
	public void setToolOptions(Map<String, Object> pToolOptions)
    {
        this.mvToolOptions = pToolOptions;
    }
	@Override
    public int compareTo(DashboardCategoriesModel arg0)
    {
        return this.mvIndex - arg0.mvIndex;
    }
   
   
   
}
