package com.ttl.old.itrade.base.dashboard.model;
/**
 * 
 * @author jay.wince
 * @since  2013-12-05
 */
public enum EDashboardWidgetRatio {
    /*Arithmetical Format(Literal Format,Franctional Format)*/
    ONE_THIRD("1bs3","1/3"),
    TWO_THIRD("2bs3","2/3"),
    FULL("full","3/3");
    private String mvLiteralV;
    private String mvFranctionalV;
    
    private EDashboardWidgetRatio(String pLiteralV,String pFractionalV){
        this.mvLiteralV     = pLiteralV;
        this.mvFranctionalV = pFractionalV;
	}
    /**
     * Literal Format
     * @author jay.wince
     * @since  20141210
     * @return
     */
	public String getValue(){
        return this.mvLiteralV;
	}
    /**
     * Arithmetical Format.
     * @author jay.wince
     * @since  20141210
     * @return
     */
    public String getArithmeticalV(){
        return this.toString().toLowerCase().replace("_", "-");
    }
	
    /**
     * Franctional Format.
     * @author jay.wince
     * @since  20141210
     * @return
     */
    public String getFranctionalV(){
        return this.mvFranctionalV;
    }
/*  
	public String mappingName(final String value){
		EDashboardWidgetRatio[] values = EDashboardWidgetRatio.values();
		for (EDashboardWidgetRatio eDashboardWidgetRatio : values) {
             if (eDashboardWidgetRatio.mvLiteralV.equals(value)) {
				return eDashboardWidgetRatio.toString();
			}
		}
		return null;
	}
*/
	
/*  
	public String hyphenate(String linker){
		String name = this.toString();
		String [] array = name.split("_");
		return array[0].toLowerCase()+linker+array[1].toLowerCase();
	}
*/  
    /**
     * Arithmetical Format.
     * @author jay.wince
     * @since  20141210
     * @param value
     * @return
     */
    public static String reorganize(final String value){
        return reorganize(value, null);
    }
	public static String reorganize(final String value,final String join){
		EDashboardWidgetRatio[] values = EDashboardWidgetRatio.values();
		String linker = join==null ? "_" : join;
		for (EDashboardWidgetRatio eDashboardWidgetRatio : values) {
             if (eDashboardWidgetRatio.mvLiteralV.equals(value)) {
			    if(eDashboardWidgetRatio.equals(EDashboardWidgetRatio.FULL)){
			    	return EDashboardWidgetRatio.FULL.toString().toLowerCase();
			    }
				String s = eDashboardWidgetRatio.toString();
				String[] splitor = s.split("_");
				return splitor[0].toLowerCase().concat(linker).concat(splitor[1].toLowerCase());
			}
		}
		return null;
	}
}