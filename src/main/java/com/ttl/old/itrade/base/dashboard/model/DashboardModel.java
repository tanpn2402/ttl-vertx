package com.ttl.old.itrade.base.dashboard.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author jay.wince
 * @since  2013-12-06
 */
public class DashboardModel implements Serializable ,Comparable<DashboardModel>{
    /**
	 * 
	 */
	private static final long serialVersionUID = 7499291343703785641L;
	private String layoutItems;
    private List<DashboardWidgetModel> widgets;
    private int index;
    private String id;//trading , news&research,ca&ipo
// SSOL0-0    
  private Map<String, String> namehashes;
// ESOL0-0
    
// SSOL0-1    
//    private String namehashes;
// ESOL0-1
    
//------------------------------------Jay on 20141104:Aggregate the settings into one: tool options.    
/*    
    private String editable  = "false";
    private String deletable = "false";
    private String sortable  = "true";
    private String addable   = "true";
*/
    
/*  private String active;*/
    
//  private List<DashboardLayoutModel> layoutItems;
//  -----------------Jay on 20141104:More tools option control.
//  -----------------Format: separated by comma
//  -----------------Enumerations: R-Refresh,M-More,D-Delete,A-Add widget,S-Save layout,G-sortable,E-editable  
//    private String toolOptions = "R,S";//Default value.
    private Map<String, Object> toolOptions;
    
//--Jay on 20141208:See DashboardCategoryModel.    
    private DashboardCategoryMgrModel mvCategoryMgrModel;
    
	public String getLayout() {
		return layoutItems;
	}
	public void setLayout(String layout) {
		this.layoutItems = layout;
	}
	public List<DashboardWidgetModel> getWidgets() {
		return widgets;
	}
	public void setWidgets(List<DashboardWidgetModel> widgets) {
		this.widgets = widgets;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
     
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
// RSOL0-0	
/*	
	public HashMap<String, String> getNamehashes() {
		return namehashes;
	}
	
	public void setNamehashes(HashMap<String, String> namehashes) {
		this.namehashes = namehashes;
	}
*/	
// RSOL0-1
	/*public void setNamehashes(String namehashes) {
		this.namehashes = namehashes;
	}
	
	public String getNamehashes() {
		return namehashes;
	}*/
	
	public void setNamehashes(Map<String, String> namehashes) {
		this.namehashes = namehashes;
	}
	
	public Map<String, String> getNamehashes() {
		return namehashes;
	}
	
/*	
	public String getEditable() {
		return editable;
	}
	public void setEditable(String editable) {
		this.editable = editable;
	}
	
	public String getDeletable() {
		return deletable;
	}
	
	public void setDeletable(String deletable) {
		this.deletable = deletable;
	}
	
	public void setSortable(String sortable) {
		this.sortable = sortable;
	}
	
	public String getSortable() {
		return sortable;
	}
	
	public void setAddable(String addable) {
		this.addable = addable;
	}
	public String getAddable() {
		return addable;
	}
*/	
	public Map<String, Object> getToolOptions(){
		return this.toolOptions;
	}
	
	public void setToolOptions(Map<String, Object> pToolOptions){
		this.toolOptions = pToolOptions;
	}
//--Jay on 20141208:See DashboardCategoryMgrModel.	
	public void setCategoryManager(DashboardCategoryMgrModel pCategoryMgrModel)
    {
        this.mvCategoryMgrModel = pCategoryMgrModel;
    }
	
	public DashboardCategoryMgrModel getCategoryManager()
    {
        return mvCategoryMgrModel;
	}
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(DashboardModel o) {
		return this.index - o.index;
	}
}
