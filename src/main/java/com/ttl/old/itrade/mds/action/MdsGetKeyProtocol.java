package com.ttl.old.itrade.mds.action;

import java.util.HashMap;
import java.util.Map;

public class MdsGetKeyProtocol {
	private String mvMessage;
	private Map<String, String> MappingData;
	
	public MdsGetKeyProtocol(String pvMessage) {
		Character SplitCode ='\u0001';
		this.MappingData = new HashMap(100);
		this.setMvMessage(pvMessage);
		String[] pvValue = this.mvMessage.split(SplitCode.toString());
		for(int i= 0;i<pvValue.length;++i) {
			String[] TempString = pvValue[i].split("=");
			getMappingData().put(TempString[0], TempString[1]);
		}
	}

	public String getKey(int pvKey) {
		try {
			return this.MappingData.get(String.valueOf(pvKey)).trim();
		}
		catch(Exception e ) {
			return "-";
		}
	}
	
	public String getMvMessage() {
		return mvMessage;
	}

	public void setMvMessage(String mvMessage) {
		this.mvMessage = mvMessage;
	}



	public Map<String, String> getMappingData() {
		return MappingData;
	}



	public void setMappingData(HashMap<String, String> mappingData) {
		MappingData = mappingData;
	}
}
