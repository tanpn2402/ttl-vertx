package com.ttl.old.itrade.mds.bean;

import java.math.BigDecimal;

public class mdsStockInfoBean {
	private String mvStockCode;
	private String mvMarket;
	// --References
	private String mvCeiling;
	private String mvFloor;
	private String mvReferences;
	// --Best Bid
	private BigDecimal mvBidPrice1;
	private BigDecimal mvBidPrice2;
	private BigDecimal mvBidPrice3;
	private long mvBidVol1;
	private long mvBidVol2;
	private long mvBidVol3;
	// --Matching
	private String mvMatchPrice; // Price Matching currently
	private String mvMatchVol; // Volume matching currently
	private String mvMatchUpDown; // Compare Price Matching with References;
	private String mvMatchVolTotal; // Volume Total Matching
	// --Best Offer
	private BigDecimal mvOfferPrice1;
	private BigDecimal mvOfferPrice2;
	private BigDecimal mvOfferPrice3;
	private long mvOfferVol1;
	private long mvOfferVol2;
	private long mvOfferVol3;
	// --Price History
	private String mvOpen;
	private String mvHigh;
	private String mvLow;
	private String mvNomial; // Avg
	// --Foreign investment
	private String mvForeignForBuy;
	private String mvForeignForSell;
	private String mvForeignForRoom;

	public String getMvStockCode() {
		return mvStockCode;
	}

	public void setMvStockCode(String pvStockCode) {
		this.mvStockCode = pvStockCode;
	}

	public String getMvMarket() {
		return mvMarket;
	}

	public void setMvMarket(String mvMarket) {
		this.mvMarket = mvMarket;
	}

	public String getMvCeiling() {
		return mvCeiling;
	}

	public void setMvCeiling(String mvCeiling) {
		this.mvCeiling = mvCeiling;
	}

	public String getMvFloor() {
		return mvFloor;
	}

	public void setMvFloor(String mvFloor) {
		this.mvFloor = mvFloor;
	}

	public String getMvReferences() {
		return mvReferences;
	}

	public void setMvReferences(String mvReferences) {
		this.mvReferences = mvReferences;
	}

	public String getMvMatchPrice() {
		return mvMatchPrice;
	}

	public void setMvMatchPrice(String mvMatchPrice) {
		this.mvMatchPrice = mvMatchPrice;
	}

	public String getMvMatchVol() {
		return mvMatchVol;
	}

	public void setMvMatchVol(String mvMatchVol) {
		this.mvMatchVol = mvMatchVol;
	}

	public String getMvMatchUpDown() {
		return mvMatchUpDown;
	}

	public void setMvMatchUpDown(String mvMatchUpDown) {
		this.mvMatchUpDown = mvMatchUpDown;
	}

	public String getMvMatchVolTotal() {
		return mvMatchVolTotal;
	}

	public void setMvMatchVolTotal(String mvMatchVolTotal) {
		this.mvMatchVolTotal = mvMatchVolTotal;
	}

	public String getMvOpen() {
		return mvOpen;
	}

	public void setMvOpen(String mvOpen) {
		this.mvOpen = mvOpen;
	}

	public String getMvHigh() {
		return mvHigh;
	}

	public void setMvHigh(String mvHigh) {
		this.mvHigh = mvHigh;
	}

	public String getMvLow() {
		return mvLow;
	}

	public void setMvLow(String mvLow) {
		this.mvLow = mvLow;
	}

	public String getMvNomial() {
		return mvNomial;
	}

	public void setMvNomial(String mvNomial) {
		this.mvNomial = mvNomial;
	}

	public String getMvForeignForBuy() {
		return mvForeignForBuy;
	}

	public void setMvForeignForBuy(String mvForeignForBuy) {
		this.mvForeignForBuy = mvForeignForBuy;
	}

	public String getMvForeignForSell() {
		return mvForeignForSell;
	}

	public void setMvForeignForSell(String mvForeignForSell) {
		this.mvForeignForSell = mvForeignForSell;
	}

	public String getMvForeignForRoom() {
		return mvForeignForRoom;
	}

	public void setMvForeignForRoom(String mvForeignForRoom) {
		this.mvForeignForRoom = mvForeignForRoom;
	}

	public BigDecimal getMvBidPrice1() {
		return mvBidPrice1;
	}

	public void setMvBidPrice1(BigDecimal mvBidPrice1) {
		try {
			this.mvBidPrice1 = mvBidPrice1;
		} catch (Exception e) {

		}
	}

	public BigDecimal getMvBidPrice2() {
		return mvBidPrice2;
	}

	public void setMvBidPrice2(BigDecimal mvBidPrice2) {
		this.mvBidPrice2 = mvBidPrice2;
	}

	public BigDecimal getMvBidPrice3() {
		return mvBidPrice3;
	}

	public void setMvBidPrice3(BigDecimal mvBidPrice3) {
		this.mvBidPrice3 = mvBidPrice3;
	}

	public long getMvBidVol1() {
		return mvBidVol1;
	}

	public void setMvBidVol1(long mvBidVol1) {
		this.mvBidVol1 = mvBidVol1;
	}

	public long getMvBidVol2() {
		return mvBidVol2;
	}

	public void setMvBidVol2(long mvBidVol2) {
		this.mvBidVol2 = mvBidVol2;
	}

	public long getMvBidVol3() {
		return mvBidVol3;
	}

	public void setMvBidVol3(long mvBidVol3) {
		this.mvBidVol3 = mvBidVol3;
	}

	public BigDecimal getMvOfferPrice1() {
		return mvOfferPrice1;
	}

	public void setMvOfferPrice1(BigDecimal mvOfferPrice1) {
		this.mvOfferPrice1 = mvOfferPrice1;
	}

	public BigDecimal getMvOfferPrice2() {
		return mvOfferPrice2;
	}

	public void setMvOfferPrice2(BigDecimal mvOfferPrice2) {
		this.mvOfferPrice2 = mvOfferPrice2;
	}

	public BigDecimal getMvOfferPrice3() {
		return mvOfferPrice3;
	}

	public void setMvOfferPrice3(BigDecimal mvOfferPrice3) {
		this.mvOfferPrice3 = mvOfferPrice3;
	}

	public long getMvOfferVol1() {
		return mvOfferVol1;
	}

	public void setMvOfferVol1(long mvOfferVol1) {
		this.mvOfferVol1 = mvOfferVol1;
	}

	public long getMvOfferVol2() {
		return mvOfferVol2;
	}

	public void setMvOfferVol2(long mvOfferVol2) {
		this.mvOfferVol2 = mvOfferVol2;
	}

	public long getMvOfferVol3() {
		return mvOfferVol3;
	}

	public void setMvOfferVol3(long mvOfferVol3) {
		this.mvOfferVol3 = mvOfferVol3;
	}
}
