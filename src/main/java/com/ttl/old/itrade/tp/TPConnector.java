package com.ttl.old.itrade.tp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.net.SocketException;
import java.text.DecimalFormat;

import com.ttl.old.itrade.util.Log;
/**
 * This class for TP connect
 * @author not attribute
 *
 */
public class TPConnector
{
	// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090818 Add log - Issue #: Publish connection status
	public static boolean svIsConnected = false;
	
	// Giang Tran #: add the variable to check 
	public static boolean svExternalIsConnected = false;
	
	// BEGIN - Task #: YuLong Xu 20090818
	//BEGIN TASK # TTL-HK-WLCW-00951 Walter Lau 17 Dec 2009 set ReceiveBufferLen to configurable
	private int						ivReceiveBufferLen = 10000000;
	private int						ivHeaderLength = 10;
	//END TASK # TTL-HK-WLCW-00951 Walter Lau 17 Dec 2009 set ReceiveBufferLen to configurable
	private DecimalFormat			ivHeaderFormater = new DecimalFormat("0000000000");

	private Socket					ivTPSocket;
	private String[]				ivIPAddress = new String[10];
    private int[]					ivPort = new int[10];
	private int						ivTPNumber = 0;
	private int						ivTPNumberCounter = 0;
	private boolean					ivIsRetried = false;
	

	private TPConnectorData			ivSocketData = null;
	private BufferedOutputStream	ivBufferedOutputStream;
	private BufferedInputStream		ivBufferedInputStream;

	/**
	 * Indicator for connection establishment
	 */
	private boolean					ivIsConnected = false;

	// Timeout(in ms) to read from socket
	private int						ivReadTimeout = 0;
	
	// Loop counter for stack trace
	private int						ivLoopCounter = 0;

	//BEGIN TASK # TTL-HK-WLCW-00951 Walter Lau 17 Dec 2009 set ReceiveBufferLen to configurable
	/**
	 * Default constructor for TPConnector class
	 * @param pTPIPAddress the tp ip address
	 * @param pPort the port
	 */
	public TPConnector(String[] pTPIPAddress, int[] pPort, int pReceiveBufferLen, int pHeaderLength)
	{
		// For Debug
		Log.println("New TPConnector is created.", Log.ACCESS_LOG);
		
		// Modified by Bowen Chau on 6 June 2006
		ivTPNumber = pTPIPAddress.length;
		ivReceiveBufferLen = pReceiveBufferLen;
		ivHeaderLength = pHeaderLength;
		for (int i = 0; i < pTPIPAddress.length; i++) {
			ivIPAddress[i] = pTPIPAddress[i].trim();
            ivPort[i] = pPort[i];
		}		
	}
	//END TASK # TTL-HK-WLCW-00951 Walter Lau 17 Dec 2009 set ReceiveBufferLen to configurable


	/**
	 * The method for is connected
	 * @return the is connected
	 */
	public synchronized boolean isConnected()
	{
		return ivIsConnected;
	}
	/**
	 * The method for read time out in second
	 * @param pValue the read time out in second
	 */
	public void setReadTimeoutInSecond(int pValue)
	{

		// zero for infinite read
		ivReadTimeout = pValue * 1000;
	}
	/**
	 * This method for start TP connect
	 * @return connect Success return true,connect Failure return false
	 */
	public synchronized boolean startConnect()
	{
		try
		{
			if (!ivIsConnected)
			{
				if (!ivIsRetried) {
					ivTPNumberCounter = 0;
					ivIsRetried = true;
				} else {
					ivTPNumberCounter++;
					if (ivTPNumberCounter >= ivTPNumber) {
						ivTPNumberCounter = 0;
					}
				}
				Log.println("iTrade is now trying to connect " + ivIPAddress[ivTPNumberCounter], Log.ACCESS_LOG);
				ivTPSocket = new Socket(ivIPAddress[ivTPNumberCounter], ivPort[ivTPNumberCounter]);


				// ivTPSocket.setSoTimeout(ivReadTimeout);

				ivBufferedOutputStream = new BufferedOutputStream(ivTPSocket.getOutputStream());
				ivBufferedInputStream = new BufferedInputStream(ivTPSocket.getInputStream());

				// create a new socket data to receive data
                //BEGIN TASK # TTL-HK-WLCW-00951 Walter Lau 17 Dec 2009 set ReceiveBufferLen to configurable
                Log.println("iTrade connect buffer length is " + ivReceiveBufferLen , Log.ACCESS_LOG);
				ivSocketData = new TPConnectorData(ivHeaderLength, ivReceiveBufferLen);
				//END TASK # TTL-HK-WLCW-00951 Walter Lau 17 Dec 2009 set ReceiveBufferLen to configurable
				ivSocketData.init(ivBufferedInputStream);

				ivIsConnected = true;
				// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090818 Add log - Issue #: Publish connection status
				svIsConnected = true;
				svExternalIsConnected = true;
				// BEGIN - Task #: YuLong Xu 20090818
				ivIsRetried = false;
			}
		}
		catch (Exception lvException)
		{
			ivIsConnected = false;
			// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090818 Add log - Issue #: Publish connection status
			//svIsConnected = false;
			//svExternalIsConnected = false;
			// BEGIN - Task #: YuLong Xu 20090818
		}
		return ivIsConnected;
	}

	/**
	 * Disconnects the socket connection
	 */
	public synchronized void disconnect(boolean isExternal)
	{
		ivIsConnected = false;
		// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090818 Add log - Issue #: Publish connection status
		if(isExternal){
			svExternalIsConnected = false;
		} else {
			svIsConnected = false;
		}

		if (ivSocketData != null)
		{
			try
			{
				ivSocketData.cleanUp();
			}
			catch (Exception lvException) {}
			ivSocketData = null;
		}

		if (ivBufferedInputStream != null)
		{
			try
			{
				ivBufferedInputStream.close();
			}
			catch (Exception lvException) {}
			ivBufferedInputStream = null;
		}

		if (ivBufferedOutputStream != null)
		{
			try
			{
				ivBufferedOutputStream.close();
			}
			catch (Exception lvException) {}
			ivBufferedOutputStream = null;
		}

		if (ivTPSocket != null)
		{
			try
			{
				ivTPSocket.close();
			}
			catch (Exception lvException) {}
			ivTPSocket = null;
		}
	}


	/**
	 * Sends data to TP
	 * @param pMessage is message that needs to be sent
	 * @return Returns the result, true is success, false is fail
	 */
	public synchronized void send(byte[] pMessage) throws Exception
	{

		// ivIsConnected may not be true
		// because if TP is re-started, this flag is true
		// so re-try is necessary
		if (!ivIsConnected)
		{
			if (!startConnect())
			{
				throw new Exception("No connection with TP");
			}
		}

		// int lvLen = svHeaderLength + 2 + pMessage.length;
		String  lvXMLHeader = ivHeaderFormater.format(pMessage.length) + "\r\n";

		try
		{
			ivBufferedOutputStream.write(lvXMLHeader.getBytes("UTF8"));
			ivBufferedOutputStream.write(pMessage);
			ivBufferedOutputStream.flush();
		}
		catch (IOException lvException)
		{

			// ivIsConnected is true, but actually connection was lost
			// re-try once
			disconnect(false);
			startConnect();
//			if (startConnect())
//			{
//				ivBufferedOutputStream.write(lvXMLHeader.getBytes("UTF8"));
//				ivBufferedOutputStream.write(pMessage);
//				ivBufferedOutputStream.flush();
//			}
//			else
//			{
				throw new Exception("Connection to TP disconnected. Retry failed.");
//			}
		}
	}
	/**
	 * This method for extracted message
	 * @return the extracted Message
	 * @throws Exception the IOException
	 */
	public String receive(boolean isExternal) throws Exception
	{
		String  lvExtractedMessage = null;
		
		// Issue new ID
		if (ivLoopCounter <= 10000) {
			ivLoopCounter++;
		} else {
			ivLoopCounter = 0;
		}

		if (!ivIsConnected)
		{
			throw new IOException("No connection with TP");
		}

		try
		{		
			// For Debug
			Log.println("[ TPConnector.receive(): ID: " + ivLoopCounter + " Extracting received message. ]", Log.ACCESS_LOG);

			String  lvReceivedMesg = ivSocketData.extract();
			return lvReceivedMesg;
		}
		catch (InterruptedIOException lvInterruptedIOException)
		{

			// read time-out expired
			ivIsConnected = false;
			// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090818 Add log - Issue #: Publish connection status
			if(isExternal) {
				svExternalIsConnected = false;
			} else {
				svIsConnected = false;
			}
			// BEGIN - Task #: YuLong Xu 20090818
			disconnect(isExternal);
			Log.println("[ TPConnector.receive(): ID: " + ivLoopCounter + " Catches Interrupted IO Exception. ]", Log.ACCESS_LOG);
			throw new Exception("Connection with Gateway was lost");
		}
		catch (SocketException lvSocketException) {
			// Socket exception : Connection reset
			ivIsConnected = false;
			// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090818 Add log - Issue #: Publish connection status
			if(isExternal) {
				svExternalIsConnected = false;
			} else {
				svIsConnected = false;
			}
			// BEGIN - Task #: YuLong Xu 20090818
			disconnect(isExternal);
			Log.println("[ TPConnector.receive(): ID: " + ivLoopCounter + " Catches Socket Exception. ]", Log.ACCESS_LOG);
			throw new Exception("Socket exception: " + lvSocketException.toString());
		}
		catch (IOException lvIOException)
		{
			// IO exception: close is closed
			ivIsConnected = false;
			// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090818 Add log - Issue #: Publish connection status
			if(isExternal) {
				svExternalIsConnected = false;
			} else {
				svIsConnected = false;
			}
			// BEGIN - Task #: YuLong Xu 20090818
			disconnect(isExternal);
			Log.println("[ TPConnector.receive(): ID: " + ivLoopCounter + " Catches IO Exception. ]", Log.ACCESS_LOG);
			throw new Exception("Connection lost: " + lvIOException.toString());
		}
		catch (Exception lvException)
		{
			// Exception: message parsing error
			ivIsConnected = false;
			// BEGIN - Task #: TTL-GZ-XYL-00090 - YuLong Xu 20090818 Add log - Issue #: Publish connection status
			if(isExternal) {
				svExternalIsConnected = false;
			} else {
				svIsConnected = false;
			}
			// BEGIN - Task #: YuLong Xu 20090818
			disconnect(isExternal);
			Log.println("[ TPConnector.receive(): ID: " + ivLoopCounter + " Catches Exception. ]", Log.ACCESS_LOG);
			throw lvException;
		}
	}

}
