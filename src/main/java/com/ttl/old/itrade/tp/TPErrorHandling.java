package com.ttl.old.itrade.tp;

/**
 * Title:        TP transaction Error Handling
 * Description:  This class detects any error in the TP transaction
 * Copyright:    Copyright (c) 2002
 * Company:      SYSTEK IT Ltd.
 * @author
 * @version 1.0
 * @since 2002.12.13
 */

import com.systekit.common.msg.*;
import com.ttl.old.itrade.interfaces.IMain;

public class TPErrorHandling
{

	public static final String  APP_ERR = "A_ER0001";
	public static final String  SYS_ERR = "ERROR";
	// check login account  
	public static final String	USER_ERROR = "NACK";
	
	public static final String  DESC = "DESC";
	public static final String  C_ERROR_CODE = "C_ERROR_CODE";
	public static final String  C_ERROR_DESC = "C_ERROR_DESC";
	public static final String  C_ERROR_MESSAGE = "Message";
	public static final int		TP_NORMAL = 0;
	public static final int		TP_APP_ERR = 1;
	public static final int		TP_SYS_ERR = 2;	
	public static final int		TP_USER_ERR = 3;	

	
	private int					status;
	private String				description;
	private String				errCode;
	/**
	 * This method for check TP error
	 * @param lvRetNode the return node
	 * @return check error code
	 */
	public int checkError(IMsgXMLNode lvRetNode)
	{

		String  nodeName = lvRetNode.getName();
		if (nodeName.equalsIgnoreCase(SYS_ERR))
		{
			setStatus(TP_SYS_ERR);
			setErrDesc(lvRetNode.getChildNode(DESC).getValue());
		}
		else if (nodeName.equalsIgnoreCase(APP_ERR))
		{
			setStatus(TP_APP_ERR);
			setErrCode(lvRetNode.getChildNode(C_ERROR_CODE).getValue());
			setErrDesc(lvRetNode.getChildNode(C_ERROR_DESC).getValue());
		}
		//check login fail error
		else if ( IMain.getProperty("AgentIgnoreCheckPass") != null &&  IMain.getProperty("AgentIgnoreCheckPass").trim().equalsIgnoreCase("false") &&  nodeName.equalsIgnoreCase(USER_ERROR))
		{
			setStatus(TP_USER_ERR);			
			setErrDesc(lvRetNode.getChildNode(C_ERROR_MESSAGE).getValue());			
		}			
		else
		{
			setStatus(TP_NORMAL);
		}
		return getStatus();
	}

	/**
	 * Get method for status
	 * @return the status
	 */
	public int getStatus()
	{
		return status;
	}
	/**
	 * Set method for status
	 * @param pStatus the status
	 */
	public void setStatus(int pStatus)
	{
		this.status = pStatus;
	}

	/**
	 * Get method for TP error code
	 * @return the TP error code
	 */
	public String getErrCode()
	{
		return errCode;
	}
	/**
	 * Set method for TP error code
	 * @param pErrCode the TP error code
	 */
	public void setErrCode(String pErrCode)
	{
		this.errCode = pErrCode;
	}
	/**
	 * Get method for TP error description
	 * @return the TP error description
	 */
	public String getErrDesc()
	{
		return description;
	}
	
	/**
	 * Set method for TP error description 
	 * @param pDescription the TP error description
	 */
	public void setErrDesc(String pDescription)
	{
		this.description = pDescription;
	}
}
