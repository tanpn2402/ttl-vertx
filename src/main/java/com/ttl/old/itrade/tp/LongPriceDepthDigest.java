package com.ttl.old.itrade.tp;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

/**
 * The LongPriceDepthDigest class defined method that are depth digest the long price.
 * @author
 */
public class LongPriceDepthDigest
{
	/**
	 * The class defined price element.
	 */
	private class PriceElement
	{
		int mvQty;
		int mvNumberOfBroker;
	}

	/**
	 * The class defined methods that is compare the ask ordering.
	 */
	public class AskOrdering implements Comparator // ascending ordering
	{
		/**
		 * This method comparison of the two parameters is used to sort.
		 */
		public int compare(Object o1, Object o2)
		{
			int i1 = ( (Integer) o1).intValue();
			int i2 = ( (Integer) o2).intValue();

			return i1 - i2;
		}

		/**
		 * This method eirected to whether some other object with this object "equal".
		 */
		public boolean equals(Object obj)
		{
			if (compare(this, obj) == 0)
				return true;
			else
				return false;
		}
	}

	/**
	 * The class defined methods that is compare the bid ordering. 
	 */
	public class BidOrdering implements Comparator // descending ordering
	{
		/**
		 * This method comparison of the two parameters is used to sort.
		 */
		public int compare(Object o1, Object o2)
		{
			int i1 = ( (Integer) o1).intValue();
			int i2 = ( (Integer) o2).intValue();

			return i2 - i1;
		}

		/**
		 * This method eirected to whether some other object with this object "equal".
		 */
		public boolean equals(Object obj)
		{
			if (compare(this, obj) == 0)
				return true;
			else
				return false;
		}
	}

	/**
	 * This method get digest long price depth.
	 * @param pData the data.
	 * @param pBidAsk the bid or ask.
	 * @return A string of order map.
	 */
	// pData likes 10000,4,10011,2,11512,5,11512,5
	// return 10000,4,1,10011,2,1,11512,10,2
	public String getDigestedLongPirceDepth(String pData, String pBidAsk)
	{
		TreeMap lvMap = null;
		if (pBidAsk.equalsIgnoreCase("B"))
		{
			lvMap = new TreeMap(new BidOrdering()); // Price -> PriceElement
		}
		else
		{
			lvMap = new TreeMap(new AskOrdering()); // Price -> PriceElement
		}

		if (! pData.equalsIgnoreCase("0"))
		{
			StringTokenizer lvSK = new StringTokenizer(pData, ",");
			while (lvSK.hasMoreTokens())
			{
				Integer lvPrice = Integer.valueOf(lvSK.nextToken());

				lvSK.hasMoreTokens();
				int lvQty = Integer.parseInt(lvSK.nextToken());

				PriceElement lvPE = (PriceElement) lvMap.get(lvPrice);
				if (lvPE == null)
				{
					lvPE = new PriceElement();
					lvMap.put(lvPrice, lvPE);
				}
				lvPE.mvQty += lvQty;
				lvPE.mvNumberOfBroker++;
			}
		}
		return toAppletString(lvMap);
	}

	/**
	 * This method make map to applet string.
	 * @param pMap the data map.
	 * @return A applet string.
	 */
	private String toAppletString(Map pMap)
	{
		StringBuffer lvRet = new StringBuffer();

		for (Iterator lvIter = pMap.entrySet().iterator(); lvIter.hasNext(); )
		{
			Map.Entry lvEntry = (Map.Entry) lvIter.next();
			String lvPrice = lvEntry.getKey().toString();
			PriceElement lvPE = (PriceElement) lvEntry.getValue();
			lvRet.append(lvPrice).append(",");
			lvRet.append(lvPE.mvQty).append(",");
			lvRet.append(lvPE.mvNumberOfBroker + ",");
		}

		String lvAppletString = lvRet.toString();
		if (lvAppletString.length() > 0)
			lvAppletString = lvAppletString.substring(0, lvAppletString.length()-1);
		return lvAppletString;
	}

	/**
	 * This method to dump thd map.
	 * @param pMap the specified map.
	 */
	private void dumpMap(Map pMap)
	{
		for (Iterator lvIter = pMap.entrySet().iterator(); lvIter.hasNext(); )
		{
			Map.Entry lvEntry = (Map.Entry) lvIter.next();
			PriceElement lvPE = (PriceElement) lvEntry.getValue();
			System.out.println(lvEntry.getKey().toString() + ", " + lvPE.mvQty + "(" + lvPE.mvNumberOfBroker + ")");
		}
	}
}
