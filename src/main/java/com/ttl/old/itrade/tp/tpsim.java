package com.ttl.old.itrade.tp;

import java.util.*;
import java.io.*;
import java.net.*;
import java.text.*;
import com.systekit.common.msg.*;

public class tpsim extends Thread
{
	private TPConnector				ivTPConnector;
	static private int				svReceiveBufferLen = 300000;
	static private int				svHeaderLength = 10;
	private Socket					ivSocket;
	private Hashtable				ivReplyMap = new Hashtable();
	private IMsgXMLParser			ivParser;
	private BufferedOutputStream	ivBufferedOutputStream;
	private BufferedInputStream		ivBufferedInputStream;
	private DecimalFormat			ivHeaderFormater = new DecimalFormat("0000000000");
	private String					ivPath = "C:\\ITrade\\com\\itrade\\hsi\\tp\\TpRequestXML\\";

	public tpsim(Socket pSocket) throws Exception
	{
		ivSocket = pSocket;

//		MsgManager.setParserClassName("com.systekit.common.msg.MsgSpeedXMLParser");
		ivParser = MsgManager.createParser();

		ivReplyMap.put("HSIOR004Q01", "R_CancelOrder.xml");
		ivReplyMap.put("HSISN003Q01", "R_ChangePassword.xml");
		ivReplyMap.put("HSIOR001Q02", "R_ClientInformation.xml");
		ivReplyMap.put("HSISN004Q01", "R_DisclaimerAgreement.xml");
		ivReplyMap.put("COMCM001Q01", "R_DownloadSeries.xml");
		ivReplyMap.put("HSISN001Q01", "R_Login.xml");
		ivReplyMap.put("HSISN002Q01", "R_Logout.xml");
		ivReplyMap.put("HSIMF004Q01", "R_LongPriceDepth.xml");
		ivReplyMap.put("HSIOR001Q03", "R_MarginStatus.xml");
		ivReplyMap.put("HSIOR002Q01", "R_OrderEnquiry.xml");
		ivReplyMap.put("HSIOR001Q01", "R_PlaceOrder.xml");
		ivReplyMap.put("HSIMF002Q01", "R_PriceDepth.xml");
		ivReplyMap.put("HSIMF001Q01", "R_PriceInformation.xml");
		ivReplyMap.put("HSIUT001Q01", "R_UnscribePriceDepth.xml");

		/***	added by MAy on 20-01-2004	***/
		ivReplyMap.put("HSIOR003Q01", "R_ModifyOrder.xml");
		/******************************************/

		// ivReplyMap.put("HSIUT001Q01", "UnscribePriceInformation.xml");
	}

	private IMsgXMLNode getReplyNode(String pMessageName) throws Exception
	{
		String  lvFullPath = ivPath + (String) ivReplyMap.get(pMessageName);
		return ivParser.parseXML(readFile(lvFullPath));
	}

	private String readFile(String pFileName) throws Exception
	{
		BufferedReader  lvReader = new BufferedReader(new InputStreamReader(new FileInputStream(pFileName)));

		StringBuffer	sb = new StringBuffer();
		String			s = "";
		while ((s = lvReader.readLine()) != null)
		{
			sb.append(s);
		}
		return sb.toString().replace('\t', ' ');
	}

	public static void main(String[] argv) throws Exception
	{
		ServerSocket	ss = new ServerSocket(3000);
		while (true)
		{
			Socket  lvSocket = ss.accept();
			tpsim   tpsim1 = new tpsim(lvSocket);
			tpsim1.start();
		}
	}

	public void run()
	{
		try
		{
			while (true)
			{
				ivBufferedOutputStream = new BufferedOutputStream(ivSocket.getOutputStream());
				ivBufferedInputStream = new BufferedInputStream(ivSocket.getInputStream());

				// create a new socket data to receive data
				TPConnectorData lvSocketData = new TPConnectorData(svHeaderLength, svReceiveBufferLen);
				lvSocketData.init(ivBufferedInputStream);
				while (true)
				{
					String  lvReceivedMesg = lvSocketData.extract();	// .replace('\t', ' ');
					processMessage(lvReceivedMesg);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void processMessage(String pData) throws Exception
	{
		System.out.println("Request = " + pData);
		IMsgXMLNode lvIncomingNode = ivParser.parseUTF8XML(pData);
		String		lvName = lvIncomingNode.getName();

		IMsgXMLNode lvReplyNode = getReplyNode(lvName);
		lvReplyNode.setAttribute("resvr", lvIncomingNode.getAttribute("resvr"));

		// change windowid
		if (lvIncomingNode.getChildNode("WINDOWSID") != null)
		{
			if (lvReplyNode.getChildNode("WINDOWSID") != null)
			{
				lvReplyNode.getChildNode("WINDOWSID").setValue(lvIncomingNode.getChildNode("WINDOWSID").getValue());
			}
			else
			{
				lvReplyNode.addChildNode("WINDOWSID").setValue(lvIncomingNode.getChildNode("WINDOWSID").getValue());
			}
		}

		// change series id
		if (lvIncomingNode.getChildNode("SERIESID") != null)
		{
			if (lvReplyNode.getChildNode("SERIESID") != null)
			{
				lvReplyNode.getChildNode("SERIESID").setValue(lvIncomingNode.getChildNode("SERIESID").getValue());
			}
			else
			{
				lvReplyNode.addChildNode("SERIESID").setValue(lvIncomingNode.getChildNode("SERIESID").getValue());
			}
		}

		String  lvReplyXML = lvReplyNode.toString();
		System.out.println("Reply = " + lvReplyXML);
		send(lvReplyXML.getBytes("UTF8"));
	}

	public void send(byte[] pMessage)
	{

		// int lvLen = svHeaderLength + 2 + pMessage.length;
		String  lvXMLHeader = ivHeaderFormater.format(pMessage.length) + "\r\n";

		try
		{
			ivBufferedOutputStream.write(lvXMLHeader.getBytes("UTF8"));
			ivBufferedOutputStream.write(pMessage);
			ivBufferedOutputStream.flush();
		}
		catch (Exception e)
		{
			System.out.println(e.toString());
		}
	}
}
