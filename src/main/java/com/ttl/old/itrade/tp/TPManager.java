package com.ttl.old.itrade.tp;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

//import com.itrade.hks.txn.HKSDownloadInstrumentTxn;
import com.ttl.old.itrade.interfaces.IBroadcastManager;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLParser;

/**
 * This class for role of managing the operation of tp
 * @author not attribute
 *
 */
public class TPManager extends Thread
{
        public static int svRequestWaitTimeOut = 10000;
        public static String svProduct = "";

        // BEGIN TASK: TTL-HK-AHJ-00004 12-Feb-2010 [iTrade R4 BankComm] Instrument Download Multipart
       // public static HKSDownloadInstrumentTxn svDownloadInstrumentTxn;
        // END TASK: TTL-HK-AHJ-00004 12-Feb-2010 [iTrade R4 BankComm] Instrument Download Multipart
 
        public Vector ivDataReceived = new Vector(10000);
        //commented by mingl
        //public String ivTPAddress;
        //public int ivTPPort;

        private IBroadcastManager ivBroadcastManager = null;
        private TPConnector ivTPConnector;
        private String ivTPServerSide;
        private Hashtable ivRequestPendingMap = new Hashtable();
        private Hashtable ivRequestResultMap = new Hashtable();
        private IMsgXMLParser ivParser;
        private Hashtable ivRequestMap = new Hashtable();

        /***	added by May on 18-02-2004		***/
        public static Hashtable seriesVector = new Hashtable();
        public static Boolean svSeriesDownloaded = new Boolean(false);
        /***************************************/

        public static Map mvInstrumentVector = Collections.synchronizedMap(new HashMap());
        public static Map mvInstrumentNameVector = Collections.synchronizedMap(new HashMap());
        //public static Hashtable mvInstrumentVector = new Hashtable();

        private IMain mvIMain;
        
        /**
         * Default constructor for TPManager class
         * @param pTPConnector the TPConnector class
         * @param pIMain the IMain class
         * @param pTPServerSide the TP server side
         */
        public TPManager(TPConnector pTPConnector, IMain pIMain, String pTPServerSide)
        {
                //F : Front Office
                //E : External Server
                setName("TP-Manager");
                mvIMain = pIMain;

                ivTPConnector = pTPConnector;
                ivTPServerSide = pTPServerSide;
                ivParser = mvIMain.getParser();
        }

        /**
         * This method for register Request
         * @param lvRequestName the request name
         * @param lvRequest the TPBaseRequest class
         */
        public void registerRequest(String lvRequestName, TPBaseRequest lvRequest)
        {
                ivRequestMap.put(lvRequestName, lvRequest);
        }
        
        /**
         * Get method for request
         * @param lvRequestName the request name
         * @return the TPBaseRequest object
         */
        public TPBaseRequest getRequest(String lvRequestName)
        {
                return (TPBaseRequest) ivRequestMap.get(lvRequestName);
        }
        
        /**
         * This method for Send XML file information
         * @param pLinkID the link id
         * @param pData the date
         * @return the xml node message information
         * @throws Exception the Exception
         */
        public IMsgXMLNode send(String pLinkID, String pData) throws Exception
        {
        	// BEGIN TASK: TTL-HK-AHJ-00004 12-Feb-2010 [iTrade R4 BankComm] Instrument Download Multipart
        	// For a download instrument list request, send and do NOT wait for response.
        	IMsgXMLNode lvDataNode = ivParser.parseUTF8XML(pData);
        	if (lvDataNode.getName().compareTo("COMCM001Q01") == 0)
        	{
        		// use the link ID as the current conversation ID
        		String lvLinkID = lvDataNode.getAttribute("resvr");
        		//svDownloadInstrumentTxn.setLinkID(lvLinkID);
        		// send the request 
        		ivTPConnector.send(pData.getBytes("UTF8"));
        		return null;
        	}
        	// END TASK: TTL-HK-AHJ-00004 12-Feb-2010 [iTrade R4 BankComm] Instrument Download Multipart

            Object lvWaitObject = new Object();
            ivRequestPendingMap.put(pLinkID, lvWaitObject);
            try
            {
                synchronized (lvWaitObject)
                {
                    ivTPConnector.send(pData.getBytes("UTF8"));

                    lvWaitObject.wait(svRequestWaitTimeOut);

                    // now, the result is available, remove the result from map
                    return (IMsgXMLNode) ivRequestResultMap.remove(pLinkID);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                ivRequestPendingMap.remove(pLinkID);
            }
        }
        /**
         * This method  process received message
         * @param pData
         */
        private void processReceivedMessage(String pData)
        {
            try
            {
                //Log.println("Received: " + pData, Log.DEBUG_LOG);
                IMsgXMLNode lvRootNode = ivParser.parseUTF8XML(pData);
                String pLinkID = lvRootNode.getAttribute("resvr");

                if (pLinkID != null)
                {
                    Object lvWaitObject = ivRequestPendingMap.get(pLinkID);
                    
                    if (lvWaitObject != null)
                    {
                        ivRequestResultMap.put(pLinkID, lvRootNode);
                        synchronized (lvWaitObject)
                        {
                        	lvWaitObject.notifyAll();
                        }
                    }
                    // BEGIN TASK: TTL-HK-AHJ-00004 12-Feb-2010 [iTrade R4 BankComm] Instrument Download Multipart
                    // handle the case of download instrument list response
                    else if(lvRootNode.getName().compareTo("HKSFODL001D02") == 0)
                    {
                    	// simulate package dropping for debugging purpose
                    	//if (lvRootNode.getAttribute("Part").compareTo("5") == 0) return;
                    	
                    	// this is a flag to signal new arrival
                    	//TPManager.svDownloadInstrumentTxn.setHasNewArrival(true);
                    	// pass the newly arrived node to the downloader
                    //	svDownloadInstrumentTxn.downloadInstrument(lvRootNode);
                    }
                    // END TASK: TTL-HK-AHJ-00004 12-Feb-2010 [iTrade R4 BankComm] Instrument Download Multipart
                    else
                    {
                        Log.println("Broadcast message received, but don't have corresponding request mesg " + pData, Log.ERROR_LOG);
                    }
                }
                else
                {
                    //Log.println("[ Broadcast message received : " + lvRootNode, Log.ACCESS_LOG);

                    // un-solicited message was received
                    onBroadcastMessageReceived(lvRootNode);
                }
            }
            catch (Exception e)
            {
                Log.println("TP Manager throws " + e.toString() + " with message " + pData, Log.ERROR_LOG);
                Log.println(e, Log.ERROR_LOG);
            }
        }
        /**
         * This method on broadcast message received
         * @param pNode the IMsgXMLNode object
         */
    protected void onBroadcastMessageReceived(IMsgXMLNode pNode)
    {
        if (ivBroadcastManager != null)
        {
           //Log.println(pNode.toString(), Log.DEBUG_LOG);
           Log.println(pNode.toString(), Log.URL_RECEIVED_LOG);
           ivBroadcastManager.onBroadcastMessageReceived(pNode);
        }
    }
    /**
     * start a connector thread to received data
     */
        public void run()
        {
                // commented by mingl
                // start a connector thread to received data
//		TPConnectorThread lvTPConnectorThread = new TPConnectorThread(this, ivTPConnector);
//		lvTPConnectorThread.setName("TPConnector");
//		lvTPConnectorThread.start();

                Vector lvTempDataVector = new Vector(1000);
                String lvReceivedMessage;

                while (true)
                {
                        try
                        {
                                synchronized (ivDataReceived)
                                {
                                        if (ivDataReceived.size() == 0)
                                        {
                                                ivDataReceived.wait();
                                        }
                                        lvTempDataVector.addAll(ivDataReceived);
                                        ivDataReceived.clear();
                                }

                                for (Iterator lvIter = lvTempDataVector.iterator(); lvIter.hasNext(); )
                                {
                                        lvReceivedMessage = (String) lvIter.next();
                                        lvReceivedMessage = lvReceivedMessage.replace('\t', ' ');
                                        processReceivedMessage(lvReceivedMessage);
                                }
                                lvTempDataVector.clear();
                        }
                        catch (Exception e)
                        {
                                Log.println("TPManager throws " + e.toString(), Log.ERROR_LOG);
                        }
                }
        }

        /**
         * Sets broadcast manager
         * @param pBroadcastManager Broadcast Manager
         */
        public void setBroadcastManager(IBroadcastManager pBroadcastManager) {
                ivBroadcastManager = pBroadcastManager;
        }

        // added by mingl
        /**
         * Runs right after TPConnector is connected
         */
        public void onTPConnect()
        {
                Log.println("TP Server Side is : " + getTPServerSide(), Log.DEBUG_LOG);
                //if(getTPServerSide().equals("F"))
                //{
                mvIMain.onTPConnect();
                //}
        }
        /**
         * Runs right after External TPConnector is connected
         */
        public void onExternalTPConnect()
        {
                Log.println("TP Server Side is : External", Log.DEBUG_LOG);
                //if(getTPServerSide().equals("F"))
                //{
		mvIMain.onExternalTPConnect();
                //}
        }
        /**
         * Get method for TP Server side
         * @return the TP Server side
         */
        public String getTPServerSide()
        {
                return ivTPServerSide;
        }
}
