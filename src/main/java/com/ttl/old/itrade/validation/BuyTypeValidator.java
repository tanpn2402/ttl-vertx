package com.ttl.old.itrade.validation;

/**
 * <p>
 * Title: Buy type validation
 * </p>
 * <p>
 * Description: The BuyTypeValidator class defined methods to set the validation
 * rules for buy type.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class BuyTypeValidator extends DefaultFieldValidator {
	/**
	 * Constructor for BuyTypeValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pFieldValue
	 *            The field value
	 */
	public BuyTypeValidator(String pFieldName, String pFieldValue) {
		super(pFieldName, pFieldValue);
		_bIsValid = (pFieldValue.equals("B") || pFieldValue.equals("S"));
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 6
	 */
	public static int getErrCode() {
		return 6;
	}
}
