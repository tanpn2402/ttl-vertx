package com.ttl.old.itrade.validation;

import com.ttl.old.itrade.interfaces.Validator;

/**
 * <p>
 * Title: Default field validation
 * </p>
 * <p>
 * Description: The DefaultFieldValidator class defined methods to set the
 * validation rules for default fields.
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author not attributable
 * @version 1.0
 */
public class DefaultFieldValidator implements Validator {
	protected boolean _bIsValid = true;
	protected String _sFieldName = null;
	protected String _sValue = null;

	public DefaultFieldValidator() {
	}

	/**
	 * Constructor for DefaultFieldValidator class
	 * 
	 * @param pFieldName
	 *            The field name
	 * @param pValue
	 *            The field value
	 */
	public DefaultFieldValidator(String pFieldName, String pValue) {
		_sFieldName = pFieldName;
		_sValue = pValue;
	}

	/**
	 * @return _bIsValid A boolean to show is whether the field validated.
	 */
	public boolean isValid() {
		return _bIsValid;
	}

	/**
	 * A static method for getting error code
	 * 
	 * @return 1
	 */
	public static int getErrCode() {
		return 1;
	}
}
