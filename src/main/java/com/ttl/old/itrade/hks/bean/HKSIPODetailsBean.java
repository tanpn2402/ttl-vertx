package com.ttl.old.itrade.hks.bean;

/**
 * The HKSIPODetailsBean class define variables that to save values 
 * for action 
 * @author 
 */
public class HKSIPODetailsBean {
	private String mvStockCode;
	private String mvStockName;
	private String mvListingDate;
	private String mvEntitlementID;
	private String mvConfirmedPrice;
	private String mvOfferPrice;
	private String mvNumberofOfferShare;
	private String mvNumberofPubOfferShare;
	private String mvIPOClosingDate; 
	private String mvAppStatus;
	private String mvIPOOnlineClosingDate;
	private String mvProspectusLink;
	private String mvWebsite;
	
	/**
     * This method returns the id of stock.
     * @return the id of stock.
     */
	public String getMvStockCode() {
		return mvStockCode;
	}
	
	/**
     * This method sets the id of stock.
     * @param pStockCode The id of stock.
     */
	public void setMvStockCode(String pStockCode) {
		mvStockCode = pStockCode;
	}
	
	/**
     * This method returns the name of stock.
     * @return the name of stock.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the name of stock.
     * @param pStockName The name of stock.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the listing date.
     * @return the listing date.
     */
	public String getMvListingDate() {
		return mvListingDate;
	}
	
	/**
     * This method sets the listing date.
     * @param pListingDate The listing date.
     */
	public void setMvListingDate(String pListingDate) {
		mvListingDate = pListingDate;
	}
	
	/**
     * This method returns the entitlement id.
     * @return the entitlement id.
     */
	public String getMvEntitlementID() {
		return mvEntitlementID;
	}
	/**
     * This method sets the entitlement id.
     * @param pEntitlementID The entitlement id.
     */
	public void setMvEntitlementID(String pEntitlementID) {
		mvEntitlementID = pEntitlementID;
	}
	
	/**
     * This method returns the confirmed price.
     * @return the confirmed price.
     */
	public String getMvConfirmedPrice() {
		return mvConfirmedPrice;
	}
	
	/**
     * This method sets the confirmed pricewhich form action.
     * @param pConfirmedPrice The confirmed price.
     */
	public void setMvConfirmedPrice(String pConfirmedPrice) {
		mvConfirmedPrice = pConfirmedPrice;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price is formatted.
     */
	public String getMvOfferPrice() {
		return mvOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param pOfferPrice The offer price is formatted.
     */
	public void setMvOfferPrice(String pOfferPrice) {
		mvOfferPrice = pOfferPrice;
	}
	
	/**
     * This method returns the number of offer share.
     * @return the number of offer share.
     */
	public String getMvNumberofOfferShare() {
		return mvNumberofOfferShare;
	}
	
	/**
     * This method sets the number of offer share.
     * @param pNumberofOfferShare The number of offer share.
     */
	public void setMvNumberofOfferShare(String pNumberofOfferShare) {
		mvNumberofOfferShare = pNumberofOfferShare;
	}
	
	/**
     * This method returns the number of pub offer share.
     * @return the number of pub offer share.
     */
	public String getMvNumberofPubOfferShare() {
		return mvNumberofPubOfferShare;
	}
	
	/**
     * This method sets the number of pub offer share.
     * @param pNumberofPubOfferShare The number of pub offer share.
     */
	public void setMvNumberofPubOfferShare(String pNumberofPubOfferShare) {
		mvNumberofPubOfferShare = pNumberofPubOfferShare;
	}
	
	/**
     * This method returns the ipo closing date.
     * @return the ipo closing date.
     */
	public String getMvIPOClosingDate() {
		return mvIPOClosingDate;
	}
	
	/**
     * This method sets the ipo closing date.
     * @param pIPOClosingDate The ipo closing date.
     */
	public void setMvIPOClosingDate(String pIPOClosingDate) {
		mvIPOClosingDate = pIPOClosingDate;
	}
	
	/**
     * This method returns the apply status.
     * @return the apply status.
     */
	public String getMvAppStatus() {
		return mvAppStatus;
	}
	
	/**
     * This method sets the apply status.
     * @param pAppStatus The apply status.
     */
	public void setMvAppStatus(String pAppStatus) {
		mvAppStatus = pAppStatus;
	}
	
	/**
     * This method returns the ipo online closing date.
     * @return the ipo online closing date.
     */
	public String getMvIPOOnlineClosingDate() {
		return mvIPOOnlineClosingDate;
	}
	
	/**
     * This method sets the ipo online closing date.
     * @param pIPOOnlineClosingDate The ipo online closing date.
     */
	public void setMvIPOOnlineClosingDate(String pIPOOnlineClosingDate) {
		mvIPOOnlineClosingDate = pIPOOnlineClosingDate;
	}
	
	/**
     * This method returns the prospectus link.
     * @return the prospectus link.
     */
	public String getMvProspectusLink() {
		return mvProspectusLink;
	}
	
	/**
     * This method sets the prospectus link.
     * @param pProspectusLink The prospectus link.
     */
	public void setMvProspectusLink(String pProspectusLink) {
		mvProspectusLink = pProspectusLink;
	}
	
	/**
     * This method returns the web site.
     * @return the web site.
     */
	public String getMvWebsite() {
		return mvWebsite;
	}
	
	/**
     * This method sets the web site.
     * @param pWebsite The web site.
     */
	public void setMvWebsite(String pWebsite) {
		mvWebsite = pWebsite;
	}
}
