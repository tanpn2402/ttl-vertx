package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSMarketIndexTxn class definition for all method
 * Be accessed from the host market indices
 * @author not attributable
 *
 */
public class HKSMarketIndexTxn extends BaseTxn
{

	private String mvMarketID;
	private String mvLoopCounter;
	private String mvIsMarketIndex;
	/**
	 * Constructor for HKSMarketIndexTxn class
	 * @param pMarketID the market id
	 */
	public HKSMarketIndexTxn(String pMarketID)
    {
       super();
       mvMarketID = pMarketID;
    }

	/**
	 * This method process Be accessed from the host market indices
	 * @return Be accessed from the host market indices
	 */
	public Vector process()
    {
    	Hashtable lvTxnMap = new Hashtable();
    	lvTxnMap.put(TagName.MARKETID, getMarketID());

    	Vector lvReturn = new Vector();
    	if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSMarketIndexRequest, lvTxnMap))
    	{
    		HashMap lvIndex = null;
            IMsgXMLNodeList lvDetailList = mvReturnNode.getNodeList(TagName.DetailNode);
            for(int i = 0; i < lvDetailList.size() ; i++)
            {
                    IMsgXMLNode lvDetailNode = lvDetailList.getNode(i);
                    lvIndex = new HashMap();

                    lvIndex.put(TagName.PRODUCTID, lvDetailNode.getChildNode(TagName.PRODUCTID).getValue());
                    lvIndex.put(TagName.MARKETID, lvDetailNode.getChildNode(TagName.MARKETID).getValue());
                    lvIndex.put("MARKETTIME", lvDetailNode.getChildNode("MARKETTIME").getValue());
                    lvIndex.put("MARKETINDEX", lvDetailNode.getChildNode("MARKETINDEX").getValue());
                    lvIndex.put(TagName.INDEXID, lvDetailNode.getChildNode(TagName.INDEXID).getValue());
                    lvIndex.put("TOTALVALUE", lvDetailNode.getChildNode("TOTALVALUE").getValue());
                    lvIndex.put("TOTALVOLUME", lvDetailNode.getChildNode("TOTALVOLUME").getValue());
                    lvIndex.put("LASTTRADEDATEINDEX", lvDetailNode.getChildNode("LASTTRADEDATEINDEX").getValue());
                    lvIndex.put("INDEXCHANGE", lvDetailNode.getChildNode("INDEXCHANGE").getValue());
                    lvIndex.put("INDEXPERCENT", lvDetailNode.getChildNode("INDEXPERCENT").getValue());

//        	lvIndex.put("Index_ID", lvDetailNode.getChildNode("Index_ID").getValue());
//       	lvIndex.put("Total_Volume", lvDetailNode.getChildNode("Total_Volume").getValue());
//        	lvIndex.put("Up_Volume", lvDetailNode.getChildNode("Up_Volume").getValue());
//        	lvIndex.put("PCT_Index", lvDetailNode.getChildNode("PCT_Index").getValue());
//        	lvIndex.put("Total_Trade", lvDetailNode.getChildNode("Total_Trade").getValue());
                    /*
                                    lvMarketIndex.put(TagName.PRODUCTID, lvIndexMarketDataSModel.getProductID());
                                    lvMarketIndex.put(TagName.MARKETID, lvIndexMarketDataSModel.getMarketID());
                                    lvMarketIndex.put("MARKETTIME", new java.sql.Date(lvIndexMarketDataSModel.getMarketTime().getTime()).toString());
                                    lvMarketIndex.put(TagName.INDEXID, lvIndexMarketDataSModel.getIndexID());
                                    lvMarketIndex.put("TOTALVALUE", String.valueOf(lvIndexMarketDataSModel.getMarketTotalValue()));
                                    lvMarketIndex.put("TOTALVOLUME", String.valueOf(lvIndexMarketDataSModel.getMarketTotalTrade()));
                                    lvMarketIndex.put("LASTTRADEDATEINDEX", lvIndexMarketDataSModel.getLastTradeDateIndex() != null ? String.valueOf(lvIndexMarketDataSModel.getLastTradeDateIndex()) : "");
                     */

                    lvReturn.add(lvIndex);
            }
    	}

    	return lvReturn;
    }
	/**
	 * Get method for market id
	 * @return market id
	 */
	public String getMarketID()
    {
        return mvMarketID;
    }
	/**
	 * Set method for market id
	 * @param pMarketID the market id
	 */
	public void setMarketID(String pMarketID)
    {
        mvMarketID = pMarketID;
    }
	/**
	 * Get method for loop counter
	 * @return Loop counter counts the number of
	 */
	public int getLoopCounter()
    {
        int lvCnt;
        try
        {
            lvCnt = Integer.parseInt(mvLoopCounter);
        }
        catch (Exception e)
        {
            return -1;
        }
        return lvCnt;
    }
	/**
	 * Set method for loop counter
	 * @param pLoopCounter the loop counter
	 */
	public void setLoopCounter(String pLoopCounter)
    {
        mvLoopCounter = pLoopCounter;
    }

}
