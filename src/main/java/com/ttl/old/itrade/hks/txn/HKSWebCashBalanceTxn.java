package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.HKSTag;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.common.msg.IMsgXMLNode;

public class HKSWebCashBalanceTxn extends BaseTxn {
	private String mvClientId;
	private String mvTradingAccSeq;
	private String mvOutStandingLoan;
	private boolean mvIsMarginAcc;
	
	private String mvCSettled;
	private String mvManualReserve;
	public String getClientId() {
		return mvClientId;
	}

	public void setClientId(String mvClientId) {
		this.mvClientId = mvClientId;
	}

	public String getTradingAccSeq() {
		return mvTradingAccSeq;
	}

	public void setTradingAccSeq(String mvTradingAccSeq) {
		this.mvTradingAccSeq = mvTradingAccSeq;
	}

	
	public String getLedgerBalace() {
		return mvLedgerBalace;
	}

	public void setLedgerBalace(String mvLedgerBalace) {
		this.mvLedgerBalace = mvLedgerBalace;
	}

	
	public String getHoldAmount() {
		return mvHoldAmount;
	}

	public void setHoldAmount(String mvHoldAmount) {
		this.mvHoldAmount = mvHoldAmount;
	}

	public String getReserveAmount() {
		return mvReserveAmount;
	}

	public void setReserveAmount(String mvReserveAmount) {
		this.mvReserveAmount = mvReserveAmount;
	}

	public String getMarginValue() {
		return mvMarginValue;
	}

	public void setMarginValue(String mvMarginValue) {
		this.mvMarginValue = mvMarginValue;
	}

	public String getMarginableValue() {
		return mvMarginableValue;
	}

	public void setMarginableValue(String mvMarginableValue) {
		this.mvMarginableValue = mvMarginableValue;
	}

	public String getMarginPercentage() {
		return mvMarginPercentage;
	}

	public void setMarginPercentage(String mvMarginPercentage) {
		this.mvMarginPercentage = mvMarginPercentage;
	}

	public String getMarginPos() {
		return mvMarginPos;
	}

	public void setMarginPos(String mvMarginPos) {
		this.mvMarginPos = mvMarginPos;
	}

	public String getMarginCall() {
		return mvMarginCall;
	}

	public void setMarginCall(String mvMarginCall) {
		this.mvMarginCall = mvMarginCall;
	}

	public String getBuyingPowerd() {
		return mvBuyingPowerd;
	}

	public void setBuyingPowerd(String mvBuyingPowerd) {
		this.mvBuyingPowerd = mvBuyingPowerd;
	}

	public String getRemaining() {
		return mvRemaining;
	}

	public void setRemaining(String mvRemaining) {
		this.mvRemaining = mvRemaining;
	}

	
	public String getCTodayBuy() {
		return mvCTodayBuy;
	}

	public void setCTodayBuy(String mvCTodayBuy) {
		this.mvCTodayBuy = mvCTodayBuy;
	}

	public String getCTodaySell() {
		return mvCTodaySell;
	}

	public void setCTodaySell(String mvCTodaySell) {
		this.mvCTodaySell = mvCTodaySell;
	}

	public String getCTodayConfirmBuy() {
		return mvCTodayConfirmBuy;
	}

	public void setCTodayConfirmBu1y(String mvCTodayConfirmBuy) {
		this.mvCTodayConfirmBuy = mvCTodayConfirmBuy;
	}

	public String getCTodayConfirmSell() {
		return mvCTodayConfirmSell;
	}

	public void setCTodayConfirmSell(String mvCTodayConfirmSell) {
		this.mvCTodayConfirmSell = mvCTodayConfirmSell;
	}

	public String getMarketValue() {
		return mvMarketValue;
	}

	public void setMarketValue(String mvMarketValue) {
		this.mvMarketValue = mvMarketValue;
	}

	public String getCDueBuy() {
		return mvCDueBuy;
	}

	public void setCDueBuy(String mvCDueBuy) {
		this.mvCDueBuy = mvCDueBuy;
	}

	public String getCDueSell() {
		return mvDueSell;
	}

	public void setCDueSell(String mvDueSell) {
		this.mvDueSell = mvDueSell;
	}

	public String getUnsettleBuy() {
		return mvUnsettleBuy;
	}

	public void setUnsettleBuy(String mvUnsettleBuy) {
		this.mvUnsettleBuy = mvUnsettleBuy;
	}

	public String getCUnsettleSell() {
		return mvUnsettleSell;
	}

	public void setCUnsettleSell(String mvUnsettleSell) {
		this.mvUnsettleSell = mvUnsettleSell;
	}

	
	private String mvLedgerBalace;
	private String mvHoldAmount;
	private String mvReserveAmount;
	private String mvMarginValue;
	private String mvMarginableValue;
	private String mvMarginPercentage;
	private String mvMarginPos;
	private String mvMarginCall;
	private String mvBuyingPowerd;
	private String mvRemaining;
	private String mvCTodayBuy;
	private String mvCTodaySell;
	private String mvCTodayConfirmBuy;
	private String mvCTodayConfirmSell;
	private String mvMarketValue;
	private String mvCDueBuy;
	private String mvDueSell;
	private String mvUnsettleBuy;
	private String mvUnsettleSell;
	private String mvDrawableBalAmt;
	private String mvAvailAdvanceAmt;
	
	public HKSWebCashBalanceTxn(String pClientID,String pTradingAccSeq,boolean pIsMarinAcc){
		mvClientId = pClientID;
		mvTradingAccSeq = pTradingAccSeq;
		mvIsMarginAcc = pIsMarinAcc;
	}

	@SuppressWarnings("unchecked")
	public void process() {
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(HKSTag.CLIENTID, mvClientId);
		lvTxnMap.put(HKSTag.TRADINGACCSEQ, mvTradingAccSeq);
		lvTxnMap.put("ISMARGINACC", mvIsMarginAcc == true ? "Y":"N");
		
		if (TPErrorHandling.TP_NORMAL == process("HKSQueryWebCashBalanceInfoRequest", lvTxnMap)) {
			
			IMsgXMLNode childNode = mvReturnNode.getChildNode("ChildRow");
	         setLedgerBalace(childNode.getChildNode("LEDGERBAL").getValue());
	         setHoldAmount(childNode.getChildNode(HKSTag.CMANUALHOLD).getValue());
	         setReserveAmount(childNode.getChildNode(HKSTag.CMANUALRESERVE).getValue());
	         
	         setCTodayConfirmSell(childNode.getChildNode(HKSTag.CTODAYCONFIRMSELL).getValue());
	         setCTodayBuy(childNode.getChildNode(HKSTag.CTODAYBUY).getValue());
	         setCTodaySell(childNode.getChildNode(HKSTag.CTODAYSELL).getValue());

	         setCDueBuy(childNode.getChildNode(HKSTag.CDUEBUY).getValue());
	         setCDueSell(childNode.getChildNode(HKSTag.CDUESELL).getValue());
	         setCUnsettleSell(childNode.getChildNode(HKSTag.CUNSETTLESELL).getValue());

	         setDrawableBal(childNode.getChildNode(HKSTag.DRAWABLEBAL).getValue());

	         setMarginValue(childNode.getChildNode("MARGINVALUEF").getValue());
	         setMarginableValue(childNode.getChildNode("MARGINABLEVALUEF").getValue());
	      
	         setMarginCall(childNode.getChildNode("MARGINCALLF").getValue());
	         setBuyingPowerd(childNode.getChildNode("VIRTUALCHANNELTRADABLEBALF").getValue());

	         setMarketValue(childNode.getChildNode("MARKETVALUEF").getValue());
	         
	         setOutStandingLoan(childNode.getChildNode("LOANOUTSTANDINGF").getValue());
	         
	         setMvCSettled(childNode.getChildNode("CSETTLED").getValue());
	         setMvManualReserve(childNode.getChildNode("CMANUALRESERVE").getValue());
	         
	         setMvAvailAdvanceAmt(childNode.getChildNode("AVAILABLEADVANCEMONEY").getValue());
	         
	         
		} else { 
			 // Unhandled Business Exception
	         setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode("C_ERROR_DESC").getValue()));

	         // Handle special cases
	         // 1. Agreement is not signed
	         if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("HKSFOE00026")) {
	        	 setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode("C_ERROR_DESC").getValue()));
	         }

	         // 2. When TP is down
	         if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
	        	 setErrorCode(new ErrorCode(new String[0], "0100", "No connection with TP"));
	         }
	      }
	}

	public void setDrawableBal(String mvDrawableBalAmt) {
		this.mvDrawableBalAmt = mvDrawableBalAmt;
	}

	public String getDrawableBal() {
		return mvDrawableBalAmt;
	}

	public void setOutStandingLoan(String mvOutStandingLoan) {
		this.mvOutStandingLoan = mvOutStandingLoan;
	}

	public String getOutStandingLoan() {
		return mvOutStandingLoan;
	}

	public void setMvManualReserve(String mvManualReserve) {
		this.mvManualReserve = mvManualReserve;
	}

	public String getMvManualReserve() {
		return mvManualReserve;
	}

	public void setMvCSettled(String mvCSettled) {
		this.mvCSettled = mvCSettled;
	}

	public String getMvCSettled() {
		return mvCSettled;
	}

	public void setMvAvailAdvanceAmt(String mvAvailAdvanceAmt) {
		this.mvAvailAdvanceAmt = mvAvailAdvanceAmt;
	}

	public String getMvAvailAdvanceAmt() {
		return mvAvailAdvanceAmt;
	}
}
