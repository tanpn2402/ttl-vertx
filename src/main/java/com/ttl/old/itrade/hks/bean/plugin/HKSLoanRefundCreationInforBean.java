package com.ttl.old.itrade.hks.bean.plugin;

public class HKSLoanRefundCreationInforBean {
	private String advAvailable;
	private String loan;
	private String cashrsv;
	
	public String getAdvAvailable() {
		return advAvailable;
	}
	public void setAdvAvailable(String advAvailable) {
		this.advAvailable = advAvailable;
	}
	
	public String getCashrsv() {
		return cashrsv;
	}
	public void setCashrsv(String cashrsv) {
		this.cashrsv = cashrsv;
	}
	public String getLoan() {
		return loan;
	}
	public void setLoan(String loan) {
		this.loan = loan;
	}
}
