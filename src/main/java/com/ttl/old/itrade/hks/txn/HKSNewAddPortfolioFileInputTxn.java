//Begin Task #: TTL-GZ-ZYZ-00002   Yingzhi Zhang 20091202 [iTradeR5] Import/input transactions 
/**
 * 
 * @(#)HKSNewAddPortfolioFileInput.java        20091202
 *  
 * Copyright @${year} Transaction Technologies Limited.
 * Yingzhi Zhang GuangZhou, 020 ,China
 * All rights reserved. 
 * 
 *This software is the confidential and proprietary information of TTL,
 *Inc.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with TTL.
 * 
**/
package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.winvest.hks.config.mapping.TagName;
/**
 *	This class is a portfolio of information storage and processing
 * @author Yingzhi Zhang
 * @since 20091202
 */
public class HKSNewAddPortfolioFileInputTxn extends BaseTxn 
{
	private String mvClientID;
	private String mvNewQuery;
	private String mvFileInputList;
	private String mvLanguage;

	/**
	 * Constructor for HKSNewAddPortfolioFileInputTxn class
	 * @param pClientID the client id
	 * @param pNewQuery the new query
	 * @param pFileInputList the file input list
	 * @param pLanguage the user locale
	 */
	public HKSNewAddPortfolioFileInputTxn(String pClientID,String pNewQuery, 
			String pFileInputList,String pLanguage)
	{
		super();
		mvClientID = pClientID;
		mvNewQuery = pNewQuery;
		mvFileInputList = pFileInputList;
		mvLanguage = pLanguage;
	}
	/**
	 * Constructor for HKSNewAddPortfolioFileInputTxn class
	 * @param pClientID the client id
	 * @param pNewQuery the new query
	 * @param pFileInputList the file input list
	 */
	public HKSNewAddPortfolioFileInputTxn(String pClientID,String pNewQuery, String pFileInputList)
	{
		super();
		mvClientID = pClientID;
		mvNewQuery = pNewQuery;
		mvFileInputList = pFileInputList;
		mvLanguage = "en_US";
	}
	/**
	 * This method a portfolio of information storage and processing
	 * @return null
	 */
	public Vector process() 
	{
		Hashtable lvTxnMap = new Hashtable();
		
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put("NEWQUERY", mvNewQuery);
		lvTxnMap.put("FILEINPUTLIST", mvFileInputList);
		
		if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSNewAddPortfolioFileInput, lvTxnMap,mvLanguage)) 
		{
			System.out.println("**************** ok *******************");
		}
		
		return null;
	}
	/**
	 * Get method for client id
	 * @return client id
	 */
	public String getMvClientID() 
	{
		return mvClientID;
	}
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
	public void setMvClientID(String pClientID) 
	{
		mvClientID = pClientID;
	}

	/**
	 * Get method for new query
	 * @return new query
	 */
	public String getMvNewQuery() 
	{
		return mvNewQuery;
	}

	/**
	 * Set method for new query
	 * @param pNewQuery the new query
	 */
	public void setMvNewQuery(String pNewQuery) 
	{
		this.mvNewQuery = pNewQuery;
	}

	/**
	 * Get method for file input list
	 * @return file input list 
	 */
	public String getMvFileInputList() 
	{
		return mvFileInputList;
	}

	/**
	 * Set method for file input list 
	 * @param pFileInputList the file input list 
	 */
	public void setMvFileInputList(String pFileInputList)
	{
		this.mvFileInputList = pFileInputList;
	}

	/**
	 * Get method for user locale
	 * @return the mvLanguage
	 */
	public String getMvLanguage()
	{
		return mvLanguage;
	}

	/**
	 * Set method for user locale
	 * @param pLanguage the mvLanguage to set
	 */
	public void setMvLanguage(String pLanguage) 
	{
		this.mvLanguage = pLanguage;
	}
}

//End Task #: TTL-GZ-ZYZ-00002   Yingzhi Zhang 20091202 */
