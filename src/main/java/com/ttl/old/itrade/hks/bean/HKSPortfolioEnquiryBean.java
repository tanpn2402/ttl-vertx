package com.ttl.old.itrade.hks.bean;
/**
 * The HKSPortfolioEnquiryBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSPortfolioEnquiryBean {
	private String stockName;
	private String stockCode;
	private String queneBuy;
	private String queneSell;
	private String confirmBuy;
	private String confirmSell;
	private String manualHold;
	private String useableBal;
	private String marketValue;
	private String buyOrSell;
	
	/**
	 * This method returns the buy or sell.
	 * @return the buy or sell is not formatted.
     */
	public String getBuyOrSell() {
		return buyOrSell;
	}
	
	/**
     * This method sets the ledger balance.
     * @param pBuyOrSell The buy or sell.
     */
	public void setBuyOrSell(String pBuyOrSell) {
		buyOrSell = pBuyOrSell;
	}
	
	/**
	 * This method returns the quene buy.
	 * @return the quene buy is not formatted.
     */
	public String getQueneBuy() {
		return queneBuy;
	}
	
	/**
     * This method sets the quene buy.
     * @param pQueneBuy The quene buy.
     */
	public void setQueneBuy(String pQueneBuy) {
		queneBuy = pQueneBuy;
	}
	
	/**
 	 * This method returns the quene sell.
	 * @return the quene sell is not formatted.
     */
	public String getQueneSell() {
		return queneSell;
	}
	
	/**
     * This method sets the quene sell.
     * @param pQueneSell The quene sell.
     */
	public void setQueneSell(String pQueneSell) {
		queneSell = pQueneSell;
	}
	
	/**
	 * This method returns the confirm buy.
	 * @return the confirm buy is not formatted.
     */
	public String getConfirmBuy() {
		return confirmBuy;
	}
	
	/**
     * This method sets the confirm buy.
     * @param pConfirmBuy The confirm buy.
     */
	public void setConfirmBuy(String pConfirmBuy) {
		confirmBuy = pConfirmBuy;
	}
	
	/**
	 * This method returns the confirm sell.
	 * @return the confirm sell is not formatted.
     */
	public String getConfirmSell() {
		return confirmSell;
	}
	
	/**
     * This method sets the confirm sell.
     * @param pConfirmSell The confirm sell.
     */
	public void setConfirmSell(String pConfirmSell) {
		confirmSell = pConfirmSell;
	}
	
	/**
	 * This method returns the manual hold.
	 * @return the manual hold is not formatted.
     */
	public String getManualHold() {
		return manualHold;
	}
	
	/**
     * This method sets the manual hold.
     * @param pManualHold The manual hold.
     */
	public void setManualHold(String pManualHold) {
		manualHold = pManualHold;
	}
	
	/**
	 * This method returns the useable balance.
	 * @return the useable balance is not formatted.
     */
	public String getUseableBal() {
		return useableBal;
	}
	
	/**
     * This method sets the manual hold.
     * @param pUseableBal The manual hold.
     */
	public void setUseableBal(String pUseableBal) {
		useableBal = pUseableBal;
	}
	
	/**
	 * This method returns the market value.
	 * @return the market value is not formatted.
     */
	public String getMarketValue() {
		return marketValue;
	}
	
	/**
     * This method sets the market value.
     * @param pMarketValue The market value.
     */
	public void setMarketValue(String pMarketValue) {
		marketValue = pMarketValue;
	}
	
	/**
	 * This method returns the stock name.
	 * @return the stock name is not formatted.
     */
	public String getStockName() {
		return stockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name.
     */
	public void setStockName(String pStockName) {
		stockName = pStockName;
	}
	
	/**
	 * This method returns the stock code.
	 * @return the stock code is not formatted.
     */
	public String getStockCode() {
		return stockCode;
	}
	
	/**
     * This method sets the stock code.
     * @param pStockCode The stock code.
     */
	public void setStockCode(String pStockCode) {
		stockCode = pStockCode;
	}
}
