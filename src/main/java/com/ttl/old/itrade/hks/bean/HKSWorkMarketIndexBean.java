package com.ttl.old.itrade.hks.bean;


/**
 * The HKSEnterOrderConfirmActionBean class define variables that to save values 
 * for action 
 * @author Wind zhao
 *
 */
public class HKSWorkMarketIndexBean {
	private String indexName;
	private String indexValue;
	private String indexChange;
	private String percentChange;
	private boolean valueChanged;
	/**
	 * @return the indexName
	 */
	public String getIndexName() {
		return indexName;
	}
	/**
	 * @param indexName the indexName to set
	 */
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}
	/**
	 * @return the indexValue
	 */
	public String getIndexValue() {
		return indexValue;
	}
	/**
	 * @param indexValue the indexValue to set
	 */
	public void setIndexValue(String indexValue) {
		this.indexValue = indexValue;
	}
	/**
	 * @return the indexChange
	 */
	public String getIndexChange() {
		return indexChange;
	}
	/**
	 * @param indexChange the indexChange to set
	 */
	public void setIndexChange(String indexChange) {
		this.indexChange = indexChange;
	}
	/**
	 * @param percentChange the percentChange to set
	 */
	public void setPercentChange(String percentChange) {
		this.percentChange = percentChange;
	}
	/**
	 * @return the percentChange
	 */
	public String getPercentChange() {
		return percentChange;
	}
	public boolean isValueChanged() {
		return valueChanged;
	}
	public void setValueChanged(boolean valueChanged) {
		this.valueChanged = valueChanged;
	}
	
}
