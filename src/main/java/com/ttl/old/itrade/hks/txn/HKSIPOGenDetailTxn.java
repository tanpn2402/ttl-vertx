package com.ttl.old.itrade.hks.txn;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSIPOGenDetailTxn class definition for all method
 * From the host to obtain information on the details listed
 * @author not attributable
 *
 */
public class HKSIPOGenDetailTxn
{
    private String mvEntitlementId;
    private String mvClientId;
    private String mvLotSpreadDetail;
    private String mvCalculateFeeFlag;
    private String mvApplicationQty;
    private String mvTradingAccSeq;
    private String mvAccountSeq;
    private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;

    private HKSIPOGenInfoDetails   mvHKSIPOGenInfoDetails;
    private Vector mvIPOLotListVector;
    private Vector mvIPOAmountListVector;
    private BigDecimal mvHandlingFeeAmount;
    //Variable is used to identify
    public static final String CLIENTID = "CLIENTID";
    public static final String INSTRUMENTID = "INSTRUMENTID";
    public static final String ENTITLEMENTID = "ENTITLEMENTID";
    public static final String LOTSPREADDETAIL = "LOTSPREADDETAIL";
    public static final String CALCULATEFEEFLAG = "CALCULATEFEEFLAG";
    public static final String APPLIEDQTY = "APPLIEDQTY";
    public static final String OVERRIDEAMOUNT = "OVERRIDEAMOUNT";

    public static final String NUMBEROFOFFERSHARES = "NUMBEROFOFFERSHARES";
    public static final String NUMBEROFPUBLICOFFERSHARES = "NUMBEROFPUBLICOFFERSHARES";
    public static final String MINOFFERPRICE = "MINOFFERPRICE";
    public static final String MAXOFFERPRICE = "MAXOFFERPRICE";
    public static final String CONFIRMEDPRICE = "CONFIRMEDPRICE";
    public static final String ENDDATE = "ENDDATE";
    public static final String ENDTIME = "ENDTIME";
    public static final String EIPOENDDATE = "EIPOENDDATE";
    public static final String EIPOENDTIME = "EIPOENDTIME";
    public static final String STATUS = "STATUS";
    public static final String PROSPECTUS = "PROSPECTUS";
    public static final String CHINESEPROSPECTUS = "CHINESEPROSPECTUS";
    public static final String WEBSITE = "WEBSITE";
    // BEGIN RN00030 Ricky Ngan 20080923
    public static final String CHINESEWEBSITE = "CHWEBSITE";
    // END RN00030 
    public static final String LISTINGDATE = "LISTINGDATE";
    // BEGIN RN00028 Ricky Ngan 20080816
    public static final String INTERESTVALUEDATE = "INTERESTVALUEDATE";
    public static final String ALLOTMENTDATE = "ALLOTMENTDATE";
    public static final String INTCHANNELCUTOFFDATE = "INTCHANNELCUTOFFDATE";
    // END RN00028
    public static final String LOOP = "LOOP";
    public static final String LOOP_ELEMENT = "LOOP_ELEMENT";
    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling				tpError;
    Presentation presentation = null;

    /**
     * Default constructor for HKSIPOGenDetailTxn class
     * @param pPresentation the Presentation class
     */
    public HKSIPOGenDetailTxn(Presentation pPresentation)
    {
        tpError = new TPErrorHandling();
        presentation = pPresentation;
    }

    /**
     * Default constructor for HKSIPOGenDetailTxn class
     * @param pPresentation the Presentation class
     * @param pClientId the client id
     * @param pTradingAccSeq the trading account sequence
     * @param pAccountSeq the account sequence
     */
    public HKSIPOGenDetailTxn(Presentation pPresentation, String pClientId, String pTradingAccSeq, String pAccountSeq)
    {
        this(pPresentation);
        setClientId(pClientId);
        setTradingAccSeq(pTradingAccSeq);
        setAccountSeq(pAccountSeq);
    }
    /**
     * This method process From the host to obtain information on the details listed
     * @param pPresentation the Presentation class
     */
    public void process(Presentation pPresentation)
    {
        try
        {
            Hashtable		lvTxnMap = new Hashtable();

            IMsgXMLNode		lvRetNode;
            TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSIPODetailsRequest);
            TPBaseRequest   lvIPODetailsRequest = ivTPManager.getRequest(RequestName.HKSIPODetailsRequest);

            lvTxnMap.put(CLIENTID, getClientId());
            lvTxnMap.put(TagName.ACCOUNTSEQ, getAccountSeq());
            lvTxnMap.put(TagName.TRADINGACCSEQ, getTradingAccSeq());
            lvTxnMap.put(ENTITLEMENTID, getEntitlementId());
            lvTxnMap.put(LOTSPREADDETAIL, getLotSpreadDetail() == null ? "N" : getLotSpreadDetail());
            if ("Y".equalsIgnoreCase(getCalculateFeeFlag()))
            {
                lvTxnMap.put(CALCULATEFEEFLAG, "Y");
                lvTxnMap.put(APPLIEDQTY, getApplicationQty());
            }
            else
            {
                lvTxnMap.put(CALCULATEFEEFLAG, "N");
            }

            lvRetNode = lvIPODetailsRequest.send(lvTxnMap);

            setReturnCode(tpError.checkError(lvRetNode));
            if (mvReturnCode != TPErrorHandling.TP_NORMAL)
            {
                setErrorMessage(tpError.getErrDesc());
                if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
                {
                    setErrorCode(tpError.getErrCode());
                }
            }
            else
            {
                mvHKSIPOGenInfoDetails = new HKSIPOGenInfoDetails();

                if (lvRetNode.getChildNode(INSTRUMENTID) == null)
                    return;

                mvHKSIPOGenInfoDetails.setInstrumentID(lvRetNode.getChildNode(INSTRUMENTID).getValue());
                mvHKSIPOGenInfoDetails.setEntitlementID(lvRetNode.getChildNode(ENTITLEMENTID).getValue());
                mvHKSIPOGenInfoDetails.setNumberOfOfferShares(lvRetNode.getChildNode(NUMBEROFOFFERSHARES).getValue());
                mvHKSIPOGenInfoDetails.setNumberOfPublicOfferShares(lvRetNode.getChildNode(NUMBEROFPUBLICOFFERSHARES).getValue());
                mvHKSIPOGenInfoDetails.setMinOfferPrice(lvRetNode.getChildNode(MINOFFERPRICE).getValue());
                mvHKSIPOGenInfoDetails.setMaxOfferPrice(lvRetNode.getChildNode(MAXOFFERPRICE).getValue());
                mvHKSIPOGenInfoDetails.setConfirmedPrice(lvRetNode.getChildNode(CONFIRMEDPRICE).getValue());
                mvHKSIPOGenInfoDetails.setEndDate(lvRetNode.getChildNode(ENDDATE).getValue());
                mvHKSIPOGenInfoDetails.setEndTime(lvRetNode.getChildNode(ENDTIME).getValue());
                // BEGIN RN00028 Ricky Ngan 20080816
                mvHKSIPOGenInfoDetails.setEIPOEndDate(lvRetNode.getChildNode(INTCHANNELCUTOFFDATE).getValue());
                mvHKSIPOGenInfoDetails.setAllotmentDate(lvRetNode.getChildNode(ALLOTMENTDATE).getValue());
                mvHKSIPOGenInfoDetails.setInterestValueDate(lvRetNode.getChildNode(INTERESTVALUEDATE).getValue());
                mvHKSIPOGenInfoDetails.setEIPOEndTime(lvRetNode.getChildNode("INTENDTIME").getValue());
                // END RN00028
                mvHKSIPOGenInfoDetails.setStatus(lvRetNode.getChildNode(STATUS).getValue());
                mvHKSIPOGenInfoDetails.setProspectus(lvRetNode.getChildNode(PROSPECTUS).getValue());
                mvHKSIPOGenInfoDetails.setChineseProspectus(lvRetNode.getChildNode(CHINESEPROSPECTUS).getValue());
                mvHKSIPOGenInfoDetails.setWebsite(lvRetNode.getChildNode(WEBSITE).getValue());
                // BEGIN RN00030 Ricky Ngan 20080923
                mvHKSIPOGenInfoDetails.setChineseWebsite(lvRetNode.getChildNode(CHINESEWEBSITE).getValue());
                // END RN00030
                mvHKSIPOGenInfoDetails.setListingDate(lvRetNode.getChildNode(LISTINGDATE).getValue());
                mvHKSIPOGenInfoDetails.setRemark(lvRetNode.getChildNode("REMARK").getValue());

                if (lvRetNode.getChildNode(LOTSPREADDETAIL).getChildNode(LOOP) != null)
                {
                    IMsgXMLNodeList lvLotDetails = lvRetNode.getChildNode(LOTSPREADDETAIL).getChildNode(LOOP).getNodeList(LOOP_ELEMENT);

                    int lvSize = lvLotDetails.size();

                    if (lvSize > 0)
                    {
                        mvIPOLotListVector = new Vector();
                        mvIPOAmountListVector = new Vector();

                        for (int i = 0; i<lvSize; i++)
                        {
                            mvIPOLotListVector.add(lvLotDetails.getNode(i).getChildNode(APPLIEDQTY).getValue());
                            mvIPOAmountListVector.add(lvLotDetails.getNode(i).getChildNode(OVERRIDEAMOUNT).getValue());
                        }
                    }
                }

                if (lvRetNode.getChildNode("FEEID") != null)
                {
                    String lvValue = lvRetNode.getChildNode("FEEID").getValue();
                    if (lvValue != null && !lvValue.equals(""))
                        mvHandlingFeeAmount = new BigDecimal(lvValue);
                    else
                        mvHandlingFeeAmount = new BigDecimal(0);
                }
            }
        }
        catch (Exception e)
        {
            Log.println( e , Log.ERROR_LOG);
        }
    }
    /**
     * Get method for entitlement id
     * @return entitlement id
     */
    public String getEntitlementId()
    {
        return mvEntitlementId;
    }
    /**
     * Set method for entitlement id
     * @param pEntitlementId the entitlement id
     */ 
    public void setEntitlementId(String pEntitlementId)
    {
        mvEntitlementId = pEntitlementId;
    }
    /**
     * Get method for handling fee amount
     * @return handling fee amount
     */
    public BigDecimal getHandlingFeeAmount()
    {
        return mvHandlingFeeAmount;
    }
    /**
     * Set method for handling fee amount
     * @param pHandlingFeeAmount the handling fee amount
     */
    public void setHandlingFeeAmount(BigDecimal pHandlingFeeAmount)
    {
        mvHandlingFeeAmount = pHandlingFeeAmount;
    }
    /**
     * Get method for lot spread detail
     * @return lot spread detail
     */
    public String getLotSpreadDetail()
    {
        return mvLotSpreadDetail;
    }
    /**
     * Set method for lot spread detail
     * @param pLotSpreadDetail the lot spread detail
     */
    public void setLotSpreadDetail(String pLotSpreadDetail)
    {
        mvLotSpreadDetail = pLotSpreadDetail;
    }
    /**
     * Get method for calculate fee flag
     * @return calculate fee flag
     */
    public String getCalculateFeeFlag()
    {
        return mvCalculateFeeFlag;
    }
    /**
     * Set method for calculate fee flag
     * @param pCalculateFeeFlag the calculate fee flag
     */
    public void setCalculateFeeFlag(String pCalculateFeeFlag)
    {
        mvCalculateFeeFlag = pCalculateFeeFlag;
    }
    /**
     * Get method for the account sequence
     * @return account sequence
     */
    public String getAccountSeq()
    {
            return mvAccountSeq;
    }
    /**
     * Set method for the account sequence
     * @param pAccountSeq the account sequence
     */
    public void setAccountSeq(String pAccountSeq)
    {
            mvAccountSeq = pAccountSeq;
    }
    /**
     * Get method for trading account sequence
     * @return trading account sequence
     */
    public String getTradingAccSeq()
    {
            return mvTradingAccSeq;
    }
    /**
     * Set method for trading account sequence
     * @param pTradingAccSeq the trading account sequence
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
            mvTradingAccSeq = pTradingAccSeq;
    }


    /**
     * Get method for application quantity
     * @return application quantity
     */
    public String getApplicationQty()
    {
        return mvApplicationQty;
    }
    /**
     * Set method for application quantity
     * @param pApplicationQty the application quantity
     */
    public void setApplicationQty(String pApplicationQty)
    {
        mvApplicationQty = pApplicationQty;
    }
    /**
     * Get method for client id
     * @return client id
     */
    public String getClientId()
    {
        return mvClientId;
    }
    /**
     * Set method for client id
     * @param pClientId the client id
     */
    public void setClientId(String pClientId)
    {
        mvClientId = pClientId;
    }
    /**
     * Get method for IPO gen info details
     * @return IPO gen info details
     */
    public HKSIPOGenInfoDetails getIPOGenInfoDetails()
    {
        return mvHKSIPOGenInfoDetails;
    }
    /**
     * Set method for IPO gen info details
     * @param pHKSIPOGenInfoDetails the IPO gen info details
     */
    public void setIPOInfoDetails(HKSIPOGenInfoDetails pHKSIPOGenInfoDetails)
    {
        mvHKSIPOGenInfoDetails = pHKSIPOGenInfoDetails;
    }
    /**
     * Get method for a vector the IPO lot
     * @return a vector the IPO lot
     */
    public Vector getIPOLotListVector()
    {
        return mvIPOLotListVector;
    }
    /**
     * Set method for a vector the IPO lot
     * @param pIPOLotListVector the a vector the IPO lot
     */
    public void setIPOLotListVector(Vector pIPOLotListVector)
    {
        mvIPOLotListVector = pIPOLotListVector;
    }
    /**
     * Get method for a vector the IPO amount 
     * @return a vector the IPO amount 
     */
    public Vector getIPOAmountListVector()
    {
        return mvIPOAmountListVector;
    }
    /**
     * Set method for a vector the IPO amount 
     * @param pIPOAmountListVector the a vector the IPO amount 
     */
    public void setIPOAmountListVector(Vector pIPOAmountListVector)
    {
        mvIPOAmountListVector = pIPOAmountListVector;
    }
    /**
     * Get method for return system code
     * @return return system code
     */
    public int getReturnCode()
    {
        return mvReturnCode;
    }
    /**
     * Set method for return system code
     * @param pReturnCode the return system code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Get method for system error code
     * @return system error code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }
    /**
     * Set method for system error code
     * @param pErrorCode the system error code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Get method for system error message
     * @return system error message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * Get method for localized system error message
     * @param pErrorCode the system error code
     * @param pDefaultMesg the default message
     * @return localized system error message
     */
   public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
   {
      String lvRetMessage;

      try
      {
         if (pErrorCode.equals("0") || pErrorCode.equals("999"))
         {
            lvRetMessage = pDefaultMesg;
         }
         else
         {
            lvRetMessage = (String)presentation.getErrorCodeBundle().getString(pErrorCode);
         }
      }
      catch (Exception ex)
      {
         Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

         lvRetMessage = pDefaultMesg;
      }
      return lvRetMessage;
   }
}
