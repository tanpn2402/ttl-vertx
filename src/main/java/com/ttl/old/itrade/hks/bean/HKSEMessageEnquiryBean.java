package com.ttl.old.itrade.hks.bean;

/**
 * The HKSEMessageEnquiryBean class define variables that to save values 
 * for action 
 * @author Wind zhao
 *
 */
public class HKSEMessageEnquiryBean {
	
	//Start By YuLong On 20090710
	private String mvClientID;
	
	/**
     * This method returns the client id.
     * @return the client id.
     * @type String.
     */
	public String getMvClientID() {
		return mvClientID;
	}
	
	/**
     * This method sets the client id.
     * @param pClientID The client id.
     * @type String.
     */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}
	
	private String mvMessageID;
	
	/**
     * This method returns the message id.
     * @return the message id.
     * @type String.
     */
	public String getMvMessageID() {
		return mvMessageID;
	}
	
	/**
     * This method sets the message id.
     * @param pMessageID The message id.
     * @type String.
     */
	public void setMvMessageID(String pMessageID) {
		mvMessageID = pMessageID;
	}
	//End By YuLong On 20090710
	
	private String mvPlaceDate;
	private String mvEMessage;
	private String mvTime;
	private String mvDay;
	
	/**
     * This method returns the message place time.
     * @return the message place time is formatted.
     * @type String.
     */
	public String getMvTime() {
		return mvTime;
	}
	
	/**
     * This method sets the message place time.
     * @param pTime The message place time is formatted.
     * @type String.
     */
	public void setMvTime(String pTime) {
		mvTime = pTime;
	}
	
	/**
     * This method returns the message place day.
     * @return the message place day is formatted.
     * @type String.
     */
	public String getMvDay() {
		return mvDay;
	}
	
	/**
     * This method sets the message place day.
     * @param pDay The message place day is formatted.
     * @type String.
     */
	public void setMvDay(String pDay) {
		mvDay = pDay;
	}
	
	/**
     * This method returns the message place date.
     * @return the message place date is formatted.
     * @type String.
     */
	public String getMvPlaceDate() {
		return mvPlaceDate;
	}
	
	/**
     * This method sets the message place date.
     * @param pPlaceDate The message place date is formatted.
     * @type String.
     */
	public void setMvPlaceDate(String pPlaceDate) {
		mvPlaceDate = pPlaceDate;
	}
	
	/**
     * This method returns the message content.
     * @return the message content.
     * @type String.
     */
	public String getMvEMessage() {
		return mvEMessage;
	}
	
	/**
     * This method sets the message content.
     * @param pEMessage The message content.
     * @type String.
     */
	public void setMvEMessage(String pEMessage) {
		mvEMessage = pEMessage;
	}
}
