package com.ttl.old.itrade.hks.txn;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

/**
 * This class store the transaction history
 * 
 * @author not attributable
 * 
 */
public class HKSTransactionHistoryCashReportTxn {

	private String mvClientId;
	private String mvFromDate;
	private String mvToDate;
	private String mvStatus;

	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;
	
	private String mvBalBF;
	private String mvBalCF;

	// private HKSTxnHistoryOrderDetails[] mvHKSTxnHistoryOrderDetails;
	// private HKSTxnHistoryDWDetails[] mvHKSTxnHistoryDWDetails;
	@SuppressWarnings("unchecked")
	private List mvHKSCashTransactionReportDetails;

	public static final String CLIENTID = "CLIENTID";
	public static final String FROMDATE = "FROMDATE";
	public static final String TODATE = "TODATE";

	TPErrorHandling mvTpError;
	SimpleDateFormat mvDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Constructor for HKSTransactionHistoryTxn class
	 * 
	 * @param pClientId
	 *            the client id
	 * @param pFromDate
	 *            the form date
	 * @param pToDate
	 *            the to date
	 * @param pTxnType
	 *            the txntype
	 */
	public HKSTransactionHistoryCashReportTxn(String pClientId,
			String pFromDate, String pToDate) {
		setClientId(pClientId);
		setFromDate(pFromDate);
		setToDate(pToDate);
		mvTpError = new TPErrorHandling();
	}

	/*** added by May on 18-12-2003 ***/
	/**
	 * The method for time type conversion
	 * 
	 * @param longTime
	 *            the long time
	 */
	public static String shortTime(String longTime) {
		SimpleDateFormat origFormatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat newFormatter = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		Date longDate = new Date();
		try {
			longDate = origFormatter.parse(longTime);
		} catch (ParseException ex) {
			Log.println(
					"[ TransactionHistory.shortTime(): ParseException, longTime="
							+ longTime + " ]", Log.ERROR_LOG);
		}

		return newFormatter.format(longDate);
		/*
		 * int dot = longTime.lastIndexOf("."); if (dot > 0) return
		 * longTime.substring(0,dot); else return longTime;
		 */
	}

	/*******************************************/

	// comment by May on 17-12-2003
	// public void process()
	/**
	 * This method store the transaction history
	 * 
	 * @param presentation
	 *            the Presentation class
	 */
	@SuppressWarnings("unchecked")
	public void process(Presentation presentation) {
		try {
			Hashtable lvTxnMap = new Hashtable();

			IMsgXMLNode lvRetNode;
			TPManager ivTPManager = ITradeServlet
					.getTPManager("HKSCashTransactionHistoryReportRequest");
			TPBaseRequest lvTransactionHistoryRequest = ivTPManager
					.getRequest("HKSCashTransactionHistoryReportRequest");

			lvTxnMap.put(CLIENTID, getClientId());
			lvTxnMap.put(FROMDATE, getFromDate());
			lvTxnMap.put(TODATE, getToDate());

			lvRetNode = lvTransactionHistoryRequest.send(lvTxnMap);

			setReturnCode(mvTpError.checkError(lvRetNode));
			if (mvReturnCode != mvTpError.TP_NORMAL) {
				setErrorMessage(mvTpError.getErrDesc());
				if (mvReturnCode == mvTpError.TP_APP_ERR) {
					setErrorCode(mvTpError.getErrCode());
				}
			} else {

				IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("Row");
				HashMap returnModel = null;
				setMvHKSCashTransactionReportDetails(new ArrayList());
			
				for (int i = 0; i < lvRowList.size(); i++) {
					returnModel = new HashMap();

					IMsgXMLNode lvRow = lvRowList.getNode(i);
					//returnModel.put("RN", lvRow.getChildNode("RN").getValue());
					returnModel.put("CASHSETTLEDATE", lvRow.getChildNode("CASHSETTLEDATE").getValue());
					returnModel.put("DESCRIPTION", lvRow.getChildNode("DESCRIPTION").getValue());
					returnModel.put("DEBITAMT", lvRow.getChildNode("DEBITAMT").getValue());
					returnModel.put("CREDITAMT", lvRow.getChildNode("CREDITAMT").getValue());
					
					if(i==0){
					setMvBalBF(lvRow.getChildNode("BALBF").getValue());
					setMvBalCF(lvRow.getChildNode("BALCF").getValue());
					}
				
					getMvHKSCashTransactionReportDetails().add(returnModel);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get method for Client ID
	 * 
	 * @return Client ID
	 */
	public String getClientId() {
		return mvClientId;
	}

	/**
	 * Set method for Client ID
	 * 
	 * @param pClientId
	 *            the Client ID
	 */
	public void setClientId(String pClientId) {
		mvClientId = pClientId;
	}

	/**
	 * Get method for From Date
	 * 
	 * @return From Date
	 */
	public String getFromDate() {
		return mvFromDate;
	}

	/**
	 * Set method for From Date
	 * 
	 * @param pFromDate
	 *            the From Date
	 */
	public void setFromDate(String pFromDate) {
		mvFromDate = pFromDate;
	}

	/**
	 * Get method for To Date
	 * 
	 * @return To Date
	 */
	public String getToDate() {
		return mvToDate;
	}

	/**
	 * Set method for To Date
	 * 
	 * @param pToDate
	 *            the To Date
	 */
	public void setToDate(String pToDate) {
		mvToDate = pToDate;
	}

	/**
	 * Get method for Status
	 * 
	 * @return Status
	 */
	public String getStatus() {
		return mvStatus;
	}

	/**
	 * Set method for Status
	 * 
	 * @param pStatus
	 *            the Status
	 */
	public void setStatus(String pStatus) {
		mvStatus = pStatus;
	}

	/**
	 * Get method for Return Code
	 * 
	 * @return Return Code
	 */
	public int getReturnCode() {
		return mvReturnCode;
	}

	/**
	 * Set method for Return Code
	 * 
	 * @param pReturnCode
	 *            the Return Code
	 */
	public void setReturnCode(int pReturnCode) {
		mvReturnCode = pReturnCode;
	}

	/**
	 * Get method for Error Code
	 * 
	 * @return Error Code
	 */
	public String getErrorCode() {
		return mvErrorCode;
	}

	/**
	 * Set method for Error Code
	 * 
	 * @param pErrorCode
	 *            the Error Code
	 */
	public void setErrorCode(String pErrorCode) {
		mvErrorCode = pErrorCode;
	}

	/**
	 * Get method for Error Message
	 * 
	 * @return Error Message
	 */
	public String getErrorMessage() {
		return mvErrorMessage;
	}

	/**
	 * Set method for Error Message
	 * 
	 * @param pErrorMessage
	 *            the Error Message
	 */
	public void setErrorMessage(String pErrorMessage) {
		mvErrorMessage = pErrorMessage;
	}

	/**
	 * @param mvHKSCashTransactionReportDetails the mvHKSCashTransactionReportDetails to set
	 */
	public void setMvHKSCashTransactionReportDetails(
			List mvHKSCashTransactionReportDetails) {
		this.mvHKSCashTransactionReportDetails = mvHKSCashTransactionReportDetails;
	}

	/**
	 * @return the mvHKSCashTransactionReportDetails
	 */
	public List getMvHKSCashTransactionReportDetails() {
		return mvHKSCashTransactionReportDetails;
	}

	/**
	 * @param mvBalBF the mvBalBF to set
	 */
	public void setMvBalBF(String mvBalBF) {
		this.mvBalBF = mvBalBF;
	}

	/**
	 * @return the mvBalBF
	 */
	public String getMvBalBF() {
		return mvBalBF;
	}

	/**
	 * @param mvBalCF the mvBalCF to set
	 */
	public void setMvBalCF(String mvBalCF) {
		this.mvBalCF = mvBalCF;
	}

	/**
	 * @return the mvBalCF
	 */
	public String getMvBalCF() {
		return mvBalCF;
	}
}
