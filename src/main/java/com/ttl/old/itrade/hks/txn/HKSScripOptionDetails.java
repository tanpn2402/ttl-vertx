package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
/**
 * This class the HKSScripOptionStatusEnquiryTxn class entity
 * @author not attributable
 */
public class HKSScripOptionDetails
{
    private String mvEntitlementID;
    private String mvInstrumentID;
    private String mvInstrumentName;
    private String mvInstrumentShortName;
    private String mvInstrumentChineseName;
    private String mvInstrumentChineseShortName;
    private String mvStatus;
    private String mvStatusDescription;
    private String mvCurrencyID;
    private String mvProductID;
    private String mvMarketID;

    private String mvClientID;
    private String mvTradingAccSeq;
    private String mvAccountSeq;
    private String mvInputDate;



    private String mvBookCloseDate;
    private String mvBookCloseQty;
    private String mvDividendAmount;
    private String mvMaxDividendAmount;
    private String mvScripOptionQty;


    private String mvNumberOfSharesHeld;
    private String mvNumOfSharesHeldBalanceDate;
    private String mvDividendDistributed;

    private String mvDividendPerShare;
    private String mvConversionPrice;
    private String mvClosingDate;

    private String mvMaxResultQty;
    private String mvReinvestAmount;
    private String mvRemainingDividendAmount;
    private String mvCompletedDateTime;

    private String mvRegisteredQty;

    private String mvDividendDeliver;
    private String mvDividendRatioPer;

    private String mvSettleCurrencyID;





    /**
     * Get method for instrument chinese short name
     * @return instrument chinese short name
     */
    public String getInstrumentChineseShortName()
    {
            return mvInstrumentChineseShortName;
    }
    /**
     * Get method for currency id
     * @return currency id
     */
    public String getCurrencyID()
    {
            return mvCurrencyID;
    }
    /**
     * Get method for trading account sequence
     * @return trading account sequence
     */
    public String getTradingAccSeq()
    {
            return mvTradingAccSeq;
    }
    /**
     * Get method for market id
     * @return market id
     */
    public String getMarketID()
    {
            return mvMarketID;
    }
    /**
     * Get method for status
     * @return status
     */
    public String getStatus()
    {
            return mvStatus;
    }
    /**
     * Get method for status description
     * @return status description
     */
    public String getStatusDescription()
    {
            return mvStatusDescription;
    }
    /**
     * Get method for instrument name
     * @return instrument name
     */
    public String getInstrumentName()
    {
            return mvInstrumentName;
    }

//    public String getReverseStatus()
//    {
//        return mvReverseStatus;
//    }
    /**
     * Get method for entitlement id
     * @return entitlement id
     */
    public String getEntitlementID()
    {
            return mvEntitlementID;
    }
    /**
     * Get method for instrument short name
     * @return instrument short name
     */
    public String getInstrumentShortName()
    {
            return mvInstrumentShortName;
    }
    /**
     * Get method for account sequence
     * @return account sequence
     */
    public String getAccountSeq()
    {
            return mvAccountSeq;
    }
    /**
     * Get method for instrument chinese name
     * @return instrument chinese name
     */
    public String getInstrumentChineseName()
    {
            return mvInstrumentChineseName;
    }
    /**
     * Get method for input date
     * @return input date
     */
    public String getInputDate()
    {
            return mvInputDate;
    }
    /**
     * Get method for instrument id
     * @return instrument id
     */
    public String getInstrumentID()
    {
            return mvInstrumentID;
    }
    /**
     * Get method for client id
     * @return client id
     */
    public String getClientID()
    {
            return mvClientID;
    }
    /**
     * Get method for product id
     * @return product id
     */
    public String getProductID()
    {
            return mvProductID;
    }	
    /**
     * Get method for book close date
     * @return book close date
     */
    public String getBookCloseDate()
    {
            return mvBookCloseDate;
    }
    /**
     * Get method for book close quantity
     * @return book close quantity
     */
    public String getBookCloseQty()
    {
            return mvBookCloseQty;
    }
    /**
     * Get method for dividend amount
     * @return dividend amount
     */
    public String getDividendAmount()
    {
            return  mvDividendAmount;
    }
    /**
     * Get method for max dividend amount
     * @return max dividend amount
     */
    public String getMaxDividendAmount()
    {
            return mvMaxDividendAmount;
    }
    /**
     * Get method for script option quantity
     * @return script option quantity
     */
    public String getScripOptionQty()
    {
            return  mvScripOptionQty;
    }
    /**
     * Get method for number of shares held
     * @return number of shares held
     */
    public String getNumberOfSharesHeld()
    {
            return mvNumberOfSharesHeld;
    }
    /**
     * Get method for number of shares held balance date
     * @return number of shares held balance date
     */
    public String getNumOfSharesHeldBalanceDate()
    {
            return mvNumOfSharesHeldBalanceDate;
    }
    /**
     * Get method for dividend distributed
     * @return dividend distributed
     */
    public String getDividendDistributed()
    {
            return mvDividendDistributed;
    }
    /**
     * Get method for dividend per share
     * @return dividend per share
     */
    public String getDividendPerShare()
    {
            return mvDividendPerShare;
    }
    /**
     * Get method for conversion price
     * @return conversion price
     */
    public String getConversionPrice()
    {
            return mvConversionPrice;
    }
    /**
     * Get method for closing date
     * @return closing date
     */
    public String getClosingDate()
    {
            return mvClosingDate;
    }
    /**
     * Get method for max result quantity
     * @return max result quantity
     */
    public String getMaxResultQty()
    {
            return mvMaxResultQty;
    }
    /**
     * Get method for reinvest amount
     * @return reinvest amount
     */
    public String getReinvestAmount()
    {
            return mvReinvestAmount;
    }
    /**
     * Get method for remaining dividend amount
     * @return remaining dividend amount
     */
    public String getRemainingDividendAmount()
    {
            return mvRemainingDividendAmount;
    }
    /**
     * Get method for completed date time
     * @return completed date time
     */
    public String getCompletedDateTime()
    {
            return mvCompletedDateTime;
    }
    /**
     * Get method for registered quantity
     * @return registered quantity
     */
    public String getRegisteredQty()
    {
            return mvRegisteredQty;
    }
    /**
     * Get method for dividend deliver
     * @return dividend deliver
     */
    public String getDividendDeliver()
    {
            return mvDividendDeliver;
    }
    /**
     * Get method for dividend ratio per
     * @return dividend ratio per
     */
    public String getDividendRatioPer()
    {
            return mvDividendRatioPer;
    }
    /**
     * Get method for settle currency id
     * @return settle currency id
     */
    public String getSettleCurrencyID()
    {
            return mvSettleCurrencyID;
    }


    /**
     * Set method for instrument chinese short name
     * @param pInstrumentChineseShortName the instrument chinese short name
     */
    public void setInstrumentChineseShortName(String pInstrumentChineseShortName)
    {
            mvInstrumentChineseShortName = pInstrumentChineseShortName;
    }
    /**
     * Set method for currency id
     * @param pCurrencyID the currency id
     */
    public void setCurrencyID(String pCurrencyID)
    {
            mvCurrencyID = pCurrencyID;
    }
    /**
     * Set method for trading account sequence
     * @param pTradingAccSeq the trading account sequence
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
            mvTradingAccSeq = pTradingAccSeq;
    }
    /**
     * Set method for market id
     * @param pMarketID market id
     */
    public void setMarketID(String pMarketID)
    {
            mvMarketID = pMarketID;
    }
    /**
     * Set method for status
     * @param pStatus the status
     */
    public void setStatus(String pStatus)
    {
            mvStatus = pStatus;
    }
    /**
     * Set method for status description 
     * @param pStatusDescription the status description 
     */
    public void setStatusDescription(String pStatusDescription)
    {
            mvStatusDescription = pStatusDescription;
    }
    /**
     * Set method for instrument name
     * @param pInstrumentName the instrument name
     */
    public void setInstrumentName(String pInstrumentName)
    {
            mvInstrumentName = pInstrumentName;
    }

//    public void setReverseStatus(String pReverseStatus)
//    {
//        mvReverseStatus = pReverseStatus;
//    }
    /**
     * Set method for entitlement id
     * @param pEntitlementID the entitlement id
     */
    public void setEntitlementID(String pEntitlementID)
    {
            mvEntitlementID = pEntitlementID;
    }
    /**
     * Set method for instrument short name
     * @param pInstrumentShortName the instrument short name
     */
    public void setInstrumentShortName(String pInstrumentShortName)
    {
            mvInstrumentShortName = pInstrumentShortName;
    }
    /**
     * Set method for account sequence
     * @param pAccountSeq the account sequence
     */
    public void setAccountSeq(String pAccountSeq)
    {
            mvAccountSeq = pAccountSeq;
    }
    /**
     * Set method for instrument chinese name
     * @param pInstrumentChineseName the instrument chinese name
     */
    public void setInstrumentChineseName(String pInstrumentChineseName)
    {
            mvInstrumentChineseName = pInstrumentChineseName;
    }
    /**
     * Set method for input date
     * @param pInputDate the input date
     */
    public void setInputDate(String pInputDate)
    {
            mvInputDate = pInputDate;
    }
    /**
     * Set method for instrument id
     * @param pInstrumentID the instrument id
     */
    public void setInstrumentID(String pInstrumentID)
    {
            mvInstrumentID = pInstrumentID;
    }
    /**
     * Set method for client id
     * @param pClientID the client id
     */
    public void setClientID(String pClientID)
    {
            mvClientID = pClientID;
    }
    /**
     * Set method for product id
     * @param pProductID the product id
     */
    public void setProductID(String pProductID)
    {
            mvProductID = pProductID;
    }
    /**
     * Set method for book close date
     * @param pBookCloseDate the book close date
     */
    public void setBookCloseDate(String pBookCloseDate)
    {
            mvBookCloseDate = pBookCloseDate;
    }
    /**
     * Set method for book close quantity
     * @param pBookCloseQty the book close quantity
     */
    public void setBookCloseQty(String pBookCloseQty)
    {
            mvBookCloseQty = pBookCloseQty;
    }
    /**
     * Set method for dividend amount
     * @param pDividendAmount the dividend amount
     */
    public void setDividendAmount(String pDividendAmount)
    {
            mvDividendAmount = pDividendAmount;
    }
    /**
     * Set method for dividend amount
     * @param pMaxDividendAmount the dividend amount
     */
    public void setMaxDividendAmount(String pMaxDividendAmount)
    {
            mvMaxDividendAmount = pMaxDividendAmount;
    }
    /**
     * Set method for script option quantity
     * @param pScripOptionQty the script option quantity
     */
    public void setScripOptionQty(String pScripOptionQty)
    {
            mvScripOptionQty = pScripOptionQty;
    }
    /**
     * Set method for number of shares held
     * @param pNumberOfSharesHeld the number of shares held
     */
    public void setNumberOfSharesHeld(String pNumberOfSharesHeld)
    {
            mvNumberOfSharesHeld = pNumberOfSharesHeld;
    }
    /**
     * Set method for number of shares held balance date
     * @param pNumOfSharesHeldBalanceDate the number of shares held balance date
     */
    public void setNumOfSharesHeldBalanceDate(String pNumOfSharesHeldBalanceDate)
    {
            mvNumOfSharesHeldBalanceDate = pNumOfSharesHeldBalanceDate;
    }
    /**
     * Set method for dividend distributed
     * @param pDividendDistributed the dividend distributed
     */
    public void setDividendDistributed(String pDividendDistributed)
    {
            mvDividendDistributed = pDividendDistributed;
    }
    /**
     * Set method for dividend per share
     * @param pDividendPerShare the dividend per share
     */
    public void setDividendPerShare(String pDividendPerShare)
    {
            mvDividendPerShare = pDividendPerShare;
    }
    /**
     * Set method for conversion price
     * @param pConversionPrice the conversion price
     */
    public void setConversionPrice(String pConversionPrice)
    {
            mvConversionPrice = pConversionPrice;
    }
    /**
     * Set method for closing date
     * @param pClosingDate the closing date
     */
    public void setClosingDate(String pClosingDate)
    {
            mvClosingDate = pClosingDate;
    }
    /**
     * Set method for max result quantity
     * @param pMaxResultQty the max result quantity
     */
    public void setMaxResultQty(String pMaxResultQty)
    {
            mvMaxResultQty = pMaxResultQty;
    }
    /**
     * Set method for reinvest amount
     * @param pReinvestAmount the reinvest amount
     */
    public void setReinvestAmount(String pReinvestAmount)
    {
            mvReinvestAmount = pReinvestAmount;
    }
    /**
     * Set method for remaining dividend amount
     * @param pRemainingDividendAmount the remaining dividend amount
     */
    public void setRemainingDividendAmount(String pRemainingDividendAmount)
    {
            mvRemainingDividendAmount = pRemainingDividendAmount;
    }
    /**
     * Set method for completed date time
     * @param pCompletedDateTime the completed date time
     */
    public void setCompletedDateTime(String pCompletedDateTime)
    {
            mvCompletedDateTime = pCompletedDateTime;

    }
    /**
     * Set method for registered quantity
     * @param pRegisteredQty the registered quantity
     */
    public void setRegisteredQty(String pRegisteredQty)
    {
            mvRegisteredQty = pRegisteredQty;
    }
    /**
     * Set method for dividend deliver
     * @param pDividendDeliver the dividend deliver
     */
    public void setDividendDeliver(String pDividendDeliver)
    {
            mvDividendDeliver = pDividendDeliver;
    }
    /**
     * Set method for dividend ratio per
     * @param pDividendRatioPer the dividend ratio per
     */
    public void setDividendRatioPer(String pDividendRatioPer)
    {
            mvDividendRatioPer = pDividendRatioPer;
    }
    /**
     * Set method for settle currency id
     * @param pSettleCurrencyID the settle currency id
     */
    public void setSettleCurrencyID(String pSettleCurrencyID)
    {
            mvSettleCurrencyID = pSettleCurrencyID;
    }

}
