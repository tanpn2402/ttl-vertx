/*
 * Begin Task #: TTL-GZ-JJ-00002 Yingzhi Zhang 20091127
 * 
 * @(#)HKSNewAddPortfolioBean.java        2009/11/27
 *  
 * Copyright ©2009 Transaction Technologies Limited.
 * Yingzhi Zhang GuangZhou, 020 ,China
 * All rights reserved. 
 * 
 *This software is the confidential and proprietary information of TTL,
 *Inc.("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with TTL.
 * 
 */
package com.ttl.old.itrade.hks.bean;

/**
 * The HKSNewAddPortfolioBean class define variables that to save values 
 * for action 
 * @author Yingzhi Zhang
 *
 */
public class HKSNewAddPortfolioBean {
	/* svCountId , count of the HKSNewAddPortfolioBean object which were created*/
	public static long svCountId = 0;
	
	/*bean id;*/
	private String mvId;
	
	/*confirm buy or sell value is 'y' or 'n';*/
	private String mvConfirmBuy;	
	
	/*confirm buy or sell ,opposite mvConfirmBuy ;*/
	private String mvConfirmSell;	
	//BEGIN TASK #:TTL-GZ-WSJ-00009 2010-01-18 SHAOJIAN WAN [iTrade R5] Add Market into Import/input
	private String mvMarket;
	/**
	 * Set method for market id
	 * @return  market id
	 */
	public String getMvMarket() {
		return mvMarket;
	}
	/**
	 * method method for market id
	 * @param mvMarketId the market id
	 */
	public void setMvMarket(String pMarket) {
		mvMarket = pMarket;
	}
	//END TASK #:TTL-GZ-WSJ-00009 2010-01-18 SHAOJIAN WAN [iTrade R5] Add Market into Import/input
	/*id of stock*/
	private String mvStock;
	
	/*quantitis of stock*/
	private String mvQuantity;
	
	/*price of stock*/
	private String mvPrice;
	
	/*date of buying stock*/
	private String mvDate;			
	
	/**
     * when create a new object then svCountId increment 1,
	 * pass svCountId to mvId.
     * @return
     */
	public HKSNewAddPortfolioBean(){
		svCountId += 1 ;
		mvId = "" + svCountId;	//make mvId auto increment
	}

	/**
     * This method returns the id
     * @return the id.
     */
	public String getMvId() {
		return mvId;
	}

	/**
     * This method sets the id
     * @param pId The id.
     */
	public void setMvId(String pId) {
		this.mvId = pId;
	}

	/**
     * This method returns the confirm buy
     * @return the confirm buy.
     */
	public String getMvConfirmBuy() {
		return mvConfirmBuy;
	}

	/**
     * This method sets the confirm buy
     * @param pConfirmBuy The confirm buy.
     */
	public void setMvConfirmBuy(String pConfirmBuy) {
		this.mvConfirmBuy = pConfirmBuy;
	}

	/**
     * This method returns the confirm sell
     * @return the confirm sell.
     */
	public String getMvConfirmSell() {
		return mvConfirmSell;
	}

	/**
     * This method sets the confirm sell
     * @param pConfirmSell The confirm sell.
     */
	public void setMvConfirmSell(String pConfirmSell) {
		this.mvConfirmSell = pConfirmSell;
	}

	/**
     * This method returns the stock
     * @return the stock.
     */
	public String getMvStock() {
		return mvStock;
	}

	/**
     * This method sets the stock
     * @param pStock The stock.
     */
	public void setMvStock(String pStock) {
		this.mvStock = pStock;
	}

	/**
     * This method returns the quantity
     * @return the quantity.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}

	/**
     * This method sets the quantity
     * @param pQuantity The quantity.
     */
	public void setMvQuantity(String pQuantity) {
		this.mvQuantity = pQuantity;
	}

	/**
     * This method returns the price
     * @return the price.
     */
	public String getMvPrice() {
		return mvPrice;
	}

	/**
     * This method sets the price
     * @param pPrice The price.
     */
	public void setMvPrice(String pPrice) {
		this.mvPrice = pPrice;
	}

	/**
     * This method returns the date
     * @return the date.
     */
	public String getMvDate() {
		return mvDate;
	}

	/**
     * This method sets the date
     * @param pDate The date.
     */
	public void setMvDate(String pDate) {
		this.mvDate = pDate;
	}
	
}
/*
 * End Task #: TTL-GZ-JJ-00002 Yingzhi Zhang 20091127
*/