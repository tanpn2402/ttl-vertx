package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSInformationEnquiryTxn class definition for all method
 * Information query from the host
 * @author not attributable
 *
 */
public class HKSInformationEnquiryTxn
{
   private String mvInformationID;
   private String mvMessage;

   private int mvReturnCode;
   private String mvErrorCode;
   private String mvErrorMessage;
   /**
    * This variable is used to the tp Error
    */
   TPErrorHandling tpError;
   Presentation presentation = null;

   /**
    * Constructor for HKSInformationEnquiryTxn class
    * @param pPresentation the Presentation class
    */
   public HKSInformationEnquiryTxn(Presentation pPresentation)
   {
        tpError = new TPErrorHandling();
        presentation = pPresentation;
   }
   /**
    * This method process Information query from the host
    * @param pPresentation the Presentation class
    */
   public void process(Presentation pPresentation)
   {
      Hashtable lvTxnMap = new Hashtable();

      IMsgXMLNode		lvRetNode;

      TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSInformationEnquiryRequest);
      TPBaseRequest lvHKSInformationEnquiryRequest = ivTPManager.getRequest(RequestName.HKSInformationEnquiryRequest);

      lvTxnMap.put("INFORMATIONID", getInformationID());

      lvRetNode = lvHKSInformationEnquiryRequest.send(lvTxnMap);

      setReturnCode(tpError.checkError(lvRetNode));
      if (mvReturnCode != TPErrorHandling.TP_NORMAL)
      {
              setErrorMessage(tpError.getErrDesc());
              if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
              {
                      setErrorCode(tpError.getErrCode());
              }
      }
      else
      {

              IMsgXMLNodeList lvRowList =  lvRetNode.getNodeList(ITagXsfTagName.ROW);
              for (int i = 0; i < lvRowList.size(); i++)
              {
                      IMsgXMLNode lvRow = lvRowList.getNode(i);
                      setMessage( lvRow.getChildNode(TagName.MESSAGE).getValue());
              }
      }

   }
   /**
    * Get method for information id
    * @return information id
    */
   public String getInformationID()
   {
           return mvInformationID;
   }
   /**
    * Set method for information id
    * @param pInformationID the information id
    */
   public void setInformationID(String pInformationID)
   {
           mvInformationID = pInformationID;
   }
   /**
    * Get method for system message
    * @return system message
    */
   public String getMessage()
   {
           return mvMessage;
   }
   /**
    * Set method for system message
    * @param pMessage the system message
    */
   public void setMessage(String pMessage)
   {
           mvMessage = pMessage;
   }

  /**
   * Get method for Return Code
   * @return Return Code
   */
    public int getReturnCode()
    {
        return mvReturnCode;
    }

    /**
     * Set method for Return Code
     * @param pReturnCode the Return Code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }

    /**
     * Get method for Error Code
     * @return Error Code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }

    /**
     * Set method for Error Code
     * @param pErrorCode the Error Code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }

    /**
     * Get method for Error Message
     * @return Error Message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }

    /**
     * Set method for Error Message
     * @param pErrorMessage the Error Message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * This mothod for localized system error message
     * @param pErrorCode the system error code
     * @param pDefaultMesg the default
     * @return localized system error message
     */
   public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
   {
      String lvRetMessage;

      try
      {
         if (pErrorCode.equals("0") || pErrorCode.equals("999"))
         {
            lvRetMessage = pDefaultMesg;
         }
         else
         {
            lvRetMessage = (String)presentation.getErrorCodeBundle().getString(pErrorCode);
         }
      }
      catch (Exception ex)
      {
         Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

         lvRetMessage = pDefaultMesg;
      }

      return lvRetMessage;
   }


}
