//BEGIN TASK #: - Giang Tran 20100827 [iTrade R5] HKSOddLotTxn
package com.ttl.old.itrade.hks.txn.plugin;

import java.util.HashMap;
import java.util.Hashtable;

import com.ttl.old.itrade.hks.bean.plugin.HKSOddLotBean;
import com.ttl.old.itrade.hks.bean.plugin.HKSOddLotHistoryBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSOddLotTxn extends BaseTxn {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mvClientID;
	private HKSOddLotBean[] oddLotList;
	private String mvInterfaceSeq;

	private HKSOddLotHistoryBean[] historyList;

	private Boolean mvSuccess = false;

	private int mvStartRecord;
	private int mvEndRecord;
	private String mvTotalRecord;

	/**
	 * constructor for HKSEntitlementTxn
	 * 
	 * @param pClientID
	 */
	public HKSOddLotTxn(String pClientID) {
		super();
		this.setMvClientID(pClientID);
	}

	/**
	 * Get oddlot data
	 */
	public void getOddLotData() {
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);

		if (TPErrorHandling.TP_NORMAL == process("HKSGetOddLotData", lvTxnMap)) {
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");

			// initialize result list
			oddLotList = new HKSOddLotBean[lvRowList.size()];

			for (int i = 0; i < lvRowList.size(); i++) {
				IMsgXMLNode lvRow = lvRowList.getNode(i);
				oddLotList[i] = new HKSOddLotBean();

				oddLotList[i].setMarketId(lvRow.getChildNode(TagName.MARKETID).getValue().trim());
				oddLotList[i].setStockCode(lvRow.getChildNode(TagName.INSTRUMENTID).getValue().trim());
				oddLotList[i].setStockName(lvRow.getChildNode(TagName.INSTRUMENTNAME).getValue().trim());
				oddLotList[i].setLocation(lvRow.getChildNode(TagName.LOCATIONID).getValue().trim());
				oddLotList[i].setSettledBal(lvRow.getChildNode(TagName.SETTLEDBALANCE).getValue().trim());
				oddLotList[i].setDrawable(lvRow.getChildNode("DRAWABLE").getValue().trim());
				oddLotList[i].setOddLotQty(lvRow.getChildNode("ODDLOTDRAWABLE").getValue().trim());
				oddLotList[i].setHoldQty(lvRow.getChildNode(TagName.HOLD).getValue().trim());
				oddLotList[i].setNominalPrice(lvRow.getChildNode("NOMINALPRICE").getValue().trim());
				oddLotList[i].setCollectionPrice(lvRow.getChildNode("COLLECTIONPRICE").getValue().trim());
			}

		}
	}

	public HashMap getOddLotAnnouncement() {
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();

		/*
		 * // get trade date array HashMap dateMap = HKSMain.svCurrentTradeDateMap; // get trading date by default HO market Date date =
		 * (Date) dateMap.get("HO"); SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		 */
		// set status open
		lvTxnMap.put(TagName.STATUS, "O");
		// lvTxnMap.put("RECORDDATE", dateFormatter.format(date).trim());

		HashMap result = null;

		if (TPErrorHandling.TP_NORMAL == process("HKSGetOddLotAnnouncementData", lvTxnMap)) {
			result = new HashMap();
			result.put("ANNOUNCEMENTID", mvReturnNode.getChildNode("ANNOUNCEMENTID").getValue());
			result.put(TagName.STATUS, mvReturnNode.getChildNode(TagName.STATUS).getValue());
			result.put("RECORDDATE", mvReturnNode.getChildNode("RECORDDATE").getValue());
		}

		return result;
	}

	/**
	 * Function submit odd lot list
	 * 
	 * @param oddListStr
	 */
	public void submitOddLot(String oddListStr, String announceId) {
		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put("ANNOUNCEMENTID", announceId);
		lvTxnMap.put(TagName.INTERFACESEQ, mvInterfaceSeq);
		lvTxnMap.put("DETAIL", oddListStr);

		if (TPErrorHandling.TP_NORMAL == process("HKSDoSubmitOddLot", lvTxnMap)) {
			String result = mvReturnNode.getChildNode(TagName.RESULT).getValue();
			String returnCode = mvReturnNode.getChildNode(TagName.RETURNCODE).getValue();
			String msg = mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue();
			if ("FAILED".equals(result) && result != null) {
				setMvSuccess(false);
				setErrorCode(new ErrorCode(new String[0], returnCode, msg));
			} else {
				setMvSuccess(true);
			}
		}
	}

	/**
	 * Function get odd-lot history data
	 */
	public void getOddlotHistoryData() {

		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
		lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));

		if (TPErrorHandling.TP_NORMAL == process("HKSGetOddLotHistory", lvTxnMap)) {

			if (mvReturnNode.getChildNode("TOTALRECORD") != null) {
				setMvTotalRecord(mvReturnNode.getChildNode("TOTALRECORD").getValue());
			}

			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
			setHistoryList(new HKSOddLotHistoryBean[lvRowList.size()]);
			for (int i = 0; i < lvRowList.size(); i++) {
				getHistoryList()[i] = new HKSOddLotHistoryBean();
				IMsgXMLNode lvRow = lvRowList.getNode(i);

				// market id
				historyList[i].setMarketId(lvRow.getChildNode(TagName.MARKETID).getValue());

				// instrument id
				historyList[i].setInstrumentId(lvRow.getChildNode(TagName.INSTRUMENTID).getValue());

				// location id
				historyList[i].setLocationId(lvRow.getChildNode(TagName.LOCATIONID).getValue());

				// price
				historyList[i].setPrice(lvRow.getChildNode(TagName.PRICE).getValue());

				// applied qty
				historyList[i].setAppliedQty(lvRow.getChildNode(TagName.APPLIEDQTY).getValue());

				// fee
				historyList[i].setFee(lvRow.getChildNode(TagName.FEE).getValue());

				// settle amt SETTLEAMOUNT
				historyList[i].setSettleAmt(lvRow.getChildNode(TagName.SETTLEAMOUNT).getValue());

				// confirm date
				historyList[i].setConfirmDate(lvRow.getChildNode("CONFIRMDATE").getValue());

				// value date
				historyList[i].setValueDate(lvRow.getChildNode(TagName.VALUEDATE).getValue());

				// status
				historyList[i].setStatus(lvRow.getChildNode(TagName.STATUS).getValue());

				// creation time
				historyList[i].setCreateTime(lvRow.getChildNode(TagName.CREATIONTIME).getValue());

				// modified time
				historyList[i].setLastModifiedTime(lvRow.getChildNode(TagName.LASTMODIFIEDTIME).getValue());

				// approval time
				historyList[i].setLastApprovalTime(lvRow.getChildNode(TagName.LASTAPPROVALTIME).getValue());
			}
		}

	}

	/**
	 * @param mvClientID the mvClientID to set
	 */
	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}

	/**
	 * @return the mvClientID
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * @param oddLotList the oddLotList to set
	 */
	public void setOddLotList(HKSOddLotBean[] oddLotList) {
		this.oddLotList = oddLotList;
	}

	/**
	 * @return the oddLotList
	 */
	public HKSOddLotBean[] getOddLotList() {
		return oddLotList;
	}

	/**
	 * @param historyList the historyList to set
	 */
	public void setHistoryList(HKSOddLotHistoryBean[] historyList) {
		this.historyList = historyList;
	}

	/**
	 * @return the historyList
	 */
	public HKSOddLotHistoryBean[] getHistoryList() {
		return historyList;
	}

	/**
	 * @param mvSuccess the mvSuccess to set
	 */
	public void setMvSuccess(Boolean mvSuccess) {
		this.mvSuccess = mvSuccess;
	}

	/**
	 * @return the mvSuccess
	 */
	public Boolean getMvSuccess() {
		return mvSuccess;
	}

	public void setMvStartRecord(int mvStartRecord) {
		this.mvStartRecord = mvStartRecord;
	}

	public int getMvStartRecord() {
		return mvStartRecord;
	}

	public void setMvEndRecord(int mvEndRecord) {
		this.mvEndRecord = mvEndRecord;
	}

	public int getMvEndRecord() {
		return mvEndRecord;
	}

	public void setMvTotalRecord(String mvTotalRecord) {
		this.mvTotalRecord = mvTotalRecord;
	}

	public String getMvTotalRecord() {
		return mvTotalRecord;
	}

	/**
	 * @return the mvInterfaceSeq
	 */
	public String getMvInterfaceSeq() {
		return mvInterfaceSeq;
	}

	/**
	 * @param mvInterfaceSeq the mvInterfaceSeq to set
	 */
	public void setMvInterfaceSeq(String mvInterfaceSeq) {
		this.mvInterfaceSeq = mvInterfaceSeq;
	}

}
// END TASK #: - Giang Tran 20100827 [iTrade R5] HKSOddLotTxn
