package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * This class process the user ID setup
 * @author Tree Lam
 * @since 20080625
 */
public class HKSUserIDSetupTxn
{

        private String mvClientId;
        private String mvCINO;
        private String mvUserID;
        private String mvPassword;
        private String mvUserIDRegisterResult;
        private int returnCode;
        private ErrorCode errorCode;
        private String errorMessage;

        public static final String CLIENTID = "CLIENTID";

//	public static final String  OLDPASSWORD = "OLDPASSWORD";
        public static final String PASSWORD = "PASSWORD";
        public static final String CINO = "CINO";
        public static final String USERID = "USERID";
        public static final String USERIDREGISTERRESULT = "USERIDREGISTERRESULT";


	TPErrorHandling				tpError;

	/**
	 * Default constructor for HKSUserIDSetupTxn class
	 */
	public HKSUserIDSetupTxn()
	{
		tpError = new TPErrorHandling();
	}
	;


	/**
	 * Constructor for HKSUserIDSetupTxn class
	 * @param pClientID the client id
	 * @param pCINO the CINO
	 * @param pUserID the user id
	 * @param pPassword the password
	 */
	public HKSUserIDSetupTxn(String pClientID, String pCINO, String pUserID, String pPassword)
	{
		setClientId(pClientID);
		//setOldPassword(oldPassword);
		setPassword(pPassword);
                setUserID(pUserID);
                setCINO(pCINO);
		tpError = new TPErrorHandling();
	}

   /**
    * This method process the user ID setup
    *
    */
	public void process()
	{
                try {
                        Hashtable lvTxnMap = new Hashtable();
                        IMsgXMLNode lvRetNode;
                        IMsgXMLNode lvErrorCodeNode;
                        TPManager ivTPManager = ITradeServlet.getTpManager();
                        TPBaseRequest lvHKSUserIDSetupRequest = ivTPManager.getRequest(RequestName.HKSUserIDSetupRequest);

                        lvTxnMap.put(CLIENTID, getClientId());
                        lvTxnMap.put(PASSWORD, getPassword());
                        lvTxnMap.put(CINO, getCINO());
                        lvTxnMap.put(USERID, getUserID());

                        lvRetNode = lvHKSUserIDSetupRequest.send(lvTxnMap);
                        setReturnCode(tpError.checkError(lvRetNode));

                        if(returnCode != tpError.TP_NORMAL) {
                                lvErrorCodeNode = lvRetNode.getChildNode("C_ERROR_CODE");
                                if(lvErrorCodeNode == null || lvErrorCodeNode.getValue().equalsIgnoreCase("")) {
                                        setErrorCode(new ErrorCode(new String[0], "LOGIN_DEFAULT_ERROR", ""));
                                }
                                else {
                                        setErrorCode(new ErrorCode(new String[0], lvErrorCodeNode.getValue(), ""));
                                }
                                setUserIDRegisterResult("N");
                        }
                        else
                        {
                                if(lvRetNode.getChildNode("USERIDREGISTERRESULT")!=null && !lvRetNode.getChildNode("USERIDREGISTERRESULT").equals("")){
                                        setUserIDRegisterResult(lvRetNode.getChildNode("USERIDREGISTERRESULT").getValue());
                                }

  //                              mvUserIDRegisterResult =
                        }

                        /*			setReturnCode(tpError.checkError(lvRetNode));
                           if (returnCode != tpError.TP_NORMAL)
                           {
                            setErrorMessage(tpError.getErrDesc());
                            if (returnCode == tpError.TP_APP_ERR)
                            {
                             setErrorCode(tpError.getErrCode());
                            }
                           }
                           else
                           {
                            ;
                           }
                         */
                } catch(Exception e) {
                        e.printStackTrace();
                }
        }


	/**
	 * Get method for Client ID
	 * @return Client ID
	 */
	public String getClientId()
	{
		return mvClientId;
	}

	/**
	 * Set method for Client ID
	 * @param pClientId the Client ID
	 */
	public void setClientId(String pClientId)
	{
		this.mvClientId = pClientId;
	}


	/**
	 * Get method for Client Old Password
	 * @return Client Old Password
	 */
	public String getPassword()
	{
		return mvPassword;
	}

	/**
	 * Set method for Client Old Password
	 * @param pPassword the Client Old Password
	 */
	public void setPassword(String pPassword)
	{
		this.mvPassword = pPassword;
	}

        /**
         * Get method for client NO
         * @return client NO
         */
        public String getCINO()
        {
                return mvCINO;
        }

        /**
         * Set method for client NO
         * @param pCINO the client NO
         */
        public void setCINO(String pCINO)
        {
                this.mvCINO = pCINO;
        }

        /**
         * Get method for user id
         * @return String User ID
         */
        public String getUserID()
        {
               return mvUserID;
        }

        /**
         * Set method for user id
         * @param pUserID the User ID
         */
        public void setUserID(String pUserID)
        {
                this.mvUserID = pUserID;
        }

        /**
         * Get method for user id register result
         * @return user id register result
         */
        public String getUserIDRegisterResult()
        {
               return mvUserIDRegisterResult;
        }

        /**
         * Set method for user id register result
         * @param pUserIDRegisterResult the user id register result
         */
        public void setUserIDRegisterResult(String pUserIDRegisterResult)
        {
                mvUserIDRegisterResult = pUserIDRegisterResult;
        }


	/**
	 * Get method for Return Code
	 * @return Return Code
	 */
	public int getReturnCode()
	{
		return returnCode;
	}

	/**
	 * Set method for Return Code
	 * @param pReturnCode the Return Code
	 */
	public void setReturnCode(int pReturnCode)
	{
		this.returnCode = pReturnCode;
	}


	/**
	 * Get method for Error Code
	 * @return Error Code
	 */
	public ErrorCode getErrorCode()
	{
		return errorCode;
	}

	/**
	 * Set method for Error Code
	 * @param pErrorCode the Error Code
	 */
	public void setErrorCode(ErrorCode pErrorCode)
	{
		this.errorCode = pErrorCode;
	}


	/**
	 * Get method for Error Message
	 * @return Error Message
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * Set method for Error Message
	 * @param pErrorMessage the Error Message
	 */
	public void setErrorMessage(String pErrorMessage)
	{
		this.errorMessage = pErrorMessage;
	}
}
