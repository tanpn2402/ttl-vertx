package com.ttl.old.itrade.hks.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class HKSInstrument
{
	private String mvProductID;
        //Begin Task: WL00619 Walter Lau 2007 July 27
        private String mvMarketID;
        //End Task: WL00619 Walter Lau 2007 July 27
	private String mvInstrumentID;
	private String mvShortName;
	private String mvInstrumentName;
	private String mvChineseName;
	private String mvChineseShortName;
	private String mvChineseShortNameAPI;
	private String mvCurrencyID;
	//BEGIN TASK: TTL-GZ-JJ-00008 JiangJi 20100108 [iTrade R5]Ceiling and Floor in Popup
	private String mvCeiling;
	private String mvFloor;
	//END TASK: TTL-GZ-JJ-00008 JiangJi 20100108 [iTrade R5]Ceiling and Floor in Popup

	private int mvLotSize;
	
	// BEGIN TASK: TTL-VN VanTran 20100729 [iTrade R5]Add status & boardID
	private String mvStatus;
	private String mvBoardID;
	// BEGIN TASK: TTL-VN VanTran 20100729 [iTrade R5]Add status & boardID
	// BEGIN TASK: TTL-VN VanTran 20102011 [iTrade R5]Add closing price
	private String mvClosingPrice;
	// END TASK: TTL-VN VanTran 20102011 [iTrade R5]Add closing price
	// BEGIN TASK: TTL-VN VanTran 20102011 [iTrade R5]Add open price
	private String mvOpenPrice;
	// END TASK: TTL-VN VanTran 20102011 [iTrade R5]Add open price
	
	private String mvSpreadTableCode;
	
    public HKSInstrument()
    {
    }

	public void setProductID(String pProductID)
	{
		mvProductID = pProductID;
	}

        //Begin Task: WL00619 Walter Lau 2007 July 27
        public void setMarketID(String pMarketID)
        {
                mvMarketID = pMarketID;
        }
        //End Task: WL00619 Walter Lau 2007 July 27

	public void setInstrumentID(String pInstrumentID)
	{
		mvInstrumentID = pInstrumentID;
	}

	public void setShortName(String pShortName)
	{
		mvShortName = pShortName;
	}
	//BEGIN TASK: CL00042 Charlie Liu 20081113
	public void setInstrumentName(String pInstrumentName)
	{
		mvInstrumentName = pInstrumentName;
	}
	
	public void setChineseNameAPI(String pChineseName)
	{
		mvChineseName = pChineseName;
	}
	//END TASK: CL00042 20081113
	public void setChineseShortName(String pChineseShortName)
	{
		mvChineseShortName = pChineseShortName;
	}

	public void setChineseShortNameAPI(String pChineseShortNameAPI)
	{
		mvChineseShortNameAPI = pChineseShortNameAPI;
	}
	
	public void setLotSize(int pLotSize)
	{
		mvLotSize = pLotSize;
	}

	public String getProductID()
	{
		return mvProductID;
	}

        //Begin Task: WL00619 Walter Lau 2007 July 27
        public String getMarketID()
        {
                return mvMarketID;
        }
        //End Task: WL00619 Walter Lau 2007 July 27

	public String getInstrumentID()
	{
		return mvInstrumentID;
	}

	public String getShortName()
	{
		return mvShortName;
	}

	public String getInstrumentName()
	{
		return mvInstrumentName;
	}
	
	public String getChineseNameAPI()
	{
		return mvChineseName;
	}
	
	public String getChineseShortName()
	{
		return mvChineseShortName;
	}
	
	public String getChineseShortNameAPI()
	{
		return mvChineseShortNameAPI;
	}

	public int getLotSize()
	{
		return mvLotSize;
	}

	public void setCurrencyID(String pCurrency) {
		mvCurrencyID = pCurrency;
	}

	public String getCurrencyID() {
		return mvCurrencyID;
	}
	//BEGIN TASK: TTL-GZ-JJ-00008 JiangJi 20100108 [iTrade R5]Ceiling and Floor in Popup
	/**
	 * Get the ceiling price.
	 * @return ceiling price
	 */
	public String getCeiling() {
		return mvCeiling;
	}
	/**
	 * Set the ceiling price.
	 * @param pCeiling the ceiling price.
	 */
	public void setCeiling(String pCeiling) {
		mvCeiling = pCeiling;
	}
	/**
	 * Get the floor price.
	 * @return floor price
	 */
	public String getFloor() {
		return mvFloor;
	}
	/**
	 * Set the floor price.
	 * @param pFloor the floor price.
	 */
	public void setFloor(String pFloor) {
		mvFloor = pFloor;
	}
	//END TASK: TTL-GZ-JJ-00008 JiangJi 20100108 [iTrade R5]Ceiling and Floor in Popup

	/**
	 * @param mvStatus the mvStatus to set
	 */
	public void setMvStatus(String mvStatus) {
		this.mvStatus = mvStatus;
	}

	/**
	 * @return the mvStatus
	 */
	public String getMvStatus() {
		return mvStatus;
	}

	/**
	 * @param mvBoardID the mvBoardID to set
	 */
	public void setMvBoardID(String mvBoardID) {
		this.mvBoardID = mvBoardID;
	}

	/**
	 * @return the mvBoardID
	 */
	public String getMvBoardID() {
		return mvBoardID;
	}

	public void setMvClosingPrice(String mvClosingPrice) {
		this.mvClosingPrice = mvClosingPrice;
	}

	public String getMvClosingPrice() {
		return mvClosingPrice;
	}

	public void setMvOpenPrice(String mvOpenPrice) {
		this.mvOpenPrice = mvOpenPrice;
	}

	public String getMvOpenPrice() {
		return mvOpenPrice;
	}

	public String getSpreadTableCode() {
		return mvSpreadTableCode;
	}

	public void setSpreadTableCode(String spreadTableCode) {
		this.mvSpreadTableCode = spreadTableCode;
	}
	
}
