package com.ttl.old.itrade.hks.txn.plugin;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.systekit.tag.ITagXsfTagName;

/**
 * The HKSIPOFinancingFixedChargeTxn class definition for all method
 * Host of new shares to obtain loans from the fixed charge
 * @author not attributable
 *
 */
public class HKSIPOFinancingFixedChargeTxn extends BaseTxn {
	/**
	 * This variable is used to the tp Error
	 */
	private TPErrorHandling tpError;
	
	private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;
    
    private String mvEntitlementID;
    private Vector mvResultApplyShare = new Vector();
    private Vector mvResultIPORate = new Vector();
	private Vector mvResultFlatFee = new Vector();
	private Vector mvResultDepositAmt = new Vector();
    /**
     * Default constructor for HKSIPOFinancingFixedChargeTxn class
     */
	public HKSIPOFinancingFixedChargeTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSIPOFinancingFixedChargeTxn class
	 * @param pEntitlementID the entitlement id
	 */
	public HKSIPOFinancingFixedChargeTxn(String pEntitlementID){
		tpError = new TPErrorHandling();
		setEntitlementID(pEntitlementID);
	} 
	/**
	 * This method process Host of new shares to obtain loans from the fixed charge
	 * @param pEntitlementID
	 * @return IPO loans to fixed-fee collection
	 */
	public Vector process(String pEntitlementID){
		Vector lvReturn = new Vector();
		try{
			Log.println("HKSMarginFixedChargeTxn starts", Log.ACCESS_LOG);
			
			Hashtable lvTxnMap = new Hashtable();
			
			lvTxnMap.put("ENTITLEMENTID", pEntitlementID);
			
			if (TPErrorHandling.TP_NORMAL != process(RequestName.HKSMarginFixedChargeRequest, lvTxnMap)){
				
				// TODO: simulate TP response XML
				mvReturnNode = bulidXMLNode();
				
				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
				
				for(int i=0; i< lvRowList.size(); i++){
					
					IMsgXMLNode lvNode = lvRowList.getNode(i);
					HashMap lvModel = new HashMap();
					lvModel.put("APPLYSHARE", lvNode.getChildNode("APPLYSHARE").getValue());
					lvModel.put("FLATFEE", lvNode.getChildNode("FLATFEE").getValue());
					lvModel.put("FINANCEFEE", lvNode.getChildNode("FINANCEFEE").getValue());
					lvModel.put("DEPOSITAMT",lvNode.getChildNode("DEPOSITAMT").getValue());
					lvModel.put("LOANAMT",lvNode.getChildNode("LOANAMT").getValue());
					lvModel.put("LOANRATE",lvNode.getChildNode("LOANRATE").getValue());
					lvModel.put("DEPOSITRATE",lvNode.getChildNode("DEPOSITRATE").getValue());
					lvReturn.add(lvModel);
				}
			}
		}catch(Exception e)
		{	
			Log.println("HKSMarginFixedChargeTxn error" + e.toString(), Log.ERROR_LOG);
		}
		return lvReturn;
	}
	/**
	 * Get method for entitlement id
	 * @return  entitlement id
	 */
	public String getEntitlementID(){
		return mvEntitlementID;
	}
	/**
	 * Set method for entitlement id
	 * @param pEntitlementID the entitlement id
	 */
	public void setEntitlementID(String pEntitlementID){
		this.mvEntitlementID=pEntitlementID;
	}
	/**
	 * Set method for return system code 
	 * @param pReturnCode the return system code 
	 */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Get method for system error message
     * @param pErrorCode the system error message
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    
	/**
	 * This function return a request xml node for submit advance payment.
	 * @return xml node.
	 * @author Wind.Zhao
	 */
	private IMsgXMLNode bulidXMLNode(){
		IMsgXMLNode lvReturnXMLNode = null;
		IMsgXMLParser lvIMsgXMLParser = MsgManager.createParser();
		lvReturnXMLNode = lvIMsgXMLParser.createXMLNode("HKSWB037Q01");
		lvReturnXMLNode.setAttribute("msgId", "HKSWB037Q01");
		lvReturnXMLNode.setAttribute("issueTime", "20100225174543");
		lvReturnXMLNode.setAttribute("issueLoc", "888");
		lvReturnXMLNode.setAttribute("issueMetd", "01");
		lvReturnXMLNode.setAttribute("oprId", IMain.getProperty("AgentID"));
		lvReturnXMLNode.setAttribute("pwd", IMain.getProperty("AgentPassword"));
		lvReturnXMLNode.setAttribute("resvr", "0000000015");
		lvReturnXMLNode.setAttribute("language", "en");
		lvReturnXMLNode.setAttribute("country", "us");
		lvReturnXMLNode.setAttribute("retryForTimeout", "T");
		
		
		IMsgXMLNode lvChild_Row = lvReturnXMLNode.addChildNode("Row");
		lvChild_Row.addChildNode("APPLYSHARE").setValue("3000.0000");
		lvChild_Row.addChildNode("FLATFEE").setValue("200.00");
		lvChild_Row.addChildNode("FINANCEFEE").setValue("0");
		lvChild_Row.addChildNode("DEPOSITAMT").setValue("4651.46");
		lvChild_Row.addChildNode("LOANAMT").setValue("4651.47");
		lvChild_Row.addChildNode("LOANRATE").setValue("50.000000");
		lvChild_Row.addChildNode("DEPOSITRATE").setValue("50.000000");
		
		IMsgXMLNode lvChild_Row1 = lvReturnXMLNode.addChildNode("Row");
		lvChild_Row1.addChildNode("APPLYSHARE").setValue("5000.0000");
		lvChild_Row1.addChildNode("FLATFEE").setValue("300.00");
		lvChild_Row1.addChildNode("FINANCEFEE").setValue("0");
		lvChild_Row1.addChildNode("DEPOSITAMT").setValue("3100.98");
		lvChild_Row1.addChildNode("LOANAMT").setValue("12403.90");
		lvChild_Row1.addChildNode("LOANRATE").setValue("80.000000");
		lvChild_Row1.addChildNode("DEPOSITRATE").setValue("20.000000");
		
		return lvReturnXMLNode;
	}
	
}
