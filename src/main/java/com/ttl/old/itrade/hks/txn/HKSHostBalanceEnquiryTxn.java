package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;

/**
 * The HKSHostBalanceEnquiryTxn class definition for all method
 * Host Department received inquiries from the account balance details
 * @author not attributable
 *
 */
public class HKSHostBalanceEnquiryTxn extends BaseTxn
{
	//Start Task: yulong.xu 20080801

	private String mvClientID;
	private String mvSettlementAccount;
	private String mvAvailableBalance;
	private String mvCurrentBalance;
	private String mvHostHoldAmount;
	private String mvSystemHoldAmount;
	private String mvServerAC;
	private String mvTransactionAC;
	private String mvOperatorID;

    //Begin Task #RC00181 - Rice Cheng 20090108
    private String mvLanguage;
    //End Task #RC00181 - Rice Cheng 20090108
    
	
	//Begin task YuLong Xu 04 Nov 2008
    private int mvReturnCode;
    TPErrorHandling	tpError = new TPErrorHandling();
    private String mvErrorCode;
    /**
     * Get method for return system code
     * @return return system code
     */
    public int getMvReturnCode() {
		return mvReturnCode;
	}
    /**
     * Set method for return system code
     * @param pReturnCode the return system code
     */
	public void setMvReturnCode(int pReturnCode) {
		this.mvReturnCode = pReturnCode;
	}
	/**
	 * Get method for system error code
	 * @return system error code
	 */
    public String getMvErrorCode() {
		 return mvErrorCode;
	}
    /**
     * Set method for system error code
     * @param pErrorCode the system error code
     */
	public void setMvErrorCode(String pErrorCode) {
		this.mvErrorCode = pErrorCode;
	}
   //End task YuLong Xu 04 Nov 2008
	
	/**
	 * Get method for operator id
	 * @return operator id
	 */
	public String getOperatorID() {
		return mvOperatorID;
	}
	/**
	 * Set method for operator id
	 * @param pOperatorID the operator id
	 */
	public void setOperatorID(String pOperatorID) {
		this.mvOperatorID = pOperatorID;
	}
	/**
	 * Get method for client id
	 * @return client id
	 */
	public String getClientID() {
		return mvClientID;
	}
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
	public void setClientID(String pClientID) {
		this.mvClientID = pClientID;
	}
	/**
	 * Get method for settlement bank account
	 * @return settlement bank account
	 */
	public String getSettlementAccount() {
		return mvSettlementAccount;
	}
	/**
	 * Set method for settlement bank account
	 * @param pSettlementAccount the settlement bank account
	 */
	public void setSettlementAccount(String pSettlementAccount) {
		this.mvSettlementAccount = pSettlementAccount;
	}
	/**
	 * Get method for available balance
	 * @return available balance
	 */
	public String getAvailableBalance() {
		return mvAvailableBalance;
	}
	/**
	 * Set method for available balance
	 * @param pAvailableBalance the available balance
	 */
	public void setAvailableBalance(String pAvailableBalance) {
		this.mvAvailableBalance = pAvailableBalance;
	}
	/**
	 * Get mehtod for current balance
	 * @return current balance
	 */
	public String getCurrentBalance() {
		return mvCurrentBalance;
	}
	/**
	 * Set mehtod for current balance
	 * @param pCurrentBalance the current balance
	 */
	public void setCurrentBalance(String pCurrentBalance) {
		this.mvCurrentBalance = pCurrentBalance;
	}
	/**
	 * Get method for host hold amount
	 * @return host hold amount
	 */
	public String getHostHoldAmount() {
		return mvHostHoldAmount;
	}
	/**
	 * Set method for host hold amount
	 * @param pHostHoldAmount the host hold amount
	 */
	public void setHostHoldAmount(String pHostHoldAmount) {
		this.mvHostHoldAmount = pHostHoldAmount;
	}
	/**
	 * Get method for system hold amount
	 * @return system hold amount
	 */
	public String getSystemHoldAmount() {
		return mvSystemHoldAmount;
	}
	/**
	 * Set method for system hold amount
	 * @param pSystemHoldAmount the system hold amount
	 */
	public void setSystemHoldAmount(String pSystemHoldAmount) {
		this.mvSystemHoldAmount = pSystemHoldAmount;
	}
	//BEGIN Task #:- al00001 albert lai 20080828
	/**
	 * Get method for server account
	 * @return server account
	 */
	public String getServerAC() {
		return mvServerAC;
	}
	/**
	 * Set method for server account
	 * @param pServerAC the server account
	 */
	public void setServerAC(String pServerAC) {
		this.mvServerAC = pServerAC;
	}
	/**
	 * Get method for transcation account
	 * @return transcation account
	 */
	public String getTransactionAC() {
		return mvTransactionAC;
	}
	/**
	 * Set method for transcation account
	 * @param pTransactionAC the transcation account
	 */
	public void setTransactionAC(String pTransactionAC) {
		this.mvTransactionAC = pTransactionAC;
	}
	//End Task #:- al00001 albert lai 20080828

    //Begin Task #RC00181 - Rice Cheng 20090108
	/**
	 * Get method for user locale
	 * @return user locale
	 */
    public String getLanguage()
    {
    	return mvLanguage;
    }
    /**
     * Set method for user locale
     * @param pLanguage the user locale
     */
    public void setLanguage(String pLanguage)
    {
    	mvLanguage = pLanguage;
    }
    //End Task #RC00181 - Rice Cheng 20090108
    
	/**
	 * Constructor of the host balance enquiry TXN
	 * @param pClientID the client id
	 * @param pLanguage the user locale
	 */
	//Begin Task #RC00181 - Rice Cheng 20090108
	public HKSHostBalanceEnquiryTxn(String pClientID, String pLanguage)
	{
		this.mvClientID = pClientID;
		setLanguage(pLanguage);
	}
	//End Task #RC00181 - Rice Cheng 20090108
	
	/**
     * The method process Host Department received inquiries from the account balance details
     *
     */
	public void process()
	{
		//BEGIN Task #:- al00001 albert lai 20080828
		Hashtable lvTxnMap = new Hashtable();
	      lvTxnMap.put("ServerAC", getClientID());
	      lvTxnMap.put("TransactionAC", getClientID());
	      //lvTxnMap.put("TransactionAC", getTransactionAC());

	      
	      /*//Begin task YuLong Xu 04 Nov 2008
	      IMsgXMLNode lvRetNode;
	      TPManager	ivTPManager = ITradeServlet.getTPManager(RequestName.HKSClientBankBalanceEnquiry);
		  TPBaseRequest lvHostAccountBalanceEnquiry = ivTPManager.getRequest(RequestName.HKSClientBankBalanceEnquiry);
		  lvRetNode = lvHostAccountBalanceEnquiry.send(lvTxnMap);
		  setMvReturnCode(tpError.checkError(lvRetNode));
		  if (mvReturnCode != tpError.TP_NORMAL){
			  setMvErrorCode(tpError.getErrCode());
		  }
		  //End task YuLong Xu 04 Nov 2008
*/	      

	      //Begin Task #RC00181 - Rice Cheng 20090108
	      //if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSClientBankBalanceEnquiry, lvTxnMap)){
	      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSClientBankBalanceEnquiry, lvTxnMap, getLanguage())){
	      //End Task #RC00181 - Rice Cheng 20090108
	    	  this.setSettlementAccount(mvReturnNode.getChildNode("Result").getChildNode("Account").getChildNode("SettlementAccount").getValue());
	    	  this.setAvailableBalance(mvReturnNode.getChildNode("Result").getChildNode("Account").getChildNode("AvailBalance").getValue());
	    	  this.setCurrentBalance(mvReturnNode.getChildNode("Result").getChildNode("Account").getChildNode("CurrentBalance").getValue());
	    	  this.setHostHoldAmount(mvReturnNode.getChildNode("Result").getChildNode("Account").getChildNode("HostHoldAmount").getValue());
	    	  this.setSystemHoldAmount(mvReturnNode.getChildNode("Result").getChildNode("Account").getChildNode("SystemHoldAmount").getValue());
	      }
	      else
          { // Unhandled Business Exception
             setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), ""));

             // Handle special cases
             // 1. Agreement is not signed
             if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("HKSFOE00026")) {
            	 setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode("C_ERROR_DESC").getValue()));
             }

             // 2. When TP is down
             if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
            	 setErrorCode(new ErrorCode(new String[0], "0100", "No connection with TP"));
             }
          }
	      //END Task #:- al00001 albert lai 20080828
	}

	/**
	 * Get a list of host balance enquiry detail
	 * The HKSHostBalanceEnquiryDetail class will contain the client ID, settlement account and the corresponding balance information
	 * @return A list HKSHostBalanceEnquiryDetail object
	 */
	public List getHostBalanceDetail()
	{
		return null;
	}
}
