package com.ttl.old.itrade.hks.txn.plugin;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;


import com.ttl.old.itrade.hks.bean.plugin.HKSMarginExtensionBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.hks.util.AgentUtils;
import com.ttl.old.itrade.hks.util.DateUtil;
import com.ttl.old.itrade.hks.util.TextFormatter;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TagName;

public class HKSMarginExtensionTxn extends BaseTxn {

	private String mvClientID;
	private String mvCurrentTradeDate;
	private String mvResult;
	private String mvReturnCode;
	private String mvReturnMsg;
	private String mvName;
	private String mvLoan;
	private String mvExpireDate;
	private String mvDebitInt;
	
	public String getMvName() {
		return mvName;
	}

	public void setMvName(String mvName) {
		this.mvName = mvName;
	}

	public String getMvLoan() {
		return mvLoan;
	}

	public void setMvLoan(String mvLoan) {
		this.mvLoan = mvLoan;
	}

	public String getMvExpireDate() {
		return mvExpireDate;
	}

	public void setMvExpireDate(String mvExpireDate) {
		this.mvExpireDate = mvExpireDate;
	}

	public String getMvDebitInt() {
		return mvDebitInt;
	}

	public void setMvDebitInt(String mvDebitInt) {
		this.mvDebitInt = mvDebitInt;
	}

	public String getMvResult() {
		return mvResult;
	}

	public void setMvResult(String mvResult) {
		this.mvResult = mvResult;
	}

	public String getMvReturnCode() {
		return mvReturnCode;
	}

	public void setMvReturnCode(String mvReturnCode) {
		this.mvReturnCode = mvReturnCode;
	}

	public String getMvReturnMsg() {
		return mvReturnMsg;
	}

	public void setMvReturnMsg(String mvReturnMsg) {
		this.mvReturnMsg = mvReturnMsg;
	}

	public HKSMarginExtensionTxn(String pClientID) {
		mvClientID = pClientID;
	}

	public String getMvClientID() {
		return mvClientID;
	}

	public void setMvClientID(String mvClientID) {
		this.mvClientID = mvClientID;
	}
	
	
	public String getMvCurrentTradeDate() {
		return mvCurrentTradeDate;
	}

	public void setMvCurrentTradeDate(String mvCurrentTradeDate) {
		this.mvCurrentTradeDate = mvCurrentTradeDate;
	}

	public HKSMarginExtensionBean getLocalMarginExtensionCreation() {
		HKSMarginExtensionBean bean = new HKSMarginExtensionBean();
		try{
			
			Log.println("HKSMarginExtensionTxn.getLocalMarginExtensionPayDebitInt() START: Client ID = " + mvClientID , Log.ACCESS_LOG);
			Hashtable lvTxnMap = new Hashtable();
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			
			// Send TP request
			
			if (TPErrorHandling.TP_NORMAL == process("HKSMarginContractInfo",lvTxnMap)){
				
				Date date = DateUtil.convertStringToDate("yyyy-MM-dd", mvReturnNode.getChildNode(TagName.MARGINEXPIRYDATE).getValue());
				bean.setExpiredate(mvReturnNode.getChildNode(TagName.MARGINEXPIRYDATE).getValue());
				bean.setActivedate(mvReturnNode.getChildNode("MARGINACTIVEDATE").getValue());
				int m = Integer.parseInt(IMain.getProperty("Month"));
				int dsum = Integer.parseInt(IMain.getProperty("DaySum"));
				int dsubtract = Integer.parseInt(IMain.getProperty("DaySubtract"));
				bean.setMonth(m);
				bean.setDaysum(dsum);
				bean.setDaysubtract(dsubtract);
				bean.setName(mvReturnNode.getChildNode(TagName.CLIENTNAME).getValue());
				bean.setSerial(mvReturnNode.getChildNode("SERIAL").getValue());
				
				bean.setLoan(mvReturnNode.getChildNode("LOANOUTSTANDING").getValue()); 
				bean.setCash(mvReturnNode.getChildNode("DRAWABLEBAL").getValue());
				String debitint = mvReturnNode.getChildNode("DEBITACCRUEDINTEREST").getValue().replace("-", "");
				bean.setDebitint(debitint);
				bean.setCurrentdate(mvCurrentTradeDate);
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(date); 
				cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+Integer.parseInt(IMain.getProperty("NewExpireDate"))));  
				date = cal.getTime(); 
				if (date != null)
				{
					String newexpiredate = TextFormatter.getFormattedTime(date, "dd/MM/yyyy");
					bean.setNewexpire(newexpiredate);
				}
				
				bean.setStatus(mvReturnNode.getChildNode("STATE").getValue());
			}
			
			
		} catch(Exception e) {
			Log.println("HKSMarginExtensionTxn.getLocalMarginExtensionPayDebitInt() Exception: " + e.toString() + " for clientid: " + mvClientID , Log.ERROR_LOG);
		} finally {
			Log.println("HKSMarginExtensionTxn.getLocalMarginExtensionPayDebitInt() END: Client ID = " + mvClientID, Log.ACCESS_LOG);
		}
		
		return bean;
	}
	
	public Map<String, String> submitInterestPosting() {
		Map<String, String> result = new HashMap<String, String>();
		Hashtable lvTxnMap = new Hashtable();
		try {
			Log.println("HKSMarginExtensionTxn.submitInterestPosting START: Client ID = " + mvClientID , Log.ACCESS_LOG);
			
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			//HIEU LE:
			lvTxnMap.put("OPRID", AgentUtils.getAgentID(this.getMvClientID()));
			lvTxnMap.put("EMAILLIST",AgentUtils.getOperatorEmails(this.getMvClientID(),AgentUtils.FUNCTION_TYPE_INTEREST_POSTING.toUpperCase()));
			lvTxnMap.put("NEWEXPIRYDATE", mvExpireDate);
			lvTxnMap.put("DEBITINTEREST", mvDebitInt);
			lvTxnMap.put("LOANOUTSTANDING", mvLoan);
			lvTxnMap.put("FULLNAME", mvName);
			//END HIEU LE
			
			// Send TP request
			if(TPErrorHandling.TP_NORMAL==process("HKSInterestPostingSelectedClient",lvTxnMap)){
				setMvResult(mvReturnNode.getChildNode(TagName.RESULT).getValue());
		    	setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
		    	setMvReturnMsg(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
			}
		} catch (Exception e) {
			Log.println("HKSMarginExtensionTxn.submitInterestPosting Error: " + e.getMessage(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSMarginExtensionTxn.submitInterestPosting END: Client ID = " + mvClientID, Log.ACCESS_LOG);
			lvTxnMap.clear();
		}
		
		
		return result;
	}

}
