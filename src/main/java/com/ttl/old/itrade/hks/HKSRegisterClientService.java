package com.ttl.old.itrade.hks;

import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ttl.old.itrade.tp.AppletGateway;
import com.ttl.old.itrade.util.Log;

/**
 * The HKSRegisterClientService class defines methods that register client service.
 * @author
 *
 */
public class HKSRegisterClientService extends HttpServlet {

	Object mvGlobalObject = new Object();

	/**
	 * This method invoke doPost method process request and response.
	 * @param pRequest Object that contains the request the client has made of the servlet.
	 * @param pResponse Object that contains the request the client has made of the servlet.
	 * @throws ServletException If an input or output error is detected when the servlet handles the GET request.
	 * @throws IOException If the request for the GET could not be handled
	 */
	protected void doGet(HttpServletRequest pRequest, HttpServletResponse pResponse) throws ServletException, IOException {
		doPost(pRequest, pResponse);
	}

	/**
	 * This method process client request and response.
	 * @param pRequest Object that contains the request the client has made of the servlet.
	 * @param pResponse Object that contains the request the client has made of the servlet.
	 * @throws ServletException If an input or output error is detected when the servlet handles the GET request.
	 * @throws IOException If the request for the GET could not be handled.
	 */
	protected void doPost(HttpServletRequest pRequest, HttpServletResponse pResponse) throws ServletException, IOException {

		HttpSession lvSession = pRequest.getSession(false);
      if (lvSession == null)
      {
    	  pResponse.getWriter().println("SESSIONTIMEOUT");
         return;
      }

		int lvAJAXWaitingTime = lvSession.getAttribute("AJAXWaitingTime") == null ? 120000 : Integer.parseInt(lvSession.getAttribute("AJAXWaitingTime").toString().trim());
		String lvClientID = (String) lvSession.getAttribute(HKSTag.CLIENTID);
      if(HKSMain.svSessionTracker != null)
      {
         HttpSession lvClientSession = (HttpSession)HKSMain.svSessionTracker.getSession(lvClientID);
         if(lvClientSession != null)
         {
            java.sql.Timestamp lvLastUpdatedTime = (java.sql.Timestamp)lvClientSession.getAttribute("LASTUPDATETIME");
            if(lvLastUpdatedTime != null && (System.currentTimeMillis() - lvLastUpdatedTime.getTime()) > 1800000)
            {
               try
               {
                  lvClientSession.invalidate();
               }
               catch(IllegalStateException e)
               {
                  //No need to log because it will be thrown only when Session is already invalidated
               }
               Log.println("HKSRegisterClientService : Remove Session! ", Log.DEBUG_LOG);

               HKSMain.svSessionTracker.removeSession(lvClientID);
               return;
            }
         }
      }
		Log.println("[HKSRegisterClientService.doPost(): Registering AJAX for clientID " + lvClientID, Log.ACCESS_LOG);

		if (lvClientID != null && !lvClientID.equals(""))
		{
			AppletGateway lvAppletGateway = null;
			Set lvAppletGatewaySet = (Set) AppletGateway.getAppletGatewaySetByClientID(lvClientID);

			if (lvAppletGatewaySet == null)
			{
				lvAppletGateway = new AppletGateway(lvClientID);
			}
			else
			{
				boolean lvGenerateAppletGateway = true;

				synchronized(mvGlobalObject) {
					Iterator lvIterator = lvAppletGatewaySet.iterator();
					while (lvIterator.hasNext())
					{
						lvGenerateAppletGateway = false;
						lvAppletGateway = (AppletGateway)lvIterator.next();

						if (("true").equalsIgnoreCase(pRequest.getParameter("FirstTime"))) {
							if (lvAppletGateway != null) {
								lvAppletGateway.sendToClient("TERM");

								AppletGateway.removeAppletGateway(lvAppletGateway);
							}
						}
					}
				}

				if (lvGenerateAppletGateway || ("true").equalsIgnoreCase(pRequest.getParameter("FirstTime")))
					lvAppletGateway = new AppletGateway(lvClientID);
			}
			lvAppletGateway.registerClient(lvClientID);
			//String lvReturnMessage = lvAppletGateway.waitForResponse(lvAJAXWaitingTime, lvSession);
			String lvReturnMessage = "";
			Log.println("The system is now sending message to client " + lvClientID, Log.ACCESS_LOG);
			Log.println("The message content is " + lvReturnMessage + ".", Log.ACCESS_LOG);

			if (("LOGOUT").equalsIgnoreCase(lvReturnMessage)) {
				AppletGateway.removeAppletGateway(lvAppletGateway);
			}
			pResponse.getWriter().println(lvReturnMessage);
		}
	}

	/**
	 * This method according client ID remove client.
	 * @param pClientID The ID of client.
	 */
	public void removeClient(String pClientID) {
		String lvClientID = pClientID;
		Set lvAppletGatewaySet = (Set) AppletGateway.getAppletGatewaySetByClientID(lvClientID);
		AppletGateway lvAppletGateway = null;
		try{
		if (lvAppletGatewaySet != null) {
			synchronized(mvGlobalObject) {
				Iterator lvIterator = lvAppletGatewaySet.iterator();
				Log.println("[HKSRegisterClientService.removeClient()] Removing applet gateway for client ID " + lvClientID + " - Begin.", Log.ACCESS_LOG);
				while (lvIterator.hasNext()) {
					lvAppletGateway = (AppletGateway) lvIterator.next();
					lvAppletGateway.sendToClient("LOGOUT");
					AppletGateway.removeAppletGateway(lvAppletGateway);
				}
			}
			Log.println("[HKSRegisterClientService.removeClient()] Removing applet gateway for client ID " + lvClientID + " - Completed.", Log.ACCESS_LOG);
		}
		} catch (ConcurrentModificationException e) {
			Log.println(e, Log.ACCESS_LOG);
		}
	}

}
