package com.ttl.old.itrade.hks.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */


public class HKSErrorCode
{
	public static final String  HKSFOPasswordIncorrectError = "HKSEF0037";
	// BEGIN RN00061 Ricky Ngan 20081203
	public static final String  HKSFOLoginFailedError = "HKSEF0034";
	// END RN00061
	
	public static final String  HKSFOPasswordExceedMaxLengthError = "HKSEF0054";
    public static final String  HKSFOInternetMaxLogonFailedError = "HKSEF0048";

    public static final String  CORECashAccountInterfaceNotAvailable = "CORE10172";
    public static final String HKSITradeBookCloseMasterHasBeenModified = "HKSBOE02014";
    public static final String HKSItradeScripOptionPeriodIsClosed = "HKSBOE02015";
    public static final String HKSItradeResetPasswordIncorrectInfo = "HKSEF0086";
    public static final String HKSItradeInformationEnquiryIncorrectInfo = "HKSEF0088";
    public static final String  HKSItradeInstrumentSuspended = "HKSRISK0013";
    public static final String HKSITradeClientIsClosed = "HKSRISK0014"; //Client is closed
    public static final String HKSITradeClientIsSuspended = "HKSRISK0015"; //Client is suspended
    public static final String HKSITradeNotAllowedPlaceBuyOrder = "HKSRISK0016"; //Client is not allowed to place buy order Your order has been rejected. Please call our Customer Service Team at (852) - 2913 3833 for assistance.
    public static final String HKSITradeNotAllowedPlaceSellOrder = "HKSRISK0017"; //Client is not allowed to place sell order
    public static final String HKSITradeInsufficientFund         = "HKSRISK0018";// Insufficient Fund
    public static final String HKSITradeNetAmountisShow          = "HKSRISK0001"; //Net amount is shown in HKD
    public static final String HKSITradeInstrumentisNonLocal     ="HKSRISK0031"; //Instrument 04331 is Non-Local
    public static final String HKSITradeInstrumentBeforeExpired  = "HKSRISK0032"; //2 day(s) before Expired
    public static final String HKSCancelOrderIsNotPermittedError = "HKSFOE00005"; //Cancel Order Is Not Permitted Error!
    public static final String HKSFOEOrderPriceNotInAcceptableRangeError = "HKSFOE00023";
    public static final String HKSFOEInternetChannelIsNotEnable  ="HKSFOE00040";

    // for webpage
    public static final int HKSItradePasswordIncorrectErrorCode = 604;
    public static final int HKSItradeStockCodeInvalidErrorCode = 30001;
    public static final int HKSItradeQuantityLotSizeNotMatchedErrorCode = 30002;
    public static final int HKSItradeQuantityInvalidErrorCode = 30003;
    public static final int HKSItradePriceSpreadErrorCode = 30004;
    public static final int HKSItradePriceNotInAcceptableRangeErrorCode = 30005;
    public static final int HKSItradeSplitOrderNotAcceptable = 30006;
    //Begin Task: WL00619 Walter Lau 2007 July 27
    public static final int HKSItradeMultiMarketNotAcceptable = 30007;
    //End Task: WL00619 Walter Lau 2007 July 27
    // BEGIN - TASK#: CL00034 - Charlie Liu 20081030
    public static final int HKSItradeInstrumentSuspendedErrorCode = 30008;
    public static final int HKSItradeNetAmountShownHKDErrorCode = 30009;
    public static final int HKSItradeInstrumentNonLocalErrorCode = 30010;
    public static final int HKSItradeInstrumentLastTradeDayErrorCode = 30011;
    // END - TASK#: CL00034
    
    public static final int HKSItradeQuantityLotSizeNotMatchOddLotErrorCode = 30012;
    public static final int HKSDisablePlaceOrderMarket = 30013;
    
    public static final String HKSValidateEmailNotExist = "HKSERROR0001";
    public static final String HKSValidateEmailInvalid = "HKSERROR0002";
    public static final String HKSValidatePhoneNotExist = "HKSERROR0003";
    public static final String HKSValidatePhoneInvalid = "HKSERROR0004";
    
    public static final String HKSIradeInvalidTime_ATO = "HKSERROR0005";
    public static final String HKSIradeMarketClosed = "HKSERROR0006";
    public static final String HKSIradeCancelOrderSession1 = "HKSERROR0007";


}
