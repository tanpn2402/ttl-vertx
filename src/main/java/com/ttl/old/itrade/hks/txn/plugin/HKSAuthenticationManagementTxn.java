package com.ttl.old.itrade.hks.txn.plugin;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.bean.plugin.HKSAuthenticateResultInfoBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.ttl.old.itrade.hks.bean.plugin.HKSEntrustChallengeInfoBean;;

public class HKSAuthenticationManagementTxn extends BaseTxn{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * constructor
	 */
	public HKSAuthenticationManagementTxn()
	{		
	}
	
	/**
	 * Method to call the service to get Entrust Challenge
	 * @return
	 */
	public HKSEntrustChallengeInfoBean getEntrustChallenge(String mvClientId)
	{		
		// log start process
		Log.println("[ HKSAuthenticationManagementTxn.process() getEntrustChallenge [" + mvClientId + "]", Log.ACCESS_LOG);
		Hashtable<String, String> lvParamMap = new Hashtable<String,String>();
		lvParamMap.put("CLIENTID", mvClientId);
		
		// finish map build data to map into xml
		Log.println("[ HKSAuthenticationManagementTxn.process() finished constructing lvParamMap [" + mvClientId + "]", Log.ACCESS_LOG);
		
		// challenge result		
		HKSEntrustChallengeInfoBean beanResult = null;
		
		//TODO: set data to test
		/*beanResult = new HKSEntrustChallengeInfoBean();
	   	beanResult.setMvReturnCode("0");
	   	beanResult.setMvReturnMessage("");
	   	beanResult.setMvChallenge("1234567890");*/
	     
	    if (TPErrorHandling.TP_NORMAL == super.process("GetEntrustChallenge", lvParamMap)){
	    	 // get return data from XML
	    	 beanResult = new HKSEntrustChallengeInfoBean();
	    	 beanResult.setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
	    	 beanResult.setMvReturnMessage(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
	    	 beanResult.setMvChallenge(mvReturnNode.getChildNode("CHALLENGE").getValue());
	    	 	    	 
	     }else{            
            //TODO: To do other logic for failure case.
	    	 if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase(
	    			 com.systekit.winvest.hks.config.mapping.ErrorCode.HKSTokenSecurityDisabledError)) {
	        	 setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode("C_ERROR_DESC").getValue()));
	         }
	    	 
	     }
	     Log.println("[ HKSAuthenticationManagementTxn.process() getEntrustChallenge ends [" + mvClientId + "]", Log.ACCESS_LOG);
	     
	     return beanResult;
	}
	
	/**
	 * method to call service to authenticate Entrust challenge
	 * @param mvClientId
	 * @param inputSeriNo
	 * @return
	 */
	public HKSEntrustChallengeInfoBean authenticate(String mvClientId, String inputSeriNo)
	{
		// log start process
		Log.println("[ HKSAuthenticationManagementTxn.process() getEntrustChallenge [" + mvClientId + ", " + inputSeriNo + "]", Log.ACCESS_LOG);
		Hashtable<String, String> lvParamMap = new Hashtable<String,String>();
		lvParamMap.put("CLIENTID", mvClientId);
		lvParamMap.put("TOKEN", inputSeriNo);
		
		// finish map build data to map into xml
		Log.println("[ HKSAuthenticationManagementTxn.process() finished constructing lvParamMap [" + mvClientId + "]", Log.ACCESS_LOG);
		
		// challenge result		
		HKSEntrustChallengeInfoBean beanResult = null;
		// TODO: FOR TEST
		//beanResult = new HKSEntrustChallengeInfoBean();
		//beanResult.setMvReturnCode("0");
	     
	     if (TPErrorHandling.TP_NORMAL == super.process("AuthenticateEntrustChallenge", lvParamMap)){
	    	 // get return data from XML
	    	 beanResult = new HKSEntrustChallengeInfoBean();
	    	 beanResult.setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
	    	 beanResult.setMvReturnMessage(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());	    	 		 
	     }else{            
            //TODO: To do other logic for failure case.
	     }
	     Log.println("[ HKSAuthenticationManagementTxn.process() getEntrustChallenge ends [" + mvClientId + ", " + inputSeriNo + "]", Log.ACCESS_LOG);
	     
	     return beanResult;
	}
	
	/**
	 * doResetToken method to call the service to reset Token
	 */
	public HKSAuthenticateResultInfoBean doResetToken(String mvClientId, String mvTokenSeriNo, String mvSeriNo1, String mvSeriNo2)
	{
		// log start process
		Log.println("[ HKSAuthenticationManagementTxn.process() doResetToken() starts [" + mvClientId + ", " + mvTokenSeriNo + ", " + mvSeriNo1 + ", " + mvSeriNo2 + "]", Log.ACCESS_LOG);
		
		Hashtable<String, String> lvParamMap = new Hashtable<String,String>();
		lvParamMap.put("CLIENTID", mvClientId);
		lvParamMap.put("SERINO", mvTokenSeriNo);
		lvParamMap.put("TOKEN", mvSeriNo1);
		//lvParamMap.put("SERINO2", mvSeriNo2);
		
		HKSAuthenticateResultInfoBean resultBean = null;
		
		// finish map build data to map into xml
		Log.println("[ HKSResetTokenTxn.process() finished constructing lvParamMap [" + mvClientId + "]", Log.ACCESS_LOG);
	     
	     if (TPErrorHandling.TP_NORMAL == super.process("ResetToken", lvParamMap)){
	    	 //TODO: To do the logic for success case.
	    	 resultBean = new HKSAuthenticateResultInfoBean();
	    	 // get return data from XML
	    	 resultBean.setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
	    	 resultBean.setMvReturnMessage(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
	    	 
	     }else{
            
            //TODO: To do other logic for failue case.
	     }
	     Log.println("[ HKSAuthenticationManagementTxn.process() doResetToken() ends [" + mvClientId + ", " + mvTokenSeriNo + ", " + mvSeriNo1 + ", " + mvSeriNo2 + "]", Log.ACCESS_LOG);
	     
	     return resultBean;
	}
	
	/**
	 * unlockTokenCard method to call the service to unlock Token or Card
	 */
	public HKSAuthenticateResultInfoBean unlockTokenCard(String mvClientId, String mvAuthenType, String mvSeriNo)
	{
		// log start process
		Log.println("[ HKSAuthenticationManagementTxn.process() unlockTokenCard() starts [" + mvClientId + ", " + mvAuthenType + ", " + mvSeriNo + "]", Log.ACCESS_LOG);
		
		Hashtable<String, String> lvParamMap = new Hashtable<String,String>();
		lvParamMap.put("CLIENTID", mvClientId);
		lvParamMap.put("AUTHENMETHOD", mvAuthenType);
		lvParamMap.put("SERINO", mvSeriNo);
		
		HKSAuthenticateResultInfoBean resultBean = null;
		
		// finish map build data to map into xml
		Log.println("[ HKSResetTokenTxn.process() finished constructing lvParamMap [" + mvClientId + "]", Log.ACCESS_LOG);
	     
	     if (TPErrorHandling.TP_NORMAL == super.process("UnlockTokenCard", lvParamMap)){
	    	 
	    	 //TODO: To do the logic for success case.
	    	 resultBean = new HKSAuthenticateResultInfoBean();
	    	 // get return data from XML
	    	 resultBean.setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
	    	 resultBean.setMvReturnMessage(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
	    	 
	     }else{
            
            //TODO: To do other logic for failue case.
	     }
	     Log.println("[ HKSAuthenticationManagementTxn.process() unlockTokenCard() ends [" + mvClientId + ", " + mvAuthenType + ", " + mvSeriNo  + "]", Log.ACCESS_LOG);
	     return resultBean;
	}
	
	/**
	 * unlockTokenCard method to call the service to unlock Token or Card
	 */
	public HKSAuthenticateResultInfoBean changeAuthenMethod(String mvClientId, String mvPassword, String mvAuthenType)
	{
		// log start process
		Log.println("[ HKSAuthenticationManagementTxn.process() changeAuthenMethod() starts [" + mvClientId + ", " + mvPassword + ", " + mvAuthenType + "]", Log.ACCESS_LOG);
		
		Hashtable<String, String> lvParamMap = new Hashtable<String,String>();
		lvParamMap.put("CLIENTID", mvClientId);
		lvParamMap.put("AUTHENMETHOD", mvAuthenType);
		lvParamMap.put("PASSWORD", mvPassword);
		
		HKSAuthenticateResultInfoBean resultBean = null;
		
		// finish map build data to map into xml
		Log.println("[ HKSResetTokenTxn.process() finished constructing lvParamMap [" + mvClientId + "]", Log.ACCESS_LOG);
	     
	     if (TPErrorHandling.TP_NORMAL == super.process("ChangeAuthenMethod", lvParamMap)){

	    	//TODO: To do the logic for success case.
	    	 resultBean = new HKSAuthenticateResultInfoBean();
	    	 // get return data from XML
	    	 resultBean.setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
	    	 resultBean.setMvReturnMessage(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
	    	 
	     }else{
            
            //TODO: To do other logic for failue case.
	     }
	     Log.println("[ HKSAuthenticationManagementTxn.process() unlockTokenCard() ends [" + mvClientId + ", " + mvPassword + ", " + mvAuthenType  + "]", Log.ACCESS_LOG);
	     
	     return resultBean;
	}
}
