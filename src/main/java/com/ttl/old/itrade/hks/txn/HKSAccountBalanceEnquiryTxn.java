package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: Client Account Balance Enquiry Transaction</p>
 * <p>Description: This class stores and processes the client account balance enquiry transaction</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.math.BigDecimal;
import java.util.Hashtable;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.util.Utils;

/**
 * The HKSAccountBalanceEnquiryTxn class definition for all methodsis stores and processes the client account balance enquiry transaction
 * 
 * @author Tree Lam
 * @since 20080625
 */
public class HKSAccountBalanceEnquiryTxn extends BaseTxn {
	private String mvClientId;
	private String mvCurrencyId;
	private String mvAccountStatus;
	private String mvAvailableBalance;
	private String mvSettledBalance;
	private String mvPendingBalance;
	private String mvDueBalance;
	private String mvLedgerBalace;
	private String mvTodaySettlement;
	private String mvHoldAmount;
	private String mvReserveAmount;
	private String mvInterest;
	private String mvMarginValue;
	private String mvMarginableValue;
	private String mvCreditLimit;
	private String mvMarginPercentage;
	private String mvMarginPos;
	private String mvMarginCall;
	private String mvSuplementCash;
	private String mvBuyingPowerd;
	private String mvExtraCreditd;
	private String mvRemaining;
	private String mvDPWD;
	private String mvDThreshold;
	private String mvCTodayBuy;
	private String mvCTodaySell;
	private String mvCTodayConfirmBuy;
	private String mvCTodayConfirmSell;
	private String mvMarketValue;
	private String mvHostAvailableBalance;
	private String mvTDaySOFValue;
	private String mvSysEarmarkAmt;
	private String mvHostEarmarkAmt;
	private String mvUnderDueTPlusOne;
	private String mvUnderDueTPlusX;
	private String mvOSBuyAmt;
	private String mvSettlementAccountNumber;
	private String mvTPlusBuyingPower;
	private String mvShortStockValue;
	private String mvTPlusXSOFValue;
	private String mvDueBuy;
	private String mvDueSell;
	private String mvUnsettleBuy;
	private String mvUnsettleSell;
	private String mvNextDayDueBuy;
	private String mvNextDayDueSell;
	private String mvInactiveBuy;
	private String mvShortSellAmt;
	// BEGIN - TASK#: CL00011 - Charlie Liu 20080829
	private String mvDrawableBalAmt;
	// END - TASK# CL00011

	// BEGIN TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info
	private String mvUsable;
	// END TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info

	private String mvAvailAdvanceMoney;
	private String mvOutstandingLoan;
	private String mvPenddingWithdrawMoney;

	private String mvTotalOutAdvance;

	// HIEU LE: used to show Cash Balance
	private String mvManualReserve;
	private String mvCashMaintenance;

	// Variable is used to identify
	public static final String CASHACCOUNTSUMMARY = "CASHACCOUNTSUMMARY";
	public static final String TRADINGACCOUNTSUMMARY = "TRADINGACCOUNTSUMMARY";

	public static final String CLIENTID = "CLIENTID";
	public static final String CURRENCYID = "CURRENCYID";
	public static final String ACCOUNTSTATUS = "ACCOUNTSTATUS";
	public static final String SETTLEDBALANCE = "SETTLEDBALANCE";
	public static final String AVAILABLEBALANCE = "AVAILABLEBALANCE";
	public static final String PENDINGBALANCE = "PENDINGBALANCE";
	public static final String DUEBALANCE = "DUEBALANCE";
	public static final String LEDGERBALANCE = "LEDGERBALANCE";
	public static final String TODAYSETTLEMENT = "TODAYSETTLEMENT";
	public static final String HOLDAMOUNT = "HOLDAMOUNT";
	public static final String RESERVEAMOUNT = "RESERVEAMOUNT";
	public static final String INTEREST = "INTEREST";
	public static final String MARGINVALUE = "MARGINVALUE";
	public static final String MARGINABLEVALUE = "MARGINABLEVALUE";
	public static final String CREDITLIMIT = "CREDITLIMIT";
	public static final String MARGINPERCENTAGE = "MARGINPERCENTAGE";
	public static final String MARGINPOS = "MARGINPOS";
	public static final String MARGINCALL = "MARGINCALL";
	public static final String SUPPLEMENTCASH = "SupplementCash";
	public static final String BUYINGPOWERD = "BUYINGPOWERD";
	public static final String VIRTUAL_CHANNEL_BUYINGPOWER = "VIRTUAL_CHANNEL_BUYINGPOWER";
	public static final String EXTRACREDITD = "EXTRACREDITD";
	public static final String REMAINING = "REMAINING";
	public static final String DPWD = "DPWD";
	public static final String DTHRESHOLD = "DTHRESHOLD";
	public static final String BANKACID = "BANKACID";

	public static final String CTODAYCONFIRMBUY = "CTODAYCONFIRMBUY";
	public static final String CTODAYCONFIRMSELL = "CTODAYCONFIRMSELL";
	public static final String TPLUSXBUYINGPOWER = "TPLUSXBUYINGPOWER";
	public static final String SHORTSTOCKVALUE = "SHORTSTOCKVALUE";
	public static final String TPLUSXSOFVALUE = "TPLUSXSOFVALUE";

	public static final String CDUEBUY = "CDUEBUY";
	public static final String CDUESELL = "CDUESELL";
	public static final String CUNSETTLEBUY = "CUNSETTLEBUY";
	public static final String CUNSETTLESELL = "CUNSETTLESELL";
	public static final String CNEXTDAYDUEBUY = "CNEXTDAYDUEBUY";
	public static final String CNEXTDAYDUESELL = "CNEXTDAYDUESELL";
	public static final String CINACTIVEBUY = "CINACTIVEBUY";
	public static final String CSHORTSELLAMT = "CSHORTSELLAMT";
	public static final String DRAWABLEBAL = "DRAWABLEBAL";
	public static final String EXTRACHANNELCREDITD = "EXTRACHANNELCREDITD";
	public static final String MARKETVALUE = "MARKETVALUE";
	public static final String CPENDINGWITHDRAWAL = "CPENDINGWITHDRAWAL";
	public static final String TOTALOUTSTANDINGADVANCEAMOUNT = "TOTALOUTSTANDINGADVANCEAMOUNT";
	public static final String CASHMAINTENANCEVALUE = "CASHMAINTENANCEVALUE";

	private String mvAccountType;

	// Begin task YuLong Xu 04 Nov 2008
	private int mvReturnCode;
	TPErrorHandling tpError;
	private String mvErrorCode;
	// End task YuLong Xu 04 Nov 2008

	// Begin Task #RC00181 - Rice Cheng 20090108
	private String mvLanguage;
	// End Task #RC00181 - Rice Cheng 20090108

	private boolean mvIsQueryAdvanceMoney;

	TPErrorHandling mvTpError;

	public HKSAccountBalanceEnquiryTxn() {
		mvTpError = new TPErrorHandling();
	}

	/**
	 * Default constructor for HKSAccountBalanceEnquiryTxn class
	 * 
	 * @param pClientId the Client Id
	 */
	public HKSAccountBalanceEnquiryTxn(String pClientId, String pAccountType) {
		this(pClientId, "", pAccountType);
	}

	/**
	 * Default constructor for HKSAccountBalanceEnquiryTxn class
	 * 
	 * @param pClientId the Client Id
	 * @param Language the user locale
	 */
	public HKSAccountBalanceEnquiryTxn(String pClientId, String pLanguage, String pAccountType) {
		super();
		tpError = new TPErrorHandling();
		setClientId(pClientId);
		setLanguage(pLanguage);
		mvAccountType = pAccountType;

	}

	/**
	 * The method process stores and processes the client account balance enquiry transaction
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void process() {
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(CLIENTID, getClientId());
		lvTxnMap.put("ENABLEADVANCEMONEYQUERY", mvIsQueryAdvanceMoney ? "Y" : "N");
		lvTxnMap.put("INCLUDECASHACCOUNTSUMMARY", "Y");
		lvTxnMap.put("INCLUDETRADINGACCOUNTSUMMARY", "Y");
		lvTxnMap.put("INCLUDETRADINGACCOUNTDETAILS", "N");

		// Begin Task #RC00181 - Rice Cheng 20090108
		if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSClientAccountBalanceEnquiry, lvTxnMap))
		// End Task #RC00181 - Rice Cheng 20090108;
		{

			setCurrencyId(mvReturnNode.getChildNode(CURRENCYID).getValue());
			setAccountStatus(mvReturnNode.getChildNode("TRADINGACCSTATUS").getValue());
			setAvailableBalance(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode("TPLUSXBUYINGPOWER").getValue());
			setSettledBalance(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(SETTLEDBALANCE).getValue());
			setPendingBalance(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(PENDINGBALANCE).getValue());
			setDueBalance(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(DUEBALANCE).getValue());
			setLedgerBalace(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(LEDGERBALANCE).getValue());
			setTodaySettlement(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(TODAYSETTLEMENT).getValue());
			setHoldAmount(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(HOLDAMOUNT).getValue());
			setReserveAmount(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(RESERVEAMOUNT).getValue());
			setMvManualReserve(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(RESERVEAMOUNT).getValue()); // HIEU LE
			setInterest(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(INTEREST).getValue());
			setDPWD(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(DPWD).getValue());
			setDThreshold(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(DTHRESHOLD).getValue());

			setCTodayConfirmBuy(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CTODAYCONFIRMBUY).getValue());
			setCTodayConfirmSell(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CTODAYCONFIRMSELL).getValue());
			setCTodayBuy(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CTODAYBUY").getValue());
			setCTodaySell(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("CTODAYSELL").getValue());

			setCDueBuy(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CDUEBUY).getValue());
			setCDueSell(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CDUESELL).getValue());
			setCUnsettleBuy(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CUNSETTLEBUY).getValue());
			setCUnsettleSell(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CUNSETTLESELL).getValue());
			setCNextDayDueBuy(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CNEXTDAYDUEBUY).getValue());
			setCNextDayDueSell(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CNEXTDAYDUESELL).getValue());
			setCInactiveBuy(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CINACTIVEBUY).getValue());
			setCShortSellAmt(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CSHORTSELLAMT).getValue());

			// BEGIN - TASK#: CL00011 - Charlie Liu 20080829
			setDrawableBal(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(DRAWABLEBAL).getValue());
			// END - TASK# CL00011

			// BEGIN TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info
			this.setUsable(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("USABLEBALANCE").getValue());
			// END TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info

			// BEGIN TASK: TTL-VN VanTran 20101004 Add advance amount
			if (mvIsQueryAdvanceMoney) {
				this.setMvAvailAdvanceMoney(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("AVAILABLEADVANCEMONEY").getValue());
				setMvOutstandingLoan(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("OUTSTANDINGLOAN").getValue());
				setMvTotalOutAdvance(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(TOTALOUTSTANDINGADVANCEAMOUNT).getValue());
			}
			// END TASK: TTL-VN VanTran 20101004 Add advance amount

			if (null != (mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(TagName.HOST_AVAILABLE_BALANCE))) {
				setHostAvailableBalance(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(TagName.HOST_AVAILABLE_BALANCE)
						.getValue());
				setTDaySOFValue(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("TDAYSOFVALUE").getValue());
				setHostEarmarkAmt(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("HOST_EARMARK_AMT").getValue());
				setSysEarmarkAmt(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("SYS_EARMARK_AMT").getValue());
				setUnderDueTPlusX(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("UNDERDUETPLUSX").getValue());
				setUnderDueTPlusOne(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("UNDERDUETPLUSONE").getValue());
				setOSBuyAmt(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode("OSBUYAMT").getValue());
			}

			setMarginValue(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(MARGINVALUE).getValue());
			setMarginableValue(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(MARGINABLEVALUE).getValue());
			setCreditLimit(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(CREDITLIMIT).getValue());
			setMarginPercentage(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(MARGINPERCENTAGE).getValue());
			setMarginPos(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(MARGINPOS).getValue());
			setMarginCall(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(MARGINCALL).getValue());
			setMvSuplementCash(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(SUPPLEMENTCASH).getValue());

			String extraCreditValue = mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(EXTRACREDITD).getValue();
			String extraChannelCreditValue = mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(EXTRACHANNELCREDITD).getValue();
			String boBuyingPowerd = mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode("BUYINGPOWER").getValue();

			if (Utils.parseDouble(extraChannelCreditValue) > 0) {
				setBuyingPowerd(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(VIRTUAL_CHANNEL_BUYINGPOWER).getValue());
			} else {
				BigDecimal finalBuyingPowerd = Utils.parseBigDecimal(boBuyingPowerd).subtract(Utils.parseBigDecimal(extraCreditValue))
						.add(Utils.parseBigDecimal(extraChannelCreditValue));
				setBuyingPowerd(finalBuyingPowerd.toString());
			}

			setExtraCreditd(extraCreditValue);
			setRemaining(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(REMAINING).getValue());

			setMarketValue(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(MARKETVALUE).getValue());

			setSettlementAccountNumber(mvReturnNode.getChildNode(BANKACID).getValue());
			setTPlusXBuyingPower(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(TPLUSXBUYINGPOWER).getValue());
			setShortStockValue(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(SHORTSTOCKVALUE).getValue());
			setTPlusXSOFValue(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(TPLUSXSOFVALUE).getValue());

			setMvPenddingWithdrawMoney(mvReturnNode.getChildNode(CASHACCOUNTSUMMARY).getChildNode(CPENDINGWITHDRAWAL).getValue());
			setMvCashMaintenance(mvReturnNode.getChildNode(TRADINGACCOUNTSUMMARY).getChildNode(CASHMAINTENANCEVALUE).getValue());
		} else { // Unhandled Business Exception
			setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), ""));

			// Handle special cases
			// 1. Agreement is not signed
			if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("HKSFOE00026")) {
				setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode(
						"C_ERROR_DESC").getValue()));
			}

			// 2. When TP is down
			if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
				setErrorCode(new ErrorCode(new String[0], "0100", "No connection with TP"));
			}
		}
	}

	/**
	 * Get method for Client ID
	 * 
	 * @return Client ID
	 */
	public String getClientId() {
		return mvClientId;
	}

	/**
	 * Set method for Client ID
	 * 
	 * @param pClientId the Client ID
	 */
	public void setClientId(String pClientId) {
		mvClientId = pClientId;
	}

	/**
	 * Get the currency ID
	 * 
	 * @return Currency ID (ISO standard)
	 */
	public String getCurrencyId() {
		return mvCurrencyId;
	}

	/**
	 * Set the currency ID
	 * 
	 * @param pCurrencyId the Currency ID (ISO standard)
	 */
	public void setCurrencyId(String pCurrencyId) {
		mvCurrencyId = pCurrencyId;
	}

	/**
	 * Get the account status
	 * 
	 * @return The account status
	 */
	public String getAccountStatus() {
		return mvAccountStatus;
	}

	/**
	 * Set the account status
	 * 
	 * @param pAccountStatus the account status
	 */
	public void setAccountStatus(String pAccountStatus) {
		mvAccountStatus = pAccountStatus;
	}

	/**
	 * Get settled balance
	 * 
	 * @return Settled balance
	 */
	public String getSettledBalance() {
		return mvSettledBalance;
	}

	/**
	 * Set the settled balance
	 * 
	 * @param pSettledBalance the settled balance
	 */
	public void setSettledBalance(String pSettledBalance) {
		mvSettledBalance = pSettledBalance;
	}

	/**
	 * Get the available balance
	 * 
	 * @return Available balance
	 */
	public String getAvailableBalance() {
		return mvAvailableBalance;
	}

	/**
	 * Set the available balance
	 * 
	 * @param pAvailableBalance the available balance
	 */
	public void setAvailableBalance(String pAvailableBalance) {
		mvAvailableBalance = pAvailableBalance;
	}

	/**
	 * Get the Pending Balance
	 * 
	 * @return Pending Balance
	 */
	public String getPendingBalance() {
		return mvPendingBalance;
	}

	/**
	 * Set the pending balance
	 * 
	 * @param pPendingBalance the pending balance
	 */
	public void setPendingBalance(String pPendingBalance) {
		mvPendingBalance = pPendingBalance;
	}

	/**
	 * Get Due balance
	 * 
	 * @return Due balance
	 */
	public String getDueBalance() {
		return mvDueBalance;
	}

	/**
	 * Set due balance
	 * 
	 * @param pDueBalance the due balance
	 */
	public void setDueBalance(String pDueBalance) {
		mvDueBalance = pDueBalance;
	}

	/**
	 * Get Ledger balance
	 * 
	 * @return Ledger balance
	 */
	public String getLedgerBalace() {
		return mvLedgerBalace;
	}

	/**
	 * Set ledger balance
	 * 
	 * @param pLedgerBalace the ledger balance
	 */
	public void setLedgerBalace(String pLedgerBalace) {
		mvLedgerBalace = pLedgerBalace;
	}

	/**
	 * Get Today Settlement
	 * 
	 * @return Today Settlement
	 */
	public String getTodaySettlement() {
		return mvTodaySettlement;
	}

	/**
	 * Set today settlement
	 * 
	 * @param pTodaySettlement the today settlement
	 */
	public void setTodaySettlement(String pTodaySettlement) {
		mvTodaySettlement = pTodaySettlement;
	}

	/**
	 * Get Hold Amount
	 * 
	 * @return Hold Amount
	 */
	public String getHoldAmount() {
		return mvHoldAmount;
	}

	/**
	 * Set Hold Amount
	 * 
	 * @param pHoldAmount the Hold Amount
	 */
	public void setHoldAmount(String pHoldAmount) {
		mvHoldAmount = pHoldAmount;
	}

	/**
	 * Get Reserve Amount
	 * 
	 * @return Reserve Amount
	 */
	public String getReserveAmount() {
		return mvReserveAmount;
	}

	/**
	 * Set Reserve Amount
	 * 
	 * @param pReserveAmount the Reserve Amount
	 */
	public void setReserveAmount(String pReserveAmount) {
		mvReserveAmount = pReserveAmount;
	}

	/**
	 * Get Interest
	 * 
	 * @return Interest
	 */
	public String getInterest() {
		return mvInterest;
	}

	/**
	 * Set interest
	 * 
	 * @param pInterest the interest
	 */
	public void setInterest(String pInterest) {
		mvInterest = pInterest;
	}

	/**
	 * Get Margin Value
	 * 
	 * @return Margin Value
	 */
	public String getMarginValue() {
		return mvMarginValue;
	}

	/**
	 * Set margin value
	 * 
	 * @param pMarginValue the margin value
	 */
	public void setMarginValue(String pMarginValue) {
		mvMarginValue = pMarginValue;
	}

	/**
	 * Get Marginable Value
	 * 
	 * @return Marginable Value
	 */
	public String getMarginableValue() {
		return mvMarginableValue;
	}

	/**
	 * Set marginable value
	 * 
	 * @param pMarginableValue
	 */
	public void setMarginableValue(String pMarginableValue) {
		mvMarginableValue = pMarginableValue;
	}

	/**
	 * Get Credit Limit
	 * 
	 * @return Credit Limit
	 */
	public String getCreditLimit() {
		return mvCreditLimit;
	}

	/**
	 * Set credit limit
	 * 
	 * @param pCreditLimit the credit limit
	 */
	public void setCreditLimit(String pCreditLimit) {
		mvCreditLimit = pCreditLimit;
	}

	/**
	 * Get Margin Percentage
	 * 
	 * @return Margin Percentage
	 */
	public String getMarginPercentage() {
		return mvMarginPercentage;
	}

	/**
	 * Set margin percentage
	 * 
	 * @param pMarginPercentage the margin percentage
	 */
	public void setMarginPercentage(String pMarginPercentage) {
		mvMarginPercentage = pMarginPercentage;
	}

	/**
	 * Get Margin Position
	 * 
	 * @return Margin Position
	 */
	public String getMarginPos() {
		return mvMarginPos;
	}

	/**
	 * Set margin position
	 * 
	 * @param pMarginPos the margin position
	 */
	public void setMarginPos(String pMarginPos) {
		mvMarginPos = pMarginPos;
	}

	/**
	 * Get Margin Call
	 * 
	 * @return the Margin Call
	 */
	public String getMarginCall() {
		return mvMarginCall;
	}

	/**
	 * Set Margin Call
	 * 
	 * @param pMarginCall the Margin Call
	 */
	public void setMarginCall(String pMarginCall) {
		mvMarginCall = pMarginCall;
	}

	/**
	 * Get Buying Power
	 * 
	 * @return Buying Power
	 */
	public String getBuyingPowerd() {
		return mvBuyingPowerd;
	}

	/**
	 * Set buying power
	 * 
	 * @param pBuyingPowerd the buying power
	 */
	public void setBuyingPowerd(String pBuyingPowerd) {
		mvBuyingPowerd = pBuyingPowerd;
	}

	/**
	 * Get Extra Credit
	 * 
	 * @return Extra Credit
	 */
	public String getExtraCreditd() {
		return mvExtraCreditd;
	}

	/**
	 * Set extra credit
	 * 
	 * @param pExtraCreditd the extra credit
	 */
	public void setExtraCreditd(String pExtraCreditd) {
		mvExtraCreditd = pExtraCreditd;
	}

	/**
	 * Get remaining extra credit
	 * 
	 * @return the remaining extra credit
	 */
	public String getRemaining() {
		return mvRemaining;
	}

	/**
	 * Set remaining extra credit
	 * 
	 * @param pRemaining the remaining extra credit
	 */
	public void setRemaining(String pRemaining) {
		mvRemaining = pRemaining;
	}

	/**
	 * Get deposit withdrawal
	 * 
	 * @return Deposit withdrawal
	 */
	public String getDPWD() {
		return mvDPWD;
	}

	/**
	 * Set deposit withdrawal
	 * 
	 * @param pDPWD the deposit withdrawal
	 */
	public void setDPWD(String pDPWD) {
		mvDPWD = pDPWD;
	}

	/**
	 * Get Daily threshold
	 * 
	 * @return Daily threshold
	 */
	public String getDThreshold() {
		return mvDThreshold;
	}

	/**
	 * Set Daily threshold
	 * 
	 * @param pDThreshold the Daily threshold
	 */
	public void setDThreshold(String pDThreshold) {
		mvDThreshold = pDThreshold;
	}

	/**
	 * Get Today confirmed buy amount
	 * 
	 * @return Today confirmed buy amount
	 */
	public String getCTodayConfirmBuy() {
		return mvCTodayConfirmBuy;
	}

	/**
	 * Set Today confirm buy amount
	 * 
	 * @param pCTodayConfirmBuy the Today confirmed buy amount
	 */
	public void setCTodayConfirmBuy(String pCTodayConfirmBuy) {
		mvCTodayConfirmBuy = pCTodayConfirmBuy;
	}

	/**
	 * Get Today Confirmed Sell amount
	 * 
	 * @return Today Confirmed Sell amount
	 */
	public String getCTodayConfirmSell() {
		return mvCTodayConfirmSell;
	}

	/**
	 * Set today confirmed sell amount
	 * 
	 * @param pCTodayConfirmSell the Today Confirmed Sell amount
	 */
	public void setCTodayConfirmSell(String pCTodayConfirmSell) {
		mvCTodayConfirmSell = pCTodayConfirmSell;
	}

	/**
	 * Get Today Buy amount
	 * 
	 * @return Today Buy amount
	 */
	public String getCTodayBuy() {
		return mvCTodayBuy;
	}

	/**
	 * Set Today Buy Amount
	 * 
	 * @param pCTodayBuy the Today Buy Amount
	 */
	public void setCTodayBuy(String pCTodayBuy) {
		mvCTodayBuy = pCTodayBuy;
	}

	/**
	 * Get Today Sell Amount
	 * 
	 * @return Today Sell Amount
	 */
	public String getCTodaySell() {
		return mvCTodaySell;
	}

	/**
	 * Set Today Sell Amount
	 * 
	 * @param pCTodaySell
	 */
	public void setCTodaySell(String pCTodaySell) {
		mvCTodaySell = pCTodaySell;
	}

	/**
	 * Get Due Buy Amount
	 * 
	 * @return Due Buy Amount
	 */
	public String getCDueBuy() {
		return mvDueBuy;
	}

	/**
	 * Set Due Buy Amount
	 * 
	 * @param pDueBuy the Due Buy Amount
	 */
	public void setCDueBuy(String pDueBuy) {
		mvDueBuy = pDueBuy;
	}

	/**
	 * Get Due Sell Amount
	 * 
	 * @return Due Sell Amount
	 */
	public String getCDueSell() {
		return mvDueSell;
	}

	/**
	 * Set Due Sell Amount
	 * 
	 * @param pDueSell the Due Sell Amount
	 */
	public void setCDueSell(String pDueSell) {
		mvDueSell = pDueSell;
	}

	/**
	 * Get Next Day Due Buy Amount
	 * 
	 * @return Next Day Due Buy Amount
	 */
	public String getCNextDayDueBuy() {
		return mvNextDayDueBuy;
	}

	/**
	 * Set Next Day Due Buy Amount
	 * 
	 * @param pNextDayDueBuy the Next Day Due Buy Amount
	 */
	public void setCNextDayDueBuy(String pNextDayDueBuy) {
		mvNextDayDueBuy = pNextDayDueBuy;
	}

	/**
	 * Get Next Day Due Sell Amount
	 * 
	 * @return Next Day Due Sell Amount
	 */
	public String getCNextDayDueSell() {
		return mvNextDayDueSell;
	}

	/**
	 * Set Next Day Due Sell Amount
	 * 
	 * @param pNextDayDueSell the Next Day Due Sell Amount
	 */
	public void setCNextDayDueSell(String pNextDayDueSell) {
		mvNextDayDueSell = pNextDayDueSell;
	}

	/**
	 * Get Inactive Buy Amount
	 * 
	 * @return Inactive Buy Amount
	 */
	public String getCInactiveBuy() {
		return mvInactiveBuy;
	}

	/**
	 * Set Inactive Buy Amount
	 * 
	 * @param pInactiveBuy the Inactive Buy Amount
	 */
	public void setCInactiveBuy(String pInactiveBuy) {
		mvInactiveBuy = pInactiveBuy;
	}

	/**
	 * Get Short Sell Amount
	 * 
	 * @return Short Sell Amount
	 */
	public String getCShortSellAmt() {
		return mvShortSellAmt;
	}

	/**
	 * Set Short Sell Amount
	 * 
	 * @param pShortSellAmt the Short Sell Amount
	 */
	public void setCShortSellAmt(String pShortSellAmt) {
		mvShortSellAmt = pShortSellAmt;
	}

	/**
	 * Get Unsettle Buy Amount
	 * 
	 * @return Unsettle Buy Amount
	 */
	public String getCUnsettleBuy() {
		return mvUnsettleBuy;
	}

	/**
	 * Set Unsettle buy amount
	 * 
	 * @param pUnsettleBuy the Unsettle buy amount
	 */
	public void setCUnsettleBuy(String pUnsettleBuy) {
		mvUnsettleBuy = pUnsettleBuy;
	}

	/**
	 * Get Unsettle sell amount
	 * 
	 * @return Unsettle sell amount
	 */
	public String getCUnsettleSell() {
		return mvUnsettleSell;
	}

	/**
	 * Set unsettle sell amount
	 * 
	 * @param pUnsettleSell the unsettle sell amount
	 */
	public void setCUnsettleSell(String pUnsettleSell) {
		mvUnsettleSell = pUnsettleSell;
	}

	/**
	 * Set market value
	 * 
	 * @param pMarketValue the market value
	 */
	public void setMarketValue(String pMarketValue) {
		mvMarketValue = pMarketValue;
	}

	/**
	 * Get market value
	 * 
	 * @return Market value
	 */
	public String getMarketValue() {
		return mvMarketValue;
	}

	/**
	 * Set host available balance
	 * 
	 * @param pHostAvailableBalance the Host available balance
	 */
	public void setHostAvailableBalance(String pHostAvailableBalance) {
		mvHostAvailableBalance = pHostAvailableBalance;
	}

	/**
	 * Get Host available balance
	 * 
	 * @return Host available balance
	 */
	public String getHostAvailableBalance() {
		return mvHostAvailableBalance;
	}

	/**
	 * Set Today SOF Value (Margin Value)
	 * 
	 * @param pTDaySOFValue the Today SOF Value (Margin Value)
	 */
	public void setTDaySOFValue(String pTDaySOFValue) {
		mvTDaySOFValue = pTDaySOFValue;
	}

	/**
	 * Get Today SOF Value (Margin Value)
	 * 
	 * @return Today SOF Value (Margin Value)
	 */
	public String getTDaySOFValue() {
		return mvTDaySOFValue;
	}

	/**
	 * Set System Earmark Amount
	 * 
	 * @param pSysEarmarkAmt the System Earmark Amount
	 */
	public void setSysEarmarkAmt(String pSysEarmarkAmt) {
		mvSysEarmarkAmt = pSysEarmarkAmt;
	}

	/**
	 * Get System Earmark Amount
	 * 
	 * @return System Earmark Amount
	 */
	public String getSysEarmarkAmt() {
		return mvSysEarmarkAmt;
	}

	/**
	 * Set Host Earmark Amount
	 * 
	 * @param pHostEarmarkAmt the Host Earmark Amount
	 */
	public void setHostEarmarkAmt(String pHostEarmarkAmt) {
		mvHostEarmarkAmt = pHostEarmarkAmt;
	}

	/**
	 * Get Host Earmark Amount
	 * 
	 * @return Host Earmark Amount
	 */
	public String getHostEarmarkAmt() {
		return mvHostEarmarkAmt;
	}

	/**
	 * Set Underdue T+1 Amount
	 * 
	 * @param pUnderDueTPlusOne the Underdue T+1 Amount
	 */
	public void setUnderDueTPlusOne(String pUnderDueTPlusOne) {
		mvUnderDueTPlusOne = pUnderDueTPlusOne;
	}

	/**
	 * Get Underdue T+1 Amount
	 * 
	 * @return Underdue T+1 Amount
	 */
	public String getUnderDueTPlusOne() {
		return mvUnderDueTPlusOne;
	}

	/**
	 * Set Underdue T+X amount
	 * 
	 * @param pUnderDueTPlusX the Underdue T+X Amount
	 */
	public void setUnderDueTPlusX(String pUnderDueTPlusX) {
		mvUnderDueTPlusX = pUnderDueTPlusX;
	}

	/**
	 * Get Underdue T+X Amount
	 * 
	 * @return Underdue T+X Amount
	 */
	public String getUnderDueTPlusX() {
		return mvUnderDueTPlusX;
	}

	/**
	 * Set outstanding buy amount
	 * 
	 * @param pOSBuyAmt the Outstanding Buy Amount
	 */
	public void setOSBuyAmt(String pOSBuyAmt) {
		mvOSBuyAmt = pOSBuyAmt;
	}

	/**
	 * Get Outstanding Buy Amount
	 * 
	 * @return Outstanding Buy Amount
	 */
	public String getOSBuyAmt() {
		return mvOSBuyAmt;
	}

	/**
	 * Get Settlement account number
	 * 
	 * @return Settlement account number
	 */
	public String getSettlementAccountNumber() {
		return mvSettlementAccountNumber;
	}

	/**
	 * Set settlement account number
	 * 
	 * @param pSettlementAccountNumber the Settlement account number
	 */
	public void setSettlementAccountNumber(String pSettlementAccountNumber) {
		mvSettlementAccountNumber = pSettlementAccountNumber;
	}

	// Added by Bowen Chau on 21 Mar 2006
	/**
	 * Get T+X Buying Power
	 * 
	 * @return T+X Buying Power
	 */
	public String getTPlusXBuyingPower() {
		return mvTPlusBuyingPower;
	}

	/**
	 * Set T+X Buying Power
	 * 
	 * @param pTPlusBuyingPower the T+X Buying Power
	 */
	public void setTPlusXBuyingPower(String pTPlusBuyingPower) {
		mvTPlusBuyingPower = pTPlusBuyingPower;
	}

	// Added by Bowen Chau on 30 Mar 2006
	/**
	 * Get Short Stock value
	 * 
	 * @return Short Stock value
	 */
	public String getShortStockValue() {
		return mvShortStockValue;
	}

	/**
	 * Set Short stock value
	 * 
	 * @param pShortStockValue the Short Stock value
	 */
	public void setShortStockValue(String pShortStockValue) {
		mvShortStockValue = pShortStockValue;
	}

	/**
	 * Set T+X SOF Value
	 * 
	 * @param pTPlusXSOFValue the T+X SOF Value
	 */
	public void setTPlusXSOFValue(String pTPlusXSOFValue) {
		mvTPlusXSOFValue = pTPlusXSOFValue;
	}

	/**
	 * Get T+X SOF Value
	 * 
	 * @return the T+X SOF Value
	 */
	public String getTPlusXSOFValue() {
		return mvTPlusXSOFValue;
	}

	// BEGIN - TASK#: CL00011 - Charlie Liu 20080829
	/**
	 * Get Drawable Amount Value
	 * 
	 * @return the Draw able Amount Value
	 */
	public String getDrawableBal() {
		return mvDrawableBalAmt;
	}

	/**
	 * Set Drawable Amount Value
	 * 
	 * @param pDrawableBalAmt the Drawable Amount Value
	 */
	public void setDrawableBal(String pDrawableBalAmt) {
		mvDrawableBalAmt = pDrawableBalAmt;
	}

	// End - TASK# CL00011

	// BEGIN TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info
	/**
	 * Get method for the Usable
	 * 
	 * @return the Usable
	 */
	public String getUsable() {
		return mvUsable;
	}

	/**
	 * Set method for the Usable
	 * 
	 * @param pUsable the Usable
	 */
	public void setUsable(String pUsable) {
		this.mvUsable = pUsable;
	}

	// END TASK: TTL-GZ-JJ-00001 JiangJi 20091206 [iTradeR5 VN] FO:Pop up stock info

	// Begin task YuLong Xu 04 Nov 2008
	/**
	 * Get method for the system Code
	 * 
	 * @return the system Code
	 */
	public int getMvReturnCode() {
		return mvReturnCode;
	}

	/**
	 * Set method for the system Code
	 * 
	 * @param pReturnCode the system Code
	 */
	public void setMvReturnCode(int pReturnCode) {
		this.mvReturnCode = pReturnCode;
	}

	/**
	 * Get method for the Error Code
	 * 
	 * @return the Error Code
	 */
	public String getMvErrorCode() {
		return mvErrorCode;
	}

	/**
	 * Set method for the Error Code
	 * 
	 * @param pErrorCode the Error Code
	 */
	public void setMvErrorCode(String pErrorCode) {
		this.mvErrorCode = pErrorCode;
	}

	// End task YuLong Xu 04 Nov 2008

	// Begin Task #RC00181 - Rice Cheng 20090108
	/**
	 * Get method for the Language
	 * 
	 * @return The user locale
	 */
	public String getLanguage() {
		return mvLanguage;
	}

	/**
	 * Set method for the Language
	 * 
	 * @param pLanguage The user locale
	 */
	public void setLanguage(String pLanguage) {
		mvLanguage = pLanguage;
	}

	// End Task #RC00181 - Rice Cheng 20090108

	public void setMvAvailAdvanceMoney(String mvAvailAdvanceMoney) {
		this.mvAvailAdvanceMoney = mvAvailAdvanceMoney;
	}

	public String getMvAvailAdvanceMoney() {
		return mvAvailAdvanceMoney;
	}

	public void setMvOutstandingLoan(String mvOutstandingLoan) {
		this.mvOutstandingLoan = mvOutstandingLoan;
	}

	public String getMvOutstandingLoan() {
		return mvOutstandingLoan;
	}

	public void setMvIsQueryAdvanceMoney(boolean mvIsQueryAdvanceMoney) {
		this.mvIsQueryAdvanceMoney = mvIsQueryAdvanceMoney;
	}

	public boolean isMvIsQueryAdvanceMoney() {
		return mvIsQueryAdvanceMoney;
	}

	public void setMvPenddingWithdrawMoney(String mvPenddingWithdrawMoney) {
		this.mvPenddingWithdrawMoney = mvPenddingWithdrawMoney;
	}

	public String getMvPenddingWithdrawMoney() {
		return mvPenddingWithdrawMoney;
	}

	/**
	 * @param mvTotalOutAdvance the mvTotalOutAdvance to set
	 */
	public void setMvTotalOutAdvance(String mvTotalOutAdvance) {
		this.mvTotalOutAdvance = mvTotalOutAdvance;
	}

	/**
	 * @return the mvTotalOutAdvance
	 */
	public String getMvTotalOutAdvance() {
		return mvTotalOutAdvance;
	}

	public String getMvManualReserve() {
		return mvManualReserve;
	}

	public void setMvManualReserve(String mvManualReserve) {
		this.mvManualReserve = mvManualReserve;
	}

	public String getMvAccountType() {
		return mvAccountType;
	}

	public void setMvAccountType(String mvAccountType) {
		this.mvAccountType = mvAccountType;
	}

	public String getMvSuplementCash() {
		return mvSuplementCash;
	}

	public void setMvSuplementCash(String mvSuplementCash) {
		this.mvSuplementCash = mvSuplementCash;
	}

	public String getMvCashMaintenance() {
		return mvCashMaintenance;
	}

	public void setMvCashMaintenance(String mvCashMaintenance) {
		this.mvCashMaintenance = mvCashMaintenance;
	}

}
