package com.ttl.old.itrade.hks.util;


public class DisplayNumber{
	private String mvUnformatStr;
	private int mvDecimalPlaces = 0;
	private String[] lvLanguageAndCountry = {"Vi-VN"};
	
	/**
	 * Constructor with two parameters.
	 * @param pUnformatted The value will be format.
	 * @param pDecimalPlaces The value control vale after format 
	 *        show be keep how many decimal places.
	 */
	public DisplayNumber(String pUnformatted,int pDecimalPlaces) {
		mvUnformatStr = pUnformatted;
		mvDecimalPlaces = pDecimalPlaces;
	}
	
	/**
	 * Constructor with one parameter.
	 * @param pUnformatted The value will be format.
	 */
	public DisplayNumber(String pUnformatted) {
		mvUnformatStr = pUnformatted;
	}
	
	/**
	 * Rewrite toString function
	 * @return a format value.
	 *         the format value is local currency with different decimal places and different format
	 *         according to a different language.
	 */
	public String toString(){
		String code = "0";
		try
		{
			code = TextFormatter.formatLocaleNumber(lvLanguageAndCountry[0], lvLanguageAndCountry[1], mvUnformatStr, mvDecimalPlaces, mvDecimalPlaces);
		}
		catch(Exception ex)
		{
			
		}
		
		return code;
	}
}
