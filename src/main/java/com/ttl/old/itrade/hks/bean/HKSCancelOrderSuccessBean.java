package com.ttl.old.itrade.hks.bean;

/**
 * The HKSCancelOrderSuccessBean class define variables that to save values 
 * for action 
 * @author Wind Zhao
 *
 */
public class HKSCancelOrderSuccessBean {
	private String mvErrorMessageBox;
	private String mvTextBoxClose;
	private String mvReasonMessage;
	private String mvBSValue;
	private String mvClientId;
	private String mvDateTime;
	private String mvOrderId;
	private String mvOrderGroupId;
	private String mvBuyOrSell;
	private String mvInstrumentId;
	private String mvMarketId;
	private String mvInstrumentName;
	private String mvCurrencyId;
	private String mvPrice;
	private String mvQuantity;
	private String mvOrderType;
	private String mvFilledQty;
	private String mvOSQty;
	private String mvGoodTillDate;
	private String mvGoodTillDateDisable;
	private String mvQuantityDescription;
	private String mvOrderTypeDescription;
	private String mvGoodTillDescription;
	private String mvMultiMarketDisable;
	//Begin Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for ITradeR5 [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    /*
     * mvCancelOrderFor to control hidden or show some fields in 
     * cancel order
     */
    private String mvCancelOrderFor;
    
    /**
     * This method returns the symbol
     * to control show or hidden some fields.
     * @return the cancel order for value.
     */
    public String getMvCancelOrderFor() {
    	return mvCancelOrderFor;
    }
    
    /**
     * This method sets the a symbol
     * to control show or hidden some fields.
     * @param pCancelOrderFor The symbol to control show or hidden some fields.
     */
    public void setMvCancelOrderFor(String pCancelOrderFor) {
    	mvCancelOrderFor = pCancelOrderFor;
    }
    //End Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    
    /**
     * This method returns the error message box which action.
     * @return the error message box.
     */
	public String getMvErrorMessageBox() {
		return mvErrorMessageBox;
	}
	
	/**
     * This method sets the error message box
     * is a text area object can be display.
     * @param pErrorMessageBox The error message box.
     */
	public void setMvErrorMessageBox(String pErrorMessageBox) {
		this.mvErrorMessageBox = pErrorMessageBox;
	}
	
	/**
     * This method returns the error message box end.
     * @return the error message box end.
     */
	public String getMvTextBoxClose() {
		return mvTextBoxClose;
	}
	
	/**
     * This method sets the error message box end
     * is a text area object can be display.
     * @param pTextBoxClose The error message box end.
     */
	public void setMvTextBoxClose(String pTextBoxClose) {
		this.mvTextBoxClose = pTextBoxClose;
	}
	
	/**
     * This method returns the waring message.
     * @return the waring message.
     * @type String.
     */
	public String getMvReasonMessage() {
		return mvReasonMessage;
	}
	
	/**
     * This method sets the waring message
     * @param pReasonMessage The waring message.
     * @type String.
     */
	public void setMvReasonMessage(String pReasonMessage) {
		this.mvReasonMessage = pReasonMessage;
	}
	
	/**
     * This method returns the B(Buy) or S(Sell).
     * @return the B(Buy) or S(sell) of the order.
     */
	public String getMvBSValue() {
		return mvBSValue;
	}
	
	/**
     * This method sets the B(Buy) or S(Sell).
     * @param pBSValue The B(Buy) or S(Sell) of the order.
     */
	public void setMvBSValue(String pBSValue) {
		this.mvBSValue = pBSValue;
	}
	
	/**
     * This method returns the client id.
     * @return the client id.
     */
	public String getMvClientId() {
		return mvClientId;
	}
	
	/**
     * This method sets client id.
     * @param pClientId The client id.
     */
	public void setMvClientId(String pClientId) {
		this.mvClientId = pClientId;
	}
	
	/**
     * This method returns the date time.
     * @return the date time of the order.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time.
     * @param pDateTime The date time of the order.
     */
	public void setMvDateTime(String pDateTime) {
		this.mvDateTime = pDateTime;
	}
	
	/**
     * This method returns the order id.
     * @return the order id of the order.
     */
	public String getMvOrderId() {
		return mvOrderId;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderId The order id.
     */
	public void setMvOrderId(String pOrderId) {
		this.mvOrderId = pOrderId;
	}
	
	/**
     * This method returns the order group id.
     * @return the order group id of the order.
     */
	public String getMvOrderGroupId() {
		return mvOrderGroupId;
	}
	
	/**
     * This method sets the order group id.
     * @param pOrderGroupId The order group id of the order.
     */
	public void setMvOrderGroupId(String pOrderGroupId) {
		this.mvOrderGroupId = pOrderGroupId;
	}
	
	/**
     * This method returns the buy or sell.
     * @return the buy or sell of the order.
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the buy or sell.
     * @param pBuyOrSell The buy or sell of the order.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		this.mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the instrument id.
     * @return the instrument id of the order.
     */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}
	
	/**
     * This method sets the instrument id.
     * @param pInstrumentId The instrument id of the order.
     */
	public void setMvInstrumentId(String pInstrumentId) {
		this.mvInstrumentId = pInstrumentId;
	}
	
	/**
     * This method returns the market id.
     * @return the market id of the order.
     */
	public String getMvMarketId() {
		return mvMarketId;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketId The market id of the order.
     */
	public void setMvMarketId(String pMarketId) {
		this.mvMarketId = pMarketId;
	}
	
	/**
     * This method returns the instrument name.
     * @return the instrument name of the order.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name.
     * @param pInstrumentName The instrument name of the order.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		this.mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the currency id.
     * @return the currency id of the order.
     */
	public String getMvCurrencyId() {
		return mvCurrencyId;
	}
	
	/**
     * This method sets the currency id.
     * @param pCurrencyId The currency id of the order.
     */
	public void setMvCurrencyId(String pCurrencyId) {
		this.mvCurrencyId = pCurrencyId;
	}
	
	/**
     * This method returns the price.
     * @return the price of the order is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price.
     * @param pPrice The price of the order is formatted.
     */
	public void setMvPrice(String pPrice) {
		this.mvPrice = pPrice;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity of the order is formatted.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity.
     * @param pQuantity The quantity of the order is formatted.
     */
	public void setMvQuantity(String pQuantity) {
		this.mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the order type.
     * @return the order type of the order.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the order type.
     * @param pOrderType The order type of the order.
     */
	public void setMvOrderType(String pOrderType) {
		this.mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the filled quantity.
     * @return the filled quantity of the order.
     */
	public String getMvFilledQty() {
		return mvFilledQty;
	}
	
	/**
     * This method sets the filled quantity.
     * @param  pFilledQty The filled quantity of the order.
     */
	public void setMvFilledQty(String pFilledQty) {
		this.mvFilledQty = pFilledQty;
	}
	
	/**
     * This method returns the out standing quantity.
     * @return the out standing quantity of the order.
     */
	public String getMvOSQty() {
		return mvOSQty;
	}
	
	/**
     * This method sets the out standing quantity.
     * @param pOSQty The out standing quantity of the order is formatted.
     */
	public void setMvOSQty(String pOSQty) {
		this.mvOSQty = pOSQty;
	}
	
	/**
     * This method returns the good till date.
     * @return the good till date of the order is formatted.
     */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	
	/**
     * This method sets the good till date.
     * @param pGoodTillDate The good till date of the order is formatted.
     */
	public void setMvGoodTillDate(String pGoodTillDate) {
		this.mvGoodTillDate = pGoodTillDate;
	}
	
	/**
     * This method returns the good till date disable value
     * to control show or hidden the good till date.
     * @return the good till date disable value.
     */
	public String getMvGoodTillDateDisable() {
		return mvGoodTillDateDisable;
	}
	
	/**
     * This method sets the good till date disable.
     * @param pGoodTillDateDisable The good till date disable of the order.
     */
	public void setMvGoodTillDateDisable(String pGoodTillDateDisable) {
		this.mvGoodTillDateDisable = pGoodTillDateDisable;
	}
	
	/**
     * This method returns the quantity description.
     * @return the quantity description of the order.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description.
     * @param pQuantityDescription The quantity description of the order.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		this.mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the order type description.
     * @return the order type description of the order.
     */
	public String getMvOrderTypeDescription() {
		return mvOrderTypeDescription;
	}
	
	/**
     * This method sets the order type description.
     * @param pOrderTypeDescription The order type description of the order.
     */
	public void setMvOrderTypeDescription(String pOrderTypeDescription) {
		this.mvOrderTypeDescription = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till date description.
     * @return the good till date description of the order.
     */
	public String getMvGoodTillDescription() {
		return mvGoodTillDescription;
	}
	
	/**
     * This method sets the good till date description.
     * @param pGoodTillDescription The good till date description of the order.
     */
	public void setMvGoodTillDescription(String pGoodTillDescription) {
		this.mvGoodTillDescription = pGoodTillDescription;
	}
	
	/**
     * This method returns the multi market disable.
     * @return the multi market disable of the order.
     */
	public String getMvMultiMarketDisable() {
		return mvMultiMarketDisable;
	}
	
	/**
     * This method sets the multi market disable.
     * @param pMultiMarketDisable The multi market disable of the order.
     */
	public void setMvMultiMarketDisable(String pMultiMarketDisable) {
		this.mvMultiMarketDisable = pMultiMarketDisable;
	}
}