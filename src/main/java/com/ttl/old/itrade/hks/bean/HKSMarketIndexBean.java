package com.ttl.old.itrade.hks.bean;


public class HKSMarketIndexBean {

	/**
	 * @return the mvMaketID
	 */
	public String getMvMaketID() {
		return mvMaketID;
	}
	/**
	 * @param mvMaketID the mvMaketID to set
	 */
	public void setMvMaketID(String mvMaketID) {
		this.mvMaketID = mvMaketID;
	}
	/**
	 * @return the mvMarketIndex
	 */
	public String getMvMarketIndex() {
		return mvMarketIndex;
	}
	/**
	 * @param mvMarketIndex the mvMarketIndex to set
	 */
	public void setMvMarketIndex(String mvMarketIndex) {
		this.mvMarketIndex = mvMarketIndex;
	}
	/**
	 * @return the mvDifference
	 */
	public String getMvDifference() {
		return mvDifference;
	}
	/**
	 * @param mvDifference the mvDifference to set
	 */
	public void setMvDifference(String mvDifference) {
		this.mvDifference = mvDifference;
	}
	/**
	 * @return the mvPercentage
	 */
	public String getMvPercentage() {
		return mvPercentage;
	}
	/**
	 * @param mvPercentage the mvPercentage to set
	 */
	public void setMvPercentage(String mvPercentage) {
		this.mvPercentage = mvPercentage;
	}
	/**
	 * @return the mvMarketTotalStock
	 */
	public String getMvMarketTotalQty() {
		return mvMarketTotalQty;
	}
	/**
	 * @param mvMarketTotalQty the mvMarketTotalStock to set
	 */
	public void setMvMarketTotalQty(String mvMarketTotalQty) {
		this.mvMarketTotalQty = mvMarketTotalQty;
	}
	/**
	 * @return the mvMarketTotalvalue
	 */
	public String getMvMarketTotalvalue() {
		return mvMarketTotalvalue;
	}
	/**
	 * @param mvMarketTotalvalue the mvMarketTotalvalue to set
	 */
	public void setMvMarketTotalvalue(String mvMarketTotalvalue) {
		this.mvMarketTotalvalue = mvMarketTotalvalue;
	}
	/**
	 * @return the mvMarketAdvances
	 */
	public String getMvMarketAdvances() {
		return mvMarketAdvances;
	}
	/**
	 * @param mvMarketAdvances the mvMarketAdvances to set
	 */
	public void setMvMarketAdvances(String mvMarketAdvances) {
		this.mvMarketAdvances = mvMarketAdvances;
	}
	/**
	 * @return the mvMarketDeclines
	 */
	public String getMvMarketDeclines() {
		return mvMarketDeclines;
	}
	/**
	 * @param mvMarketDeclines the mvMarketDeclines to set
	 */
	public void setMvMarketDeclines(String mvMarketDeclines) {
		this.mvMarketDeclines = mvMarketDeclines;
	}
	/**
	 * @return the mvMarketNoChange
	 */
	public String getMvMarketNoChange() {
		return mvMarketNoChange;
	}
	/**
	 * @param mvMarketNoChange the mvMarketNoChange to set
	 */
	public void setMvMarketNoChange(String mvMarketNoChange) {
		this.mvMarketNoChange = mvMarketNoChange;
	}
	/**
	 * @param mvMarketStatus the mvMarketStatus to set
	 */
	public void setMvMarketStatus(String mvMarketStatus) {
		this.mvMarketStatus = mvMarketStatus;
	}
	/**
	 * @return the mvMarketStatus
	 */
	public String getMvMarketStatus() {
		return mvMarketStatus;
	}
	public void setMvMarketTime(String mvMarketTime) {
		this.mvMarketTime = mvMarketTime;
	}
	public String getMvMarketTime() {
		return mvMarketTime;
	}
	
	public boolean isUpdated() {
		return isUpdated;
	}
	public void setUpdated(boolean isUpdated) {
		this.isUpdated = isUpdated;
	}

	private String mvMaketID;
	private String mvMarketIndex;
	private String mvDifference;
	private String mvPercentage;
	private String mvMarketTotalQty;
	private String mvMarketTotalvalue;
	private String mvMarketAdvances;
	private String mvMarketDeclines;
	private String mvMarketNoChange;
	private String mvMarketStatus;
	private String mvMarketTime;
	private boolean isUpdated;
	
}
