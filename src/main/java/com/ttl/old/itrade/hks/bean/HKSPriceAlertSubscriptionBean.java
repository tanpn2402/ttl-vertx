package com.ttl.old.itrade.hks.bean;

/**
 * The HKSPriceAlertSubscriptionBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSPriceAlertSubscriptionBean {
	private String mvInstrumentID;
	private String mvSign;
	private String mvAlertPrice;
	
	/**
	 * This method returns the instrument id.
	 * @return the instrument id is not formatted.
     */
	public String getMvInstrumentID() {
		return mvInstrumentID;
	}
	
	/**
     * This method returns the instrument id.
     * @param pInstrumentID The instrument id.
     */
	public void setMvInstrumentID(String pInstrumentID) {
		mvInstrumentID = pInstrumentID;
	}
	
	/**
	 * This method sets the Sign.
	 * @return the Sign is not formatted.
     */
	public String getMvSign() {
		return mvSign;
	}
	
	/**
     * This method sets the Sign.
     * @param pSign The Sign.
     */
	public void setMvSign(String pSign) {
		mvSign = pSign;
	}
	
	/**
	 * This method returns the alert price.
	 * @return the alert price is not formatted.
     */
	public String getMvAlertPrice() {
		return mvAlertPrice;
	}
	
	/**
     * This method sets the alert price.
     * @param pAlertPrice The alert price.
     */
	public void setMvAlertPrice(String pAlertPrice) {
		mvAlertPrice = pAlertPrice;
	}
	
}
