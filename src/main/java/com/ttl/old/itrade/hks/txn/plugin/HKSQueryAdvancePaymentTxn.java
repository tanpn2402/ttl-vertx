/**
 * @author: Wind.Zhao
 * @create date : 20091116
 */
//Begin Task #: TTL-GZ-ZZW-00010 Wind Zhao 20091116 for [ITradeR5]Advance payment online
package com.ttl.old.itrade.hks.txn.plugin;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.bean.HKSCashTransactionHistoryBean;
import com.ttl.old.itrade.hks.bean.plugin.HKSAdvancePaymentCreationInforBean;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.hks.util.TextFormatter;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSOnLineAdvancePaymentTxn class definition for all method On-line query advance payment processing
 * 
 * @author Wind.Zhao
 * @since 20091116
 */

// Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module
// Begin Task #: TTL-GZ-ZZW-00010 Wind Zhao 20091116 for [ITradeR5]Advance payment online
public class HKSQueryAdvancePaymentTxn extends BaseTxn {
	private String mvClientID;
	private String mvTradingAccSeq;
	private String mvBankID;
	private String mvSettlement;
	private String mvAmount;

	private int mvStartRecord;
	private int mvEndRecord;
	private String mvTotalRecord;
	private boolean mvIsQueryBank = false;

	private HKSCashTransactionHistoryBean[] cashTransactionList = null;

	/**
	 * Returns the bank id of the advance payment.
	 * 
	 * @return the bank id of the advance payment.
	 */
	public String getMvBankID() {
		return mvBankID;
	}

	/**
	 * Sets the bank id.
	 * 
	 * @param pBankAccount The bank id.
	 */
	public void setMvBankID(String pBankID) {
		mvBankID = pBankID;
	}

	/**
	 * Returns the settlement of the advance payment.
	 * 
	 * @return the settlement of the advance payment.
	 */
	public String getMvSettlement() {
		return mvSettlement;
	}

	/**
	 * Sets the settlement.
	 * 
	 * @param pSettlement The settlement.
	 */
	public void setMvSettlement(String pSettlement) {
		mvSettlement = pSettlement;
	}

	/**
	 * Set method for client id
	 * 
	 * @param pClientID the client id
	 */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}

	/**
	 * Get method for client id
	 * 
	 * @return client id
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * 
	 * @return
	 */
	public String getMvTradingAccSeq() {
		return mvTradingAccSeq;
	}

	/**
	 * 
	 * @param mvTradingAccSeq
	 */
	public void setMvTradingAccSeq(String mvTradingAccSeq) {
		this.mvTradingAccSeq = mvTradingAccSeq;
	}

	public HKSQueryAdvancePaymentTxn(String pClientID) {
		mvClientID = pClientID;
	}

	/**
	 * Constructor for HKSQueryAdvancePaymentTxn class
	 * 
	 * @param [1]pClientID the client id. [2]pBankID the bank id. [3]pSettlement the settlement.
	 * @author Wind.Zhao
	 */
	public HKSQueryAdvancePaymentTxn(String pClientID, String pTradingAccSeq, String pBankID, String pSettlement) {
		mvClientID = pClientID;
		mvTradingAccSeq = pTradingAccSeq;
		mvBankID = pBankID;
		mvSettlement = pSettlement;
	}

	public HKSQueryAdvancePaymentTxn(String pClientID, String pTradingAccSeq, String pBankID, String pSettlement, String pAmount) {
		mvClientID = pClientID;
		mvTradingAccSeq = pTradingAccSeq;
		mvBankID = pBankID;
		mvSettlement = pSettlement;
		mvAmount = pAmount;
	}

	/**
	 * The method for On-line advance processing
	 * 
	 * @return the hash table of all the advance payment's information.
	 * @author Wind.Zhao
	 */
	public List<Hashtable<String, String>> process() {
		Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put(TagName.TRADINGACCSEQ, mvTradingAccSeq);
		lvTxnMap.put("BANKID", mvBankID);
		lvTxnMap.put("TPLUSX", mvSettlement);

		ArrayList<Hashtable<String, String>> lvResultList = new ArrayList<Hashtable<String, String>>();

		// Send TP request
		if (TPErrorHandling.TP_NORMAL == process("QueryAdvancePayment", lvTxnMap)) {
			Hashtable<String, String> lvParentMap = new Hashtable<String, String>();
			IMsgXMLNode parentNode = mvReturnNode.getChildNode("ParentRow");
			lvParentMap.put("CLIENTID", parentNode.getChildNode("CLIENTID").getValue());
			lvParentMap.put("BANKID", parentNode.getChildNode("BANKID").getValue());
			lvParentMap.put("BANKACID", "--");// parentNode.getChildNode("BANKACID").getValue());
			lvParentMap.put("TPLUSX", parentNode.getChildNode("TPLUSX").getValue());
			lvResultList.add(lvParentMap);
			IMsgXMLNodeList lvChildList = mvReturnNode.getNodeList("ChildRow");
			for (int i = 0; i < lvChildList.size(); i++) {
				Hashtable<String, String> lvChildMap = new Hashtable<String, String>();
				IMsgXMLNode lvChildNode = lvChildList.getNode(i);
				lvChildMap.put("ORDERID", lvChildNode.getChildNode("ORDERID").getValue());
				lvChildMap.put("CONTRACTID", lvChildNode.getChildNode("CONTRACTID").getValue());
				lvChildMap.put("STOCKID", lvChildNode.getChildNode("STOCKID").getValue());
				lvChildMap.put("AMOUNT", lvChildNode.getChildNode("AMOUNT").getValue());
				lvChildMap.put("QUANTITY", lvChildNode.getChildNode("QTY").getValue());
				lvResultList.add(lvChildMap);
			}
		}

		return lvResultList;
	}

	/**
	 * Function calculate interest amount
	 * 
	 * @return
	 */
	public String calculateInterestAmt() {
		String lvResult = "";
		try {

			Log.println(
					"HKSQueryAdvancePaymentTxn.calculateInterestAmt START: Client ID = " + mvClientID + " Settle date: " + mvSettlement,
					Log.ACCESS_LOG);

			Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put(TagName.TRADINGACCSEQ, mvTradingAccSeq);
			lvTxnMap.put("SETTLEDATE", mvSettlement);
			lvTxnMap.put(TagName.FEEID, IMain.getProperty("localAPFeeID"));
			lvTxnMap.put(TagName.AMOUNT, mvAmount);

			// Send TP request
			if (TPErrorHandling.TP_NORMAL == process("QueryLocalInterestAmt", lvTxnMap)) {
				lvResult = mvReturnNode.getChildNode(TagName.INTERESTAMOUNT).getValue();
			}

		} catch (Exception e) {
			Log.println("HKSQueryAdvancePaymentTxn.calculateInterestAmt() Exception: " + e.toString(), Log.ERROR_LOG);
		} finally {
			Log.println(
					"HKSQueryAdvancePaymentTxn.calculateInterestAmt() END: Client ID = " + mvClientID + " Settle date: " + mvSettlement,
					Log.ACCESS_LOG);
		}
		return lvResult;
	}

	public String getAdvanceableAmount(String pClientId) {
		Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();
		String result = "";
		try {
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put(TagName.TRADINGACCSEQ, mvTradingAccSeq);
			lvTxnMap.put(TagName.OPERATORID, IMain.getProperty("AgentID"));
			if (TPErrorHandling.TP_NORMAL == process("TTLAdvance", lvTxnMap)) {
				result = mvReturnNode.getChildNode(TagName.AVAILABLE_BALANCE).getValue();
			}
		} catch (Exception e) {
			Log.print(e, Log.ERROR_LOG);
		} finally {
			lvTxnMap.clear();
		}
		return result;
	}

	/**
	 * Function txn to get local advance data from BO
	 * 
	 * @return
	 */
	public List<Hashtable<String, String>> queryLocalAdvancePaymentInfo() {
		ArrayList<Hashtable<String, String>> lvResultList = new ArrayList<Hashtable<String, String>>();
		try {

			Log.println("HKSQueryAdvancePaymentTxn.queryLocalAdvancePaymentInfo() START: Client ID = " + mvClientID + ". Bank ID = "
					+ mvBankID + " Settle date: " + mvSettlement, Log.ACCESS_LOG);

			Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put(TagName.TRADINGACCSEQ, mvTradingAccSeq);
			lvTxnMap.put("SETTLEDATE", mvSettlement);
			lvTxnMap.put(TagName.FEEID, IMain.getProperty("localAPFeeID"));

			// Send TP request
			if (TPErrorHandling.TP_NORMAL == process("QueryLocalAdvancePaymentInfo", lvTxnMap)) {
				Hashtable<String, String> lvParentMap = new Hashtable<String, String>();
				IMsgXMLNode parentNode = mvReturnNode.getChildNode("ParentRow");
				lvParentMap.put(TagName.CLIENTID, parentNode.getChildNode(TagName.CLIENTID).getValue());
				lvParentMap.put(TagName.LENDINGAMOUNT, parentNode.getChildNode(TagName.LENDINGAMOUNT).getValue());
				lvResultList.add(lvParentMap);
				IMsgXMLNodeList lvChildList = mvReturnNode.getNodeList("ChildRow");
				for (int i = 0; i < lvChildList.size(); i++) {

					Hashtable<String, String> lvChildMap = new Hashtable<String, String>();
					IMsgXMLNode lvChildNode = lvChildList.getNode(i);
					lvChildMap.put(TagName.ORDERID, lvChildNode.getChildNode(TagName.ORDERID).getValue());
					lvChildMap.put(TagName.CONTRACTID, lvChildNode.getChildNode(TagName.CONTRACTID).getValue());
					lvChildMap.put(TagName.TRANDATE, lvChildNode.getChildNode(TagName.TRANDATE).getValue());
					lvChildMap.put(TagName.TRADEDATE, lvChildNode.getChildNode(TagName.TRADEDATE).getValue());
					lvChildMap.put(TagName.CASHSETTLEDATE, lvChildNode.getChildNode(TagName.CASHSETTLEDATE).getValue());
					lvChildMap.put(TagName.MARKETID, lvChildNode.getChildNode(TagName.MARKETID).getValue());
					lvChildMap.put(TagName.INSTRUMENTID, lvChildNode.getChildNode(TagName.INSTRUMENTID).getValue());
					lvChildMap.put(TagName.PRICE, lvChildNode.getChildNode(TagName.PRICE).getValue());
					lvChildMap.put(TagName.QTY, lvChildNode.getChildNode(TagName.QTY).getValue());
					lvChildMap.put(TagName.NETAMOUNT, lvChildNode.getChildNode(TagName.NETAMOUNT).getValue());
					lvChildMap.put(TagName.TRADINGFEE, lvChildNode.getChildNode(TagName.TRADINGFEE).getValue());

					lvResultList.add(lvChildMap);
				}
			}

		} catch (Exception e) {
			Log.println("HKSQueryAdvancePaymentTxn.queryLocalAdvancePaymentInfo() Exception: " + e.toString(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSQueryAdvancePaymentTxn.queryLocalAdvancePaymentInfo() END: Client ID = " + mvClientID + ". Bank ID = "
					+ mvBankID + " Settle date: " + mvSettlement, Log.ACCESS_LOG);
		}

		return lvResultList;
	}

	/**
	 * Function txn to get local advance data from BO
	 * 
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List querySoldOrders() {
		ArrayList lvResultList = new ArrayList();
		try {

			Log.println("HKSQueryAdvancePaymentTxn.querySoldOrders() START: Client ID = " + mvClientID + " Settle date: " + mvSettlement,
					Log.ACCESS_LOG);

			Hashtable lvTxnMap = new Hashtable();
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put("SETTLEDATE", mvSettlement);

			// Send TP request
			if (TPErrorHandling.TP_NORMAL == process("QuerySoldOrderMatchedList", lvTxnMap)) {
				IMsgXMLNodeList lvChildList = mvReturnNode.getNodeList("ChildRow");
				for (int i = 0; i < lvChildList.size(); i++) {

					Hashtable lvChildMap = new Hashtable();
					IMsgXMLNode lvChildNode = lvChildList.getNode(i);
					lvChildMap.put(TagName.ORDERID, lvChildNode.getChildNode(TagName.ORDERID).getValue());
					lvChildMap.put(TagName.CONTRACTID, lvChildNode.getChildNode(TagName.CONTRACTID).getValue());
					lvChildMap.put(TagName.TRANDATE, lvChildNode.getChildNode(TagName.TRANDATE).getValue());
					lvChildMap.put(TagName.TRADEDATE, lvChildNode.getChildNode(TagName.TRADEDATE).getValue());
					lvChildMap.put(TagName.CASHSETTLEDATE, lvChildNode.getChildNode(TagName.CASHSETTLEDATE).getValue());
					lvChildMap.put(TagName.MARKETID, lvChildNode.getChildNode(TagName.MARKETID).getValue());
					lvChildMap.put(TagName.INSTRUMENTID, lvChildNode.getChildNode(TagName.INSTRUMENTID).getValue());
					lvChildMap.put(TagName.PRICE, lvChildNode.getChildNode(TagName.PRICE).getValue());
					lvChildMap.put(TagName.QTY, lvChildNode.getChildNode(TagName.QTY).getValue());
					lvChildMap.put(TagName.NETAMOUNT, lvChildNode.getChildNode(TagName.NETAMOUNT).getValue());
					lvChildMap.put(TagName.TRADINGFEE, lvChildNode.getChildNode(TagName.TRADINGFEE).getValue());

					lvResultList.add(lvChildMap);
				}
			}

		} catch (Exception e) {
			Log.println("HKSQueryAdvancePaymentTxn.querySoldOrders() Exception: " + e.toString(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSQueryAdvancePaymentTxn.querySoldOrders() END: Client ID = " + mvClientID + " Settle date: " + mvSettlement,
					Log.ACCESS_LOG);
		}

		return lvResultList;
	}

	/**
	 * Function txn to get local advance data from BO
	 * 
	 * @return
	 */
	public List<Hashtable<String, String>> queryBankAdvancePaymentInfo() {
		ArrayList<Hashtable<String, String>> lvResultList = new ArrayList<Hashtable<String, String>>();
		try {

			Log.println("HKSQueryAdvancePaymentTxn.queryBankAdvancePaymentInfo() START: Client ID = " + mvClientID + ". Bank ID = "
					+ mvBankID + " Settle date: " + mvSettlement, Log.ACCESS_LOG);
			Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put(TagName.TRADINGACCSEQ, mvTradingAccSeq);
			lvTxnMap.put("BANKID", mvBankID);
			lvTxnMap.put("TPLUSX", mvSettlement);
			lvTxnMap.put(TagName.FEEID, IMain.getProperty("bankAPFeeID"));

			// Send TP request
			if (TPErrorHandling.TP_NORMAL == process("QueryAdvancePayment", lvTxnMap)) {
				Hashtable<String, String> lvParentMap = new Hashtable<String, String>();
				IMsgXMLNode parentNode = mvReturnNode.getChildNode("ParentRow");
				lvParentMap.put(TagName.CLIENTID, parentNode.getChildNode(TagName.CLIENTID).getValue());
				lvParentMap.put(TagName.LENDINGAMOUNT, parentNode.getChildNode(TagName.LENDINGAMOUNT).getValue());
				lvParentMap.put("BANKID", parentNode.getChildNode("BANKID").getValue());
				lvParentMap.put("BANKACID", parentNode.getChildNode("BANKACID").getValue());
				lvParentMap.put("TPLUSX", parentNode.getChildNode("TPLUSX").getValue());

				lvResultList.add(lvParentMap);
				IMsgXMLNodeList lvChildList = mvReturnNode.getNodeList("ChildRow");
				for (int i = 0; i < lvChildList.size(); i++) {

					Hashtable<String, String> lvChildMap = new Hashtable<String, String>();
					IMsgXMLNode lvChildNode = lvChildList.getNode(i);
					lvChildMap.put(TagName.ORDERID, lvChildNode.getChildNode(TagName.ORDERID).getValue());
					lvChildMap.put(TagName.CONTRACTID, lvChildNode.getChildNode(TagName.CONTRACTID).getValue());
					lvChildMap.put(TagName.TRADEDATE, lvChildNode.getChildNode(TagName.TRADEDATE).getValue());
					lvChildMap.put(TagName.SETTLEDATE, lvChildNode.getChildNode(TagName.SETTLEDATE).getValue());
					lvChildMap.put(TagName.STOCKID, lvChildNode.getChildNode(TagName.STOCKID).getValue());
					lvChildMap.put(TagName.PRICE, lvChildNode.getChildNode(TagName.PRICE).getValue());
					lvChildMap.put(TagName.QTY, lvChildNode.getChildNode(TagName.QTY).getValue());
					lvChildMap.put(TagName.NETAMOUNT, lvChildNode.getChildNode(TagName.AMOUNT).getValue());
					lvChildMap.put("AVAILABLEAMOUNT", lvChildNode.getChildNode("AVAILABLEAMOUNT").getValue());
					lvChildMap.put("TPLUSX", lvChildNode.getChildNode("TPLUSX").getValue());
					
					lvResultList.add(lvChildMap);
				}
			}

		} catch (Exception e) {
			Log.println("HKSQueryAdvancePaymentTxn.queryBankAdvancePaymentInfo() Exception: " + e.toString(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSQueryAdvancePaymentTxn.queryBankAdvancePaymentInfo() END: Client ID = " + mvClientID + ". Bank ID = "
					+ mvBankID + " Settle date: " + mvSettlement, Log.ACCESS_LOG);
		}

		return lvResultList;
	}

	/**
	 * getCashAdvanceHistory
	 * DuongHN 22Dec2014 - Add isQueryBank for bank request 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getCashAdvanceHistory() {
		try {

			Log.println("HKSQueryAdvancePaymentTxn.getCashAdvanceHistory() START: Client ID = " + mvClientID, Log.ACCESS_LOG);

			Hashtable lvTxnMap = new Hashtable();

			lvTxnMap.put("CLIENTID", mvClientID);
			lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
			lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));
			lvTxnMap.put("ISQUERYBANK", String.valueOf(mvIsQueryBank));

			// Send TP request
			if (TPErrorHandling.TP_NORMAL == process("QueryAdvancePaymentHistory", lvTxnMap)) {

				if (mvReturnNode.getChildNode("TOTALRECORD") != null) {
					setMvTotalRecord(mvReturnNode.getChildNode("TOTALRECORD").getValue());
				}

				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList("ChildRow");
				setCashTransactionList(new HKSCashTransactionHistoryBean[lvRowList.size()]);
				for (int i = 0; i < lvRowList.size(); i++) {
					getCashTransactionList()[i] = new HKSCashTransactionHistoryBean();

					IMsgXMLNode lvRow = lvRowList.getNode(i);

					// get status
					getCashTransactionList()[i].setStatus(lvRow.getChildNode("STATUS").getValue());

					// get creation time
					String createTime = lvRow.getChildNode("CREATIONTIME").getValue();
					getCashTransactionList()[i].setCreationTime(TextFormatter.getFormattedTime_IWS(createTime,
							IMain.getProperty("dateTimeFormat")));

					// set create time to order in GUI
					getCashTransactionList()[i].setCreateTime(createTime);

					// get amount
					getCashTransactionList()[i].setTotalLendingAmt(lvRow.getChildNode("TOTALLENDINGAMT").getValue());
					getCashTransactionList()[i].setInterestAccured(lvRow.getChildNode("INTERESTACCURED").getValue());

					String approvedTime = lvRow.getChildNode("LASTAPPROVALTIME").getValue();
					getCashTransactionList()[i].setTranID(lvRow.getChildNode("APPLICATIONID").getValue());
					getCashTransactionList()[i].setLastApprovaltime(TextFormatter.getFormattedTime_IWS(approvedTime,
							IMain.getProperty("dateTimeFormat")));
				}
			}

		} catch (Exception e) {
			Log.println("HKSQueryAdvancePaymentTxn.getCashAdvanceHistory() Exception: " + e.toString(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSQueryAdvancePaymentTxn.getCashAdvanceHistory() END: Client ID = " + mvClientID, Log.ACCESS_LOG);
		}
	}

	public void setMvAmount(String mvAmount) {
		this.mvAmount = mvAmount;
	}

	public String getMvAmount() {
		return mvAmount;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HKSAdvancePaymentCreationInforBean getLocalAdvanceCreation() {
		HKSAdvancePaymentCreationInforBean bean = new HKSAdvancePaymentCreationInforBean();
		try {

			Log.println("HKSQueryAdvancePaymentTxn.getLocalAdvanceCreation() START: Client ID = " + mvClientID, Log.ACCESS_LOG);
			Hashtable lvTxnMap = new Hashtable();
			lvTxnMap.put(TagName.CLIENTID, mvClientID);

			// Send TP request
			if (TPErrorHandling.TP_NORMAL == process("QueryAdvancePaymentCreationInfo", lvTxnMap)) {

				bean.setAdvAvailable(mvReturnNode.getChildNode("AVAILABLEADVANCEMONEY").getValue());
				bean.setAdvPending(mvReturnNode.getChildNode("PENDINGADV").getValue());
				// HIEU LE: get temporary rate to hold fee
				// bean.setInterestRate(mvReturnNode.getChildNode("INTERESTRATE").getValue());
				// bean.setMinInterestRate(mvReturnNode.getChildNode("MININTERESTACCURED").getValue());
				bean.setInterestRate(mvReturnNode.getChildNode("DEFAULTRATE").getValue());
				bean.setMinInterestRate(mvReturnNode.getChildNode("MININTEREST").getValue());
				// END HIEU LE
				bean.setT0AdvAvailable(mvReturnNode.getChildNode("CTODAYADVANCEAVAILABLE").getValue());
				bean.setT0Days(mvReturnNode.getChildNode("T0DAYS").getValue());
				bean.setT1AdvAvailable(mvReturnNode.getChildNode("CT1ADVANCEAVAILABLE").getValue());
				bean.setT1Days(mvReturnNode.getChildNode("T1DAYS").getValue());
				bean.setT2AdvAvailable(mvReturnNode.getChildNode("CT2ADVANCEAVAILABLE").getValue());
				bean.setT2Days(mvReturnNode.getChildNode("T2DAYS").getValue());
				bean.setAdvFee(mvReturnNode.getChildNode("ADVFEE").getValue());
			}

		} catch (Exception e) {
			Log.println("HKSQueryAdvancePaymentTxn.getLocalAdvanceCreation() Exception: " + e.toString() + " for clientid: " + mvClientID,
					Log.ERROR_LOG);
		} finally {
			Log.println("HKSQueryAdvancePaymentTxn.getLocalAdvanceCreation() END: Client ID = " + mvClientID, Log.ACCESS_LOG);
		}

		return bean;
	}

	/**
	 * @param mvStartRecord the mvStartRecord to set
	 */
	public void setMvStartRecord(int mvStartRecord) {
		this.mvStartRecord = mvStartRecord;
	}

	/**
	 * @return the mvStartRecord
	 */
	public int getMvStartRecord() {
		return mvStartRecord;
	}

	/**
	 * @param mvEndRecord the mvEndRecord to set
	 */
	public void setMvEndRecord(int mvEndRecord) {
		this.mvEndRecord = mvEndRecord;
	}

	/**
	 * @return the mvEndRecord
	 */
	public int getMvEndRecord() {
		return mvEndRecord;
	}

	/**
	 * @return the mvIsQueryBank
	 */
	public boolean isMvIsQueryBank() {
		return mvIsQueryBank;
	}

	/**
	 * @param mvIsQueryBank the mvIsQueryBank to set
	 */
	public void setMvIsQueryBank(boolean mvIsQueryBank) {
		this.mvIsQueryBank = mvIsQueryBank;
	}

	/**
	 * @param mvTotalRecord the mvTotalRecord to set
	 */
	public void setMvTotalRecord(String mvTotalRecord) {
		this.mvTotalRecord = mvTotalRecord;
	}

	/**
	 * @return the mvTotalRecord
	 */
	public String getMvTotalRecord() {
		return mvTotalRecord;
	}

	/**
	 * @param cashTransactionList the cashTransactionList to set
	 */
	public void setCashTransactionList(HKSCashTransactionHistoryBean[] cashTransactionList) {
		this.cashTransactionList = cashTransactionList;
	}

	/**
	 * @return the cashTransactionList
	 */
	public HKSCashTransactionHistoryBean[] getCashTransactionList() {
		return cashTransactionList;
	}
}
// End Task #: TTL-GZ-ZZW-00010 Wind Zhao 20091116 for [ITradeR5]Advance payment online
// End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module