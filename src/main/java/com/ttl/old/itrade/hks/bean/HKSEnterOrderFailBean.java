package com.ttl.old.itrade.hks.bean;

/**
 * The HKSEnterOrderFailBean class define variables that to save values 
 * for action 
 * @author Wind zhao
 *
 */
public class HKSEnterOrderFailBean {
	private String mvTransType;
	private String mvBS;
	private String mvBuyOrSell;
	private String mvClientId;
	private String mvDateTime;
	private String mvMarketId;
	private String mvInstrumentId;
	private String mvInstrumentIdValue;
	private String mvInstrumentName;
	private String mvInstrumentNameValue;
	private String mvError_Type;
	private String mvPrice;
	private String mvPriceNumber;
	private String mvQuantityNumber;
	private String mvQuantity;
	private String mvOrderType;
	private String mvLastOrderType;
	private String mvStopPriceValue;
	private String mvStopTypeValue;
	private String mvFormAtted_StopPriceValue;
	private String mvOriginalNetAmount;
	private String mvCurrencyId;
	private String mvGoodTillDate;
	private String mvErrorMsg;
	private String mvPasswordIncorrect;
	private String mvQuantityDescription;
	private String mvOrderTypeDescription;
	private String mvGoodTillDescription;
	private String mvAALoginURL;
	private String mvPriceValue;
	private String mvQuantityValue;
	private String mvGoodTillDateValue;
	private String mvGrossAmt;
	private String mvFeesCommission;
	private String mvTotalCost;
	
	/**
     * This method returns the type of transaction.
     * @return the type of transaction.
     */
	public String getMvTransType() {
		return mvTransType;
	}
	
	/**
     * This method sets the type of transaction.
     * @param pTransType The type of transaction.
     */
	public void setMvTransType(String pTransType) {
		mvTransType = pTransType;
	}
	
	/**
     * This method returns the B(Buy) or S(Sell) of order.
     * @return the B(Buy) or S(Sell)  of order.
     *         [0] B is Buy.
     *         [1] S is Sell.
     */
	public String getMvBS() {
		return mvBS;
	}
	
	/**
     * This method sets the B(Buy) or S(Sell) of order.
     * @param pBS The B(Buy) or S(Sell) of order.
     */
	public void setMvBS(String pBS) {
		mvBS = pBS;
	}
	
	/**
     * This method returns the Buy or Sell of order.
     * @return the Buy or Sell  of order.
     *         [0] Buy.
     *         [1] Sell.
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the Buy or Sell of order.
     * @param pBuyOrSell The Buy or Sell of order.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the client id.
     * @return the client id of order.
     */
	public String getMvClientId() {
		return mvClientId;
	}
	
	/**
     * This method sets the client id.
     * @param pClientId The client id of order.
     */
	public void setMvClientId(String pClientId) {
		mvClientId = pClientId;
	}
	
	/**
     * This method returns the date time of order.
     * @return the date time id of order.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time of order.
     * @param pDateTime The date time of order.
     */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}
	
	/**
     * This method returns the market id of order.
     * @return the market id of order.
     */
	public String getMvMarketId() {
		return mvMarketId;
	}
	
	/**
     * This method sets the market id of order.
     * @param pMarketId The market id of order.
     */
	public void setMvMarketId(String pMarketId) {
		mvMarketId = pMarketId;
	}
	
	/**
     * This method returns the instrument id of order.
     * @return the instrument id of order.
     */
	public String getMvInstrumentIdValue() {
		return mvInstrumentIdValue;
	}
	
	/**
     * This method sets the instrument id of order.
     * @param pInstrumentIdValue The instrument id of order.
     */
	public void setMvInstrumentIdValue(String pInstrumentIdValue) {
		mvInstrumentIdValue = pInstrumentIdValue;
	}
	
	/**
     * This method returns the instrument name of order.
     * @return the instrument name of order.
     */
	public String getMvInstrumentNameValue() {
		return mvInstrumentNameValue;
	}
	
	/**
     * This method sets the instrument name of order.
     * @param pInstrumentNameValue The instrument name of order.
     */
	public void setMvInstrumentNameValue(String pInstrumentNameValue) {
		mvInstrumentNameValue = pInstrumentNameValue;
	}
	
	/**
     * This method returns the instrument id of order.
     * @return the instrument id of order.
     */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}
	
	/**
     * This method sets the instrument id of order.
     * @param pInstrumentId The instrument id of order.
     */
	public void setMvInstrumentId(String pInstrumentId) {
		mvInstrumentId = pInstrumentId;
	}
	
	/**
     * This method returns the instrument name of order.
     * @return the instrument name of order.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name of order.
     * @param pInstrumentName The instrument name of order.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the error type of order.
     * @return the error type of order.
     */
	public String getMvError_Type() {
		return mvError_Type;
	}
	
	/**
     * This method sets the error type of order.
     * @param pError_Type The error type of order.
     */
	public void setMvError_Type(String pError_Type) {
		mvError_Type = pError_Type;
	}
	
	/**
     * This method returns the price of order.
     * @return the price of order is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price of order.
     * @param pPrice The price of order is formatted.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the price of order.
     * @return the price of order is not formatted.
     */
	public String getMvPriceNumber() {
		return mvPriceNumber;
	}
	
	/**
     * This method sets the price of order.
     * @param pPriceNumber The price of order is not formatted.
     */
	public void setMvPriceNumber(String pPriceNumber) {
		mvPriceNumber = pPriceNumber;
	}
	
	/**
     * This method returns the quantity of order.
     * @return the quantity of order is not formatted.
     */
	public String getMvQuantityNumber() {
		return mvQuantityNumber;
	}
	
	/**
     * This method sets the quantity of order.
     * @param pQuantityNumber The quantity of order is not formatted.
     */
	public void setMvQuantityNumber(String pQuantityNumber) {
		mvQuantityNumber = pQuantityNumber;
	}
	
	/**
     * This method returns the quantity of order.
     * @return the quantity of order is formatted.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity of order.
     * @param pQuantity The quantity of order is formatted.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the type of order.
     * @return the type of order.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the type of order.
     * @param pOrderType The type of order is formatted.
     */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the last type of order.
     * @return the last type of order.
     */
	public String getMvLastOrderType() {
		return mvLastOrderType;
	}
	
	/**
     * This method sets the last type of order.
     * @param pLastOrderType The last type of order is formatted.
     */
	public void setMvLastOrderType(String pLastOrderType) {
		mvLastOrderType = pLastOrderType;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order.
     */
	public String getMvStopPriceValue() {
		return mvStopPriceValue;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pStopPriceValue The stop price of order.
     */
	public void setMvStopPriceValue(String pStopPriceValue) {
		mvStopPriceValue = pStopPriceValue;
	}
	
	/**
     * This method returns the stop type of order.
     * @return the stop type of order.
     */
	public String getMvStopTypeValue() {
		return mvStopTypeValue;
	}
	
	/**
     * This method sets the stop type of order.
     * @param pStopTypeValue The stop type of order.
     */
	public void setMvStopTypeValue(String pStopTypeValue) {
		mvStopTypeValue = pStopTypeValue;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order is formatted with currency id.
     */
	public String getMvFormAtted_StopPriceValue() {
		return mvFormAtted_StopPriceValue;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pFormAtted_StopPriceValue The stop price of order is formatted with currency id.
     */
	public void setMvFormAtted_StopPriceValue(String pFormAtted_StopPriceValue) {
		mvFormAtted_StopPriceValue = pFormAtted_StopPriceValue;
	}
	
	/**
     * This method returns the original net amount of order.
     * @return the original net amount of order is formatted.
     */
	public String getMvOriginalNetAmount() {
		return mvOriginalNetAmount;
	}
	
	/**
     * This method sets the original net amount of order.
     * @param pOriginalNetAmount The original net amount of order is formatted.
     */
	public void setMvOriginalNetAmount(String pOriginalNetAmount) {
		mvOriginalNetAmount = pOriginalNetAmount;
	}
	
	/**
     * This method returns the currency id of instrument.
     * @return currency id of instrument.
     */
	public String getMvCurrencyId() {
		return mvCurrencyId;
	}
	
	/**
     * This method sets the currency id of instrument.
     * @param pCurrencyId The currency id of instrument.
     */
	public void setMvCurrencyId(String pCurrencyId) {
		mvCurrencyId = pCurrencyId;
	}
	
	/**
     * This method returns the good till date of order.
     * @return the good till date of order is formatted.
     */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	
	/**
     * This method sets the good till date of order.
     * @param pGoodTillDate The good till date of order is formatted.
     */
	public void setMvGoodTillDate(String pGoodTillDate) {
		mvGoodTillDate = pGoodTillDate;
	}
	
	/**
     * This method returns the error message of order.
     * @return the error message of order.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message of order.
     * @param pErrorMsg The error message date of order.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	
	/**
     * This method returns the password if correct when enter order.
     * @return the password if correct.
     */
	public String getMvPasswordIncorrect() {
		return mvPasswordIncorrect;
	}
	
	/**
     * This method sets the password if correct when enter order.
     * @param pPasswordIncorrect The password if correct.
     */
	public void setMvPasswordIncorrect(String pPasswordIncorrect) {
		mvPasswordIncorrect = pPasswordIncorrect;
	}
	
	/**
     * This method returns the quantity description of order.
     * @return the quantity description of order.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description of order.
     * @param pQuantityDescription The quantity description of order.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the type description of order.
     * @return the type description of order.
     */
	public String getMvOrderTypeDescription() {
		return mvOrderTypeDescription;
	}
	
	/**
     * This method sets the type description of order.
     * @param pOrderTypeDescription The type description of order.
     */
	public void setMvOrderTypeDescription(String pOrderTypeDescription) {
		mvOrderTypeDescription = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till date description of order.
     * @return the good till date description of order.
     */
	public String getMvGoodTillDescription() {
		return mvGoodTillDescription;
	}
	
	/**
     * This method sets the good till date description of order.
     * @param pGoodTillDescription The good till date description of order.
     */
	public void setMvGoodTillDescription(String pGoodTillDescription) {
		mvGoodTillDescription = pGoodTillDescription;
	}
	
	/**
     * This method returns the aastock login url.
     * @return the aastock login url .
     */
	public String getMvAALoginURL() {
		return mvAALoginURL;
	}
	
	/**
     * This method sets the aastock login url.
     * @param pAALoginURL The aastock login url.
     */
	public void setMvAALoginURL(String pAALoginURL) {
		mvAALoginURL = pAALoginURL;
	}
	
	/**
     * This method returns the price of order.
     * @return the price of order.
     */
	public String getMvPriceValue() {
		return mvPriceValue;
	}
	
	/**
     * This method sets the price of order.
     * @param pPriceValue The price of order.
     */
	public void setMvPriceValue(String pPriceValue) {
		mvPriceValue = pPriceValue;
	}
	
	/**
     * This method returns the quantity of order.
     * @return the quantity of order.
     */
	public String getMvQuantityValue() {
		return mvQuantityValue;
	}
	
	/**
     * This method sets the quantity of order.
     * @param pQuantityValue The quantity of order.
     */
	public void setMvQuantityValue(String pQuantityValue) {
		mvQuantityValue = pQuantityValue;
	}
	
	/**
     * This method returns the good till date of order.
     * @return the good till date of order.
     */
	public String getMvGoodTillDateValue() {
		return mvGoodTillDateValue;
	}
	
	/**
     * This method sets the good till date of order.
     * @param pGoodTillDateValue The good till date of order.
     */
	public void setMvGoodTillDateValue(String pGoodTillDateValue) {
		mvGoodTillDateValue = pGoodTillDateValue;
	}
	
	/**
     * This method returns the gross amount of order.
     * @return the gross amount of order is formatted.
     */
	public String getMvGrossAmt() {
		return mvGrossAmt;
	}
	
	/**
     * This method sets the gross amount of order.
     * @param pGrossAmt The gross amount of order is formatted.
     */
	public void setMvGrossAmt(String pGrossAmt) {
		mvGrossAmt = pGrossAmt;
	}
	
	/**
     * This method returns the fees commission of order.
     * @return the fees commission of order is formatted.
     */
	public String getMvFeesCommission() {
		return mvFeesCommission;
	}
	
	/**
     * This method sets the fees commission of order.
     * @param pFeesCommission The fees commission of order is formatted.
     */
	public void setMvFeesCommission(String pFeesCommission) {
		mvFeesCommission = pFeesCommission;
	}
	
	/**
     * This method returns the total cost of order.
     * @return the total cost of order is formatted.
     */
	public String getMvTotalCost() {
		return mvTotalCost;
	}
	
	/**
     * This method sets the total cost of order.
     * @param pTotalCost The total cost of order is formatted.
     */
	public void setMvTotalCost(String pTotalCost) {
		mvTotalCost = pTotalCost;
	}
}
