package com.ttl.old.itrade.hks.bean.plugin;
/**
 * The HKSIPOStatusEnquiryBean class define variables that to save values 
 * for action
 * @author shaojian wan
 * @since 2010-02-10
 */
public class HKSIPOStatusEnquiryBean {
	private String mvRefNo;
	private boolean mvIsAllotmentResultOn;
	private boolean mvIsApplicationStatusReady;
	private String mvAllotmentResult;
	private String mvInstrumentID;
	private String mvStockName;
	private String mvAppliedQty;
	private String mvOfferPrice;
	private String mvApplDate;
	private String mvApplStatus;
	private boolean mvOnclick;
	
	/**
     * This method returns the ref number.
     * @return the ref number.
     */
	public String getMvRefNo() {
		return mvRefNo;
	}
	
	/**
     * This method sets the ref number.
     * @param pRefNo The ref number.
     */
	public void setMvRefNo(String pRefNo) {
		mvRefNo = pRefNo;
	}
	
	/**
     * This method returns the allotment result is on.
     * @return the allotment result is on.
     */
	public boolean isMvIsAllotmentResultOn() {
		return mvIsAllotmentResultOn;
	}
	
	/**
     * This method sets the allotment result is on.
     * @param pIsAllotmentResultOn The allotment result is on.
     */
	public void setMvIsAllotmentResultOn(boolean pIsAllotmentResultOn) {
		mvIsAllotmentResultOn = pIsAllotmentResultOn;
	}
	
	/**
     * This method returns the application status is ready.
     * @return the application status is ready.
     */
	public boolean isMvIsApplicationStatusReady() {
		return mvIsApplicationStatusReady;
	}
	
	/**
     * This method sets the application status is ready.
     * @param pIsApplicationStatusReady The application status is ready.
     */
	public void setMvIsApplicationStatusReady(boolean pIsApplicationStatusReady) {
		mvIsApplicationStatusReady = pIsApplicationStatusReady;
	}
	
	/**
     * This method returns the allotment result.
     * @return the allotment result.
     */
	public String getMvAllotmentResult() {
		return mvAllotmentResult;
	}
	
	/**
     * This method sets the allotment result.
     * @param pAllotmentResult The allotment result.
     */
	public void setMvAllotmentResult(String pAllotmentResult) {
		mvAllotmentResult = pAllotmentResult;
	}
	
	/*public String getMvNoAllotmentResult() {
		return mvNoAllotmentResult;
	}
	public void setMvNoAllotmentResult(String mvNoAllotmentResult) {
		mvNoAllotmentResult = mvNoAllotmentResult;
	}*/
	
	/**
     * This method returns the instrument id.
     * @return the instrument id.
     */
	public String getMvInstrumentID() {
		return mvInstrumentID;
	}
	
	/**
     * This method sets the instrument id.
     * @param pInstrumentID The instrument id.
     */
	public void setMvInstrumentID(String pInstrumentID) {
		mvInstrumentID = pInstrumentID;
	}
	
	/**
     * This method returns the stock name.
     * @return the stock name.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the stock name.
     * @param pStockName The stock name.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the applied quantity.
     * @return the applied quantity is formatted.
     */
	public String getMvAppliedQty() {
		return mvAppliedQty;
	}
	
	/**
     * This method sets the applied quantity.
     * @param pAppliedQty The applied quantity is formatted.
     */
	public void setMvAppliedQty(String pAppliedQty) {
		mvAppliedQty = pAppliedQty;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price is formatted.
     */
	public String getMvOfferPrice() {
		return mvOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param pOfferPrice The offer price is formatted.
     */
	public void setMvOfferPrice(String pOfferPrice) {
		mvOfferPrice = pOfferPrice;
	}
	
	/**
     * This method returns the apply date.
     * @return the apply date.
     */
	public String getMvApplDate() {
		return mvApplDate;
	}
	
	/**
     * This method sets the apply date.
     * @param pApplDate The apply date.
     */
	public void setMvApplDate(String pApplDate) {
		mvApplDate = pApplDate;
	}
	
	/**
     * This method returns the apply status.
     * @return the apply status.
     */
	public String getMvApplStatus() {
		return mvApplStatus;
	}
	
	/**
     * This method sets the apply status.
     * @param pApplStatus The apply status.
     */
	public void setMvApplStatus(String pApplStatus) {
		mvApplStatus = pApplStatus;
	}
	
	/**
     * This method sets the button if click.
     * @param pOnclick The button if click.
     */
	public boolean isMvOnclick() {
		return mvOnclick;
	}
		
	/**
	* This method sets the button if click.
	* @param pOnclick The button if click.
	*/
	public void setMvOnclick(boolean pOnclick) {
		mvOnclick = pOnclick;
	}
	
}
