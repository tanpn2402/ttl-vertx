package com.ttl.old.itrade.hks.txn;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.server.model.order.OrderSModel;

/**
 * The HKSOrderHistoryEnquiryNewTxn class definition for all method
 * stores the order History enquiry details
 * @author not attributable
 *
 */
public class HKSOrderHistoryEnquiryNewTxn extends BaseTxn
{
   private String mvClientID;
   private String mvInstrumentID;
   private String mvBS;
   private String mvFromTime;
   private String mvToTime;
   
   private int mvStartRecord;
   private int mvEndRecord; 
   
   private int mvTotalOrders;
   
   private String mvSorting;
   private String mvStatus;
   private String mvStatusFilter;
   private String mvLoopCounter;
   private boolean mvIsExportData;
   private Vector<OrderSModel> lvModelList;
 

   /**
    * Constructor for HKSOrderHistoryEnquiryTxn class
    * @param pClientID the client id
    * @param pInstrumentID the instrument id
    * @param pBS the buy or sell
    * @param pFromTime the from time
    * @param pToTime the to time
    * @param mvStartRecord the from page
    * @param mvEndRecord the to page    
    * @param mvIsExportData the from export 
    * @param pSorting the sorting
    * @param pStatusFilter the status filter
    */
   public HKSOrderHistoryEnquiryNewTxn(String pClientID, String pInstrumentID, String pBS, String pFromTime, String pToTime
		   , int mvStartRecord, int mvEndRecord
		   , boolean mvIsExportData, String pSorting, String pStatusFilter)
   {
      super();
      mvClientID = pClientID;
      mvInstrumentID = pInstrumentID;
      mvBS = pBS;
      mvFromTime = pFromTime;
      mvToTime = pToTime;
      this.mvStartRecord	= mvStartRecord;
      this.mvEndRecord   	= mvEndRecord;
      mvSorting = pSorting;
      mvStatusFilter = pStatusFilter;
      this.mvIsExportData=mvIsExportData;
   }
   /**
    * The method process stores the order History enquiry details
    * @return Order History Collection 
    */
   public void process() {
      Hashtable<String,String> lvTxnMap = new Hashtable<String,String>();
      lvTxnMap.put(TagName.CLIENTID, mvClientID);
      if(null == mvInstrumentID || mvInstrumentID.isEmpty()  || "ALL".equalsIgnoreCase(mvInstrumentID)){
    	  mvInstrumentID="";
      }
      lvTxnMap.put(TagName.INSTRUCTIONID, mvInstrumentID);     
      lvTxnMap.put(TagName.BS, mvBS);
      lvTxnMap.put(TagName.FROMTIME, mvFromTime);
      lvTxnMap.put(TagName.TOTIME, mvToTime);
      lvTxnMap.put(TagName.SORTING, mvSorting);
      lvTxnMap.put("STATUSFILTER", mvStatusFilter);      
      lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
      lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));
      
	  
      if (mvIsExportData) {
    	  lvTxnMap.put("EXPORTDATA", "Y");
      }else{
    	  lvTxnMap.put("EXPORTDATA", "N");
      }
      
  	  lvModelList = new Vector<OrderSModel>();
  	  int status	= process(RequestName.OrderHistoryEnquiry, lvTxnMap);
  	 
      if (TPErrorHandling.TP_NORMAL == status)
      {
          IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
          mvTotalOrders = Integer.parseInt( mvReturnNode.getChildNode("TOTALRECORD").getValue());
          if(mvTotalOrders > 0){
        	  for (int i = 0; i < lvRowList.size(); i++)
              {
               	 IMsgXMLNode lvRow=lvRowList.getNode(i);                                       
                  OrderSModel lvModel =parserBean(lvRow);    
                  if(null != lvModel){
                 	 lvModelList.add(lvModel);	
                  } 
              }             
          }
        
      }      
     //return mvHKSOrderEnqDetails;
   }
   
   private static OrderSModel parserBean(IMsgXMLNode lvRow){
	   try {
		   OrderSModel lvModel = new OrderSModel();
           lvModel.setOrderGroupID(parseValue(lvRow, TagName.ORDERGROUPID));
           lvModel.setOrderID(parseValue(lvRow, TagName.ORDERID));
		//lvModel.setInputTime(new Timestamp(Long.parseLong((String)lvHMap.get(TagName.INPUTTIME))));
		
		
		lvModel.setInputTime(Timestamp.valueOf(parseValue(lvRow, TagName.INPUTTIME)));
		lvModel.setSettleDate(Date.valueOf(parseValue(lvRow, TagName.HISTORYDATE)));
		
		lvModel.setOrderType(parseValue(lvRow, TagName.ORDERTYPE));
		lvModel.setStockID(parseValue(lvRow, TagName.INSTRUMENTID));
		//Begin Task: WL00619 Walter Lau 2007 July 27
		lvModel.setMarketID(parseValue(lvRow, TagName.MARKETID));
		//End Task: WL00619 Walter Lau 2007 July 27
		lvModel.setBS(parseValue(lvRow, TagName.BS));
		//lvModel.setPrice(Double.parseDouble(lvRow.getChildNode(TagName.PRICE).getValue()));
		lvModel.setPrice(new BigDecimal(parseValue(lvRow, TagName.PRICE)));
		//lvModel.setAvgPrice(new BigDecimal((String)lvHMap.get(TagName.AVGPRICE)));
		lvModel.setQty(Long.parseLong(parseValue(lvRow, TagName.QTY)));
		//lvModel.setOSQty(Long.parseLong(lvHMap.get(TagnName.OSQTY)));
		lvModel.setFilledQty(Long.parseLong(parseValue(lvRow, TagName.FILLEDQTY)));
		lvModel.setCancelledQty(Long.parseLong(parseValue(lvRow, TagName.CANCELQTY))); //Use cancelled qty as a storage only!
		lvModel.setChannelID(parseValue(lvRow, TagName.CHANNELID));
		lvModel.setStopOrderType(parseValue(lvRow, TagName.STOPORDERTYPE));
		lvModel.setStopPrice(new BigDecimal(parseValue(lvRow, TagName.STOPPRICE)));
		lvModel.setPendPrice(new BigDecimal(parseValue(lvRow, TagName.AVGPRICE)));
		//lvModel.setPendPrice(Double.parseDouble(lvRow.getChildNode(TagName.AVGPRICE).getValue())); //Use pend price as a storage only!
		//lvModel.setPendPrice(new BigDecimal((String)lvHMap.get(TagName.AVGPRICE)));
		lvModel.setExceededAmt(new BigDecimal(parseValue(lvRow, TagName.VALUE)));
		lvModel.setStatus(parseValue(lvRow, TagName.STATUS_INTERNAL));
		lvModel.setRejectReason(parseValue(lvRow, TagName.REJECTREASON));
		return lvModel;
	   } catch (Exception e) {}
	   return null;
   }

   /**
	 * Get method for loop counter
	 * @return Loop counter counts the number of
	 */
   public int getLoopCounter()
{
    int lvCnt;
    try
    {
        lvCnt = Integer.parseInt(mvLoopCounter);
    }
    catch (Exception e)
    {
        return -1;
    }
    return lvCnt;
}
   
   private static String parseValue(IMsgXMLNode lvRow, String keyName){
		String str	= "";
		try {			
			if(null != lvRow.getChildNode(keyName)){
				str  = lvRow.getChildNode(keyName).getValue();				
			}
			if(null == str)
				return "";
			
		} catch (Exception e) {}		
		return str;
	}

/**
 * Set method for Loop Counter
 * @param Loop Counter
 */
public void setLoopCounter(String pLoopCounter)
{
        mvLoopCounter = pLoopCounter;
}

/**
 * @return the mvTotalOrders
 */
public int getMvTotalOrders() {
	return mvTotalOrders;
}
/**
 * @return the lvModelList
 */
public Vector<OrderSModel> getLvModelList() {
	return lvModelList;
}




}
