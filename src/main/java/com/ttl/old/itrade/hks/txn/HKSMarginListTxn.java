package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.bean.HKSMarginListBean;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

public class HKSMarginListTxn {
	/**
	 * This variable is used to the tp Error
	 */
	TPErrorHandling mvTpError;

	private String mvInstrumentID;

	private String mvMarketID;
	
	private int mvLending = 0;

	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;
	
	private int mvStartRecord;
	private int mvEndRecord;
	private String mvTotalRecord;
	
	private boolean mvIsExportData = false;

	private HKSMarginListBean[] avaiablemarginList = null;
	
	/**
	 * Get method for Return Code
	 * 
	 * @return Return Code
	 */
	public int getReturnCode() {
		return mvReturnCode;
	}

	public String getMvInstrumentID() {
		return mvInstrumentID;
	}

	public void setMvInstrumentID(String mvInstrumentID) {
		this.mvInstrumentID = mvInstrumentID;
	}

	public String getMvMarketID() {
		return mvMarketID;
	}

	public void setMvMarketID(String mvMarketID) {
		this.mvMarketID = mvMarketID;
	}

	public int getMvLending() {
		return mvLending;
	}

	public void setMvLending(int mvLending) {
		this.mvLending = mvLending;
	}

	/**
	 * Set method for Return Code
	 * 
	 * @param pReturnCode
	 *            the Return Code
	 */
	public void setReturnCode(int pReturnCode) {
		mvReturnCode = pReturnCode;
	}

	/**
	 * Get method for Error Code
	 * 
	 * @return Error Code
	 */
	public String getErrorCode() {
		return mvErrorCode;
	}

	/**
	 * Set method for Error Code
	 * 
	 * @param pErrorCode
	 *            the Error Code
	 */
	public void setErrorCode(String pErrorCode) {
		mvErrorCode = pErrorCode;
	}

	/**
	 * Get method for Error Message
	 * 
	 * @return Error Message
	 */
	public String getErrorMessage() {
		return mvErrorMessage;
	}

	/**
	 * Set method for Error Message
	 * 
	 * @param pErrorMessage
	 *            the Error Message
	 */
	public void setErrorMessage(String pErrorMessage) {
		mvErrorMessage = pErrorMessage;
	}

	/**
	 * Default constructor for HKSMarginListTxn class
	 */
	public HKSMarginListTxn() {
		mvTpError = new TPErrorHandling();
	}

	/**
	 * Default constructor for HKSMarginListTxn class
	 * 
	 * @param pMarketID the Market ID
	 * @param pInstrumentID the Instrument ID
	 * @param pLending the Lend percentage
	 */
	public HKSMarginListTxn(String pMarketID, String pInstrumentID, Integer pLending) {
		
		mvTpError = new TPErrorHandling();
		this.mvMarketID = pMarketID;
		this.mvInstrumentID = pInstrumentID;
		this.mvLending = pLending;
	}
	
	
	public void getMarginList() {
		try {
			Hashtable lvTxnMap = new Hashtable();

			IMsgXMLNode lvRetNode;
			// TPManager ivTPManager = ITradeServlet.getTPManager();
			TPManager ivTPManager = ITradeServlet
					.getTPManager("HKSMarginList");
			TPBaseRequest lvTransactionHistoryRequest = ivTPManager
					.getRequest("HKSMarginList");

			lvTxnMap.put("MARKETID", mvMarketID);
			lvTxnMap.put("INSTRUMENTID", mvInstrumentID);
			lvTxnMap.put("LENDINGPERCENTAGE", String.valueOf(mvLending));
		    lvTxnMap.put("STARTRECORD", String.valueOf(mvStartRecord));
			lvTxnMap.put("ENDRECORD", String.valueOf(mvEndRecord));
			if (mvIsExportData) {
		      lvTxnMap.put("EXPORTDATA", "Y");
		    }else{
		      lvTxnMap.put("EXPORTDATA", "N");
		    }

			lvRetNode = lvTransactionHistoryRequest.send(lvTxnMap);

			setReturnCode(mvTpError.checkError(lvRetNode));
			if (mvReturnCode != mvTpError.TP_NORMAL) {
				setErrorMessage(mvTpError.getErrDesc());
				if (mvReturnCode == mvTpError.TP_APP_ERR) {
					setErrorCode(mvTpError.getErrCode());
				}
			} else {
				
				if (lvRetNode.getChildNode("TOTALRECORD") != null) {
					setMvTotalRecord(lvRetNode.getChildNode("TOTALRECORD")
							.getValue());
				}

				IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("ChildRow");
				avaiablemarginList = new HKSMarginListBean[lvRowList.size()];
				for (int i = 0; i < lvRowList.size(); i++) {
					avaiablemarginList[i] = new HKSMarginListBean();

					IMsgXMLNode lvRow = lvRowList.getNode(i);
					
					// get instrument id
					avaiablemarginList[i].setMvRowNum(lvRow.getChildNode("ROWNUM").getValue());
					
					// get instrument id
					avaiablemarginList[i].setMvInstrumentID(lvRow.getChildNode("INSTRUMENTID").getValue());
					
					// get market id
					avaiablemarginList[i].setMvMarketID(lvRow.getChildNode("MARKETID").getValue());
					
					// instrument name
					avaiablemarginList[i].setMvInstrumentName(lvRow.getChildNode("INSTRUMENTNAME").getValue());
					
					//lending percentage					
					avaiablemarginList[i].setMvLendPercent(lvRow.getChildNode("LENDINGPERCENTAGE").getValue());

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public int getMvStartRecord() {
		return mvStartRecord;
	}

	public void setMvStartRecord(int mvStartRecord) {
		this.mvStartRecord = mvStartRecord;
	}

	public void setMvEndRecord(int mvEndRecord) {
		this.mvEndRecord = mvEndRecord;
	}

	public int getMvEndRecord() {
		return mvEndRecord;
	}

	public void setMvTotalRecord(String mvTotalRecord) {
		this.mvTotalRecord = mvTotalRecord;
	}

	public String getMvTotalRecord() {
		return mvTotalRecord;
	}

	/**
	 * @param mvIsExportData the mvIsExportData to set
	 */
	public void setMvIsExportData(boolean mvIsExportData) {
		this.mvIsExportData = mvIsExportData;
	}

	/**
	 * @return the mvIsExportData
	 */
	public boolean isMvIsExportData() {
		return mvIsExportData;
	}

	public HKSMarginListBean[] getAvaiablemarginList() {
		return avaiablemarginList;
	}

	public void setAvaiablemarginList(HKSMarginListBean[] avaiablemarginList) {
		this.avaiablemarginList = avaiablemarginList;
	}
	
	

}
