package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: Client Portfolio Enquiry Transaction</p>
 * <p>Description: This class stores and processes the client portfolio enquiry transactio</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.tp.TPManager;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.util.FieldSplitter;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.tag.ITagXsfTagName;
import com.systekit.winvest.hks.config.mapping.TagName;
import com.systekit.winvest.hks.util.Utils;

/**
 * This class stores and processes the client portfolio enquiry transactio
 * 
 * @author Tree Lam
 * @since 20080625
 */
public class HKSPortfolioEnquiryTxn extends BaseTxn {

	private String mvClientId;
	private String mvTradingAccSeq;
	private String mvAccountType;
	private boolean mvPortfolioWithTrade = false;
	private String mvRecordCount;
	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;

	private HKSPortfolioEnquiryDetails[] mvHKSPortfolioEnqDtls;

	public static final String LOOPCOUNTER = "LoopCounter";
	public static final String LOOPROWS = "LoopRows";
	public static final String FIELDNAMES = "FieldNames";
	public static final String VALUES = "Values";
	public static final String RESULT = "RESULT";

	public static final String CLIENTID = "CLIENTID";
	public static final String TRADINGACCSEQ = "TRADINGACCSEQ";
	public static final String WITHTRADE = "WITHTRADE";
	public static final String INSTRUMENTID = "INSTRUMENTID";
	// Begin Task: WL00619 Walter Lau 2007 July 27
	public static final String PRODUCTID = "PRODUCTID";
	public static final String MARKETID = "MARKETID";
	// End Task: WL00619 Walter Lau 2007 July 27

	// public static final String INSTRUMENTSHORTNAME = "INSTRUMENTSHORTNAME";
	// public static final String INSTRUMENTCHINESESHORTNAME =
	// "INSTRUMENTCHINESESHORTNAME";
	public static final String LEDGERQTY = "LEDGERQTY";
	public static final String TTODAYBUY = "TTODAYBUY";
	public static final String TTODAYCONFIRMBUY = "TTODAYCONFIRMBUY";
	public static final String TTODAYSELL = "TTODAYSELL";
	public static final String TTODAYCONFIRMSELL = "TTODAYCONFIRMSELL";
	public static final String TINACTIVEBUY = "TINACTIVEBUY";
	public static final String TINACTIVESELL = "TINACTIVESELL";
	public static final String TRADABLEQTY = "TRADABLEQTY";
	public static final String NOMINALPRICE = "NOMINALPRICE";
	public static final String CLOSINGPRICE = "CLOSINGPRICE";
	public static final String COSTPERSHARE = "COSTPERSHARE";
	public static final String COSTQTY = "COSTQTY";
	public static final String TCOSTAMOUNT = "TCOSTAMOUNT";
	public static final String TCOSTQTY = "TCOSTQTY";
	public static final String TUNSETTLEBUY = "TUNSETTLEBUY";
	public static final String TUNSETTLESELL = "TUNSETTLESELL";

	public static final String MARGINPERCENTAGE = "MARGINPERCENTAGE";
	public static final String MARGINVALUE = "MARGINVALUE";
	public static final String TOTALAVERAGECOST = "TOTALAVERAGECOST";
	public static final String TOTALCOSTAMOUNT = "TOTALCOSTAMOUNT";

	// BEGIN - TASK#: CL00014 - Charlie Liu 20080829
	public static final String TMANUALHOLD = "TMANUALHOLD";
	// END - TASK# CL00014

	// public static final String HYPERLINK;

	private HKSPortfolioEnquiryDetails[] mvHKSPortfolioEnquiryDetails;

	private HKSPortfolioAccountSummaryEqdDetails mvAccountSummaryEqdDetails;

	private HKSPortfolioTradingAccSummaryDetails mvTradingAccSummayDetails;

	private String mvLoopCounter;

	TPErrorHandling mvTpError;

	// Begin Task #RC00181 - Rice Cheng 20090108
	private String mvLanguage;
	// End Task #RC00181 - Rice Cheng 20090108

	private List mvPendEntitleList = new ArrayList();

	private boolean mvIsQueryAdvanceMoney = true;

	/**
	 * Default constructor for HKSPortfolioEnquiryTxn class
	 */
	public HKSPortfolioEnquiryTxn() {
		mvTpError = new TPErrorHandling();
	}

	/**
	 * Default constructor for HKSPortfolioEnquiryTxn class
	 * 
	 * @param pClientId
	 *            the Client ID
	 * @param pTradingAccSeq
	 *            the Trading Account Sequence
	 * @param pPortfolioWithTrade
	 *            the portfolio with trade
	 */
	public HKSPortfolioEnquiryTxn(String pClientId, String pTradingAccSeq, boolean pPortfolioWithTrade) {
		// Begin Task #RC00181 - Rice Cheng 20090108
		// setClientId(pClientId);
		// setTradingAccSeq(pTradingAccSeq);
		// setPortfolioWithTrade(pPortfolioWithTrade);
		// mvTpError = new TPErrorHandling();
		this(pClientId, pTradingAccSeq, pPortfolioWithTrade, "");
		// End Task #RC00181 - Rice Cheng 20090108
	}

	/**
	 * Default constructor for HKSPortfolioEnquiryTxn class
	 * 
	 * @param pClientId
	 *            the Client ID
	 * @param pTradingAccSeq
	 *            Trading Account Sequence
	 * @param pPortfolioWithTrade
	 *            the portfolio with trade
	 * @param pLanguage
	 *            the user locale
	 */
	// Begin Task #RC00181 - Rice Cheng 20090108
	public HKSPortfolioEnquiryTxn(String pClientId, String pTradingAccSeq, boolean pPortfolioWithTrade, String pLanguage) {
		setClientId(pClientId);
		setTradingAccSeq(pTradingAccSeq);
		setPortfolioWithTrade(pPortfolioWithTrade);
		mvTpError = new TPErrorHandling();
		setLanguage(pLanguage);
	}

	// End Task #RC00181 - Rice Cheng 20090108

	/**
	 * The method process the client portfolio enquiry transactio
	 * 
	 */
	public void process() {
		process(null);
	}

	/**
	 * The method process the client portfolio enquiry transactio
	 * 
	 * @param presentation
	 *            the Presentation class
	 */

	public void process(Presentation presentation) {
		enquiryPortfolioUsingBO();
	}

	public void enquiryPortfolioUsingBO() {
		Log.println("[ HKSPortfolioEnquiryTxn._process() CLIENTID : " + getClientId() + " ]", Log.ACCESS_LOG);
		try {
			Hashtable lvTxnMap = new Hashtable();

			lvTxnMap.put(CLIENTID, getClientId());
			lvTxnMap.put(TRADINGACCSEQ, getTradingAccSeq());
			lvTxnMap.put("WITHTRADE", getPortfolioWithTrade() ? "Y" : "N");

			lvTxnMap.put("ENABLEADVANCEMONEYQUERY", mvIsQueryAdvanceMoney ? "Y" : "N");
			lvTxnMap.put("INCLUDECASHACCOUNTSUMMARY", "Y");
			lvTxnMap.put("INCLUDETRADINGACCOUNTSUMMARY", "Y");
			lvTxnMap.put("INCLUDETRADINGACCOUNTDETAILS", "N");

			IMsgXMLNode lvErrorCodeNode;

			if (TPErrorHandling.TP_NORMAL != process(RequestName.HKSPortfolioEnquiryRequest, lvTxnMap)) {
				if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
					setErrorCode(new ErrorCode(new String[0], "0100", "No connection with PLugin TP"));
				}

				Log.println("Error error occur when communicate with Plugin TP", Log.ACCESS_LOG);
			} else {
				IMsgXMLNode lvNodeFnames = mvReturnNode.getChildNode("TRADINGACCOUNTEQD").getChildNode(FIELDNAMES);

				IMsgXMLNodeList lvNodeListValues = mvReturnNode.getChildNode("TRADINGACCOUNTEQD").getChildNode(LOOPROWS).getNodeList(VALUES);
				mvHKSPortfolioEnquiryDetails = new HKSPortfolioEnquiryDetails[lvNodeListValues.size()];

				for (int i = 0; i < lvNodeListValues.size(); i++) {
					mvHKSPortfolioEnquiryDetails[i] = new HKSPortfolioEnquiryDetails();

					String lvFieldnames = lvNodeFnames.getValue();
					String lvValues = lvNodeListValues.getNode(i).getValue();
					Hashtable lvHMap = FieldSplitter.tokenize(lvFieldnames, lvValues, "|");

					mvHKSPortfolioEnquiryDetails[i].setHashtable(lvHMap);
					mvHKSPortfolioEnquiryDetails[i].setInstrumentId((String) lvHMap.get(INSTRUMENTID));
					// Begin Task: WL00619 Walter Lau 2007 July 27
					mvHKSPortfolioEnquiryDetails[i].setProductID((String) lvHMap.get(PRODUCTID));
					mvHKSPortfolioEnquiryDetails[i].setMarketID((String) lvHMap.get(MARKETID));
					// End Task: WL00619 Walter Lau 2007 July 27
					// mvHKSPortfolioEnquiryDetails[i].setInstrumentShortName((String)
					// lvHMap.get(INSTRUMENTSHORTNAME));
					// mvHKSPortfolioEnquiryDetails[i].setInstrumentChineseShortName((String)
					// lvHMap.get(INSTRUMENTCHINESESHORTNAME));
					// mvHKSPortfolioEnquiryDetails[i].setLedgerQty((String)
					// lvHMap.get(LEDGERQTY));
					mvHKSPortfolioEnquiryDetails[i].setTTodayBuy((String) lvHMap.get(TTODAYBUY));
					mvHKSPortfolioEnquiryDetails[i].setTTodayConfirmBuy((String) lvHMap.get(TTODAYCONFIRMBUY));
					mvHKSPortfolioEnquiryDetails[i].setTTodaySell((String) lvHMap.get(TTODAYSELL));
					mvHKSPortfolioEnquiryDetails[i].setTTodayConfirmSell((String) lvHMap.get(TTODAYCONFIRMSELL));
					mvHKSPortfolioEnquiryDetails[i].setTInactiveBuy((String) lvHMap.get(TINACTIVEBUY));
					mvHKSPortfolioEnquiryDetails[i].setTInactiveSell((String) lvHMap.get(TINACTIVESELL));
					mvHKSPortfolioEnquiryDetails[i].setTradableQty((String) lvHMap.get(TRADABLEQTY));
					mvHKSPortfolioEnquiryDetails[i].setNominalPrice((String) lvHMap.get(NOMINALPRICE));
					mvHKSPortfolioEnquiryDetails[i].setClosingPrice((String) lvHMap.get(CLOSINGPRICE));
					mvHKSPortfolioEnquiryDetails[i].setCostPerShare((String) lvHMap.get(COSTPERSHARE));
					mvHKSPortfolioEnquiryDetails[i].setCostQty((String) lvHMap.get(COSTQTY));
					mvHKSPortfolioEnquiryDetails[i].setMarketValue((String) lvHMap.get(TagName.MARKETVALUE));
					//HIEU LE: more infor for MAS
					//mvHKSPortfolioEnquiryDetails[i].setMarginPercentage((String) lvHMap.get(MARGINPERCENTAGE));
					mvHKSPortfolioEnquiryDetails[i].setMarginPercentage((String) lvHMap.get("FINALMARGINPERCENTAGE"));
					mvHKSPortfolioEnquiryDetails[i].setMarginValue((String) lvHMap.get(MARGINVALUE));
					mvHKSPortfolioEnquiryDetails[i].setMaintenancePercentage((String) lvHMap.get("MAINTENANCEPERCENTAGE"));
					mvHKSPortfolioEnquiryDetails[i].setMaintenanceValue((String) lvHMap.get("MAINTENANCEVALUE"));
					//END HIEU LE
					mvHKSPortfolioEnquiryDetails[i].setTSettled((String) lvHMap.get(TagName.TSETTLED));
					mvHKSPortfolioEnquiryDetails[i].setTDueBuy((String) lvHMap.get(TagName.TDUEBUY));
					mvHKSPortfolioEnquiryDetails[i].setTDueSell((String) lvHMap.get(TagName.TDUESELL));
					mvHKSPortfolioEnquiryDetails[i].setTUnSettleBuy((String) lvHMap.get(TagName.TUNSETTLEBUY));
					mvHKSPortfolioEnquiryDetails[i].setTUnSettleSell((String) lvHMap.get(TagName.TUNSETTLESELL));
					mvHKSPortfolioEnquiryDetails[i].setTManualHold((String) lvHMap.get(TagName.TMANUALHOLD));
					mvHKSPortfolioEnquiryDetails[i].setNormalHold((String) lvHMap.get("TNORMALHOLDQTY"));
					mvHKSPortfolioEnquiryDetails[i].setConditionalHold((String) lvHMap.get("TCONDITIONALHOLDQTY"));
					mvHKSPortfolioEnquiryDetails[i].setTNomineeQty((String) lvHMap.get(TagName.NOMINEEQTY));
					mvHKSPortfolioEnquiryDetails[i].setTCostAmount((String) lvHMap.get(TagName.TCOSTAMOUNT));
					mvHKSPortfolioEnquiryDetails[i].setMvTTodayCostAmount((String) lvHMap.get("TTODAYCOSTAMOUNT"));
					mvHKSPortfolioEnquiryDetails[i].setTCostQty((String) lvHMap.get("TCOSTQTY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTTodayCostQty((String) lvHMap.get("TTODAYCOSTQTY"));
					mvHKSPortfolioEnquiryDetails[i].setTotalAverageCost((String) lvHMap.get("TOTALAVERAGECOST"));
					mvHKSPortfolioEnquiryDetails[i].setTotalCostAmount((String) lvHMap.get("TOTALCOSTAMOUNT"));
					mvHKSPortfolioEnquiryDetails[i].setInstrumentName((String) lvHMap.get("INSTRUMENTNAME"));
					mvHKSPortfolioEnquiryDetails[i].setInstrumentShortName((String) lvHMap.get("INSTRUMENTSHORTNAME"));
					// Begin Task #:- TTL-CN-ZZW-0001 Wind Zhao 20090901
					mvHKSPortfolioEnquiryDetails[i].setMvCurrencyID((String) lvHMap.get("CURRENCYID"));
					// End Task #:- TTL-CN-ZZW-0001 Wind Zhao 20090901

					/*
					 * // BEGIN TASK #:- TTL-GZ-XYL-00038 XuYuLong 20091119
					 * ITrade // .R5 VN Adding extra fields
					 * mvHKSPortfolioEnquiryDetails[i] .setMvMktPrice((String)
					 * lvHMap.get("MARKETPRICE"));
					 * mvHKSPortfolioEnquiryDetails[i] .setMvHoldAmt((String)
					 * lvHMap.get("HOLD")); mvHKSPortfolioEnquiryDetails[i]
					 * .setMvUnitPrice((String) lvHMap.get("UNITPRICE"));
					 * mvHKSPortfolioEnquiryDetails[i].setMvTValue((String)
					 * lvHMap .get("TVALUE"));
					 * mvHKSPortfolioEnquiryDetails[i].setMvPdBuy((String)
					 * lvHMap .get("PENDINGBUY"));
					 * mvHKSPortfolioEnquiryDetails[i].setMvPdSell((String)
					 * lvHMap .get("PENDINGSELL")); // END TASK #:-
					 * TTL-GZ-XYL-00038 XuYuLong 20091119 ITrade // .R5 VN
					 * Adding extra fields
					 */
					mvHKSPortfolioEnquiryDetails[i].setMvTTodayUnsettleBuy((String) lvHMap.get("TTODAYUNSETTLEBUY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTTodayUnsettleSell((String) lvHMap.get("TTODAYUNSETTLESELL"));

					mvHKSPortfolioEnquiryDetails[i].setMvTT1UnsettleBuy((String) lvHMap.get("TT1UNSETTLEBUY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTT1UnsettleSell((String) lvHMap.get("TT1UNSETTLESELL"));

					mvHKSPortfolioEnquiryDetails[i].setMvTT2UnsettleBuy((String) lvHMap.get("TT2UNSETTLEBUY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTT2UnsettleSell((String) lvHMap.get("TT2UNSETTLESELL"));
					mvHKSPortfolioEnquiryDetails[i].setTSettled((String) lvHMap.get("TSETTLED"));

					/*
					 * //BEGIN TASK #:- TTL-VN VanTran 20100930 ITrade VN Adding
					 * extra fields if(mvPendEntitleList !=null){ HashMap
					 * pendingModel; for (Object pendItem: mvPendEntitleList) {
					 * pendingModel = (HashMap)pendItem; if
					 * (pendingModel.get(TagName
					 * .MARKETID).toString().trim().equals
					 * (lvHMap.get(MARKETID).toString()) &&
					 * pendingModel.get(TagName
					 * .INSTRUMENTID).toString().trim().equals
					 * (lvHMap.get(INSTRUMENTID).toString())){
					 * mvHKSPortfolioEnquiryDetails
					 * [i].setMvTEntitlementQty((String)
					 * pendingModel.get("RECEIVABLEQTY")); break; } } }
					 */

					mvHKSPortfolioEnquiryDetails[i].setMvTPendingEntitlementQty((String) lvHMap.get("TPENDINGENTITLEMENTQTY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTPendingEntitlementCostAmt((String) lvHMap.get("TPENDINGENTITLEMENTCOSTAMT"));

					mvHKSPortfolioEnquiryDetails[i].setMvTAwaitingTraceCert((String) lvHMap.get("TAWAITINGTRADECERT"));

					String awaitDeposit = (lvHMap.get("TAWAITINGDEPOSITCERT") != null) ? (String) lvHMap.get("TAWAITINGDEPOSITCERT") : "0";
					mvHKSPortfolioEnquiryDetails[i].setMvTAwaitingDepositCert(awaitDeposit);

					String awaitWithdrawal = (lvHMap.get("TAWAITINGWITHDRAWALCERT") != null) ? (String) lvHMap.get("TAWAITINGWITHDRAWALCERT") : "0";
					mvHKSPortfolioEnquiryDetails[i].setMvTAwaitingWithdrawalCert(awaitWithdrawal);

					String mortgateQty = (lvHMap.get("TMORTGAGEQTY") != null) ? (String) lvHMap.get("TMORTGAGEQTY") : "0";
					mvHKSPortfolioEnquiryDetails[i].setMvTMortgateQty(mortgateQty);
					// END TASK #:- TTL-VN VanTran 20100930 ITrade VN Adding
					// extra fields

					// BEGIN TASK #:- TTL-VN Giang Tran 20101124 Count ledger
					// quantity
					// Ledger = (Usable + Conf Buy + Pending T+0 Buy + Pending
					// T+1 Buy + Pending T+2 Buy + Due Buy + Await Deposit Cert
					// + Await Trade Cert + Pending Entitlement + Normal Hold +
					// Mortgage hold)
					// pending buy = Pending T+0 Buy + Pending T+1 Buy + Pending
					// T+2 Buy + Due Buy
					// get usable
					BigDecimal usable = (lvHMap.get(TRADABLEQTY) != null && lvHMap.get(TRADABLEQTY).toString().trim().length() > 0) ? new BigDecimal(lvHMap
							.get(TRADABLEQTY).toString().trim()) : new BigDecimal(0);
					// get normalHold
					BigDecimal normalHold = (lvHMap.get(TagName.TMANUALHOLD) != null && lvHMap.get(TagName.TMANUALHOLD).toString().trim().length() > 0) ? new BigDecimal(
							lvHMap.get(TagName.TMANUALHOLD).toString().trim()) : new BigDecimal(0);
					// get mortgage
					BigDecimal mortgage = new BigDecimal(mortgateQty.trim());
					// get await deposit
					BigDecimal awtDeposit = new BigDecimal(awaitDeposit.trim());

					// get await trace
					BigDecimal awaitTradeCert = (lvHMap.get("TAWAITINGTRADECERT") != null && lvHMap.get("TAWAITINGTRADECERT").toString().trim().length() > 0) ? new BigDecimal(
							lvHMap.get("TAWAITINGTRADECERT").toString().trim()) : new BigDecimal(0);
					// Pending Entitlement
					BigDecimal pendingEntilement = (mvHKSPortfolioEnquiryDetails[i].getMvTPendingEntitlementQty() != null && mvHKSPortfolioEnquiryDetails[i]
							.getMvTPendingEntitlementQty().trim().length() > 0) ? new BigDecimal(mvHKSPortfolioEnquiryDetails[i].getMvTPendingEntitlementQty()
							.trim()) : new BigDecimal(0);
					// today Confirm Buy
					BigDecimal todayCfmBuy = (lvHMap.get("TTODAYCONFIRMBUY") != null && lvHMap.get("TTODAYCONFIRMBUY").toString().trim().length() > 0) ? new BigDecimal(
							lvHMap.get("TTODAYCONFIRMBUY").toString().trim()) : new BigDecimal(0);
					// Pending Buy = = Pending T+0 Buy + Pending T+1 Buy +
					// Pending T+2 Buy + Due Buy
					BigDecimal pdBuy = (lvHMap.get("PENDINGBUY") != null && lvHMap.get("PENDINGBUY").toString().trim().length() > 0) ? new BigDecimal(lvHMap
							.get("PENDINGBUY").toString().trim()) : new BigDecimal(0);

					// get ledgerQty
					BigDecimal lvLegderQty = usable.add(todayCfmBuy).add(pdBuy).add(awtDeposit).add(awaitTradeCert).add(pendingEntilement).add(normalHold)
							.add(mortgage);
					mvHKSPortfolioEnquiryDetails[i].setLedgerQty(lvLegderQty.toString());

				}

				IMsgXMLNode lvLoopCounterNode = mvReturnNode.getChildNode("TRADINGACCOUNTEQD").getChildNode(LOOPCOUNTER);
				if (lvLoopCounterNode != null) {
					setLoopCounter(lvLoopCounterNode.getValue());
				} else {
					setLoopCounter("0");
				}

				IMsgXMLNode lvCashAccSummaryNodeFnames = mvReturnNode.getChildNode("CASHACCOUNTSUMMARY").getChildNode(FIELDNAMES);

				IMsgXMLNodeList lvCashAccSummaryNodeListValues = mvReturnNode.getChildNode("CASHACCOUNTSUMMARY").getChildNode(LOOPROWS).getNodeList(VALUES);

				mvAccountSummaryEqdDetails = new HKSPortfolioAccountSummaryEqdDetails();

				String lvCashAccSummaryFieldnames = lvCashAccSummaryNodeFnames.getValue();
				String lvCashAccSummaryValues = lvCashAccSummaryNodeListValues.getNode(0).getValue();

				Hashtable lvCashAccSummaryHMap = FieldSplitter.tokenize(lvCashAccSummaryFieldnames, lvCashAccSummaryValues, "|");

				mvAccountSummaryEqdDetails.setMvSettledBalance((String) lvCashAccSummaryHMap.get("SETTLEDBALANCE"));
				mvAccountSummaryEqdDetails.setLedgerBal((String) lvCashAccSummaryHMap.get("LEDGERBALANCE"));
				mvAccountSummaryEqdDetails.setMvTodaySettlement((String) lvCashAccSummaryHMap.get("TODAYSETTLEMENT"));
				mvAccountSummaryEqdDetails.setMvHoldAmount((String) lvCashAccSummaryHMap.get("HOLDAMOUNT"));
				mvAccountSummaryEqdDetails.setMvReserveAmount((String) lvCashAccSummaryHMap.get("RESERVEAMOUNT"));
				mvAccountSummaryEqdDetails.setMvInterest((String) lvCashAccSummaryHMap.get("INTEREST"));
				mvAccountSummaryEqdDetails.setMvDPWD((String) lvCashAccSummaryHMap.get("DPWD"));
				mvAccountSummaryEqdDetails.setCTodayConfirmBuy((String) lvCashAccSummaryHMap.get("CTODAYCONFIRMBUY"));
				
				mvAccountSummaryEqdDetails.setCTodayBuy((String) lvCashAccSummaryHMap.get("CTODAYBUY"));
				mvAccountSummaryEqdDetails.setCTodaySell((String) lvCashAccSummaryHMap.get("CTODAYSELL"));
				mvAccountSummaryEqdDetails.setCDueBuy((String) lvCashAccSummaryHMap.get("CDUEBUY"));				
				mvAccountSummaryEqdDetails.setCUnsettleBuy((String) lvCashAccSummaryHMap.get("CUNSETTLEBUY"));
				
				mvAccountSummaryEqdDetails.setCTodayConfirmSell((String) lvCashAccSummaryHMap.get("CTODAYCONFIRMSELL"));
				mvAccountSummaryEqdDetails.setCDueSell((String) lvCashAccSummaryHMap.get("CDUESELL"));
				mvAccountSummaryEqdDetails.setCTodayUnsettleSell((String) lvCashAccSummaryHMap.get("CTODAYUNSETTLESELL"));				
				mvAccountSummaryEqdDetails.setCUnsettleSell((String) lvCashAccSummaryHMap.get("CUNSETTLESELL"));				
				mvAccountSummaryEqdDetails.setCT1UnsettleSell((String) lvCashAccSummaryHMap.get("CT1UNSETTLESELL"));
				mvAccountSummaryEqdDetails.setCT2UnsettleSell((String) lvCashAccSummaryHMap.get("CT2UNSETTLESELL"));
				mvAccountSummaryEqdDetails.setCT3UnsettleSell((String) lvCashAccSummaryHMap.get("CT3UNSETTLESELL"));
				
				mvAccountSummaryEqdDetails.setCNextDayDueBuy((String) lvCashAccSummaryHMap.get("CNEXTDAYDUEBUY"));
				mvAccountSummaryEqdDetails.setCNextDayDueSell((String) lvCashAccSummaryHMap.get("CNEXTDAYDUESELL"));
				mvAccountSummaryEqdDetails.setCInactiveBuy((String) lvCashAccSummaryHMap.get("CINACTIVEBUY"));
				mvAccountSummaryEqdDetails.setCShortSellAmt((String) lvCashAccSummaryHMap.get("CSHORTSELLAMT"));
				mvAccountSummaryEqdDetails.setDrawableBal((String) lvCashAccSummaryHMap.get("DRAWABLEBAL"));
				mvAccountSummaryEqdDetails.setMvUsable((String) lvCashAccSummaryHMap.get("USABLEBALANCE"));
				mvAccountSummaryEqdDetails.setMvPenddingWithdrawMoney((String) lvCashAccSummaryHMap.get("CPENDINGWITHDRAWAL"));				
				mvAccountSummaryEqdDetails.setCTodayIn((String) lvCashAccSummaryHMap.get("CTODAYIN"));
				mvAccountSummaryEqdDetails.setCPendingWithdrawal((String) lvCashAccSummaryHMap.get("CPENDINGWITHDRAWAL"));
				//HIEU LE: get some values for portfolio
				mvAccountSummaryEqdDetails.setCManualReserve((String) lvCashAccSummaryHMap.get("CManualReserve"));
				mvAccountSummaryEqdDetails.setMvCCashWithdrawable((String) lvCashAccSummaryHMap.get("WITHDRAWABLEBAL"));
				mvAccountSummaryEqdDetails.setCTodayOut((String) lvCashAccSummaryHMap.get("CTODAYOUT"));
				mvAccountSummaryEqdDetails.setBuyAmt((String) lvCashAccSummaryHMap.get("OSBUYAMT"));
				mvAccountSummaryEqdDetails.setTotalAsset((String) lvCashAccSummaryHMap.get("TOTALASSET"));
				mvAccountSummaryEqdDetails.setEquity((String) lvCashAccSummaryHMap.get("EQUITY"));
				mvAccountSummaryEqdDetails.setStockMaintenance((String) lvCashAccSummaryHMap.get("STOCKMAINTENANCEVALUE"));
				mvAccountSummaryEqdDetails.setCashMaintenance((String) lvCashAccSummaryHMap.get("CASHMAINTENANCEVALUE"));
				mvAccountSummaryEqdDetails.setDebitAccruedInterest((String) lvCashAccSummaryHMap.get("DEBITINTEREST"));
				mvAccountSummaryEqdDetails.setUnrealizedLoan((String) lvCashAccSummaryHMap.get("UNREALIZEDLOAN"));
				//END HIEU LE

				if (mvIsQueryAdvanceMoney) {
					mvAccountSummaryEqdDetails.setMvAvailAdvanceMoney((String) lvCashAccSummaryHMap.get("AVAILABLEADVANCEMONEY"));
					mvAccountSummaryEqdDetails.setMvOutstandingLoan((String) lvCashAccSummaryHMap.get("OUTSTANDINGLOAN"));
					mvAccountSummaryEqdDetails.setMvTotalOutAdvance((String) lvCashAccSummaryHMap.get("TOTALOUTSTANDINGADVANCEAMOUNT"));
				}

				IMsgXMLNode lvTradingAccSummaryNodeFnames = mvReturnNode.getChildNode("TRADINGACCOUNTSUMMARY").getChildNode(FIELDNAMES);
				IMsgXMLNodeList lvTradingAccSummaryNodeListValues = mvReturnNode.getChildNode("TRADINGACCOUNTSUMMARY").getChildNode(LOOPROWS)
						.getNodeList(VALUES);

				String lvTradingAccSummaryFieldnames = lvTradingAccSummaryNodeFnames.getValue();
				String lvTradingAccSummaryValues = lvTradingAccSummaryNodeListValues.getNode(0).getValue();
				Hashtable lvTradingAccSummaryHMap = FieldSplitter.tokenize(lvTradingAccSummaryFieldnames, lvTradingAccSummaryValues, "|");

				mvTradingAccSummayDetails = new HKSPortfolioTradingAccSummaryDetails();
				
					
				mvTradingAccSummayDetails.setMvAvailableBalance((String) lvTradingAccSummaryHMap.get("TPLUSXBUYINGPOWER"));
				
				mvTradingAccSummayDetails.setMarginValue((String) lvTradingAccSummaryHMap.get("MARGINVALUE"));
				mvTradingAccSummayDetails.setMarginableValue((String) lvTradingAccSummaryHMap.get("MARGINABLEVALUE"));
				mvTradingAccSummayDetails.setCreditLimit((String) lvTradingAccSummaryHMap.get("CREDITLIMIT"));
				mvTradingAccSummayDetails.setMvMarginPercentage((String) lvTradingAccSummaryHMap.get("MARGINPERCENTAGE"));
				mvTradingAccSummayDetails.setMarginPosition((String) lvTradingAccSummaryHMap.get("MARGINPOS"));
				mvTradingAccSummayDetails.setMarginCall((String) lvTradingAccSummaryHMap.get("MARGINCALL"));

				String extraCreditValue = (String) lvTradingAccSummaryHMap.get("EXTRACREDITD");
				String extraChannelCreditValue = (String) lvTradingAccSummaryHMap.get("EXTRACHANNELCREDITD");
				String boBuyingPowerd = (String) lvTradingAccSummaryHMap.get("BUYINGPOWER");
				if (Utils.parseDouble(extraChannelCreditValue) > 0) {
					mvTradingAccSummayDetails.setMvBuyingPowerd((String) lvTradingAccSummaryHMap.get("VIRTUAL_CHANNEL_BUYINGPOWER"));
				} else {
					BigDecimal finalBuyingPowerd = Utils.parseBigDecimal(boBuyingPowerd).subtract(Utils.parseBigDecimal(extraCreditValue))
							.add(Utils.parseBigDecimal(extraChannelCreditValue));
					mvTradingAccSummayDetails.setMvBuyingPowerd(finalBuyingPowerd.toString());
				}

				mvTradingAccSummayDetails.setMvExtraCreditd(extraCreditValue);
				mvTradingAccSummayDetails.setMvRemaining((String) lvTradingAccSummaryHMap.get("REMAINING"));
				mvTradingAccSummayDetails.setMarketValue((String) lvTradingAccSummaryHMap.get("MARKETVALUE"));
				mvTradingAccSummayDetails.setMvTPlusBuyingPower((String) lvTradingAccSummaryHMap.get("TPLUSXBUYINGPOWER"));				
				mvTradingAccSummayDetails.setMvTPlusXSOFValue((String) lvTradingAccSummaryHMap.get("TPLUSXSOFVALUE"));
				
				mvTradingAccSummayDetails.setTotalAsset((String) lvTradingAccSummaryHMap.get("TOTALASSET"));
				mvTradingAccSummayDetails.setEquity((String) lvTradingAccSummaryHMap.get("EQUITY"));
				mvTradingAccSummayDetails.setCashMaintenance((String) lvTradingAccSummaryHMap.get("CASHMAINTENANCEVALUE"));
				mvTradingAccSummayDetails.setStockMaintenance((String) lvTradingAccSummaryHMap.get("STOCKMAINTENANCEVALUE"));
				mvTradingAccSummayDetails.setClientMaintenancePercent((String) lvTradingAccSummaryHMap.get("CLIENTMAINTENANCEPERCENT"));
				mvTradingAccSummayDetails.setMaintenanceRatio((String) lvTradingAccSummaryHMap.get("MAINTENANCERATIO"));
				mvTradingAccSummayDetails.setSupplementCash((String) lvTradingAccSummaryHMap.get("SUPPLEMENTCASH"));
				
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void enquiryPortfolioUsingFO() {
		try {

			Hashtable lvTxnMap = new Hashtable();
			IMsgXMLNode lvNode;

			IMsgXMLNode lvRetNode;
			TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSClientPortfolioEnquiry);
			TPBaseRequest lvPortfolioEnquiryRequest = ivTPManager.getRequest(RequestName.HKSClientPortfolioEnquiry);
			lvTxnMap.put(CLIENTID, getClientId());
			lvTxnMap.put(TRADINGACCSEQ, getTradingAccSeq());
			lvTxnMap.put("WITHTRADE", getPortfolioWithTrade() ? "Y" : "N");

			// Call to get pending entitlement list
			processGetPendEntitlementList();

			// BEGIN Task #:- al00001 albert lai 20080828

			// Begin Task #RC00181 - Rice Cheng 20090108
			// if (TPErrorHandling.TP_NORMAL ==
			// process(RequestName.HKSClientPortfolioEnquiry, lvTxnMap))
			if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSClientPortfolioEnquiry, lvTxnMap, getLanguage()))
			// End Task #RC00181 - Rice Cheng 20090108
			{
				IMsgXMLNode lvNodeFnames = mvReturnNode.getChildNode(RESULT).getChildNode(FIELDNAMES);
				IMsgXMLNodeList lvNodeListValues = mvReturnNode.getChildNode(RESULT).getChildNode(LOOPROWS).getNodeList(VALUES);
				mvHKSPortfolioEnquiryDetails = new HKSPortfolioEnquiryDetails[lvNodeListValues.size()];

				for (int i = 0; i < lvNodeListValues.size(); i++) {
					mvHKSPortfolioEnquiryDetails[i] = new HKSPortfolioEnquiryDetails();

					String lvFieldnames = lvNodeFnames.getValue();
					String lvValues = lvNodeListValues.getNode(i).getValue();
					Hashtable lvHMap = FieldSplitter.tokenize(lvFieldnames, lvValues, "|");

					mvHKSPortfolioEnquiryDetails[i].setHashtable(lvHMap);
					mvHKSPortfolioEnquiryDetails[i].setInstrumentId((String) lvHMap.get(INSTRUMENTID));
					// Begin Task: WL00619 Walter Lau 2007 July 27
					mvHKSPortfolioEnquiryDetails[i].setProductID((String) lvHMap.get(PRODUCTID));
					mvHKSPortfolioEnquiryDetails[i].setMarketID((String) lvHMap.get(MARKETID));
					// End Task: WL00619 Walter Lau 2007 July 27
					// mvHKSPortfolioEnquiryDetails[i].setInstrumentShortName((String)
					// lvHMap.get(INSTRUMENTSHORTNAME));
					// mvHKSPortfolioEnquiryDetails[i].setInstrumentChineseShortName((String)
					// lvHMap.get(INSTRUMENTCHINESESHORTNAME));
					// mvHKSPortfolioEnquiryDetails[i].setLedgerQty((String)
					// lvHMap.get(LEDGERQTY));
					mvHKSPortfolioEnquiryDetails[i].setTTodayBuy((String) lvHMap.get(TTODAYBUY));
					mvHKSPortfolioEnquiryDetails[i].setTTodayConfirmBuy((String) lvHMap.get(TTODAYCONFIRMBUY));
					mvHKSPortfolioEnquiryDetails[i].setTTodaySell((String) lvHMap.get(TTODAYSELL));
					mvHKSPortfolioEnquiryDetails[i].setTTodayConfirmSell((String) lvHMap.get(TTODAYCONFIRMSELL));
					mvHKSPortfolioEnquiryDetails[i].setTInactiveBuy((String) lvHMap.get(TINACTIVEBUY));
					mvHKSPortfolioEnquiryDetails[i].setTInactiveSell((String) lvHMap.get(TINACTIVESELL));
					mvHKSPortfolioEnquiryDetails[i].setTradableQty((String) lvHMap.get(TRADABLEQTY));
					mvHKSPortfolioEnquiryDetails[i].setNominalPrice((String) lvHMap.get(NOMINALPRICE));
					mvHKSPortfolioEnquiryDetails[i].setClosingPrice((String) lvHMap.get(CLOSINGPRICE));
					mvHKSPortfolioEnquiryDetails[i].setCostPerShare((String) lvHMap.get(COSTPERSHARE));
					mvHKSPortfolioEnquiryDetails[i].setCostQty((String) lvHMap.get(COSTQTY));
					mvHKSPortfolioEnquiryDetails[i].setMarketValue((String) lvHMap.get(TagName.MARKETVALUE));
					mvHKSPortfolioEnquiryDetails[i].setMarginPercentage((String) lvHMap.get(MARGINPERCENTAGE));
					mvHKSPortfolioEnquiryDetails[i].setMarginValue((String) lvHMap.get(MARGINVALUE));
					mvHKSPortfolioEnquiryDetails[i].setTSettled((String) lvHMap.get(TagName.TSETTLED));
					mvHKSPortfolioEnquiryDetails[i].setTDueBuy((String) lvHMap.get(TagName.TDUEBUY));
					mvHKSPortfolioEnquiryDetails[i].setTDueSell((String) lvHMap.get(TagName.TDUESELL));
					mvHKSPortfolioEnquiryDetails[i].setTUnSettleBuy((String) lvHMap.get(TagName.TUNSETTLEBUY));
					mvHKSPortfolioEnquiryDetails[i].setTUnSettleSell((String) lvHMap.get(TagName.TUNSETTLESELL));
					mvHKSPortfolioEnquiryDetails[i].setTManualHold((String) lvHMap.get(TagName.TMANUALHOLD));
					mvHKSPortfolioEnquiryDetails[i].setTNomineeQty((String) lvHMap.get(TagName.NOMINEEQTY));
					mvHKSPortfolioEnquiryDetails[i].setTCostAmount((String) lvHMap.get(TagName.TCOSTAMOUNT));
					mvHKSPortfolioEnquiryDetails[i].setMvTTodayCostAmount((String) lvHMap.get("TTODAYCOSTAMOUNT"));
					mvHKSPortfolioEnquiryDetails[i].setTCostQty((String) lvHMap.get("TCOSTQTY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTTodayCostQty((String) lvHMap.get("TTODAYCOSTQTY"));
					mvHKSPortfolioEnquiryDetails[i].setTotalAverageCost((String) lvHMap.get("TOTALAVERAGECOST"));
					mvHKSPortfolioEnquiryDetails[i].setTotalCostAmount((String) lvHMap.get("TOTALCOSTAMOUNT"));
					mvHKSPortfolioEnquiryDetails[i].setInstrumentName((String) lvHMap.get("INSTRUMENTNAME"));
					mvHKSPortfolioEnquiryDetails[i].setInstrumentShortName((String) lvHMap.get("INSTRUMENTSHORTNAME"));
					// Begin Task #:- TTL-CN-ZZW-0001 Wind Zhao 20090901
					mvHKSPortfolioEnquiryDetails[i].setMvCurrencyID((String) lvHMap.get("CURRENCYID"));
					// End Task #:- TTL-CN-ZZW-0001 Wind Zhao 20090901

					// BEGIN TASK #:- TTL-GZ-XYL-00038 XuYuLong 20091119 ITrade
					// .R5 VN Adding extra fields
					mvHKSPortfolioEnquiryDetails[i].setMvMktPrice((String) lvHMap.get("MARKETPRICE"));
					mvHKSPortfolioEnquiryDetails[i].setMvHoldAmt((String) lvHMap.get("HOLD"));
					mvHKSPortfolioEnquiryDetails[i].setMvUnitPrice((String) lvHMap.get("UNITPRICE"));
					mvHKSPortfolioEnquiryDetails[i].setMvTValue((String) lvHMap.get("TVALUE"));
					mvHKSPortfolioEnquiryDetails[i].setMvPdBuy((String) lvHMap.get("PENDINGBUY"));
					mvHKSPortfolioEnquiryDetails[i].setMvPdSell((String) lvHMap.get("PENDINGSELL"));
					// END TASK #:- TTL-GZ-XYL-00038 XuYuLong 20091119 ITrade
					// .R5 VN Adding extra fields

					mvHKSPortfolioEnquiryDetails[i].setMvTTodayUnsettleBuy((String) lvHMap.get("TTODAYUNSETTLEBUY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTTodayUnsettleSell((String) lvHMap.get("TTODAYUNSETTLESELL"));

					mvHKSPortfolioEnquiryDetails[i].setMvTT1UnsettleBuy((String) lvHMap.get("TT1UNSETTLEBUY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTT1UnsettleSell((String) lvHMap.get("TT1UNSETTLESELL"));

					mvHKSPortfolioEnquiryDetails[i].setMvTT2UnsettleBuy((String) lvHMap.get("TT2UNSETTLEBUY"));
					mvHKSPortfolioEnquiryDetails[i].setMvTT2UnsettleSell((String) lvHMap.get("TT2UNSETTLESELL"));
					mvHKSPortfolioEnquiryDetails[i].setTSettled((String) lvHMap.get("TSETTLED"));

					/*
					 * //BEGIN TASK #:- TTL-VN VanTran 20100930 ITrade VN Adding
					 * extra fields if(mvPendEntitleList !=null){ HashMap
					 * pendingModel; for (Object pendItem: mvPendEntitleList) {
					 * pendingModel = (HashMap)pendItem; if
					 * (pendingModel.get(TagName
					 * .MARKETID).toString().trim().equals
					 * (lvHMap.get(MARKETID).toString()) &&
					 * pendingModel.get(TagName
					 * .INSTRUMENTID).toString().trim().equals
					 * (lvHMap.get(INSTRUMENTID).toString())){
					 * mvHKSPortfolioEnquiryDetails
					 * [i].setMvTEntitlementQty((String)
					 * pendingModel.get("RECEIVABLEQTY")); break; } } }
					 */

					mvHKSPortfolioEnquiryDetails[i].setMvTPendingEntitlementQty((String) lvHMap.get("TPENDINGENTITLEMENT"));
					mvHKSPortfolioEnquiryDetails[i].setMvTPendingEntitlementCostAmt((String) lvHMap.get("TPENDINGENTITLEMENTCOSTAMT"));

					mvHKSPortfolioEnquiryDetails[i].setMvTAwaitingTraceCert((String) lvHMap.get("TAWAITINGTRADECERT"));

					String awaitDeposit = (lvHMap.get("TAWAITINGDEPOSITCERT") != null) ? (String) lvHMap.get("TAWAITINGDEPOSITCERT") : "0";
					mvHKSPortfolioEnquiryDetails[i].setMvTAwaitingDepositCert(awaitDeposit);

					String awaitWithdrawal = (lvHMap.get("TAWAITINGWITHDRAWALCERT") != null) ? (String) lvHMap.get("TAWAITINGWITHDRAWALCERT") : "0";
					mvHKSPortfolioEnquiryDetails[i].setMvTAwaitingWithdrawalCert(awaitWithdrawal);

					String mortgateQty = (lvHMap.get("TMORTGAGEQTY") != null) ? (String) lvHMap.get("TMORTGAGEQTY") : "0";
					mvHKSPortfolioEnquiryDetails[i].setMvTMortgateQty(mortgateQty);
					// END TASK #:- TTL-VN VanTran 20100930 ITrade VN Adding
					// extra fields

					// BEGIN TASK #:- TTL-VN Giang Tran 20101124 Count ledger
					// quantity
					// Ledger = (Usable + Conf Buy + Pending T+0 Buy + Pending
					// T+1 Buy + Pending T+2 Buy + Due Buy + Await Deposit Cert
					// + Await Trade Cert + Pending Entitlement + Normal Hold +
					// Mortgage hold)
					// pending buy = Pending T+0 Buy + Pending T+1 Buy + Pending
					// T+2 Buy + Due Buy
					// get usable
					BigDecimal usable = (lvHMap.get(TRADABLEQTY) != null && lvHMap.get(TRADABLEQTY).toString().trim().length() > 0) ? new BigDecimal(lvHMap
							.get(TRADABLEQTY).toString().trim()) : new BigDecimal(0);
					// get normalHold
					BigDecimal normalHold = (lvHMap.get(TagName.TMANUALHOLD) != null && lvHMap.get(TagName.TMANUALHOLD).toString().trim().length() > 0) ? new BigDecimal(
							lvHMap.get(TagName.TMANUALHOLD).toString().trim()) : new BigDecimal(0);
					// get mortgage
					BigDecimal mortgage = new BigDecimal(mortgateQty.trim());
					// get await deposit
					BigDecimal awtDeposit = new BigDecimal(awaitDeposit.trim());

					// get await trace
					BigDecimal awaitTradeCert = (lvHMap.get("TAWAITINGTRADECERT") != null && lvHMap.get("TAWAITINGTRADECERT").toString().trim().length() > 0) ? new BigDecimal(
							lvHMap.get("TAWAITINGTRADECERT").toString().trim()) : new BigDecimal(0);
					// Pending Entitlement
					BigDecimal pendingEntilement = (mvHKSPortfolioEnquiryDetails[i].getMvTPendingEntitlementQty() != null && mvHKSPortfolioEnquiryDetails[i]
							.getMvTPendingEntitlementQty().trim().length() > 0) ? new BigDecimal(mvHKSPortfolioEnquiryDetails[i].getMvTPendingEntitlementQty()
							.trim()) : new BigDecimal(0);
					// today Confirm Buy
					BigDecimal todayCfmBuy = (lvHMap.get("TTODAYCONFIRMBUY") != null && lvHMap.get("TTODAYCONFIRMBUY").toString().trim().length() > 0) ? new BigDecimal(
							lvHMap.get("TTODAYCONFIRMBUY").toString().trim()) : new BigDecimal(0);
					// Pending Buy = = Pending T+0 Buy + Pending T+1 Buy +
					// Pending T+2 Buy + Due Buy
					BigDecimal pdBuy = (lvHMap.get("PENDINGBUY") != null && lvHMap.get("PENDINGBUY").toString().trim().length() > 0) ? new BigDecimal(lvHMap
							.get("PENDINGBUY").toString().trim()) : new BigDecimal(0);

					// get ledgerQty
					BigDecimal lvLegderQty = usable.add(todayCfmBuy).add(pdBuy).add(awtDeposit).add(awaitTradeCert).add(pendingEntilement).add(normalHold)
							.add(mortgage);
					mvHKSPortfolioEnquiryDetails[i].setLedgerQty(lvLegderQty.toString());

				}

				IMsgXMLNode lvLoopCounterNode = mvReturnNode.getChildNode(RESULT).getChildNode(LOOPCOUNTER);
				if (lvLoopCounterNode != null) {
					setLoopCounter(lvLoopCounterNode.getValue());
				} else {
					setLoopCounter("0");
				}

			} else { // Unhandled Business Exception
				setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), ""));

				// Handle special cases
				// 1. Agreement is not signed
				if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("HKSFOE00026")) {
					setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode("C_ERROR_DESC")
							.getValue()));
				}

				// 2. When TP is down
				if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
					setErrorCode(new ErrorCode(new String[0], "0100", "No connection with TP"));
				}
			}
			/*
			 * lvRetNode = lvPortfolioEnquiryRequest.send(lvTxnMap);
			 * 
			 * setReturnCode(mvTpError.checkError(lvRetNode)); if (mvReturnCode
			 * != mvTpError.TP_NORMAL) {
			 * setErrorMessage(mvTpError.getErrDesc()); if (mvReturnCode ==
			 * mvTpError.TP_APP_ERR) { setErrorCode(mvTpError.getErrCode()); } }
			 * else { IMsgXMLNode lvNodeFnames =
			 * lvRetNode.getChildNode(RESULT).getChildNode(FIELDNAMES);
			 * IMsgXMLNodeList lvNodeListValues =
			 * lvRetNode.getChildNode(RESULT).
			 * getChildNode(LOOPROWS).getNodeList(VALUES);
			 * 
			 * mvHKSPortfolioEnquiryDetails = new
			 * HKSPortfolioEnquiryDetails[lvNodeListValues.size()];
			 * 
			 * for (int i = 0; i < lvNodeListValues.size(); i++) {
			 * mvHKSPortfolioEnquiryDetails[i] = new
			 * HKSPortfolioEnquiryDetails();
			 * 
			 * String lvFieldnames = lvNodeFnames.getValue(); String lvValues =
			 * lvNodeListValues.getNode(i).getValue(); Hashtable lvHMap =
			 * FieldSplitter.tokenize(lvFieldnames, lvValues, "|");
			 * 
			 * mvHKSPortfolioEnquiryDetails[i].setHashtable(lvHMap);
			 * mvHKSPortfolioEnquiryDetails[i].setInstrumentId((String)
			 * lvHMap.get(INSTRUMENTID)); //Begin Task: WL00619 Walter Lau 2007
			 * July 27 mvHKSPortfolioEnquiryDetails[i].setProductID((String)
			 * lvHMap.get(PRODUCTID));
			 * mvHKSPortfolioEnquiryDetails[i].setMarketID((String)
			 * lvHMap.get(MARKETID)); //End Task: WL00619 Walter Lau 2007 July
			 * 27 //
			 * mvHKSPortfolioEnquiryDetails[i].setInstrumentShortName((String)
			 * lvHMap.get(INSTRUMENTSHORTNAME)); //
			 * mvHKSPortfolioEnquiryDetails[
			 * i].setInstrumentChineseShortName((String)
			 * lvHMap.get(INSTRUMENTCHINESESHORTNAME));
			 * mvHKSPortfolioEnquiryDetails[i].setLedgerQty((String)
			 * lvHMap.get(LEDGERQTY));
			 * mvHKSPortfolioEnquiryDetails[i].setTTodayBuy((String)
			 * lvHMap.get(TTODAYBUY));
			 * mvHKSPortfolioEnquiryDetails[i].setTTodayConfirmBuy((String)
			 * lvHMap.get(TTODAYCONFIRMBUY));
			 * mvHKSPortfolioEnquiryDetails[i].setTTodaySell((String)
			 * lvHMap.get(TTODAYSELL));
			 * mvHKSPortfolioEnquiryDetails[i].setTTodayConfirmSell((String)
			 * lvHMap.get(TTODAYCONFIRMSELL));
			 * mvHKSPortfolioEnquiryDetails[i].setTInactiveBuy((String)
			 * lvHMap.get(TINACTIVEBUY));
			 * mvHKSPortfolioEnquiryDetails[i].setTInactiveSell((String)
			 * lvHMap.get(TINACTIVESELL));
			 * mvHKSPortfolioEnquiryDetails[i].setTradableQty((String)
			 * lvHMap.get(TRADABLEQTY));
			 * mvHKSPortfolioEnquiryDetails[i].setNominalPrice((String)
			 * lvHMap.get(NOMINALPRICE));
			 * mvHKSPortfolioEnquiryDetails[i].setClosingPrice((String)
			 * lvHMap.get(CLOSINGPRICE));
			 * mvHKSPortfolioEnquiryDetails[i].setCostPerShare((String)
			 * lvHMap.get(COSTPERSHARE));
			 * mvHKSPortfolioEnquiryDetails[i].setCostQty((String)
			 * lvHMap.get(COSTQTY));
			 * mvHKSPortfolioEnquiryDetails[i].setMarketValue((String)
			 * lvHMap.get(TagName.MARKETVALUE));
			 * mvHKSPortfolioEnquiryDetails[i].setMarginPercentage((String)
			 * lvHMap.get(MARGINPERCENTAGE));
			 * mvHKSPortfolioEnquiryDetails[i].setMarginValue((String)
			 * lvHMap.get(MARGINVALUE));
			 * mvHKSPortfolioEnquiryDetails[i].setTSettled
			 * ((String)lvHMap.get(TagName.TSETTLED));
			 * mvHKSPortfolioEnquiryDetails
			 * [i].setTDueBuy((String)lvHMap.get(TagName.TDUEBUY));
			 * mvHKSPortfolioEnquiryDetails
			 * [i].setTDueSell((String)lvHMap.get(TagName.TDUESELL));
			 * mvHKSPortfolioEnquiryDetails
			 * [i].setTUnSettleBuy((String)lvHMap.get(TagName.TUNSETTLEBUY));
			 * mvHKSPortfolioEnquiryDetails
			 * [i].setTUnSettleSell((String)lvHMap.get(TagName.TUNSETTLESELL));
			 * mvHKSPortfolioEnquiryDetails
			 * [i].setTManualHold((String)lvHMap.get(TagName.TMANUALHOLD));
			 * mvHKSPortfolioEnquiryDetails[i].setTNomineeQty((String)
			 * lvHMap.get(TagName.NOMINEEQTY));
			 * mvHKSPortfolioEnquiryDetails[i].setTCostAmount
			 * ((String)lvHMap.get(TagName.TCOSTAMOUNT));
			 * mvHKSPortfolioEnquiryDetails
			 * [i].setTCostQty((String)lvHMap.get("TCOSTQTY"));
			 * mvHKSPortfolioEnquiryDetails
			 * [i].setTotalAverageCost((String)lvHMap.get("TOTALAVERAGECOST"));
			 * mvHKSPortfolioEnquiryDetails
			 * [i].setTotalCostAmount((String)lvHMap.get("TOTALCOSTAMOUNT")); }
			 * 
			 * IMsgXMLNode lvLoopCounterNode =
			 * lvRetNode.getChildNode(RESULT).getChildNode(LOOPCOUNTER); if
			 * (lvLoopCounterNode != null) {
			 * setLoopCounter(lvLoopCounterNode.getValue()); } else {
			 * setLoopCounter("0"); }
			 */

		} catch (Throwable e) {
			Log.println(e, Log.ERROR_LOG);
		}
	}

	// END Task #:- al00001 albert lai 20080828

	@SuppressWarnings({ "unchecked" })
	private void processGetPendEntitlementList() {
		Hashtable lvTxnMap = new Hashtable();
		IMsgXMLNode lvNode;

		lvTxnMap.put(CLIENTID, getClientId());
		lvTxnMap.put(TRADINGACCSEQ, getTradingAccSeq());
		HashMap lvModel = null;
		if (TPErrorHandling.TP_NORMAL == process("HKSQueryPendEntitlementEnquiry", lvTxnMap)) {
			IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.CHILD_ROW);
			for (int i = 0; i < lvRowList.size(); i++) {
				lvNode = lvRowList.getNode(i);
				lvModel = new HashMap();
				lvModel.put(TagName.MARKETID, lvNode.getChildNode(TagName.MARKETID).getValue());
				lvModel.put(TagName.INSTRUMENTID, lvNode.getChildNode(TagName.INSTRUMENTID).getValue());
				lvModel.put("RECEIVABLEQTY", lvNode.getChildNode("RECEIVABLEQTY").getValue());

				mvPendEntitleList.add(lvModel);
			}
		} else {
			Log.println("Error error occur when communicate with TP", Log.ACCESS_LOG);
		}
	}

	/**
	 * Get method for Client ID
	 * 
	 * @return Client ID
	 */
	public String getClientId() {
		return mvClientId;
	}

	/**
	 * Set method for Client ID
	 * 
	 * @param Client
	 *            ID the Client ID
	 */
	public void setClientId(String pClientId) {
		mvClientId = pClientId;
	}

	/**
	 * Get method for Trading Account Sequence
	 * 
	 * @return Trading Account Sequence
	 */
	public String getTradingAccSeq() {
		return mvTradingAccSeq;
	}

	/**
	 * Set method for Trading Account Sequence
	 * 
	 * @param pTradingAccSeq
	 *            the Trading Account Sequence
	 */
	public void setTradingAccSeq(String pTradingAccSeq) {
		mvTradingAccSeq = pTradingAccSeq;
	}

	/**
	 * Get method for protfolio with trading
	 * 
	 * @return protfolio with trading
	 */
	public boolean getPortfolioWithTrade() {
		return mvPortfolioWithTrade;
	}

	/**
	 * Set method for protfolio with trading
	 * 
	 * @param pPortfolioWithTrade
	 *            the protfolio with trading
	 */
	public void setPortfolioWithTrade(boolean pPortfolioWithTrade) {
		mvPortfolioWithTrade = pPortfolioWithTrade;
	}

	/**
	 * Get method for Portfolio Enquiry Details
	 * 
	 * @return Array of Portfolio Enquiry Details
	 */
	public HKSPortfolioEnquiryDetails[] getPortfolioEnquiryDetails() {
		return mvHKSPortfolioEnquiryDetails;
	}

	/**
	 * Set method for Portfolio Enquiry Details
	 * 
	 * @param pHKSPortfolioEnquiryDetails
	 *            the Array of Portfolio Enquiry Details
	 */
	public void setPortfolioEnquiryDetails(HKSPortfolioEnquiryDetails[] pHKSPortfolioEnquiryDetails) {
		mvHKSPortfolioEnquiryDetails = pHKSPortfolioEnquiryDetails;
	}

	/**
	 * Get method for Return Code
	 * 
	 * @return Return Code
	 */
	/*
	 * public int getReturnCode() { return mvReturnCode; }
	 */
	/**
	 * Set method for Return Code
	 * 
	 * @param pReturnCode
	 *            the Return Code
	 */
	public void setReturnCode(int pReturnCode) {
		mvReturnCode = pReturnCode;
	}

	/**
	 * Get method for Error Code
	 * 
	 * @return Error Code
	 */
	/*
	 * public String getErrorCode() { return mvErrorCode; }
	 */
	/**
	 * Set method for Error Code
	 * 
	 * @param Error
	 *            Code
	 */
	/*
	 * public void setErrorCode(String pErrorCode) { mvErrorCode = pErrorCode; }
	 */
	/**
	 * Get method for Error Message
	 * 
	 * @return Error Message
	 */
	/*
	 * public String getErrorMessage() { return mvErrorMessage; }
	 */
	/**
	 * Set method for Error Message
	 * 
	 * @param pErrorMessage
	 *            the Error Message
	 */
	public void setErrorMessage(String pErrorMessage) {
		mvErrorMessage = pErrorMessage;
	}

	/**
	 * Get method for Loop Counter
	 * 
	 * @return Loop Counter
	 */
	public int getLoopCounter() {
		int lvCnt;
		try {
			lvCnt = Integer.parseInt(mvLoopCounter);
		} catch (Exception e) {
			return -1;
		}
		return lvCnt;
	}

	/**
	 * Set method for Loop Counter
	 * 
	 * @param pLoopCounter
	 *            the Loop Counter
	 */
	public void setLoopCounter(String pLoopCounter) {
		mvLoopCounter = pLoopCounter;
	}

	// Begin Task #RC00181 - Rice Cheng 20090108
	/**
	 * Get method for user locale
	 * 
	 * @return user locale
	 */
	public String getLanguage() {
		return mvLanguage;
	}

	/**
	 * Set method for user locale
	 * 
	 * @param pLanguage
	 *            the user locale
	 */
	public void setLanguage(String pLanguage) {
		mvLanguage = pLanguage;
	}

	// End Task #RC00181 - Rice Cheng 20090108

	/**
	 * @return the mvAccountType
	 */
	public String getMvAccountType() {
		return mvAccountType;
	}

	/**
	 * @param mvAccountType
	 *            the mvAccountType to set
	 */
	public void setMvAccountType(String mvAccountType) {
		this.mvAccountType = mvAccountType;
	}

	public void setMvIsQueryAdvanceMoney(boolean mvIsQueryAdvanceMoney) {
		this.mvIsQueryAdvanceMoney = mvIsQueryAdvanceMoney;
	}

	public boolean isMvIsQueryAdvanceMoney() {
		return mvIsQueryAdvanceMoney;
	}

	public HKSPortfolioAccountSummaryEqdDetails getMvAccountSummaryEqdDetails() {
		return mvAccountSummaryEqdDetails;
	}

	public void setMvAccountSummaryEqdDetails(HKSPortfolioAccountSummaryEqdDetails mvAccountSummaryEqdDetails) {
		this.mvAccountSummaryEqdDetails = mvAccountSummaryEqdDetails;
	}

	public HKSPortfolioTradingAccSummaryDetails getMvTradingAccSummayDetails() {
		return mvTradingAccSummayDetails;
	}

	public void setMvTradingAccSummayDetails(HKSPortfolioTradingAccSummaryDetails mvTradingAccSummayDetails) {
		this.mvTradingAccSummayDetails = mvTradingAccSummayDetails;
	}
}
