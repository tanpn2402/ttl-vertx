package com.ttl.old.itrade.hks.bean;

/**
 * The HKSGenFundTransferBean class define variables that to save values 
 * for action 
 * @author Wind zhao
 *
 */
public class HKSGenFundTransferBean {
	private String mvDateTime;
	private String mvTransactionType;
	private String mvPrice;
	private String mvErrMsg;
	private String mvPriceDisplay;
	private String mvClientId;
	
	/**
     * This method returns the date time of the transaction.
     * @return the date time of the transaction.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time of the transaction.
     * @param pDateTime The date time of the transaction.
     */
	public void setMvDateTime(String pDateTime) {
		this.mvDateTime = pDateTime;
	}
	
	/**
     * This method returns the transaction type of transaction.
     * @return the transaction type transaction.
     */
	public String getMvTransactionType() {
		return mvTransactionType;
	}
	
	/**
     * This method sets the transaction type of transaction.
     * @param pDateTime The transaction type of transaction.
     */
	public void setMvTransactionType(String pTransactionType) {
		this.mvTransactionType = pTransactionType;
	}
	
	/**
     * This method returns the price of transaction.
     * @return the price of transaction.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price of transaction.
     * @param pPrice The price of transaction.
     */
	public void setMvPrice(String pPrice) {
		this.mvPrice = pPrice;
	}
	
	/**
     * This method returns the error message of transaction.
     * @return the error message of transaction.
     */
	public String getMvErrMsg() {
		return mvErrMsg;
	}
	
	/**
     * This method sets the error message of transaction
     * @param pErrMsg The error message of transaction.
     */
	public void setMvErrMsg(String pErrMsg) {
		this.mvErrMsg = pErrMsg;
	}
	
	/**
     * This method returns the price of transaction.
     * @return the price of transaction is formatted will be display.
     */
	public String getMvPriceDisplay() {
		return mvPriceDisplay;
	}
	
	/**
     * This method sets the price of transaction.
     * @param pPriceDisplay The price of transaction is formatted will be display.
     */
	public void setMvPriceDisplay(String pPriceDisplay) {
		this.mvPriceDisplay = pPriceDisplay;
	}
	
	/**
     * This method returns the client id.
     * @return the client id.
     */
	public String getMvClientId() {
		return mvClientId;
	}
	
	/**
     * This method sets the client id.
     * @param pClientId The client id.
     */
	public void setMvClientId(String pClientId) {
		this.mvClientId = pClientId;
	}
	
}
