package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.winvest.hks.config.mapping.TagName;
/**
 * The HKSEMessageDeleteTxn class defines methodsis that all
 *  Used to delete the e-mail Message
 * 
 * @author not attributable
 *
 */
public class HKSEMessageDeleteTxn extends BaseTxn
{
	/**
	 * This method process  Used to delete the e-mail Message
	 * @param pMessageID the e-mail Messsage ID
	 */
   public void process(String pMessageID)
   {
      Hashtable lvTxnMap = new Hashtable();
      lvTxnMap.put(TagName.MESSAGEID, pMessageID);
      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSEMessageDeleteRequest, lvTxnMap))
      {

      }
   }
}
