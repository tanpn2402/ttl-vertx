package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.winvest.hks.config.mapping.TagName;
/**
 * This class processes the Price Alert Delete
 * @author not attributable
 *
 */
public class HKSPriceAlertDeleteTxn extends BaseTxn
{	
	/**
	 * The method process the  Price Alert Delete
	 * @param pAlertID the alert id
	 */
   public void process(String pAlertID)
   {
      Hashtable lvTxnMap = new Hashtable();
      lvTxnMap.put(TagName.ALERTID, pAlertID);

      if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSPriceAlertDeleteRequest, lvTxnMap))
      {

      }
   }
}
