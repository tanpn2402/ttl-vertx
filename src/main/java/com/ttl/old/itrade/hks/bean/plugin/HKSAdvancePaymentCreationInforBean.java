package com.ttl.old.itrade.hks.bean.plugin;
public class HKSAdvancePaymentCreationInforBean {
	
	private String advAvailable;
	private String t1AdvAvailable;
	private String t2AdvAvailable;
	private String t0AdvAvailable;
	private String advPending;
	private String interestRate;
	private String minInterestRate;
	private String t0Days;
	private String t1Days;
	private String t2Days;
	private String advFee;
	
	
	/**
	 * @return the advAvailable
	 */
	public String getAdvAvailable() {
		return advAvailable;
	}
	/**
	 * @param advAvailable the advAvailable to set
	 */
	public void setAdvAvailable(String advAvailable) {
		this.advAvailable = advAvailable;
	}
	/**
	 * @return the t1AdvAvailable
	 */
	public String getT1AdvAvailable() {
		return t1AdvAvailable;
	}
	/**
	 * @param t1AdvAvailable the t1AdvAvailable to set
	 */
	public void setT1AdvAvailable(String t1AdvAvailable) {
		this.t1AdvAvailable = t1AdvAvailable;
	}
	/**
	 * @return the t2AdvAvailable
	 */
	public String getT2AdvAvailable() {
		return t2AdvAvailable;
	}
	/**
	 * @param t2AdvAvailable the t2AdvAvailable to set
	 */
	public void setT2AdvAvailable(String t2AdvAvailable) {
		this.t2AdvAvailable = t2AdvAvailable;
	}
	/**
	 * @return the t0AdvAvailable
	 */
	public String getT0AdvAvailable() {
		return t0AdvAvailable;
	}
	/**
	 * @param t0AdvAvailable the t0AdvAvailable to set
	 */
	public void setT0AdvAvailable(String t0AdvAvailable) {
		this.t0AdvAvailable = t0AdvAvailable;
	}
	/**
	 * @return the advPending
	 */
	public String getAdvPending() {
		return advPending;
	}
	/**
	 * @param advPending the advPending to set
	 */
	public void setAdvPending(String advPending) {
		this.advPending = advPending;
	}
	/**
	 * @return the interestRate
	 */
	public String getInterestRate() {
		return interestRate;
	}
	/**
	 * @param interestRate the interestRate to set
	 */
	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}
	/**
	 * @return the minInterestRate
	 */
	public String getMinInterestRate() {
		return minInterestRate;
	}
	/**
	 * @param minInterestRate the minInterestRate to set
	 */
	public void setMinInterestRate(String minInterestRate) {
		this.minInterestRate = minInterestRate;
	}
	/**
	 * @return the t0Days
	 */
	public String getT0Days() {
		return t0Days;
	}
	/**
	 * @param t0Days the t0Days to set
	 */
	public void setT0Days(String t0Days) {
		this.t0Days = t0Days;
	}
	/**
	 * @return the t1Days
	 */
	public String getT1Days() {
		return t1Days;
	}
	/**
	 * @param t1Days the t1Days to set
	 */
	public void setT1Days(String t1Days) {
		this.t1Days = t1Days;
	}
	/**
	 * @return the t2Days
	 */
	public String getT2Days() {
		return t2Days;
	}
	/**
	 * @param t2Days the t2Days to set
	 */
	public void setT2Days(String t2Days) {
		this.t2Days = t2Days;
	}
	
	public String getAdvFee() {
		return advFee;
	}
	
	public void setAdvFee(String advFee) {
		this.advFee = advFee;
	}
	
}
