package com.ttl.old.itrade.hks.txn;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
/**
 * The HKSIPOApplStatusEnquiryTxn class entity
 * @author not attributable
 */
public class HKSIPOApplStatusDetails
{
    private String mvEntitlementID;
    private String mvCurrencyID;
    private String mvMinOfferPrice;
    private String mvMaxOfferPrice;
    private String mvConfirmedPrice;
    private String mvProductID;
    private String mvMarketID;
    private String mvInstrumentID;
    private String mvInstrumentName;
    private String mvInstrumentShortName;
    private String mvInstrumentChineseName;
    private String mvInstrumentChineseShortName;
    private String mvSubscriptionID;
    private String mvClientID;
    private String mvTradingAccSeq;
    private String mvAccountSeq;
    private String mvAppliedQty;
    private String mvAppliedBy;
    private String mvAppliedLendingPercentage;
    private String mvAppliedLendingAmount;
    private String mvAppliedAmount;
    private String mvConfirmedQty;
    private String mvConfirmedAmount;
    private String mvUnallottedQty;
    private String mvRefundAmount;
    private String mvSecondRefundAmount;
    private String mvInputDate;
    private String mvStatus;
//    private String mvReverseStatus;
    // BEGIN RN00028 Ricky Ngan 20080816
    private String mvApplicationType;
    private String mvSMSReply;
    private String mvSMSLanguage;
    private String mvForcecal;
    private String mvMarginRefNo;
    private String mvReference;
    private String mvMarginFinancingType;
    private String mvFinancingPlanType;
    private BigDecimal mvDefaultEndingPercentage;
    private BigDecimal mvOverridelEndingPercentage;
    private String mvDefaultInterestRateBasis;
    private String mvOverridedInterestRateBasis;
    private BigDecimal mvDefaultInterestAmount;
    private BigDecimal mvOverrideInterestRate;
    private BigDecimal mvDefaultEndingAmount;
    private BigDecimal mvOverridelEndingAmount;
    private String mvLoanCustomerNumber;
    private BigDecimal mvFinancingFeeAmount;
    private String mvContactEMail;
    private String mvContactNumber;
    private String mvSMSMobileNumber;
    private String mvIDNumber;
    private String mvLastTransferBatchID;
    private Timestamp mvLastTransferDate;
    private BigDecimal mvImportConfirmedQty;
    private BigDecimal mvImportConfirmedAmount;
    private String mvImportConfirmedDate;
    private BigDecimal mvImportRefundAmount;
    private String mvImportRefundDate;
    private BigDecimal mvImporTSecondRefundAmount;
    private String mvImportSecondRefundDate;
    private String mvImportBatchID; 
    private String mvProcessdFlag;
    private BigDecimal mvInputConfirmConsideration;
    private String mvBranchID;
    private String mvPhoneInPIN;
    private String mvState;
    private String mvPendAction;
    private String mvCreatorUserID;
    private Timestamp mvCreationTime;
    private String mvLastModifiedUserID;
    private Timestamp mvLastModifiedTime;
    private String mvLastApproverUserID;
    private Timestamp mvLastApprovalTime;
    private String mvChannelID;
	// END RN00028
    // BEGIN RN00040 Ricky Ngan 20081107
    private String mvAllotmentResultEnable;
    // END RN00040
    /**
     * Get method for instrument chinese short name
     * @return instrument chinese short name
     */
    public String getInstrumentChineseShortName()
    {
        return mvInstrumentChineseShortName;
    }
    /**
     * Get method for currency id
     * @return currency id
     */
    public String getCurrencyID()
    {
        return mvCurrencyID;
    }
    /**
     * Get method for trading account sequence
     * @return trading account sequence
     */
    public String getTradingAccSeq()
    {
        return mvTradingAccSeq;
    }
    /**
     * Get method for market id
     * @return market id
     */
    public String getMarketID()
    {
        return mvMarketID;
    }
    /**
     * Get method for IPO application status
     * @return IPO application status
     */
    public String getStatus()
    {
        return mvStatus;
    }
    /**
     * Get method for subscription id
     * @return subscription id
     */
    public String getSubscriptionID()
    {
        return mvSubscriptionID;
    }
    /**
     * Get method for refund amount
     * @return refund amount
     */
    public String getRefundAmount()
    {
        return mvRefundAmount;
    }
    /**
     * Get method for unalloted quantity
     * @return unalloted quantity
     */
    public String getUnallotedQty()
    {
    	return mvUnallottedQty;
    }
    /**
     * Get method for confirmed quantity
     * @return confirmed quantity
     */
    public String getConfirmedQty()
    {
        return mvConfirmedQty;
    }
    /**
     * Get method for instrument name
     * @return instrument name
     */
    public String getInstrumentName()
    {
        return mvInstrumentName;
    }

//    public String getReverseStatus()
//    {
//        return mvReverseStatus;
//    }
    /**
     * Get method for entitlement id
     * @return entitlement id
     */
    public String getEntitlementID()
    {
        return mvEntitlementID;
    }
    /**
     * Get method for confirmed amount
     * @return confirmed amount
     */
    public String getConfirmedAmount()
    {
        return mvConfirmedAmount;
    }
    /**
     * Get method for instrument short name
     * @return instrument short name
     */
    public String getInstrumentShortName()
    {
        return mvInstrumentShortName;
    }
    /**
     * Get method for account sequence
     * @return account sequence
     */
    public String getAccountSeq()
    {
        return mvAccountSeq;
    }
    /**
     * Get method for applied quantity
     * @return applied quantity
     */
    public String getAppliedQty()
    {
        return mvAppliedQty;
    }
    /**
     * Get method for instrument chinese name
     * @return instrument chinese name
     */
    public String getInstrumentChineseName()
    {
        return mvInstrumentChineseName;
    }
    /**
     * Get method for application amount
     * @return application amount
     */
    public String getAppliedAmount()
    {
        return mvAppliedAmount;
    }
    /**
     * Get method for second refund amount
     * @return second refund amount
     */
    public String getSecondRefundAmount()
    {
        return mvSecondRefundAmount;
    }
    /**
     * Get method for application by
     * @return application by
     */
    public String getAppliedBy()
    {
        return mvAppliedBy;
    }
    /**
     * Get method for input date
     * @return input date
     */
    public String getInputDate()
    {
        return mvInputDate;
    }
    /**
     * Get method for instrument id
     * @return instrument id
     */
    public String getInstrumentID()
    {
        return mvInstrumentID;
    }
    /**
     * Get method for application lending amount
     * @return application lending amount
     */
    public String getAppliedLendingAmount()
    {
        return mvAppliedLendingAmount;
    }
    /**
     * Get method for application lending percentage
     * @return application lending percentage
     */
    public String getAppliedLendingPercentage()
    {
        return mvAppliedLendingPercentage;
    }
    /**
     * Get method for confirmed price
     * @return confirmed price
     */
    public String getConfirmedPrice()
    {
        return mvConfirmedPrice;
    }
    /**
     * Get method for max offer price
     * @return max offer price
     */
    public String getMaxOfferPrice()
    {
        return mvMaxOfferPrice;
    }
    /**
     * Get method for client id
     * @return client id
     */
    public String getClientID()
    {
        return mvClientID;
    }
    /**
     * Get method for small offer price
     * @return small offer price
     */
    public String getMinOfferPrice()
    {
        return mvMinOfferPrice;
    }
    /**
     * Get method for product id
     * @return product id
     */
    public String getProductID()
    {
        return mvProductID;
    }
    /**
     * Set method for instrument chinese short name
     * @param pInstrumentChineseShortName the instrument chinese short name
     */
    public void setInstrumentChineseShortName(String pInstrumentChineseShortName)
    {
        mvInstrumentChineseShortName = pInstrumentChineseShortName;
    }
    /**
     * Set method for currency id
     * @param pCurrencyID the currency id
     */
    public void setCurrencyID(String pCurrencyID)
    {
        mvCurrencyID = pCurrencyID;
    }
    /**
     * Set method for trading account sequence
     * @param pTradingAccSeq the trading account sequence
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
        mvTradingAccSeq = pTradingAccSeq;
    }
    /**
     * Set method for market id
     * @param pMarketID the market id
     */
    public void setMarketID(String pMarketID)
    {
        mvMarketID = pMarketID;
    }
    /**
     * Set method for IPO application status
     * @param pStatus the IPO application status
     */
    public void setStatus(String pStatus)
    {
        mvStatus = pStatus;
    }
    /**
     * Set method for subscription id
     * @param pSubscriptionID the subscription id
     */
    public void setSubscriptionID(String pSubscriptionID)
    {
        mvSubscriptionID = pSubscriptionID;
    }
    /**
     * Set method for refund amount
     * @param pRefundAmount the refund amount
     */
    public void setRefundAmount(String pRefundAmount)
    {
        mvRefundAmount = pRefundAmount;
    }
    /**
     * Set method for unallotted quantity
     * @param pUnallottedQty the unallotted quantity
     */
    public void setUnallottedQty(String pUnallottedQty)
    {
    	mvUnallottedQty = pUnallottedQty;
    }
    /**
     * Set method for confirmed quantity
     * @param pConfirmedQty the confirmed quantity
     */
    public void setConfirmedQty(String pConfirmedQty)
    {
        mvConfirmedQty = pConfirmedQty;
    }
    /**
     * Set method for instrument name
     * @param pInstrumentName the instrument name
     */
    public void setInstrumentName(String pInstrumentName)
    {
        mvInstrumentName = pInstrumentName;
    }

//    public void setReverseStatus(String pReverseStatus)
//    {
//        mvReverseStatus = pReverseStatus;
//    }
    /**
     * Set method for entitlement id
     * @param pEntitlementID the entitlement id
     */
    public void setEntitlementID(String pEntitlementID)
    {
        mvEntitlementID = pEntitlementID;
    }
    /**
     * Set method for confirmed amount
     * @param pConfirmedAmount the confirmed amount
     */
    public void setConfirmedAmount(String pConfirmedAmount)
    {
        mvConfirmedAmount = pConfirmedAmount;
    }
    /**
     * Set method for instrument short name
     * @param pInstrumentShortName the instrument short name
     */
    public void setInstrumentShortName(String pInstrumentShortName)
    {
        mvInstrumentShortName = pInstrumentShortName;
    }
    /**
     * Set method for account sequence
     * @param pAccountSeq the account sequence
     */
    public void setAccountSeq(String pAccountSeq)
    {
        mvAccountSeq = pAccountSeq;
    }
    /**
     * Set method for application quantity
     * @param pAppliedQty the application quantity
     */
    public void setAppliedQty(String pAppliedQty)
    {
        mvAppliedQty = pAppliedQty;
    }
    /**
     * Set method for instrument chinese name
     * @param pInstrumentChineseName the instrument chinese name
     */
    public void setInstrumentChineseName(String pInstrumentChineseName)
    {
        mvInstrumentChineseName = pInstrumentChineseName;
    }
    /**
     * Set method for application amount
     * @param pAppliedAmount the application amount
     */
    public void setAppliedAmount(String pAppliedAmount)
    {
        mvAppliedAmount = pAppliedAmount;
    }
    /**
     * Set method for second refund amount
     * @param pSecondRefundAmount the second refund amount
     */
    public void setSecondRefundAmount(String pSecondRefundAmount)
    {
        mvSecondRefundAmount = pSecondRefundAmount;
    }
    /**
     * Set method for application by
     * @param pAppliedBy the application by 
     */
    public void setAppliedBy(String pAppliedBy)
    {
        mvAppliedBy = pAppliedBy;
    }
    /**
     * Set method for input date
     * @param pInputDate the input date
     */
    public void setInputDate(String pInputDate)
    {
        mvInputDate = pInputDate;
    }
    /**
     * Set method for instrument id
     * @param pInstrumentID the instrument id
     */
    public void setInstrumentID(String pInstrumentID)
    {
        mvInstrumentID = pInstrumentID;
    }
    /**
     * Set method for application lending amount
     * @param pAppliedLendingAmount the application lending amount
     */
    public void setAppliedLendingAmount(String pAppliedLendingAmount)
    {
        mvAppliedLendingAmount = pAppliedLendingAmount;
    }
    /**
     * Set method for application lending percentage
     * @param pAppliedLendingPercentage the application lending percentage
     */
    public void setAppliedLendingPercentage(String pAppliedLendingPercentage)
    {
        mvAppliedLendingPercentage = pAppliedLendingPercentage;
    }
    /**
     * Set method for confirmed price
     * @param pConfirmedPrice the confirmed price
     */
    public void setConfirmedPrice(String pConfirmedPrice)
    {
        mvConfirmedPrice = pConfirmedPrice;
    }
    /**
     * Set method for max offer price
     * @param pMaxOfferPrice the max offer price
     */
    public void setMaxOfferPrice(String pMaxOfferPrice)
    {
        mvMaxOfferPrice = pMaxOfferPrice;
    }
    /**
     * Set method for client id
     * @param pClientID the client id
     */
    public void setClientID(String pClientID)
    {
        mvClientID = pClientID;
    }
    /**
     * Set method for small offer price
     * @param pMinOfferPrice the small offer price
     */
    public void setMinOfferPrice(String pMinOfferPrice)
    {
        mvMinOfferPrice = pMinOfferPrice;
    }
    /**
     * Set method for product id
     * @param pProductID the product id
     */
    public void setProductID(String pProductID)
    {
        mvProductID = pProductID;
    }
    // BEGIN RN00028 Ricky Ngan 20080816
    /**
     * Get method for application type
     * @return application type
     */
	public String getApplicationType() {
		return mvApplicationType;
	}
	/**
	 * Set method for application type
	 * @param pApplicationType the application type
	 */
	public void setApplicationType(String pApplicationType) {
		mvApplicationType = pApplicationType;
	}
	/**
	 * Get method for system message reply
	 * @return system message reply
	 */
	public String getSMSReply() {
		return mvSMSReply;
	}
	/**
	 * Set method for system message reply
	 * @param pReply the  system message reply
	 */
	public void setSMSReply(String pReply) {
		mvSMSReply = pReply;
	}
	/**
	 * Set method for system message language
	 * @return system message language
	 */
	public String getSMSLanguage() {
		return mvSMSLanguage;
	}
	/**
	 * Set method for system message language
	 * @param pSMSLanguage the system message language
	 */
	public void setSMSLanguage(String pSMSLanguage) {
		mvSMSLanguage = pSMSLanguage;
	}
	/**
	 * Get method for force cancel
	 * @return force cancel
	 */
	public String getForcecal() {
		return mvForcecal;
	}
	/**
	 * Set method for force cancel
	 * @param pForcecal the force cancel
	 */
	public void setForcecal(String pForcecal) {
		mvForcecal = pForcecal;
	}
	/**
	 * Get method for margin reference no
	 * @return margin reference no
	 */
	public String getMarginRefNo() {
		return mvMarginRefNo;
	}
	/**
	 * Set method for margin reference no
	 * @param pMarginRefNo the margin reference no
	 */
	public void setMarginRefNo(String pMarginRefNo) {
		mvMarginRefNo = pMarginRefNo;
	}
	/**
	 * Get method for reference
	 * @return reference
	 */
	public String getReference() {
		return mvReference;
	}
	/**
	 * Set method for reference
	 * @param pReference the reference
	 */
	public void setReference(String pReference) {
		mvReference = pReference;
	}
	/**
	 * Get method for margin financing type
	 * @return margin financing type
	 */
	public String getMarginFinancingType() {
		return mvMarginFinancingType;
	}
	/**
	 * Set method for margin financing type
	 * @param pMarginFinancingType the margin financing type
	 */
	public void setMarginFinancingType(String pMarginFinancingType) {
		mvMarginFinancingType = pMarginFinancingType;
	}
	/**
	 * Get method for financing plan type
	 * @return financing plan type
	 */
	public String getFinancingPlanType() {
		return mvFinancingPlanType;
	}
	/**
	 * Set method for financing plan type
	 * @param pFinancingPlanType the financing plan type
	 */
	public void setFinancingPlanType(String pFinancingPlanType) {
		mvFinancingPlanType = pFinancingPlanType;
	}
	/**
	 * Get method for default ending percentage
	 * @return default ending percentage
	 */
	public BigDecimal getDefaultEndingPercentage() {
		return mvDefaultEndingPercentage;
	}
	/**
	 * Set method for default ending percentage
	 * @param pDefaultEndingPercentage the default ending percentage
	 */
	public void setDefaultEndingPercentage(BigDecimal pDefaultEndingPercentage) {
		mvDefaultEndingPercentage = pDefaultEndingPercentage;
	}
	/**
	 * Get method for override ending percentage
	 * @return override ending percentage
	 */
	public BigDecimal getOverridelEndingPercentage() {
		return mvOverridelEndingPercentage;
	}
	/**
	 * Set method for override ending percentage
	 * @param pOverridelEndingPercentage the override ending percentage
	 */
	public void setOverridelEndingPercentage(BigDecimal pOverridelEndingPercentage) {
		mvOverridelEndingPercentage = pOverridelEndingPercentage;
	}
	/**
	 * Get method for default interest rate basis
	 * @return default interest rate basis
	 */
	public String getDefaultInterestRateBasis() {
		return mvDefaultInterestRateBasis;
	}
	/**
	 * Set method for default interest rate basis
	 * @param pDefaultInterestRateBasis the default interest rate basis
	 */
	public void setDefaultInterestRateBasis(String pDefaultInterestRateBasis) {
		mvDefaultInterestRateBasis = pDefaultInterestRateBasis;
	}
	/**
	 * Get method for override interest rate basis
	 * @return override interest rate basis
	 */
	public String getOverridedInterestRateBasis() {
		return mvOverridedInterestRateBasis;
	}
	/**
	 * Set method for override interest rate basis
	 * @param pOverridedInterestRateBasis the override interest rate basis
	 */
	public void setOverridedInterestRateBasis(String pOverridedInterestRateBasis) {
		mvOverridedInterestRateBasis = pOverridedInterestRateBasis;
	}
	/**
	 * Get method for default interest amount
	 * @return default interest amount
	 */
	public BigDecimal getDefaultInterestAmount() {
		return mvDefaultInterestAmount;
	}
	/**
	 * Set method for default interest amount
	 * @param pDefaultInterestAmount the default interest amount
	 */
	public void setDefaultInterestAmount(BigDecimal pDefaultInterestAmount) {
		mvDefaultInterestAmount = pDefaultInterestAmount;
	}
	/**
	 * Get method for override interest rate
	 * @return override interest rate
	 */
	public BigDecimal getOverrideInterestRate() {
		return mvOverrideInterestRate;
	}
	/**
	 * Set method for override interest rate
	 * @param pOverrideInterestRate the override interest rate
	 */
	public void setOverrideInterestRate(BigDecimal pOverrideInterestRate) {
		mvOverrideInterestRate = pOverrideInterestRate;
	}
	/**
	 * Get method for default ending amount 
	 * @return default ending amount 
	 */
	public BigDecimal getDefaultEndingAmount() {
		return mvDefaultEndingAmount;
	}
	/**
	 * Set method for default ending amount 
	 * @param pDefaultEndingAmount the default ending amount 
	 */
	public void setDefaultEndingAmount(BigDecimal pDefaultEndingAmount) {
		mvDefaultEndingAmount = pDefaultEndingAmount;
	}
	/**
	 * Get method for override ending amount
	 * @return override ending amount
	 */
	public BigDecimal getOverridelEndingAmount() {
		return mvOverridelEndingAmount;
	}
	/**
	 * Set method for override ending amount
	 * @param pOverridelEndingAmount the override ending amount
	 */
	public void setOverridelEndingAmount(BigDecimal pOverridelEndingAmount) {
		mvOverridelEndingAmount = pOverridelEndingAmount;
	}
	/**
	 * Get method for loan customer number
	 * @return loan customer number
	 */
	public String getLoanCustomerNumber() {
		return mvLoanCustomerNumber;
	}
	/**
	 * Set method for loan customer number
	 * @param pLoanCustomerNumber the loan customer number
	 */
	public void setLoanCustomerNumber(String pLoanCustomerNumber) {
		mvLoanCustomerNumber = pLoanCustomerNumber;
	}
	/**
	 * Get method for financing fee amount 
	 * @return financing fee amount 
	 */
	public BigDecimal getFinancingFeeAmount() {
		return mvFinancingFeeAmount;
	}
	/**
	 * Set method for financing fee amount 
	 * @param pFinancingFeeAmount the financing fee amount 
	 */
	public void setFinancingFeeAmount(BigDecimal pFinancingFeeAmount) {
		mvFinancingFeeAmount = pFinancingFeeAmount;
	}
	/**
	 * Get method for contact email
	 * @return contact email
	 */
	public String getContactEMail() {
		return mvContactEMail;
	}
	/**
	 * Set method for contact email
	 * @param pContactEMail the contact email
	 */
	public void setContactEMail(String pContactEMail) {
		mvContactEMail = pContactEMail;
	}
	/**
	 * Get method for contact number
	 * @return contact number
	 */
	public String getContactNumber() {
		return mvContactNumber;
	}
	/**
	 * Set method for contact number
	 * @param pContactNumber the contact number
	 */
	public void setContactNumber(String pContactNumber) {
		mvContactNumber = pContactNumber;
	}
	/**
	 * Get method for system message moving number
	 * @return system message moving number
	 */
	public String getSMSMobileNumber() {
		return mvSMSMobileNumber;
	}
	/**
	 * Set method for system message moving number
	 * @param pMobileNumber the system message moving number
	 */
	public void setSMSMobileNumber(String pMobileNumber) {
		mvSMSMobileNumber = pMobileNumber;
	}
	/**
	 * Get method for id number
	 * @return id number
	 */
	public String getIDNumber() {
		return mvIDNumber;
	}
	/**
	 * Set method for id number
	 * @param pIDNumber the id number
	 */
	public void setIDNumber(String pIDNumber) {
		mvIDNumber = pIDNumber;
	}
	/**
	 * Get method for last transfer batch id
	 * @return last transfer batch id
	 */
	public String getLastTransferBatchID() {
		return mvLastTransferBatchID;
	}
	/**
	 * Set method for last transfer batch id
	 * @param pLastTransferBatchID the last transfer batch id
	 */
	public void setLastTransferBatchID(String pLastTransferBatchID) {
		mvLastTransferBatchID = pLastTransferBatchID;
	}
	/**
	 * Get method for late transfer date
	 * @return late transfer date
	 */
	public Timestamp getLastTransferDate() {
		return mvLastTransferDate;
	}
	/**
	 * Set method for late transfer date
	 * @param pLastTransferDate the late transfer date
	 */
	public void setLastTransferDate(Timestamp pLastTransferDate) {
		mvLastTransferDate = pLastTransferDate;
	}
	/**
	 * Get method for import confirmed quantity 
	 * @return import confirmed quantity 
	 */
	public BigDecimal getImportConfirmedQty() {
		return mvImportConfirmedQty;
	}
	/**
	 * Set method for import confirmed quantity 
	 * @param pImportConfirmedQty the import confirmed quantity 
	 */
	public void setImportConfirmedQty(BigDecimal pImportConfirmedQty) {
		mvImportConfirmedQty = pImportConfirmedQty;
	}
	/**
	 * Get method for import confirmed amount
	 * @return import confirmed amount
	 */
	public BigDecimal getImportConfirmedAmount() {
		return mvImportConfirmedAmount;
	}
	/**
	 * Set method for import confirmed amount
	 * @param pImportConfirmedAmount the for import confirmed amount
	 */
	public void setImportConfirmedAmount(BigDecimal pImportConfirmedAmount) {
		mvImportConfirmedAmount = pImportConfirmedAmount;
	}
	/**
	 * Get method for import confirmed date
	 * @return import confirmed date
	 */
	public String getImportConfirmedDate() {
		return mvImportConfirmedDate;
	}
	/**
	 * Set method for import confirmed date
	 * @param pImportConfirmedDate the import confirmed date
	 */
	public void setImportConfirmedDate(String pImportConfirmedDate) {
		mvImportConfirmedDate = pImportConfirmedDate;
	}
	/**
	 * Get method for import refund amount 
	 * @return import refund amount
	 */
	public BigDecimal getImportRefundAmount() {
		return mvImportRefundAmount;
	}
	/**
	 * Set method for import refund amount 
	 * @param pImportRefundAmount the import refund amount 
	 */
	public void setImportRefundAmount(BigDecimal pImportRefundAmount) {
		mvImportRefundAmount = pImportRefundAmount;
	}
	/**
	 * Get method for import refund date 
	 * @return import refund date 
	 */
	public String getImportRefundDate() {
		return mvImportRefundDate;
	}
	/**
	 * Set method for import refund date 
	 * @param pImportRefundDate the import refund date 
	 */
	public void setImportRefundDate(String pImportRefundDate) {
		mvImportRefundDate = pImportRefundDate;
	}
	/**
	 * Get method for import trading second refund amount
	 * @return import trading second refund amount
	 */
	public BigDecimal getImporTSecondRefundAmount() {
		return mvImporTSecondRefundAmount;
	}
	/**
	 * Set method for import trading second refund amount
	 * @param pImporTSecondRefundAmount the import trading second refund amount
	 */
	public void setImporTSecondRefundAmount(BigDecimal pImporTSecondRefundAmount) {
		mvImporTSecondRefundAmount = pImporTSecondRefundAmount;
	}
	/**
	 * Get method for import trading second refund date 
	 * @return import trading second refund date 
	 */
	public String getImportSecondRefundDate() {
		return mvImportSecondRefundDate;
	}
	/**
	 * Set method for import trading second refund date
	 * @param pImportSecondRefundDate the import trading second refund date
	 */
	public void setImportSecondRefundDate(String pImportSecondRefundDate) {
		mvImportSecondRefundDate = pImportSecondRefundDate;
	}
	/**
	 * Get method for import batch id
	 * @return import batch id
	 */
	public String getImportBatchID() {
		return mvImportBatchID;
	}
	/**
	 * Set method for import batch id
	 * @param pImportBatchID the import batch id
	 */
	public void setImportBatchID(String pImportBatchID) {
		mvImportBatchID = pImportBatchID;
	}
	/**
	 * Get method for process flag
	 * @return process flag
	 */
	public String getProcessdFlag() {
		return mvProcessdFlag;
	}
	/**
	 * Set method for process flag
	 * @param pProcessdFlag the process flag
	 */
	public void setProcessdFlag(String pProcessdFlag) {
		mvProcessdFlag = pProcessdFlag;
	}
	/**
	 * Get method for input confirm consideration 
	 * @return input confirm consideration 
	 */
	public BigDecimal getInputConfirmConsideration() {
		return mvInputConfirmConsideration;
	}
	/**
	 * Set method for input confirm consideration 
	 * @param pInputConfirmConsideration the input confirm consideration 
	 */
	public void setInputConfirmConsideration(BigDecimal pInputConfirmConsideration) {
		mvInputConfirmConsideration = pInputConfirmConsideration;
	}
	/**
	 * Get method for branch id
	 * @return branch id
	 */
	public String getBranchID() {
		return mvBranchID;
	}
	/**
	 * Set method for branch id
	 * @param pBranchID the branch id
	 */
	public void setBranchID(String pBranchID) {
		mvBranchID = pBranchID;
	}
	/**
	 * Get method for phone in PIN
	 * @return phone in PIN
	 */
	public String getPhoneInPIN() {
		return mvPhoneInPIN;
	}
	/**
	 * Set method for phone in PIN
	 * @param pPhoneInPIN the phone in PIN
	 */
	public void setPhoneInPIN(String pPhoneInPIN) {
		mvPhoneInPIN = pPhoneInPIN;
	}
	/**
	 * Get method for state
	 * @return state
	 */
	public String getState() {
		return mvState;
	}
	/**
	 * Set method for state
	 * @param pState the state
	 */
	public void setState(String pState) {
		mvState = pState;
	}
	/**
	 * Get method for pend action
	 * @return pend action
	 */
	public String getPendAction() {
		return mvPendAction;
	}
	/**
	 * Set method for pend action
	 * @param pPendAction the pend action
	 */
	public void setPendAction(String pPendAction) {
		mvPendAction = pPendAction;
	}
	/**
	 * Get method for creator user id
	 * @return creator user id
	 */
	public String getCreatorUserID() {
		return mvCreatorUserID;
	}
	/**
	 * Set method for creator user id
	 * @param pCreatorUserID the creator user id
	 */
	public void setCreatorUserID(String pCreatorUserID) {
		mvCreatorUserID = pCreatorUserID;
	}
	/**
	 * Get method for creation time
	 * @return creation time
	 */
	public Timestamp getCreationTime() {
		return mvCreationTime;
	}
	/**
	 * Set method for creation time
	 * @param pCreationTime the creation time
	 */
	public void setCreationTime(Timestamp pCreationTime) {
		mvCreationTime = pCreationTime;
	}
	/**
	 * Get method for last modified user id
	 * @return last modified user id
	 */
	public String getLastModifiedUserID() {
		return mvLastModifiedUserID;
	}
	/**
	 * Set method for last modified user id
	 * @param pLastModifiedUserID the last modified user id
	 */
	public void setLastModifiedUserID(String pLastModifiedUserID) {
		mvLastModifiedUserID = pLastModifiedUserID;
	}
	/**
	 * Get method for last modified time
	 * @return last modified time
	 */
	public Timestamp getLastModifiedTime() {
		return mvLastModifiedTime;
	}
	/**
	 * Set method for last modified time
	 * @param pLastModifiedTime the last modified time
	 */
	public void setLastModifiedTime(Timestamp pLastModifiedTime) {
		mvLastModifiedTime = pLastModifiedTime;
	}
	/**
	 * Get method for last approver user id
	 * @return last approver user id
	 */
	public String getLastApproverUserID() {
		return mvLastApproverUserID;
	}
	/**
	 * Set method for last approver user id
	 * @param pLastApproverUserID the last approver user id
	 */
	public void setLastApproverUserID(String pLastApproverUserID) {
		mvLastApproverUserID = pLastApproverUserID;
	}
	/**
	 * Get method for last approval time
	 * @return last approval time
	 */
	public Timestamp getLastApprovalTime() {
		return mvLastApprovalTime;
	}
	/**
	 * Set method for last approval time
	 * @param pLastApprovalTime the last approval time
	 */
	public void setLastApprovalTime(Timestamp pLastApprovalTime) {
		mvLastApprovalTime = pLastApprovalTime;
	}
	/**
	 * Get method for channel id
	 * @return channel id
	 */
	public String getChannelID() {
		return mvChannelID;
	}
	/**
	 * Set method for channel id
	 * @param pChannelID the channel id
	 */
	public void setChannelID(String pChannelID) {
		mvChannelID = pChannelID;
	}
	// END RN00028

	// BEGIN RN00040 Ricky Ngan 20081107
	/**
	 * Get method for allotment result enable
	 * @return allotment result enable
	 */
	public String getAllotmentResultEnable() {
		return mvAllotmentResultEnable;
	}
	/**
	 * Set method for allotment result enable
	 * @param pAllotmentResultEnable the allotment result enable
	 */
	public void setAllotmentResultEnable(String pAllotmentResultEnable) {
		mvAllotmentResultEnable = pAllotmentResultEnable;
	}
	// END RN00040
}
