package com.ttl.old.itrade.hks.util;

import com.ttl.old.itrade.util.ErrorCode;

public class CommonValidation {
	
	public CommonValidation() {
		
	}
	
	/**
	 * function validation email address
	 * @param email
	 * @return
	 */
	public ErrorCode validateEmail(String email){
		
		ErrorCode error = null;
		
		String at="@";
		String dot=".";
		int lat=email.indexOf(at);
		int lemail=email.length();
		int ldot=email.indexOf(dot);
		
		if(email == null || email.trim().length() == 0) {
			error = new ErrorCode(new String[0], HKSErrorCode.HKSValidateEmailNotExist, HKSErrorCode.HKSValidateEmailNotExist);
			return error;
		}
		
		if (email.indexOf(at)==-1){
			error = new ErrorCode(new String[0], HKSErrorCode.HKSValidateEmailInvalid, HKSErrorCode.HKSValidateEmailInvalid);
			return error;
		}

		if (email.indexOf(at)==-1 || email.indexOf(at)==0 || email.indexOf(at)==lemail){
			error = new ErrorCode(new String[0], HKSErrorCode.HKSValidateEmailInvalid, HKSErrorCode.HKSValidateEmailInvalid);
			return error;
		}

		if (email.indexOf(dot)==-1 || email.indexOf(dot)==0 || email.indexOf(dot)==lemail){
			error = new ErrorCode(new String[0], HKSErrorCode.HKSValidateEmailInvalid, HKSErrorCode.HKSValidateEmailInvalid);
			return error;
		}

		 if (email.indexOf(at,(lat+1))!=-1){
			 error = new ErrorCode(new String[0], HKSErrorCode.HKSValidateEmailInvalid, HKSErrorCode.HKSValidateEmailInvalid);
			return error;
		 }

		 if (email.substring(lat-1,lat)==dot || email.substring(lat+1,lat+2)==dot){
			 error = new ErrorCode(new String[0], HKSErrorCode.HKSValidateEmailInvalid, HKSErrorCode.HKSValidateEmailInvalid);
				return error;
		 }

		 if (email.indexOf(dot,(lat+2))==-1){
			 error = new ErrorCode(new String[0], HKSErrorCode.HKSValidateEmailInvalid, HKSErrorCode.HKSValidateEmailInvalid);
				return error;
		 }
		
		 if (email.indexOf(" ")!=-1){
			 error = new ErrorCode(new String[0], HKSErrorCode.HKSValidateEmailInvalid, HKSErrorCode.HKSValidateEmailInvalid);
				return error;
		 }
		
		
		return error;
	}
	
	/**
	 * function validation phone number
	 * @param phone
	 * @return
	 */
	public ErrorCode validatePhone(String phone){
		
		ErrorCode error = null;
		
		if(phone == null || phone.trim().length() == 0) {
			error = new ErrorCode(new String[0], HKSErrorCode.HKSValidatePhoneNotExist, HKSErrorCode.HKSValidatePhoneNotExist);
			return error;
		}
		
		try {  
		     Integer.parseInt(phone);  
		}  
		catch( Exception ex ) {  
			error = new ErrorCode(new String[0], HKSErrorCode.HKSValidatePhoneInvalid, HKSErrorCode.HKSValidatePhoneInvalid);
			return error;
		} 
		
		return error;
	}
	
}