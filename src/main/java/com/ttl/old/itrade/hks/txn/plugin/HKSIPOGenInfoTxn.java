package com.ttl.old.itrade.hks.txn.plugin;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.util.HKSInstrument;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

/**
 * The HKSIPOGenInfoTxn class definition for all method
 * From the host to obtain all the information listed
 * @author not attributable
 *
 */
public class HKSIPOGenInfoTxn
{

	private String mvClientId;
	// BEGIN RN00028 Ricky Ngan 20080816
	private String mvMarginFinancingType;
	// END RN00028
	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;
	// BEGIN Task: RN00026 Ricky Ngan 200806020
	private String mvDayAfterAllotment;
	// END Task: RN00026
	private String mvlsMarginEnable;

	private HKSIPOGenInfoDetails[]   mvHKSIPOGenInfoDetails;
	private Vector HKSAppliedEntitlementID = new Vector();
	//Variable is used to identify
	public static final String CLIENTID = "CLIENTID";
	public static final String INSTRUMENTID = "INSTRUMENTID";
	public static final String ENTITLEMENTID = "ENTITLEMENTID";
	public static final String STATUS = "STATUS";
	public static final String LISTINGDATE = "LISTINGDATE";
	public static final String MINOFFERPRICE = "MINOFFERPRICE";
	public static final String MAXOFFERPRICE = "MAXOFFERPRICE";
	public static final String CONFIRMEDPRICE = "CONFIRMEDPRICE";
	// BEGIN Task: RN00026 Ricky Ngan 200806020
	public static final String DAYAFTERALLOTMENT = "DAYAFTERALLOTMENT";
	// END Task: RN00026
	public static final String EIPOOPENINGLIST = "EIPOOPENINGLIST";
	public static final String LOOP = "LOOP";
	public static final String LOOP_ELEMENT = "LOOP_ELEMENT";
	
	public static final String MARGIN_WEB_ENABLE = "MARGIN_WEB_ENABLE";
	// BEGIN RN00042 Ricky Ngan 20081111
	public static final String INDIVIDUAL_IPO_WEB_ENABLE = "INDIVIDUAL_IPO_WEB_ENABLE";
	public static final String INDIVIDUAL_MARGIN_WEB_ENABLE = "INDIVIDUAL_MARGIN_WEB_ENABLE";
	// END
	//Begin Task: CL00070 Charlie Lau 20090204
	public static final String INSTRUMENTSHORTNAME = "INSTRUMENTSHORTNAME";
	public static final String INSTRUMENTCHINESESHORTNAME = "INSTRUMENTCHINESESHORTNAME";
	public static String mvMarketID = "HKEX";
	// End Task: CL00070
	/**
	 * This variable is used to the tp Error
	 */
	TPErrorHandling				tpError;
	Presentation presentation = null;

	/**
	 * Default constructor for HKSIPOGenInfoTxn class
	 * @param pPresentation the Presentation class
	 */
	public HKSIPOGenInfoTxn(Presentation pPresentation)
	{
		tpError = new TPErrorHandling();
		presentation = pPresentation;
	}
	/**
	 * Default constructor for HKSIPOGenInfoTxn class
	 * @param pPresentation the Presentation class
	 * @param pClientId the client id
	 * @param pDayAfterAllotment the day after allotment
	 */
	public HKSIPOGenInfoTxn(Presentation pPresentation, String pClientId, String pDayAfterAllotment)
	{
		this(pPresentation);
		setClientId(pClientId);
		setDayAfterAllotment(pDayAfterAllotment);
	}
	/**
	 *  Default constructor for HKSIPOGenInfoTxn class
	 * @param pPresentation the Presentation class
	 * @param pClientId the client id
	 * @param pMarginFinancingType the margin financing type
	 * @param pDayAfterAllotment the day after allotment
	 */
	// BEGIN RN00028 Ricky Ngan 20080816
	public HKSIPOGenInfoTxn(Presentation pPresentation, String pClientId, String pMarginFinancingType, String pDayAfterAllotment)
	{
		this(pPresentation);
		setClientId(pClientId);
		setMarginFinancingType(pMarginFinancingType);
		setDayAfterAllotment(pDayAfterAllotment);
	}
	// END RN00028
	//Begin Task: CL00070 Charlie Lau 20090204
	/**
	 * Set method for market id
	 * @param market id 
	 */
	public void setMarketID(String pMarketID){
		mvMarketID = pMarketID;
	}
	// End Task: CL00070
	/**
	 * This method process From the host to obtain all the information listed
	 * @param pPresentation the Presentation class
	 */
	public void process(Presentation pPresentation)
	{
		try
		{
			Hashtable		lvTxnMap = new Hashtable();

			IMsgXMLNode		lvRetNode;
			TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSIPOOpeningListEnquiry);
			TPBaseRequest   lvIPOOpeningListEnquiry = ivTPManager.getRequest(RequestName.HKSIPOOpeningListEnquiry);

			lvTxnMap.put(CLIENTID, getClientId());
			//BEGIN Task: RN00026 Ricky Ngan 20080620
			lvTxnMap.put(DAYAFTERALLOTMENT, getDayAfterAllotment());
			//END Task: RN00026
			// BEGIN RN00028 Ricky Ngan 20080816
			if(getMarginFinancingType() != null)
				lvTxnMap.put("MARGINFINANCINGTYPE", getMarginFinancingType());
			// END RN00028

			lvRetNode = lvIPOOpeningListEnquiry.send(lvTxnMap);

			setReturnCode(tpError.checkError(lvRetNode));
			if (mvReturnCode != TPErrorHandling.TP_NORMAL)
			{
				setErrorMessage(tpError.getErrDesc());
				if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
				{
					setErrorCode(tpError.getErrCode());
				}
			}
			else
			{
				//Begin Task #: - TTL-GZ-ZZW-00026 Wind Zhao 20100205 [iTrade R5] Merge IPO and IPO margin
				//if(lvRetNode.getChildNode(MARGIN_WEB_ENABLE).getValue() != null)
					//mvlsMarginEnable =  lvRetNode.getChildNode(MARGIN_WEB_ENABLE).getValue();
				//End Task #: - TTL-GZ-ZZW-00026 Wind Zhao 20100205 [iTrade R5] Merge IPO and IPO margin
			
				IMsgXMLNodeList lvOpeningNodeList = lvRetNode.getChildNode(EIPOOPENINGLIST).getChildNode(LOOP).getNodeList(LOOP_ELEMENT);
				if (lvOpeningNodeList == null || lvOpeningNodeList.size() == 0)
				{
					mvHKSIPOGenInfoDetails = new HKSIPOGenInfoDetails[0];
					return;
				}
				int lvSize = lvOpeningNodeList.size();
				mvHKSIPOGenInfoDetails = new HKSIPOGenInfoDetails[lvSize];
			
				for (int i = 0; i < lvSize; i++)
				{	
					mvHKSIPOGenInfoDetails[i] = new HKSIPOGenInfoDetails();

					//Begin Task #: - TTL-GZ-ZZW-00026 Wind Zhao 20100205 [iTrade R5] Merge IPO and IPO margin
					mvHKSIPOGenInfoDetails[i].setMvMarginEnable(lvOpeningNodeList.getNode(i).getChildNode(MARGIN_WEB_ENABLE).getValue());
					//End Task #: - TTL-GZ-ZZW-00026 Wind Zhao 20100205 [iTrade R5] Merge IPO and IPO margin
					
					mvHKSIPOGenInfoDetails[i].setInstrumentID(lvOpeningNodeList.getNode(i).getChildNode(INSTRUMENTID).getValue());
					mvHKSIPOGenInfoDetails[i].setEntitlementID(lvOpeningNodeList.getNode(i).getChildNode(ENTITLEMENTID).getValue());
					mvHKSIPOGenInfoDetails[i].setStatus(lvOpeningNodeList.getNode(i).getChildNode(STATUS).getValue());
					mvHKSIPOGenInfoDetails[i].setListingDate(lvOpeningNodeList.getNode(i).getChildNode(LISTINGDATE).getValue());
					mvHKSIPOGenInfoDetails[i].setMinOfferPrice(lvOpeningNodeList.getNode(i).getChildNode(MINOFFERPRICE).getValue());
					mvHKSIPOGenInfoDetails[i].setMaxOfferPrice(lvOpeningNodeList.getNode(i).getChildNode(MAXOFFERPRICE).getValue());
					mvHKSIPOGenInfoDetails[i].setConfirmedPrice(lvOpeningNodeList.getNode(i).getChildNode(CONFIRMEDPRICE).getValue());
					// BEGIN RN00042 Ricky Ngan 20081111
					mvHKSIPOGenInfoDetails[i].setIndividualIPOAbleForInternet(lvOpeningNodeList.getNode(i).getChildNode(INDIVIDUAL_IPO_WEB_ENABLE).getValue());
					mvHKSIPOGenInfoDetails[i].setIndividualMarginAbleForInternet(lvOpeningNodeList.getNode(i).getChildNode(INDIVIDUAL_MARGIN_WEB_ENABLE).getValue());
					// END RN00042
					//Begin Task: CL00070 Charlie Lau 20090204
					mvHKSIPOGenInfoDetails[i].setInstrumentShortName(lvOpeningNodeList.getNode(i).getChildNode(INSTRUMENTSHORTNAME).getValue());
					mvHKSIPOGenInfoDetails[i].setInstrumentChineseShortName(lvOpeningNodeList.getNode(i).getChildNode(INSTRUMENTCHINESESHORTNAME).getValue());

					HKSInstrument lvInstrumentNameMap = new HKSInstrument();

					lvInstrumentNameMap.setMarketID(mvMarketID);
					lvInstrumentNameMap.setInstrumentID(mvHKSIPOGenInfoDetails[i].getInstrumentID());
					lvInstrumentNameMap.setShortName(mvHKSIPOGenInfoDetails[i].getInstrumentShortName());
					lvInstrumentNameMap.setChineseShortName(mvHKSIPOGenInfoDetails[i].getInstrumentChineseShortName());
					
					TPManager.mvInstrumentNameVector.remove(mvMarketID +"|"+ mvHKSIPOGenInfoDetails[i].getInstrumentID().toUpperCase());
                    TPManager.mvInstrumentNameVector.put(mvMarketID +"|"+ mvHKSIPOGenInfoDetails[i].getInstrumentID().toUpperCase(), lvInstrumentNameMap);
                    // End Task: CL00070
				}
				IMsgXMLNodeList lvAppliedEntitlementID = lvRetNode.getChildNode("APPLIEDENTITLEMENTID").getNodeList("ENTITLEMENTID");
				for (int i = 0; i < lvAppliedEntitlementID.size(); i++)
				{
					HKSAppliedEntitlementID.add(lvAppliedEntitlementID.getNode(i).getValue());
				}
			}
		}
		catch (Exception e)
		{
			Log.println( e , Log.ERROR_LOG);
		}
	}
	/**
	 * Get method for list a application entitlement id
	 * @return list a application entitlement id
	 */
	public Vector getHKSAppliedEntitlementID()
	{
		return HKSAppliedEntitlementID;
	}

	/**
	 * Get method for Client ID
	 * @return Client ID
	 */
	public String getClientId()
	{
		return mvClientId;
	}

	/**
	 * Set method for Client ID
	 * @param pClientId the Client ID
	 */
	public void setClientId(String pClientId)
	{
		mvClientId = pClientId;
	}
	// BEGIN RN00028 Ricky Ngan 20080816
	/**
	 * Get method for margin financing type
	 * @return margin financing type
	 */
	public String getMarginFinancingType(){
		return mvMarginFinancingType;
	}
	/**
	 * Set method for margin financing type
	 * @param pMarginFinancingType the margin financing type
	 */
	public void setMarginFinancingType(String pMarginFinancingType){
		mvMarginFinancingType = pMarginFinancingType;
	}
	// END RN00028
	//BEGIN Task: RN00026 Ricky Ngan 200080620
	/**
	 * Set method for day after allotment
	 * @param pDayAfterAllotment the day after allotment
	 */
	public void setDayAfterAllotment(String pDayAfterAllotment)
	{
		mvDayAfterAllotment = pDayAfterAllotment;
	}
	/**
	 * Get method for day after allotment
	 * @return day after allotment
	 */
	public String getDayAfterAllotment()
	{
		return mvDayAfterAllotment;
	}
	// END Task: RN00026
	/**
	 * Get method for IPO List Details
	 * @return Array of IPO List Details
	 */
	public HKSIPOGenInfoDetails[] getIPOGenInfoDetails()
	{
		return mvHKSIPOGenInfoDetails;
	}

	/**
	 * Set method for IPO Detail
	 * @param pHKSIPOGenInfoDetails the Array of IPO Detail
	 */
	public void setIPOGenInfoDetails(HKSIPOGenInfoDetails[] pHKSIPOGenInfoDetails)
	{
		mvHKSIPOGenInfoDetails = pHKSIPOGenInfoDetails;
	}


	/**
	 * Get method for Return Code
	 * @return Return Code
	 */
	public int getReturnCode()
	{
		return mvReturnCode;
	}

	/**
	 * Set method for Return Code
	 * @param pReturnCode the Return Code
	 */
	public void setReturnCode(int pReturnCode)
	{
		mvReturnCode = pReturnCode;
	}

	/**
	 * Get method for Error Code
	 * @return Error Code
	 */
	public String getErrorCode()
	{
		return mvErrorCode;
	}

	/**
	 * Set method for Error Code
	 * @param Error Code
	 */
	public void setErrorCode(String pErrorCode)
	{
		mvErrorCode = pErrorCode;
	}


	/**
	 * Get method for Error Message
	 * @return Error Message
	 */
	public String getErrorMessage()
	{
		return mvErrorMessage;
	}

	/**
	 * Set method for Error Message
	 * @param pErrorMessage the Error Message
	 */
	public void setErrorMessage(String pErrorMessage)
	{
		mvErrorMessage = pErrorMessage;
	}
	/**
	 * Get method for localized system error message
	 * @param pErrorCode the system error code
	 * @param pDefaultMesg the default message
	 * @return localized system error message
	 */
	public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
	{
		String lvRetMessage;

		try
		{
			if (pErrorCode.equals("0") || pErrorCode.equals("999"))
			{
				lvRetMessage = pDefaultMesg;
			}
			else
			{
				lvRetMessage = (String)presentation.getErrorCodeBundle().getString(pErrorCode);
			}
		}
		catch (Exception ex)
		{
			Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

			lvRetMessage = pDefaultMesg;
		}

		return lvRetMessage;
	}
	/**
	 * Get method for is margin enable
	 * @return is margin enable
	 */
	public String getIsMarginEnable() {
		return mvlsMarginEnable;
	}
	/**
	 * Set method for is margin enable
	 * @param pIsMarginEnable the is margin enable
	 */
	public void setIsMarginEnable(String pIsMarginEnable) {
		this.mvlsMarginEnable = pIsMarginEnable;
	}
}
