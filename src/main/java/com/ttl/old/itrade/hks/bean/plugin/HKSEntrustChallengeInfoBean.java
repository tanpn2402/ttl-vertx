package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSAdvancePaymentInfoBean class define variables that to save values for action 
 * @author Wind.Zhao
 *
 */

//Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSEntrustChallengeInfoBean {
	private String mvReturnCode;
	private String mvReturnMessage;
	private String mvChallenge;
	/**
	 * @return the mvReturnCode
	 */
	public String getMvReturnCode() {
		return mvReturnCode;
	}
	/**
	 * @param mvReturnCode the mvReturnCode to set
	 */
	public void setMvReturnCode(String mvReturnCode) {
		this.mvReturnCode = mvReturnCode;
	}
	/**
	 * @return the mvReturnMessage
	 */
	public String getMvReturnMessage() {
		return mvReturnMessage;
	}
	/**
	 * @param mvReturnMessage the mvReturnMessage to set
	 */
	public void setMvReturnMessage(String mvReturnMessage) {
		this.mvReturnMessage = mvReturnMessage;
	}
	/**
	 * @return the mvChallenge
	 */
	public String getMvChallenge() {
		return mvChallenge;
	}
	/**
	 * @param mvChallenge the mvChallenge to set
	 */
	public void setMvChallenge(String mvChallenge) {
		this.mvChallenge = mvChallenge;
	}	
}
//End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
