package com.ttl.old.itrade.hks.bean;

/**
 * The HKSMarginApplStatusEnquiryBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSMarginApplStatusEnquiryBean {
	private String mvInstrumentID;
	private String mvStockName;
	private String mvAppliedQty;
	private String mvOfferPrice;
	private String mvApplDate;
	private String mvRefNo;
	private String mvApplStatus;
	private String mvAllotmentResult;
	private boolean mvOnclick;
	
	/**
     * This method returns the button if click.
     * @return the button if click.
     */
	public boolean isMvOnclick() {
		return mvOnclick;
	}
	
	/**
     * This method sets the button if click.
     * @param pOnclick The button if click.
     */
	public void setMvOnclick(boolean pOnclick) {
		mvOnclick = pOnclick;
	}
	
	/**
     * This method returns the allotment result.
     * @return the allotment result.
     */
	public String getMvAllotmentResult() {
		return mvAllotmentResult;
	}
	
	/**
     * This method sets the allotment result.
     * @param pAllotmentResult The allotment result.
     */
	public void setMvAllotmentResult(String pAllotmentResult) {
		mvAllotmentResult = pAllotmentResult;
	}
	
	/**
     * This method returns the instrument id.
     * @return the instrument id.
     */
	public String getMvInstrumentID() {
		return mvInstrumentID;
	}
	
	/**
     * This method sets the instrument id.
     * @param pInstrumentID The instrument id.
     */
	public void setMvInstrumentID(String pInstrumentID) {
		mvInstrumentID = pInstrumentID;
	}
	
	/**
     * This method returns the name of stock.
     * @return the name of stock.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the name of stock.
     * @param pStockName The name of stock.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the applied quantity.
     * @return the applied quantity is formatted.
     */
	public String getMvAppliedQty() {
		return mvAppliedQty;
	}
	
	/**
     * This method sets the applied quantity.
     * @param pAppliedQty the applied quantity is formatted.
     */
	public void setMvAppliedQty(String pAppliedQty) {
		mvAppliedQty = pAppliedQty;
	}
	
	/**
     * This method returns the offer price.
     * @return the offer price is formatted.
     */
	public String getMvOfferPrice() {
		return mvOfferPrice;
	}
	
	/**
     * This method sets the offer price.
     * @param pOfferPrice the offer price is formatted.
     */
	public void setMvOfferPrice(String pOfferPrice) {
		mvOfferPrice = pOfferPrice;
	}
	
	/**
     * This method returns the apply date.
     * @return the apply date.
     */
	public String getMvApplDate() {
		return mvApplDate;
	}
	
	/**
     * This method sets the apply date.
     * @param pApplDate The apply date.
     */
	public void setMvApplDate(String pApplDate) {
		mvApplDate = pApplDate;
	}
	
	/**
     * This method returns the ref number.
     * @return the ref number.
     */
	public String getMvRefNo() {
		return mvRefNo;
	}
	
	/**
     * This method sets the ref number.
     * @param pRefNo The ref number.
     */
	public void setMvRefNo(String pRefNo) {
		mvRefNo = pRefNo;
	}
	
	/**
     * This method returns the apply status.
     * @return the apply status.
     */
	public String getMvApplStatus() {
		return mvApplStatus;
	}
	
	/**
     * This method sets the apply status.
     * @param pApplStatus The apply status.
     */
	public void setMvApplStatus(String pApplStatus) {
		mvApplStatus = pApplStatus;
	}
}