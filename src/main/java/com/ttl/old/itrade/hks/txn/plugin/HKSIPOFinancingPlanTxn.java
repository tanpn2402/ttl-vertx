package com.ttl.old.itrade.hks.txn.plugin;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.systekit.tag.ITagXsfTagName;

/**
 * The HKSIPOFinancingPlanTxn class definition for all method
 * Planning IPO Financing Plan
 * @author User1
 *
 */
public class HKSIPOFinancingPlanTxn extends BaseTxn {
	/**
	 * This variable is used to the tp Error
	 */
	private TPErrorHandling tpError;
	private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;
    
	private String mvEntitlementID;
	private Vector mvResultLoanAmountLessThan;
	private Vector mvResultInterestRate;
    /**
     * Default constructor for HKSIPOFinancingPlanTxn class
     */
	public HKSIPOFinancingPlanTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSIPOFinancingPlanTxn class
	 * @param pEntitlementID the entitlement id
	 */
	public HKSIPOFinancingPlanTxn(String pEntitlementID){
		tpError = new TPErrorHandling();
		setEntitlementID(pEntitlementID);
	}
	/**
	 * This method process for Planning IPO Financing Plan
	 * @param pEntitlementID the entitlement id
	 * @return Planning IPO Financing Plan
	 */
	public Vector process(String pEntitlementID){
		Vector lvReturn = new Vector();
		try{
			Log.println("HKSIPOFinancingPlanTxn starts", Log.ACCESS_LOG);
			
			Hashtable lvTxnMap = new Hashtable();
			
			lvTxnMap.put("ENTITLEMENTID", pEntitlementID);
			
			if (TPErrorHandling.TP_NORMAL != process(RequestName.HKSMarginPlanRequest, lvTxnMap)){
				// TODO: simulate TP response XML
				mvReturnNode = bulidXMLNode();
				
				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
				for(int i=0; i< lvRowList.size(); i++){
					IMsgXMLNode lvNode = lvRowList.getNode(i);
					HashMap lvModel = new HashMap();
					lvModel.put("LOANAMOUNTLESSTHAN", lvNode.getChildNode("LOANAMOUNTLESSTHAN").getValue());
					lvModel.put("INTERESTRATE", lvNode.getChildNode("INTERESTRATE").getValue());
					lvReturn.add(lvModel);
				}
			}
		}catch(Exception e)
		{	
			Log.println("HKSIPOFinancingPlanTxn error" + e.toString(), Log.ERROR_LOG);
		}
		return lvReturn;
	}
	
	/**
	 * Get method for entitlement id
	 * @return entitlement id
	 */
	public String getEntitlementID(){
		return mvEntitlementID;
	}
	/**
	 * Set method for entitlement id
	 * @param pEntitlementID the entitlement id
	 */
	public void setEntitlementID(String pEntitlementID){
		this.mvEntitlementID=pEntitlementID;
	}
	/**
	 * Set method for return system code
	 * @param pReturnCode the return system code
	 */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Set method for system error code
     * @param pErrorCode the system error code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }

    /**
	 * This function return a request xml node for submit advance payment.
	 * @return xml node.
	 * @author Wind.Zhao
	 */
	private IMsgXMLNode bulidXMLNode(){
		IMsgXMLNode lvReturnXMLNode = null;
		IMsgXMLParser lvIMsgXMLParser = MsgManager.createParser();
		lvReturnXMLNode = lvIMsgXMLParser.createXMLNode("HKSWB036Q01");
		lvReturnXMLNode.setAttribute("msgId", "HKSWB036Q01");
		lvReturnXMLNode.setAttribute("issueTime", "20100225190637");
		lvReturnXMLNode.setAttribute("issueLoc", "888");
		lvReturnXMLNode.setAttribute("issueMetd", "01");
		lvReturnXMLNode.setAttribute("oprId", IMain.getProperty("AgentID"));
		lvReturnXMLNode.setAttribute("pwd", IMain.getProperty("AgentPassword"));
		lvReturnXMLNode.setAttribute("resvr", "0000000015");
		lvReturnXMLNode.setAttribute("language", "en");
		lvReturnXMLNode.setAttribute("country", "us");
		lvReturnXMLNode.setAttribute("retryForTimeout", "T");
		
		
		IMsgXMLNode lvChild_Row = lvReturnXMLNode.addChildNode("Row");
		lvChild_Row.addChildNode("LOANAMOUNTLESSTHAN").setValue("1000.00");
		lvChild_Row.addChildNode("INTERESTRATE").setValue("3.00000");
		IMsgXMLNode lvChild_Row1 = lvReturnXMLNode.addChildNode("Row");
		lvChild_Row1.addChildNode("LOANAMOUNTLESSTHAN").setValue("20000.00");
		lvChild_Row1.addChildNode("INTERESTRATE").setValue("5.00000");
		IMsgXMLNode lvChild_Row2 = lvReturnXMLNode.addChildNode("Row");
		lvChild_Row2.addChildNode("LOANAMOUNTLESSTHAN").setValue("400000.00");
		lvChild_Row2.addChildNode("INTERESTRATE").setValue("7.00000");
		
		return lvReturnXMLNode;
	}
}
