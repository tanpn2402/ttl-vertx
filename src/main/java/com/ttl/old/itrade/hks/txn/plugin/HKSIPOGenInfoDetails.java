package com.ttl.old.itrade.hks.txn.plugin;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
/**
 * The HKSIPOGenInfoTxn class entity
 * @author not attributable
 */
public class HKSIPOGenInfoDetails
{
    private String mvEntitlementID;
    private String mvProductID;
    private String mvMarketID;
    private String mvInstrumentID;
    private String mvStartDate;
    private String mvEndDate;
    private String mvEndTime;
    private String mvEIPOEndDate;
    private String mvEIPOEndTime;
    private String mvAllotmentDate;
    private String mvCurrencyID;
    private String mvNumberOfOfferShares;
    private String mvNumberOfPublicOfferShares;
    private String mvMinOfferPrice;
    private String mvMaxOfferPrice;
    private String mvConfirmedPrice;
    private String mvProspectus;
    private String mvChineseProspectus;
    private String mvWebsite;
    // BEGIN RN00030 Ricky Ngan 20080923
    private String mvChineseWebsite;
    // END RN0030 
    private String mvStatus;
    private String mvRemark;
    private String mvListingDate;
    
    // BEGIN RN00042 Ricky Ngan 20081111
    private String mvIndividualIPOAbleForInternet;
    private String mvIndividualMarginAbleForInternet;
    // END
    
    // BEGIN RN00028 Ricky Ngan 20080816
	protected String mvIPOEnabled;
	protected String mvAllotmentEnabled;
    protected BigDecimal mvMarketCapitalization;
    protected int mvTotalMarinCount;
	protected BigDecimal mvTotalMarginAppAmount;
	protected int mvTotalIPOCount;
	protected BigDecimal mvTotalIPOAppAmount;
	protected String mvInterestValueDate;
	protected String mvCustAdviceRemark;
	protected String mvState;
	protected String mvPendAction;
	protected String mvCreatorUserID;
	protected Timestamp mvCreationTime;
	protected String mvLastModifiedUserID;
	protected Timestamp mvLastModifiedTime;
	protected String mvLastApproverUserID;
	protected Timestamp mvLastApprovalTime;
	// END RN00028
	//Begin Task: CL00070 Charlie Lau 20090204
	protected String mvShortName;
	protected String mvChineseShortName;
	// End Task: CL00070
	
	//Begin Task #: - TTL-GZ-ZZW-00026 Wind Zhao 20100205 [iTrade R5] Merge IPO and IPO margin
	private String mvMarginEnable;
	
	/**
	 * Returns the margin is enable.
	 * @return the margin is enable.
	 * 		  [1]"N" is not enable.
	 * 		  [2]"Y" is enable.
	 */
	public String getMvMarginEnable() {
		return mvMarginEnable;
	}
	
	/**
	 * Returns the margin is enable.
	 * @param pMarginEnable The margin is enable.
	 */
	public void setMvMarginEnable(String pMarginEnable) {
		mvMarginEnable = pMarginEnable;
	}
	//End Task #: - TTL-GZ-ZZW-00026 Wind Zhao 20100205 [iTrade R5] Merge IPO and IPO margin
	
	/**
	 * Get method for entitlement id
	 * @return entitlement id
	 */
    public String getEntitlementID()
    {
        return mvEntitlementID;
    }
    /**
     * Get method for product id
     * @return product id
     */
    public String getProductID()
    {
        return mvProductID;
    }
    /**
     * Get method for market id
     * @return market id
     */
    public String getMarketID()
    {
        return mvMarketID;
    }
    /**
     * Get method for instrument id
     * @return instrument id
     */
    public String getInstrumentID()
    {
        return mvInstrumentID;
    }
    /**
     * Get method for start date
     * @return start date
     */
    public String getStartDate()
    {
        return mvStartDate;
    }
    /**
     * Get method for end date
     * @return end date
     */
    public String getEndDate()
    {
        return mvEndDate;
    }
    /**
     *Get method for end time 
     * @return end time
     */
    public String getEndTime()
    {
        return mvEndTime;
    }
    /**
     * Get method for IPO end date
     * @return IPO end date
     */
    public String getEIPOEndDate()
    {
        return mvEIPOEndDate;
    }
    /**
     * Get method for IPO end time
     * @return IPO end time
     */
    public String getEIPOEndTime()
    {
        return mvEIPOEndTime;
    }
    /**
     * Get method for allotment date
     * @return allotment date
     */
    public String getAllotmentDate()
    {
        return mvAllotmentDate;
    }
    /**
     * Get method for currency id
     * @return currency id
     */
    public String getCurrencyID()
    {
        return mvCurrencyID;
    }
    /**
     * Get method for number of offer shares
     * @return number of offer shares
     */
    public String getNumberOfOfferShares()
    {
        return mvNumberOfOfferShares;
    }
    /**
     * Get method for number of public offer shares
     * @return number of public offer shares
     */
    public String getNumberOfPublicOfferShares()
    {
        return mvNumberOfPublicOfferShares;
    }
    /**
     * Get method for small offer price
     * @return small offer price
     */
    public String getMinOfferPrice()
    {
        return mvMinOfferPrice;
    }
    /**
     * Get method for max offer price
     * @return max offer price
     */
    public String getMaxOfferPrice()
    {
        return mvMaxOfferPrice;
    }
    /**
     * Get method for confirmed price
     * @return confirmed price
     */
    public String getConfirmedPrice()
    {
        return mvConfirmedPrice;
    }
    /**
     * Get method for prospectus
     * @return prospectus
     */
    public String getProspectus()
    {
        return mvProspectus;
    }
    /**
     * Get method for chinese prospectus
     * @return chinese prospectus
     */
    public String getChineseProspectus()
    {
        return mvChineseProspectus;
    }
    /**
     * Get method for web site
     * @return web site
     */
    public String getWebsite()
    {
        return mvWebsite;
    }
    /**
     * Get method for status
     * @return status
     */
    public String getStatus()
    {
        return mvStatus;
    }
    /**
     * Get method for remark
     * @return remark
     */
    public String getRemark()
    {
        return mvRemark;
    }
    /**
     * Get method for listing date
     * @return listing date
     */
    public String getListingDate()
    {
        return mvListingDate;
    }
    /**
     * Set method for entitlement id
     * @param pEntitlementID the entitlement id
     */
    public void setEntitlementID(String pEntitlementID)
    {
        mvEntitlementID = pEntitlementID;
    }
    /**
     * Set method for product id
     * @param pProductID the product id
     */
    public void setProductID(String pProductID)
    {
        mvProductID = pProductID;
    }
    /**
     * Set method for market id
     * @param pMarketID the market id
     */
    public void setMarketID(String pMarketID)
    {
        mvMarketID = pMarketID;
    }
    /**
     * Set method for instrument id
     * @param pInstrumentID the instrument id
     */
    public void setInstrumentID(String pInstrumentID)
    {
        mvInstrumentID = pInstrumentID;
    }
    /**
     * Set method for start date
     * @param pStartDate the start date
     */
    public void setStartDate(String pStartDate)
    {
        mvStartDate = pStartDate;
    }
    /**
     * Set method for end date
     * @param pEndDate the end date
     */
    public void setEndDate(String pEndDate)
    {
        mvEndDate = pEndDate;
    }
    /**
     * Set method for end time
     * @param pEndTime the end time
     */
    public void setEndTime(String pEndTime)
    {
        mvEndTime = pEndTime;
    }
    /**
     * Set method for IPO end date
     * @param pEIPOEndDate the IPO end date
     */
    public void setEIPOEndDate(String pEIPOEndDate)
    {
        mvEIPOEndDate = pEIPOEndDate;
    }
    /**
     * Set method for IPO end time
     * @param pEIPOEndTime the IPO end time
     */
    public void setEIPOEndTime(String pEIPOEndTime)
    {
        mvEIPOEndTime = pEIPOEndTime;
    }
    /**
     * Set method for allotment date
     * @param pAllotmentDate the allotment date
     */
    public void setAllotmentDate(String pAllotmentDate)
    {
        mvAllotmentDate = pAllotmentDate;
    }
    /**
     * Set method for currency id
     * @param pCurrencyID the currency id
     */
    public void setCurrencyID(String pCurrencyID)
    {
        mvCurrencyID = pCurrencyID;
    }
    /**
     * Set method for number of offer shares
     * @param pNumberOfOfferShares the number of offer shares
     */
    public void setNumberOfOfferShares(String pNumberOfOfferShares)
    {
        mvNumberOfOfferShares = pNumberOfOfferShares;
    }
    /**
     * Set method for number of public offer shares
     * @param pNumberOfPublicOfferShares the number of public offer shares
     */
    public void setNumberOfPublicOfferShares(String pNumberOfPublicOfferShares)
    {
        mvNumberOfPublicOfferShares = pNumberOfPublicOfferShares;
    }
    /**
     * Set method for small offer price 
     * @param pMinOfferPrice the small offer price
     */
    public void setMinOfferPrice(String pMinOfferPrice)
    {
        mvMinOfferPrice = pMinOfferPrice;
    }
    /**
     * Set method for max offer price
     * @param pMaxOfferPrice the max offer price
     */
    public void setMaxOfferPrice(String pMaxOfferPrice)
    {
        mvMaxOfferPrice = pMaxOfferPrice;
    }
    /**
     * Set method for confirmed price
     * @param pConfirmedPrice the confirmed price
     */
    public void setConfirmedPrice(String pConfirmedPrice)
    {
        mvConfirmedPrice = pConfirmedPrice;
    }
    /**
     * Set method for prospectus
     * @param pProspectus the prospectus
     */
    public void setProspectus(String pProspectus)
    {
        mvProspectus = pProspectus;
    }
    /**
     * Set method for chinese prospectus
     * @param pChineseProspectus the chinese prospectus
     */
    public void setChineseProspectus(String pChineseProspectus)
    {
        mvChineseProspectus = pChineseProspectus;
    }
    /**
     * Set method for web site
     * @param pWebsite the web site
     */
    public void setWebsite(String pWebsite)
    {
        mvWebsite = pWebsite;
    }
    /**
     * Set method for status
     * @param pStatus ths status
     */
    public void setStatus(String pStatus)
    {
        mvStatus = pStatus;
    }
    /**
     * Set method for remark
     * @param pRemark the remark
     */
    public void setRemark(String pRemark)
    {
        mvRemark = pRemark;
    }
    /**
     * Set method for listing date
     * @param pListingDate the listing date
     */
    public void setListingDate(String pListingDate)
    {
        mvListingDate = pListingDate;
    }
	// BEGIN RN00028 Ricky Ngan 20080816
    /**
     * Get method for IPO enabled
     * @return IPO enabled
     */
	public String getIPOEnabled() {
		return mvIPOEnabled;
	}
	/**
	 * Set method for IPO enabled
	 * @param pEnabled the IPO enabled
	 */
	public void setIPOEnabled(String pEnabled) {
		mvIPOEnabled = pEnabled;
	}
	/**
	 * Get method for allotment enabled
	 * @return allotment enabled
	 */
	public String getAllotmentEnabled() {
		return mvAllotmentEnabled;
	}
	/**
	 * Set method for allotment enabled
	 * @param pAllotmentEnabled the allotment enabled
	 */
	public void setAllotmentEnabled(String pAllotmentEnabled) {
		mvAllotmentEnabled = pAllotmentEnabled;
	}
	/**
	 * Get method for market capitalization
	 * @return market capitalization
	 */
	public BigDecimal getMarketCapitalization() {
		return mvMarketCapitalization;
	}
	/**
	 * Set method for market capitalization
	 * @param pMarketCapitalization market capitalization
	 */
	public void setMarketCapitalization(BigDecimal pMarketCapitalization) {
		mvMarketCapitalization = pMarketCapitalization;
	}
	/**
	 * Get method for total margin count
	 * @return total margin count
	 */
	public int getTotalMarinCount() {
		return mvTotalMarinCount;
	}
	/**
	 * Set method for total margin count
	 * @param pTotalMarinCount the total margin count
	 */
	public void setTotalMarinCount(int pTotalMarinCount) {
		mvTotalMarinCount = pTotalMarinCount;
	}
	/**
	 * Get method for total margin application amount
	 * @return total margin application amount
	 */
	public BigDecimal getTotalMarginAppAmount() {
		return mvTotalMarginAppAmount;
	}
	/**
	 * Set method for total margin application amount
	 * @param pTotalMarginAppAmount the total margin application amount
	 */
	public void setTotalMarginAppAmount(BigDecimal pTotalMarginAppAmount) {
		mvTotalMarginAppAmount = pTotalMarginAppAmount;
	}
	/**
	 * Get method for total IPO count
	 * @return total IPO count
	 */
	public int getTotalIPOCount() {
		return mvTotalIPOCount;
	}
	/**
	 * Set method for total IPO count
	 * @param pTotalIPOCount the total IPO count
	 */
	public void setTotalIPOCount(int pTotalIPOCount) {
		mvTotalIPOCount = pTotalIPOCount;
	}
	/**
	 * Get method for total IPO application amount
	 * @return total IPO application amount
	 */
	public BigDecimal getTotalIPOAppAmount() {
		return mvTotalIPOAppAmount;
	}
	/**
	 * Set method for total IPO application amount
	 * @param pTotalIPOAppAmount the total IPO application amount
	 */
	public void setTotalIPOAppAmount(BigDecimal pTotalIPOAppAmount) {
		mvTotalIPOAppAmount = pTotalIPOAppAmount;
	}
	/**
	 * Get method for interest value date
	 * @return interest value date
	 */
	public String getInterestValueDate() {
		return mvInterestValueDate;
	}
	/**
	 * Set method for interest value date
	 * @param pInterestValueDate the interest value date
	 */
	public void setInterestValueDate(String pInterestValueDate) {
		mvInterestValueDate = pInterestValueDate;
	}
	/**
	 * Get method for custom advice remark
	 * @return  custom advice remark
	 */
	public String getCustAdviceRemark() {
		return mvCustAdviceRemark;
	}
	/**
	 * Set method for custom advice remark
	 * @param pCustAdviceRemark the  custom advice remark
	 */
	public void setCustAdviceRemark(String pCustAdviceRemark) {
		mvCustAdviceRemark = pCustAdviceRemark;
	}
	/**
	 * Get method for stare
	 * @return stare
	 */
	public String getState() {
		return mvState;
	}
	/**
	 * Set method for stare
	 * @param pState the stare
	 */
	public void setState(String pState) {
		mvState = pState;
	}
	/**
	 * Get method for pend action
	 * @return pend action
	 */
	public String getPendAction() {
		return mvPendAction;
	}
	/**
	 * Set method for pend action
	 * @param pPendAction the pend action
	 */
	public void setPendAction(String pPendAction) {
		mvPendAction = pPendAction;
	}
	/**
	 * Get method for creator user id
	 * @return creator user id
	 */
	public String getCreatorUserID() {
		return mvCreatorUserID;
	}
	/**
	 * Set method for creator user id
	 * @param pCreatorUserID the creator user id
	 */
	public void setCreatorUserID(String pCreatorUserID) {
		mvCreatorUserID = pCreatorUserID;
	}
	/**
	 * Get method for creation time
	 * @return creation time
	 */
	public Timestamp getCreationTime() {
		return mvCreationTime;
	}
	/**
	 * Set method for creation time
	 * @param pCreationTime creation time
	 */
	public void setCreationTime(Timestamp pCreationTime) {
		mvCreationTime = pCreationTime;
	}
	/**
	 * Get method for last modified user id
	 * @return last modified user id
	 */
	public String getLastModifiedUserID() {
		return mvLastModifiedUserID;
	}
	/**
	 * Set method for last modified user id
	 * @param pLastModifiedUserID the last modified user id
	 */
	public void setLastModifiedUserID(String pLastModifiedUserID) {
		mvLastModifiedUserID = pLastModifiedUserID;
	}
	/**
	 * Get method for last modified time
	 * @return last modified time
	 */
	public Timestamp getLastModifiedTime() {
		return mvLastModifiedTime;
	}
	/**
	 * Set method for last modified time
	 * @param pLastModifiedTime the last modified time
	 */
	public void setLastModifiedTime(Timestamp pLastModifiedTime) {
		mvLastModifiedTime = pLastModifiedTime;
	}
	/**
	 * Get method for last approver user id
	 * @return last approver user id
	 */
	public String getLastApproverUserID() {
		return mvLastApproverUserID;
	}
	/**
	 * Set method for last approver user id
	 * @param pLastApproverUserID the last approver user id
	 */
	public void setLastApproverUserID(String pLastApproverUserID) {
		mvLastApproverUserID = pLastApproverUserID;
	}
	/**
	 * Get method for last approval time
	 * @return last approval time
	 */
	public Timestamp getLastApprovalTime() {
		return mvLastApprovalTime;
	}
	/**
	 * Set method for last approval time
	 * @param pLastApprovalTime the last approval time
	 */
	public void setLastApprovalTime(Timestamp pLastApprovalTime) {
		mvLastApprovalTime = pLastApprovalTime;
	}
	// END RN00028
	// BEGIN RN00030 Ricky Ngan 20080923
	/**
	 * Get method for chinese web site
	 * @return chinese web site
	 */
	public String getChineseWebsite() {
		return mvChineseWebsite;
	}
	/**
	 * Set method for chinese web site
	 * @param pChineseWebsite the chinese web site
	 */
	public void setChineseWebsite(String pChineseWebsite) {
		mvChineseWebsite = pChineseWebsite;
	}
	// END RN00030

	// BEGIN RN00042 Ricky Ngan 20081111
	/**
	 * Get method for individual IPO able for internet
	 * @return individual IPO able for internet
	 */
	public String getIndividualIPOAbleForInternet() {
		return mvIndividualIPOAbleForInternet;
	}
	/**
	 * Set method for individual IPO able for internet
	 * @param pIndividualIPOAbleForInternet the individual IPO able for internet
	 */
	public void setIndividualIPOAbleForInternet(String pIndividualIPOAbleForInternet) {
		mvIndividualIPOAbleForInternet = pIndividualIPOAbleForInternet;
	}
	/**
	 * Get method for individual margin able for internet
	 * @return individual margin able for internet
	 */
	public String getIndividualMarginAbleForInternet() {
		return mvIndividualMarginAbleForInternet;
	}
	/**
	 * Set method for individual margin able for internet
	 * @param pIndividualMarginAbleForInternet the individual margin able for internet
	 */
	public void setIndividualMarginAbleForInternet(
			String pIndividualMarginAbleForInternet) {
		mvIndividualMarginAbleForInternet = pIndividualMarginAbleForInternet;
	}
	// END RN00042
	
	//Begin Task: CL00070 Charlie Lau 20090204
	/**
	 * Get method for instrument short name
	 * @return instrument short name
	 */
	public String getInstrumentShortName() {
		return mvShortName;
	}
	/**
	 * Set method for instrument short name
	 * @param pShortName the instrument short name
	 */
	public void setInstrumentShortName(String pShortName) {
		mvShortName = pShortName;
	}
	/**
	 * Get method for instrument chinese short name
	 * @return instrument chinese short name
	 */
	public String getInstrumentChineseShortName() {
		return mvChineseShortName;
	}
	/**
	 * Set method for instrument chinese short name
	 * @param pChineseShortName the instrument chinese short name
	 */
	public void setInstrumentChineseShortName(String pChineseShortName) {
		mvChineseShortName = pChineseShortName;
	}
	// End Task: CL00070
}
