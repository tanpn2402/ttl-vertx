package com.ttl.old.itrade.hks.txn.plugin;

import java.math.BigDecimal;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSIPOGenDetailTxn class definition for all method
 * From the host to obtain information on the details listed
 * @author not attributable
 *
 */
public class HKSIPOGenDetailTxn
{
    private String mvEntitlementId;
    private String mvClientId;
    private String mvLotSpreadDetail;
    private String mvCalculateFeeFlag;
    private String mvApplicationQty;
    private String mvTradingAccSeq;
    private String mvAccountSeq;
    private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;

    private HKSIPOGenInfoDetails   mvHKSIPOGenInfoDetails;
    private Vector mvIPOLotListVector;
    private Vector mvIPOAmountListVector;
    private BigDecimal mvHandlingFeeAmount;
    //Variable is used to identify
    public static final String CLIENTID = "CLIENTID";
    public static final String INSTRUMENTID = "INSTRUMENTID";
    public static final String ENTITLEMENTID = "ENTITLEMENTID";
    public static final String LOTSPREADDETAIL = "LOTSPREADDETAIL";
    public static final String CALCULATEFEEFLAG = "CALCULATEFEEFLAG";
    public static final String APPLIEDQTY = "APPLIEDQTY";
    public static final String OVERRIDEAMOUNT = "OVERRIDEAMOUNT";

    public static final String NUMBEROFOFFERSHARES = "NUMBEROFOFFERSHARES";
    public static final String NUMBEROFPUBLICOFFERSHARES = "NUMBEROFPUBLICOFFERSHARES";
    public static final String MINOFFERPRICE = "MINOFFERPRICE";
    public static final String MAXOFFERPRICE = "MAXOFFERPRICE";
    public static final String CONFIRMEDPRICE = "CONFIRMEDPRICE";
    public static final String ENDDATE = "ENDDATE";
    public static final String ENDTIME = "ENDTIME";
    public static final String EIPOENDDATE = "EIPOENDDATE";
    public static final String EIPOENDTIME = "EIPOENDTIME";
    public static final String STATUS = "STATUS";
    public static final String PROSPECTUS = "PROSPECTUS";
    public static final String CHINESEPROSPECTUS = "CHINESEPROSPECTUS";
    public static final String WEBSITE = "WEBSITE";
    // BEGIN RN00030 Ricky Ngan 20080923
    public static final String CHINESEWEBSITE = "CHWEBSITE";
    // END RN00030 
    public static final String LISTINGDATE = "LISTINGDATE";
    // BEGIN RN00028 Ricky Ngan 20080816
    public static final String INTERESTVALUEDATE = "INTERESTVALUEDATE";
    public static final String ALLOTMENTDATE = "ALLOTMENTDATE";
    public static final String INTCHANNELCUTOFFDATE = "INTCHANNELCUTOFFDATE";
    // END RN00028
    public static final String LOOP = "LOOP";
    public static final String LOOP_ELEMENT = "LOOP_ELEMENT";
    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling tpError;
    Presentation presentation = null;

    /**
     * Default constructor for HKSIPOGenDetailTxn class
     * @param pPresentation the Presentation class
     */
    public HKSIPOGenDetailTxn(Presentation pPresentation)
    {
        tpError = new TPErrorHandling();
        presentation = pPresentation;
    }

    /**
     * Default constructor for HKSIPOGenDetailTxn class
     * @param pPresentation the Presentation class
     * @param pClientId the client id
     * @param pTradingAccSeq the trading account sequence
     * @param pAccountSeq the account sequence
     */
    public HKSIPOGenDetailTxn(Presentation pPresentation, String pClientId, String pTradingAccSeq, String pAccountSeq)
    {
        this(pPresentation);
        setClientId(pClientId);
        setTradingAccSeq(pTradingAccSeq);
        setAccountSeq(pAccountSeq);
    }
    /**
     * This method process From the host to obtain information on the details listed
     * @param pPresentation the Presentation class
     */
    public void process(Presentation pPresentation)
    {
        try
        {
            Hashtable		lvTxnMap = new Hashtable();

            IMsgXMLNode		lvRetNode;
            TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSIPODetailsRequest);
            TPBaseRequest   lvIPODetailsRequest = ivTPManager.getRequest(RequestName.HKSIPODetailsRequest);

            lvTxnMap.put(CLIENTID, getClientId());
            lvTxnMap.put(TagName.ACCOUNTSEQ, getAccountSeq());
            lvTxnMap.put(TagName.TRADINGACCSEQ, getTradingAccSeq());
            lvTxnMap.put(ENTITLEMENTID, getEntitlementId());
            lvTxnMap.put(LOTSPREADDETAIL, getLotSpreadDetail() == null ? "N" : getLotSpreadDetail());
            if ("Y".equalsIgnoreCase(getCalculateFeeFlag()))
            {
                lvTxnMap.put(CALCULATEFEEFLAG, "Y");
                lvTxnMap.put(APPLIEDQTY, getApplicationQty());
            }
            else
            {
                lvTxnMap.put(CALCULATEFEEFLAG, "N");
            }

            lvRetNode = lvIPODetailsRequest.send(lvTxnMap);

            // TODO: simulate TP response XML
            lvRetNode = bulidXMLNode();
            
            setReturnCode(tpError.checkError(lvRetNode));
            if (mvReturnCode != TPErrorHandling.TP_NORMAL)
            {
                setErrorMessage(tpError.getErrDesc());
                if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
                {
                    setErrorCode(tpError.getErrCode());
                }
            }
            else
            {
                mvHKSIPOGenInfoDetails = new HKSIPOGenInfoDetails();

                if (lvRetNode.getChildNode(INSTRUMENTID) == null)
                    return;

                mvHKSIPOGenInfoDetails.setInstrumentID(lvRetNode.getChildNode(INSTRUMENTID).getValue());
                mvHKSIPOGenInfoDetails.setEntitlementID(lvRetNode.getChildNode(ENTITLEMENTID).getValue());
                mvHKSIPOGenInfoDetails.setNumberOfOfferShares(lvRetNode.getChildNode(NUMBEROFOFFERSHARES).getValue());
                mvHKSIPOGenInfoDetails.setNumberOfPublicOfferShares(lvRetNode.getChildNode(NUMBEROFPUBLICOFFERSHARES).getValue());
                mvHKSIPOGenInfoDetails.setMinOfferPrice(lvRetNode.getChildNode(MINOFFERPRICE).getValue());
                mvHKSIPOGenInfoDetails.setMaxOfferPrice(lvRetNode.getChildNode(MAXOFFERPRICE).getValue());
                mvHKSIPOGenInfoDetails.setConfirmedPrice(lvRetNode.getChildNode(CONFIRMEDPRICE).getValue());
                mvHKSIPOGenInfoDetails.setEndDate(lvRetNode.getChildNode(ENDDATE).getValue());
                mvHKSIPOGenInfoDetails.setEndTime(lvRetNode.getChildNode(ENDTIME).getValue());
                // BEGIN RN00028 Ricky Ngan 20080816
                mvHKSIPOGenInfoDetails.setEIPOEndDate(lvRetNode.getChildNode(INTCHANNELCUTOFFDATE).getValue());
                mvHKSIPOGenInfoDetails.setAllotmentDate(lvRetNode.getChildNode(ALLOTMENTDATE).getValue());
                mvHKSIPOGenInfoDetails.setInterestValueDate(lvRetNode.getChildNode(INTERESTVALUEDATE).getValue());
                mvHKSIPOGenInfoDetails.setEIPOEndTime(lvRetNode.getChildNode("INTENDTIME").getValue());
                // END RN00028
                mvHKSIPOGenInfoDetails.setStatus(lvRetNode.getChildNode(STATUS).getValue());
                mvHKSIPOGenInfoDetails.setProspectus(lvRetNode.getChildNode(PROSPECTUS).getValue());
                //mvHKSIPOGenInfoDetails.setChineseProspectus(lvRetNode.getChildNode(CHINESEPROSPECTUS).getValue());
                mvHKSIPOGenInfoDetails.setChineseProspectus("DS");
                mvHKSIPOGenInfoDetails.setWebsite(lvRetNode.getChildNode(WEBSITE).getValue());
                // BEGIN RN00030 Ricky Ngan 20080923
                mvHKSIPOGenInfoDetails.setChineseWebsite(lvRetNode.getChildNode(CHINESEWEBSITE).getValue());
                // END RN00030
                mvHKSIPOGenInfoDetails.setListingDate(lvRetNode.getChildNode(LISTINGDATE).getValue());
                mvHKSIPOGenInfoDetails.setRemark(lvRetNode.getChildNode("REMARK").getValue());

                if (lvRetNode.getChildNode(LOTSPREADDETAIL).getChildNode(LOOP) != null)
                {
                    IMsgXMLNodeList lvLotDetails = lvRetNode.getChildNode(LOTSPREADDETAIL).getChildNode(LOOP).getNodeList(LOOP_ELEMENT);

                    int lvSize = lvLotDetails.size();

                    if (lvSize > 0)
                    {
                        mvIPOLotListVector = new Vector();
                        mvIPOAmountListVector = new Vector();

                        for (int i = 0; i<lvSize; i++)
                        {
                            mvIPOLotListVector.add(lvLotDetails.getNode(i).getChildNode(APPLIEDQTY).getValue());
                            mvIPOAmountListVector.add(lvLotDetails.getNode(i).getChildNode(OVERRIDEAMOUNT).getValue());
                        }
                    }
                }

                if (lvRetNode.getChildNode("FEEID") != null)
                {
                    String lvValue = lvRetNode.getChildNode("FEEID").getValue();
                    if (lvValue != null && !lvValue.equals(""))
                        mvHandlingFeeAmount = new BigDecimal(lvValue);
                    else
                        mvHandlingFeeAmount = new BigDecimal(0);
                }
            }
        }
        catch (Exception e)
        {
            Log.println( e , Log.ERROR_LOG);
        }
    }
    /**
     * Get method for entitlement id
     * @return entitlement id
     */
    public String getEntitlementId()
    {
        return mvEntitlementId;
    }
    /**
     * Set method for entitlement id
     * @param pEntitlementId the entitlement id
     */ 
    public void setEntitlementId(String pEntitlementId)
    {
        mvEntitlementId = pEntitlementId;
    }
    /**
     * Get method for handling fee amount
     * @return handling fee amount
     */
    public BigDecimal getHandlingFeeAmount()
    {
        return mvHandlingFeeAmount;
    }
    /**
     * Set method for handling fee amount
     * @param pHandlingFeeAmount the handling fee amount
     */
    public void setHandlingFeeAmount(BigDecimal pHandlingFeeAmount)
    {
        mvHandlingFeeAmount = pHandlingFeeAmount;
    }
    /**
     * Get method for lot spread detail
     * @return lot spread detail
     */
    public String getLotSpreadDetail()
    {
        return mvLotSpreadDetail;
    }
    /**
     * Set method for lot spread detail
     * @param pLotSpreadDetail the lot spread detail
     */
    public void setLotSpreadDetail(String pLotSpreadDetail)
    {
        mvLotSpreadDetail = pLotSpreadDetail;
    }
    /**
     * Get method for calculate fee flag
     * @return calculate fee flag
     */
    public String getCalculateFeeFlag()
    {
        return mvCalculateFeeFlag;
    }
    /**
     * Set method for calculate fee flag
     * @param pCalculateFeeFlag the calculate fee flag
     */
    public void setCalculateFeeFlag(String pCalculateFeeFlag)
    {
        mvCalculateFeeFlag = pCalculateFeeFlag;
    }
    /**
     * Get method for the account sequence
     * @return account sequence
     */
    public String getAccountSeq()
    {
            return mvAccountSeq;
    }
    /**
     * Set method for the account sequence
     * @param pAccountSeq the account sequence
     */
    public void setAccountSeq(String pAccountSeq)
    {
            mvAccountSeq = pAccountSeq;
    }
    /**
     * Get method for trading account sequence
     * @return trading account sequence
     */
    public String getTradingAccSeq()
    {
            return mvTradingAccSeq;
    }
    /**
     * Set method for trading account sequence
     * @param pTradingAccSeq the trading account sequence
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
            mvTradingAccSeq = pTradingAccSeq;
    }


    /**
     * Get method for application quantity
     * @return application quantity
     */
    public String getApplicationQty()
    {
        return mvApplicationQty;
    }
    /**
     * Set method for application quantity
     * @param pApplicationQty the application quantity
     */
    public void setApplicationQty(String pApplicationQty)
    {
        mvApplicationQty = pApplicationQty;
    }
    /**
     * Get method for client id
     * @return client id
     */
    public String getClientId()
    {
        return mvClientId;
    }
    /**
     * Set method for client id
     * @param pClientId the client id
     */
    public void setClientId(String pClientId)
    {
        mvClientId = pClientId;
    }
    /**
     * Get method for IPO gen info details
     * @return IPO gen info details
     */
    public HKSIPOGenInfoDetails getIPOGenInfoDetails()
    {
        return mvHKSIPOGenInfoDetails;
    }
    /**
     * Set method for IPO gen info details
     * @param pHKSIPOGenInfoDetails the IPO gen info details
     */
    public void setIPOInfoDetails(HKSIPOGenInfoDetails pHKSIPOGenInfoDetails)
    {
        mvHKSIPOGenInfoDetails = pHKSIPOGenInfoDetails;
    }
    /**
     * Get method for a vector the IPO lot
     * @return a vector the IPO lot
     */
    public Vector getIPOLotListVector()
    {
        return mvIPOLotListVector;
    }
    /**
     * Set method for a vector the IPO lot
     * @param pIPOLotListVector the a vector the IPO lot
     */
    public void setIPOLotListVector(Vector pIPOLotListVector)
    {
        mvIPOLotListVector = pIPOLotListVector;
    }
    /**
     * Get method for a vector the IPO amount 
     * @return a vector the IPO amount 
     */
    public Vector getIPOAmountListVector()
    {
        return mvIPOAmountListVector;
    }
    /**
     * Set method for a vector the IPO amount 
     * @param pIPOAmountListVector the a vector the IPO amount 
     */
    public void setIPOAmountListVector(Vector pIPOAmountListVector)
    {
        mvIPOAmountListVector = pIPOAmountListVector;
    }
    /**
     * Get method for return system code
     * @return return system code
     */
    public int getReturnCode()
    {
        return mvReturnCode;
    }
    /**
     * Set method for return system code
     * @param pReturnCode the return system code
     */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Get method for system error code
     * @return system error code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }
    /**
     * Set method for system error code
     * @param pErrorCode the system error code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Get method for system error message
     * @return system error message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * Get method for localized system error message
     * @param pErrorCode the system error code
     * @param pDefaultMesg the default message
     * @return localized system error message
     */
   public String getLocalizedErrorMessage(String pErrorCode, String pDefaultMesg)
   {
      String lvRetMessage;

      try
      {
         if (pErrorCode.equals("0") || pErrorCode.equals("999"))
         {
            lvRetMessage = pDefaultMesg;
         }
         else
         {
            lvRetMessage = (String)presentation.getErrorCodeBundle().getString(pErrorCode);
         }
      }
      catch (Exception ex)
      {
         Log.println("Presentation._showPage: missing error mapping for " + pErrorCode + ", Default = " + pDefaultMesg, Log.ERROR_LOG);

         lvRetMessage = pDefaultMesg;
      }
      return lvRetMessage;
   }
   
   /**
	 * This function return a request xml node for submit advance payment.
	 * @return xml node.
	 * @author Wind.Zhao
	 */
	private IMsgXMLNode bulidXMLNode(){
		IMsgXMLNode lvReturnXMLNode = null;
		IMsgXMLParser lvIMsgXMLParser = MsgManager.createParser();
		lvReturnXMLNode = lvIMsgXMLParser.createXMLNode("HKSWB007R01");
		lvReturnXMLNode.setAttribute("msgId", "HKSWB007R01");
		lvReturnXMLNode.setAttribute("issueTime", "20100225190637");
		lvReturnXMLNode.setAttribute("issueLoc", "888");
		lvReturnXMLNode.setAttribute("issueMetd", "01");
		lvReturnXMLNode.setAttribute("oprId", IMain.getProperty("AgentID"));
		lvReturnXMLNode.setAttribute("pwd", IMain.getProperty("AgentPassword"));
		lvReturnXMLNode.setAttribute("resvr", "0000000015");
		lvReturnXMLNode.setAttribute("language", "en");
		lvReturnXMLNode.setAttribute("country", "us");
		lvReturnXMLNode.setAttribute("retryForTimeout", "T");
		
		
		lvReturnXMLNode.addChildNode("INSTRUMENTID").setValue("00939");		
		lvReturnXMLNode.addChildNode("ENTITLEMENTID").setValue("10000292");
		lvReturnXMLNode.addChildNode("NUMBEROFOFFERSHARES").setValue("100000.0000");		
		lvReturnXMLNode.addChildNode("NUMBEROFPUBLICOFFERSHARES").setValue("100.0000");
		lvReturnXMLNode.addChildNode("MINOFFERPRICE").setValue("1.000000000");		
		lvReturnXMLNode.addChildNode("MAXOFFERPRICE").setValue("3.070000000");
		lvReturnXMLNode.addChildNode("CONFIRMEDPRICE");		
		lvReturnXMLNode.addChildNode("REMARK");
		lvReturnXMLNode.addChildNode("ENDDATE").setValue("04/07/2008");
		lvReturnXMLNode.addChildNode("INTCHANNELCUTOFFDATE").setValue("16/12/2010");		
		lvReturnXMLNode.addChildNode("ENDTIME").setValue("12:00:00");
		lvReturnXMLNode.addChildNode("INTENDTIME").setValue("12:00:00");
		lvReturnXMLNode.addChildNode("INTERESTVALUEDATE").setValue("04/07/2008");		
		lvReturnXMLNode.addChildNode("ALLOTMENTDATE").setValue("05/07/2008");
		lvReturnXMLNode.addChildNode("STATUS").setValue("0");
		lvReturnXMLNode.addChildNode("PROSPECTUS");
		lvReturnXMLNode.addChildNode("WEBSITE");
		lvReturnXMLNode.addChildNode("CHWEBSITE");
		lvReturnXMLNode.addChildNode("LISTINGDATE").setValue("27/10/2005");
		IMsgXMLNode lvLoop = lvReturnXMLNode.addChildNode("LOTSPREADDETAIL").addChildNode("LOOP");
		IMsgXMLNode lvLoop_Element1 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element1.addChildNode("APPLIEDQTY").setValue("1000.0000");
		lvLoop_Element1.addChildNode("OVERRIDEAMOUNT").setValue("3100.97");
		IMsgXMLNode lvLoop_Element2 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element2.addChildNode("APPLIEDQTY").setValue("2000.0000");
		lvLoop_Element2.addChildNode("OVERRIDEAMOUNT").setValue("6201.96");
		IMsgXMLNode lvLoop_Element3 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element3.addChildNode("APPLIEDQTY").setValue("3000.0000");
		lvLoop_Element3.addChildNode("OVERRIDEAMOUNT").setValue("9302.93");
		IMsgXMLNode lvLoop_Element4 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element4.addChildNode("APPLIEDQTY").setValue("4000.0000");
		lvLoop_Element4.addChildNode("OVERRIDEAMOUNT").setValue("12403.90");
		IMsgXMLNode lvLoop_Element5 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element5.addChildNode("APPLIEDQTY").setValue("5000.0000");
		lvLoop_Element5.addChildNode("OVERRIDEAMOUNT").setValue("15504.88");
		IMsgXMLNode lvLoop_Element6 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element6.addChildNode("APPLIEDQTY").setValue("6000.0000");
		lvLoop_Element6.addChildNode("OVERRIDEAMOUNT").setValue("18605.86");
		IMsgXMLNode lvLoop_Element7 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element7.addChildNode("APPLIEDQTY").setValue("7000.0000");
		lvLoop_Element7.addChildNode("OVERRIDEAMOUNT").setValue("21706.83");
		IMsgXMLNode lvLoop_Element8 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element8.addChildNode("APPLIEDQTY").setValue("8000.0000");
		lvLoop_Element8.addChildNode("OVERRIDEAMOUNT").setValue("24807.81");
		IMsgXMLNode lvLoop_Element9 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element9.addChildNode("APPLIEDQTY").setValue("9000.0000");
		lvLoop_Element9.addChildNode("OVERRIDEAMOUNT").setValue("3100.97");
		IMsgXMLNode lvLoop_Element10 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element10.addChildNode("APPLIEDQTY").setValue("10000.0000");
		lvLoop_Element10.addChildNode("OVERRIDEAMOUNT").setValue("31009.77");
		IMsgXMLNode lvLoop_Element11 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element11.addChildNode("APPLIEDQTY").setValue("11000.0000");
		lvLoop_Element11.addChildNode("OVERRIDEAMOUNT").setValue("34110.74");
		IMsgXMLNode lvLoop_Element12 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element12.addChildNode("APPLIEDQTY").setValue("12000.0000");
		lvLoop_Element12.addChildNode("OVERRIDEAMOUNT").setValue("37211.71");
		IMsgXMLNode lvLoop_Element13 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element13.addChildNode("APPLIEDQTY").setValue("13000.0000");
		lvLoop_Element13.addChildNode("OVERRIDEAMOUNT").setValue("40312.70");
		IMsgXMLNode lvLoop_Element14 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element14.addChildNode("APPLIEDQTY").setValue("14000.0000");
		lvLoop_Element14.addChildNode("OVERRIDEAMOUNT").setValue("43413.67");
		IMsgXMLNode lvLoop_Element15 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element15.addChildNode("APPLIEDQTY").setValue("15000.0000");
		lvLoop_Element15.addChildNode("OVERRIDEAMOUNT").setValue("3100.97");
		IMsgXMLNode lvLoop_Element16 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element16.addChildNode("APPLIEDQTY").setValue("16000.0000");
		lvLoop_Element16.addChildNode("OVERRIDEAMOUNT").setValue("49615.62");
		IMsgXMLNode lvLoop_Element17 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element17.addChildNode("APPLIEDQTY").setValue("17000.0000");
		lvLoop_Element17.addChildNode("OVERRIDEAMOUNT").setValue("52716.60");
		IMsgXMLNode lvLoop_Element18 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element18.addChildNode("APPLIEDQTY").setValue("18000.0000");
		lvLoop_Element18.addChildNode("OVERRIDEAMOUNT").setValue("55817.57");
		IMsgXMLNode lvLoop_Element19 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element19.addChildNode("APPLIEDQTY").setValue("19000.0000");
		lvLoop_Element19.addChildNode("OVERRIDEAMOUNT").setValue("58918.55");
		IMsgXMLNode lvLoop_Element20 = lvLoop.addChildNode(LOOP_ELEMENT);
		lvLoop_Element20.addChildNode("APPLIEDQTY").setValue("20000.0000");
		lvLoop_Element20.addChildNode("OVERRIDEAMOUNT").setValue("62019.53");
		
		lvReturnXMLNode.addChildNode("FEEID");
		
		return lvReturnXMLNode;
	}
}
