package com.ttl.old.itrade.hks.bean;

public class HKSMobileLoginInfo {
	private String userID;
	private String tradingAccSeq;
	private String accountSeq;
	private String accountType;
	private String fullName;
	private String lastLoginDate;
	private String lastLoginTime;
	private String lastLoginStatus;
	private String loginFailedCount;
	private String passwordExpiredDate;
	private String authenCardNo;
	
	public HKSMobileLoginInfo() {
		
	}
	
	public HKSMobileLoginInfo(String userID, String tradingAccSeq,
			String accountSeq, String accountType, String fullName,
			String lastLoginDate, String lastLoginTime, String lastLoginStatus,
			String loginFailedCount, String passwordExpiredDate) {
		super();
		this.userID = userID;
		this.tradingAccSeq = tradingAccSeq;
		this.accountSeq = accountSeq;
		this.accountType = accountType;
		this.fullName = fullName;
		this.lastLoginDate = lastLoginDate;
		this.lastLoginTime = lastLoginTime;
		this.lastLoginStatus = lastLoginStatus;
		this.loginFailedCount = loginFailedCount;
		this.passwordExpiredDate = passwordExpiredDate;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getTradingAccSeq() {
		return tradingAccSeq;
	}
	public void setTradingAccSeq(String tradingAccSeq) {
		this.tradingAccSeq = tradingAccSeq;
	}
	public String getAccountSeq() {
		return accountSeq;
	}
	public void setAccountSeq(String accountSeq) {
		this.accountSeq = accountSeq;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getLastLoginDate() {
		return lastLoginDate;
	}
	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getLastLoginStatus() {
		return lastLoginStatus;
	}
	public void setLastLoginStatus(String lastLoginStatus) {
		this.lastLoginStatus = lastLoginStatus;
	}
	public String getLoginFailedCount() {
		return loginFailedCount;
	}
	public void setLoginFailedCount(String loginFailedCount) {
		this.loginFailedCount = loginFailedCount;
	}
	public String getPasswordExpiredDate() {
		return passwordExpiredDate;
	}
	public void setPasswordExpiredDate(String passwordExpiredDate) {
		this.passwordExpiredDate = passwordExpiredDate;
	}

	public String getAuthenCardNo() {
		return authenCardNo;
	}

	public void setAuthenCardNo(String authenCardNo) {
		this.authenCardNo = authenCardNo;
	}
	
	
}
