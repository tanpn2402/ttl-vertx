package com.ttl.old.itrade.hks.bean;

/**
 * The HKSChangePasswordBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSChangePasswordBean {
	private String pClientID;
	private String pClientName;
	private String pInstrumentID;
	private String pPriceValue;
	private String pDateTime;
	private String pQuantityValue;
	private String pFirstTimeChangePassword;
	private String pAALoginURL;
	private String pBuySell;
	private String pPasswordMinLength;
	private String pPasswordMaxLength;
	private String pDisplayneedchangepasswordStart;
	private String pDisplayneedchangepasswordEnd;
	private String pErrorMsg;
	private String pData;
	
	private String mvExpiredDate = "";
	
	public String getExpiredDate() {
		return mvExpiredDate;
	}

	public void setExpiredDate(String pExpiredDate) {
		this.mvExpiredDate = pExpiredDate;
	}
	
	 /**
     * This method returns the information of change password or not form action.
     * @return the information of change password or not.
     * @type String.
     */
	public String getPData() {
		return pData;
	}
	
	/**
     * This method sets the information of change password or not form action.
     * @param pData The information of change password or not.
     */
	public void setPData(String pData) {
		this.pData = pData;
	}
	
	/**
     * This method returns the client id.
     * @return the client id.
     */
	public String getPClientID() {
		return pClientID;
	}
	
	/**
     * This method sets client id.
     * @param pClientID The client id.
     */
	public void setPClientID(String pClientID) {
		this.pClientID = pClientID;
	}
	
	/**
     * This method returns the client name.
     * @return the client name.
     */
	public String getPClientName() {
		return pClientName;
	}
	
	/**
     * This method sets client name.
     * @param pClientName The client name.
     */
	public void setPClientName(String pClientName) {
		this.pClientName = pClientName;
	}
	
	/**
     * This method returns the instrument id.
     * @return the instrument id.
     */
	public String getPInstrumentID() {
		return pInstrumentID;
	}
	
	/**
     * This method sets the instrument id.
     * @param pInstrumentID The instrument id.
     */
	public void setPInstrumentID(String pInstrumentID) {
		this.pInstrumentID = pInstrumentID;
	}
	
	/**
     * This method returns the price value.
     * @return the price value.
     */
	public String getPPriceValue() {
		return pPriceValue;
	}
	
	/**
     * This method sets the price value.
     * @param pPriceValue The price value.
     */
	public void setPPriceValue(String pPriceValue) {
		this.pPriceValue = pPriceValue;
	}
	
	/**
     * This method returns the date time.
     * @return the date time.
     */
	public String getPDateTime() {
		return pDateTime;
	}
	
	/**
     * This method sets the date time.
     * @param pDateTime The date time.
     */
	public void setPDateTime(String pDateTime) {
		this.pDateTime = pDateTime;
	}
	
	/**
     * This method returns the quantity value.
     * @return the quantity value.
     */
	public String getPQuantityValue() {
		return pQuantityValue;
	}
	
	/**
     * This method sets the quantity value.
     * @param pQuantityValue The quantity value.
     */
	public void setPQuantityValue(String pQuantityValue) {
		this.pQuantityValue = pQuantityValue;
	}
	
	/**
     * This method returns the if first time change password.
     * @return the if first time change password.
     */
	public String getPFirstTimeChangePassword() {
		return pFirstTimeChangePassword;
	}
	
	/**
     * This method sets the if first time change password.
     * @param pFirstTimeChangePassword The if first time change password.
     */
	public void setPFirstTimeChangePassword(String pFirstTimeChangePassword) {
		this.pFirstTimeChangePassword = pFirstTimeChangePassword;
	}
	
	/**
     * This method returns the aastock login url.
     * @return the aastock login url .
     */
	public String getPAALoginURL() {
		return pAALoginURL;
	}
	
	/**
     * This method sets the aastock login url.
     * @param pAALoginURL The aastock login url.
     */
	public void setPAALoginURL(String pAALoginURL) {
		this.pAALoginURL = pAALoginURL;
	}
	
	/**
     * This method returns the buy or sell.
     * @return the buy or sell.
     */
	public String getPBuySell() {
		return pBuySell;
	}
	
	/**
     * This method sets the buy or sell.
     * @param pBuySell The buy or sell.
     */
	public void setPBuySell(String pBuySell) {
		this.pBuySell = pBuySell;
	}
	
	/**
     * This method returns the password min length.
     * @return the password min length.
     */
	public String getPPasswordMinLength() {
		return pPasswordMinLength;
	}
	
	/**
     * This method sets the password min length.
     * @param pPasswordMinLength The password min length.
     */
	public void setPPasswordMinLength(String pPasswordMinLength) {
		this.pPasswordMinLength = pPasswordMinLength;
	}
	
	/**
     * This method returns the password max length.
     * @return the password max length.
     */
	public String getPPasswordMaxLength() {
		return pPasswordMaxLength;
	}
	
	/**
     * This method sets the password max length.
     * @param pPasswordMaxLength The password max length.
     */
	public void setPPasswordMaxLength(String pPasswordMaxLength) {
		this.pPasswordMaxLength = pPasswordMaxLength;
	}
	
	/**
     * This method returns the if need to change password
     * width getPDisplayneedchangepasswordEnd().
     * @return the if need to change password.
     */
	public String getPDisplayneedchangepasswordStart() {
		return pDisplayneedchangepasswordStart;
	}
	
	/**
     * This method sets the if need to change password
     * width setPDisplayneedchangepasswordEnd().
     * @param pDisplayneedchangepasswordStart The if need to change password.
     */
	public void setPDisplayneedchangepasswordStart(
			String pDisplayneedchangepasswordStart) {
		this.pDisplayneedchangepasswordStart = pDisplayneedchangepasswordStart;
	}
	
	/**
     * This method returns the if need to change password
     * * width getPDisplayneedchangepasswordStart().
     * @return the if need to change password.
     */
	public String getPDisplayneedchangepasswordEnd() {
		return pDisplayneedchangepasswordEnd;
	}
	
	/**
     * This method sets the if need to change password
     * * width setPDisplayneedchangepasswordStart().
     * @param pDisplayneedchangepasswordEnd The if need to change password.
     */
	public void setPDisplayneedchangepasswordEnd(String pDisplayneedchangepasswordEnd) {
		this.pDisplayneedchangepasswordEnd = pDisplayneedchangepasswordEnd;
	}
	
	/**
     * This method returns the error message when change password.
     * @return the error message.
     */
	public String getPErrorMsg() {
		return pErrorMsg;
	}
	
	/**
     * This method sets the error message when change password
     * @param pErrorMsg The error message.
     */
	public void setPErrorMsg(String pErrorMsg) {
		this.pErrorMsg = pErrorMsg;
	}
	
}