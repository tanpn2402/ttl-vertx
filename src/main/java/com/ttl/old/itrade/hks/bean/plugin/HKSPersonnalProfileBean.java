/**
 * 
 */
package com.ttl.old.itrade.hks.bean.plugin;

import java.util.List;

/**
 * @author Khanh Nguyen
 *
 */

public class HKSPersonnalProfileBean {
	
	private List mvAgentList;
	
	private String mvName;
	private String mvAccountNumber ;
	private String mvPhoneNumber ;
	private String mvEmail;
	private String mvIDNumber;
	private String mvAddress;
	
	/**
	 *  Set method for agent list list
	 * @param pAgentList the agent list
	 */
	public void setMvAgentList(List pAgentList) {
		this.mvAgentList = pAgentList;
	}

	/**
	 * @return the mvAgentList
	 */
	public List getMvAgentList() {
		return mvAgentList;
	}

	/**
	 * @param mvName the mvName to set
	 */
	public void setMvName(String mvName) {
		this.mvName = mvName;
	}

	/**
	 * @return the mvName
	 */
	public String getMvName() {
		return mvName;
	}

	/**
	 * @param mvAccountNumber the mvAccountNumber to set
	 */
	public void setMvAccountNumber(String mvAccountNumber) {
		this.mvAccountNumber = mvAccountNumber;
	}

	/**
	 * @return the mvAccountNumber
	 */
	public String getMvAccountNumber() {
		return mvAccountNumber;
	}

	/**
	 * @param mvPhoneNumber the mvPhoneNumber to set
	 */
	public void setMvPhoneNumber(String mvPhoneNumber) {
		this.mvPhoneNumber = mvPhoneNumber;
	}

	/**
	 * @return the mvPhoneNumber
	 */
	public String getMvPhoneNumber() {
		return mvPhoneNumber;
	}

	/**
	 * @param mvEmail the mvEmail to set
	 */
	public void setMvEmail(String mvEmail) {
		this.mvEmail = mvEmail;
	}

	/**
	 * @return the mvEmail
	 */
	public String getMvEmail() {
		return mvEmail;
	}

	/**
	 * @param mvIDNumber the mvIDNumber to set
	 */
	public void setMvIDNumber(String mvIDNumber) {
		this.mvIDNumber = mvIDNumber;
	}

	/**
	 * @return the mvIDNumber
	 */
	public String getMvIDNumber() {
		return mvIDNumber;
	}

	/**
	 * @param mvAddress the mvAddress to set
	 */
	public void setMvAddress(String mvAddress) {
		this.mvAddress = mvAddress;
	}

	/**
	 * @return the mvAddress
	 */
	public String getMvAddress() {
		return mvAddress;
	}

}
