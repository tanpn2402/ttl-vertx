package com.ttl.old.itrade.hks.txn;

public class HKSPortfolioTradingAccSummaryDetails {

	private String mvClientID;
	private int mvTradingAccSeq;
	private String mvProductID;
	private String mvInvestorGroupID;
	private String mvInvestorTypeID;
	private String mvInvestorClassID;
	private String mvAEID;
	private String mvCurrencyID;
	private String mvMarginValueF;
	private String mvMarginValue;
	private String mvMarketValueF;
	private String mvMarketValue;
	private String mvLedgerMarginValue;
	private String mvCashValuef;
	private String mvCashValue;
	private String mvAccruedInterest;
	private String mvMarginPositionf;
	private String mvMarginPosition;
	private String mvMarginableValueF;
	private String mvMarginableValue;
	private String mvTradableBalf;
	private String mvTradableBal;
	private String mvCreditLineFlag;
	private String mvCreditLine;
	private String mvVirtualChannelTradableBalF;
	private String mvVirtualChannelTradableBal;
	private String mvVirtualChannelCreditLineFlag;
	private String mvVirtualChannelCreditLine;
	private String mvCreditLimit;
	private String mvClientName;
	private String mvMaintenanceValue;
	private String mvMaintenanceValueF;
	private String mvLoanOutStanding;
	private String mvMarginCall;
	private String mvMarginCallF;
	private String mvLoanToMarketValuePercentage;
	private String mvWAPPhoneNumber;
	private String mvPhoneNumber;
	private String mvMobileNumber;
	private String mvPagerNumber;
	private String mvJointName;
	private String mvShortStockValue;
	private String mvShortStockValueF;
	private String mvMaintenanceRatio;

	private String mvAvailableBalance;
	private String mvMarginPercentage;
	private String mvBuyingPowerd;
	private String mvTPlusBuyingPower;
	private String mvExtraCreditd;
	private String mvRemaining;
	private String mvTPlusXSOFValue;
	
	private String equity;
	private String totalAsset;
	private String stockMaintenance;
	private String cashMaintenance;
	private String clientMaintenancePercent;
	private String supplementCash;

	public HKSPortfolioTradingAccSummaryDetails() {
	}

	public String getClientID() {
		return this.mvClientID;
	}

	public int getTradingAccSeq() {
		return this.mvTradingAccSeq;
	}

	public String getProductID() {
		return this.mvProductID;
	}

	public String getInvestorGroupID() {
		return this.mvInvestorGroupID;
	}

	public String getInvestorTypeID() {
		return this.mvInvestorTypeID;
	}

	public String getInvestorClassID() {
		return this.mvInvestorClassID;
	}

	public String getAEID() {
		return this.mvAEID;
	}

	public String getCurrencyID() {
		return this.mvCurrencyID;
	}

	public String getMarginValueF() {
		return this.mvMarginValueF;
	}

	public String getMarginValue() {
		return this.mvMarginValue;
	}

	public String getLedgerMarginValue() {
		return this.mvLedgerMarginValue;
	}

	public String getMarginableValueF() {
		return this.mvMarginableValueF;
	}

	public String getMarginableValue() {
		return this.mvMarginableValue;
	}

	public String getMarketValueF() {
		return this.mvMarketValueF;
	}

	public String getMarketValue() {
		return this.mvMarketValue;
	}

	public String getCashValuef() {
		return this.mvCashValuef;
	}

	public String getCashValue() {
		return this.mvCashValue;
	}

	public String getAccruedInterest() {
		return this.mvAccruedInterest;
	}

	public String getMarginPositionf() {
		return this.mvMarginPositionf;
	}

	public String getMarginPosition() {
		return this.mvMarginPosition;
	}

	public String getTradableBalf() {
		return this.mvTradableBalf;
	}

	public String getTradableBal() {
		return this.mvTradableBal;
	}

	public String getCreditLineFlag() {
		return this.mvCreditLineFlag;
	}

	public String getCreditLine() {
		return this.mvCreditLine;
	}

	public String getVirtualChannelTradableBalF() {
		return this.mvVirtualChannelTradableBalF;
	}

	public String getVirtualChannelTradableBal() {
		return this.mvVirtualChannelTradableBal;
	}

	public String getVirtualChannelCreditLineFlag() {
		return this.mvVirtualChannelCreditLineFlag;
	}

	public String getVirtualChannelCreditLine() {
		return this.mvVirtualChannelCreditLine;
	}

	public String getCreditLimit() {
		return this.mvCreditLimit;
	}

	public String getClientName() {
		return this.mvClientName;
	}

	public String getMaintenanceValue() {
		return this.mvMaintenanceValue;
	}

	public String getMaintenanceValueF() {
		return this.mvMaintenanceValueF;
	}

	public String getMaintenanceRatio() {
		return this.mvMaintenanceRatio;
	}

	public String getLoanOutStanding() {
		return this.mvLoanOutStanding;
	}

	public String getMarginCall() {
		return this.mvMarginCall;
	}

	public String getMarginCallF() {
		return this.mvMarginCallF;
	}

	public String getLoanToMarketValuePercentage() {
		return this.mvLoanToMarketValuePercentage;
	}

	public String getWAPPhoneNumber() {
		return this.mvWAPPhoneNumber;
	}

	public String getPhoneNumber() {
		return this.mvPhoneNumber;
	}

	public String getMobileNumber() {
		return this.mvMobileNumber;
	}

	public String getPagerNumber() {
		return this.mvPagerNumber;
	}

	public String getJointName() {
		return this.mvJointName;
	}

	public void setClientID(String paramString) {
		this.mvClientID = paramString;
	}

	public void setTradingAccSeq(int paramInt) {
		this.mvTradingAccSeq = paramInt;
	}

	public void setProductID(String paramString) {
		this.mvProductID = paramString;
	}

	public void setInvestorGroupID(String paramString) {
		this.mvInvestorGroupID = paramString;
	}

	public void setInvestorTypeID(String paramString) {
		this.mvInvestorTypeID = paramString;
	}

	public void setInvestorClassID(String paramString) {
		this.mvInvestorClassID = paramString;
	}

	public void setAEID(String paramString) {
		this.mvAEID = paramString;
	}

	public void setCurrencyID(String paramString) {
		this.mvCurrencyID = paramString;
	}

	public void setMarginValueF(String paramString) {
		this.mvMarginValueF = paramString;
	}

	public void setMarginValue(String paramString) {
		this.mvMarginValue = paramString;
	}

	public void setLedgerMarginValue(String paramString) {
		this.mvLedgerMarginValue = paramString;
	}

	public void setMarginableValueF(String paramString) {
		this.mvMarginableValueF = paramString;
	}

	public void setMarginableValue(String paramString) {
		this.mvMarginableValue = paramString;
	}

	public void setMarketValueF(String paramString) {
		this.mvMarketValueF = paramString;
	}

	public void setMarketValue(String paramString) {
		this.mvMarketValue = paramString;
	}

	public void setCashValuef(String paramString) {
		this.mvCashValuef = paramString;
	}

	public void setCashValue(String paramString) {
		this.mvCashValue = paramString;
	}

	public void setAccruedInterest(String paramString) {
		this.mvAccruedInterest = paramString;
	}

	public void setMarginPositionf(String paramString) {
		this.mvMarginPositionf = paramString;
	}

	public void setMarginPosition(String paramString) {
		this.mvMarginPosition = paramString;
	}

	public void setTradableBalf(String paramString) {
		this.mvTradableBalf = paramString;
	}

	public void setTradableBal(String paramString) {
		this.mvTradableBal = paramString;
	}

	public void setCreditLineFlag(String paramString) {
		this.mvCreditLineFlag = paramString;
	}

	public void setVirtualChannelTradableBalF(String paramString) {
		this.mvVirtualChannelTradableBalF = paramString;
	}

	public void setVirtualChannelTradableBal(String paramString) {
		this.mvVirtualChannelTradableBal = paramString;
	}

	public void setVirtualChannelCreditLineFlag(String paramString) {
		this.mvVirtualChannelCreditLineFlag = paramString;
	}

	public void setVirtualChannelCreditLine(String paramString) {
		this.mvVirtualChannelCreditLine = paramString;
	}

	public void setCreditLine(String paramString) {
		this.mvCreditLine = paramString;
	}

	public void setCreditLimit(String paramString) {
		this.mvCreditLimit = paramString;
	}

	public void setClientName(String paramString) {
		this.mvClientName = paramString;
	}

	public void setMaintenanceValueF(String paramString) {
		this.mvMaintenanceValueF = paramString;
	}

	public void setMaintenanceValue(String paramString) {
		this.mvMaintenanceValue = paramString;
	}

	public void setMaintenanceRatio(String paramString) {
		this.mvMaintenanceRatio = paramString;
	}

	public void setLoanOutStanding(String paramString) {
		this.mvLoanOutStanding = paramString;
	}

	public void setMarginCall(String paramString) {
		this.mvMarginCall = paramString;
	}

	public void setMarginCallF(String paramString) {
		this.mvMarginCallF = paramString;
	}

	public void setLoanToMarketValuePercentage(String paramString) {
		this.mvLoanToMarketValuePercentage = paramString;
	}

	public void setWAPPhoneNumber(String paramString) {
		this.mvWAPPhoneNumber = paramString;
	}

	public void setPhoneNumber(String paramString) {
		this.mvPhoneNumber = paramString;
	}

	public void setMobileNumber(String paramString) {
		this.mvMobileNumber = paramString;
	}

	public void setPagerNumber(String paramString) {
		this.mvPagerNumber = paramString;
	}

	public void setJointName(String paramString) {
		this.mvJointName = paramString;
	}

	public String getShortStockValue() {
		return this.mvShortStockValue;
	}

	public void setShortStockValue(String paramString) {
		this.mvShortStockValue = paramString;
	}

	public String getShortStockValueF() {
		return this.mvShortStockValueF;
	}

	public void setShortStockValueF(String paramString) {
		this.mvShortStockValueF = paramString;
	}

	/**
	 * @return the mvAvailableBalance
	 */
	public String getMvAvailableBalance() {
		return mvAvailableBalance;
	}

	/**
	 * @param mvAvailableBalance
	 *            the mvAvailableBalance to set
	 */
	public void setMvAvailableBalance(String mvAvailableBalance) {
		this.mvAvailableBalance = mvAvailableBalance;
	}

	/**
	 * @return the mvMarginPercentage
	 */
	public String getMvMarginPercentage() {
		return mvMarginPercentage;
	}

	/**
	 * @param mvMarginPercentage
	 *            the mvMarginPercentage to set
	 */
	public void setMvMarginPercentage(String mvMarginPercentage) {
		this.mvMarginPercentage = mvMarginPercentage;
	}

	/**
	 * @return the mvBuyingPowerd
	 */
	public String getMvBuyingPowerd() {
		return mvBuyingPowerd;
	}

	/**
	 * @param mvBuyingPowerd
	 *            the mvBuyingPowerd to set
	 */
	public void setMvBuyingPowerd(String mvBuyingPowerd) {
		this.mvBuyingPowerd = mvBuyingPowerd;
	}

	/**
	 * @return the mvExtraCreditd
	 */
	public String getMvExtraCreditd() {
		return mvExtraCreditd;
	}

	/**
	 * @param mvExtraCreditd
	 *            the mvExtraCreditd to set
	 */
	public void setMvExtraCreditd(String mvExtraCreditd) {
		this.mvExtraCreditd = mvExtraCreditd;
	}

	/**
	 * @return the mvRemaining
	 */
	public String getMvRemaining() {
		return mvRemaining;
	}

	/**
	 * @param mvRemaining
	 *            the mvRemaining to set
	 */
	public void setMvRemaining(String mvRemaining) {
		this.mvRemaining = mvRemaining;
	}

	/**
	 * @return the mvTPlusBuyingPower
	 */
	public String getMvTPlusBuyingPower() {
		return mvTPlusBuyingPower;
	}

	/**
	 * @param mvTPlusBuyingPower the mvTPlusBuyingPower to set
	 */
	public void setMvTPlusBuyingPower(String mvTPlusBuyingPower) {
		this.mvTPlusBuyingPower = mvTPlusBuyingPower;
	}

	/**
	 * @return the mvTPlusXSOFValue
	 */
	public String getMvTPlusXSOFValue() {
		return mvTPlusXSOFValue;
	}

	/**
	 * @param mvTPlusXSOFValue the mvTPlusXSOFValue to set
	 */
	public void setMvTPlusXSOFValue(String mvTPlusXSOFValue) {
		this.mvTPlusXSOFValue = mvTPlusXSOFValue;
	}

	public String getEquity() {
		return equity;
	}

	public void setEquity(String equity) {
		this.equity = equity;
	}

	public String getTotalAsset() {
		return totalAsset;
	}

	public void setTotalAsset(String totalAsset) {
		this.totalAsset = totalAsset;
	}

	public String getStockMaintenance() {
		return stockMaintenance;
	}

	public void setStockMaintenance(String stockMaintenance) {
		this.stockMaintenance = stockMaintenance;
	}

	public String getCashMaintenance() {
		return cashMaintenance;
	}

	public void setCashMaintenance(String cashMaintenance) {
		this.cashMaintenance = cashMaintenance;
	}

	public String getClientMaintenancePercent() {
		return clientMaintenancePercent;
	}

	public void setClientMaintenancePercent(String clientMaintenancePercent) {
		this.clientMaintenancePercent = clientMaintenancePercent;
	}

	public String getSupplementCash() {
		return supplementCash;
	}

	public void setSupplementCash(String supplementCash) {
		this.supplementCash = supplementCash;
	}
	
}
