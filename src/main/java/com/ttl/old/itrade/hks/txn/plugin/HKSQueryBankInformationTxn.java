package com.ttl.old.itrade.hks.txn.plugin;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSQueryBankInformationTxn class handle query bank information by client id.
 * 
 * @author not Wind.Zhao
 * 
 */

// Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSQueryBankInformationTxn extends BaseTxn {
	private String mvClientID;
	private String mvTradingAccSeq;

	/**
	 * Set method for client id
	 * 
	 * @param pClientID the client id
	 */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}

	/**
	 * Get method for client id
	 * 
	 * @return client id
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * 
	 * @return
	 */
	public String getMvTradingAccSeq() {
		return mvTradingAccSeq;
	}

	/**
	 * 
	 * @param mvTradingAccSeq
	 */
	public void setMvTradingAccSeq(String mvTradingAccSeq) {
		this.mvTradingAccSeq = mvTradingAccSeq;
	}

	/**
	 * Constructor for HKSQueryBankInformationTxn class
	 * 
	 * @param [1]pClientID the client id.
	 * @author Wind.Zhao
	 */
	public HKSQueryBankInformationTxn(String pClientID, String pTradingAccSeq) {
		mvClientID = pClientID;
		mvTradingAccSeq = pTradingAccSeq;
	}

	/**
	 * The method for bank information processing
	 * 
	 * @return the list of all the bank information of the client.
	 * @author Wind.Zhao
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List process() {

		// Send TP request
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put(TagName.TRADINGACCSEQ, mvTradingAccSeq);

		ArrayList lvResultList = null;

		if (TPErrorHandling.TP_NORMAL == process("QueryBankInfo", lvTxnMap)) {
			lvResultList = new ArrayList();
			Hashtable lvBankInfoMap = null;

			IMsgXMLNodeList lvBankList = mvReturnNode.getNodeList("ChildRow");
			for (int i = 0; i < lvBankList.size(); i++) {
				lvBankInfoMap = new Hashtable();
				IMsgXMLNode lvBanInfoNode = lvBankList.getNode(i);
				// check if bank have a valid value, for Ex: <BANKID/>
				if (lvBanInfoNode.getChildNode("BANKID").getValue().length() > 0) {
					lvBankInfoMap.put("BANKID", lvBanInfoNode.getChildNode("BANKID").getValue());
					lvBankInfoMap.put("BANKACID", lvBanInfoNode.getChildNode("BANKACID").getValue());
					lvBankInfoMap.put("ISDEFAULT", lvBanInfoNode.getChildNode("ISDEFAULT").getValue());
					lvBankInfoMap.put("INTERFACESEQ", lvBanInfoNode.getChildNode("INTERFACESEQ").getValue());
					lvResultList.add(lvBankInfoMap);
				}
			}
		} else {
			setErrorCode(new ErrorCode(new String[0], mvReturnNode.getChildNode("C_ERROR_CODE").getValue(), mvReturnNode.getChildNode(
					"C_ERROR_DESC").getValue()));
		}

		return lvResultList;
	}
}
// End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module