// ----------------------------------------
// Modified by Bowen Chau on 7 Mar 2006
// Added account registration request name
// ----------------------------------------


package com.ttl.old.itrade.hks.request;

/**
 * The RequestName class defined that some constant for request name.
 * @author
 *
 */
public class RequestName
{
   public final static String HKSLoginRequest = "HKSLoginRequest";
   public final static String HKSLogoutRequest = "HKSLogoutRequest";
   public final static String HKSRegisterDN = "HKSRegisterDN";

   public final static String HKSPlaceOrderRequest = "HKSPlaceOrderRequest";
   public final static String HKSPlaceOrderRequest_TW = "HKSPlaceOrderRequest_TW";
   public final static String HKSPlaceOrderRequest_CN = "HKSPlaceOrderRequest_CN";
   public final static String HKSPlaceOrderRequest_VI = "HKSPlaceOrderRequest_VI";

   public final static String HKSDownloadInstrumentRequest = "HKSDownloadInstrumentRequest";
   public final static String HKSDownloadTradeDateRequest = "HKSDownloadTradeDateRequest";

   public final static String HKSCheckTPStatusRequest = "HKSCheckTPStatusRequest";

   public final static String HKSOperatorLoginRequest = "HKSOperatorLoginRequest";

   public final static String HKSChangePasswordRequest = "HKSChangePasswordRequest";

   public final static String HKSResetPasswordRequest = "HKSResetPasswordRequest";

   public final static String HKSInformationEnquiryRequest = "HKSInformationEnquiryRequest";

   public final static String HKSDisclaimerAgreementRequest = "HKSDisclaimerAgreementRequest";

   public final static String HKSOrderEnquiryRequest = "HKSOrderEnquiryRequest";
   public final static String HKSTransactionHistoryRequest = "HKSTransactionHistoryRequest";

   public final static String HKSModifyOrderRequest = "HKSModifyOrderRequest";
   public final static String HKSModifyOrderRequest_TW = "HKSModifyOrderRequest_TW";
   public final static String HKSModifyOrderRequest_CN = "HKSModifyOrderRequest_CN";
   public final static String HKSModifyOrderRequest_VI = "HKSModifyOrderRequest_VI";

   public final static String HKSClientPortfolioEnquiry = "HKSClientPortfolioEnquiry";
   public final static String HKSClientAccountBalanceEnquiry = "HKSClientAccountBalanceEnquiry";
   
   public final static String HKSGetStockBalanceCashInfo = "HKSGetStockBalanceCashInfo";

   public final static String HKSFundTransferSubmitRequest = "HKSFundTransferSubmitRequest";
   public final static String HKSFundTransferRequest = "HKSFundTransferRequest";
   public final static String HKSFundTransferQuery = "HKSFundTransferQuery";

   public final static String HKSIPOOpeningListEnquiry = "HKSIPOOpeningListEnquiry";
   public final static String HKSIPODetailsRequest = "HKSIPODetailsRequest";
   public final static String HKSIPOApplStatusRequest = "HKSIPOApplStatusRequest";
   public final static String HKSIPOConfirmDataRequest = "HKSIPOConfirmDataRequest";
   public final static String HKSIPOConfirmDataRequest_TW = "HKSIPOConfirmDataRequest_TW";



   // BEGIN - Task #: WL00637 - Walter Lau - 9 Oct 2007
   public final static String HKSScripOptionStatusRequest = "HKSScripOptionStatusRequest";
   public final static String HKSScripOptionSubmitRequest = "HKSScripOptionSubmitRequest";
   public final static String HKSScripOptionDetailsRequest = "HKSScripOptionDetailsRequest";
   public final static String HKSScripOptionConfirmDataRequest = "HKSScripOptionConfirmDataRequest";

   public final static String HKSExerciseStatusRequest = "HKSExerciseStatusRequest";
   public final static String HKSExerciseSubmitRequest = "HKSExerciseSubmitRequest";
   public final static String HKSExerciseDetailsRequest = "HKSExerciseDetailsRequest";
   public final static String HKSExerciseConfirmDataRequest = "HKSExerciseConfirmDataRequest";
   // END - Task #: WL00637 - Walter Lau - 9 Oct 2007


   public final static String HKSOrderHistoryEnquiry = "HKSOrderHistoryEnquiry";
   public final static String HKSTradeEnquiry = "HKSTradeEnquiry";
   public final static String HKSOrderDetailsEnquiry = "HKSOrderDetailsEnquiry";
   public final static String HKSPriceAlertSubscriptionRequest = "HKSPriceAlertSubscriptionRequest";
   public final static String HKSPriceAlertEnquiry = "HKSPriceAlertEnquiry";
   public final static String HKSPriceAlertDeleteRequest = "HKSPriceAlertDeleteRequest";
   public final static String HKSEMessageEnquiry = "HKSEMessageEnquiry";
   public final static String HKSInstrumentNewsAndResearchReportEnquiry = "HKSInstrumentNewsAndResearchReportEnquiry";
   public final static String HKSResearchReportsEnquiry = "HKSResearchReportsEnquiry";
   public final static String HKSEMessageDeleteRequest = "HKSEMessageDeleteRequest";

   public final static String HKSContactUsRequest = "HKSContactUsRequest";
   public final static String HKSAccountRegistrationRequest = "HKSAccountRegistrationRequest";

   //BEGIN Task #: WL00619 Walter Lau 27 July 2007
   public final static String HKSDownloadMarketRequest = "HKSDownloadMarketRequest";
   //END Task #: WL00619 Walter Lau 27 July 2007

   //BEGIN TASK#: KN00008 KEVIN NG 20071218
   public final static String HKSMarketDataRequest = "HKSMarketDataRequest";
   //END TASK#: KN00008

// BEGIN TASK#: KN00008 KEVIN NG 20071229
   public final static String HKSMarketDataDetailRequest = "HKSMarketDataDetailRequest";
   //END TASK#: KN00008

   // BEGIN - TASK#: ST00062 - Sammul Tse 20080103
   public final static String HKSMarketIndexRequest = "HKSMarketIndexRequest";
   public final static String HKSNewsReportRequest = "HKSNewsReportRequest";
   // END - TASK#: ST00062

   // BEGIN Task: RN00027 Ricky Ngan 20080718
   public final static String HKSCINORequest = "HKSCINORequest";
   public final static String HKSCheckMultileClientIDRequest = "HKSCheckMultileClientIDRequest";
   public final static String HKSWSQuerySTAccountNoRequest = "HKSWSQuerySTAccountNoRequest";
   public final static String HKSWSCheckPasswordRequest = "HKSWSCheckPasswordRequest";

   // END Task: RN00027

   //BEGIN Task: al00001 albert lai 20080813
   public final static String HKSClientBankBalanceEnquiry = "HKSClientBankBalanceEnquiry";
   //END Task: al00001 albert lai 20080813

   //BEGIN Task: CL00040 Charlie 20081108
   public final static String HKSCommissionRateRequest = "HKSCommissionRateRequest";
   //END Task: CL00040

   // BEGIN Task: RN00028 Ricky Ngan 20080813
   public final static String HKSMarginFixedChargeRequest = "HKSMarginFixedChargeRequest";
   public final static String HKSMarginPlanRequest = "HKSMarginPlanRequest";
   public final static String HKSMarginLendingPercentageRequest = "HKSMarginLendingPercentageRequest";
   // END Task: RN00028

   // BEGIN - Task# : WL00702 - Walter Lau 20080820
   public final static String HKSUserIDSetupRequest = "HKSUserIDSetupRequest";
   public final static String HKSGenSingleSignOnUserIDSetupPageRequest = "HKSGenSingleSignOnUserIDSetupPageRequest";

   // END - Task# : WL00702 - Walter Lau 20080820

   // BEGIN RN00028 Ricky Ngan 20080827
   public final static String HKSMarginInterestRatePlanFeeRequest = "HKSMarginInterestRatePlanFeeRequest";
   // END RN00028

   // BEGIN TASK #: -TTL-GZ-WSJ-00002 SHAOJIAN WAN 2009-12-05 [iTradeR5] Import/input transactions 
   public final static String HKSNewAddPortfolioFileInput = "HKSNewAddPortfolioFileInput";
   // END TASK #: -TTL-GZ-WSJ-00002 SHAOJIAN WAN 2009-12-05 [iTradeR5] Import/input transactions
   
   //Begin Task #: TTL-GZ-ZZW-00010 Wind Zhao 20091125 for [ITradeR5]Advance payment online
   public final static String HKSOnLineAdvancePaymentRequest = "HKSOnLineAdvancePaymentRequest";
   //End Task #: TTL-GZ-ZZW-00010 Wind Zhao 20091125 for [ITradeR5]Advance payment online
   
   //Begin Task #: TTL VanTran 20100412 for [ITradeR5]Cash Transaction history
   public final static String HKSCashTransactionHistoryRequest = "HKSCashTransactionHistoryRequest";
   //Begin Task #: TTL VanTran 20100412 for [ITradeR5]Cash Transaction history
   
   //Begin Task #: TTL Khanh Nguyen 20110329 for [ITradeR5]MarketIndexHistoryDataRequest
   public final static String HKSMarketIndexHistoryDataRequest = "HKSMarketIndexHistoryDataRequest";
   //Begin Task #: TTL Khanh Nguyen 20110329 for [ITradeR5]MarketIndexHistoryDataRequest
   
   
   //Begin Task #: TTL Khanh Nguyen 20110512 for HKSUIManagementRequest
   public final static String HKSUIManagementRequest = "HKSUIManagementRequest";
   //End Task #: TTL Khanh Nguyen 20110512 for HKSUIManagementRequest
   
   
   //Begin Task #: TTL Khanh Nguyen 20110607 for HKSPortfolioEnquiryRequest
   public final static String HKSPortfolioEnquiryRequest = "HKSPortfolioEnquiryRequest";
   //End Task #: TTL Khanh Nguyen 20110607 for HKSPortfolioEnquiryRequest
   
   public static final String OrderHistoryEnquiry = "OrderHistoryEnquiry";
   
   // START TASK #3939 17-01-2013 TTL-VN Viet.Pham: Control Customer Service
   public static final String ItradeCustomerService = "ItradeCustomerService";
   // END TASK #3939 17-01-2013 TTL-VN Viet.Pham: Control Customer Service
   
   // BEGIN Task: WTRAD-4[http://tts.tx-tech.com/devtrack/browse/WTRAD-4] TanPham 20170706
   public static final String HKSSignOrderEnquiry = "HKSSignOrderEnquiry";
   public static final String SignOrderEnquiry = "SignOrderEnquiry";
   // END Task: WTRAD-4
   
}
