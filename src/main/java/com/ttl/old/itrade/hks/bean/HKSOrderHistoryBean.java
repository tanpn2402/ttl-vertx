package com.ttl.old.itrade.hks.bean;

/**
 * The HKSOrderHistoryBean class define variables that to save values 
 * for action 
 * @author 
 *
 */
public class HKSOrderHistoryBean {
	private String orderGroupId;
	private String marketId;
	private String stockCode;
	private String stockName;
	private String buySell;
	private String price;
	private String qty;
	private String cancelQty;
	private String outstandingQty;
	private String exeQty;
	private String avgPrice;
	private String media;
	private String status;
	private String orderType;
	private String shortOrderType;
	private String stopPrice;
	private String rejReason;
	private String inputTime;
	private String goodTillDate;
	private String estNetAmount;

	/**
     * This method returns the cancel quantity
     * @return the cancel quantity.
     */
	public String getCancelQty() {
		return cancelQty;
	}
	
	/**
     * This method sets the cancel quantity
     * @param pCancelQty The cancel quantity.
     */
	public void setCancelQty(String pCancelQty) {
		cancelQty = pCancelQty;
	}
	
	/**
     * This method returns the out standing quantity
     * @return the out standing quantity.
     */
	public String getOutstandingQty() {
		return outstandingQty;
	}
	
	/**
     * This method sets the out standing quantity
     * @param pOutstandingQty The out standing quantity.
     */
	public void setOutstandingQty(String pOutstandingQty) {
		outstandingQty = pOutstandingQty;
	}
	
	/**
     * This method returns the short order typed
     * @return the short order typed.
     */
	public String getShortOrderType() {
		return shortOrderType;
	}
	
	/**
     * This method sets the short order typed
     * @param pShortOrderType The short order typed.
     */
	public void setShortOrderType(String pShortOrderType) {
		shortOrderType = pShortOrderType;
	}
	
	/**
     * This method returns the good till date
     * @return the good till date.
     */
	public String getGoodTillDate() {
		return goodTillDate;
	}
	
	/**
     * This method sets the good till date
     * @param pGoodTillDate The good till date.
     */
	public void setGoodTillDate(String pGoodTillDate) {
		goodTillDate = pGoodTillDate;
	}
	
	/**
     * This method returns the est net amount
     * @return the est net amount.
     */
	public String getEstNetAmount() {
		return estNetAmount;
	}
	
	/**
     * This method sets the est net amount
     * @param pEstNetAmount The est net amount.
     */
	public void setEstNetAmount(String pEstNetAmount) {
		estNetAmount = pEstNetAmount;
	}
	
	/**
     * This method returns the order group id
     * @return the order group id.
     */
	public String getOrderGroupId() {
		return orderGroupId;
	}
	
	/**
     * This method sets the order group id
     * @param pOrderGroupId The order group id.
     */
	public void setOrderGroupId(String pOrderGroupId) {
		orderGroupId = pOrderGroupId;
	}
	
	/**
     * This method returns the market id
     * @return the market id.
     */
	public String getMarketId() {
		return marketId;
	}
	
	/**
     * This method sets the market id form action
     * @param pMarketId The market id.
     */
	public void setMarketId(String pMarketId) {
		marketId = pMarketId;
	}
	
	/**
     * This method returns the stock code
     * @return the stock code.
     */
	public String getStockCode() {
		return stockCode;
	}
	
	/**
     * This method sets the stock code
     * @param pStockCode The stock code.
     */
	public void setStockCode(String pStockCode) {
		stockCode = pStockCode;
	}
	
	/**
     * This method returns the stock name
     * @return the stock name.
     */
	public String getStockName() {
		return stockName;
	}
	
	/**
     * This method sets the stock name
     * @param pStockName The stock name.
     */
	public void setStockName(String pStockName) {
		stockName = pStockName;
	}
	
	/**
     * This method returns the Buy or Sell
     * @return the Buy or Sell.
     *         [0] Buy
     *         [1] Sell
     */
	public String getBuySell() {
		return buySell;
	}
	
	/**
     * This method sets the Buy or Sell
     * @param pBuySell The Buy or Sell.
     */
	public void setBuySell(String pBuySell) {
		buySell = pBuySell;
	}
	
	/**
     * This method returns the price
     * @return the price.
     */
	public String getPrice() {
		return price;
	}
	
	/**
     * This method sets the price
     * @param pPrice The price.
     */
	public void setPrice(String pPrice) {
		price = pPrice;
	}
	
	/**
     * This method returns the quantity
     * @return the quantity.
     */
	public String getQty() {
		return qty;
	}
	
	/**
     * This method sets the quantity
     * @param pQty The quantity.
     */
	public void setQty(String pQty) {
		qty = pQty;
	}
	
	/**
     * This method returns the exec quantity
     * @return the exec quantity.
     */
	public String getExeQty() {
		return exeQty;
	}
	
	/**
     * This method sets the exec quantity
     * @param pExeQty The exec quantity.
     */
	public void setExeQty(String pExeQty) {
		exeQty = pExeQty;
	}
	
	/**
     * This method returns the average price
     * @return the average price.
     */
	public String getAvgPrice() {
		return avgPrice;
	}
	
	/**
     * This method sets the average price
     * @param pAvgPrice The average price.
     */
	public void setAvgPrice(String pAvgPrice) {
		avgPrice = pAvgPrice;
	}
	
	/**
     * This method returns the media
     * @return the media.
     */
	public String getMedia() {
		return media;
	}
	
	/**
     * This method sets the media
     * @param pMedia The media.
     */
	public void setMedia(String pMedia) {
		media = pMedia;
	}
	
	/**
     * This method returns the status
     * @return the status.
     */
	public String getStatus() {
		return status;
	}
	
	/**
     * This method sets the status
     * @param pStatus The status.
     */
	public void setStatus(String pStatus) {
		status = pStatus;
	}
	
	/**
     * This method returns the order type
     * @return the order type.
     */
	public String getOrderType() {
		return orderType;
	}
	
	/**
     * This method sets the order type
     * @param pOrderType The order type.
     */
	public void setOrderType(String pOrderType) {
		orderType = pOrderType;
	}
	
	/**
     * This method returns the stop price
     * @return the stop price.
     */
	public String getStopPrice() {
		return stopPrice;
	}
	
	/**
     * This method sets the stop price
     * @param pStopPrice The stop price.
     */
	public void setStopPrice(String pStopPrice) {
		stopPrice = pStopPrice;
	}
	
	/**
     * This method returns the reject reason
     * @return the reject reason.
     */
	public String getRejReason() {
		return rejReason;
	}
	
	/**
     * This method sets the reject reason
     * @param pRejReason The reject reason.
     */
	public void setRejReason(String pRejReason) {
		rejReason = pRejReason;
	}
	
	/**
     * This method returns the input time
     * @return the input time.
     */
	public String getInputTime() {
		return inputTime;
	}
	
	/**
     * This method sets the input time
     * @param pInputTime The input time.
     */
	public void setInputTime(String pInputTime) {
		inputTime = pInputTime;
	}
}
