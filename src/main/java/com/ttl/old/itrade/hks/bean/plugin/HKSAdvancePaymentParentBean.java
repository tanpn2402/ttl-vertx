package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSAdvancePaymentParentBean class define variables that to save values for action 
 * @author Wind.Zhao
 *
 */

//Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSAdvancePaymentParentBean {
	private String mvClientID;
	private String mvBankID;
	private String mvBankAccID;
	private String mvSettlement;
	private String mvCurrencySymbol;
	private String mvLendAmt;
	private String mvFeeRate;
	private String mvMaxFeeAmt;
	private String mvMinFeeAmt;
	/**
	 * Returns the client id.
	 * @return the client id.
	 */
	public String getMvClientID() {
		return mvClientID;
	}
	
	/**
	 * Sets the client id.
	 * @param pClientID the client id.
	 */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}
	
	/**
	 * Returns the bank id.
	 * @return the bank id.
	 */
	public String getMvBankID() {
		return mvBankID;
	}
	
	/**
	 * Sets the bank id.
	 * @param pBankID the bank id.
	 */
	public void setMvBankID(String pBankID) {
		mvBankID = pBankID;
	}
	
	/**
	 * Returns the bank account id.
	 * @return the bank account id.
	 */
	public String getMvBankAccID() {
		return mvBankAccID;
	}

	/**
	 * Sets the bank account id .
	 * @param pBankAccID the bank account id.
	 */
	public void setMvBankAccID(String pBankAccID) {
		mvBankAccID = pBankAccID;
	}
	/**
	 * Returns the settlement.
	 * @return the settlement.
	 */
	public String getMvSettlement() {
		return mvSettlement;
	}
	/**
	 * Sets the settlement.
	 * @param pSettlement the settlement.
	 */
	public void setMvSettlement(String pSettlement) {
		mvSettlement = pSettlement;
	}
	
	/**
	 * Returns the currency symbol.
	 * @return the currency symbol.
	 */
	public String getMvCurrencySymbol() {
		return mvCurrencySymbol;
	}

	/**
	 * Sets the currency symbol.
	 * @param pCurrencySymbol the currency symbol.
	 */
	public void setMvCurrencySymbol(String pCurrencySymbol) {
		mvCurrencySymbol = pCurrencySymbol;
	}

	public void setMvLendAmt(String mvLendAmt) {
		this.mvLendAmt = mvLendAmt;
	}

	public String getMvLendAmt() {
		return mvLendAmt;
	}

	public void setMvFeeRate(String mvFeeRate) {
		this.mvFeeRate = mvFeeRate;
	}

	public String getMvFeeRate() {
		return mvFeeRate;
	}

	public void setMvMaxFeeAmt(String mvMaxFeeAmt) {
		this.mvMaxFeeAmt = mvMaxFeeAmt;
	}

	public String getMvMaxFeeAmt() {
		return mvMaxFeeAmt;
	}

	public void setMvMinFeeAmt(String mvMinFeeAmt) {
		this.mvMinFeeAmt = mvMinFeeAmt;
	}

	public String getMvMinFeeAmt() {
		return mvMinFeeAmt;
	}
}
//End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module