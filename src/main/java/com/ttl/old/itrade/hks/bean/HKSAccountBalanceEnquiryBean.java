package com.ttl.old.itrade.hks.bean;

/**
 * The HKSAccountBalanceEnquiryBean class define variables that to save values for action
 * 
 * @author
 * 
 */
public class HKSAccountBalanceEnquiryBean {

	// BEGIN TASK: TTL-GZ-XYL-00062 XuYuLong 20090930 [iTrade R5] To revise account balance
	private String mvCurrencyId;
	private String mvSettledBalance;
	private String mvDueBalance;
	private String mvTodayBS;
	private String mvPendingBalance;
	private String mvMarketValue;
	private String mvWithdrawableAmount;
	private String mvBuyingPowerd;
	private String mvMarginValue;
	private String mvCreditLimit;
	private String mvMarginCall;
	private String mvSupplementCash;
	private String mvTotalHoldAmount;
	private String mvReceivableAmount;
	private String mvAvailableBalance;
	private String mvTodaySettlement;
	private String mvLedgerBalace;
	private String mvMarginableValue;
	private String mvInterest;
	private String mvDPWD;
	private String mvDThreshold;
	private String mvExtraCreditd;
	private String mvRemaining;
	private String mvMarginPercentage;
	private String mvClientId;
	private String mvTotalAccountValue;
	private String mvAvailableBalanceDisableStart;
	private String mvAvailableBalanceDisableEnd;
	private String mvForMarginClientStart;
	private String mvForMarginClientEnd;
	private String mvDate;
	// END TASK: TTL-GZ-XYL-00062 XuYuLong 20090930 [iTrade R5] To revise account balance

	// BEGIN TASK: TTL-GZ-XYL-00061 XuYuLong 20091229 [iTrade R5] Extra fields in Account Balance
	private String mvUsable;
	private String mvHoldingAmt;
	private String mvReceivableAmt;
	private String mvAccountType;

	private String mvOutstandingLoan;

	private String mvCSettled;
	private String mvManualReserve;
	private String mvAdvanceableAmount;
	private String mvPendingWithdraw;
	private String mvPendingSettled;
	private String mvBuyHoldAmount;
	private String mvTotalOutAdvance;

	private String mvPendingBuy;
	private String mvCashMaintenance;
	private String mvTemporaryHoldCash;
	private String mvDueSell;
	private String mvBankId;
	private String mvBankAcId;

	/**
	 * This method returns the holding amount.
	 * 
	 * @return the holding amount.
	 */
	public String getMvHoldingAmt() {
		return mvHoldingAmt;
	}

	/**
	 * This method sets the holding amount.
	 * 
	 * @param pHoldingAmt The holding amount.
	 */
	public void setMvHoldingAmt(String pHoldingAmt) {
		this.mvHoldingAmt = pHoldingAmt;
	}

	/**
	 * This method returns the usable.
	 * 
	 * @return the usable.
	 */
	public String getMvUsable() {
		return mvUsable;
	}

	/**
	 * This method sets the usable.
	 * 
	 * @param pUsable The usable.
	 */
	public void setMvUsable(String pUsable) {
		this.mvUsable = pUsable;
	}

	/**
	 * This method returns the receive able amount.
	 * 
	 * @return the receive able amount.
	 */
	public String getMvReceivableAmt() {
		return mvReceivableAmt;
	}

	/**
	 * This method sets the receive able amount.
	 * 
	 * @param pReceivableAmt The amount.
	 */
	public void setMvReceivableAmt(String pReceivableAmt) {
		this.mvReceivableAmt = pReceivableAmt;
	}

	// END TASK: TTL-GZ-XYL-00061 XuYuLong 20091229 [iTrade R5] Extra fields in Account Balance

	/**
	 * This method returns the currency id.
	 * 
	 * @return the currency id.
	 */
	public String getMvCurrencyId() {
		return mvCurrencyId;
	}

	/**
	 * This method sets the currency id.
	 * 
	 * @param pCurrencyId The currency id form action.
	 */
	public void setMvCurrencyId(String pCurrencyId) {
		this.mvCurrencyId = pCurrencyId;
	}

	/**
	 * This method returns the settled balance.
	 * 
	 * @return the settled balance.
	 */
	public String getMvSettledBalance() {
		return mvSettledBalance;
	}

	/**
	 * This method sets the settled balance.
	 * 
	 * @param pSettledBalance The settled balance.
	 */
	public void setMvSettledBalance(String pSettledBalance) {
		this.mvSettledBalance = pSettledBalance;
	}

	/**
	 * This method returns the due balance.
	 * 
	 * @return the due balance.
	 */
	public String getMvDueBalance() {
		return mvDueBalance;
	}

	/**
	 * This method sets the due balance.
	 * 
	 * @param pDueBalance The due balance.
	 */
	public void setMvDueBalance(String pDueBalance) {
		this.mvDueBalance = pDueBalance;
	}

	/**
	 * This method returns the today's confirm buy and sell amount.
	 * 
	 * @return the today's confirm buy and sell amount.
	 */
	public String getMvTodayBS() {
		return mvTodayBS;
	}

	/**
	 * This method sets the today's confirm buy and sell amount.
	 * 
	 * @param pTodayBS The today's confirm buy and sell amount.
	 */
	public void setMvTodayBS(String pTodayBS) {
		this.mvTodayBS = pTodayBS;
	}

	/**
	 * This method returns the pending balance.
	 * 
	 * @return the pending balance.
	 */
	public String getMvPendingBalance() {
		return mvPendingBalance;
	}

	/**
	 * This method sets the pending balance.
	 * 
	 * @param pPendingBalance The pending balance.
	 */
	public void setMvPendingBalance(String pPendingBalance) {
		this.mvPendingBalance = pPendingBalance;
	}

	/**
	 * This method returns the market value.
	 * 
	 * @return the market value.
	 */
	public String getMvMarketValue() {
		return mvMarketValue;
	}

	/**
	 * This method sets the market value.
	 * 
	 * @param pMarketValue The market value.
	 */
	public void setMvMarketValue(String pMarketValue) {
		this.mvMarketValue = pMarketValue;
	}

	/**
	 * This method returns the withdrawal amount.
	 * 
	 * @return the withdrawal amount.
	 */
	public String getMvWithdrawableAmount() {
		return mvWithdrawableAmount;
	}

	/**
	 * This method sets the withdrawal amount.
	 * 
	 * @param pWithdrawableAmount The withdrawal amount.
	 */
	public void setMvWithdrawableAmount(String pWithdrawableAmount) {
		this.mvWithdrawableAmount = pWithdrawableAmount;
	}

	/**
	 * This method returns the buying power.
	 * 
	 * @return the buying power.
	 */
	public String getMvBuyingPowerd() {
		return mvBuyingPowerd;
	}

	/**
	 * This method sets the buying power.
	 * 
	 * @param pBuyingPowerd The buying power.
	 */
	public void setMvBuyingPowerd(String pBuyingPowerd) {
		this.mvBuyingPowerd = pBuyingPowerd;
	}

	/**
	 * This method returns the margin value.
	 * 
	 * @return the margin value.
	 */
	public String getMvMarginValue() {
		return mvMarginValue;
	}

	/**
	 * This method sets the margin value.
	 * 
	 * @param pMarginValue The margin value.
	 */
	public void setMvMarginValue(String pMarginValue) {
		this.mvMarginValue = pMarginValue;
	}

	/**
	 * This method returns the credit limit.
	 * 
	 * @return the credit limit.
	 */
	public String getMvCreditLimit() {
		return mvCreditLimit;
	}

	/**
	 * This method sets the credit limit.
	 * 
	 * @param pCreditLimit The credit limit.
	 */
	public void setMvCreditLimit(String pCreditLimit) {
		this.mvCreditLimit = pCreditLimit;
	}

	/**
	 * This method returns the margin call.
	 * 
	 * @return the margin call.
	 */
	public String getMvMarginCall() {
		return mvMarginCall;
	}

	/**
	 * This method sets the margin call.
	 * 
	 * @param pMarginCall The margin call.
	 */
	public void setMvMarginCall(String pMarginCall) {
		this.mvMarginCall = pMarginCall;
	}

	/**
	 * This method returns the today hold amount.
	 * 
	 * @return the today hold amount.
	 */
	public String getMvTotalHoldAmount() {
		return mvTotalHoldAmount;
	}

	/**
	 * This method sets the today hold amount.
	 * 
	 * @param pTotalHoldAmount The today hold amount.
	 */
	public void setMvTotalHoldAmount(String pTotalHoldAmount) {
		this.mvTotalHoldAmount = pTotalHoldAmount;
	}

	/**
	 * This method returns the receivable amount.
	 * 
	 * @return the receivable amount.
	 */
	public String getMvReceivableAmount() {
		return mvReceivableAmount;
	}

	/**
	 * This method sets the receivable amount.
	 * 
	 * @param pReceivableAmount The receivable amount.
	 */
	public void setMvReceivableAmount(String pReceivableAmount) {
		this.mvReceivableAmount = pReceivableAmount;
	}

	/**
	 * This method returns the available balance.
	 * 
	 * @return the available balance.
	 */
	public String getMvAvailableBalance() {
		return mvAvailableBalance;
	}

	/**
	 * This method sets the available balance.
	 * 
	 * @param pAvailableBalance The available balance.
	 */
	public void setMvAvailableBalance(String pAvailableBalance) {
		this.mvAvailableBalance = pAvailableBalance;
	}

	/**
	 * This method returns the today settlement.
	 * 
	 * @return the today settlement.
	 */
	public String getMvTodaySettlement() {
		return mvTodaySettlement;
	}

	/**
	 * This method sets the today settlement.
	 * 
	 * @param pTodaySettlement The today settlement.
	 */
	public void setMvTodaySettlement(String pTodaySettlement) {
		this.mvTodaySettlement = pTodaySettlement;
	}

	/**
	 * This method returns the ledger balance.
	 * 
	 * @return the ledger balance.
	 */
	public String getMvLedgerBalace() {
		return mvLedgerBalace;
	}

	/**
	 * This method sets the ledger balance.
	 * 
	 * @param pLedgerBalace The ledger balance.
	 */
	public void setMvLedgerBalace(String pLedgerBalace) {
		this.mvLedgerBalace = pLedgerBalace;
	}

	/**
	 * This method returns the marginable value.
	 * 
	 * @return the marginable value.
	 */
	public String getMvMarginableValue() {
		return mvMarginableValue;
	}

	/**
	 * This method sets the marginable value.
	 * 
	 * @param pMarginableValue The marginable value.
	 */
	public void setMvMarginableValue(String pMarginableValue) {
		this.mvMarginableValue = pMarginableValue;
	}

	/**
	 * This method returns the interest.
	 * 
	 * @return the interest.
	 */
	public String getMvInterest() {
		return mvInterest;
	}

	/**
	 * This method sets the interest.
	 * 
	 * @param pInterest The interest.
	 */
	public void setMvInterest(String pInterest) {
		this.mvInterest = pInterest;
	}

	/**
	 * This method returns the dpwd.
	 * 
	 * @return the dpwd.
	 */
	public String getMvDPWD() {
		return mvDPWD;
	}

	/**
	 * This method sets the dpwd.
	 * 
	 * @param pDPWD The dpwd.
	 */
	public void setMvDPWD(String pDPWD) {
		this.mvDPWD = pDPWD;
	}

	/**
	 * This method returns the threshold.
	 * 
	 * @return the threshold.
	 */
	public String getMvDThreshold() {
		return mvDThreshold;
	}

	/**
	 * This method sets the threshold.
	 * 
	 * @param pDThreshold The threshold.
	 */
	public void setMvDThreshold(String pDThreshold) {
		this.mvDThreshold = pDThreshold;
	}

	/**
	 * This method returns the extra credit.
	 * 
	 * @return the extra credit.
	 */
	public String getMvExtraCreditd() {
		return mvExtraCreditd;
	}

	/**
	 * This method sets the extra credit.
	 * 
	 * @param pExtraCreditd The extra credit.
	 */
	public void setMvExtraCreditd(String pExtraCreditd) {
		this.mvExtraCreditd = pExtraCreditd;
	}

	/**
	 * This method returns the remaining.
	 * 
	 * @return the remaining.
	 */
	public String getMvRemaining() {
		return mvRemaining;
	}

	/**
	 * This method sets the remaining.
	 * 
	 * @param pRemaining The remaining.
	 */
	public void setMvRemaining(String pRemaining) {
		this.mvRemaining = pRemaining;
	}

	/**
	 * This method returns the margin percentage.
	 * 
	 * @return the margin percentage.
	 */
	public String getMvMarginPercentage() {
		return mvMarginPercentage;
	}

	/**
	 * This method sets the margin percentage.
	 * 
	 * @param pMarginPercentage The margin percentage.
	 */
	public void setMvMarginPercentage(String pMarginPercentage) {
		this.mvMarginPercentage = pMarginPercentage;
	}

	/**
	 * This method returns the client id.
	 * 
	 * @return the client id.
	 */
	public String getMvClientId() {
		return mvClientId;
	}

	/**
	 * This method sets the client id.
	 * 
	 * @param pClientId The client id.
	 */
	public void setMvClientId(String pClientId) {
		this.mvClientId = pClientId;
	}

	/**
	 * This method returns the total account value.
	 * 
	 * @return the total account value.
	 */
	public String getMvTotalAccountValue() {
		return mvTotalAccountValue;
	}

	/**
	 * This method sets the total account value.
	 * 
	 * @param pTotalAccountValue The total account value.
	 */
	public void setMvTotalAccountValue(String pTotalAccountValue) {
		this.mvTotalAccountValue = pTotalAccountValue;
	}

	/**
	 * This method returns the available balance disable start value to control show or hidden available balance.
	 * 
	 * @return the available balance disable start value.
	 */
	public String getMvAvailableBalanceDisableStart() {
		return mvAvailableBalanceDisableStart;
	}

	/**
	 * This method sets the available balance disable start value to control show or hidden available balance.
	 * 
	 * @param pAvailableBalanceDisableStart The available balance disable start value.
	 */
	public void setMvAvailableBalanceDisableStart(String pAvailableBalanceDisableStart) {
		this.mvAvailableBalanceDisableStart = pAvailableBalanceDisableStart;
	}

	/**
	 * This method returns the available balance disable end value to control show or hidden available balance.
	 * 
	 * @return the available balance disable end value.
	 */
	public String getMvAvailableBalanceDisableEnd() {
		return mvAvailableBalanceDisableEnd;
	}

	/**
	 * This method sets the available balance disable end value to control show or hidden available balance.
	 * 
	 * @param pAvailableBalanceDisableEnd The available balance disable end value.
	 */
	public void setMvAvailableBalanceDisableEnd(String pAvailableBalanceDisableEnd) {
		this.mvAvailableBalanceDisableEnd = pAvailableBalanceDisableEnd;
	}

	/**
	 * This method returns the margin client start value to control show or hidden margin client.
	 * 
	 * @return the margin client start value.
	 */
	public String getMvForMarginClientStart() {
		return mvForMarginClientStart;
	}

	/**
	 * This method sets the margin client start value to control show or hidden margin client.
	 * 
	 * @param pForMarginClientStart The margin client start value.
	 */
	public void setMvForMarginClientStart(String pForMarginClientStart) {
		this.mvForMarginClientStart = pForMarginClientStart;
	}

	/**
	 * This method returns the margin client end value to control show or hidden margin client.
	 * 
	 * @return the available balance disable end value.
	 */
	public String getMvForMarginClientEnd() {
		return mvForMarginClientEnd;
	}

	/**
	 * This method sets the margin client start value to control show or hidden margin client.
	 * 
	 * @param pForMarginClientEnd The margin client start value.
	 */
	public void setMvForMarginClientEnd(String pForMarginClientEnd) {
		this.mvForMarginClientEnd = pForMarginClientEnd;
	}

	/**
	 * This method returns the date
	 * 
	 * @return the date.
	 */
	public String getMvDate() {
		return mvDate;
	}

	/**
	 * This method sets the date
	 * 
	 * @param pDate The date action.
	 */
	public void setMvDate(String pDate) {
		this.mvDate = pDate;
	}

	public void setMvAccountType(String mvAccountType) {
		this.mvAccountType = mvAccountType;
	}

	public String getMvAccountType() {
		return mvAccountType;
	}

	public void setMvOutstandingLoan(String mvOutstandingLoan) {
		this.mvOutstandingLoan = mvOutstandingLoan;
	}

	public String getMvOutstandingLoan() {
		return mvOutstandingLoan;
	}

	public void setMvCSettled(String mvCSettled) {
		this.mvCSettled = mvCSettled;
	}

	public String getMvCSettled() {
		return mvCSettled;
	}

	public void setMvManualReserve(String mvManualReserve) {
		this.mvManualReserve = mvManualReserve;
	}

	public String getMvManualReserve() {
		return mvManualReserve;
	}

	public void setMvAdvanceableAmount(String mvAdvanceableAmount) {
		this.mvAdvanceableAmount = mvAdvanceableAmount;
	}

	public String getMvAdvanceableAmount() {
		return mvAdvanceableAmount;
	}

	public void setMvPendingWithdraw(String mvPendingWithdraw) {
		this.mvPendingWithdraw = mvPendingWithdraw;
	}

	public String getMvPendingWithdraw() {
		return mvPendingWithdraw;
	}

	public void setMvPendingSettled(String mvPendingSettled) {
		this.mvPendingSettled = mvPendingSettled;
	}

	public String getMvPendingSettled() {
		return mvPendingSettled;
	}

	public void setMvBuyHoldAmount(String mvBuyHoldAmount) {
		this.mvBuyHoldAmount = mvBuyHoldAmount;
	}

	public String getMvBuyHoldAmount() {
		return mvBuyHoldAmount;
	}

	/**
	 * @param mvTotalOutAdvance the mvTotalOutAdvance to set
	 */
	public void setMvTotalOutAdvance(String mvTotalOutAdvance) {
		this.mvTotalOutAdvance = mvTotalOutAdvance;
	}

	/**
	 * @return the mvTotalOutAdvance
	 */
	public String getMvTotalOutAdvance() {
		return mvTotalOutAdvance;
	}

	public String getMvPendingBuy() {
		return mvPendingBuy;
	}

	public void setMvPendingBuy(String mvPendingBuy) {
		this.mvPendingBuy = mvPendingBuy;
	}

	public String getMvSupplementCash() {
		return mvSupplementCash;
	}

	public void setMvSupplementCash(String mvSupplementCash) {
		this.mvSupplementCash = mvSupplementCash;
	}

	public String getMvCashMaintenance() {
		return mvCashMaintenance;
	}

	public void setMvCashMaintenance(String mvCashMaintenance) {
		this.mvCashMaintenance = mvCashMaintenance;
	}

	public String getMvTemporaryHoldCash() {
		return mvTemporaryHoldCash;
	}

	public void setMvTemporaryHoldCash(String mvTemporaryHoldCash) {
		this.mvTemporaryHoldCash = mvTemporaryHoldCash;
	}

	public String getMvDueSell() {
		return mvDueSell;
	}

	public void setMvDueSell(String mvDueSell) {
		this.mvDueSell = mvDueSell;
	}

	public String getMvBankId() {
		return mvBankId;
	}

	public void setMvBankId(String mvBankId) {
		this.mvBankId = mvBankId;
	}

	public String getMvBankAcId() {
		return mvBankAcId;
	}

	public void setMvBankAcId(String mvBankAcId) {
		this.mvBankAcId = mvBankAcId;
	}

}
