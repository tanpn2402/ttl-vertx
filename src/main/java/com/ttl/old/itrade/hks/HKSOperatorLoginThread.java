package com.ttl.old.itrade.hks;

import java.util.List;

import com.ttl.old.itrade.hks.bean.HKSMarketIndexBean;
import com.ttl.old.itrade.hks.request.RequestName;
//import com.itrade.hks.txn.HKSDownloadInstrumentTxn;
//import com.itrade.hks.txn.HKSDownloadTradeDateTxn;
import com.ttl.old.itrade.hks.txn.HKSGetMarketStatusInfoTxn;
//BEGIN Task #: WL00619 Walter Lau 27 July 2007
//import com.itrade.hks.txn.HKSDownloadMarketTxn;
//END Task #: WL00619 Walter Lau 27 July 2007
//import com.itrade.hks.txn.HKSCheckTPStatusTxn;
import com.ttl.old.itrade.hks.txn.HKSRegisterDNTxn;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.tp.txn.OperatorLoginTxn;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TopicName;
import com.systekit.winvest.hks.util.Utils;

/**
 * The HKSOperatorLoginThread class defines methods that operator Login Thread started going to query and check.
 * @author
 *
 */
class HKSOperatorLoginThread extends Thread {

    private TPManager ivTPManager = null;
    private static int svNumberOfAvailableGoodTillDate = 7;
    //BEGIN Task #: WL00619 Walter Lau 27 July 2007
    private static String svProductID = "HKS";
    private static String svUsingStockCodeFormatMarket = "HKEX";

    //BEGIN - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
    private static int svReDownloadDataTime = 60000;
    private static int svReDownloadDataDownloadTimes = 3;
    private int lvReDownloadDataDownloadTimes = 1;
    private static boolean svReDownloadDataEnable = false;

    private boolean mvDownloadTradeDateFinished = false;
    private boolean mvDownloadInstrumentFinished = false;
    private boolean mvDownloadMarketIDFinished = false;
    private boolean mvRegisterDynamicNotificationFinished = false;
    private boolean mvCheckTPStatusFinished = false;
    private boolean mvTimerStarted = false;
    //BEGIN - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
    private static String svTPServerSide = "F";

    /**
     * Constructor for HKSExternalOperatorLoginThread class.
     * @param pTPManager The TPManager object.
     * @param pNumberOfAvailableGoodTillDate The number of available good till date.
     * @param pProductID The Product ID.
     * @param pUsingStockCodeFormatMarket The using stock code with format market.
     * @param pReDownloadDataTime The time of re download data.
     * @param pTPServerSide The server side of TP.
     */
    HKSOperatorLoginThread(TPManager pTPManager, int pNumberOfAvailableGoodTillDate, String pProductID, String pUsingStockCodeFormatMarket, int pReDownloadDataTime, String pTPServerSide, int pReDownloadDataDownloadTimes, boolean pReDownloadDatabEnable) {
        ivTPManager = pTPManager;
        svNumberOfAvailableGoodTillDate = pNumberOfAvailableGoodTillDate;
        svProductID = pProductID;
        svUsingStockCodeFormatMarket = pUsingStockCodeFormatMarket;
        svReDownloadDataTime = pReDownloadDataTime;
        svTPServerSide = pTPServerSide;
        svReDownloadDataDownloadTimes = pReDownloadDataDownloadTimes;
        svReDownloadDataEnable = pReDownloadDatabEnable;
    }
    //End Task #: WL00619 Walter Lau 27 July 2007
    //END - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue 
        
    /**
     * This method to re download default data.
     */
    public void reDownloadDefaultData() {
        try {
            if(lvReDownloadDataDownloadTimes<svReDownloadDataDownloadTimes) {
                mvTimerStarted = true;
                Log.println("[ TPConnector.operatorLogin() re-get Data form TP sleep start: ]", Log.ACCESS_LOG);
                HKSOperatorLoginThread.sleep(svReDownloadDataTime);
                lvReDownloadDataDownloadTimes++;
                Log.println("[ TPConnector.operatorLogin() re-get Data form TP sleep stop and download again: Times + " + lvReDownloadDataDownloadTimes + " ]", Log.ACCESS_LOG);
                getDefaultDataFromTP();
            }
            else {
                Log.println("[ TPConnector.operatorLogin() re-get Data the times more than Default setting : " + svReDownloadDataDownloadTimes + " Re-get Download Stopped ]", Log.ERROR_LOG);
            }
        } catch (Exception ex) {
            Log.println(ex, Log.ERROR_LOG);
        }
    }
    //END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data

    /**
     * This method judge returnCode of operator Login whether correct.
     */
    public void run() {
		OperatorLoginTxn lvOperatorLoginTxn = new OperatorLoginTxn(ivTPManager, RequestName.HKSOperatorLoginRequest);
		lvOperatorLoginTxn.process();

		int returnCode = lvOperatorLoginTxn.getReturnCode();
		if (returnCode == TPErrorHandling.TP_NORMAL) {
			//BEGIN - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
			//BEGIN - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
			if(!Utils.isNullStr(svTPServerSide) && svTPServerSide.equals("F"))
			    getDefaultDataFromTP();
			//END - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
            //END - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
		} else {
			// BEGIN - Task #: TTL-HK-WLCW-00973. - Walter Lau 20100209  Can't Display Re-Try Error Message
			Log.println("[ TPConnector.operatorLogin() failed with error:" + lvOperatorLoginTxn.getErrorMessage() + " ]", Log.ALERT_LOG);

			if (returnCode == TPErrorHandling.TP_APP_ERR) {
				Log.println("[ TPConnector.operatorLogin() failed with application error:" + lvOperatorLoginTxn.getErrorMessage() + " ]", Log.ALERT_LOG);
			}
			else if (returnCode == TPErrorHandling.TP_SYS_ERR) {
				Log.println("[ TPConnector.operatorLogin() failed with system error:" + lvOperatorLoginTxn.getErrorMessage() + " ]", Log.ALERT_LOG);
			}
			// END - Task #: TTL-HK-WLCW-00973. - Walter Lau 20100209  Can't Display Re-Try Error Message
		}
		
	}

    //BEGIN - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
    /**
     * This method to get default data from TP.
     */
    private void getDefaultDataFromTP() {
        Log.println("[ TPBroadcastManager.download Data From TP : Operator Login Thread ]", Log.ACCESS_LOG);

        if (!mvDownloadTradeDateFinished) {
            //queryNextTradeDateProcess();
        }

        if (!mvDownloadInstrumentFinished) {
            initInstrumentProcess();
        }

        if (!mvRegisterDynamicNotificationFinished) {
            registerDynamicNotification();
        }

        if (!mvDownloadMarketIDFinished) {
           // queryMarketIDProcess();
        }

        if (!mvCheckTPStatusFinished) {
           // checkTPStatus();
        }
        
        // BEGIN TASK: VanTran TTL-VN 2010-08-30 Add download market information : market index & market status
        if (mvCheckTPStatusFinished) {
        	queryMarketInformation();
        }
        // END TASK: VanTran TTL-VN 2010-08-30 Add download market information : market index & market status
        
        // BEGIN TASK: TTL-HK-AHJ-00004 12-Feb-2010 [iTrade R4 BankComm] Instrument Download Multipart
        // wait for the first message to arrive
        try {
			HKSOperatorLoginThread.sleep(svReDownloadDataTime);
		}
		catch (Exception ex) {
			Log.println(ex, Log.ERROR_LOG);
        }
        
        // wake up every so often to check for new download message
//        while(true) {
////        	// if all parts are received, finish off
////        	if (TPManager.svDownloadInstrumentTxn.downloadSucceded()) {
////            	lvReDownloadDataDownloadTimes = 1;
////            	mvDownloadInstrumentFinished = true;
////                Log.println("[ TPManager.initInstrumentProcess(): Download Instrument Action Finished", Log.ACCESS_LOG);
////                
////                //BEGIN - Task #: TTL-VN - VanTran Move load historical data only after download instrument list
////        		IMain.loadStockHistData();
////        		//END - Task #: TTL-VN - VanTran Move load historical data only after download instrument list 
////                return;
////        	}
//        	// if no new arrival within the period, trigger re-download
//        	else if (!TPManager.svDownloadInstrumentTxn.getHasNewArrival()) {
//            	mvDownloadInstrumentFinished = false;
//            	if(svReDownloadDataEnable)
//				{
//					reDownloadDefaultData();
//				}
//            	return;
//        	}
//        	// sleep for one more period of time
//        	else {
//        		TPManager.svDownloadInstrumentTxn.setHasNewArrival(false);
//        		try {
//        			HKSOperatorLoginThread.sleep(svReDownloadDataTime);
//        		}
//        		catch (Exception ex) {
//        			Log.println(ex, Log.ERROR_LOG);
//		        }
//        	}
//        }
        // END TASK: TTL-HK-AHJ-00004 12-Feb-2010 [iTrade R4 BankComm] Instrument Download Multipart
    }
    //END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
    
    /**
     * This method inti instrument that process instrument download.
     */
    private void initInstrumentProcess() {
		Log.println("[ TPManager.initInstrumentProcess(): Download Instrument Action Begin", Log.ACCESS_LOG);

        //BEGIN Task #: WL00619 Walter Lau 27 July 2007
		//TPManager.svDownloadInstrumentTxn = new HKSDownloadInstrumentTxn(svUsingStockCodeFormatMarket);
        //END Task #: WL00619 Walter Lau
//		mvCheckTPStatusFinished = false;
//		Log.println("[ HKSOperatorLoginThread.checkStatus(): Started]", Log.ACCESS_LOG);
//		HKSCheckTPStatusTxn lvHKSCheckTPStatusTxn = new HKSCheckTPStatusTxn();
//		
//		try {
//			lvHKSCheckTPStatusTxn.process();
//		} catch(Exception ex) {
//			Log.println(ex, Log.ERROR_LOG);
//			mvTimerStarted = false;
//		}
//		int lvReturnCode = lvHKSCheckTPStatusTxn.getReturnCode();
//		if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
//			if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
//				Log.println("[ HKSOperatorLoginThread.checkTPStatusProcess(): Check TP Status Action failed with application error:" +
//						lvHKSCheckTPStatusTxn.getErrorMessage() + " ]",
//						Log.ALERT_LOG);
//			} else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
//				Log.println("[ HKSOperatorLoginThread.checkTPStatusProcess(): Check TP Status Action failed with system error:" +
//						lvHKSCheckTPStatusTxn.getErrorMessage() + " ]",
//						Log.ALERT_LOG);
//			}
//		
//			mvTimerStarted = false;
//		} else {
//			mvCheckTPStatusFinished = true;
//		}
//		
//		if (! mvCheckTPStatusFinished && !mvTimerStarted) {
//			if(svReDownloadDataEnable)
//				reDownloadDefaultData();
//		}
//		Log.println("[ HKSOperatorLoginThread.checkStatus(): Completed]", Log.ACCESS_LOG);
//     27 July 2007

        try {
        	//TPManager.svDownloadInstrumentTxn.process();
        } catch(Exception ex) {
            mvTimerStarted = false;
            Log.println(ex, Log.ERROR_LOG);
        }
    }

    /**
     * This method register dynamic notification.
     */
    private void registerDynamicNotification() {
		//BEGIN - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
		mvRegisterDynamicNotificationFinished = false;
		//END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data

		HKSRegisterDNTxn lvHKSRegisterDNTxn = new HKSRegisterDNTxn();
		lvHKSRegisterDNTxn.addTopicName(TopicName.TOPIC_HKSFO_PRICE_ALERT);
		lvHKSRegisterDNTxn.addTopicName(TopicName.TOPIC_HKSFO_ORDER_ENQUIRY);
		lvHKSRegisterDNTxn.addTopicName(TopicName.TOPIC_HKSFO_ITRADE_CLIENT_LOGIN);

		lvHKSRegisterDNTxn.addTopicName("TOPIC_HKSFO_EMESSAGE");
		lvHKSRegisterDNTxn.addTopicName(TopicName.TOPIC_HKSFO_SECURITIES_ENQUIRY_OBCD);
		// Added by Bowen Chau on 29 Mar 2006
		// Register dynamic notification for MCSYS and CORDL001D49 to update good till date
		lvHKSRegisterDNTxn.addTopicName("MCSYS");
		lvHKSRegisterDNTxn.addTopicName("CORDL001D49");

		// Added by Bowen Chau on 29 Mar 2006
		// Register dynamic notification for MCINSTRUMENT and CORDL001D19 to update instrument information
		lvHKSRegisterDNTxn.addTopicName("MCINSTRUMENT");
		lvHKSRegisterDNTxn.addTopicName("CORDL001D19");

		// Added by Bowen Chau on 29 Mar 2006
		// Register dynamic notification for ADVANCE_TRADEDATE to update good till date in HKS
		lvHKSRegisterDNTxn.addTopicName("TOPIC_HKSBO_MARKET_TRADEDATE_ADVANCED");

		// Added by Bowen Chau on 31 Mar 2006
		// Register dynamic notification for TOPIC_HKSFO_MARKET_STATUS for update good till date when market close
		lvHKSRegisterDNTxn.addTopicName("TOPIC_HKSFO_MARKET_STATUS");

		// Added by Bowen Chau on 13 June 2006
		// Register dynamic notification for TOPIC_ORDER_EXPIRATION for update good till date when order expiration
		lvHKSRegisterDNTxn.addTopicName("TOPIC_ORDER_EXPIRATION");

		// Added by Bowen Chau on 13 June 2006
		// Register dynamic notification for TOPIC_DAY_END for update good till date when order expiration
		lvHKSRegisterDNTxn.addTopicName("TOPIC_DAY_END");
		
		boolean isUsingTTL = "true".equalsIgnoreCase(IMain.getProperty("isUsingTTLService"));
		if(isUsingTTL){
			// VanTran 2010-22-06 register TOPIC_HKSFO_MARKET_INDEX
			lvHKSRegisterDNTxn.addTopicName("TOPIC_HKSFO_MARKET_INDEX");
		}

		lvHKSRegisterDNTxn.process();

		int lvReturnCode = lvHKSRegisterDNTxn.getReturnCode();
		if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
			// BEGIN - Task #: TTL-HK-WLCW-00973. - Walter Lau 20100209  Can't Display Re-Try Error Message
			if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
				Log.println("[ TPManager.registerDynamicNotification(): Register Client Action failed with application error:"
						+ lvHKSRegisterDNTxn.getErrorMessage() + " ]",
						Log.ALERT_LOG);
			} else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
				Log.println("[ TPManager.registerDynamicNotification(): Register Client Action failed with system error:"
						+ lvHKSRegisterDNTxn.getErrorMessage() + " ]",
						Log.ALERT_LOG);
			}
			// END - Task #: TTL-HK-WLCW-00973. - Walter Lau 20100209  Can't Display Re-Try Error Message
		    mvTimerStarted = false;
		} else {
			mvRegisterDynamicNotificationFinished = true;
			Log.println("[ HKSOperatorLoginThread.registerDynamicNotification(): Register Client Finished ", Log.ACCESS_LOG);
		}
		if (!mvRegisterDynamicNotificationFinished && !mvTimerStarted) {
			if(svReDownloadDataEnable)
				reDownloadDefaultData();
		}
		//END - Task #: TTL-HK-WLCW-00963. - Walter Lau 20100122 Re-Try Download Data
	}

    /**
     * This method query next Trade process date.
     */
//	private void queryNextTradeDateProcess() {
//		mvDownloadTradeDateFinished = false;
//		//HKSDownloadTradeDateTxn lvHKSDownloadTradeDateTxn = new HKSDownloadTradeDateTxn(svNumberOfAvailableGoodTillDate);
//		
//		try {
//		    lvHKSDownloadTradeDateTxn.process();
//		} catch (Exception ex) {
//			mvTimerStarted = false;
//			Log.println(ex, Log.ERROR_LOG);
//		}
//		
//		int lvReturnCode = lvHKSDownloadTradeDateTxn.getReturnCode();
//		if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
//			if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
//				Log.println("[ HKSOperatorLoginThread.queryNextTradeDateProcess(): Download TradeDate Action failed with application error:" +
//					lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]",
//					Log.ALERT_LOG);
//			} else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
//				Log.println("[ HKSOperatorLoginThread.queryNextTradeDateProcess(): Download TradeDate Action failed with system error:" +
//					lvHKSDownloadTradeDateTxn.getErrorMessage() + " ]",
//					Log.ALERT_LOG);
//			}
//			mvTimerStarted = false;
//		} else {
//			mvDownloadTradeDateFinished = true;
//			Log.println("[ HKSOperatorLoginThread.queryNextTradeDateProcess(): Download Trade Date Action Finished", Log.ACCESS_LOG);
//		}
//
//		if (!mvDownloadTradeDateFinished && !mvTimerStarted) {
//			if (svReDownloadDataEnable) {
//				reDownloadDefaultData();
//			}
//		}
//		//END - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
//	}

    //BEGIN Task #: WL00619 Walter Lau 27 July 2007
    //BEGIN - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
    /**
     * This method to query market ID.
     */
//	private void queryMarketIDProcess() {
//		mvDownloadMarketIDFinished = false;
//		HKSDownloadMarketTxn lvHKSDownloadMarketTxn = new HKSDownloadMarketTxn(svProductID);
//
//		try {
//			lvHKSDownloadMarketTxn.process();
//		} catch (Exception ex) {
//			mvTimerStarted = false;
//			Log.println(ex, Log.ERROR_LOG);
//		}
//
//		int lvReturnCode = lvHKSDownloadMarketTxn.getReturnCode();
//		if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
//			if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
//				Log.println("[ HKSOperatorLoginThread.queryMarketIDProcess(): Download Market ID Action failed with application error:" +
//						lvHKSDownloadMarketTxn.getErrorMessage() + " ]",
//						Log.ALERT_LOG);
//			} else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
//				Log.println("[ HKSOperatorLoginThread.queryMarketIDProcess(): Download Market ID Action failed with system error:" +
//						lvHKSDownloadMarketTxn.getErrorMessage() + " ]",
//						Log.ALERT_LOG);
//			}
//			mvTimerStarted = false;
//		}
//		else {
//			mvDownloadMarketIDFinished = true;
//			Log.println("[ HKSOperatorLoginThread.queryMarketIDProcess(): Download Market ID Action Finished", Log.ACCESS_LOG);
//		}
//
//		if (!mvDownloadMarketIDFinished && !mvTimerStarted) {
//			if (svReDownloadDataEnable) {
//				reDownloadDefaultData();
//			}
//		}
//    }
    //END - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
    //BEGIN Task #: WL00619 Walter Lau 27 July 2007

    //BEGIN - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
    /**
     * This method to check the status of TP.
     */
//	private void checkTPStatus() {
//		mvCheckTPStatusFinished = false;
//		Log.println("[ HKSOperatorLoginThread.checkStatus(): Started]", Log.ACCESS_LOG);
//		HKSCheckTPStatusTxn lvHKSCheckTPStatusTxn = new HKSCheckTPStatusTxn();
//		
//		try {
//			lvHKSCheckTPStatusTxn.process();
//		} catch(Exception ex) {
//			Log.println(ex, Log.ERROR_LOG);
//			mvTimerStarted = false;
//		}
//		int lvReturnCode = lvHKSCheckTPStatusTxn.getReturnCode();
//		if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
//			if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
//				Log.println("[ HKSOperatorLoginThread.checkTPStatusProcess(): Check TP Status Action failed with application error:" +
//						lvHKSCheckTPStatusTxn.getErrorMessage() + " ]",
//						Log.ALERT_LOG);
//			} else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
//				Log.println("[ HKSOperatorLoginThread.checkTPStatusProcess(): Check TP Status Action failed with system error:" +
//						lvHKSCheckTPStatusTxn.getErrorMessage() + " ]",
//						Log.ALERT_LOG);
//			}
//		
//			mvTimerStarted = false;
//		} else {
//			mvCheckTPStatusFinished = true;
//		}
//		
//		if (! mvCheckTPStatusFinished && !mvTimerStarted) {
//			if(svReDownloadDataEnable)
//				reDownloadDefaultData();
//		}
//		Log.println("[ HKSOperatorLoginThread.checkStatus(): Completed]", Log.ACCESS_LOG);
//    }
    //END - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument

//	private boolean RegisterPriceDepth(String pSeriesID)
//	{
//		TPErrorHandling lvErrorHandling = new TPErrorHandling();
//
//		// Price Depth Registration
//		TPBaseRequest lvBaseRequest = ivTPManager.getRequest(RequestName.HSIPriceDepthRequest);
//		Hashtable lvParameterMap = new Hashtable();
//		//lvParameterMap.put(HsiTag.WINDOW_ID, ivAppletClientID.toString());
//		lvParameterMap.put(HsiTag.SERIESID, pSeriesID);
//		lvParameterMap.put("SUBSCRIBE", "Y");
//
//		//Log.println("Price Depth Reg " + pSeriesID, Log.DEBUG_LOG);
//		IMsgXMLNode lvRetNode = lvBaseRequest.send(lvParameterMap);
//
//		lvErrorHandling.checkError(lvRetNode);
//		if (lvErrorHandling.getStatus() == TPErrorHandling.TP_NORMAL)
//		{
//			/*
//			Hashtable   lvRet = ivBroadcastManager.getDigestedPriceDepthMessage(lvRetNode);
//			if (lvRet != null)
//			{
//				String  lvPriceData = (String) lvRet.get("PRICE");
//				String  lvQtyData = (String) lvRet.get("QTY");
//				String  lvSeriesID = (String) lvRet.get("SERIESID");
//
//				try
//				{
//					// for the first time, put the first image to map
//					ivBroadcastManager.svPriceDepthRTDataMap.put(lvSeriesID, lvPriceData + lvQtyData);
//				}
//				catch (Exception e)
//				{
//				}
//			}
//			*/
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//	private boolean RegisterPriceInformation(String pSeriesID)
//	{
//		TPErrorHandling lvErrorHandling = new TPErrorHandling();
//
//		// Price Information Registration
//		TPBaseRequest lvBaseRequest = ivTPManager.getRequest(RequestName.HSIPriceInformationRequest);
//		Hashtable lvParameterMap = new Hashtable();
//		//lvParameterMap.put("WINDOWID", ivAppletClientID.toString());
//		lvParameterMap.put(HsiTag.SERIESID, pSeriesID);
//		lvParameterMap.put("SUBSCRIBE", "Y");
//
//		//Log.println("Price Info Reg " + pSeriesID, Log.DEBUG_LOG);
//		IMsgXMLNode lvRetNode = lvBaseRequest.send(lvParameterMap);
//
//		lvErrorHandling.checkError(lvRetNode);
//		if (lvErrorHandling.getStatus() == TPErrorHandling.TP_NORMAL)
//		{
//			/*
//			Hashtable lvRet = ivBroadcastManager.getDigestedPriceInfoMessage(lvRetNode);
//
//			if (lvRet != null)
//			{
//				String  lvData = (String) lvRet.get("DATA");
//				String  lvSeriesID = (String) lvRet.get("SERIESID");
//				try
//				{
//					// for the first time, put the first image to map
//					ivBroadcastManager.svPriceInfoRTDataMap.put(lvSeriesID, lvData);
//				}
//				catch (Exception e)
//				{
//				}
//			}
//			*/
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
	
	private void queryMarketInformation(){
		Log.println("[ HKSOperatorLoginThread.queryMarketInformation(): Started]", Log.ACCESS_LOG);
		HKSGetMarketStatusInfoTxn lvHKSMarketStatusTxn = new HKSGetMarketStatusInfoTxn();
		
		try {
			lvHKSMarketStatusTxn.getMarketIndexInfo();
			List<HKSMarketIndexBean> marketBean = lvHKSMarketStatusTxn.getMarketIndexList();
			for (HKSMarketIndexBean marketIndexItem : marketBean) {
				IMain.mvMarketIndex.put(marketIndexItem.getMvMaketID().trim(), marketIndexItem);
				IMain.mvMarketStatus.put(marketIndexItem.getMvMaketID().trim(), marketIndexItem.getMvMarketStatus().trim());
			}
			
		} catch(Exception ex) {
			Log.println(ex, Log.ERROR_LOG);
		}
		int lvReturnCode = lvHKSMarketStatusTxn.getReturnCode();
		if (lvReturnCode != TPErrorHandling.TP_NORMAL) {
			if (lvReturnCode == TPErrorHandling.TP_APP_ERR) {
				Log.println("[ HKSOperatorLoginThread.queryMarketInformation(): Check TP Status Action failed with application error:" +
						lvHKSMarketStatusTxn.getErrorMessage() + " ]",
						Log.ALERT_LOG);
			} else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR) {
				Log.println("[ HKSOperatorLoginThread.queryMarketInformation(): Check TP Status Action failed with system error:" +
						lvHKSMarketStatusTxn.getErrorMessage() + " ]",
						Log.ALERT_LOG);
			}
		} 
		
		Log.println("[ HKSOperatorLoginThread.queryMarketInformation(): Completed]", Log.ACCESS_LOG);
	}
}
