/**
 * @author: Wind Zhao
 * @create date : 20091116
 */
//Begin Task #: TTL-GZ-ZZW-00010 Wind Zhao 20091116 for [ITradeR5]Advance payment online
package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.systekit.winvest.hks.config.mapping.TagName;
/**
 * The HKSOnLineAdvancePaymentTxn class definition for all method
 *  On-line advance processing
 * @author Wind Zhao
 * @since 20091116
 */
public class HKSOnLineAdvancePaymentTxn extends BaseTxn {
	private String mvClientID;
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
	public void setMvClientID(String pClientID){
		this.mvClientID = pClientID;
	}
	/**
	 * Get method for client id
	 * @return  client id
	 */
	public String getMvClientID(){
		return this.mvClientID;
	}
	/**
	 * Constructor for HKSOnLineAdvancePaymentTxn class
	 * @param pClientID the client id
	 */
	public HKSOnLineAdvancePaymentTxn(String pClientID) {
		super();
		this.mvClientID = pClientID;
	}
	/**
	 * The method for On-line advance processing
	 */
	public void process(){
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(TagName.CLIENTID, this.mvClientID);
		
		if(TPErrorHandling.TP_NORMAL==process(RequestName.HKSOnLineAdvancePaymentRequest,lvTxnMap)){
			
			
		}
	}
}
//End Task #: TTL-GZ-ZZW-00010 Wind Zhao 20091116 for [ITradeR5]Advance payment online