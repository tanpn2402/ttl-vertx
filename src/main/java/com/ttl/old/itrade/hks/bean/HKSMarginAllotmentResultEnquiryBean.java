package com.ttl.old.itrade.hks.bean;

/**
 * The HKSMarginAllotmentResultEnquiryBean class define variables that to save values 
 * for action 
 * @author
 *
 */
public class HKSMarginAllotmentResultEnquiryBean {
	private String mvStockID;
	private String mvStockName;
	private String mvAppliedQty;
	private String mvConfirmQty;
	private String mvUnAllottedQty;
	private String mvRefund;
	
	/**
     * This method returns the stock id.
     * @return the stock id.
     */
	public String getMvStockID() {
		return mvStockID;
	}
	
	/**
     * This method sets the stock id.
     * @param pStockCode The stock id.
     */
	public void setMvStockID(String pStockID) {
		mvStockID = pStockID;
	}
	
	/**
     * This method returns the name of stock.
     * @return the name of stock.
     */
	public String getMvStockName() {
		return mvStockName;
	}
	
	/**
     * This method sets the name of stock.
     * @param pStockName The name of stock.
     */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}
	
	/**
     * This method returns the applied quantity.
     * @return the applied quantity is formatted.
     */
	public String getMvAppliedQty() {
		return mvAppliedQty;
	}
	
	/**
     * This method sets the applied quantity.
     * @param pAppliedQty the applied quantity is formatted.
     */
	public void setMvAppliedQty(String pAppliedQty) {
		mvAppliedQty = pAppliedQty;
	}
	
	/**
     * This method returns the confirm quantity.
     * @return the confirm quantity is formatted.
     */
	public String getMvConfirmQty() {
		return mvConfirmQty;
	}
	/**
     * This method sets the confirm quantity.
     * @param pConfirmQty the confirm quantity is formatted.
     */
	public void setMvConfirmQty(String pConfirmQty) {
		mvConfirmQty = pConfirmQty;
	}
	
	/**
     * This method returns the unallotted quantity.
     * @return the unallotted quantity is formatted.
     */
	public String getMvUnAllottedQty() {
		return mvUnAllottedQty;
	}
	
	/**
     * This method sets the unallotted quantity.
     * @param pUnAllottedQty the unallotted quantity quantity is formatted.
     */
	public void setMvUnAllottedQty(String pUnAllottedQty) {
		mvUnAllottedQty = pUnAllottedQty;
	}
	
	/**
     * This method returns the refund.
     * @return the refund.
     */
	public String getMvRefund() {
		return mvRefund;
	}
	
	/**
     * This method sets the refund .
     * @param pRefund the refund is formatted.
     */
	public void setMvRefund(String pRefund) {
		mvRefund = pRefund;
	}
}