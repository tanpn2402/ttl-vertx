package com.ttl.old.itrade.hks.txn;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.ttl.old.itrade.hks.bean.HKSCustomerServiceBean;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.ErrorCode;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

public class HKSCustomerServiceTxn extends BaseTxn{
	public static final String CLIENTID = "CLIENTID";
	private final String HKS_DEFAULT_ITRADE_SERVICE = "DefaultService";
	private final String HKS_CUSTOMER_SERVICE = "CustomerService";
	
	private String clientID;
	private List<HKSCustomerServiceBean> lvListCustomerService = null;
	private List<HKSCustomerServiceBean> lvListDefaultItradeService = null;
	
	public List<HKSCustomerServiceBean> getLvListDefaultItradeService() {
		return lvListDefaultItradeService;
	}

	public void setLvListDefaultItradeService(
			List<HKSCustomerServiceBean> lvListDefaultItradeService) {
		this.lvListDefaultItradeService = lvListDefaultItradeService;
	}

	public List<HKSCustomerServiceBean> getLvListCustomerService() {
		return lvListCustomerService;
	}

	public void setLvListCustomerService(
			List<HKSCustomerServiceBean> lvListCustomerService) {
		this.lvListCustomerService = lvListCustomerService;
	}

	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}

	public HKSCustomerServiceTxn(String clientID){
		this.clientID = clientID;
	}
	
	public void process(){
		process(null);
	}
	
	private void process(Presentation presentation) {
		getCustomerServiceByClientID();
	}
	
	private void getCustomerServiceByClientID(){
		Log.println("[ HKSCustomerServiceTxn._process() CLIENTID : " + getClientID() + " ]", Log.ACCESS_LOG);
		Hashtable lvTxnMap = new Hashtable();
		lvTxnMap.put(CLIENTID, getClientID());
		if (TPErrorHandling.TP_NORMAL != process(RequestName.ItradeCustomerService, lvTxnMap, "en_US")) {
			if (mvReturnNode.getChildNode("C_ERROR_CODE").getValue().equalsIgnoreCase("TPBASEREQERROR")) {
				setErrorCode(new ErrorCode(new String[0], "0100", "No connection with PLugin TP"));
			}

			Log.println("Error error occur when getting Customer Service", Log.ACCESS_LOG);
		}else {
			try{
				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(HKS_DEFAULT_ITRADE_SERVICE);
				lvListDefaultItradeService = new ArrayList<HKSCustomerServiceBean>();
				HKSCustomerServiceBean lvBean = null;
				for (int k = 0; k < lvRowList.size(); k++){
					IMsgXMLNode lvRow = lvRowList.getNode(k);
					lvBean = new HKSCustomerServiceBean();
					lvBean.setServiceID(lvRow.getChildNode("SERVICEID").getValue());
					lvBean.setServiceName(lvRow.getChildNode("SERVICENAME").getValue());
					lvListDefaultItradeService.add(lvBean);
				}
				
				IMsgXMLNodeList lvRowList1 = mvReturnNode.getNodeList(HKS_CUSTOMER_SERVICE);
				lvListCustomerService = new ArrayList<HKSCustomerServiceBean>();
				HKSCustomerServiceBean lvBean1 = null;
				for (int i = 0; i < lvRowList1.size(); i++) {
					IMsgXMLNode lvRow1 = lvRowList1.getNode(i);
					lvBean1 = new HKSCustomerServiceBean();
					lvBean1.setServiceID(lvRow1.getChildNode("SERVICEID").getValue());
					lvBean1.setServiceName(lvRow1.getChildNode("SERVICENAME").getValue());
					lvBean1.setServiceStatus(lvRow1.getChildNode("SERVICESTATUS").getValue());
					lvListCustomerService.add(lvBean1);
				}
			}catch (Exception ex){
				Log.println("Error error occur when getting Customer Service Txn: " + ex.getMessage(), Log.ACCESS_LOG);
			}
		}
	}
}
