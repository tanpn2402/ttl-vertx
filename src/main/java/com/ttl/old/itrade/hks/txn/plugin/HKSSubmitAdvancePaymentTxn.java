package com.ttl.old.itrade.hks.txn.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.hks.util.AgentUtils;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.winvest.hks.config.mapping.TagName;

/**
 * The HKSSubmitAdvancePaymentTxn class definition for all method On-line submit advance payment processing
 * 
 * @author Wind.Zhao
 */

// End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100122 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSSubmitAdvancePaymentTxn extends BaseTxn {
	private String mvClientID;
	private String mvTradingAccSeq;
	private String mvBankID;
	private String mvBankACID;
	private String mvTPLUSX;
	private String mvLendingAmount;
	private String mvOrderIDArray;
	private String mvContractIDArray;
	private String mvPassword;
	private String mvSecurityCode;
	private String mvInterestAmt;
	private String mvClientName;
	private String totalAmt;
	private String mvResult;
	private String mvReturnCode;
	private String mvReturnMsg;
	private String mvAdvAvai;
	private String mvAdvReq;

	public String getMvAdvAvai() {
		return mvAdvAvai;
	}

	public void setMvAdvAvai(String mvAdvAvai) {
		this.mvAdvAvai = mvAdvAvai;
	}

	public String getMvAdvReq() {
		return mvAdvReq;
	}

	public void setMvAdvReq(String mvAdvReq) {
		this.mvAdvReq = mvAdvReq;
	}

	/**
	 * Set method for client id
	 * 
	 * @param pClientID the client id
	 */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}

	/**
	 * Get method for client id
	 * 
	 * @return client id
	 */
	public String getMvClientID() {
		return mvClientID;
	}
	
	public String getMvTradingAccSeq() {
		return mvTradingAccSeq;
	}

	public void setMvTradingAccSeq(String mvTradingAccSeq) {
		this.mvTradingAccSeq = mvTradingAccSeq;
	}

	/**
	 * Returns the bank id of the advance payment.
	 * 
	 * @return the bank id of the advance payment.
	 */
	public String getMvBankID() {
		return mvBankID;
	}

	/**
	 * Sets the bank id.
	 * 
	 * @param pBankAccount The bank id.
	 */
	public void setMvBankID(String pBankID) {
		mvBankID = pBankID;
	}

	/**
	 * Return the bank account id.
	 * 
	 * @return the bank account id.
	 */
	public String getMvBankACID() {
		return mvBankACID;
	}

	/**
	 * Sets the bank account id.
	 * 
	 * @param pBankACID the bank account id.
	 */
	public void setMvBankACID(String pBankACID) {
		mvBankACID = pBankACID;
	}

	/**
	 * Sets the tplusx.
	 * 
	 * @return the tplusx
	 */
	public String getMvTPLUSX() {
		return mvTPLUSX;
	}

	/**
	 * Sets the tplusx
	 * 
	 * @param pTPLUSX the tplusx.
	 */
	public void setMvTPLUSX(String pTPLUSX) {
		mvTPLUSX = pTPLUSX;
	}

	/**
	 * Returns lending amount.
	 * 
	 * @return the lending amount.
	 */
	public String getMvLendingAmount() {
		return mvLendingAmount;
	}

	/**
	 * Sets the lending amount.
	 * 
	 * @param pLendingAmount the lending amount.
	 */
	public void setMvLendingAmount(String pLendingAmount) {
		mvLendingAmount = pLendingAmount;
	}

	/**
	 * Returns the order id array.
	 * 
	 * @return the order id array.
	 */
	public String getMvOrderIDArray() {
		return mvOrderIDArray;
	}

	/**
	 * Sets the order id array.
	 * 
	 * @param pOrderIDArray the order id array.
	 */
	public void setMvOrderIDArray(String pOrderIDArray) {
		mvOrderIDArray = pOrderIDArray;
	}

	/**
	 * Returns the contract id array.
	 * 
	 * @return the contract id array.
	 */
	public String getMvContractIDArray() {
		return mvContractIDArray;
	}

	/**
	 * Sets the contract id array.
	 * 
	 * @param pContractIDArray the contract id array.
	 */
	public void setMvContractIDArray(String pContractIDArray) {
		mvContractIDArray = pContractIDArray;
	}

	/**
	 * Returns the password.
	 * 
	 * @return the password.
	 */
	public String getMvPassword() {
		return mvPassword;
	}

	/**
	 * Sets the password.
	 * 
	 * @param pPassword The password.
	 */
	public void setMvPassword(String pPassword) {
		mvPassword = pPassword;
	}

	/**
	 * Return the security code.
	 * 
	 * @return the security code.
	 */
	public String getMvSecurityCode() {
		return mvSecurityCode;
	}

	/**
	 * Sets the security code.
	 * 
	 * @param pSecurityCode the security code.
	 */
	public void setMvSecurityCode(String pSecurityCode) {
		mvSecurityCode = pSecurityCode;
	}

	/**
	 * submitAdvancePayment
	 * 
	 * @return
	 */
	public Map<String, String> submitAdvancePayment() {
		Map<String, String> result = new HashMap<String, String>();
		Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();
		try {
			Log.println("HKSSubmitAdvancePaymentTxn.submitAdvancePayment START: Client ID = " + mvClientID + " Settle date: " + mvTPLUSX,
					Log.ACCESS_LOG);
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put(TagName.TRADINGACCSEQ, mvTradingAccSeq);
			lvTxnMap.put(TagName.AMOUNT, mvLendingAmount);
			lvTxnMap.put(TagName.INTERESTAMOUNT, mvInterestAmt);
			lvTxnMap.put(TagName.INTERESTID, IMain.getProperty("localAPFeeID"));
			lvTxnMap.put(TagName.CLIENTNAME, mvClientName);
			lvTxnMap.put(TagName.SETTLEDATE, mvTPLUSX);

			lvTxnMap.put(TagName.MININTERESTACCURED, IMain.getProperty("localMinInterest"));

			// Send TP request
			if (TPErrorHandling.TP_NORMAL == process("SubmitAdvancePayment", lvTxnMap)) {
				setMvResult(mvReturnNode.getChildNode(TagName.RESULT).getValue());
				setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
				setMvReturnMsg(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
			}
		} catch (Exception e) {
			Log.println("HKSSubmitAdvancePaymentTxn.submitAdvancePayment Error: " + e.getMessage(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSSubmitAdvancePaymentTxn.submitAdvancePayment END: Client ID = " + mvClientID + " Settle date: " + mvTPLUSX,
					Log.ACCESS_LOG);
			lvTxnMap.clear();
		}

		return result;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, String> submitAdvancePaymentCreation() {
		Map<String, String> result = new HashMap<String, String>();
		Hashtable lvTxnMap = new Hashtable();
		try {
			Log.println("HKSSubmitAdvancePaymentTxn.submitAdvancePaymentCreation START: Client ID = " + mvClientID, Log.ACCESS_LOG);
			// HIEU LE:
			lvTxnMap.put("OPRID", AgentUtils.getAgentID(this.getMvClientID()));
			lvTxnMap.put("EMAILLIST", AgentUtils.getOperatorEmails(this.getMvClientID(), AgentUtils.FUNCTION_TYPE_ADVANCE.toUpperCase()));
			// END HIEU LE
			lvTxnMap.put(TagName.CLIENTID, mvClientID);
			lvTxnMap.put(TagName.AMOUNT, mvLendingAmount);
			lvTxnMap.put(TagName.FULLNAME, mvClientName);
			lvTxnMap.put("ADVAVAIABLE", mvAdvAvai);
			lvTxnMap.put("ADVREQUEST", mvAdvReq);
			// BEGIN: Bao Tran 20140113 issue #VNPJT-1173
			lvTxnMap.put("ISAPPROVAL", IMain.getProperty("IsApprovalAdvance"));
			// END: Bao Tran 20140113 issue #VNPJT-1173

			// Send TP request
			if (TPErrorHandling.TP_NORMAL == process("SubmitAdvancePaymentCreation", lvTxnMap)) {
				setMvResult(mvReturnNode.getChildNode(TagName.RESULT).getValue());
				setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
				setMvReturnMsg(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
			}
		} catch (Exception e) {
			Log.println("HKSSubmitAdvancePaymentTxn.submitAdvancePaymentCreation Error: " + e.getMessage(), Log.ERROR_LOG);
		} finally {
			Log.println("HKSSubmitAdvancePaymentTxn.submitAdvancePaymentCreation END: Client ID = " + mvClientID, Log.ACCESS_LOG);
			lvTxnMap.clear();
		}

		return result;
	}

	/**
	 * Constructor for HKSSubmitAdvancePaymentTxn class
	 * 
	 * @param [1]pClientID the client id. [2]pBankID the bank id. [3]pBankACID the bank account id. [4]pTPLUSX the tplusx. [5]pLendingAmount
	 *        the lending amount. [6]pOrderIDList the order id list. [7]pContractIDList the contract id list. [8]pPassword the password.
	 *        [9]pSecurityCode the security code.
	 * @author Wind.Zhao
	 */
	public HKSSubmitAdvancePaymentTxn(String pClientID, String pTradingAccSeq, String pBankID, String pBankACID, String pTPLUSX, String pLendingAmount,
			String pOrderIDArray, String pContractIDArray, String pPassword, String pSecurityCode) {
		mvClientID = pClientID;
		mvTradingAccSeq = pTradingAccSeq;
		mvBankID = pBankID;
		mvBankACID = pBankACID;
		mvTPLUSX = pTPLUSX;
		mvLendingAmount = pLendingAmount;
		mvOrderIDArray = pOrderIDArray;
		mvContractIDArray = pContractIDArray;
		mvPassword = pPassword;
		mvSecurityCode = pSecurityCode;
	}

	public HKSSubmitAdvancePaymentTxn(String pClientID, String pTradingAccSeq, String pBankID, String pBankACID, String pTPLUSX, String pLendingAmount,
			String pOrderIDArray, String pContractIDArray, String pPassword, String pSecurityCode, String pTotal, String pName) {
		mvClientID = pClientID;
		mvTradingAccSeq = pTradingAccSeq;
		mvBankID = pBankID;
		mvBankACID = pBankACID;
		mvTPLUSX = pTPLUSX;
		mvLendingAmount = pLendingAmount;
		mvOrderIDArray = pOrderIDArray;
		mvContractIDArray = pContractIDArray;
		totalAmt = pTotal;
		mvClientName = pName;
		mvPassword = pPassword;
		mvSecurityCode = pSecurityCode;
	}

	public HKSSubmitAdvancePaymentTxn(String pClientID, String pTradingAccSeq, String pTPLUSX, String pLendingAmount, String pInterestAmt, String pClientName) {
		mvClientID = pClientID;
		mvTradingAccSeq = pTradingAccSeq;
		mvTPLUSX = pTPLUSX;
		mvLendingAmount = pLendingAmount;
		setMvInterestAmt(pInterestAmt);
		setMvClientName(pClientName);
	}

	/**
	 * The method for On-line advance processing
	 * 
	 * @return the hash table of all the advance payment's information.
	 * @author Wind.Zhao
	 */
	public List<Hashtable<String, String>> process() {

		// Send TP request
		List<Hashtable<String, String>> result = new ArrayList<Hashtable<String, String>>();
		Hashtable<String, String> lvTxnMap = new Hashtable<String, String>();
		lvTxnMap.put(TagName.CLIENTID, mvClientID);
		lvTxnMap.put(TagName.TRADINGACCSEQ, mvTradingAccSeq);
		lvTxnMap.put(TagName.BANKID, mvBankID);
		lvTxnMap.put(TagName.TPLUSX, mvTPLUSX);
		lvTxnMap.put("LENDINGAMOUNT", mvLendingAmount);
		lvTxnMap.put("ORDERIDLIST", mvOrderIDArray);
		lvTxnMap.put("CONTRACTIDLIST", mvContractIDArray);
		lvTxnMap.put(TagName.TOTALAMT, totalAmt);
		lvTxnMap.put(TagName.CLIENTNAME, mvClientName);
		lvTxnMap.put(TagName.FEEID, IMain.getProperty("bankAPFeeID"));

		if (TPErrorHandling.TP_NORMAL == process("SubmitBankAdvance", lvTxnMap)) {			
			setMvResult(mvReturnNode.getChildNode(TagName.RESULT).getValue());
			setMvReturnCode(mvReturnNode.getChildNode("RETURNCODE").getValue());
			setMvReturnMsg(mvReturnNode.getChildNode(TagName.RETURNMESSAGE).getValue());
		}
		return result;
	}

	public void setMvInterestAmt(String mvInterestAmt) {
		this.mvInterestAmt = mvInterestAmt;
	}

	public String getMvInterestAmt() {
		return mvInterestAmt;
	}

	public void setMvClientName(String mvClientName) {
		this.mvClientName = mvClientName;
	}

	public String getMvClientName() {
		return mvClientName;
	}

	public void setTotalAmt(String totalAmt) {
		this.totalAmt = totalAmt;
	}

	public String getTotalAmt() {
		return totalAmt;
	}

	public void setMvResult(String mvResult) {
		this.mvResult = mvResult;
	}

	public String getMvResult() {
		return mvResult;
	}

	public void setMvReturnCode(String mvReturnCode) {
		this.mvReturnCode = mvReturnCode;
	}

	public String getMvReturnCode() {
		return mvReturnCode;
	}

	public void setMvReturnMsg(String mvReturnMsg) {
		this.mvReturnMsg = mvReturnMsg;
	}

	public String getMvReturnMsg() {
		return mvReturnMsg;
	}

}
