//BEGIN - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue
package com.ttl.old.itrade.hks;

import com.ttl.old.itrade.hks.request.RequestName;
//import com.itrade.hks.txn.HKSCheckTPStatusTxn;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.tp.txn.OperatorLoginTxn;
import com.ttl.old.itrade.util.Log;

/**
 * The HKSExternalOperatorLoginThread class defines methods that operator external Login Thread started going to check.
 * @author 
 *
 */
class HKSExternalOperatorLoginThread extends Thread {

        private TPManager ivTPManager = null;
        private static int svNumberOfAvailableGoodTillDate = 7;
        //BEGIN Task #: WL00619 Walter Lau 27 July 2007
        private static String svProductID = "HKS";
        private static String svUsingStockCodeFormatMarket = "HKEX";
        //BEGIN - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument

        /**
         * Constructor for HKSExternalOperatorLoginThread class.
         * @param pTPManager The TPManager object.
         * @param pNumberOfAvailableGoodTillDate The number of available good till date.
         * @param pProductID The Product ID.
         * @param pUsingStockCodeFormatMarket The using stock code with format market.
         */
        HKSExternalOperatorLoginThread(TPManager pTPManager, int pNumberOfAvailableGoodTillDate, String pProductID, String pUsingStockCodeFormatMarket)
        {
                ivTPManager = pTPManager;
                svNumberOfAvailableGoodTillDate = pNumberOfAvailableGoodTillDate;
                svProductID = pProductID;
                svUsingStockCodeFormatMarket = pUsingStockCodeFormatMarket;
        }
        //End Task #: WL00619 Walter Lau 27 July 2007

        
        /**
         * Judge returnCode of external operator Login whether correct.
         */
        public void run()
        {
                OperatorLoginTxn lvExternalOperatorLoginTxn = new OperatorLoginTxn(ivTPManager, RequestName.HKSOperatorLoginRequest);
                lvExternalOperatorLoginTxn.process();

                int returnCode = lvExternalOperatorLoginTxn.getReturnCode();
                if(returnCode != TPErrorHandling.TP_NORMAL)
                {
                        if (returnCode == TPErrorHandling.TP_APP_ERR)
                        {
                                Log.println("[ TPConnector.operatorLogin() failed with application error:" + lvExternalOperatorLoginTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                        } else if (returnCode == TPErrorHandling.TP_SYS_ERR)
                        {
                                Log.println("[ TPConnector.operatorLogin() failed with system error:" + lvExternalOperatorLoginTxn.getErrorMessage() + " ]", Log.ERROR_LOG);
                        }
                }
                else
                {
                        checkTPStatus();
                }
        }



        //BEGIN - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument
        /**
         * Check the status of TP.
         */
        private void checkTPStatus()
        {
                Log.println("[ HKSExternalOperatorLoginThread.checkStatus(): Started]", Log.ACCESS_LOG);
               // HKSCheckTPStatusTxn lvHKSCheckTPStatusTxn = new HKSCheckTPStatusTxn();
                try{
                       // lvHKSCheckTPStatusTxn.process();
                }
                catch(Exception ex)
                {
                        Log.println(ex, Log.ERROR_LOG);
                        
                }
               // int lvReturnCode = lvHKSCheckTPStatusTxn.getReturnCode();
               /* if (lvReturnCode != TPErrorHandling.TP_NORMAL)
                {
                        if (lvReturnCode == TPErrorHandling.TP_APP_ERR)
                        {
                                Log.println("[ ExternalTPManager.checkTPStatusProcess(): Check TP Status Action failed with application error:" +
                                        lvHKSCheckTPStatusTxn.getErrorMessage() + " ]",
                                        Log.ERROR_LOG);
                        }
                        else if (lvReturnCode == TPErrorHandling.TP_SYS_ERR)
                        {
                                Log.println("[ ExternalTPManager.checkTPStatusProcess(): Check TP Status Action failed with system error:" +
                                        lvHKSCheckTPStatusTxn.getErrorMessage() + " ]",
                                        Log.ERROR_LOG);

                        }

                } */
                Log.println("[ HKSExternalOperatorLoginThread.checkStatus(): Completed]", Log.ACCESS_LOG);
        }
        //END - Task #: TTL-HK-WLCW-00913 - Walter Lau 20090913 Redownload Trade Date and Instrument

}

//END - Task #: TTL-HK-WLCW-00921 - Walter Lau 20090917 Splict TP Connect and Operator Login Theard for fix Redownload issue 
