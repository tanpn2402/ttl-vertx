package com.ttl.old.itrade.hks.txn;

import java.util.HashMap;
import java.util.Hashtable;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;

/**
 * This class process the IPO financing interest rate plan fee
 * @author not attributable
 *
 */
public class IPOFinancingInterestRatePlanFeeTxn extends BaseTxn {
	private TPErrorHandling tpError;
	private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;
    
    private String mvClientID;
	private String mvEntitlementID;
	private String mvAppliedQty;
	private String mvMarginPercentage;
    /**
     * Default constructor for IPOFinancingInterestRatePlanFeeTxn class
     */
	public IPOFinancingInterestRatePlanFeeTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for IPOFinancingInterestRatePlanFeeTxn class
	 * @param pClientID the client id
	 * @param pEntitlementID the entitlement id
	 * @param pAppliedQty applied quantity
	 * @param pMarginPercentage margin percentage
	 */
	public IPOFinancingInterestRatePlanFeeTxn(String pClientID, String pEntitlementID, String pAppliedQty, String pMarginPercentage){
		tpError = new TPErrorHandling();
		setClientID(pClientID);
		setEntitlementID(pEntitlementID);
		setAppliedQty(pAppliedQty);
		setMarginPercentage(pMarginPercentage);
	}
	/**
	 * This method process the IPO financing interest rate plan fee
	 * @return IPO financing interest rate plan fee result set
	 */
	public HashMap process(){
		HashMap lvModel = new HashMap();
		try{
			Log.println("IPOFinancingInterestRatePlanFeeTxn starts", Log.ACCESS_LOG);
			
			Hashtable lvTxnMap = new Hashtable();
			lvTxnMap.put("CLIENTID", getClientID());
			lvTxnMap.put("ENTITLEMENTID", getEntitlementID());
			lvTxnMap.put("APPLIEDQTY", getAppliedQty());
			lvTxnMap.put("MARGINPERCENTAGE", getMarginPercentage());

			if (TPErrorHandling.TP_NORMAL == process(RequestName.HKSMarginInterestRatePlanFeeRequest, lvTxnMap)){
				lvModel.put("FINANCEFEE", mvReturnNode.getChildNode("FINANCEFEE").getValue());
				lvModel.put("LOANAMOUNT", mvReturnNode.getChildNode("LOANAMOUNT").getValue());
				lvModel.put("DEPOSITAMOUNT", mvReturnNode.getChildNode("DEPOSITAMOUNT").getValue());
				lvModel.put("LOANINTEREST", mvReturnNode.getChildNode("LOANINTEREST").getValue());
				lvModel.put("INTERESTRATE", mvReturnNode.getChildNode("INTERESTRATE").getValue());
				lvModel.put("INTERESTRATEBASIS", mvReturnNode.getChildNode("INTERESTRATEBASIS").getValue());
			}
			
		}catch(Exception e)
		{	
			Log.println("IPOFinancingInterestRatePlanFeeTxn error" + e.toString(), Log.ERROR_LOG);
		}
		return lvModel;
	}
	/**
	 * Get method for entitlement id
	 * @return entitlement id
	 */
	public String getEntitlementID(){
		return mvEntitlementID;
	}
	/**
	 * Set method for entitlement id
	 * @param pEntitlementID the entitlement id
	 */
	public void setEntitlementID(String pEntitlementID){
		this.mvEntitlementID=pEntitlementID;
	}
	/**
	 * Get method for applied quantity
	 * @return applied quantity
	 */
	public String getAppliedQty() {
		return mvAppliedQty;
	}
	/**
	 * Set method for applied quantity
	 * @param appliedQty the applied quantity
	 */
	public void setAppliedQty(String appliedQty) {
		mvAppliedQty = appliedQty;
	}
	/**
	 * Get method for margin percentage
	 * @return margin percentage
	 */
	public String getMarginPercentage() {
		return mvMarginPercentage;
	}
	/**
	 * Set method for margin percentage
	 * @param pMarginPercentage the margin percentage
	 */
	public void setMarginPercentage(String pMarginPercentage) {
		mvMarginPercentage = pMarginPercentage;
	}
	/**
	 * Set method for return system code
	 * @param pReturnCode the return system code
	 */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
    /**
     * Set method for error system code
     * @param pErrorCode the error system code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Set method for error system message
     * @param pErrorMessage the error system message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    /**
     * Get method for client id
     * @return client id
     */
	public String getClientID() {
		return mvClientID;
	}
	/**
	 * Set method for client id
	 * @param pClientID the client id
	 */
	public void setClientID(String pClientID) {
		mvClientID = pClientID;
	}
}
