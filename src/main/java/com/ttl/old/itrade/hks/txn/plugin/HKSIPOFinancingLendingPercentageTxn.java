package com.ttl.old.itrade.hks.txn.plugin;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.hks.txn.BaseTxn;
import com.ttl.old.itrade.interfaces.IMain;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;
import com.systekit.common.msg.IMsgXMLParser;
import com.systekit.common.msg.MsgManager;
import com.systekit.tag.ITagXsfTagName;

/**
 * The HKSIPOFinancingLendingPercentageTxn class definition for all method
 * IPO percentage of loan financing
 * @author not attributable
 *
 */
public class HKSIPOFinancingLendingPercentageTxn extends BaseTxn {
	/**
	 * This variable is used to the tp Error
	 */
	private TPErrorHandling tpError;
	private int mvReturnCode;
    private String mvErrorCode;
    private String mvErrorMessage;
    
	private String mvEntitlementID;
	/**
	 * Default constructor for HKSIPOFinancingLendingPercentageTxn class
	 */
	public HKSIPOFinancingLendingPercentageTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSIPOFinancingLendingPercentageTxn class
	 * @param pEntitlementID the entitlement id
	 */
	public HKSIPOFinancingLendingPercentageTxn(String pEntitlementID){
		tpError = new TPErrorHandling();
		setEntitlementID(pEntitlementID);
	}
	/**
	 * This method process IPO percentage of loan financing
	 * @param pEntitlementID the entitlement id
	 * @return The percentage of IPOs to obtain a collection of loan financing
	 */
	public Vector process(String pEntitlementID){
		Vector lvReturn = new Vector();
		try{
			Log.println("HKSIPOFinancingLendingPercentageTxn starts", Log.ACCESS_LOG);
			
			Hashtable lvTxnMap = new Hashtable();
			
			lvTxnMap.put("ENTITLEMENTID", pEntitlementID);
			
			if (TPErrorHandling.TP_NORMAL != process(RequestName.HKSMarginLendingPercentageRequest, lvTxnMap)){
				// TODO: simulate TP response XML
				mvReturnNode = bulidXMLNode();
				
				IMsgXMLNodeList lvRowList = mvReturnNode.getNodeList(ITagXsfTagName.ROW);
				for(int i=0; i< lvRowList.size(); i++){
					IMsgXMLNode lvNode = lvRowList.getNode(i);
					HashMap lvModel = new HashMap();
					lvModel.put("LEADINGPERCENTAGE", lvNode.getChildNode("LEADINGPERCENTAGE").getValue());
					lvReturn.add(lvModel);
				}
			}
		}catch(Exception e)
		{	
			Log.println("HKSIPOFinancingLendingPercentageTxn error" + e.toString(), Log.ERROR_LOG);
		}
		return lvReturn;
	}
	/**
	 * Set method for entitlement id
	 * @param pEntitlementID the entitlement id
	 */
	public void setEntitlementID(String pEntitlementID){
		this.mvEntitlementID=pEntitlementID;
	}
	/**
	 * Set method for return system code
	 * @param pReturnCode the return system code
	 */
	public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }
	/**
	 * Set method for system error code
	 * @param pErrorCode the system error code
	 */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
    /**
     * Set method for system error message
     * @param pErrorMessage the system error message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
    
    /**
	 * This function return a request xml node for submit advance payment.
	 * @return xml node.
	 * @author Wind.Zhao
	 */
	private IMsgXMLNode bulidXMLNode(){
		IMsgXMLNode lvReturnXMLNode = null;
		IMsgXMLParser lvIMsgXMLParser = MsgManager.createParser();
		lvReturnXMLNode = lvIMsgXMLParser.createXMLNode("HKSWB038Q01");
		lvReturnXMLNode.setAttribute("msgId", "HKSWB038Q01");
		lvReturnXMLNode.setAttribute("issueTime", "20100225190637");
		lvReturnXMLNode.setAttribute("issueLoc", "888");
		lvReturnXMLNode.setAttribute("issueMetd", "01");
		lvReturnXMLNode.setAttribute("oprId", IMain.getProperty("AgentID"));
		lvReturnXMLNode.setAttribute("pwd", IMain.getProperty("AgentPassword"));
		lvReturnXMLNode.setAttribute("resvr", "0000000015");
		lvReturnXMLNode.setAttribute("language", "en");
		lvReturnXMLNode.setAttribute("country", "us");
		lvReturnXMLNode.setAttribute("retryForTimeout", "T");
		
		
		IMsgXMLNode lvChild_Row = lvReturnXMLNode.addChildNode("Row");
		lvChild_Row.addChildNode("LEADINGPERCENTAGE").setValue("50");
		IMsgXMLNode lvChild_Row1 = lvReturnXMLNode.addChildNode("Row");
		lvChild_Row1.addChildNode("LEADINGPERCENTAGE").setValue("60");
		IMsgXMLNode lvChild_Row2 = lvReturnXMLNode.addChildNode("Row");
		lvChild_Row2.addChildNode("LEADINGPERCENTAGE").setValue("70");
		
		return lvReturnXMLNode;
	}
}
