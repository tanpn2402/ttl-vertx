package com.ttl.old.itrade.hks.request;

import java.util.Hashtable;
import java.util.Vector;

import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * The HKSModifyOrderRequest class extends TPBaseRequest abstract class defined methods that operation xml with modify order request.
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
public class HKSModifyOrderRequest extends TPBaseRequest
{
	/**
	 * Constructor for HKSModifyOrderRequest class.
	 * @param pTPManager Object of TPManager.
	 * @param pTemplateNode Object of IMsgXMLNode.
	 */
	public HKSModifyOrderRequest(TPManager pTPManager, IMsgXMLNode pTemplateNode)
	{
		super(pTPManager, pTemplateNode);
	}

	/**
	 * This method send data of modify order xml request.
	 * @param pParameterMap The Map about data.
	 */
	public IMsgXMLNode send(Hashtable pParameterMap)
	{
		//Begin Task #RC00181 - Rice Cheng 20090109
//		IMsgXMLNode lvRootNode = getRequestNode();
//		IMsgXMLNode lvLoop0Node = lvRootNode.getChildNode("LOOP_ORDER").getChildNode("LOOP_ORDER_ELEMENT");
//		Vector		lvMissingField = mergeParameter(pParameterMap, lvLoop0Node);
//		if (lvMissingField.size() > 0)
//		{
//			return makeErrorXML(new Exception("Missing mandatory fields: " + lvMissingField.toString()));
//		}
//		else
//		{
//			return send(lvRootNode);
//		}
		return send(pParameterMap, "");
		//End Task #RC00181 - Rice Cheng 20090109
	}
	
	//Begin Task #RC00181 - Rice Cheng 20090109
	/**
	 * This method send IMsgXMLNode of xml request.
	 * @param pParameterMap The Map about data.
	 * @param pLanguage The currently language.
	 */
	public IMsgXMLNode send(Hashtable pParameterMap, String pLanguage)
	{
		IMsgXMLNode lvRootNode = getRequestNode();
		IMsgXMLNode lvLoop0Node = lvRootNode.getChildNode("LOOP_ORDER").getChildNode("LOOP_ORDER_ELEMENT");
		Vector		lvMissingField = mergeParameter(pParameterMap, lvLoop0Node);
		if (lvMissingField.size() > 0)
		{
			return makeErrorXML(new Exception("Missing mandatory fields: " + lvMissingField.toString()));
		}
		else
		{
			return send(lvRootNode);
		}
	}
	//End Task #RC00181 - Rice Cheng 20090109
}
