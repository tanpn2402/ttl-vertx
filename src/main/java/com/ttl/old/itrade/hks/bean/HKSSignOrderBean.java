package com.ttl.old.itrade.hks.bean;

/**
 * The HKSSignOrderBean class define variables that to save values for action
 * 
 * @author
 * 
 *
 */
public class HKSSignOrderBean {
	private int mvOrderBeanID;
	private String mvStockName;
	private String mvStockID;
	
	private String mvFilledQty;
	private String mvFilledPrice;

	private String mvOrderType;
	private String mvClientID;
	private String mvBS;
	private String mvPrice;
	private String mvQty;  
	
	private String mvStatus;
	private String mvCancelQty;
	private String mvShortName;
	private String mvTradeTime;
	
	private String mvMarketID;
	
	private String mvOrderID;
	

	/**
	 * This method returns the stock name
	 * 
	 * @return the stock name.
	 */
	public String getMvStockName() {
		return mvStockName;
	}

	/**
	 * This method sets the stock name
	 * 
	 * @param pStockName The stock name.
	 */
	public void setMvStockName(String pStockName) {
		mvStockName = pStockName;
	}

	
	/**
	 * This method returns the short name
	 * 
	 * @return the short name.
	 */
	public String getMvShortName() {
		return mvShortName;
	}

	/**
	 * This method sets the short name
	 * 
	 * @param pShortName The short name.
	 */
	public void setMvShortName(String pShortName) {
		mvShortName = pShortName;
	}

	/**

	/**
	 * This method returns the trade time
	 * 
	 * @return the trade time.
	 */
	public String getMvTradeTime() {
		return mvTradeTime;
	}

	/**
	 * This method sets the trade time
	 * 
	 * @param pTradeTime The trade time.
	 */
	public void setMvTradeTime(String pTradeTime) {
		mvTradeTime = pTradeTime;
	}


	/**
	 * This method returns the order type
	 * 
	 * @return the order type.
	 */
	public String getMvOrderType() {
		return mvOrderType;
	}

	/**
	 * This method sets the order type
	 * 
	 * @param pOrderType The order type.
	 */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}

	/**
	 * This method returns the stock id
	 * 
	 * @return the stock id.
	 */
	public String getMvStockID() {
		return mvStockID;
	}

	/**
	 * This method sets the stock id
	 * 
	 * @param pStockID The stock id.
	 */
	public void setMvStockID(String pStockID) {
		mvStockID = pStockID;
	}

	/**
	 * This method returns the client id
	 * 
	 * @return the client id.
	 */
	public String getMvClientID() {
		return mvClientID;
	}

	/**
	 * This method sets the client id
	 * 
	 * @param pClientID The client id.
	 */
	public void setMvClientID(String pClientID) {
		mvClientID = pClientID;
	}

	/**
	 * This method returns the B(Buy) or S(Sell)
	 * 
	 * @return the B(Buy) or S(Sell). [0] B is Buy. [1] S is Sell.
	 */
	public String getMvBS() {
		return mvBS;
	}

	/**
	 * This method sets the B(Buy) or S(Sell)
	 * 
	 * @param pBS The B(Buy) or S(Sell).
	 */
	public void setMvBS(String pBS) {
		mvBS = pBS;
	}

	/**
	 * This method returns the price
	 * 
	 * @return the price.
	 */
	public String getMvPrice() {
		return mvPrice;
	}

	/**
	 * This method sets the price
	 * 
	 * @param pPrice The price.
	 */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}

	/**
	 * This method returns the quantity
	 * 
	 * @return the quantity.
	 */
	public String getMvQty() {
		return mvQty;
	}

	/**
	 * This method sets the quantity
	 * 
	 * @param pQty The quantity.
	 */
	public void setMvQty(String pQty) {
		mvQty = pQty;
	}

	/**
	 * This method returns the cancel quantity
	 * 
	 * @return the cancel quantity.
	 */
	public String getMvCancelQty() {
		return mvCancelQty;
	}

	/**
	 * This method sets the cancel quantity
	 * 
	 * @param pCancelQty The cancel quantity.
	 */
	public void setMvCancelQty(String pCancelQty) {
		mvCancelQty = pCancelQty;
	}


	/**
	 * This method returns the filled quantity
	 * 
	 * @return the filled quantity.
	 */
	public String getMvFilledQty() {
		return mvFilledQty;
	}

	/**
	 * This method sets the filled quantity
	 * 
	 * @param pFilledQty The filled quantity.
	 */
	public void setMvFilledQty(String pFilledQty) {
		mvFilledQty = pFilledQty;
	}


	/**
	 * This method returns the status
	 * 
	 * @return the status.
	 */
	public String getMvStatus() {
		return mvStatus;
	}

	/**
	 * This method sets the status
	 * 
	 * @param pStatus The status.
	 */
	public void setMvStatus(String pStatus) {
		mvStatus = pStatus;
	}


	public String getMvFilledPrice() {
		return mvFilledPrice;
	}

	public void setMvFilledPrice(String mvFilledPrice) {
		this.mvFilledPrice = mvFilledPrice;
	}

	public int getMvOrderBeanID() {
		return mvOrderBeanID;
	}

	public void setMvOrderBeanID(int mvOrderBeanID) {
		this.mvOrderBeanID = mvOrderBeanID;
	}

	public String getMvMarketID() {
		return mvMarketID;
	}

	public void setMvMarketID(String mvMarketID) {
		this.mvMarketID = mvMarketID;
	}

	public String getMvOrderID() {
		return mvOrderID;
	}

	public void setMvOrderID(String mvOrderID) {
		this.mvOrderID = mvOrderID;
	}

}
