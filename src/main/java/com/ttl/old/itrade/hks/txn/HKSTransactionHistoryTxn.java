package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title:  Order Enquiry Transaction </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.Presentation;
import com.systekit.common.msg.IMsgXMLNode;
import com.systekit.common.msg.IMsgXMLNodeList;

/**
 * This class store the transaction history
 * @author not attributable
 *
 */
public class HKSTransactionHistoryTxn
{

	private String						mvClientId;
	private String						mvFromDate;
	private String						mvToDate;
	private String						mvStatus;
//	private String						mvOrderRequest;
//	private String						mvDwRequest;

    private String mvTxnType;
//	private String						mvOrderLoopCounter;
//	private String						mvDwLoopCounter;
	private String						mvLoopCounter;
	private int							mvReturnCode;
	private String						mvErrorCode;
	private String						mvErrorMessage;

//	private HKSTxnHistoryOrderDetails[]	mvHKSTxnHistoryOrderDetails;
//	private HKSTxnHistoryDWDetails[]	mvHKSTxnHistoryDWDetails;
	private HKSTxnHistoryDetails[]	mvHKSTxnHistoryDetails;

	public static final String			LOOPCOUNTER = "LoopCounter";
	public static final String			LOOPROWS = "LoopRows";
	public static final String			FIELDNAMES = "FieldNames";
	public static final String			VALUES = "Values";
//	public static final String			CLIENTID = "CLIENTID";
//	public static final String			FROMDATE = "FROMDATE";
//	public static final String			TODATE = "TODATE";
	public static final String			ORDER = "ORDER";
	public static final String			DW = "DW";

	// Response fields from node - ORDER
//	public static final String			ORDERID = "ORDERID";
//	public static final String			SERIESID = "SERIESID";
//	public static final String			STOCKID = "STOCKID";
//	public static final String			PRODUCT = "PRODUCT";
//	public static final String			CONTRACTMONTH = "CONTRACTMONTH";
//	public static final String			BS = "BS";
//	public static final String			PRICE = "PRICE";
//	public static final String			QTY = "QTY";
//	public static final String			VALIDITY = "VALIDITY";
//	public static final String			VALIDITYDATE = "VALIDITYDATE";
//	public static final String			OL = "OL";
//	public static final String			AO = "AO";
//	public static final String			STOP = "STOP";
//	public static final String			SPRICE = "SPRICE";
//	public static final String			REMARKS = "REMARKS";
//	public static final String			CREATETIME = "CREATETIME";

	// added by May on 18-12-2003
//	public static final String			SHORTTIME = "SHORTTIME";

	public static final String			CLIENTID = "CLIENTID";
	public static final String			TRADEDATE = "TRADEDATE";
	public static final String			TYPE = "TYPE";
	public static final String			DESC = "DESC";
	public static final String			NAME = "NAME";
	public static final String			CSHORTNAME = "CSHORTNAME";
        public static final String                      MARKETID = "MARKETID";
	public static final String			STOCKID = "STOCKID";
	public static final String			QTY = "QTY";
	public static final String			AVGPRICE = "AVGPRICE";
	public static final String			AMT = "AMT";
	public static final String			FROMDATE = "FROMDATE";
	public static final String			TODATE = "TODATE";

	TPErrorHandling mvTpError;
	SimpleDateFormat					mvDateFormatter = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Default constructor for HKSTransactionHistoryTxn class
	 */
	public HKSTransactionHistoryTxn()
	{
		mvTpError = new TPErrorHandling();
	}
	;


	/**
	 * Default constructor for HKSTransactionHistoryTxn class
	 * @param pClientId the Client ID
	 */
	public HKSTransactionHistoryTxn(String pClientId)
	{
		setClientId(pClientId);
		mvTpError = new TPErrorHandling();
	}
	;


	/*
	 * Constructor for HKSTransactionHistoryTxn class
	 * @param Client ID
	 * @param From Date
	 * @param To Date
	 * @param Order
	 * @param DW
	 */
//	public HKSTransactionHistoryTxn(String pClientId, String pFromDate, String pToDate, String pOrder, String pDw)
//	{
//		setClientId(pClientId);
//		setFromDate(pFromDate);
//		setToDate(pToDate);
//		setOrder(pOrder);
//		setDW(pDw);
//		mvTpError = new TPErrorHandling();
//	}
	/**
	 * Constructor for HKSTransactionHistoryTxn class
	 * @param pClientId the client id
	 * @param pFromDate the form date
	 * @param pToDate the to date
	 * @param pTxnType the txntype
	 */
    public HKSTransactionHistoryTxn(String pClientId, String pFromDate, String pToDate, String pTxnType)
    {
        setClientId(pClientId);
        setFromDate(pFromDate);
        setToDate(pToDate);
        mvTxnType = pTxnType;
        mvTpError = new TPErrorHandling();
    }


/***	added by May on 18-12-2003	***/
    /**
     * The method for time type conversion
     * @param longTime the long time
     */
	public static String shortTime(String longTime)
	{
		SimpleDateFormat origFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date longDate = new Date();
		try
		{
			longDate = origFormatter.parse(longTime);
		}
		catch (ParseException ex)
		{
			Log.println("[ TransactionHistory.shortTime(): ParseException, longTime=" + longTime + " ]", Log.ERROR_LOG);
		}

		return newFormatter.format(longDate);
/*
		int dot = longTime.lastIndexOf(".");
		if (dot > 0)
			return longTime.substring(0,dot);
		else
			return longTime;
*/
	}
/*******************************************/

//	comment by May on 17-12-2003
//	public void process()
	/**
	 * This method store the transaction history
	 * @param presentation the Presentation class
	 */
	public void process(Presentation presentation)
	{
		try
		{
			Hashtable		lvTxnMap = new Hashtable();

			IMsgXMLNode		lvRetNode;
			//TPManager		ivTPManager = ITradeServlet.getTPManager();
			TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSTransactionHistoryRequest);
			TPBaseRequest   lvTransactionHistoryRequest = ivTPManager.getRequest(RequestName.HKSTransactionHistoryRequest);

			lvTxnMap.put(CLIENTID, getClientId());
			lvTxnMap.put(FROMDATE, getFromDate());
			lvTxnMap.put(TODATE, getToDate());
         lvTxnMap.put("TXNTYPE", mvTxnType);
//			lvTxnMap.put(ORDER, getOrder());
//			lvTxnMap.put(DW, getDW());

			lvRetNode = lvTransactionHistoryRequest.send(lvTxnMap);

			setReturnCode(mvTpError.checkError(lvRetNode));
			if (mvReturnCode != mvTpError.TP_NORMAL)
			{
				setErrorMessage(mvTpError.getErrDesc());
				if (mvReturnCode == mvTpError.TP_APP_ERR)
				{
					setErrorCode(mvTpError.getErrCode());
				}
			}
			else
			{

				IMsgXMLNodeList lvRowList = lvRetNode.getNodeList("Row");

                mvHKSTxnHistoryDetails = new HKSTxnHistoryDetails[lvRowList.size()];
                for (int i = 0; i < lvRowList.size(); i++)
                {
                    mvHKSTxnHistoryDetails[i] = new HKSTxnHistoryDetails();

                    IMsgXMLNode lvRow = lvRowList.getNode(i);
                    mvHKSTxnHistoryDetails[i].setClientId(lvRow.getChildNode("CLIENTID").getValue());
                    mvHKSTxnHistoryDetails[i].setTradeDate(lvRow.getChildNode("CASHSETTLEDATE").getValue());
                    mvHKSTxnHistoryDetails[i].setType(lvRow.getChildNode("TYPE").getValue() == null ? "" : lvRow.getChildNode("TYPE").getValue().trim());
                    mvHKSTxnHistoryDetails[i].setDesc(lvRow.getChildNode("DESCRIPTION").getValue());
                    mvHKSTxnHistoryDetails[i].setQty(lvRow.getChildNode("QTY").getValue());
                    mvHKSTxnHistoryDetails[i].setAvgPrice(lvRow.getChildNode("PRICE").getValue());
                    mvHKSTxnHistoryDetails[i].setAmt(lvRow.getChildNode("SETTLEAMOUNT").getValue());
                    mvHKSTxnHistoryDetails[i].setStockId(lvRow.getChildNode("INSTRUMENTID").getValue());
                    // BEGIN TASK: CL00049 Charlie Lau 20081230
                    mvHKSTxnHistoryDetails[i].setTxnType(lvRow.getChildNode("BS").getValue() == null ? "" : lvRow.getChildNode("BS").getValue().trim());
                    mvHKSTxnHistoryDetails[i].setMvTotalFee(lvRow.getChildNode("TOTALFEE").getValue());
                    // END TASK: CL00062
                }

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * Get method for Client ID
	 * @return Client ID
	 */
	public String getClientId()
	{
		return mvClientId;
	}

	/**
	 * Set method for Client ID
	 * @param pClientId the Client ID
	 */
	public void setClientId(String pClientId)
	{
		mvClientId = pClientId;
	}


	/**
	 * Get method for From Date
	 * @return From Date
	 */
	public String getFromDate()
	{
		return mvFromDate;
	}

	/**
	 * Set method for From Date
	 * @param pFromDate the From Date
	 */
	public void setFromDate(String pFromDate)
	{
		mvFromDate = pFromDate;
	}


	/**
	 * Get method for To Date
	 * @return To Date
	 */
	public String getToDate()
	{
		return mvToDate;
	}

	/**
	 * Set method for To Date
	 * @param pToDate the To Date
	 */
	public void setToDate(String pToDate)
	{
		mvToDate = pToDate;
	}


	/**
	 * Get method for Status
	 * @return Status
	 */
	public String getStatus()
	{
		return mvStatus;
	}

	/**
	 * Set method for Status
	 * @param pStatus the Status
	 */
	public void setStatus(String pStatus)
	{
		mvStatus = pStatus;
	}


	/*
	 * Get method for Order
	 * @return Order
	 */
//	public String getOrder()
//	{
//		return mvOrderRequest;
//	}

	/*
	 * Set method for Order
	 * @param Order
	 */
//	public void setOrder(String pOrderRequest)
//	{
//		mvOrderRequest = pOrderRequest;
//	}


	/*
	 * Get method for DW
	 * @return DW
	 */
//	public String getDW()
//	{
//		return mvDwRequest;
//	}

	/*
	 * Set method for DW
	 * @param DW
	 */
//	public void setDW(String pDwRequest)
//	{
//		mvDwRequest = pDwRequest;
//	}


//	/*
//	 * Get method for Order Loop Counter
//	 * @return Order Loop Counter
//	 */
//	public int getOrderLoopCounter()
//	{
//		int lvCnt;
//		try
//		{
//			lvCnt = Integer.parseInt(mvOrderLoopCounter);
//		}
//		catch (Exception e)
//		{
//			return -1;
//		}
//		return lvCnt;
//	}
//
//	/*
//	 * Set method for Order Loop Counter
//	 * @param Order Loop Counter
//	 */
//	public void setOrderLoopCounter(String pOrderLoopCounter)
//	{
//		mvOrderLoopCounter = pOrderLoopCounter;
//	}

	/*
	 * Get method for Loop Counter
	 * @return Loop Counter
	 */
//	public int getLoopCounter()
//	{
//		int lvCnt;
//		try
//		{
//			lvCnt = Integer.parseInt(mvLoopCounter);
//		}
//		catch (Exception e)
//		{
//			return -1;
//		}
//		return lvCnt;
//	}

	/**
	 * Set method for Loop Counter
	 * @param pLoopCounter the Loop Counter
	 */
	public void setLoopCounter(String pLoopCounter)
	{
		mvLoopCounter = pLoopCounter;
	}

//	/*
//	 * Get method for DW Loop Counter
//	 * @return DW Loop Counter
//	 */
//	public int getDWLoopCounter()
//	{
//		int lvCnt;
//		try
//		{
//			lvCnt = Integer.parseInt(mvDwLoopCounter);
//		}
//		catch (Exception e)
//		{
//			return -1;
//		}
//		return lvCnt;
//	}
//
//	/*
//	 * Set method for DW Loop Counter
//	 * @param DW Loop Counter
//	 */
//	public void setDWLoopCounter(String pDwLoopCounter)
//	{
//		mvDwLoopCounter = pDwLoopCounter;
//	}


//	/*
//	 * Get method for Order Details for Transaction History
//	 * @return Array of Order Details for Transaction History
//	 */
//	public HKSTxnHistoryOrderDetails[] getTxnHistoryOrderDetails()
//	{
//		return mvHKSTxnHistoryOrderDetails;
//	}

//	/*
//	 * Set method for Order Details for Transaction History
//	 * @param Array of Order Details for Transaction History
//	 */
//	public void setTxnHistoryOrderDetails(HKSTxnHistoryOrderDetails[] pHKSTxnHistoryOrderDetails)
//	{
//		mvHKSTxnHistoryOrderDetails = pHKSTxnHistoryOrderDetails;
//	}


//	/*
//	 * Get method for DW Details for Transaction History
//	 * @return Array of DW Details for Transaction History
//	 */
//	public HKSTxnHistoryDWDetails[] getTxnHistoryDWDetails()
//	{
//		return mvHKSTxnHistoryDWDetails;
//	}
//
//	/*
//	 * Set method for DW Details for Transaction History
//	 * @param Array of DW Details for Transaction History
//	 */
//	public void setTxnHistoryDWDetails(HKSTxnHistoryDWDetails[] pHKSTxnHistoryDWDetails)
//	{
//		mvHKSTxnHistoryDWDetails = pHKSTxnHistoryDWDetails;
//	}

	/**
	 * Set method for Details for Transaction History
	 * @param pHKSTxnHistoryDetails the Array of Details for Transaction History
	 */
	public void setTxnHistoryDetails(HKSTxnHistoryDetails[] pHKSTxnHistoryDetails)
	{
		mvHKSTxnHistoryDetails = pHKSTxnHistoryDetails;
	}

	/**
	 * Get method for Details for Transaction History
	 * @return Array of Details for Transaction History
	 */
	public HKSTxnHistoryDetails[] getTxnHistoryDetails()
	{
		return mvHKSTxnHistoryDetails;
	}

	/**
	 * Get method for Return Code
	 * @return Return Code
	 */
	public int getReturnCode()
	{
		return mvReturnCode;
	}

	/**
	 * Set method for Return Code
	 * @param pReturnCode the Return Code
	 */
	public void setReturnCode(int pReturnCode)
	{
		mvReturnCode = pReturnCode;
	}


	/**
	 * Get method for Error Code
	 * @return Error Code
	 */
	public String getErrorCode()
	{
		return mvErrorCode;
	}

	/**
	 * Set method for Error Code
	 * @param pErrorCode the Error Code
	 */
	public void setErrorCode(String pErrorCode)
	{
		mvErrorCode = pErrorCode;
	}

	/**
	 * Get method for Error Message
	 * @return Error Message
	 */
	public String getErrorMessage()
	{
		return mvErrorMessage;
	}

	/**
	 * Set method for Error Message
	 * @param pErrorMessage the Error Message
	 */
	public void setErrorMessage(String pErrorMessage)
	{
		mvErrorMessage = pErrorMessage;
	}
}
