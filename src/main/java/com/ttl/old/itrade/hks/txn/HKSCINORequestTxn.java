package com.ttl.old.itrade.hks.txn;

import java.util.Hashtable;

import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * The HKSCINORequestTxn class defines methodsis that all
 * CINO Request and handling server errors
 * 
 * @author not attributable
 *
 */
public class HKSCINORequestTxn extends BaseTxn{

	private String mvCINO;
	private String mvClientID;

	private String mvCINOfromWinvest;
	private String mvClientIDfromWinest;
	//This variable is used to the tp Error
	TPErrorHandling tpError;

	private int mvReturnCode;
	private String mvErrorCode;
	private String mvErrorMessage;
	/**
	 * Default constructor for HKSCINORequestTxn class
	 */
	public HKSCINORequestTxn(){
		tpError = new TPErrorHandling();
	}
	/**
	 * Default constructor for HKSCINORequestTxn class
	 * @param pCINO the CINO
	 */
	public HKSCINORequestTxn(String pCINO){
		tpError = new TPErrorHandling();
		setCINO(pCINO);
	}
	/**
	 * The mehtod process the CINO Request and handling server errors
	 * 
	 */
	public void process()
	{
		try
		{
			Log.println("[ HKSCINORequestTxn.process(): CINO "+getCINO()+"starts " , Log.ACCESS_LOG);
			Hashtable lvTxnMap = new Hashtable();
			IMsgXMLNode lvRetNode;
			TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSCINORequest);
			TPBaseRequest  lvHKSCINORequest = ivTPManager.getRequest(RequestName.HKSCINORequest);

			lvTxnMap.put("CINO", getCINO());

			lvRetNode = lvHKSCINORequest.send(lvTxnMap);

			setReturnCode(tpError.checkError(lvRetNode));
			if (mvReturnCode != TPErrorHandling.TP_NORMAL)
			{
				setErrorMessage(tpError.getErrDesc());
				if (mvReturnCode == TPErrorHandling.TP_APP_ERR)
				{
					setErrorCode(tpError.getErrCode());
				}
			}
			else
			{
				if(lvRetNode.getChildNode("CLIENTID")!=null && !lvRetNode.getChildNode("CLIENTID").equals("")){
					//setCINOfromWinvest(lvRetNode.getChildNode("CLIENTID").getValue());
                                        setClientID(lvRetNode.getChildNode("CLIENTID").getValue());
				}
			}
		}catch (Exception e)
		{
			Log.println("[ HKSCINORequestTxn.process(): ClientId "+getClientID() +" error" + e.toString(), Log.ERROR_LOG);
		}
	}
	/**
	 * Set method for the System Return Code
	 * @param pReturnCode the System Return Code
	 */
	public void setReturnCode(int pReturnCode)
	{
	    mvReturnCode = pReturnCode;
	}
	/**
	 * Get method for the System Return Code
	 * @param pErrorCode the System Return Code
	 */
	public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }
	/**
	 *  Set method for the System Error Message
	 * @param pErrorMessage the System Error Message
	 */
	public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }
	/**
	 * Get method for the CINO
	 * @return the CINO
	 */
	public String getCINO() {
		return mvCINO;
	}
	/**
	 * Set method for the CINO
	 * @param pCINO the CINO
	 */
	public void setCINO(String pCINO) {
		mvCINO = pCINO;
	}
	/**
	 * Get method for the Client ID
	 * @return the Client ID
	 */
	public String getClientID() {
		return mvClientID;
	}
	/**
	 * Set method for the Client ID
	 * @param pCLIENTID the Client ID
	 */
	public void setClientID(String pCLIENTID) {
		mvClientID = pCLIENTID;
	}
	/**
	 * Get method for the CINO from Winvest
	 * @return the CINO from Winvest
	 */
	public String getCINOfromWinvest() {
		return mvCINOfromWinvest;
	}
	/**
	 * Set method for the CINO from Winvest
	 * @param pCINOfromWinvest
	 */
	public void setCINOfromWinvest(String pCINOfromWinvest) {
		this.mvCINOfromWinvest = pCINOfromWinvest;
	}
	/**
	 * Get method for the Client ID from Winest
	 * @return the Client ID from Winest
	 */
	public String getClientIDfromWinest() {
		return mvClientIDfromWinest;
	}
	/**
	 * Set method for the Client ID from Winest
	 * @param pClientIDfromWinest the Client ID from Winest
	 */
	public void setClientIDfromWinest(String pClientIDfromWinest) {
		this.mvClientIDfromWinest = pClientIDfromWinest;
	}
}
