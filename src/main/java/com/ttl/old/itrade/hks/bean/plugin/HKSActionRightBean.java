package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSActionRightBean class define variables that to save values for action
 * 
 * @author Giang Tran
 * 
 */

// Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSActionRightBean {
	private String stockId;
	private String typeDescription;
	private String remark;
	private String totalScript;
	private String totalIssue;
	private String totalBonusRight;
	private String totalRemainAmt;
	private String paidDate;
	private String payableDate;
	private String status;
	private String price;
	private String issueRatioPer;
	private String issueRatioDelivery;
	private String issueType;
	private String bookCloseDate;
	private String startDate;
	private String stockRate;
	private String cashRate;
	private String isStockCash;
	private String bookCloseQty;

	/**
	 * @return the stockId
	 */
	public String getStockId() {
		return stockId;
	}

	/**
	 * @param stockId the stockId to set
	 */
	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	/**
	 * @return the typeDescription
	 */
	public String getTypeDescription() {
		return typeDescription;
	}

	/**
	 * @param typeDescription the typeDescription to set
	 */
	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the totalScript
	 */
	public String getTotalScript() {
		return totalScript;
	}

	/**
	 * @param totalScript the totalScript to set
	 */
	public void setTotalScript(String totalScript) {
		this.totalScript = totalScript;
	}

	/**
	 * @return the totalIssue
	 */
	public String getTotalIssue() {
		return totalIssue;
	}

	/**
	 * @param totalIssue the totalIssue to set
	 */
	public void setTotalIssue(String totalIssue) {
		this.totalIssue = totalIssue;
	}

	/**
	 * @return the totalBonusRight
	 */
	public String getTotalBonusRight() {
		return totalBonusRight;
	}

	/**
	 * @param totalBonusRight the totalBonusRight to set
	 */
	public void setTotalBonusRight(String totalBonusRight) {
		this.totalBonusRight = totalBonusRight;
	}

	/**
	 * @return the totalRemainAmt
	 */
	public String getTotalRemainAmt() {
		return totalRemainAmt;
	}

	/**
	 * @param totalRemainAmt the totalRemainAmt to set
	 */
	public void setTotalRemainAmt(String totalRemainAmt) {
		this.totalRemainAmt = totalRemainAmt;
	}

	/**
	 * @return the paidDate
	 */
	public String getPaidDate() {
		return paidDate;
	}

	/**
	 * @param paidDate the paidDate to set
	 */
	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the issueRatioPer
	 */
	public String getIssueRatioPer() {
		return issueRatioPer;
	}

	/**
	 * @param issueRatioPer the issueRatioPer to set
	 */
	public void setIssueRatioPer(String issueRatioPer) {
		this.issueRatioPer = issueRatioPer;
	}

	/**
	 * @return the issueRatioDelivery
	 */
	public String getIssueRatioDelivery() {
		return issueRatioDelivery;
	}

	/**
	 * @param issueRatioDelivery the issueRatioDelivery to set
	 */
	public void setIssueRatioDelivery(String issueRatioDelivery) {
		this.issueRatioDelivery = issueRatioDelivery;
	}

	/**
	 * @return the issueType
	 */
	public String getIssueType() {
		return issueType;
	}

	/**
	 * @param issueType the issueType to set
	 */
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	/**
	 * @return the bookCloseDate
	 */
	public String getBookCloseDate() {
		return bookCloseDate;
	}

	/**
	 * @param bookCloseDate the bookCloseDate to set
	 */
	public void setBookCloseDate(String bookCloseDate) {
		this.bookCloseDate = bookCloseDate;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @param stockRate the stockRate to set
	 */
	public void setStockRate(String stockRate) {
		this.stockRate = stockRate;
	}

	/**
	 * @return the stockRate
	 */
	public String getStockRate() {
		return stockRate;
	}

	/**
	 * @param cashRate the cashRate to set
	 */
	public void setCashRate(String cashRate) {
		this.cashRate = cashRate;
	}

	/**
	 * @return the cashRate
	 */
	public String getCashRate() {
		return cashRate;
	}

	/**
	 * @param isStockCash the isStockCash to set
	 */
	public void setIsStockCash(String isStockCash) {
		this.isStockCash = isStockCash;
	}

	/**
	 * @return the isStockCash
	 */
	public String getIsStockCash() {
		return isStockCash;
	}

	public String getPayableDate() {
		return payableDate;
	}

	public void setPayableDate(String payableDate) {
		this.payableDate = payableDate;
	}

	/**
	 * Get original book close quantity
	 * @return
	 */
	public String getBookCloseQty() {
		return bookCloseQty;
	}

	/**
	 * 
	 * @param bookCloseQty
	 */
	public void setBookCloseQty(String bookCloseQty) {
		this.bookCloseQty = bookCloseQty;
	}

}
// End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
