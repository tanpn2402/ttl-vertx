package com.ttl.old.itrade.hks.bean;

/**
 * The HKSModifyOrderFailBean class define variables that to save values 
 * for action 
 * @author Wind Zhao
 *
 */
public class HKSModifyOrderFailBean {
	private String mvBuyOrderTitleColor;
	private String mvBuyOrderBackgroundColor;
	private String mvSellOrderTitleColor;
	private String mvSellOrderBackgroundColor;
	private String mvBS;
	private String mvOrderID;
	private String mvBuyOrSell;
	private String mvInstrumentID;
	private String mvMarketID;
	private String mvInstrumentName;
	private String mvPrice;
	private String mvQuantity;
	private String mvQuantityDescription;
	private String mvOrderType;
	private String mvOrderTypeDescription;
	private String mvGoodTitllDescription;
	private String mvCurrencyID;
	private String mvQuantityValue;
	private String mvPriceValue;
	private String mvOrderTypeValue;
	private String mvOrderIDValue;
	private String mvOrderGroupIDValue;
	private String mvFilledQtyValue;
	private String mvCancelledQutyValue;
	private String mvGrossAmtValue;
	private String mvOrigPriceValue;
	private String mvOrigQtyValue;
	private String mvLotSizeValue;
	private String mvAveragePrice;
	private String mvAON;
	private String mvStopOrderType;
	private String mvValidityDate;
	private String mvActivationDate;
	private String mvRemark;
	private String mvContactPhone;
	private String mvNetAmt;
	private String mvSCRIP;
	private String mvStopPrice;
	private String mvGoodtillDate;
	private String mvGoodTillDateDisable;
	private String mvMultiMarketDisable;
	private String mvErrorMsg;
	private String mvPasswordIncorrect;
	private String mvModifyOrderJS;
	//Begin Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    
	/*
     * mvModifyOrderFor to control hidden or show some fields in 
     * modify order
     */
    private String mvModifyOrderFor;
    
    /**
     * This method returns the modify order for
     * to control show or hidden some fields.
     * @return the modify order for value.
     */
    
    public String getMvModifyOrderFor() {
    	return mvModifyOrderFor;
    }
    
    /**
     * This method sets the modify order for
     * * to control show or hidden some fields.
     * @param pModifyOrderFor The modify order for value.
     */
    public void setMvModifyOrderFor(String pModifyOrderFor) {
    	mvModifyOrderFor = pModifyOrderFor;
    }
    //End Task #: TTL-GZ-ZZW-00011 Wind Zhao 20091118 for [ITradeR5 VN]Allow to modifycancel orders like ROSE's version
    
    /**
     * This method returns the buy order title color.
     * @return the buy order title color.
     */
	public String getMvBuyOrderTitleColor() {
		return mvBuyOrderTitleColor;
	}
	
	/**
     * This method sets the buy order title color.
     * @param pBuyOrderTitleColor The buy order title color.
     */
	public void setMvBuyOrderTitleColor(String pBuyOrderTitleColor) {
		mvBuyOrderTitleColor = pBuyOrderTitleColor;
	}
	
	/**
     * This method returns the buy order background color.
     * @return the buy order background color.
     */
	public String getMvBuyOrderBackgroundColor() {
		return mvBuyOrderBackgroundColor;
	}
	
	/**
     * This method sets the buy order background color.
     * @param pBuyOrderBackgroundColor The buy order background color.
     */
	public void setMvBuyOrderBackgroundColor(String pBuyOrderBackgroundColor) {
		mvBuyOrderBackgroundColor = pBuyOrderBackgroundColor;
	}
	
	/**
     * This method returns the sell order title color.
     * @return the sell order title color.
     */
	public String getMvSellOrderTitleColor() {
		return mvSellOrderTitleColor;
	}
	
	/**
     * This method sets the sell order title color.
     * @param pSellOrderTitleColor The sell order title color.
     */
	public void setMvSellOrderTitleColor(String pSellOrderTitleColor) {
		mvSellOrderTitleColor = pSellOrderTitleColor;
	}
	
	/**
     * This method returns the sell order background color.
     * @return the sell order background color.
     */
	public String getMvSellOrderBackgroundColor() {
		return mvSellOrderBackgroundColor;
	}
	
	/**
     * This method sets the sell order background color.
     * @param pSellOrderBackgroundColor The sell order background color.
     */
	public void setMvSellOrderBackgroundColor(String pSellOrderBackgroundColor) {
		mvSellOrderBackgroundColor = pSellOrderBackgroundColor;
	}
	
	/**
     * This method returns the buy sell.
     * @return the buy sell.
     */
	public String getMvBS() {
		return mvBS;
	}
	
	/**
     * This method sets the buy sell.
     * @param pBS The buy sell.
     */
	public void setMvBS(String pBS) {
		mvBS = pBS;
	}
	
	/**
     * This method returns the order id.
     * @return the order id.
     */
	public String getMvOrderID() {
		return mvOrderID;
	}
	
	/**
     * This method sets the order id.
     * @param pOrderID The order id.
     */
	public void setMvOrderID(String pOrderID) {
		mvOrderID = pOrderID;
	}
	
	/**
     * This method returns the buy or sell.
     * @return the buy or sell.
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the buy or sell.
     * @param pBuyOrSell The buy or sell.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the instrument id.
     * @return the instrument id.
     */
	public String getMvInstrumentID() {
		return mvInstrumentID;
	}
	
	/**
     * This method sets the instrument id.
     * @param pInstrumentID The instrument id.
     */
	public void setMvInstrumentID(String pInstrumentID) {
		mvInstrumentID = pInstrumentID;
	}
	
	/**
     * This method returns the market id.
     * @return the market id.
     */
	public String getMvMarketID() {
		return mvMarketID;
	}
	
	/**
     * This method sets the market id.
     * @param pMarketID The market id.
     */
	public void setMvMarketID(String pMarketID) {
		mvMarketID = pMarketID;
	}
	
	/**
     * This method returns the instrument name.
     * @return the instrument name.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name.
     * @param pInstrumentName The instrument name.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the price.
     * @return the price is formatted.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price.
     * @param pPrice The price is formatted.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the quantity.
     * @return the quantity is formatted.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity.
     * @param pQuantity The quantity is formatted.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the quantity description.
     * @return the quantity description.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description.
     * @param pQuantityDescription The quantity description.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the order type.
     * @return the order type.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the order type.
     * @param pOrderType The order type.
     */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the order type description.
     * @return the order type description.
     */
	public String getMvOrderTypeDescription() {
		return mvOrderTypeDescription;
	}
	
	/**
     * This method sets the order type description.
     * @param pOrderTypeDescription The order type description.
     */
	public void setMvOrderTypeDescription(String pOrderTypeDescription) {
		mvOrderTypeDescription = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till description.
     * @return the good till description.
     */
	public String getMvGoodTitllDescription() {
		return mvGoodTitllDescription;
	}
	
	/**
     * This method sets the good till description.
     * @param pGoodTitllDescription The good till description.
     */
	public void setMvGoodTitllDescription(String pGoodTitllDescription) {
		mvGoodTitllDescription = pGoodTitllDescription;
	}
	
	/**
     * This method returns the currency id.
     * @return the currency id.
     */
	public String getMvCurrencyID() {
		return mvCurrencyID;
	}
	
	/**
     * This method sets the currency id.
     * @param pCurrencyID The currency id.
     */
	public void setMvCurrencyID(String pCurrencyID) {
		mvCurrencyID = pCurrencyID;
	}
	
	/**
     * This method returns the quantity value.
     * @return the quantity value.
     */
	public String getMvQuantityValue() {
		return mvQuantityValue;
	}
	
	/**
     * This method sets the quantity value.
     * @param pQuantityValue The quantity value.
     */
	public void setMvQuantityValue(String pQuantityValue) {
		mvQuantityValue = pQuantityValue;
	}
	
	/**
     * This method returns the price value.
     * @return the price value.
     */
	public String getMvPriceValue() {
		return mvPriceValue;
	}
	
	/**
     * This method sets the price value.
     * @param pPriceValue The price value.
     */
	public void setMvPriceValue(String pPriceValue) {
		mvPriceValue = pPriceValue;
	}
	
	/**
     * This method returns the order type value.
     * @return the order type value.
     */
	public String getMvOrderTypeValue() {
		return mvOrderTypeValue;
	}
	
	/**
     * This method sets the order type value.
     * @param pOrderTypeValue The order type value.
     */
	public void setMvOrderTypeValue(String pOrderTypeValue) {
		mvOrderTypeValue = pOrderTypeValue;
	}
	
	/**
     * This method returns the order id value.
     * @return the order id value.
     */
	public String getMvOrderIDValue() {
		return mvOrderIDValue;
	}
	
	/**
     * This method sets the order id value.
     * @param pOrderIDValue The order id value.
     */
	public void setMvOrderIDValue(String pOrderIDValue) {
		mvOrderIDValue = pOrderIDValue;
	}
	
	/**
     * This method returns the order group id value.
     * @return the order group id value.
     */
	public String getMvOrderGroupIDValue() {
		return mvOrderGroupIDValue;
	}
	
	/**
     * This method sets the order group id value.
     * @param pOrderGroupIDValue The order group id value.
     */
	public void setMvOrderGroupIDValue(String pOrderGroupIDValue) {
		mvOrderGroupIDValue = pOrderGroupIDValue;
	}
	
	/**
     * This method returns the filled quantity value.
     * @return the filled quantity value.
     */
	public String getMvFilledQtyValue() {
		return mvFilledQtyValue;
	}
	
	/**
     * This method sets the filled quantity value.
     * @param pFilledQtyValue The filled quantity value.
     */
	public void setMvFilledQtyValue(String pFilledQtyValue) {
		mvFilledQtyValue = pFilledQtyValue;
	}
	
	/**
     * This method returns the canceled quantity value.
     * @return the canceled quantity value.
     */
	public String getMvCancelledQutyValue() {
		return mvCancelledQutyValue;
	}
	
	/**
     * This method sets the canceled quantity value.
     * @param pCancelledQutyValue The canceled quantity value.
     */
	public void setMvCancelledQutyValue(String pCancelledQutyValue) {
		mvCancelledQutyValue = pCancelledQutyValue;
	}
	
	/**
     * This method returns the gross amount value.
     * @return the gross amount value.
     */
	public String getMvGrossAmtValue() {
		return mvGrossAmtValue;
	}
	
	/**
     * This method sets the gross amount value.
     * @param pGrossAmtValue The gross amount value.
     */
	public void setMvGrossAmtValue(String pGrossAmtValue) {
		mvGrossAmtValue = pGrossAmtValue;
	}
	
	/**
     * This method returns the orig price value.
     * @return the orig price value.
     */
	public String getMvOrigPriceValue() {
		return mvOrigPriceValue;
	}
	
	/**
     * This method sets the orig price value.
     * @param pOrigPriceValue The orig price value.
     */
	public void setMvOrigPriceValue(String pOrigPriceValue) {
		mvOrigPriceValue = pOrigPriceValue;
	}
	
	/**
     * This method returns the orig quantity value.
     * @return the orig quantity value.
     */
	public String getMvOrigQtyValue() {
		return mvOrigQtyValue;
	}
	
	/**
     * This method sets the orig quantity value.
     * @param pOrigQtyValue The orig quantity value.
     */
	public void setMvOrigQtyValue(String pOrigQtyValue) {
		mvOrigQtyValue = pOrigQtyValue;
	}
	
	/**
     * This method returns the lot size value.
     * @return the lot size value.
     */
	public String getMvLotSizeValue() {
		return mvLotSizeValue;
	}
	
	/**
     * This method sets the lot size value.
     * @param pLotSizeValue The lot size value.
     */
	public void setMvLotSizeValue(String pLotSizeValue) {
		mvLotSizeValue = pLotSizeValue;
	}
	
	/**
     * This method returns the average value.
     * @return the average value.
     */
	public String getMvAveragePrice() {
		return mvAveragePrice;
	}
	
	/**
     * This method sets the average value.
     * @param pAveragePrice The average value.
     */
	public void setMvAveragePrice(String pAveragePrice) {
		mvAveragePrice = pAveragePrice;
	}
	
	/**
     * This method returns the aon.
     * @return the aon.
     */
	public String getMvAON() {
		return mvAON;
	}
	
	/**
     * This method sets the aon.
     * @param pAON The aon.
     */
	public void setMvAON(String pAON) {
		mvAON = pAON;
	}
	
	/**
     * This method returns the stop order type.
     * @return the stop order type.
     */
	public String getMvStopOrderType() {
		return mvStopOrderType;
	}
	
	/**
     * This method sets the stop order type.
     * @param pStopOrderType The stop order type.
     */
	public void setMvStopOrderType(String pStopOrderType) {
		mvStopOrderType = pStopOrderType;
	}
	
	/**
     * This method returns the validity date.
     * @return the validity date.
     */
	public String getMvValidityDate() {
		return mvValidityDate;
	}
	
	/**
     * This method sets the validity date.
     * @param pValidityDate The validity date.
     */
	public void setMvValidityDate(String pValidityDate) {
		mvValidityDate = pValidityDate;
	}
	
	/**
     * This method returns the activation date.
     * @return the activation date.
     */
	public String getMvActivationDate() {
		return mvActivationDate;
	}
	
	/**
     * This method sets the activation date.
     * @param pActivationDate The activation date.
     */
	public void setMvActivationDate(String pActivationDate) {
		mvActivationDate = pActivationDate;
	}
	
	/**
     * This method returns the remark.
     * @return the remark.
     */
	public String getMvRemark() {
		return mvRemark;
	}
	
	/**
     * This method sets the remark.
     * @param pRemark The remark.
     */
	public void setMvRemark(String pRemark) {
		mvRemark = pRemark;
	}
	
	/**
     * This method returns the contact phone.
     * @return the contact phone.
     */
	public String getMvContactPhone() {
		return mvContactPhone;
	}
	
	/**
     * This method sets the contact phone.
     * @param pContactPhone The contact phone.
     */
	public void setMvContactPhone(String pContactPhone) {
		mvContactPhone = pContactPhone;
	}
	
	/**
     * This method returns the net amt.
     * @return the net amt.
     */
	public String getMvNetAmt() {
		return mvNetAmt;
	}
	
	/**
     * This method sets the net amt.
     * @param pNetAmt The net amt.
     */
	public void setMvNetAmt(String pNetAmt) {
		mvNetAmt = pNetAmt;
	}
	
	/**
     * This method returns the scrip.
     * @return the scrip.
     */
	public String getMvSCRIP() {
		return mvSCRIP;
	}
	
	/**
     * This method sets the scrip.
     * @param pSCRIP The scrip.
     */
	public void setMvSCRIP(String pSCRIP) {
		mvSCRIP = pSCRIP;
	}
	
	/**
     * This method returns the stop price.
     * @return the stop price.
     */
	public String getMvStopPrice() {
		return mvStopPrice;
	}
	
	/**
     * This method sets the stop price.
     * @param pStopPrice The stop price.
     */
	public void setMvStopPrice(String pStopPrice) {
		mvStopPrice = pStopPrice;
	}
	
	/**
     * This method returns the good till date.
     * @return the good till date is formatted.
     */
	public String getMvGoodtillDate() {
		return mvGoodtillDate;
	}
	
	/**
     * This method sets the good till date.
     * @param pGoodtillDate The good till date is formatted.
     */
	public void setMvGoodtillDate(String pGoodtillDate) {
		mvGoodtillDate = pGoodtillDate;
	}
	
	/**
     * This method returns the good till date disable.
     * @return the good till date disable.
     */
	public String getMvGoodTillDateDisable() {
		return mvGoodTillDateDisable;
	}
	
	/**
     * This method sets the good till date disable.
     * @param pGoodTillDateDisable The good till date disable.
     */
	public void setMvGoodTillDateDisable(String pGoodTillDateDisable) {
		mvGoodTillDateDisable = pGoodTillDateDisable;
	}
	
	/**
     * This method returns the multi market disable.
     * @return the multi market disable.
     */
	public String getMvMultiMarketDisable() {
		return mvMultiMarketDisable;
	}
	
	/**
     * This method sets the multi market disable.
     * @param pMultiMarketDisable The multi market disable.
     */
	public void setMvMultiMarketDisable(String pMultiMarketDisable) {
		mvMultiMarketDisable = pMultiMarketDisable;
	}
	
	/**
     * This method returns the error message.
     * @return the error message is formatted.
     */
	public String getMvErrorMsg() {
		return mvErrorMsg;
	}
	
	/**
     * This method sets the error message.
     * @param pErrorMsg The error message is formatted.
     */
	public void setMvErrorMsg(String pErrorMsg) {
		mvErrorMsg = pErrorMsg;
	}
	
	/**
     * This method returns the password incorrect.
     * @return the password incorrect.
     */
	public String getMvPasswordIncorrect() {
		return mvPasswordIncorrect;
	}
	
	/**
     * This method sets the password incorrect.
     * @param pPasswordIncorrect The password incorrect.
     */
	public void setMvPasswordIncorrect(String pPasswordIncorrect) {
		mvPasswordIncorrect = pPasswordIncorrect;
	}
	
	/**
     * This method returns the modify order js.
     * @return the modify order js.
     */
	public String getMvModifyOrderJS() {
		return mvModifyOrderJS;
	}
	
	/**
     * This method sets the modify order js.
     * @param pModifyOrderJS The modify order js.
     */
	public void setMvModifyOrderJS(String pModifyOrderJS) {
		mvModifyOrderJS = pModifyOrderJS;
	}
}