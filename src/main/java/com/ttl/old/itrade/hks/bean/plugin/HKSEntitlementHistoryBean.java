package com.ttl.old.itrade.hks.bean.plugin;

/**
 * The HKSAdvancePaymentInfoBean class define variables that to save values for action 
 * @author Wind.Zhao
 *
 */

//Begin Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
public class HKSEntitlementHistoryBean {
	private String stockId;
	private String entitlementId;
	private String subscriptionId;
	private String price;
	private String tradeStockCode;
	private String createTime;
	private String comfirmedDate;
	private String remark;
	private String status;
	private String appliedAmt;
	private String resultQty;
	private String exerciseQty;
	private String locationId;
	private String createTimeNoFormat;
	
	
	/**
	 * @return the stockId
	 */
	public String getStockId() {
		return stockId;
	}
	/**
	 * @param stockId the stockId to set
	 */
	public void setStockId(String stockId) {
		this.stockId = stockId;
	}
	/**
	 * @return the entitlementId
	 */
	public String getEntitlementId() {
		return entitlementId;
	}
	/**
	 * @param entitlementId the entitlementId to set
	 */
	public void setEntitlementId(String entitlementId) {
		this.entitlementId = entitlementId;
	}
	/**
	 * @return the subscriptionId
	 */
	public String getSubscriptionId() {
		return subscriptionId;
	}
	/**
	 * @param subscriptionId the subscriptionId to set
	 */
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the tradeStockCode
	 */
	public String getTradeStockCode() {
		return tradeStockCode;
	}
	/**
	 * @param tradeStockCode the tradeStockCode to set
	 */
	public void setTradeStockCode(String tradeStockCode) {
		this.tradeStockCode = tradeStockCode;
	}
	/**
	 * @return the createTime
	 */
	public String getCreateTime() {
		return createTime;
	}
	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	/**
	 * @return the comfirmedDate
	 */
	public String getComfirmedDate() {
		return comfirmedDate;
	}
	/**
	 * @param comfirmedDate the comfirmedDate to set
	 */
	public void setComfirmedDate(String comfirmedDate) {
		this.comfirmedDate = comfirmedDate;
	}
	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the appliedAmt
	 */
	public String getAppliedAmt() {
		return appliedAmt;
	}
	/**
	 * @param appliedAmt the appliedAmt to set
	 */
	public void setAppliedAmt(String appliedAmt) {
		this.appliedAmt = appliedAmt;
	}
	/**
	 * @return the resultQty
	 */
	public String getResultQty() {
		return resultQty;
	}
	/**
	 * @param resultQty the resultQty to set
	 */
	public void setResultQty(String resultQty) {
		this.resultQty = resultQty;
	}
	/**
	 * @return the exerciseQty
	 */
	public String getExerciseQty() {
		return exerciseQty;
	}
	/**
	 * @param exerciseQty the exerciseQty to set
	 */
	public void setExerciseQty(String exerciseQty) {
		this.exerciseQty = exerciseQty;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}
	/**
	 * @param createTimeNoFormat the createTimeNoFormat to set
	 */
	public void setCreateTimeNoFormat(String createTimeNoFormat) {
		this.createTimeNoFormat = createTimeNoFormat;
	}
	/**
	 * @return the createTimeNoFormat
	 */
	public String getCreateTimeNoFormat() {
		return createTimeNoFormat;
	}
}
//End Task #: - TTL-GZ-ZZW-00021 Wind Zhao 20100127 [iTrade R5] Make Advance Payment a Plugin Module
