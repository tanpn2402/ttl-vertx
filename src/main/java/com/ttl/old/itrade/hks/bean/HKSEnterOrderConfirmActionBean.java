package com.ttl.old.itrade.hks.bean;

/**
 * The HKSEnterOrderConfirmActionBean class define variables that to save values 
 * for action 
 * @author Wind zhao
 *
 */
public class HKSEnterOrderConfirmActionBean {

	private String mvIsPasswordSaved;
	private String mvPasswordConfirmation;
	private String mvSCodeEnableForOrders;
	private String mvTransType;
	private String mvBS;
	private String mvBuyOrSell;
	private String mvInstrumentId;
	private String mvInstrumentIdValue;
	private String mvMarketId;
	private String mvClientId;
	private String mvDateTime;
	private String mvInstrumentName;
	private String mvPrice;
	private String mvPriceValue;
	private String mvCurrencyId;
	private String mvQuantity;
	private String mvQuantityValue;
	private String mvOrderType;
	private String mvOrderTypeValue;
	private String mvStopTypeValue;
	private String mvStopPriceValue;
	private String mvFormAttedStopPriceValue;
	private String mvORiginalNetAmount;
	private String mvGoodTillDate;
	private String mvGoodTillDateValue;
	private String mvGrossAMT;
	private String mvFeesCommission;
	private String mvQuantityDescription;
	private String mvOrderTypeDescription;
	private String mvGoodTillDescription;
	private String mvTriggerType;
	//Begin Task #:- TTL-GZ-ZZW-00009 Wind Zhao 20091113 for [ITradeR5 VN]Security Code UI
	private String mvSecurityCodeEnable;
	
	/**
     * This method returns the security code if enable
     * to control security code filed show or not.
     * @return the security code if enable.
     * @type String.
     */
	public String getMvSecurityCodeEnable(){
		return mvSecurityCodeEnable;
	}
	
	/**
     * This method sets the security code if enable
     * to control security code filed show or not.
     * @param pSecurityCodeEnable The security code if enable.
     * @type String.
     */
	public void setMvSecurityCodeEnable(String pSecurityCodeEnable){
		mvSecurityCodeEnable = pSecurityCodeEnable;
	}
	//End Task #:- TTL-GZ-ZZW-00009 Wind Zhao 20091113 for [ITradeR5 VN]Security Code UI
	
	/**
     * This method returns the trigger type of order.
     * @return the trigger type of order.
     * @type String.
     */
	public String getMvTriggerType() {
		return mvTriggerType;
	}
	
	/**
     * This method sets the trigger type of order.
     * @param pTriggerType The trigger type of order.
     * @type String.
     */
	public void setMvTriggerType(String pTriggerType) {
		this.mvTriggerType = pTriggerType;
	}
	
	/**
     * This method returns the type description of order.
     * @return the type description of order.
     * @type String.
     */
	public String getMvOrderTypeDescription() {
		return mvOrderTypeDescription;
	}
	
	/**
     * This method sets the type description of order.
     * @param pOrderTypeDescription The type description of order.
     * @type String.
     */
	public void setMvOrderTypeDescription(String pOrderTypeDescription) {
		this.mvOrderTypeDescription = pOrderTypeDescription;
	}
	
	/**
     * This method returns the good till date description of order.
     * @return the good till date description of order.
     * @type String.
     */
	public String getMvGoodTillDescription() {
		return mvGoodTillDescription;
	}
	
	/**
     * This method sets the good till date description of order.
     * @param pGoodTillDescription The good till date description of order.
     * @type String.
     */
	public void setMvGoodTillDescription(String pGoodTillDescription) {
		this.mvGoodTillDescription = pGoodTillDescription;
	}
	
	/**
     * This method returns the quantity description of order.
     * @return the quantity description of order.
     * @type String.
     */
	public String getMvQuantityDescription() {
		return mvQuantityDescription;
	}
	
	/**
     * This method sets the quantity description of order.
     * @param pQuantityDescription The quantity description of order.
     * @type String.
     */
	public void setMvQuantityDescription(String pQuantityDescription) {
		this.mvQuantityDescription = pQuantityDescription;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order.
     * @type String.
     */
	public String getMvStopPriceValue() {
		return mvStopPriceValue;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pStopPriceValue The stop price of order.
     * @type String.
     */
	public void setMvStopPriceValue(String pStopPriceValue) {
		this.mvStopPriceValue = pStopPriceValue;
	}
	
	/**
     * This method returns the password if saved or not.
     * @return the password if saved or not.
     *         [0] Y is hidden the password field.
     *         [1] N is show the password field.
     * @type String.
     */
	public String getMvIsPasswordSaved() {
		return mvIsPasswordSaved;
	}
	
	/**
     * This method sets the password if saved or not.
     * @param pIsPasswordSaved The password if saved or not.
     * @type String.
     */
	public void setMvIsPasswordSaved(String pIsPasswordSaved) {
		this.mvIsPasswordSaved = pIsPasswordSaved;
	}
	
	/**
     * This method returns the password if confirm or not.
     * @return the password if confirm or not.
     *         [0] Y is confirm the password.
     *         [1] N is not confirm the password.
     * @type String.
     */
	public String getMvPasswordConfirmation() {
		return mvPasswordConfirmation;
	}
	
	/**
     * This method sets the password if confirm or not.
     * @param pPasswordConfirmation The password if confirm or not.
     * @type String.
     */
	public void setMvPasswordConfirmation(String pPasswordConfirmation) {
		this.mvPasswordConfirmation = pPasswordConfirmation;
	}
	
	/**
     * This method returns the security code if need enter or not.
     * @return the password if confirm or not.
     *         [0] true is need to enter security code.
     *         [1] false is not need to enter security code.
     * @type String.
     */
	public String getMvSCodeEnableForOrders() {
		return mvSCodeEnableForOrders;
	}
	
	/**
     * This method sets the security code if need enter or not.
     * @param pSCodeEnableForOrders The security code if need enter or not.
     * @type String.
     */
	public void setMvSCodeEnableForOrders(String pSCodeEnableForOrders) {
		mvSCodeEnableForOrders = pSCodeEnableForOrders;
	}
	
	/**
     * This method returns the type of transaction.
     * @return the type of transaction.
     * @type String.
     */
	public String getMvTransType() {
		return mvTransType;
	}
	
	/**
     * This method sets the type of transaction.
     * @param pTransType The type of transaction.
     * @type String.
     */
	public void setMvTransType(String pTransType) {
		mvTransType = pTransType;
	}
	
	/**
     * This method returns the B(Buy) or S(Sell) of order.
     * @return the B(Buy) or S(Sell)  of order.
     *         [0] B is Buy.
     *         [1] S is Sell.
     * @type String.
     */
	public String getMvBS() {
		return mvBS;
	}
	
	/**
     * This method sets the B(Buy) or S(Sell) of order.
     * @param pBS The B(Buy) or S(Sell) of order.
     * @type String.
     */
	public void setMvBS(String pBS) {
		mvBS = pBS;
	}
	
	/**
     * This method returns the Buy or Sell of order.
     * @return the Buy or Sell  of order.
     *         [0] Buy.
     *         [1] Sell.
     * @type String.
     */
	public String getMvBuyOrSell() {
		return mvBuyOrSell;
	}
	
	/**
     * This method sets the Buy or Sell of order.
     * @param pBuyOrSell The Buy or Sell of order.
     * @type String.
     */
	public void setMvBuyOrSell(String pBuyOrSell) {
		mvBuyOrSell = pBuyOrSell;
	}
	
	/**
     * This method returns the instrument id of order.
     * @return the instrument id of order.
     * @type String.
     */
	public String getMvInstrumentId() {
		return mvInstrumentId;
	}
	
	/**
     * This method sets the instrument id of order.
     * @param pInstrumentId The instrument id of order.
     * @type String.
     */
	public void setMvInstrumentId(String pInstrumentId) {
		mvInstrumentId = pInstrumentId;
	}
	
	/**
     * This method returns the instrument id of order.
     * @return the instrument id of order.
     * @type String.
     */
	public String getMvInstrumentIdValue() {
		return mvInstrumentIdValue;
	}
	
	/**
     * This method sets the instrument id of order.
     * @param pInstrumentIdValue The instrument id of order.
     * @type String.
     */
	public void setMvInstrumentIdValue(String pInstrumentIdValue) {
		mvInstrumentIdValue = pInstrumentIdValue;
	}
	
	/**
     * This method returns the market id of order.
     * @return the market id of order.
     * @type String.
     */
	public String getMvMarketId() {
		return mvMarketId;
	}
	
	/**
     * This method sets the market id of order.
     * @param pMarketId The market id of order.
     * @type String.
     */
	public void setMvMarketId(String pMarketId) {
		mvMarketId = pMarketId;
	}
	
	/**
     * This method returns the client id.
     * @return the client id of order.
     * @type String.
     */
	public String getMvClientId() {
		return mvClientId;
	}
	
	/**
     * This method sets the client id.
     * @param pClientId The client id of order.
     * @type String.
     */
	public void setMvClientId(String pClientId) {
		mvClientId = pClientId;
	}
	
	/**
     * This method returns the date time of order.
     * @return the date time id of order.
     * @type String.
     */
	public String getMvDateTime() {
		return mvDateTime;
	}
	
	/**
     * This method sets the date time of order.
     * @param pDateTime The date time of order.
     * @type String.
     */
	public void setMvDateTime(String pDateTime) {
		mvDateTime = pDateTime;
	}
	
	/**
     * This method returns the instrument name of order.
     * @return the instrument name of order.
     * @type String.
     */
	public String getMvInstrumentName() {
		return mvInstrumentName;
	}
	
	/**
     * This method sets the instrument name of order.
     * @param pInstrumentName The instrument name of order.
     * @type String.
     */
	public void setMvInstrumentName(String pInstrumentName) {
		mvInstrumentName = pInstrumentName;
	}
	
	/**
     * This method returns the price of order.
     * @return the price of order is formatted.
     * @type String.
     */
	public String getMvPrice() {
		return mvPrice;
	}
	
	/**
     * This method sets the price of order.
     * @param pPrice The price of order is formatted.
     * @type String.
     */
	public void setMvPrice(String pPrice) {
		mvPrice = pPrice;
	}
	
	/**
     * This method returns the price of order.
     * @return the price of order.
     * @type String.
     */
	public String getMvPriceValue() {
		return mvPriceValue;
	}
	
	/**
     * This method sets the price of order.
     * @param pPriceValue The price of order.
     * @type String.
     */
	public void setMvPriceValue(String pPriceValue) {
		mvPriceValue = pPriceValue;
	}
	
	/**
     * This method returns the currency id of instrument.
     * @return currency id of instrument.
     * @type String.
     */
	public String getMvCurrencyId() {
		return mvCurrencyId;
	}
	
	/**
     * This method sets the currency id of instrument.
     * @param pCurrencyId The currency id of instrument.
     * @type String.
     */
	public void setMvCurrencyId(String pCurrencyId) {
		mvCurrencyId = pCurrencyId;
	}
	
	/**
     * This method returns the quantity of order.
     * @return the quantity of order is formatted.
     * @type String.
     */
	public String getMvQuantity() {
		return mvQuantity;
	}
	
	/**
     * This method sets the quantity of order.
     * @param pQuantity The quantity of order is formatted.
     * @type String.
     */
	public void setMvQuantity(String pQuantity) {
		mvQuantity = pQuantity;
	}
	
	/**
     * This method returns the quantity of order.
     * @return the quantity of order.
     * @type String.
     */
	public String getMvQuantityValue() {
		return mvQuantityValue;
	}
	
	/**
     * This method sets the quantity of order.
     * @param pQuantityValue The quantity of order.
     * @type String.
     */
	public void setMvQuantityValue(String pQuantityValue) {
		mvQuantityValue = pQuantityValue;
	}
	
	/**
     * This method returns the type of order.
     * @return the type of order.
     * @type String.
     */
	public String getMvOrderType() {
		return mvOrderType;
	}
	
	/**
     * This method sets the type of order.
     * @param pOrderType The type of order is formatted.
     * @type String.
     */
	public void setMvOrderType(String pOrderType) {
		mvOrderType = pOrderType;
	}
	
	/**
     * This method returns the type of order.
     * @return the type of order is formatted.
     * @type String.
     */
	public String getMvOrderTypeValue() {
		return mvOrderTypeValue;
	}
	
	/**
     * This method sets the type of order.
     * @param pOrderTypeValue The type of order is formatted.
     * @type String.
     */
	public void setMvOrderTypeValue(String pOrderTypeValue) {
		mvOrderTypeValue = pOrderTypeValue;
	}
	
	/**
     * This method returns the stop type of order.
     * @return the stop type of order.
     * @type String.
     */
	public String getMvStopTypeValue() {
		return mvStopTypeValue;
	}
	
	/**
     * This method sets the stop type of order.
     * @param pStopTypeValue The stop type of order.
     * @type String.
     */
	public void setMvStopTypeValue(String pStopTypeValue) {
		mvStopTypeValue = pStopTypeValue;
	}
	
	/**
     * This method returns the stop price of order.
     * @return the stop price of order is formatted with currency id.
     * @type String.
     */
	public String getMvFormAttedStopPriceValue() {
		return mvFormAttedStopPriceValue;
	}
	
	/**
     * This method sets the stop price of order.
     * @param pFormAttedStopPriceValue The stop price of order is formatted with currency id.
     * @type String.
     */
	public void setMvFormAttedStopPriceValue(String pFormAttedStopPriceValue) {
		mvFormAttedStopPriceValue = pFormAttedStopPriceValue;
	}
	
	/**
     * This method returns the original net amount of order.
     * @return the original net amount of order is formatted.
     * @type String.
     */
	public String getMvORiginalNetAmount() {
		return mvORiginalNetAmount;
	}
	
	/**
     * This method sets the original net amount of order.
     * @param pORiginalNetAmount The original net amount of order is formatted.
     * @type String.
     */
	public void setMvORiginalNetAmount(String pORiginalNetAmount) {
		mvORiginalNetAmount = pORiginalNetAmount;
	}
	
	/**
     * This method returns the good till date of order.
     * @return the good till date of order is formatted.
     * @type String.
     */
	public String getMvGoodTillDate() {
		return mvGoodTillDate;
	}
	
	/**
     * This method sets the good till date of order.
     * @param pGoodTillDate The good till date of order is formatted.
     * @type String.
     */
	public void setMvGoodTillDate(String pGoodTillDate) {
		mvGoodTillDate = pGoodTillDate;
	}
	
	/**
     * This method returns the good till date of order.
     * @return the good till date of order.
     * @type String.
     */
	public String getMvGoodTillDateValue() {
		return mvGoodTillDateValue;
	}
	
	/**
     * This method sets the good till date of order.
     * @param pGoodTillDateValue The good till date of order.
     * @type String.
     */
	public void setMvGoodTillDateValue(String pGoodTillDateValue) {
		mvGoodTillDateValue = pGoodTillDateValue;
	}
	
	/**
     * This method returns the gross amount of order.
     * @return the gross amount of order is formatted.
     * @type String.
     */
	public String getMvGrossAMT() {
		return mvGrossAMT;
	}
	
	/**
     * This method sets the gross amount of order.
     * @param pGrossAMT The gross amount of order is formatted.
     * @type String.
     */
	public void setMvGrossAMT(String pGrossAMT) {
		mvGrossAMT = pGrossAMT;
	}
	
	/**
     * This method returns the fees commission of order.
     * @return the fees commission of order is formatted.
     * @type String.
     */
	public String getMvFeesCommission() {
		return mvFeesCommission;
	}
	
	/**
     * This method sets the fees commission of order.
     * @param pFeesCommission The fees commission of order is formatted.
     * @type String.
     */
	public void setMvFeesCommission(String pFeesCommission) {
		mvFeesCommission = pFeesCommission;
	}
	
}
