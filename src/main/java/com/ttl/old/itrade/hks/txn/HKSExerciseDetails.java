package com.ttl.old.itrade.hks.txn;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */
/**
 * The HKSExerciseDetails class definition for all methodsis
 * Details of the market Exercise entities
 * 
 * @author not attributable
 */
public class HKSExerciseDetails
{
    private String mvEntitlementID;
    private String mvInstrumentID;
    private String mvInstrumentName;
    private String mvBookCloseInstrumentID;
    private String mvInstrumentShortName;
    private String mvInstrumentChineseName;
    private String mvInstrumentChineseShortName;
    private String mvStatus;
    private String mvCurrencyID;
    private String mvProductID;
    private String mvMarketID;
    private String mvType;
    private String mvTypeDescription;

    private String mvClientID;
    private String mvTradingAccSeq;
    private String mvAccountSeq;
    private String mvInputDate;

    private String mvExcessQty;
    private String mvExerciseQty;
    private String mvExercisableQty;
    private String mvSubScriptionID;



    private String mvNumberOfSharesHeld;
    private String mvNumOfSharesHeldBalanceDate;
    private String mvNameofRightsIssue;
    private String mvNumOfSharesAvailableOfSubscription;

    private String mvNumOfSharesAvailableForAcquisition;
    private String mvProposedAcquistionPrice;

    private String mvStatusDescription;
    private String mvOffercurrencyID;
    private String mvExerciseRatioDelivery;
    private String mvExerciseRatioPer;
    private String mvExIssueInstrumentID;

    private String mvLocationID;



    private String mvAllowExcessExerciseFlag;


    private String mvIssuePrice;

    private String mvClosingDate;


    /**
     * Get method for instrument chinese short name
     * @return instrument chinese short name
     */
    public String getInstrumentChineseShortName()
    {
            return mvInstrumentChineseShortName;
    }
    /**
     * Get method for currency id
     * @return currency id
     */
    public String getCurrencyID()
    {
            return mvCurrencyID;
    }
    /**
     * Get method for trading account sequence
     * @return trading account sequence
     */
    public String getTradingAccSeq()
    {
            return mvTradingAccSeq;
    }
    /**
     * Get method for the market id 
     * @return the market id 
     */
    public String getMarketID()
    {
            return mvMarketID;
    }
    /**
     * Get method for exercise status 
     * @return exercise status 
     */
    public String getStatus()
    {
            return mvStatus;
    }
    /**
     * Get method for exercise type
     * @return exercise type
     */
    public String getType()
    {
            return mvType;
    }
    /**
     * set method for exercise type
     * @param pType exercise type
     */
    public void setType(String pType)
    {
            mvType = pType;
    }
    /**
     * Get method for exercise type description 
     * @return exercise type description 
     */
    public String getTypeDescription()
    {
            return mvTypeDescription;
    }
    /**
     * Set method for exercise type description 
     * @param pTypeDescription the exercise type description 
     */
    public void setTypeDescription(String pTypeDescription)
    {
            mvTypeDescription = pTypeDescription;
    }
    /**
     * Get method for excess quantity 
     * @return excess quantity 
     */
    public String getExcessQty()
    {
            return mvExcessQty;
    }
    /**
     * Set method for excess quantity 
     * @param pExcessQty the excess quantity 
     */
    public void setExcessQty(String pExcessQty)
    {
            mvExcessQty = pExcessQty;
    }

    /**
     * Get method for exercisable quantity
     * @return exercisable quantity
     */
    public String getExercisableQty()
    {
            return mvExercisableQty;
    }
    /**
     * Set method for exercisable quantity
     * @param pExercisableQty the exercisable quantity
     */
    public void setExercisableQty(String pExercisableQty)
    {
            mvExercisableQty = pExercisableQty;
    }
    /**
     * Get method for subscription id
     * @return the subscription id
     */
    public String getSubScriptionID()
    {
            return mvSubScriptionID;
    }
    /**
     * Set method for subscription id
     * @param pSubScriptionID the subscription id
     */
    public void setSubscriptionID(String pSubScriptionID)
    {

            mvSubScriptionID = pSubScriptionID;
    }


    /**
     * Get method for instrument name
     * @return instrument name
     */
    public String getInstrumentName()
    {
            return mvInstrumentName;
    }

//    public String getReverseStatus()
//    {
//        return mvReverseStatus;
//    }
    /**
     * Get method for entitlement id
     * @return entitlement id
     */
    public String getEntitlementID()
    {
            return mvEntitlementID;
    }
    /**
     * Get method for instrument short name
     * @return instrument short name
     */
    public String getInstrumentShortName()
    {
            return mvInstrumentShortName;
    }
    /**
     * set method for account sequence
     * @return account sequence
     */
    public String getAccountSeq()
    {
            return mvAccountSeq;
    }
    /**
     * Get method for instrument chinese name
     * @return instrument chinese name
     */
    public String getInstrumentChineseName()
    {
            return mvInstrumentChineseName;
    }
    /**
     * Get method for Input Date market programs
     * @return Input Date market programs
     */
    public String getInputDate()
    {
            return mvInputDate;
    }
    /**
     * Get method for instrument id
     * @return instrument id
     */
    public String getInstrumentID()
    {
            return mvInstrumentID;
    }
    /**
     * Get method for client id 
     * @return client id 
     */
    public String getClientID()
    {
            return mvClientID;
    }
    /**
     * Get method for product id
     * @return product id
     */
    public String getProductID()
    {
            return mvProductID;
    }
    /**
     * Get method for the exercise quantity
     * @return the exercise quantity
     */
    public String getExerciseQty()
    {
            return
                    mvExerciseQty;
    }
    /**
     * Get method for number of shares held
     * @return number of shares held
     */
    public String getNumberOfSharesHeld()
    {
            return
                    mvNumberOfSharesHeld;
    }
    /**
     * Get method for number of shares held balance date
     * @return number of shares held balance date
     */
    public String getNumOfSharesHeldBalanceDate()
    {
            return
                    mvNumOfSharesHeldBalanceDate;
    }
    /**
     * Get method for name of rights issue
     * @return name of rights issue
     */
    public String getNameofRightsIssue()
    {
            return   mvNameofRightsIssue;
    }
    /**
     * Set method for name of rights issue
     * @param pNameofRightsIssue the name of rights issue
     */
    public void setNameofRightsIssue(String pNameofRightsIssue)
    {
            mvNameofRightsIssue = pNameofRightsIssue;
    }

    /**
     * Get method for number of shares available of subscription
     * @return number of shares available of subscription
     */
    public String getNumOfSharesAvailableOfSubscription()
    {
            return mvNumOfSharesAvailableOfSubscription;
    }
    /**
     * Set method for number of shares available of subscription
     * @param pNumOfSharesAvailableOfSubscription the number of shares available of subscription
     */
    public void setNumOfSharesAvailableOfSubscription(String pNumOfSharesAvailableOfSubscription)
    {
            mvNumOfSharesAvailableOfSubscription = pNumOfSharesAvailableOfSubscription;
    }
    /**
     * Get method for number of shares availabel for acquisition
     * @return number of shares availabel for acquisition
     */
    public String getNumOfSharesAvailableForAcquisition()
    {
            return mvNumOfSharesAvailableForAcquisition;
    }
    /**
     * Set method for number of shares availabel for acquisition
     * @param pNumOfSharesAvailableForAcquisition the shares availabel for acquisition
     */
    public void setNumOfSharesAvailableForAcquisition(String pNumOfSharesAvailableForAcquisition)
    {

            mvNumOfSharesAvailableForAcquisition = pNumOfSharesAvailableForAcquisition;
    }
    /**
     * Get method for proposed acquistion price
     * @return proposed acquistion price
     */
    public String getProposedAcquistionPrice()
    {
            return mvProposedAcquistionPrice;
    }
    /**
     * Set method for proposed acquistion price
     * @param pProposedAcquistionPrice the proposed acquistion price
     */
    public void setProposedAcquistionPrice(String pProposedAcquistionPrice)
    {
            mvProposedAcquistionPrice = pProposedAcquistionPrice;
    }

    /**
     * Get method for issue price 
     * @return issue price 
     */
    public String getIssuePrice()
    {
            return mvIssuePrice;
    }
    /**
     * set method for Issue price
     * @param pIssuePrice the issue price
     */
    public void setIssuePrice(String pIssuePrice)
    {
            mvIssuePrice = pIssuePrice;
    }
    /**
     * Get method for book closing date
     * @return book closing date
     */
    public String getClosingDate()
    {
            return mvClosingDate;
    }

    /**
     * Get method for status description
     * @return status description
     */
    public String getStatusDescription()
    {
            return mvStatusDescription;
    }
    /**
     * Get method for offer currency id
     * @return offer currency id
     */
    public String getOffercurrencyID()
    {
            return mvOffercurrencyID;
    }
    /**
     * Get method for exercise ratio delivery
     * @return exercise ratio delivery
     */
    public String getExerciseRatioDelivery()
    {
            return mvExerciseRatioDelivery;
    }
    /**
     * Get method for the exercise ration per
     * @return the exercise ration per
     */
    public String getExerciseRatioPer()
    {
            return mvExerciseRatioPer;
    }
    /**
     * Get method for the exercise issues instrument id
     * @return the exercise issues instrument id
     */
    public String getExIssueInstrumentID()
    {
            return mvExIssueInstrumentID;
    }

    /**
     * Get method for the Location id
     * @return the location id
     */
    public String getLocationID()
    {
            return mvLocationID;
    }

    /**
     * Get mehtod for the book close instruments id
     * @return the book close instruments id
     */
   public String getBookCloseInstrumentID()
   {
           return mvBookCloseInstrumentID;
   }
   /**
    * Get method for the allow excess exercise falg
    * @return the allow excess exercise falg
    */
   public String getAllowExcessExerciseFlag()
   {
           return mvAllowExcessExerciseFlag;
   }


   /**
    * Set method for the Instrument Chinese Short Name
    * @param pInstrumentChineseShortName the Instrument Chinese Short Name
    */
    public void setInstrumentChineseShortName(String pInstrumentChineseShortName)
    {
            mvInstrumentChineseShortName = pInstrumentChineseShortName;
    }
    /**
     * Set method for the Currency ID
     * @param pCurrencyID the Currency ID
     */
    public void setCurrencyID(String pCurrencyID)
    {
            mvCurrencyID = pCurrencyID;
    }
    /**
     * Set method for the Trading Account sequence
     * @param pTradingAccSeq the Trading Account sequence
     */
    public void setTradingAccSeq(String pTradingAccSeq)
    {
            mvTradingAccSeq = pTradingAccSeq;
    }
    /**
     * Set method fort the Market Id   
     * @param pMarketID the Market Id   
     */
    public void setMarketID(String pMarketID)
    {
            mvMarketID = pMarketID;
    }
    /**
     * Set method for the Status 
     * @param pStatus the Status 
     */
    public void setStatus(String pStatus)
    {
            mvStatus = pStatus;
    }
    /**
     * Set method for the Instrument Name
     * @param pInstrumentName the Instrument Name
     */
    public void setInstrumentName(String pInstrumentName)
    {
            mvInstrumentName = pInstrumentName;
    }

//    public void setReverseStatus(String pReverseStatus)
//    {
//        mvReverseStatus = pReverseStatus;
//    }
    /**
     * Set method for the Entitlement ID
     * @return pEntitlementID the Entitlement ID
     */
    public void setEntitlementID(String pEntitlementID)
    {
            mvEntitlementID = pEntitlementID;
    }
    /**
     * Set method for the Instrument Short Name
     * @param pInstrumentShortName the Instrument Short Name
     */
    public void setInstrumentShortName(String pInstrumentShortName)
    {
            mvInstrumentShortName = pInstrumentShortName;
    }
    /**
     * Set mehtod for the Account sequence
     * @param pAccountSeq the Account sequence
     */
    public void setAccountSeq(String pAccountSeq)
    {
            mvAccountSeq = pAccountSeq;
    }
    /**
     * Set method for the Instrument Chinese Name
     * @param pInstrumentChineseName the Instrument Chinese Name
     */
    public void setInstrumentChineseName(String pInstrumentChineseName)
    {
            mvInstrumentChineseName = pInstrumentChineseName;
    }
    /**
     * Set Method for the Input Date market programs
     * @param pInputDate the Input Date market programs
     */
    public void setInputDate(String pInputDate)
    {
            mvInputDate = pInputDate;
    }
    /**
     * Set method for the Instrument ID
     * @param pInstrumentID the Instrument ID
     */
    public void setInstrumentID(String pInstrumentID)
    {
            mvInstrumentID = pInstrumentID;
    }
    /**
     * Set method for the Client Id
     * @param pClientID the Client Id
     */
    public void setClientID(String pClientID)
    {
            mvClientID = pClientID;
    }
    /**
     * Set method for the Product ID
     * @param pProductID the Product ID
     */
    public void setProductID(String pProductID)
    {
            mvProductID = pProductID;
    }


    /**
     * Set Method for the Exercise Quantity
     * @param pExerciseQty the Exercise Quantity
     */
    public void setExerciseQty(String pExerciseQty)
    {
            mvExerciseQty = pExerciseQty;
    }
    /**
     * Set Method for the Number Of Shares Held
     * @param pNumberOfSharesHeld the Number Of Shares Held
     */
    public void setNumberOfSharesHeld(String pNumberOfSharesHeld)
    {
            mvNumberOfSharesHeld = pNumberOfSharesHeld;
    }
    /**
     * Set Method for the Number OfShares Held Balance Date
     * @param pNumOfSharesHeldBalanceDate the Number OfShares Held Balance Date
     */
    public void setNumOfSharesHeldBalanceDate(String pNumOfSharesHeldBalanceDate)
    {
            mvNumOfSharesHeldBalanceDate = pNumOfSharesHeldBalanceDate;
    }
    /**
     * Set Method for the Status Description
     * @param pStatusDescription the Status Description
     */
    public void setStatusDescription(String pStatusDescription)
    {
            mvStatusDescription = pStatusDescription;
    }
    /** 
     * Set Method for the Offer currency ID
     * @param pOffercurrencyID the Offer currency ID
     */
    public void setOffercurrencyID(String pOffercurrencyID)
    {
            mvOffercurrencyID = pOffercurrencyID;
    }
    /**
     * Set Method for the Exercise Ratio Delivery
     * @param pExerciseRatioDelivery the Exercise Ratio Delivery
     */
    public void setExerciseRatioDelivery(String pExerciseRatioDelivery)
    {
            mvExerciseRatioDelivery = pExerciseRatioDelivery;
    }
    /**
     * Set Method for the Exercise Ratio Per
     * @param pExerciseRatioPer the Exercise Ratio Per
     */
    public void setExerciseRatioPer(String pExerciseRatioPer)
    {
            mvExerciseRatioPer = pExerciseRatioPer;
    }
    /**
     * Set Method for Exercise Issue Instrument ID
     * @param pExIssueInstrumentID the Exercise Issue Instrument ID
     */
    public void setExIssueInstrumentID(String pExIssueInstrumentID)
    {
            mvExIssueInstrumentID = pExIssueInstrumentID;
    }

   /**
    * Set Method for the Location ID
    * @param pLocationID the Location ID
    */
   public void setLocationID(String pLocationID)
   {
           mvLocationID = pLocationID;
   }




   /**
    * Set Method for Book Closing Date 
    * @param pClosingDate the Book Closing Date 
    */
   public void setClosingDate(String pClosingDate)
   {
           mvClosingDate = pClosingDate;
   }
   /**
    * Set Method for the Book Close Instrument ID
    * @param pBookCloseInstrumentID the Book Close Instrument ID
    */
   public void setBookCloseInstrumentID(String pBookCloseInstrumentID)
   {
           mvBookCloseInstrumentID = pBookCloseInstrumentID;
   }
   /**
    * Set Method for The Allow Excess Exercise Flag
    * @param pAllowExcessExerciseFlag The Allow Excess Exercise Flag
    */
   public void setAllowExcessExerciseFlag(String pAllowExcessExerciseFlag)
   {
           mvAllowExcessExerciseFlag = pAllowExcessExerciseFlag;
   }


}
