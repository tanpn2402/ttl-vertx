package com.ttl.old.itrade.util;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

/**
 * The HtmlTemplateCache class defined methods that get HtmlTemplate object with different way.
 * @author
 *
 */
public class HtmlTemplateCache extends Object
{

	private static HtmlTemplateCache	_templateCache = null;
	private Hashtable					_hLoadedFiles;
	private int							_iMaxFiles = 0;

	/**
	 * Constructor for HtmlTemplateCache class with not parameters.
	 */
	private HtmlTemplateCache()
	{
		_hLoadedFiles = new Hashtable();
	}

	/**
	 * Constructor for HtmlTemplateCache class with max file.
	 * @param pMaxFiles The max file.
	 */
	private HtmlTemplateCache(int pMaxFiles)
	{
		_iMaxFiles = pMaxFiles;
		_hLoadedFiles = new Hashtable();
	}

	/**
	 * This method get instance of html template cache with not parameters.
	 * @return The instance of html template cache.
	 */
	public synchronized static HtmlTemplateCache getInstance()
	{
      Log.println("[HtmlTemplateCache: _templateCache == null = " + (_templateCache == null) + "]", Log.ERROR_LOG);
		if (_templateCache == null)
		{
			_templateCache = new HtmlTemplateCache();
		}
		return _templateCache;
	}

	/**
	 * This method get instance of html template cache with max file.
	 * @param pMaxFiles The max file.
	 * @return The instance of html template cache.
	 */
	public synchronized static HtmlTemplateCache getInstance(int pMaxFiles)
	{
		if (_templateCache == null)
		{
			_templateCache = new HtmlTemplateCache(pMaxFiles);
			return _templateCache;
		}
		return null;
	}

	/**
	 * This method get html template by file name and servlet.
	 * @param pFilename The file name.
	 * @param pRequest Object that contains the request the client made of the servlet.
	 * @return Object of  HtmlTemplate.
	 */
	public static HtmlTemplate getFile(String pFilename, HttpServletRequest pRequest)
	{

		if (_templateCache == null)
		{
			return null;

			// synchronized (_templateCache._hLoadedFiles) {

			// - Check to see if file already exists
			// if (_templateCache._hLoadedFiles.containsKey(sFilename)) {
		}
		HtmlFileCacheHolder cacheHolder = (HtmlFileCacheHolder) _templateCache._hLoadedFiles.get(pFilename);

		if (cacheHolder != null)
		{

			if (cacheHolder.creation == new File(cacheHolder.file.nameOfFile).lastModified())
			{
				return cacheHolder.file;
			}
			else
			{
				_templateCache._hLoadedFiles.remove(pFilename);
			}
		}

		// }

		// - Need to load the file from disk
		cacheHolder = _templateCache.new HtmlFileCacheHolder();

      Log.println("[HtmlTemplateCache: filename = " + pFilename + "]", Log.ERROR_LOG);
		try
		{
			cacheHolder.file = new HtmlTemplate(pFilename);
		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
			return null;
		}
		cacheHolder.age = System.currentTimeMillis();
		cacheHolder.creation = new File(cacheHolder.file.nameOfFile).lastModified();

		// - Check to see the maximum number of files havent been reached
		if (_templateCache._hLoadedFiles.size() > _templateCache._iMaxFiles && _templateCache._iMaxFiles != 0)
		{
			_templateCache.deleteFile();

		}
		_templateCache._hLoadedFiles.put(pFilename, cacheHolder);
		return cacheHolder.file;

		// }
	}

	/**
	 * This method get html template by file name.
	 * @param pFilename The file name.
	 * @return Object of HtmlTemplate.
	 */
	public static HtmlTemplate getFile2(String pFilename)
	{

		if (_templateCache == null)
		{
			return null;

			// synchronized (_templateCache._hLoadedFiles) {

			// - Check to see if file already exists
			// if (_templateCache._hLoadedFiles.containsKey(sFilename)) {
		}
		HtmlFileCacheHolder cacheHolder = (HtmlFileCacheHolder) _templateCache._hLoadedFiles.get(pFilename);

		if (cacheHolder != null)
		{

			if (cacheHolder.creation == new File(cacheHolder.file.nameOfFile).lastModified())
			{
				return cacheHolder.file;
			}
			else
			{
				_templateCache._hLoadedFiles.remove(pFilename);
			}
		}

		// }

		// - Need to load the file from disk
		cacheHolder = _templateCache.new HtmlFileCacheHolder();

      Log.println("[HtmlTemplateCache: filename = " + pFilename + "]", Log.ERROR_LOG);
		try
		{
			cacheHolder.file = new HtmlTemplate(pFilename);
		}
		catch (IOException e)
		{
			Log.println(e, Log.ERROR_LOG);
			return null;
		}
		cacheHolder.age = System.currentTimeMillis();
		cacheHolder.creation = new File(cacheHolder.file.nameOfFile).lastModified();

		// - Check to see the maximum number of files havent been reached
		if (_templateCache._hLoadedFiles.size() > _templateCache._iMaxFiles && _templateCache._iMaxFiles != 0)
		{
			_templateCache.deleteFile();

		}
		_templateCache._hLoadedFiles.put(pFilename, cacheHolder);
		return cacheHolder.file;

		// }
	}

	/**
	 * This method to delete the file data.
	 */
	private void deleteFile()
	{

		// - Runs through the list and removes the oldest file
		synchronized (_hLoadedFiles)
		{
			Enumeration			E = _hLoadedFiles.keys();
			HtmlFileCacheHolder cacheHolder;
			String				sKey, sToGoKey = null;
			long				lMin = System.currentTimeMillis();

			while (E.hasMoreElements())
			{
				sKey = (String) E.nextElement();
				cacheHolder = (HtmlFileCacheHolder) _hLoadedFiles.get(sKey);
				if (cacheHolder.age <= lMin)
				{
					sToGoKey = sKey;
					lMin = cacheHolder.age;
				}
			}

			if (sToGoKey != null)
			{
				_hLoadedFiles.remove(sToGoKey);
			}
		}
	}

	/**
	 * This method to delete template cache.
	 */
	public static void shutdown()
	{
		_templateCache = null;
	}

	/**
	 * The HtmlFileCacheHolder class defined field for html file cache holder.
	 * @author 
	 */
	class HtmlFileCacheHolder extends Object
	{
		public long			age;
		public long			creation;
		public HtmlTemplate file;
	}
}

