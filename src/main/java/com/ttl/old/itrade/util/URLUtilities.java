package com.ttl.old.itrade.util;

// Author  : Wilfred
// Date    : Jul 24, 2000.

import java.util.Hashtable;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.net.URLEncoder;

/**
 * This class has many utilities for URL stuffs.
 * @author Wilfred
 * @since Jul 24, 2000.
 */
public class URLUtilities
{

	/**
	 * This method will encode parameters.
	 * @param pParameters map data with need encode.
	 * @param pPrefix the prefix of URL.
	 * @return Has been encoded string.
	 */
	public static String encodeParameters(Hashtable pParameters, String pPrefix)
	{
		Enumeration		lvEnum = pParameters.keys();
		StringBuffer	sb = new StringBuffer("");
		String			key, value;

		while (lvEnum.hasMoreElements())
		{
			key = (String) lvEnum.nextElement();
			value = (String) pParameters.get(key);

			if (pPrefix == null)
			{
				sb.append(key).append("=").append(URLEncoder.encode(value));
			}
			else
			{
				sb.append(pPrefix).append(key).append("=").append(value);
			}

			if (lvEnum.hasMoreElements())
			{
				sb.append("&");
			}
		}

		return URLEncoder.encode(sb.toString());
	}

	/**
	 * This method will parse the input string, which should has the pattern of
	 * "xxx=y&www=u". So the return values in a Hashtable format where the key are
	 * xxx and www and their values are y and u respectively.
	 * @param pString The string to be parse.
	 * @return A object of Hashtable with correct format.
	 */
	public static Hashtable parse(String pString)
	{
		StringTokenizer st1 = new StringTokenizer(pString, "&");
		Hashtable		h = new Hashtable();
		String			temp;

		while (st1.hasMoreTokens())
		{
			temp = st1.nextToken();

			int psn = temp.indexOf("=");

			// if there is no "=" or "=" is at the end
			if (psn == -1 || psn == temp.length() - 1)
			{
				h.put(temp, "");
			}
			else
			{
				h.put(temp.substring(0, psn), temp.substring(psn + 1));
			}
		}

		return h;
	}

}
