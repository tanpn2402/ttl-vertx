package com.ttl.old.itrade.util;

import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

/**
 * The BundleLoader class defined methods that use language and charset to bundle loader. 
 * @author
 *
 */
public class BundleLoader
{
	private static String   _sSupportLang;
	private static boolean  _bIsOn = false;

	/**
	 * Sets the support language.
	 * @param pSupportLang The support language.
	 */
	public static void setSupportLang(String pSupportLang)
	{
		_sSupportLang = pSupportLang;
	}

	/**
	 * Sets the _bIsOn.
	 * @param pIsOn The _bIsOn.
	 */
	public static void setLoaderOn(boolean pIsOn)
	{
		_bIsOn = pIsOn;
	}

	/**
	 * This method to init the bundle.
	 * @param pBundleName The bundle name.
	 * @return The result whether init bundle is success.
	 */
	public static boolean initBundle(String pBundleName)
	{
      Log.println("[BundleLoader.initBundle():]", Log.ERROR_LOG);

		StringTokenizer semiColonST = new StringTokenizer(_sSupportLang, ";");
		StringTokenizer colonST;
		String			sLocale;
		String			sLang;
		ResourceBundle  bundle;
		String			sCharset;

		if (_bIsOn)
		{
         Log.println("[BundleLoader.initBundle(): sBundleName = " + pBundleName + "]", Log.DEBUG_LOG);
			while (semiColonST.hasMoreTokens())
			{
				sLocale = semiColonST.nextToken();
				colonST = new StringTokenizer(sLocale, ":");
				sLang = colonST.nextToken();
				sCharset = colonST.nextToken();
            Log.println("[BundleLoader.initBundle(): sLang = " + sLang + "]", Log.DEBUG_LOG);
            Log.println("[BundleLoader.initBundle(): sCharset = " + sCharset + "]", Log.DEBUG_LOG);

				LocaleNegotiator	negotiator = new LocaleNegotiator(pBundleName, sLang, sCharset);
				if (negotiator != null)
				{
					bundle = negotiator.getBundle();
					if (bundle != null)
					{
						Enumeration eKeys = bundle.getKeys();
						while (eKeys.hasMoreElements())
						{
							String  sKey = (String) eKeys.nextElement();
							String  sValue = bundle.getString(sKey);
                     Log.println("[BundleLoader.initBundle(): " + sKey + " = " + sValue + "]", Log.ERROR_LOG);
						}
					}
					else
					{
                  Log.println("[BundleLoader.initBundle(): bundle == null]", Log.ERROR_LOG);
					}
				}
				else
				{
               Log.println("[BundleLoader.initBundle(): negotiator == null]", Log.ERROR_LOG);
					return false;
				}
			}
		}
		return true;
	}
}
