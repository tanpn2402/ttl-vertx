package com.ttl.old.itrade.util;

/**
 * This class will store a information of an order that are reducing.
 */
public class ReduceOrderInfo
{
	private String  reduceQuantity = "";
	private String  originalOrderId = "";

	/**
	 * Constructor for ReduceOrderInfo class.
	 * @param pReduceQuantity the stock reduce quantity.
	 * @param pOriginalOrderId the original order ID.
	 */
	public ReduceOrderInfo(String pReduceQuantity, String pOriginalOrderId)
	{
		this.reduceQuantity = pReduceQuantity;
		this.originalOrderId = pOriginalOrderId;
	}

	/**
	 * Gets the stock reduce quantity.
	 * @return The stock reduce quantity.
	 */
	public String getReduceQuantity()
	{
		return this.reduceQuantity;
	}

	/**
	 * Gets the original order ID. 
	 * @return The original order ID.
	 */
	public String getOriginalOrderId()
	{
		return this.originalOrderId;
	}

	/**
	 * Compare this string with the specified object.
	 * @param pObject the object to be comparison.
	 * @return If the object are equal, then return true; otherwise returns false.
	 */
	public boolean equals(Object pObject)
	{
		if (pObject == null ||!(pObject instanceof ReduceOrderInfo))
		{
			return false;

		}
		ReduceOrderInfo orderInfo = (ReduceOrderInfo) pObject;
		if (this.reduceQuantity.equals(orderInfo.getReduceQuantity()) && this.originalOrderId.equals(orderInfo.getOriginalOrderId()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
