package com.ttl.old.itrade.util;

/**
 * The DefaultHandler abstract class defined methods that it to do some base handler.
 * @author
 *
 */
public abstract class DefaultHandler
{
	protected String	_sServerPath;

	/**
	 * Constructor for DefaultHandler class.
	 */
	public DefaultHandler() {}

	/**
	 * Sets the server path.
	 * @param pServerPath The server path.
	 */
	public void setServerPath(String pServerPath)
	{
		_sServerPath = pServerPath;
	}

	/**
	 * This method to prepare URL.
	 * @return The required URL.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	protected String prepareURL() throws Exception
	{
		return null;
	}

	/**
	 * This method to do transaction.
	 * @return The result of transaction for winvest.
	 */
	public WinvestResult doTransaction()
	{
		return null;
	}
}
