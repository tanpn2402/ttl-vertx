package com.ttl.old.itrade.util;

public class VertxMessagetxn {
	private String mvMessage;
	private int mvKey;
	private String mvResult;
	public VertxMessagetxn(String pvMessage, int pvKey, String pvObject) {
		this.mvMessage = pvMessage;
		this.mvKey = pvKey;
		this.mvResult = pvObject;
	}
	public String getMvMessage() {
		return mvMessage;
	}
	public void setMvMessage(String mvMessage) {
		this.mvMessage = mvMessage;
	}
	public int getMvKey() {
		return mvKey;
	}
	public void setMvKey(int mvKey) {
		this.mvKey = mvKey;
	}
	public String getMvResult() {
		return mvResult;
	}
	public void setMvObject(String mvObject) {
		this.mvResult = mvObject;
	}
	
	@Override
	public String toString() {
		 final StringBuilder sb = new StringBuilder("CustomMessage{");
		    sb.append("mvMessage=").append(mvMessage);
		    sb.append(", mvKey='").append(mvKey).append('\'');
		    sb.append(", mvResult='").append(mvResult).append('\'');
		    sb.append('}');
		return "";
	}

}
