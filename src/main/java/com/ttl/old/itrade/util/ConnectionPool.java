package com.ttl.old.itrade.util;

// Modified by Wilfred
// Date  : Jul 7, 2000.
// Modification  : change this class to normal class instead of a Thread

// Modified by Wilfred
// Date  : Sep 5, 2000.
// Modification  : Using 2 hashtable to maintain available and unavailable
// connection and this class will create connection when there is no enough
// connection

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

/**
 * The ConnectionPool class defined methods to  operation the Connection about Connection Pool.
 * @author Wilfred
 * @since Jul 7, 2000.
 */
public class ConnectionPool
{

	private String  _sFilename;
	private String  _sDbDriver, _sDbName, _sDbUser, _sDbPassword;
	private int		_iConNum;
	private boolean _bIsReady = false;
	private boolean recreate = false;
	private Thread  keeper = null;

	// Added by Wilfred start Sep 5, 2000.
	// the following 2 hashtables maintain the available and unavailable connections
	// where the key of the hashtable is the Connection and the value is a Long
	// object which represent the time when the connection was put into the list
	Hashtable		availableCon = new Hashtable();
	Hashtable		unAvailableCon = new Hashtable();
	int				numOfRetry = 2;

	// Added by Wilfred end Sep 5, 2000.

	/**
	 * Constructor for ConnectionPool class.
	 * @param pFilename The name of specified file.
	 */
	public ConnectionPool(String pFilename)
	{
		_sFilename = pFilename;
	}

	/**
	 * This method to open necessary Connection and close free Connection. 
	 * @return The result which  operation Connection is normal or not.
	 */
	public boolean init()
	{
		_bIsReady = false;
		Properties  pIni = new Properties();
		try
		{
			pIni.load(new FileInputStream(_sFilename));
			_sDbDriver = pIni.getProperty("driver");
			_sDbName = pIni.getProperty("database");
			_sDbUser = pIni.getProperty("username");
			_sDbPassword = pIni.getProperty("password");
			_iConNum = Integer.parseInt(pIni.getProperty("connections"));
			numOfRetry = Integer.parseInt(pIni.getProperty("connection.retry"));

			// check the recreate flag if true that means recreate new connection if
			// there isn't any connection left
			String  tmp = pIni.getProperty("connection.recreate");
			if (tmp != null && tmp.equalsIgnoreCase("true"))
			{
				recreate = true;
			}
			else
			{
				recreate = false;
			}
		}
		catch (IOException e)
		{

			Log.println(e, Log.ERROR_LOG);
			Log.println("[ConnectionPool.ConnectionPool(): Error = " + e + "]", Log.ERROR_LOG);
			Log.println("[ConnectionPool.ConnectionPool(): Please ensure you have the following fields: ]", Log.ERROR_LOG);
			Log.println("[ConnectionPool.ConnectionPool(): driver=]", Log.ERROR_LOG);
			Log.println("[ConnectionPool.ConnectionPool(): database=]", Log.ERROR_LOG);
			Log.println("[ConnectionPool.ConnectionPool(): username=]", Log.ERROR_LOG);
			Log.println("[ConnectionPool.ConnectionPool(): password=]", Log.ERROR_LOG);
			Log.println("[ConnectionPool.ConnectionPool(): connections=]", Log.ERROR_LOG);
			Log.println("[ConnectionPool.ConnectionPool(): in a file named dbbroker.ini]", Log.ERROR_LOG);
			return false;
		}

		// -- Attempt to open the database connections
		for (int x = 0; x < _iConNum; x++)
		{
			Connection  con = openConnection();
			if (con != null)
			{
				this.addToAvailableCon(con);
			}
			else
			{
				return false;
			}
		}

		Log.println("[ConnectionPool: " + _iConNum + " connections opened]", Log.ACCESS_LOG);
		_bIsReady = true;

		keeper = new Thread(new Runnable()
		{
			public void run()
			{
				while (true)
				{

					// check to see if the total number of connections is more than the initial
					// number of connection than release those extra connection
					// keep minimum connections in the available list
					try
					{
						while (availableCon.size() > _iConNum)
						{
							Connection  con = getFreeConnection();
							closeConnection(con);
							con = null;
						}

						// sleep for 5 mins
						Thread.sleep(300000);

						// Thread.sleep(5000);
					}
					catch (Exception ex) {}
				}		// end of while
			}			// end of run method
		});

		// if this class will not create any new connection when there is not more
		// connection then we don't have to start the thread
		if (recreate)
		{
			keeper.start();

		}
		return true;
	}

	// Modified by Wilfred start Sep 5, 2000.

	/**
	 * This method will return a available connection, if thre is no available
	 * connection then create a new connection, if no more connection can be
	 * create then return null.
	 */
	public Connection pop()
	{

		// Log.println("[ ConnectionPool.pop() # of available: " + availableCon.size() + " # of unAvailable: " + unAvailableCon.size() + " ]", Log.ACCESS_LOG );

		for (int i = 0; i < numOfRetry; i++)
		{
			Connection  con = getFreeConnection();
			if (con != null)
			{
				this.addToUnAvailableCon(con);
				return con;
			}

			// no available connection then open a new connection
			else
			{
				Log.println("[ ConnectionPool.pop: No free connections, recreate: " + recreate + " ]", Log.ERROR_LOG);
				Log.println("[ ConnectionPool.pop() # of available: " + availableCon.size() + " # of unAvailable: " + unAvailableCon.size() + " ]", Log.ERROR_LOG);

				// if the recreate flag is true then create a new connection, othrewise
				// just wait
				if (recreate)
				{
					con = openConnection();
				}
				else
				{
					con = null;
				}

				if (con != null)
				{
					this.addToUnAvailableCon(con);
					return con;
				}
				else
				{
					Log.println("[ ConnectionPool.pop: No free connections and cannot create new connection ]", Log.ERROR_LOG);
					try
					{
						Thread.sleep(1000);
					}
					catch (Exception ex) {}
				}
			}
		}
		return null;
	}

	// Modified by Wilfred end Sep 5, 2000.

	// Modified by Wilfred start Sep 5, 2000.
	/**
	 * This method will close Connection.
	 */
	public void shutdown()
	{
		if (recreate)
		{
			keeper.stop();
		}
		keeper = null;

		Enumeration e = availableCon.keys();
		while (e.hasMoreElements())
		{
			Connection  con = (Connection) e.nextElement();
			closeConnection(con);
			removeFromAvailableCon(con);
		}

		e = unAvailableCon.keys();
		while (e.hasMoreElements())
		{
			Connection  con = (Connection) e.nextElement();
			closeConnection(con);
			removeFromUnAvailableCon(con);
		}

	}

	// Modified by Wilfred end Sep 5, 2000.

	/**
	 * This method will to release resources.
	 */
	protected void finalize()
	{

		// Debug.println( "[ConnectionPool.finalize():]" );
		shutdown();
	}

	/**
	 * This method to push the Connection.
	 * @param pConnection Object of Connection class.
	 */
	// Modified by Wilfred start Sep 5, 2000.
	public void push(Connection pConnection)
	{

		// Log.println("[ ConnectionPool.push() # of available: " + availableCon.size() + " # of unAvailable: " + unAvailableCon.size() + " ]", Log.ACCESS_LOG );
		if (pConnection == null)
		{
			return;

			// remove from the unavailable list
		}
		this.removeFromUnAvailableCon(pConnection);

		try
		{
			this.addToAvailableCon(pConnection);
			pConnection.commit();
			pConnection.clearWarnings();
		}
		catch (Exception e)
		{

			// if there is any error then close the connection and reopen a new one
			closeConnection(pConnection);

			if (recreate)
			{
				pConnection = openConnection();
			}
			else
			{
				pConnection = null;
			}

			if (pConnection != null)
			{
				this.addToAvailableCon(pConnection);
			}
		}
	}

	// Modified by Wilfred end Sep 5, 2000.
	/**
	 * This method will open Connection.
	 * @return The Connection obejct to be open.
	 */
		
	private Connection openConnection()
	{
		Connection  newCon = null;
		try
		{
			Class.forName(_sDbDriver).newInstance();
			newCon = DriverManager.getConnection(_sDbName, _sDbUser, _sDbPassword);
		}
		catch (Exception e)
		{
			Log.println(e, Log.ERROR_LOG);
			newCon = null;
		}
		return newCon;
	}

	/**
	 * This method will close the specified Connection
	 * @param pConnection Object of Connection class.
	 */
	private void closeConnection(Connection pConnection)
	{
		try
		{
			pConnection.close();
		}
		catch (Exception e) {}
	}

	/**
	 * This method to judge the Connection whether be closed.
	 * @param pConnection Object of Connection class.
	 * @return The result of judge Connection  is closed.
	 */
	private boolean isClosed(Connection pConnection)
	{
		try
		{
			return pConnection.isClosed();
		}
		catch (Exception e)
		{
			return true;
		}
	}


	// Modified by Wilfred start Sep 5, 2000.

	/**
	 * This method will return a available connection or null if there is no
	 * available connection
	 */
	private Connection getFreeConnection()
	{

		synchronized (availableCon)
		{
			if (availableCon.size() > 0)
			{
				Enumeration lvEnum = availableCon.keys();

				// get the first available connection, remove it from the available list
				// then add it to unavailable list
				while (lvEnum.hasMoreElements())
				{
					Connection  con = (Connection) lvEnum.nextElement();
					this.removeFromAvailableCon(con);
					return con;
				}
			}
			return null;
		}

	}

	// Modified by Wilfred end Sep 5, 2000.

	// Added by Wilfred Sep 5, 2000.
	/**
	 * This method will remove the specified Connection form available Connection.
	 * @param pConnection Object of Connection class.
	 */
	private void removeFromAvailableCon(Connection pConnection)
	{
		synchronized (availableCon)
		{
			availableCon.remove(pConnection);
		}
	}

	// Added by Wilfred Sep 5, 2000.
	/**
	 * This method will remove the specified Connection form unavailable Connection.
	 * @param pConnection Object of Connection class.
	 */
	private void removeFromUnAvailableCon(Connection pConnection)
	{
		synchronized (unAvailableCon)
		{
			unAvailableCon.remove(pConnection);
		}
	}

	// Added by Wilfred Sep 5, 2000.
	/**
	 * This method add the specified Connection to available Connection.
	 * @param pConnection Object of Connection class.
	 */
	private void addToAvailableCon(Connection pConnection)
	{
		synchronized (availableCon)
		{
			availableCon.put(pConnection, new Long(System.currentTimeMillis()));
		}
	}

	// Added by Wilfred Sep 5, 2000.
	/**
	 * This method add the specified Connection to unavailable Connection.
	 * @param pConnection Object of Connection class.
	 */
	private void addToUnAvailableCon(Connection pConnection)
	{
		synchronized (unAvailableCon)
		{
			unAvailableCon.put(pConnection, new Long(System.currentTimeMillis()));
		}
	}

	/*
	 * class dbConnection extends Object {
	 * public Connection con;
	 * public boolean    bActive;
	 * public long         timeTaken;
	 * public long         averageTime;
	 * public long         maxTime;
	 * public int          hitRate;
	 *
	 * public dbConnection( Connection _con ){
	 * con     = _con;
	 * bActive = false;
	 * timeTaken   = 0;
	 * averageTime = 0;
	 * hitRate     = -1;
	 * maxTime     = -1;
	 * }
	 *
	 * public void setInActive(){
	 * bActive = false;
	 * long t = System.currentTimeMillis() - timeTaken;
	 * if ( t < 120000 )
	 * averageTime += t;
	 *
	 * timeTaken   = 0;
	 * if ( t > maxTime )
	 * maxTime = t;
	 * }
	 *
	 * public void setActive(){
	 * bActive = true;
	 * timeTaken   = System.currentTimeMillis();
	 * hitRate++;
	 * }
	 *
	 * public long getAverage(){
	 * if ( hitRate == 0 ) return 0;
	 * return averageTime/(long)hitRate;
	 * }
	 *
	 * public String toString(){
	 * return "[Hit: " + hitRate + "] [Avg.: " + getAverage() + "] [Use: " + bActive + "] [Max: " + maxTime + "]";
	 * }
	 * }
	 */
}
