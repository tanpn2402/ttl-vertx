package com.ttl.old.itrade.util;

/**
 * The TestPool class defined methods that are testing the pool.
 * @author
 *
 */
public class TestPool extends ObjectPool
{
	private int c = 10;

	/**
	 * Constructor for TestPool class.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public TestPool() throws Exception
	{
		super(2);
		this.numOfRetry = 5;
	}

	/**
	 * This method create a object for pool.
	 * @return a new object.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	public Object createObject() throws Exception
	{
		++c;
		System.out.println("creating");
		return new String(String.valueOf(c));
	}

	/**
	 * This method remove a object form pool.
	 * @param pObject the object to be remove.
	 */
	public void removeObject(Object pObject)
	{
		System.out.println("removing");
		pObject = null;
	}

	/**
	 * This method get string that are free object.
	 * @return string type free object.
	 */
	public String getString()
	{
		try
		{
			return (String) getFreeObject();
		}
		catch (Exception ex)
		{
			return "cannot";
		}
	}

	/**
	 * This method make a specified string to check free object or not.
	 * @param pString the string corresponding object to be check.
	 */
	public void setString(String pString)
	{
		checkInObject(pString);
	}

	/**
	 * This is the entrance way to run a program.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args)
	{
		try
		{
			TestPool	testPool = new TestPool();
			Object		obj;

			/*
			 * obj = testPool.getString();
			 * System.out.println(obj);
			 * 
			 * obj = testPool.getString();
			 * System.out.println(obj);
			 */

			obj = testPool.getString();
			System.out.println(obj);
			testPool.setString((String) obj);

			/*
			 * testPool.shutdown();
			 * testPool.init();
			 */

			obj = testPool.getString();
			System.out.println(obj);
			testPool.setString((String) obj);

			obj = testPool.getString();
			System.out.println(obj);
			testPool.setString((String) obj);

			Thread.sleep(5000);

			System.out.println(testPool);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
