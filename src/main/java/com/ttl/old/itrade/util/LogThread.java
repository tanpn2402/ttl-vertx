package com.ttl.old.itrade.util;

// Author  : Wilfred
// Date    : Aug 17, 2000.

import java.util.Vector;

import com.ttl.old.itrade.interfaces.LogActionListener;

/**
 * This class will check if the linked list contains element then call the
 * LogActionListener to perform corresponding action
 * @author Wilfred
 * @since Aug 17, 2000.
 */
public class LogThread extends Thread
{
    private LogActionListener mvLogActionListener = null;
    private Vector mvLogList = null;
    private boolean mvIsRunning = false;

    /**
     * Constructor for LogThread class.
     * @param pLogActionListener An instance of LogActionListener.
     * @param pLogList An Vector object.
     */
    public LogThread(LogActionListener pLogActionListener, Vector pLogList)
    {
        mvLogActionListener = pLogActionListener;
        mvLogList = pLogList;
        mvIsRunning = true;
    }

    /**
     * This method will stop the thread.
     */
    public void destroy()
    {
        // if the linked list is not empty then wait until it is
        while (!mvLogList.isEmpty())
        {
            try
            {
                Thread.sleep(5000);
            }
            catch (InterruptedException ex)
            {
                ex.printStackTrace();
                Log.println(ex, Log.ERROR_LOG);
            }
        }
        mvIsRunning = false;
    }

    /**
     * The implementation of this method is called when the thread.
     */
    public void run()
    {
        if (mvLogActionListener != null && mvLogList != null)
        {
            while (mvIsRunning)
            {
                if (mvLogList.size() > 0)
                {
                    Object node = mvLogList.remove(0);
                    mvLogActionListener.logActionPerform(node);
                }
                else
                {
                    try
                    {
                        this.sleep(5000);
                    }
                    catch (InterruptedException ex)
                    {
                        ex.printStackTrace();
                        Log.println(ex, Log.ERROR_LOG);
                    }
                }
            }
        }
    }
}
