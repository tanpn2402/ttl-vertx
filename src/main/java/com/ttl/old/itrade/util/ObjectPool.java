package com.ttl.old.itrade.util;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * This class will mantain a pool of objects. You can call the method,
 * getFreeObject() to get an available object, or you can call the method,
 * checkInObject() to set the object back.
 */
public abstract class ObjectPool
{

	private Thread		keeper = null;
	private Hashtable   availableObjects = new Hashtable();
	private Hashtable   unAvailableObjects = new Hashtable();
	private Hashtable   useCount = new Hashtable();
	private int			initialNumOfObject;
	private String		objectType;
	protected int		numOfRetry = 2;

	/**
	 * Construct an ObjectPool class
	 * @param pNumOfObject initial number of objects.
	 */
	protected ObjectPool(int pNumOfObject) throws Exception
	{
		initialNumOfObject = pNumOfObject;
		if (!init())
		{
			throw new Exception("ObjectPool cannot create object pool!");
		}
	}

	/**
	 * This method will create objects with the initial number of object that
	 * specified in constructor. Note you should call the shutdown method before
	 * calling this method.
	 * @return is normal or throw exception.
	 */
	public boolean init()
	{
		try
		{

			// crate all objects
			for (int i = 0; i < initialNumOfObject; i++)
			{
				Object  object = createObject();

				// get the object type for reference
				if (i == 0)
				{
					objectType = object.getClass().toString();

				}
				addToAvailableList(object);
				setUseCount(object, 0);
			}

			// start the keeper thread
			startThread();
			return true;

		}
		catch (Exception ex)
		{
			Log.println(ex, Log.ERROR_LOG);
			return false;
		}
	}

	/**
	 * This method will start a thread that will remove any extra object
	 */
	private void startThread()
	{
		keeper = new Thread(new Runnable()
		{
			public void run()
			{
				while (true)
				{

					// check to see if the total number of objects is more than the initial
					// number of objects than release those extra object
					// keep minimum objects in the available list
					try
					{
						while (availableObjects.size() > initialNumOfObject)
						{
							Object  object = getObject();
							removeFromAvailableList(object);
							removeFromUseCount(object);
							removeObject(object);
							object = null;
						}

						// sleep for 5 mins
						Thread.sleep(300000);
					}
					catch (Exception ex) {}
				}		// end of while
			}			// end of run method
		});

		keeper.start();
	}

	/**
	 * This method will return a available object, if thre is no available
	 * connection then create a new object, if no more object can be
	 * create then throw an exception
	 * @return The free Object.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	protected Object getFreeObject() throws Exception
	{

		Object  object = getObject();
		if (object != null)
		{
			addToUnAvailableList(object);
			return object;
		}

		// no available object then open a crate a new object
		else
		{
			Log.println("[ ObjectPool.getFreeObject() no free object (" + objectType + ") ]", Log.ERROR_LOG);
			for (int i = 0; i < numOfRetry; i++)
			{
				try
				{
					object = createObject();
					addToUnAvailableList(object);
					setUseCount(object, 0);
					return object;
				}
				catch (Exception ex)
				{
					Log.println("[ ObjectPool.getFreeObject() cannot create object (" + objectType + ") ]", Log.ERROR_LOG);

					// if there is an error while creating object (cannot create a new object)
					// then wait for 1 second to see if we can get another object
					try
					{
						Thread.sleep(1000);
					}
					catch (Exception ex2) {}
					object = getObject();
					if (object != null)
					{
						addToUnAvailableList(object);
						return object;
					}
				}		// end of catch
			}			// end of for loop
		}
		throw new Exception("[ ObjectPool.getFreeObject() cannot create object: " + objectType + "]");
	}


	/**
	 * This method will remove all objects
	 */
	public void shutdown()
	{
		keeper.stop();
		keeper = null;

		// remove all available objects
		Enumeration e = availableObjects.keys();
		while (e.hasMoreElements())
		{
			Object  object = e.nextElement();
			removeFromAvailableList(object);
			removeFromUseCount(object);
			removeObject(object);
			object = null;
		}

		// remove all unavailable objects
		e = unAvailableObjects.keys();
		while (e.hasMoreElements())
		{
			Object  object = e.nextElement();
			removeFromUnAvailableList(object);
			removeFromUseCount(object);
			removeObject(object);
			object = null;
		}

	}

	/**
	 * This method to release resources.
	 */
	protected void finalize()
	{
		shutdown();
	}

	/**
	 * This method will add an object back to available list. Note this mehtod
	 * will check if the object has been use for more than 500 times then this
	 * method will remove it and create a new one instead inorder to free memory.
	 * @param pObject The object to be check.
	 */
	protected void checkInObject(Object pObject)
	{
		if (pObject == null)
		{
			return;

		}
		removeFromUnAvailableList(pObject);

		// check to see the number of use of that object, if it has been use for
		// more than 500 times then remove it and create a new one
		int count = ((Integer) useCount.get(pObject)).intValue();
		if (count > 500)
		{
			Log.println("[ ObjectPool.checkInObject() the object has been used for more than 500 times. ]", Log.ERROR_LOG);
			removeFromUseCount(pObject);
			removeObject(pObject);
			try
			{
				pObject = createObject();
				setUseCount(pObject, 0);
			}
			catch (Exception ex)
			{
				Log.println(ex, Log.ERROR_LOG);
			}
		}
		else
		{
			setUseCount(pObject, ++count);
		}

		addToAvailableList(pObject);
	}


	/**
	 * This method will return a available connection or null if there is no
	 * available object
	 */
	private Object getObject()
	{

		synchronized (availableObjects)
		{
			if (availableObjects.size() > 0)
			{
				Enumeration lvEnum = availableObjects.keys();

				// get the first available object, remove it from the available list
				// then add it to unavailable list
				while (lvEnum.hasMoreElements())
				{
					Object  object = lvEnum.nextElement();
					availableObjects.remove(object);
					return object;
				}
			}
			return null;
		}

	}

	/**
	 * This method add the object to unavailable list.
	 * @param pObject The object to be add.
	 */
	private void addToUnAvailableList(Object pObject)
	{
		synchronized (unAvailableObjects)
		{
			unAvailableObjects.put(pObject, new java.util.Date());
		}
	}

	/**
	 * This method remove the object form unavailable list.
	 * @param pObject The object to be remove.
	 */
	private void removeFromUnAvailableList(Object pObject)
	{
		synchronized (unAvailableObjects)
		{
			unAvailableObjects.remove(pObject);
		}
	}

	/**
	 * This method add the object to available list.
	 * @param pObject The object to be add.
	 */
	private void addToAvailableList(Object pObject)
	{
		synchronized (availableObjects)
		{
			availableObjects.put(pObject, new java.util.Date());
		}
	}

	/**
	 * This method remove the object form available list.
	 * @param pObject The object to be remove.
	 */
	private void removeFromAvailableList(Object pObject)
	{
		synchronized (availableObjects)
		{
			availableObjects.remove(pObject);
		}
	}

	/**
	 * This method set the use count.
	 * @param pObject An object that are key.
	 * @param pCount The use count.
	 */
	private void setUseCount(Object pObject, int pCount)
	{
		synchronized (useCount)
		{
			useCount.put(pObject, new Integer(pCount));
		}
	}

	/**
	 * This method remove key form useCount map.
	 * @param pObject The key with remove.
	 */
	private void removeFromUseCount(Object pObject)
	{
		synchronized (useCount)
		{
			useCount.remove(pObject);
		}
	}

	/**
	 * This method to back String.
	 */
	public String toString()
	{
		StringBuffer	sb = new StringBuffer();
		synchronized (availableObjects)
		{
			sb.append("\nNumber of available objects: " + availableObjects.size());
		}
		synchronized (unAvailableObjects)
		{
			sb.append("\nNumber of unavailable objects: " + unAvailableObjects.size());
		}
		synchronized (useCount)
		{
			sb.append("\nuseCount: " + useCount);
		}
		sb.append("\n");
		return sb.toString();
	}

	/**
	 * This method to create a object.
	 * @return An object.
	 * @throws Exception If the program does not normally arise during the implementation.
	 */
	abstract Object createObject() throws Exception;
	/**
	 * This method to remove a object 
	 * @param pObject An object.
	 */
	abstract void removeObject(Object pObject);

}
