package com.ttl.old.itrade.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.ttl.old.itrade.interfaces.ValidationRules;

/**
 * The DefaultValidationRules class defined methods that get base ready for validator.
 * @author
 *
 */
public abstract class DefaultValidationRules implements ValidationRules
{
	/**
	 * This method to get validator class.
	 * @param pRule Object of ValidationRule.
	 * @return A validation class.
	 * @throws ClassNotFoundException If you are looking for a class does not exist.
	 */
	public static Class getValidatorClass(ValidationRule pRule) throws ClassNotFoundException
	{

		return Class.forName(VALIDATIONLOCATION + pRule.getValidation() + "Validator");
	}

	/**
	 * This method to set error code.
	 * @param pClass A class.
	 * @param pRule Object of ValidationRule.
	 * @throws NoSuchMethodException If you can not find matching method.
	 * @throws IllegalAccessException If the illegal access method.
	 * @throws IllegalArgumentException If you pass an illegal parameter.
	 * @throws InvocationTargetException If the target invocation have error. 
	 */
	public static void setErrorCode(Class pClass, ValidationRule pRule) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		Method  method = pClass.getMethod("getErrCode", null);
		Log.println("[DefaultValidationRules.setErrorCode(): c.getName() = " + pClass.getName() + "]", Log.DEBUG_LOG);
		Log.println("[DefaultValidationRules.setErrorCode(): method.getName() = " + method.getName() + "]", Log.DEBUG_LOG);
		pRule.setErrCode(((Integer) method.invoke(null, null)).intValue());
	}

	/**
	 * This method to set validate rule.
	 * @param pRule Object of ValidationRule.
	 */
	public static void setRule(ValidationRule pRule)
	{
		try
		{
			Class   c = getValidatorClass(pRule);
			setErrorCode(c, pRule);
			pRule.setValidator(c);
		}
		catch (ClassNotFoundException e)
		{
			Log.println("[DefaultValidationRules: Error = " + e + "]", Log.ERROR_LOG);
		}
		catch (NoSuchMethodException e)
		{
			Log.println("[DefaultValidationRules: Error = " + e + "]", Log.ERROR_LOG);
		}
		catch (IllegalAccessException e)
		{
			Log.println("[DefaultValidationRules: Error = " + e + "]", Log.ERROR_LOG);
		}
		catch (IllegalArgumentException e)
		{
			Log.println("[DefaultValidationRules: Error = " + e + "]", Log.ERROR_LOG);
		}
		catch (InvocationTargetException e)
		{
			Log.println("[DefaultValidationRules: Error = " + e + "]", Log.ERROR_LOG);
		}
	}
}
