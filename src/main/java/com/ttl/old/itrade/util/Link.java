package com.ttl.old.itrade.util;

/**
 * The Link class make designated object to relevance Link instance.
 * @author
 *
 */
public class Link
{

	Object  data;
	Link	next;

	/**
	 * Constructor for Link class.
	 * @param pObject A designated object.
	 * @param pLink Object of Link.
	 */
	Link(Object pObject, Link pLink)
	{

		data = pObject;
		next = pLink;
	}

}

