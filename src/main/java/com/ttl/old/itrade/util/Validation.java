package com.ttl.old.itrade.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.Hashtable;

import com.ttl.old.itrade.interfaces.ValidationRules;
import com.ttl.old.itrade.interfaces.Validator;
import com.ttl.old.itrade.validation.DefaultFieldValidator;

/**
 * The Validation class defined methods that are validating the data from actions.
 * @author
 *
 */
public class Validation
{
	private String		_sActionName = null;
	protected Hashtable _hFormData = null; 

	private String		_sActionRules = null;
	private Hashtable   _hRulesTable = null;
	private boolean		_bIsReady = false;

	/**
	 * This constructor needed to extend this business object class to handle the most generic cases
	 */
	public Validation() {}

	/**
	 * This business object constructor receives an actionName
	 * @param pActionName An action name.
	 */
	public Validation(String pActionName)
	{

		// Call the setActionName method to setup the actionName
		setActionName(pActionName);
	}

	/**
	 * The set actionName stores the actionName and constructs the actionRules name
	 * @param pActionName An action name.
	 */
	protected void setActionName(String pActionName)
	{

		// Store the actionName class variable
		_sActionName = pActionName;

		// The action rules is the actionName with "Rules" appended to it.
		// Debug.println("[Validation.setActionName(): _sActionName = " + _sActionName + "]");
		_sActionRules = _sActionName + ValidationRules.RULES_TAG;

		// Debug.println("[Validation.setActionName(): _sActionRules = " + _sActionRules + "]");
	}

	/**
	 * This method to init for load validation rules.
	 * @return If load validation rules success is true,else is false.
	 */
	public boolean init()
	{

		// Debug.println("[Validation.init():]");
		return loadValidationRules();
	}

	/**
	 * This method attempts to load the validation rules corresponding to this action.  These rules include the required status of the fields, formatting definition, and grouping of data, if any.
	 * @return If load validation rules success is true,else is false.
	 */
	private boolean loadValidationRules()
	{

		// Initialize the rules table to null
		// Debug.println("[Validation.loadValidationRules():]");
		_hRulesTable = null;
		Class   c = null;

		try
		{

			// Load the ValidationRules Class
			// Debug.println("[Validation.loadValidationRules(): ValidationRules = " + ValidationRules.RULESLOCATION + _sActionRules + "]");
			c = Class.forName(ValidationRules.RULESLOCATION + _sActionRules);

			// Debug.println("[Validation.loadValidationRules(): c = " + c + "]");
		}
		catch (ClassNotFoundException e)
		{

			// The hashtable was not found.  All actions will have some form of
			// rule interface.

			Log.println("[Validation.loadValidationRules(): Error = " + e + "]", Log.ERROR_LOG);
			Log.println("[Validation.loadValidationRules(): Class(" + ValidationRules.RULESLOCATION + _sActionRules + ") not found]", Log.ERROR_LOG);
			return false;
		}

		// Debug.println("[Validation.loadValidationRules(): get registered rules]");
		try
		{

			// Get the hashtable based upon the action name (using reflection)
			for (int i = 0; i < c.getFields().length; i++)
			{

				// Debug.println("[Validation.loadValidationRules(): c.getFields[" + i + "] = " + c.getFields()[i] + "]");
			}
			_hRulesTable = (Hashtable) c.getField("RegisterRules").get(null);
			printHashtable(_hRulesTable);
			Enumeration		e = _hRulesTable.elements();
			Class			ruleClass;
			ValidationRule  rule;

			while (e.hasMoreElements())
			{
				rule = (ValidationRule) e.nextElement();
				ruleClass = rule.getValidator();
				if (ruleClass != null)
				{

					// Debug.println("[Validation.loadValidationRules(): ruleClass.getName() = " + ruleClass.getName() + "]");
				}
			}
		}
		catch (NoSuchFieldException e)
		{
			Log.println("[Validation.loadValidationRules(): Error = " + e + "]", Log.ERROR_LOG);
			return false;
		}
		catch (IllegalAccessException e)
		{
			Log.println("[Validation.loadValidationRules(): Error = " + e + "]", Log.ERROR_LOG);
			return false;
		}

		_bIsReady = true;
		return true;
	}

	/**
	 * This method print the Hashtable data.
	 * @param pHashtable instance of Hashtable.
	 */
	private void printHashtable(Hashtable pHashtable)
	{

		/*
		 * try {
		 * Enumeration enum = h.keys();
		 * String key;
		 * String value;
		 * while (enum.hasMoreElements()) {
		 * key = (String) enum.nextElement();
		 * value = (String) h.get(key).toString();
		 * //Debug.println("[Validation.printHashtable(): = " + (key +", "+ value) + "]");
		 * }
		 * }
		 * catch (Exception e) {
		 * Debug.println("[Validation.printHashtable(): Error = " + e + "]");
		 * }
		 */
	}

	/**
	 * This method to validate the data in Hashtable.
	 * @param pFormData An instance of Hashtable.
	 * @return the validate error code.
	 */
	public ErrorCode validate(Hashtable pFormData)
	{

		_hFormData = pFormData;

		// Debug.println("[Validation.validate(): print _hFormData]");
		printHashtable(_hFormData);

		// Debug.println("[Validation.validate(): print _hRulesTable]");
		printHashtable(_hRulesTable);

		// Retrieve the keys from the rules hashtable
		Enumeration eKeys = _hRulesTable.keys();
		String		sRuleName;
		String[]	sFieldNames;

		String		sValidation = null;
		ErrorCode   errCode = null;
		String		sValidationClass = null;

		// While there are still keys remaining in the business data
		while (eKeys.hasMoreElements())
		{

			// Store the key name
			// Retrieve the key value from the rules table
			sRuleName = (String) eKeys.nextElement();

			// Debug.println("[Validation.validate(): sRuleName = " + sRuleName + "]");

			ValidationRule  rule = (ValidationRule) _hRulesTable.get(sRuleName);

			// Debug.println("[Validation.validate(): rule = " + rule + "]");

			sFieldNames = rule.getFieldNames();
			sValidation = rule.getValidation();

			// Store the user-entered value

			// the field is in the mandatory hash table

			// the field is mandatory, either because it is required
			// or because of a group dependency.

			// load the field type validator
			sValidationClass = ValidationRules.VALIDATIONLOCATION + sValidation + "Validator";

			// Debug.println("[Validation.validate(): sValidationClass = " + sValidationClass + "]");
			Validator validator = null;

			// Create the arguments contained in the constructor
			Class[]		args;
			Object[]	parameters;

			// Debug.println("[Validation.validate(): sFieldNames.length = " + sFieldNames.length + "]");
			if (sFieldNames.length == 1)
			{
				args = new Class[2];
				args[0] = String.class;
				args[1] = String.class;

				// create the arguments for the constructor
				parameters = new Object[2];

				parameters[0] = sFieldNames[0];
				parameters[1] = pFormData.get(sFieldNames[0]);

				// Debug.println("[Validation.validate(): parameters[0] = " + parameters[0] + "]");
				// Debug.println("[Validation.validate(): parameters[1] = " + parameters[1] + "]");
			}
			else
			{
				args = new Class[1];
				args[0] = Hashtable.class;

				// create the arguments for the constructor
				parameters = new Object[1];
				Hashtable   hData = new Hashtable();

				for (int i = 0; i < sFieldNames.length; i++)
				{
					hData.put(sFieldNames[i], pFormData.get(sFieldNames[i]));
				}
				parameters[0] = hData;
			}

			try
			{

				// Debug.println("[Validation.validate(): get class]");
				Class   c = rule.getValidator();

				if (c == null)
				{
					c = Class.forName(sValidationClass);
				}

				// get the constructor from the class
				// Debug.println("[Validation.validate(): c = " + c.getName() + "]");
				Constructor con;
				try
				{

					// Debug.println("[Validation.validate(): get constructor]");
					Constructor[]   cons = c.getConstructors();
					for (int i = 0; i < cons.length; i++)
					{

						// Debug.println("[Validation.validate(): cons["+ i + "].getName() = " +  cons[i].getName() + "]");
						Class[] parameterClasses = cons[i].getParameterTypes();
						for (int k = 0; k < parameterClasses.length; k++)
						{

							// Debug.println("[Validation.validate(): parameterClasses[" + k +"].getName() = " +  parameterClasses[k].getName() + "]");
						}
					}
					con = c.getConstructor(args);

					// Debug.println("[Validation.validate(): constructor created]");
				}
				catch (NoSuchMethodException e)
				{
					Log.println("[Valiation.validate(): Error = " + e + "]", Log.ERROR_LOG);
					return null;
				}

				// create the constructor with the fieldData parameter
				try
				{

					// Debug.println("[Validation.validate(): create validator]");
					validator = (Validator) con.newInstance(parameters);
				}
				catch (InstantiationException e)
				{
					Log.println("[Validation.validate(): Error = " + e + "]", Log.ERROR_LOG);
					return null;
				}
				catch (InvocationTargetException e)
				{
					Log.println("[Validation.validate(): Error = " + e + "]", Log.ERROR_LOG);
					return null;
				}
				catch (IllegalAccessException e)
				{
					Log.println("[Validation.validate(): Error = " + e + "]", Log.ERROR_LOG);
					return null;
				}
			}
			catch (ClassNotFoundException e)
			{
				// Load the default validator
            Log.println(e, Log.ERROR_LOG);
				Log.println("[Validation.validate(): load default validator]", Log.ERROR_LOG);
				validator = new DefaultFieldValidator(sFieldNames[0], (String) pFormData.get(sFieldNames[0]));
			}

			// Debug.println("[Validation.validate(): validator.isValid() = " + validator.isValid() + "]");
			// test the field level validator against the field data
			if (!validator.isValid())
			{

				// Debug.println("[Validation.validate(): return error]");
				return new ErrorCode(sFieldNames, rule.getErrCode());
			}
			if (errCode != null)
			{
				return errCode;

			}
		}

		// Debug.println("[Validation.validate(): no error]");
		try
		{
			return new ErrorCode(new String[0], 0);
		}
		catch (Exception e)
		{
			Log.println("[Validation.validate(): Error = " + e + "]", Log.ERROR_LOG);
			return null;
		}
	}
}
