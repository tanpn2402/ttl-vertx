package com.ttl.old.itrade;

import com.ttl.old.itrade.biz.model.ITradeUser;
import com.ttl.old.itrade.hks.bean.HKSStockInfoBean;
import com.ttl.old.itrade.mds.bean.mdsStockInfoBean;
import com.ttl.old.itrade.util.Log;
import com.ttl.old.itrade.util.TextUtil;
import com.ttl.old.itrade.web.engine.model.ItradeResponseJSONBean;
import com.ttl.old.itrade.web.ticker.TickerViewProccessor;
import com.ttl.wtrade.utils.WTradeLogger;
import org.atmosphere.cpr.*;
import org.atmosphere.cpr.AtmosphereResource.TRANSPORT;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/*import com.itrade.web.helper.ITradeSessionManager;*/

public class ITradeAtmosphereAPI {

    public static final String TOPIC_PROTOCOL = "/${service}/${action}/${clientID}";

    public static final String CONTRACT_SERVICE = "service";
    public static final String CONTRACT_ACTION = "action";
    public static final String CONTRACT_TOPIC = "action";
    public static final String CONTRACT_CLIENTID = "clientID";

    public static final String BROADCAST_SERVICE = "ITradePushServer";
    public static final String BROADCAST_SUBCRIBE = "Subscribe";

    private volatile static ITradeAtmosphereAPI instance = null;
    private ITradeAtmosphereUserRegister userRegister = null;

    private ITradeAtmosphereAPI() {
        userRegister = new ITradeAtmosphereUserRegister();
    }

    BroadcasterFactory mvBroadcasterFactory;
    AtmosphereResourceFactory mvAtmosphereResourceFactory;
    MetaBroadcaster mvMetaBroadcaster;
    Object lock0 = new Object();
    Object lock1 = new Object();
    Object lock2 = new Object();

    public ITradeAtmosphereAPI setBroadcasterFactory(final BroadcasterFactory pFactory) {
        if (this.mvBroadcasterFactory == null) {
            synchronized (lock0) {
                if (this.mvAtmosphereResourceFactory == null) {
                    this.mvBroadcasterFactory = pFactory;
                }
            }
        }
        return this;
    }

    public ITradeAtmosphereAPI setResourceFactory(final AtmosphereResourceFactory pAtmosphereResourceFactory) {
        if (this.mvAtmosphereResourceFactory == null) {
            synchronized (lock1) {
                if (this.mvAtmosphereResourceFactory == null) {
                    this.mvAtmosphereResourceFactory = pAtmosphereResourceFactory;
                }
            }
        }
        return this;
    }

    public ITradeAtmosphereAPI setMetaBroadcaster(final MetaBroadcaster pMetaBroadcaster) {
        if (this.mvMetaBroadcaster == null) {
            synchronized (lock2) {
                if (this.mvMetaBroadcaster == null) {
                    this.mvMetaBroadcaster = pMetaBroadcaster;
                }
            }
        }
        return this;
    }

    public static ITradeAtmosphereAPI getInstance() {
        if (instance == null) {
            synchronized (ITradeAtmosphereAPI.class) {
                if (instance == null) {
                    instance = new ITradeAtmosphereAPI();
                }
            }
        }
        return instance;
    }

    private String contract(HashMap<String, String> data) {
        HashMap<String, String> padding = data;
        String contract = null;
        if (data.get(CONTRACT_SERVICE) == null) {
            padding.put(CONTRACT_SERVICE, BROADCAST_SERVICE);
        }
        if (data.get(CONTRACT_ACTION) == null) {
            padding.put(CONTRACT_ACTION, "StockInfo");
        }
        if (data.get(CONTRACT_TOPIC) == null) {
            padding.put(CONTRACT_TOPIC, "");
        }
        if (data.get(CONTRACT_CLIENTID) == null) {
            padding.put(CONTRACT_CLIENTID, "");
        }

        contract = TextUtil.composeMessage(TOPIC_PROTOCOL, padding);

        while (contract.endsWith("/")) {
            contract = contract.substring(0, contract.length() - 1);
        }
        contract = contract.replace("//", "/");

        return contract;
    }


    private void broadcast(Broadcaster broadcaster, Object jsonObject, String topic) {

        ItradeResponseJSONBean lvResponseJSONBean = new ItradeResponseJSONBean(topic, 0, jsonObject);
        boolean ready = broadcaster != null;
        if (ready && broadcaster.getAtmosphereResources().size() > 0 && broadcaster != null) {
            broadcaster.broadcast(lvResponseJSONBean);
        }

    }

    private Broadcaster lookupBroadcaster(final String s, boolean paraBool) {
        try {
            Broadcaster b = getDefaultAtmosBroadcasterFactory().lookup(s, paraBool);
            return b;
        } catch (NullPointerException e) {
            Log.print("[ITradeAtmosphereAPI] Null Pointer Exception when lookup for: " + s, Log.ERROR_LOG);
            return null;
        }
    }

    private AtmosphereResourceFactory getAtmosphereResourceFactory() {
        return this.mvAtmosphereResourceFactory;
    }

    private BroadcasterFactory getDefaultAtmosBroadcasterFactory() {
        return this.mvBroadcasterFactory;
    }


    public void pushTopic(final String clientId, final String topic, Object o) {
        WTradeLogger.print("AtmosphereAPI", "broadcasted info of " + ((mdsStockInfoBean) o).getMvStockCode());
        HashMap<String, String> data = new HashMap<>();
        data.put(CONTRACT_CLIENTID, clientId);
        String lvBid = contract(data);
        Broadcaster lvBroadcaster = lookupBroadcaster(lvBid, false);
        broadcast(lvBroadcaster, o, topic);
    }

    public void subscribeTopic(AtmosphereResource resource, String... topics) {
        ITradeUser iTradeUser = userRegister.retrieve(resource);
        if (iTradeUser != null) {
            HashMap<String, String> padding = new HashMap<String, String>();
            padding.put(CONTRACT_CLIENTID, iTradeUser.getMvAccountID());

            for (String topic : topics) {
                padding.put(CONTRACT_TOPIC, topic);

            }
        }

    }

    private boolean checkIfEstablished(AtmosphereResource r, Broadcaster b) {
        Collection<AtmosphereResource> resources = b.getAtmosphereResources();
        for (AtmosphereResource re : resources) {
            if (re.uuid().equals(r.uuid())) {
                return true;
            }
        }
        return false;

    }

    public ITradeUser connect(AtmosphereResource resource, String clientid) {
//        Broadcaster b = resource.getBroadcaster();
        ITradeUser iTradeUSer = userRegister.offer(resource, clientid);
        return iTradeUSer;
    }

    public ITradeUser connect(AtmosphereResource resource, String bid, String clientid, boolean remove) {
        if (remove) {
            if (resource.transport() != TRANSPORT.LONG_POLLING) {
                resource.getBroadcaster().removeAtmosphereResource(resource);
            }
        }
        Broadcaster broadcaster = lookupBroadcaster(bid, false);
        if (broadcaster == null) {
            broadcaster = lookupBroadcaster(bid, true);
        }

        if (!checkIfEstablished(resource, broadcaster)) {
            broadcaster.addAtmosphereResource(resource);
            return connect(resource, bid);
        }
        return connect(resource, clientid);

    }

    public ITradeUser retrieve(final String clientId) {
        return userRegister.retrieve(clientId);
    }

    public ITradeUser retrieve(AtmosphereResource r) {
        return userRegister.retrieve(r);
    }


    private class ITradeAtmosphereUserRegister {
        ConcurrentHashMap<String, ITradeUser> users = new ConcurrentHashMap();

        public void filter(Collection<AtmosphereResource> resource) {
            for (AtmosphereResource r : resource) {
                final String uuid = r.uuid();
                if (!users.containsKey(uuid)) {
                    getAtmosphereResourceFactory().remove(uuid);
                }
            }
        }

        public ITradeUser offer(AtmosphereResource resource, String clientID) {
            ITradeUser user = users.get(resource.uuid());
            ITradeUser userSession = new ITradeUser();

            WTradeLogger.print("AtmosphereAPI","Create new user session");

            userSession.setMvAccountID(clientID);
            userSession.setMvSeesionID(resource.uuid());
            userSession.setMvLoginIpAddress(resource.getRequest().getRemoteAddr());
            userSession.setMvFullName(clientID);
            userSession.setMvAccountName(clientID);
            userSession.setMvTickerViewProccessor(new TickerViewProccessor(userSession));

            if (user == null) {
                if (userSession != null) {
                    disconnect(userSession);
                    if (!users.containsKey(clientID)) {
                        users.put(clientID, userSession);
                        WTradeLogger.print("AtmosphereAPI",String.format("-|  Mapped: %s : %s",clientID,resource.uuid()));
                    }
                }else {
                    WTradeLogger.print("AtmosphereAPI","nothing");

                }
                user = userSession;
            } else {
                WTradeLogger.print("AtmosphereAPI",String.format("-|  Disconnected: %s : %s",clientID,resource.uuid()));
                disconnect(user);
            }
            return user;
        }

        public void disconnect(ITradeUser user) {
            String uuid = retriveUuid(user.getMvAccountID());
            if (users.contains(user) && !uuid.equals("")) {
                users.remove(uuid);
            }
        }

        public ITradeUser retrieve(final String clientId) {
            ITradeUser sessionuser = null;
            Set<Entry<String, ITradeUser>> entries = users.entrySet();
            for (Entry<String, ITradeUser> entry : entries) {
                if (entry.getValue().getMvAccountID().equals(clientId)) {
                    sessionuser = (ITradeUser) entry.getValue();
                    break;
                }
            }
            return sessionuser;
        }

        public ITradeUser retrieve(final AtmosphereResource r) {
            return users.get(r.uuid());
        }

        public AtmosphereResource retriveAtmosphereResource(final ITradeUser itradeUser) {
            return retriveAtmosphereResource(itradeUser.getMvAccountID());

        }

        public AtmosphereResource retriveAtmosphereResource(final String loginid) {
            Set<Entry<String, ITradeUser>> entries = users.entrySet();
            AtmosphereResource atmosphereResource = null;
            for (Iterator<Entry<String, ITradeUser>> iterator = entries.iterator(); iterator.hasNext(); ) {
                Entry<String, ITradeUser> entry = iterator.next();
                if (loginid.equals(entry.getValue().getMvAccountID())) {
                    atmosphereResource = getAtmosphereResourceFactory().find(entry.getKey());
                    break;
                }
            }
            return atmosphereResource;
        }

        public String retriveUuid(String clienid) {
            Set<Entry<String, ITradeUser>> entries = users.entrySet();
            for (Iterator<Entry<String, ITradeUser>> iterator = entries.iterator(); iterator.hasNext(); ) {
                Entry<String, ITradeUser> entry = iterator.next();
                if (clienid.equals(entry.getValue().getMvAccountID())) {
                    return entry.getKey();
                }
            }
            return "";
        }

    }
}














