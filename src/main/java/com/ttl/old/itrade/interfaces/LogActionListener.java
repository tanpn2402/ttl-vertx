package com.ttl.old.itrade.interfaces;

/**
 * The LogActionListener interface defined methods that listener log action.
 * @author Wilfred
 * @Copyright Aug 17, 2000.
 */
public interface LogActionListener
{
	/**
	 * This method to do some operation when this listener to be triggered.
	 * @param pNode An object with trigger listener.
	 */
	public void logActionPerform(Object pNode);
}
