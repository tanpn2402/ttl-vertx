package com.ttl.old.itrade.interfaces;

/**
 * The Validator interface defined methods to perform specifically validation operation.
 * @author
 *
 */
public interface Validator
{
	/**
	 * This method to validate is valid or not.
	 * @return The result of validation.
	 */
	public boolean isValid();
}
