package com.ttl.old.itrade.interfaces;

import java.util.Hashtable;

/**
 * The ValidationRules interface defined constant fields which repeatedly use on validation.
 * @author
 *
 */
public interface ValidationRules
{
	/**
	 * Initialize the hashtables that will contain the rules
	 */
	public static Hashtable		RegisterRules = new Hashtable();

	// Store the RULES_TAG variable used for BusinessRules objects
	public static final String  RULES_TAG = "Rules";
	public static final String  MISSING_TAG = "_Missing";
	public static final String  ERROR_TAG = "_Error";
	public static final String  RULESLOCATION = "com.itrade.rules.";
	public static final String  VALIDATIONLOCATION = "com.itrade.validation.";
	public static final String  RULESNAME = null;

	public static final String  PACKAGEROOT = "com.itrade.";
}
