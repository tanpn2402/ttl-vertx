package com.ttl.old.itrade.comet.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Utils {
	public static int getCurrentHHmm() {
		final Calendar cal = Calendar.getInstance();
		return (100 * cal.get(Calendar.HOUR_OF_DAY) + cal.get(Calendar.MINUTE));
	}
	/**
	 * Check current date is trading date.
	 * 
	 * @param listDayOff List of day-off in format 'FROMDATE:TODATE', the date is in format 'YYYYMMDD'
	 * @return True if today is trading date, otherwise false
	 */
	public static boolean isTradingDate(final List<String> listDayOff) {
		final Calendar cal = Calendar.getInstance();
		final int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
			return false;
		}
		if (listDayOff != null && !listDayOff.isEmpty()) {
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			final String cDate = sdf.format(new Date());
			for (String dayOff : listDayOff) {
				final String[] array = dayOff.split(":");
				if (array == null || array.length != 2)
					continue;
				if (array[0].compareTo(cDate) <= 0  && cDate.compareTo(array[1]) <= 0) {
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * Format date in a given pattern.
	 * 
	 * @param dbDate java.sql.Date  
	 * @param pattern String
	 * @return String
	 */
	public static String formatDate(final java.sql.Date dbDate, final String pattern) {
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(dbDate.getTime());
		final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(cal.getTime());
	}
	
	public static String toSqlIN(final String... elements) {
		if (elements == null || elements.length == 0)
			throw new IllegalArgumentException();
		final StringBuilder str = new StringBuilder();
		for (String elm : elements) {
			if (str.length() == 0) {
				str.append('(').append("'").append(elm).append("'");
			} else {
				str.append(',').append("'").append(elm).append("'");
			}
		}
		str.append(')');
		return str.toString();
	}	
	

	public static String getCurrentHHmmAsString() {
		final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(new Date());
	}
	
	public static String getServerTimeInJson() {
		final SimpleDateFormat sdf = new SimpleDateFormat(Constants.SERVER_TIME_PATTERN_24H);
		return String.format("{'time':'%s'}", sdf.format(new Date()));
	}
}
