package com.ttl.old.itrade.comet.eventHandler.caching;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ttl.old.itrade.comet.dataInfo.StockWatchInfo;
import com.ttl.old.itrade.comet.dataInfo.StockWatchList;
//import com.itrade.comet.eventHandler.producer.StockWatchReader;
import com.ttl.old.itrade.hks.bean.HKSMarketDataBean;

public class StockWatchDataCachingManager {
	
	private static final Logger logger = LogManager.getLogger(StockWatchDataCachingManager.class);
	
	private static final Map<String, StockWatchInfo> mapStockWatch = new HashMap<String, StockWatchInfo>();
	
	public static final BlockingQueue<StockWatchInfo> pendingRequests = new LinkedBlockingQueue<StockWatchInfo>();
	
	public static void registerStockWatch(final String clientId, final String currentSymbol, final String marketId, String mvStockWatchID) {
		registerStockWatch(clientId, currentSymbol, marketId, mvStockWatchID, StockWatchInfo.STOCK_WATCH);
	}
	
	public static void registerStockWatch(final String clientId, final String currentSymbol, final String marketId, String mvStockWatchID, String watchType) {
		synchronized (mapStockWatch) {
			if (!GenericValidator.isBlankOrNull(mvStockWatchID)) {
				StockWatchInfo stockWatch = mapStockWatch.get(mvStockWatchID);
				if (stockWatch == null) {
					String key = marketId + "|" + marketId;
					//stockWatch = StockWatchReader.stockInfoCached.get(key);
					if(stockWatch == null){
						stockWatch = new StockWatchInfo();
					}else {
						try {
							stockWatch = stockWatch.clone();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}

				stockWatch.setMvClientId(clientId);
				stockWatch.setCurrentSymbol(currentSymbol);
				stockWatch.setMarketId(marketId);
				stockWatch.setMvStockWatchID(mvStockWatchID);
				stockWatch.setWatchType(watchType);
				mapStockWatch.put(mvStockWatchID, stockWatch);
				pendingRequests.offer(stockWatch);
			}
			// System.out.println("[Register]: Stock watch map size: " +
			// mapStockWatch.size() + " -> " +
			// mapStockWatch.keySet().toString());
		}
	}
	
	public static StockWatchInfo unregisterStockWatch(final String stockWachId) {
		if (logger.isDebugEnabled())
			logger.debug("Remove caching stock watch id: " + stockWachId);
		synchronized (mapStockWatch) {
			StockWatchInfo removed = mapStockWatch.remove(stockWachId);
			//System.out.println("[Removed]: Stock watch map size: " + mapStockWatch.size() + " -> " + mapStockWatch.keySet().toString());
			return removed;
		}
	}
	
	public static boolean updateStockWatchInfoIfChanged(final StockWatchInfo nAccSum) {
		synchronized (mapStockWatch) {
			boolean isChanged = false;
			final StockWatchInfo oAccSum = mapStockWatch.get(nAccSum.getMvStockWatchID());
			if (oAccSum != null && StockWatchDataCachingManager.updateIfChanged(oAccSum, nAccSum)) {				
				isChanged = true;
				mapStockWatch.put(nAccSum.getMvStockWatchID(), nAccSum);
			}
			return isChanged;
		}
	}
	
	public static boolean updateIfChanged(StockWatchInfo oldBean, StockWatchInfo newBean)
	{
		boolean isChanged = false;
		//Check change in MvMarketDataBean
		HKSMarketDataBean newMvMarketDataBean = newBean.getMvMarketDataBean();
		HKSMarketDataBean oldMvMarketDataBean = oldBean.getMvMarketDataBean();
		//oldMvMarketDataBean.setUpdate(false);
		if((oldMvMarketDataBean == null && newMvMarketDataBean != null)
			|| !newMvMarketDataBean.getMvBestBid1Price().equalsIgnoreCase(oldMvMarketDataBean.getMvBestBid1Price())
			|| !newMvMarketDataBean.getMvBestBid1Volume().equalsIgnoreCase(oldMvMarketDataBean.getMvBestBid1Volume())
			|| !newMvMarketDataBean.getMvBestBid2Price().equalsIgnoreCase(oldMvMarketDataBean.getMvBestBid2Price())
			|| !newMvMarketDataBean.getMvBestBid2Volume().equalsIgnoreCase(oldMvMarketDataBean.getMvBestBid2Volume())
			|| !newMvMarketDataBean.getMvBestBid3Price().equalsIgnoreCase(oldMvMarketDataBean.getMvBestBid3Price())
			|| !newMvMarketDataBean.getMvBestBid3Volume().equalsIgnoreCase(oldMvMarketDataBean.getMvBestBid3Volume())
			|| !newMvMarketDataBean.getMvBestOffer1Price().equalsIgnoreCase(oldMvMarketDataBean.getMvBestOffer1Price())
			|| !newMvMarketDataBean.getMvBestOffer1Volume().equalsIgnoreCase(oldMvMarketDataBean.getMvBestOffer1Volume())
			|| !newMvMarketDataBean.getMvBestOffer2Price().equalsIgnoreCase(oldMvMarketDataBean.getMvBestOffer2Price())
			|| !newMvMarketDataBean.getMvBestOffer2Volume().equalsIgnoreCase(oldMvMarketDataBean.getMvBestOffer2Volume())
			|| !newMvMarketDataBean.getMvBestOffer3Price().equalsIgnoreCase(oldMvMarketDataBean.getMvBestOffer3Price())
			|| !newMvMarketDataBean.getMvBestOffer3Volume().equalsIgnoreCase(oldMvMarketDataBean.getMvBestOffer3Volume())
			|| !newMvMarketDataBean.getMvBuyForeignQty().equalsIgnoreCase(oldMvMarketDataBean.getMvBuyForeignQty())
			|| !newMvMarketDataBean.getMvCeilingPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvCeilingPrice())
			|| !newMvMarketDataBean.getMvClosePrice().equalsIgnoreCase(oldMvMarketDataBean.getMvClosePrice())
			|| !newMvMarketDataBean.getMvCurrentRoom().equalsIgnoreCase(oldMvMarketDataBean.getMvCurrentRoom())
			|| !newMvMarketDataBean.getMvFloorPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvFloorPrice())
			|| !newMvMarketDataBean.getMvHighPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvHighPrice())
			|| !newMvMarketDataBean.getMvLowPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvLowPrice())
			|| !newMvMarketDataBean.getMvMarketID().equalsIgnoreCase(oldMvMarketDataBean.getMvMarketID())
			|| !newMvMarketDataBean.getMvMatchPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvMatchPrice())
			|| !newMvMarketDataBean.getMvMatchQty().equalsIgnoreCase(oldMvMarketDataBean.getMvMatchQty())
			|| !newMvMarketDataBean.getMvNoalPriSubRefPri().equalsIgnoreCase(oldMvMarketDataBean.getMvNoalPriSubRefPri())
			|| !newMvMarketDataBean.getMvNoalPriPerRate().equalsIgnoreCase(oldMvMarketDataBean.getMvNoalPriPerRate())
			|| newMvMarketDataBean.getMvNomCmToRefPri() != (oldMvMarketDataBean.getMvNomCmToRefPri())
			|| !newMvMarketDataBean.getMvNominalPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvNominalPrice())
			|| !newMvMarketDataBean.getMvOpenPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvOpenPrice())
			|| !newMvMarketDataBean.getMvReferencePrice().equalsIgnoreCase(oldMvMarketDataBean.getMvReferencePrice())
			|| !newMvMarketDataBean.getMvSellForeignQty().equalsIgnoreCase(oldMvMarketDataBean.getMvSellForeignQty())
			|| !newMvMarketDataBean.getMvStockName().equalsIgnoreCase(oldMvMarketDataBean.getMvStockName())
			|| !newMvMarketDataBean.getMvStopLostPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvStopLostPrice())
			|| !newMvMarketDataBean.getMvStopProfitPrice().equalsIgnoreCase(oldMvMarketDataBean.getMvStopProfitPrice())
			|| !newMvMarketDataBean.getMvSymbol().equalsIgnoreCase(oldMvMarketDataBean.getMvSymbol())
			|| !newMvMarketDataBean.getMvTargetBuyPrc().equalsIgnoreCase(oldMvMarketDataBean.getMvTargetBuyPrc())
			|| !newMvMarketDataBean.getMvTotalTradingQty().equalsIgnoreCase(oldMvMarketDataBean.getMvTotalTradingQty())
			|| !newMvMarketDataBean.getMvTotalTradingValue().equalsIgnoreCase(oldMvMarketDataBean.getMvTotalTradingValue())
			|| !newMvMarketDataBean.getMvTotalVol().equalsIgnoreCase(oldMvMarketDataBean.getMvTotalVol())
			)		
		{
			isChanged = true;
			newMvMarketDataBean.setLstUpdate(new Date());
		}
		
		//Check mvStockInfoBean
		
		return isChanged;
	}
	
	public static List<StockWatchInfo> getStockWatchInfo(final String... stockWatchIds) {
		synchronized (mapStockWatch) {
			final List<StockWatchInfo> retList = new ArrayList<StockWatchInfo>();
			for (String stockWatchId : stockWatchIds) {
				final StockWatchInfo asi = mapStockWatch.get(stockWatchId);
				if (asi != null) {
					retList.add(asi);
				}
			}
			return retList;
		}
	}
	
	public static Set<String> getStockWatchIds()
	{
		return new HashSet<String>(mapStockWatch.keySet());
	}
	
	public static Collection<StockWatchInfo> getStockWatchValues()
	{
		return mapStockWatch.values();
	}
	
	public static StockWatchInfo getStockWatchInfo(final String stockWatchId) {
		synchronized (mapStockWatch) {
			StockWatchInfo asi = mapStockWatch.get(stockWatchId);
			return asi;
		}
	}
	
	public static StockWatchList getStockWatchList(final String accountNo)
	{
		StockWatchList swlist = new StockWatchList();
		List<StockWatchInfo> list = new ArrayList<StockWatchInfo>();
		Set<String> keyIds = new HashSet<String>(mapStockWatch.keySet());
		for(String swId : keyIds)
		{
			StockWatchInfo sw = mapStockWatch.get(swId);
			if(sw != null && accountNo.equalsIgnoreCase(sw.getMvClientId()))
			{
				list.add(sw);
			}			
		}		
		
		swlist.setStocks(list);
		
		return swlist;
	}

}
