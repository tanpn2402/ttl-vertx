package com.ttl.old.itrade.comet.dataInfo;


public class CommonInfo {
	//FO connection
	private boolean svIsConnected;
	//BO connection
	private boolean svExternalIsConnected;
	
	private String msgNotification;
	
	public CommonInfo() {
		super();
	}

	public CommonInfo(final boolean svIsConnected, final boolean svExternalIsConnected)
	{
		this.svIsConnected = svIsConnected;
		this.svExternalIsConnected = svExternalIsConnected;
	}

	public boolean isSvIsConnected() {
		return svIsConnected;
	}

	public void setSvIsConnected(boolean svIsConnected) {
		this.svIsConnected = svIsConnected;
	}

	public boolean isSvExternalIsConnected() {
		return svExternalIsConnected;
	}

	public void setSvExternalIsConnected(boolean svExternalIsConnected) {
		this.svExternalIsConnected = svExternalIsConnected;
	}

	public String getMsgNotification() {
		return msgNotification;
	}

	public void setMsgNotification(String msgNotification) {
		this.msgNotification = msgNotification;
	}
		
}
