package com.ttl.old.itrade.comet.dataInfo;

import java.util.ArrayList;
import java.util.List;

import com.ttl.old.itrade.hks.bean.HKSOrderBean;


public class OrderEnquiryInfo {
	private List<HKSOrderBean> orderBeanList = new ArrayList<HKSOrderBean>();

	public List<HKSOrderBean> getOrderBeanList() {
		return orderBeanList;
	}

	public void setOrderBeanList(List<HKSOrderBean> orderBeanList) {
		this.orderBeanList = orderBeanList;
	}
	
} 
