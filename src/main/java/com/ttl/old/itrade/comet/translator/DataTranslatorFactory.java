/* 
 * Copyright 2010 Sacombank Securities Company. All rights reserved.
 * Program  : STrade 
 * Revision : 1.0  
 * 
 * History Change
 * -----------------------------------
 * Date         Author          Reason
 * 20/08/2010   bac.nh          Use Comet Technology
 * 
 */
package com.ttl.old.itrade.comet.translator;

public class DataTranslatorFactory {
	private static DataTranslator dataTranslator;
	/**
	 * Set data translator.
	 * 
	 * @param translator DataTranslator
	 */
	public static void setDataTranslator(final DataTranslator translator) {
		dataTranslator = translator; 
	}
	/**
	 * Get the instance of DataTranslator.
	 * 
	 * @return DataTranslator
	 */
	public static DataTranslator getDataTranslator() {
		return dataTranslator;
	}
}
