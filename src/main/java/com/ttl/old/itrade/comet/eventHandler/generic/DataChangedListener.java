package com.ttl.old.itrade.comet.eventHandler.generic;

public interface DataChangedListener<T> {
	public void onChanged(DataChangedEvent<T> event);
}
