package com.ttl.old.itrade.comet.eventHandler.producer;

import java.util.ArrayList;
import java.util.List;

import com.ttl.old.itrade.comet.eventHandler.generic.EventProducer;
import com.ttl.old.itrade.comet.eventHandler.generic.EventQueue;
import com.ttl.old.itrade.hks.bean.HKSWorkMarketIndexBean;
import com.ttl.old.itrade.interfaces.IMain;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ttl.old.itrade.comet.dataInfo.WorldMarketIndex;
import com.ttl.old.itrade.comet.eventHandler.caching.WorldMarketIndexCachingManager;

public class WorldMarketIndexReader extends EventProducer<WorldMarketIndex> {
	private static final Logger logger = LogManager.getLogger(WorldMarketIndexReader.class);
	List<HKSWorkMarketIndexBean> mvWorldMarketIndexList = new ArrayList<HKSWorkMarketIndexBean>();

	@Override
	public void product(EventQueue<WorldMarketIndex> eventQueue) {
		try {
			if (IMain.mvWorldMarketIndex != null && IMain.mvWorldMarketIndex.size() > 0) {
				mvWorldMarketIndexList.clear();
				for (int i = 0; i < IMain.mvWorldMarketIndex.size(); i++) {
					if (IMain.mvWorldMarketIndex.get(i) != null) {
						mvWorldMarketIndexList.add(IMain.mvWorldMarketIndex.get(i));
					}
				}
			}

			if (WorldMarketIndexCachingManager.updateMarketDataIfChanged(mvWorldMarketIndexList)) {
				eventQueue.addDataEvent(WorldMarketIndexCachingManager.getLastWorldMarketIndex());
			}
		} catch (Exception ex) {
			logger.error(ex);
		}

	}
}
