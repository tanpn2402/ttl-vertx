package com.ttl.old.itrade.comet.eventHandler.generic;

import java.util.List;

public interface NotificationAction<T> {
	public void sendData(List<T> data);
	public void destroy();
}
