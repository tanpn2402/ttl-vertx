package com.ttl.old.itrade.comet.eventHandler.generic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EventQueue<T> {
	private final List<DataChangedListener<T>> listeners;
	private final List<T> queue;
	private boolean running = false;
	private boolean stoppingDone = false;
	public EventQueue() {
		listeners = Collections.synchronizedList(new ArrayList<DataChangedListener<T>>());
		queue = new ArrayList<T>();
	}
	
	public void addDataChangedListener(final DataChangedListener<T> lisneter) {
		listeners.add(lisneter);
	}
	
	public void removeDataChangedListener(final DataChangedListener<T> lisneter) {
		listeners.remove(lisneter);
	}
	
	public void addDataEvent(final T... events) {
		synchronized (queue) {
			for (T evt : events)
				queue.add(evt);
			queue.notify();
		}
	}
	
	private void notifyDataChanged(final List<T> pendingList) {
		for (final DataChangedListener<T> l : listeners) {
			l.onChanged(new DataChangedEvent<T>(pendingList));									
		}
	}
	
	public void start() {
		running = true;
		final Thread thread = new Thread(new Runnable() {			
			public void run() {
				while (running) {					
					synchronized (queue) {
						if (queue.isEmpty()) {
							try {
								queue.wait();
							} catch (InterruptedException e) {}
						}
						if (!queue.isEmpty()) {
							final List<T> pendingList = new ArrayList<T>();
							pendingList.addAll(queue);
							queue.clear();
							
							notifyDataChanged(pendingList);
						}	
					}					
				}
				//Can stop
				synchronized (queue) {
					queue.notify();
				}
				stoppingDone = true;
			}
		}, "EventQueueNotifyThread");
		thread.setDaemon(true);
		thread.start();
	}
	
	public void stop() {
		if (!running)
			return;
		running = false;
		while (!stoppingDone) {
			synchronized (queue) {
				queue.notify();
				if (!queue.isEmpty()) {
					try {
						queue.wait();
					} catch (InterruptedException e) {}
				}
			}
		}
	}
}
