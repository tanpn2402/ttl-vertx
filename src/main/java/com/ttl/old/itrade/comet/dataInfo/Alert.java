package com.ttl.old.itrade.comet.dataInfo;

import java.util.Date;

import net.sf.json.JSONObject;

public abstract class Alert {
	protected String alertType;
	protected Date time;
		
	public String getAlertType() {
		return alertType;
	}
	
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}
	
	public Date getTime() {
		return time;
	}
	
	public void setTime(Date time) {
		this.time = time;
	}		

	public abstract JSONObject toJson();
}
