package com.systekit.common.obp;

/**
 * A thread pool that uses ObpThreadFactory to create threads
 * To use the pool, just checkout a object, which will be of type ObpWorkerThread
 * and use the ObpWorkerThread.doJob to exec a job.
 * The entry point of a worker thread is defined in the class which has to implement a interface
 * from IObpThreadPoolJob
 * For example
 *
 * 	ivThreadPool = new ObpThreadPool("Test Pool", 10, 10000);
 *    ObpWorkerThread ivThread = (ObpWorkerThread) ivThreadPool.checkOut(true);
 *  	ivThread.doJob(someobject that implements IObpThreadPoolJob);
 *
 * The checkout thread will do the job and will check in automatically when the job finished.
 * user of the thread pool has to make sure that the entry point does not throw any exception and
 * the job will not run continusously without ending, doing so will cause a thread leakage of the pool
 *
 * Creation date: (20/6/2001)
 * @author: Pear
 */

public class ObpThreadPool extends ObpObjectPool
{
   public ObpThreadPool(String pPoolName, int pInitialCapacity, int pMaxCapacity)
	{
		super(new ObpThreadFactory(new ThreadGroup(pPoolName)), pMaxCapacity);
		ObpThreadFactory lvThreadFactory = (ObpThreadFactory) ivObjectFactory;
		lvThreadFactory.setThreadPool(this);
		createInitialObject(pInitialCapacity);
   }
}
