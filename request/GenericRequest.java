package com.itrade.hks.request;

import com.itrade.tp.TPBaseRequest;
import com.itrade.tp.TPManager;
import com.systekit.common.msg.IMsgXMLNode;

/**
 * The GenericRequest class extends TPBaseRequest abstract class defined methods that operation xml with generic request.
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

public class GenericRequest extends TPBaseRequest
{
	/**
	 * Constructor for GenericRequest class.
	 * @param pTPManager Object of TPManager.
	 * @param pTemplateNode Object of IMsgXMLNode.
	 */
	public GenericRequest(TPManager pTPManager, IMsgXMLNode pTemplateNode)
	{
		super(pTPManager, pTemplateNode);
	}
}
