package com.ttl.old.itrade.hks.txn;

import java.util.*;
import com.systekit.common.msg.*;
import com.ttl.old.itrade.ITradeServlet;
import com.ttl.old.itrade.hks.request.RequestName;
import com.ttl.old.itrade.tp.TPBaseRequest;
import com.ttl.old.itrade.tp.TPErrorHandling;
import com.ttl.old.itrade.tp.TPManager;
import com.ttl.old.itrade.util.Log;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author not attributable
 * @version 1.0
 */

/**
 * Title:        Fund Transfer Information Transaction
 * Description:  This class stores and processes the Fund Transfer transaction
 * Copyright:    Copyright (c) 2002
 * Company:
 * @author
 * @version 1.0
 * @since 2002.11.28
 */
/**
 * This HKSFundTransferSubmitTxn class definition for all method
 * stores and processes the Fund Transfer transaction
 * @author not attributable
 * @since 2002.11.28
 */
public class HKSFundTransferSubmitTxn
{
    private String mvClientId;
    private String mvDW;
    private String mvAmount;
    private int mvReturnCode;
    private String mvStatus;
    private String mvReturnMessage;
    private String mvErrorCode;
    private String mvErrorMessage;

    private String mvPassword;
    private String mvIsSuccess;
    private boolean mvConfirmationWithPass;
    private Vector mvAccountNumber = new Vector();

    public static final String CLIENTID = "CLIENTID";
    public static final String DW = "DW";
    public static final String AMOUNT = "AMOUNT";

    public static final String RETURNMESSAGE = "RETURNMESSAGE";
    /**
     * This variable is used to the tp Error
     */
    TPErrorHandling tpError;
    /**
     * Set method for Collection used to store account number
     * @param pAccountNumber the Collection used to store account number
     */
    public void setMvAccountNumber(Vector pAccountNumber)
    {
        this.mvAccountNumber = pAccountNumber;
    }
    /**
     * Get method for Collection used to store account number
     * @return Collection used to store account number
     */
    public Vector getMvAccountNumber()
    {
        return mvAccountNumber;
    }

   /**
    * Default constructor for HKSFundTransferSubmitTxn class
    */
    public HKSFundTransferSubmitTxn()
    {
        tpError = new TPErrorHandling();
    }
    ;

    /**
     * Constructor for HKSFundTransferSubmitTxn class
     * @param pClientId the client id
     */
    public HKSFundTransferSubmitTxn(String pClientId)
    {
        setClientId(pClientId);
        tpError = new TPErrorHandling();
    }

   /**
    * Constructor for HKSFundTransferSubmitTxn class
    * @param pClientId the client id
    * @param pDW the dw
    * @param pAmount the amout 
    */
    public HKSFundTransferSubmitTxn(String pClientId, String pDW, String pAmount)
    {
        setClientId(pClientId);
        setDW(pDW);
        setAmount(pAmount);

        tpError = new TPErrorHandling();
    }
    /**
     * This method process stores and processes the Fund Transfer transaction
     */
    public void process()
    {
        Log.println("[ HKSFundTransferSubmitTxn.process() starts [" + getClientId() + "]", Log.ACCESS_LOG);
        try
        {
            Hashtable lvTxnMap = new Hashtable();
            IMsgXMLNode lvRetNode;
            TPManager ivTPManager = ITradeServlet.getTPManager(RequestName.HKSFundTransferSubmitRequest);
            TPBaseRequest lvRequest = ivTPManager.getRequest(RequestName.HKSFundTransferSubmitRequest);

            lvTxnMap.put(CLIENTID, getClientId());
            lvTxnMap.put(DW, getDW());
            lvTxnMap.put(AMOUNT, getAmount());

            Log.println("[ HKSFundTransferSubmitTxn.process() finished constructing lvTxnMap [" + getClientId() + "]", Log.ACCESS_LOG);

            lvRetNode = lvRequest.send(lvTxnMap);

            setReturnCode(tpError.checkError(lvRetNode));
            if (mvReturnCode != tpError.TP_NORMAL)
            {
                setErrorMessage(tpError.getErrDesc());
                if (mvReturnCode == tpError.TP_APP_ERR)
                {
                    setErrorCode(tpError.getErrCode());
                }
            }
            else
            {
                IMsgXMLNodeList lvAccountNumberList = lvRetNode.getNodeList("ACCOUNTNUMBER");
                for (int i = 0; i < lvAccountNumberList.size(); i++)
                {
                    mvAccountNumber.add(lvAccountNumberList.getNode(i).getValue());
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

   /**
    * Get method for Client ID
    * @return client id
    */
    public String getClientId()
    {
        return mvClientId;
    }

   /**
    * Set method for Client ID
    * @param pClientId the client id
    */
    public void setClientId(String pClientId)
    {
        mvClientId = pClientId;
    }

    /**
     * Get method for DW
     * @return dw
     */
    public String getDW()
    {
        return mvDW;
    }

    /**
     * Set method for DW
     * @param pDW the dw
     */
    public void setDW(String pDW)
    {
        mvDW = pDW;
    }

   /**
    * Get method for Amount
    * @return Amount
    */
    public String getAmount()
    {
        return mvAmount;
    }

    /**
     * Set method for Amount
     * @param pAmount the Amount
     */
    public void setAmount(String pAmount)
    {
        mvAmount = pAmount;
    }

   /**
    * Get method for system Return Code
    * @return system Return Code
    */
    public int getReturnCode()
    {
        return mvReturnCode;
    }

   /**
    * Set method for system Return Code
    * @param pReturnCode the system Return Code
    */
    public void setReturnCode(int pReturnCode)
    {
        mvReturnCode = pReturnCode;
    }

   /**
    * Get method for system Return Message
    * @return system Return Message
    */
    public String getReturnMessage()
    {
        return mvReturnMessage;
    }

    /**
     * Set method for system Return Message
     * @param pReturnMessage the system Return Message
     */
    public void setReturnMessage(String pReturnMessage)
    {
        mvReturnMessage = pReturnMessage;
    }

    /**
     * Get method for system Error Code
     * @return system Error Code
     */
    public String getErrorCode()
    {
        return mvErrorCode;
    }

    /**
     * Set method for system Error Code
     * @param pErrorCode the system Error Code
     */
    public void setErrorCode(String pErrorCode)
    {
        mvErrorCode = pErrorCode;
    }

    /**
     * Get method for System Error Message
     * @return System Error Message
     */
    public String getErrorMessage()
    {
        return mvErrorMessage;
    }

    /**
     * Set method for System Error Message
     * @param pErrorMessage the System Error Message
     */
    public void setErrorMessage(String pErrorMessage)
    {
        mvErrorMessage = pErrorMessage;
    }

   /**
    * Get method for fund transfer Is Success
    * @return fund transfer Is Success
    */
    public String getIsSuccess()
    {
        return mvIsSuccess;
    }

    /**
     * Set method for fund transfer Is Success
     * @param pIsSuccess the fund transfer Is Success
     */
    public void setIsSuccess(String pIsSuccess)
    {
        this.mvIsSuccess = pIsSuccess;
    }

    /**
     * Get method for bank account Password
     * @return bank account Password
     */
    public String getPassword()
    {
        return mvPassword;
    }

   /**
    * Set method for bank account Password
    * @param pPassword the bank account Password
    */
    public void setPassword(String pPassword)
    {
        mvPassword = pPassword == null ? "" : pPassword;
    }

    /**
     * Get method for Confirmation password is passed
     * @return Confirmation password is passed
     */
    public boolean getConfirmationWithPass()
    {
        return mvConfirmationWithPass;
    }

    /**
     * Set method for Confirmation password is passed
     * @param pConfirmationWithPass the Confirmation password is passed
     */
    public void setConfirmationWithPass(boolean pConfirmationWithPass)
    {
        mvConfirmationWithPass = pConfirmationWithPass;
    }

}
