
echo "Running install library"
mvn clean
echo "Intall Common"
mvn install:install-file -Dfile=lib/Common.jar -DgroupId=com.itrade -DartifactId=Common -Dversion=1.0 -Dpackaging=jar
echo "install CoreCommon"
mvn install:install-file -Dfile=lib/CoreCommon.jar -DgroupId=com.itrade -DartifactId=CoreCommon -Dversion=1.0 -Dpackaging=jar
echo "install DecEncrypt"
mvn install:install-file -Dfile=lib/DecEncrypt.jar -DgroupId=com.itrade -DartifactId=DecEncrypt -Dversion=1.0 -Dpackaging=jar
echo "install EzXML"
mvn install:install-file -Dfile=lib/EzXML.jar -DgroupId=com.itrade -DartifactId=EzXML -Dversion=1.0 -Dpackaging=jar
echo "install HKSCommon"
mvn install:install-file -Dfile=lib/HKSCommon.jar -DgroupId=com.itrade -DartifactId=HKSCommon -Dversion=1.0 -Dpackaging=jar
echo "install Tag"
mvn install:install-file -Dfile=lib/Tag.jar -DgroupId=com.itrade -DartifactId=Tag -Dversion=1.0 -Dpackaging=jar
echo "install axis-ant"
mvn install:install-file -Dfile=lib/axis-ant.jar -DgroupId=com.itrade -DartifactId=axis-ant -Dversion=1.0 -Dpackaging=jar
echo "install axis"
mvn install:install-file -Dfile=lib/axis.jar -DgroupId=com.itrade -DartifactId=axis -Dversion=1.0 -Dpackaging=jar
echo "install com.ttl.mds.model"
mvn install:install-file -Dfile=lib/com.ttl.mds.model.jar -DgroupId=com.itrade -DartifactId=com.ttl.mds.model -Dversion=1.0 -Dpackaging=jar
echo "install commons-beanutils-1.8.0"
mvn install:install-file -Dfile=lib/commons-beanutils-1.8.0.jar -DgroupId=com.itrade -DartifactId=commons-beanutils-1.8.0 -Dversion=1.0 -Dpackaging=jar
echo "install commons-beanutils-collections"
mvn install:install-file -Dfile=lib/commons-beanutils-bean-collections-1.8.0.jar -DgroupId=com.itrade -DartifactId=commons-beanutils-bean-collections-1.8.0 -Dversion=1.0 -Dpackaging=jar
echo "install commons-beanutils-core-1.8.0"
mvn install:install-file -Dfile=lib/commons-beanutils-core-1.8.0.jar -DgroupId=com.itrade -DartifactId=commons-beanutils-core-1.8.0 -Dversion=1.0 -Dpackaging=jar
echo "install commons-codec-1.1"
mvn install:install-file -Dfile=lib/commons-codec-1.1.jar -DgroupId=com.itrade -DartifactId=commons-codec-1.1 -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=lib/commons-codec-1.4.jar -DgroupId=com.itrade -DartifactId=commons-codec-1.4 -Dversion=1.0 -Dpackaging=jar
echo "install commons-collections-2.1.1"
mvn install:install-file -Dfile=lib/commons-collections-2.1.1.jar -DgroupId=com.itrade -DartifactId=commons-collections-2.1.1 -Dversion=1.0 -Dpackaging=jar
echo "install commons-collections-2.1.1"
mvn install:install-file -Dfile=lib/commons-collections-2.1.1.jar -DgroupId=com.itrade -DartifactId=commons-collections-2.1.1 -Dversion=1.0 -Dpackaging=jar
echo "install commons-collections-3.2"
mvn install:install-file -Dfile=lib/commons-collections-3.2.jar -DgroupId=com.itrade -DartifactId=commons-collections-3.2 -Dversion=1.0 -Dpackaging=jar
echo "install commons-disgester-1.7"
mvn install:install-file -Dfile=lib/commons-digester-1.7.jar -DgroupId=com.itrade -DartifactId=commons-digester-1.7 -Dversion=1.0 -Dpackaging=jar
echo "install commons-discovery-0.2"
mvn install:install-file -Dfile=lib/commons-discovery-0.2.jar -DgroupId=com.itrade -DartifactId=commons-discovery-0.2 -Dversion=1.0 -Dpackaging=jar
echo "install commons-fileupload-1.2.1"
mvn install:install-file -Dfile=lib/commons-fileupload-1.2.1.jar -DgroupId=com.itrade -DartifactId=commons-fileupload-1.2.1 -Dversion=1.0 -Dpackaging=jar
echo "install commons-httpclient-3.0.1"
mvn install:install-file -Dfile=lib/commons-httpclient-3.0.1.jar -DgroupId=com.itrade -DartifactId=commons-httpclient-3.0.1 -Dversion=1.0 -Dpackaging=jar
echo "install commons-io-1.3.1."
mvn install:install-file -Dfile=lib/commons-io-1.3.1.jar -DgroupId=com.itrade -DartifactId=commons-io-1.3.1 -Dversion=1.0 -Dpackaging=jar
echo "install commons-lang-2.1"
mvn install:install-file -Dfile=lib/commons-lang-2.1.jar -DgroupId=com.itrade -DartifactId=commons-lang-2.1 -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=lib/commons-logging-1.1.jar -DgroupId=com.itrade -DartifactId=commons-logging-1.1 -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=lib/commons-validator-1.3.1.jar -DgroupId=com.itrade -DartifactId=commons-validator-1.3.1 -Dversion=1.0 -Dpackaging=jar
echo "install crimson"
mvn install:install-file -Dfile=lib/crimson.jar -DgroupId=com.itrade -DartifactId=crimson -Dversion=1.0 -Dpackaging=jar
echo "install derbyclient"
mvn install:install-file -Dfile=lib/derbyclient.jar -DgroupId=com.itrade -DartifactId=derbyclient -Dversion=1.0 -Dpackaging=jar
echo "install ezmorph"
mvn install:install-file -Dfile=lib/ezmorph-1.0.jar -DgroupId=com.itrade -DartifactId=ezmorph-1.0 -Dversion=1.0 -Dpackaging=jar
echo "install itext"
mvn install:install-file -Dfile=lib/itext-2.1.0.jar -DgroupId=com.itrade -DartifactId=itext-2.1.0 -Dversion=1.0 -Dpackaging=jar
echo "install filters"
mvn install:install-file -Dfile=lib/filters-2.0.235.jar -DgroupId=com.itrade -DartifactId=filters-2.0.235 -Dversion=1.0 -Dpackaging=jar
echo "install freemarker"
mvn install:install-file -Dfile=lib/freemarker-2.3.8.jar -DgroupId=com.itrade -DartifactId=freemarker-2.3.8 -Dversion=1.0 -Dpackaging=jar
echo "install jasperreports"
mvn install:install-file -Dfile=lib/jasperreports-3.7.0.jar -DgroupId=com.itrade -DartifactId=jasperreports-3.7.0 -Dversion=1.0 -Dpackaging=jar
echo "install jasypt"
mvn install:install-file -Dfile=lib/jasypt-1.5.jar -DgroupId=com.itrade -DartifactId=jasypt-1.5 -Dversion=1.0 -Dpackaging=jar
echo "install jaxrpc"
mvn install:install-file -Dfile=lib/jaxrpc.jar -DgroupId=com.itrade -DartifactId=jaxrpc -Dversion=1.0 -Dpackaging=jar
echo "install jcaptcha"
mvn install:install-file -Dfile=lib/jcaptcha-all-1.0-RC6.jar -DgroupId=com.itrade -DartifactId=jcaptcha-all-1.0-RC6 -Dversion=1.0 -Dpackaging=jar
echo "install jcommon-1.0.14.jar"
mvn install:install-file -Dfile=lib/jcommon-1.0.14.jar -DgroupId=com.itrade -DartifactId=jcommon-1.0.14 -Dversion=1.0 -Dpackaging=jar
echo "install jdt-compiler-3.1.1.jar"
mvn install:install-file -Dfile=lib/jdt-compiler-3.1.1.jar -DgroupId=com.itrade -DartifactId=jdt-compiler-3.1.1 -Dversion=1.0 -Dpackaging=jar
echo "install jetty-continuation-"
mvn install:install-file -Dfile=lib/jetty-continuation-7.1.6.jar -DgroupId=com.itrade -DartifactId=jetty-continuation-7.1.6 -Dversion=1.0 -Dpackaging=jar
echo "install json"
mvn install:install-file -Dfile=lib/json-lib-2.4-jdk15.jar -DgroupId=com.itrade -DartifactId=json-lib-2.4-jdk15 -Dversion=1.0 -Dpackaging=jar
echo "install jsonplugin"
mvn install:install-file -Dfile=lib/jsonplugin-0.32.jar -DgroupId=com.itrade -DartifactId=jsonplugin-0.32 -Dversion=1.0 -Dpackaging=jar
echo "install jstl"
mvn install:install-file -Dfile=lib/jstl.jar -DgroupId=com.itrade -DartifactId=jstl -Dversion=1.0 -Dpackaging=jar
echo "install jxl"
mvn install:install-file -Dfile=lib/jxl.jar -DgroupId=com.itrade -DartifactId=jxl -Dversion=1.0 -Dpackaging=jar
echo "install jtds"
mvn install:install-file -Dfile=lib/jtds-1.2.4.jar -DgroupId=com.itrade -DartifactId=jtds-1.2.4 -Dversion=1.0 -Dpackaging=jar
echo "install log4j"
mvn install:install-file -Dfile=lib/log4j-1.2.14.jar -DgroupId=com.itrade -DartifactId=log4j-1.2.14 -Dversion=1.0 -Dpackaging=jar
echo "install ognl"
mvn install:install-file -Dfile=lib/ognl-2.6.11.jar -DgroupId=com.itrade -DartifactId=ognl-2.6.11 -Dversion=1.0 -Dpackaging=jar
echo "install packtag"
mvn install:install-file -Dfile=lib/packtag-3.7.jar -DgroupId=com.itrade -DartifactId=packtag-3.7 -Dversion=1.0 -Dpackaging=jar
echo "install poi"
mvn install:install-file -Dfile=lib/poi-3.5-FINAL-20090928.jar -DgroupId=com.itrade -DartifactId=poi-3.5-FINAL-20090928 -Dversion=1.0 -Dpackaging=jar
echo "install quartz"
mvn install:install-file -Dfile=lib/quartz-1.8.3.jar -DgroupId=com.itrade -DartifactId=quartz-1.8.3 -Dversion=1.0 -Dpackaging=jar
echo "install saaj"
mvn install:install-file -Dfile=lib/saaj.jar -DgroupId=com.itrade -DartifactId=saaj -Dversion=1.0 -Dpackaging=jar
echo "install servlet-api.jar"
mvn install:install-file -Dfile=lib/servlet-api-2.5.jar -DgroupId=com.itrade -DartifactId=servlet-api-2.5 -Dversion=1.0 -Dpackaging=jar
echo "install slf4j-api-1."
mvn install:install-file -Dfile=lib/slf4j-api-1.5.10.jar -DgroupId=com.itrade -DartifactId=slf4j-api-1.5.10 -Dversion=1.0 -Dpackaging=jar
echo "install slf4j-log4j12-1.5.10"
mvn install:install-file -Dfile=lib/slf4j-log4j12-1.5.10.jar -DgroupId=com.itrade -DartifactId=slf4j-log4j12-1.5.10 -Dversion=1.0 -Dpackaging=jar
echo "install speedParser"
mvn install:install-file -Dfile=lib/speedParser.jar -DgroupId=com.itrade -DartifactId=speedParser -Dversion=1.0 -Dpackaging=jar
echo "install standard"
mvn install:install-file -Dfile=lib/standard.jar -DgroupId=com.itrade -DartifactId=standard -Dversion=1.0 -Dpackaging=jar
echo "install wb"
mvn install:install-file -Dfile=lib/wb.jar -DgroupId=com.itrade -DartifactId=wb -Dversion=1.0 -Dpackaging=jar
echo "install wsdl4j"
mvn install:install-file -Dfile=lib/wsdl4j.jar -DgroupId=com.itrade -DartifactId=wsdl4j -Dversion=1.0 -Dpackaging=jar
echo "install xercesImpl"
mvn install:install-file -Dfile=lib/xercesImpl-2.7.0.jar -DgroupId=com.itrade -DartifactId=xercesImpl-2.7.0 -Dversion=1.0 -Dpackaging=jar
echo "install xwork"
mvn install:install-file -Dfile=lib/xwork-2.0.1.jar -DgroupId=com.itrade -DartifactId=xwork-2.0.1 -Dversion=1.0 -Dpackaging=jar
echo "install MDSInfogate"
mvn install:install-file -Dfile=lib/ttl.MDSInfogate-4.0.0.jar -DgroupId=com.itrade -DartifactId=ttl.MDSInfogate-4.0.0 -Dversion=1.0 -Dpackaging=jar
echo "install MDSAPI"
mvn install:install-file -Dfile=lib/ttl.MDSAPI-4.0.0.jar -DgroupId=com.itrade -DartifactId=ttl.MDSAPI-4.0.0 -Dversion=1.0 -Dpackaging=jar
echo "install MDSMessage"
mvn install:install-file -Dfile=lib/ttl.MDSMessage-4.0.0.jar -DgroupId=com.itrade -DartifactId=ttl.MDSMessage-4.0.0 -Dversion=1.0 -Dpackaging=jar
echo "install javolution"
mvn install:install-file -Dfile=lib/javolution-5.4.5.jar -DgroupId=com.itrade -DartifactId=javolution-5.4.5 -Dversion=1.0 -Dpackaging=jar
echo "install MDSCommon"
mvn install:install-file -Dfile=lib/ttl.MDSCommon-4.0.0.jar -DgroupId=com.itrade -DartifactId=ttl.MDSCommon-4.0.0 -Dversion=1.0 -Dpackaging=jar
echo "install disruptor"
mvn install:install-file -Dfile=lib/disruptor-2.10.4.jar -DgroupId=com.itrade -DartifactId=disruptor-2.10.4 -Dversion=1.0 -Dpackaging=jar
echo "install guava"
mvn install:install-file -Dfile=lib/guava-15.0.jar -DgroupId=com.itrade -DartifactId=guava-15.0 -Dversion=1.0 -Dpackaging=jar




